!-------------------------------------------------------------------------------
!< accelerated BLAS routines, specifically from cublas
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!* module containing accelerated BLAS routines, specifically from cublas
!-------------------------------------------------------------------------------
MODULE acc_blas_mod
  USE cublas
  USE iso_c_binding
  USE local
  USE openacc
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: acc_blas

  TYPE :: acc_blas_type
    !* cublas handle
    TYPE(cublashandle) :: h
  CONTAINS

    PROCEDURE, PASS(abt) :: init
    PROCEDURE, PASS(abt) :: destroy
    PROCEDURE, PASS(abt) :: dmatinvBatched
    PROCEDURE, PASS(abt) :: zmatinvBatched
  END TYPE

  TYPE(acc_blas_type) :: acc_blas
CONTAINS

!-------------------------------------------------------------------------------
!* initialize the cublas library
!-------------------------------------------------------------------------------
  SUBROUTINE init(abt)
    IMPLICIT NONE

    !* acc blas type use
    CLASS(acc_blas_type), INTENT(INOUT) :: abt

    INTEGER(c_int) :: istat

    istat=cublasCreate(abt%h)
    istat=cublasSetMathMode(abt%h,                                              &
                            CUBLAS_MATH_DISALLOW_REDUCED_PRECISION_REDUCTION)
  END SUBROUTINE init

!-------------------------------------------------------------------------------
!* destroy (deallocate) the cublas library
!-------------------------------------------------------------------------------
  SUBROUTINE destroy(abt)
    IMPLICIT NONE

    !* acc blas type use
    CLASS(acc_blas_type), INTENT(INOUT) :: abt

    INTEGER(c_int) :: istat

    istat=cublasDestroy(abt%h)
  END SUBROUTINE destroy

!-------------------------------------------------------------------------------
!* find batched matrix inverses for double types
!-------------------------------------------------------------------------------
  SUBROUTINE dmatinvBatched(abt,n,na,dptr_matrix,dptr_matinv,id)
    IMPLICIT NONE

    !* acc blas type use
    CLASS(acc_blas_type), INTENT(INOUT) :: abt
    !* number of row/columns in each matrix
    INTEGER(i4), INTENT(IN) :: n
    !* number of matrices
    INTEGER(i4), INTENT(IN) :: na
    !* device pointers to matrices to invert
    TYPE(c_devptr), INTENT(IN) :: dptr_matrix(na)
    !* device pointers to output matrices inverse
    TYPE(c_devptr), INTENT(IN) :: dptr_matinv(na)
    !* kernel async stream ID
    INTEGER(i4), INTENT(IN) :: id

    INTEGER(c_int) :: istat,infomatinv(na)
    INTEGER(i4) :: jq

    !$acc enter data create(infomatinv) async(id)
    istat=cublasSetStream(abt%h,acc_get_cuda_stream(id))
!-------------------------------------------------------------------------------
!   call cublas
!-------------------------------------------------------------------------------
    !$acc host_data use_device(dptr_matrix,dptr_matinv,infomatinv)
    istat=cublasDmatinvBatched(abt%h,n,dptr_matrix,n,dptr_matinv,n,             &
                               infomatinv,na)
    !$acc end host_data
    !$acc exit data delete(infomatinv) finalize async(id)
    RETURN
  END SUBROUTINE dmatinvBatched

!-------------------------------------------------------------------------------
!* find batched matrix inverses for complex double types
!-------------------------------------------------------------------------------
  SUBROUTINE zmatinvBatched(abt,n,na,dptr_matrix,dptr_matinv,id)
    IMPLICIT NONE

    !* acc blas type use
    CLASS(acc_blas_type), INTENT(INOUT) :: abt
    !* number of row/columns in each matrix
    INTEGER(i4), INTENT(IN) :: n
    !* number of matrices
    INTEGER(i4), INTENT(IN) :: na
    !* device pointers to matrices to invert
    TYPE(c_devptr), INTENT(IN) :: dptr_matrix(na)
    !* device pointers to output matrices inverse
    TYPE(c_devptr), INTENT(IN) :: dptr_matinv(na)
    !* kernel async stream ID
    INTEGER(i4), INTENT(IN) :: id

    INTEGER(c_int) :: istat,infomatinv(na)
    INTEGER(i4) :: jq

    !$acc enter data create(infomatinv) async(id)
    istat=cublasSetStream(abt%h,acc_get_cuda_stream(id))
!-------------------------------------------------------------------------------
!   call cublas
!-------------------------------------------------------------------------------
    !$acc host_data use_device(dptr_matrix,dptr_matinv,infomatinv)
    istat=cublasZmatinvBatched(abt%h,n,dptr_matrix,n,dptr_matinv,n,             &
                               infomatinv,na)
    !$acc end host_data
    !$acc exit data delete(infomatinv) finalize async(id)
    RETURN
  END SUBROUTINE zmatinvBatched

END MODULE acc_blas_mod
