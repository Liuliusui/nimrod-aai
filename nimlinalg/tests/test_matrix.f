!-------------------------------------------------------------------------------
!* test matrix routines driver
!-------------------------------------------------------------------------------
PROGRAM test_matrix
  USE linalg_registry_mod
  USE local
  USE matrix_mod
  USE nimtest_utils
  USE pardata_mod
  USE seam_mod
  USE timer_mod
  USE vector_mod
  IMPLICIT NONE

  TYPE(rvec_storage), ALLOCATABLE :: rvec(:)
  TYPE(cv1m_storage), ALLOCATABLE :: cv1m(:)
  TYPE(cvec_storage), ALLOCATABLE :: cvec(:)
  TYPE(rmat_storage), ALLOCATABLE :: rmat(:)
  TYPE(cmat_storage), ALLOCATABLE :: cmat(:,:)
  TYPE(seam_type) :: seam
  INTEGER(i4) :: nargs,itest
  CHARACTER(64) :: test_name
  INTEGER(i4) :: idepth
  INTEGER(i4), SAVE :: iftn=-1
!-------------------------------------------------------------------------------
! determine which test to run from the command line
!-------------------------------------------------------------------------------
  nargs=command_argument_count()
  IF (nargs /= 2) THEN
    CALL print_usage
    CALL par%nim_stop('Argument error')
  ENDIF
  CALL get_command_argument(1,test_type)
  CALL get_command_argument(2,test_name)
  CALL par%init
  CALL timer%init
  CALL timer%start_timer_l0('test','test_matrix',iftn,idepth)
!-------------------------------------------------------------------------------
! setup registry based on test type
!-------------------------------------------------------------------------------
  CALL set_test_type
!-------------------------------------------------------------------------------
! use only a single block for these tests
!-------------------------------------------------------------------------------
  single_block=.TRUE.
!-------------------------------------------------------------------------------
! loop over test parameters
!-------------------------------------------------------------------------------
  DO itest=1,ntests
    CALL setup_linalg(rvec,cv1m,cvec,rmat,cmat,seam,itest)
!-------------------------------------------------------------------------------
!   run the test
!-------------------------------------------------------------------------------
    SELECT CASE(TRIM(test_name))
    CASE ("alloc_dealloc")
      ! do nothing
    CASE ("matvec")
      CALL test_matvec(rmat(1)%m,cmat(1,1)%m,rvec(1)%v,cv1m(1)%v,cvec(1)%v)
    CASE ("static_condensation")
      CALL test_static_condensation(rmat(1)%m,cmat(1,1)%m,                      &
                                    rvec(1)%v,cv1m(1)%v,cvec(1)%v)
    CASE ("assemble")
      CALL test_assemble(rmat(1)%m,cmat(1,1)%m,rvec(1)%v,cv1m(1)%v)
    CASE ("bc_regularity")
      CALL test_bc_regularity(rmat(1)%m,cmat(1,1)%m,                            &
                              rvec(1)%v,cv1m(1)%v,seam%s(1))
    CASE ("jacobi_infrastructure")
      CALL test_jacobi_infrastructure(rmat(1)%m,cmat(1,1)%m,                    &
                                      rvec(1)%v,cv1m(1)%v,cvec(1)%v,seam%s(1))
    CASE DEFAULT
      CALL par%nim_write('No test named '//TRIM(test_name))
      CALL print_usage
    END SELECT
    CALL teardown_linalg(rvec,cv1m,cvec,rmat,cmat,seam)
  ENDDO
  CALL timer%end_timer_l0(iftn,idepth)
  CALL timer%report
  CALL timer%finalize
  CALL par%nim_stop('Normal termination.',clean_shutdown=.TRUE.)
CONTAINS

!-------------------------------------------------------------------------------
! helper routine to print usage
!-------------------------------------------------------------------------------
  SUBROUTINE print_usage
    CALL par%nim_write('Usage: ./test_matrix <test type> <test name>')
    CALL print_types
    CALL par%nim_write('  where <test name> is one of')
    CALL par%nim_write('    alloc_dealloc')
    CALL par%nim_write('    matvec')
    CALL par%nim_write('    static_condensation')
    CALL par%nim_write('    assemble')
    CALL par%nim_write('    bc_regularity')
    CALL par%nim_write('    jacobi_infrastructure')
    CALL par%nim_stop('check input')
  END SUBROUTINE print_usage

!-------------------------------------------------------------------------------
!* set a vector to a constant, multiply by the identity and confirm no change
!-------------------------------------------------------------------------------
  SUBROUTINE test_matvec(rmat,cmat,rvec,cv1m,cvec)
    USE matrix_mod
    USE nimtest_utils
    USE vector_mod
    IMPLICIT NONE

    CLASS(rmatrix), INTENT(INOUT) :: rmat
    CLASS(cmatrix), INTENT(INOUT) :: cmat
    CLASS(rvector), INTENT(INOUT) :: rvec
    CLASS(cvector1m), INTENT(INOUT) :: cv1m
    CLASS(cvector), INTENT(INOUT) :: cvec

    CLASS(rvector), ALLOCATABLE :: rvec_out
    CLASS(cvector1m), ALLOCATABLE :: cv1m_out
    CLASS(cvector), ALLOCATABLE :: cvec_out
    INTEGER(i4) :: imode
!-------------------------------------------------------------------------------
!   test the real version
!-------------------------------------------------------------------------------
    CALL rvec%alloc_with_mold(rvec_out)
    CALL rvec%assign_for_testing('index')
    CALL rmat%set_identity
    CALL rmat%matvec(rvec,rvec_out)
    CALL wrtrue(rvec%test_if_equal(rvec_out),"rmat%matvec")
    CALL rvec_out%dealloc
!-------------------------------------------------------------------------------
!   test the complex version -- single-mode interface
!-------------------------------------------------------------------------------
    CALL cv1m%alloc_with_mold(cv1m_out)
    CALL cv1m%assign_for_testing('index')
    CALL cmat%set_identity
    CALL cmat%matvec(cv1m,cv1m_out)
    CALL wrtrue(cv1m%test_if_equal(cv1m_out),"cmat%matvec1m")
    CALL cv1m_out%dealloc
!-------------------------------------------------------------------------------
!   test the complex version -- multi-mode interface
!-------------------------------------------------------------------------------
    CALL cvec%alloc_with_mold(cvec_out)
    CALL cvec%assign_for_testing('index')
    CALL cmat%set_identity
    DO imode=1,cvec%nmodes
      CALL cmat%matvec(cvec,cvec_out,imode,imode)
    ENDDO
    CALL wrtrue(cvec%test_if_equal(cvec_out),"cmat%matvec")
    CALL cvec_out%dealloc
  END SUBROUTINE test_matvec

!-------------------------------------------------------------------------------
!> Set a matrix to the identity, apply elim_inv_int to the matrix and then set
!  a vector to a constant and apply elim_presolve. Do a matvec and then apply
!  elim_postsolve to the result. Confirm that the vector is equal to the
!  originial value. For the special case of the identity this works.
!-------------------------------------------------------------------------------
  SUBROUTINE test_static_condensation(rmat,cmat,rvec,cv1m,cvec)
    USE matrix_mod
    USE nimtest_utils
    USE vector_mod
    IMPLICIT NONE

    CLASS(rmatrix), INTENT(INOUT) :: rmat
    CLASS(cmatrix), INTENT(INOUT) :: cmat
    CLASS(rvector), INTENT(INOUT) :: rvec
    CLASS(cvector1m), INTENT(INOUT) :: cv1m
    CLASS(cvector), INTENT(INOUT) :: cvec

    CLASS(rvector), ALLOCATABLE :: rvec2
    CLASS(cvector1m), ALLOCATABLE :: cv1m2
    CLASS(cvector), ALLOCATABLE :: cvec2
    INTEGER(i4) :: imode
!-------------------------------------------------------------------------------
!   test the real version
!-------------------------------------------------------------------------------
    ALLOCATE(rvec2,SOURCE=rvec)
    CALL rvec%assign_for_testing('index')
    CALL rmat%set_identity
    CALL rmat%elim_inv_int
    CALL rmat%elim_presolve(rvec,rvec2)
    CALL rmat%matvec(rvec2,rvec)
    CALL rmat%elim_postsolve(rvec,rvec2)
    CALL rvec%assign_for_testing('index')
    CALL wrtrue(rvec%test_if_equal(rvec2),"rmat%static_condensation")
!-------------------------------------------------------------------------------
!   test the complex version -- single-mode interface
!-------------------------------------------------------------------------------
    ALLOCATE(cv1m2,SOURCE=cv1m)
    CALL cv1m%assign_for_testing('index')
    CALL cmat%set_identity
    CALL cmat%elim_inv_int
    CALL cmat%elim_presolve(cv1m,cv1m2)
    CALL cmat%matvec(cv1m2,cv1m)
    CALL cmat%elim_postsolve(cv1m,cv1m2)
    CALL cv1m%assign_for_testing('index')
    CALL wrtrue(cv1m%test_if_equal(cv1m2),"cmat%static_condensation 1 mode")
!-------------------------------------------------------------------------------
!   test the complex version -- multi-mode interface
!-------------------------------------------------------------------------------
    ALLOCATE(cvec2,SOURCE=cvec)
    CALL cvec%assign_for_testing('index')
    CALL cmat%set_identity
    CALL cmat%elim_inv_int
    DO imode=1,cvec%nmodes
      CALL cmat%elim_presolve(cvec,cvec2,imode)
      CALL cmat%matvec(cvec2,cvec,imode,imode)
      CALL cmat%elim_postsolve(cvec,cvec2,imode)
    ENDDO
    CALL cvec%assign_for_testing('index')
    CALL wrtrue(cvec%test_if_equal(cvec2),"cmat%static_condensation 1 mode")
  END SUBROUTINE test_static_condensation

!-------------------------------------------------------------------------------
!> test the assembly routines, the result of this test should be the number of
! elements that contribute to each DOF. There is no good to way to confirm
! this result in general.
!-------------------------------------------------------------------------------
  SUBROUTINE test_assemble(rmat,cmat,rvec,cv1m)
    USE matrix_mod
    USE nimtest_utils
    USE vector_mod
    IMPLICIT NONE

    CLASS(rmatrix), INTENT(INOUT) :: rmat
    CLASS(cmatrix), INTENT(INOUT) :: cmat
    CLASS(rvector), INTENT(INOUT) :: rvec
    CLASS(cvector1m), INTENT(INOUT) :: cv1m

    CLASS(rvector), ALLOCATABLE :: rvec_out
    CLASS(cvector1m), ALLOCATABLE :: cv1m_out
    REAL(r8), ALLOCATABLE :: rintegrand(:,:,:,:,:)
    REAL(r8) :: norm(rvec%inf_norm_size)
    COMPLEX(r8), ALLOCATABLE :: cintegrand(:,:,:,:,:)
    CHARACTER(512) :: msg
!-------------------------------------------------------------------------------
!   test the real version
!-------------------------------------------------------------------------------
    ALLOCATE(rintegrand(rmat%nqty,rmat%nqty,rmat%nel,rmat%u_ndof,rmat%u_ndof))
    !$acc enter data create(rintegrand) async(rmat%id) if(rmat%on_gpu)
    !$acc kernels present(rintegrand) async(rmat%id) if(rmat%on_gpu)
    rintegrand=1._r8
    !$acc end kernels
    CALL rmat%zero
    CALL rmat%assemble(rintegrand)
    !$acc exit data delete(rintegrand) finalize async(rmat%id) if(rmat%on_gpu)
    DEALLOCATE(rintegrand)
!-------------------------------------------------------------------------------
!   calculate the infnorm to trigger potential valgrind errors
!-------------------------------------------------------------------------------
    CALL rvec%alloc_with_mold(rvec_out)
    CALL rvec%assign_for_testing('index')
    CALL rmat%matvec(rvec,rvec_out)
    CALL rvec_out%inf_norm(norm)
    !$acc wait if(rvec_out%on_gpu)
    WRITE(msg,'(a,es10.3)') 'inf_norm=',MAXVAL(norm)
    CALL par%nim_write(msg)
    CALL rvec_out%dealloc
!-------------------------------------------------------------------------------
!   test the complex version
!-------------------------------------------------------------------------------
    ALLOCATE(cintegrand(cmat%nqty,cmat%nqty,cmat%nel,cmat%u_ndof,cmat%u_ndof))
    !$acc enter data create(cintegrand) async(cmat%id) if(rmat%on_gpu)
    !$acc kernels present(cintegrand) async(cmat%id) if(rmat%on_gpu)
    cintegrand=1._r8
    !$acc end kernels
    CALL cmat%zero
    CALL cmat%assemble(cintegrand)
    !$acc exit data delete(cintegrand) finalize async(cmat%id) if(rmat%on_gpu)
    DEALLOCATE(cintegrand)
!-------------------------------------------------------------------------------
!   calculate the infnorm to trigger potential valgrind errors
!-------------------------------------------------------------------------------
    CALL cv1m%alloc_with_mold(cv1m_out)
    CALL cv1m%assign_for_testing('index')
    CALL cmat%matvec(cv1m,cv1m_out)
    CALL cv1m_out%inf_norm(norm)
    !$acc wait if(cv1m_out%on_gpu)
    WRITE(msg,'(a,es10.3)') 'inf_norm=',MAXVAL(norm)
    CALL par%nim_write(msg)
    CALL cv1m_out%dealloc
  END SUBROUTINE test_assemble

!-------------------------------------------------------------------------------
!> Test the boundary and regularity conditions
!-------------------------------------------------------------------------------
  SUBROUTINE test_bc_regularity(rmat,cmat,rvec,cv1m,edge)
    USE edge_mod
    USE matrix_mod
    USE nimtest_utils
    USE vector_mod
    IMPLICIT NONE

    CLASS(rmatrix), INTENT(INOUT) :: rmat
    CLASS(cmatrix), INTENT(INOUT) :: cmat
    CLASS(rvector), INTENT(INOUT) :: rvec
    CLASS(cvector1m), INTENT(INOUT) :: cv1m
    TYPE(edge_type), INTENT(INOUT) :: edge

    CLASS(rvector), ALLOCATABLE :: rvec_out
    CLASS(cvector1m), ALLOCATABLE :: cv1m_out
    REAL(r8) :: norm(rvec%inf_norm_size)
    INTEGER(i4) :: iq
    CHARACTER(64) :: bcfl
    CHARACTER(512) :: msg
!-------------------------------------------------------------------------------
!   test the real version
!-------------------------------------------------------------------------------
    CALL rmat%set_identity
    CALL rmat%find_diag_scale
    CALL wrequal(1._r8,rmat%diag_scale,"rmat%find_diag_scale")
    bcfl=" "
    IF (rmat%nqty<3) THEN
      DO iq=1,rmat%nqty
        bcfl(3*(iq-1)+1:3*iq)="sd "
      ENDDO
    ELSE
      bcfl(1:4)="3vn "
      DO iq=4,rmat%nqty
        bcfl(3*(iq-4)+5:3*(iq-3)+4)="sd "
      ENDDO
    ENDIF
    CALL rmat%dirichlet_bc(bcfl,edge)
    CALL rmat%set_identity
    bcfl=" "
    IF (rmat%nqty<3) THEN
      DO iq=1,rmat%nqty
        bcfl(3*(iq-1)+1:3*iq)="sf "
      ENDDO
    ELSE
      bcfl(1:4)="3vt "
      DO iq=4,rmat%nqty
        bcfl(3*(iq-4)+5:3*(iq-3)+4)="sf "
      ENDDO
    ENDIF
    CALL rmat%dirichlet_bc(bcfl,edge)
    CALL rmat%regularity(edge)
!-------------------------------------------------------------------------------
!   calculate the infnorm to trigger potential valgrind errors
!-------------------------------------------------------------------------------
    CALL rvec%alloc_with_mold(rvec_out)
    CALL rvec%assign_for_testing('index')
    CALL rmat%matvec(rvec,rvec_out)
    CALL rvec_out%inf_norm(norm)
    !$acc wait if(rvec_out%on_gpu)
    WRITE(msg,'(a,es10.3)') 'inf_norm=',MAXVAL(norm)
    CALL par%nim_write(msg)
    CALL rvec_out%dealloc
!-------------------------------------------------------------------------------
!   test the complex version
!-------------------------------------------------------------------------------
    CALL cmat%set_identity
    CALL cmat%find_diag_scale
    CALL wrequal(1._r8,cmat%diag_scale,"cmat%find_diag_scale")
    bcfl=" "
    IF (cmat%nqty<3) THEN
      DO iq=1,cmat%nqty
        bcfl(3*(iq-1)+1:3*iq)="sd "
      ENDDO
    ELSE
      bcfl(1:4)="3vn "
      DO iq=4,cmat%nqty
        bcfl(3*(iq-4)+5:3*(iq-3)+4)="sd "
      ENDDO
    ENDIF
    CALL cmat%dirichlet_bc(bcfl,edge)
    CALL cmat%set_identity
    bcfl=" "
    IF (cmat%nqty<3) THEN
      DO iq=1,cmat%nqty
        bcfl(3*(iq-1)+1:3*iq)="sf "
      ENDDO
    ELSE
      bcfl(1:4)="3vt "
      DO iq=4,cmat%nqty
        bcfl(3*(iq-4)+5:3*(iq-3)+4)="sf "
      ENDDO
    ENDIF
    CALL cmat%dirichlet_bc(bcfl,edge)
    CALL cmat%regularity(edge)
!-------------------------------------------------------------------------------
!   calculate the infnorm to trigger potential valgrind errors
!-------------------------------------------------------------------------------
    CALL cv1m%alloc_with_mold(cv1m_out)
    CALL cv1m%assign_for_testing('index')
    CALL cmat%matvec(cv1m,cv1m_out)
    CALL cv1m_out%inf_norm(norm)
    !$acc wait if(cv1m_out%on_gpu)
    WRITE(msg,'(a,es10.3)') 'inf_norm=',MAXVAL(norm)
    CALL par%nim_write(msg)
    CALL cv1m_out%dealloc
  END SUBROUTINE test_bc_regularity

!-------------------------------------------------------------------------------
!> Test the infrastructure for Jacobi preconditioning.
!  Construct the identity matrix, extract it as a vector, multiply 2,
!  make a copy and invert the copy, use apply_diag_matvec between the original
!  and the copy and check the result.
!-------------------------------------------------------------------------------
  SUBROUTINE test_jacobi_infrastructure(rmat,cmat,rvec,cv1m,cvec,edge)
    USE edge_mod
    USE io
    USE matrix_mod
    USE nimtest_utils
    USE vector_mod
    IMPLICIT NONE

    CLASS(rmatrix), INTENT(INOUT) :: rmat
    CLASS(cmatrix), INTENT(INOUT) :: cmat
    CLASS(rvector), INTENT(INOUT) :: rvec
    CLASS(cvector1m), INTENT(INOUT) :: cv1m
    CLASS(cvector), INTENT(INOUT) :: cvec
    TYPE(edge_type), INTENT(INOUT) :: edge

    CLASS(rvector), ALLOCATABLE :: rvec_inv,rvec_matmul
    CLASS(cvector1m), ALLOCATABLE :: cv1m_inv,cv1m_matmul
    CLASS(cvector), ALLOCATABLE :: cvec_inv,cvec_matmul
    INTEGER(i4) :: imode
    REAL(r8) :: norm(rvec%inf_norm_size)
!-------------------------------------------------------------------------------
!   test the real version
!-------------------------------------------------------------------------------
    CALL rvec%zero
    CALL rvec%alloc_with_mold(rvec_inv)
    CALL rvec%alloc_with_mold(rvec_matmul)
    CALL rmat%set_identity
    CALL rmat%get_diag_as_vec(rvec)
    CALL rvec%mult(2._r8)
    CALL rvec%inf_norm(norm)
    !$acc wait if(rvec%on_gpu)
    CALL wrequal(2._r8,MAXVAL(norm),"rvec%inf_norm")
    CALL rvec_inv%assign_rvec(rvec)
    CALL rvec_inv%invert
    CALL rvec_inv%inf_norm(norm)
    !$acc wait if(rvec_inv%on_gpu)
    CALL wrequal(0.5_r8,MAXVAL(norm),"rvec_inv%inf_norm")
    CALL rvec%apply_diag_matvec(rvec_inv,rvec_matmul)
    CALL rvec_matmul%inf_norm(norm)
    !$acc wait if(rvec_matmul%on_gpu)
    CALL wrequal(1._r8,MAXVAL(norm),"rvec_matmul%inf_norm")
    CALL rvec_inv%dealloc
    CALL rvec_matmul%dealloc
!-------------------------------------------------------------------------------
!   test the complex version -- single-mode interface
!-------------------------------------------------------------------------------
    CALL cv1m%zero
    CALL cv1m%alloc_with_mold(cv1m_inv)
    CALL cv1m%alloc_with_mold(cv1m_matmul)
    CALL cmat%set_identity
    CALL cmat%get_diag_as_vec(cv1m)
    CALL cv1m%mult(2._r8)
    CALL cv1m%inf_norm(norm)
    !$acc wait if(cv1m%on_gpu)
    CALL wrequal(2._r8,MAXVAL(norm),"cv1m%inf_norm")
    CALL cv1m_inv%assign_cv1m(cv1m)
    CALL cv1m_inv%invert
    CALL cv1m_inv%inf_norm(norm)
    !$acc wait if(cv1m_inv%on_gpu)
    CALL wrequal(0.5_r8,MAXVAL(norm),"cv1m_inv%inf_norm")
    CALL cv1m%apply_diag_matvec(cv1m_inv,cv1m_matmul)
    CALL cv1m_matmul%inf_norm(norm)
    !$acc wait if(cv1m_matmul%on_gpu)
    CALL wrequal(1._r8,MAXVAL(norm),"cv1m_matmul%inf_norm")
    CALL cv1m_inv%dealloc
    CALL cv1m_matmul%dealloc
!-------------------------------------------------------------------------------
!   test the complex version -- multi-mode interface
!-------------------------------------------------------------------------------
    CALL cvec%zero
    CALL cvec%alloc_with_mold(cvec_inv)
    CALL cvec%alloc_with_mold(cvec_matmul)
    CALL cmat%set_identity
    DO imode=1,cvec%nmodes
      CALL cmat%get_diag_as_vec(cvec,imode)
    ENDDO
    CALL cvec%mult(2._r8)
    CALL cvec%inf_norm(norm)
    !$acc wait if(cvec%on_gpu)
    CALL wrequal(2._r8,MAXVAL(norm),"cvec%inf_norm")
    CALL cvec_inv%assign_cvec(cvec)
    CALL cvec_inv%invert
    CALL cvec_inv%inf_norm(norm)
    !$acc wait if(cv1m_inv%on_gpu)
    CALL wrequal(0.5_r8,MAXVAL(norm),"cvec_inv%inf_norm")
    CALL cvec%apply_diag_matvec(cvec_inv,cvec_matmul)
    CALL cvec_matmul%inf_norm(norm)
    !$acc wait if(cv1m_matmul%on_gpu)
    CALL wrequal(1._r8,MAXVAL(norm),"cvec_matmul%inf_norm")
    CALL cvec_inv%dealloc
    CALL cvec_matmul%dealloc
  END SUBROUTINE test_jacobi_infrastructure

END PROGRAM test_matrix
