!-------------------------------------------------------------------------------
!! test FEM to linalg allocate and transfer routines driver
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!* test FEM to linalg allocate and transfer routines driver
!-------------------------------------------------------------------------------
PROGRAM test_fem_linalg_xfer
  USE fem_registry_mod
  USE io
  USE local
  USE nodal_mod
  USE pardata_mod
  USE test_fem_linalg_xfer_comp_mod
  USE test_fem_linalg_xfer_cv1m_mod
  USE test_fem_linalg_xfer_real_mod
  USE timer_mod
  IMPLICIT NONE

  CLASS(nodal_field_comp), ALLOCATABLE :: cfield
  CLASS(nodal_field_real), ALLOCATABLE :: rfield
  CLASS(nodal_field_real), ALLOCATABLE :: coord_field
  INTEGER(i4) :: nargs,itest
  CHARACTER(64) :: test_name,test_type
  INTEGER(i4) :: idepth
  INTEGER(i4), SAVE :: iftn=-1

!-------------------------------------------------------------------------------
! determine which test to run from the command line
!-------------------------------------------------------------------------------
  nargs=command_argument_count()
  IF (nargs /= 2) THEN
    CALL print_usage
    CALL par%nim_stop('Argument error')
  ENDIF
  CALL get_command_argument(1,test_type)
  CALL get_command_argument(2,test_name)
  CALL par%init
  CALL fch5_init
  CALL timer%init
  CALL timer%start_timer_l0('test','test_fem_linalg_xfer',iftn,idepth)
!-------------------------------------------------------------------------------
! setup registry based on test type
!-------------------------------------------------------------------------------
  CALL set_field_test_type(test_type)
!-------------------------------------------------------------------------------
! loop over test parameters
!-------------------------------------------------------------------------------
  DO itest=1,ntests
    CALL setup_fem(test_type,cfield,rfield,coord_field,itest)
    IF (comp_test) THEN
      CALL run_test_comp(cfield,coord_field,test_name)
      CALL run_test_cv1m(cfield,coord_field,test_name)
    ELSE
      CALL run_test_real(rfield,coord_field,test_name)
    ENDIF
    CALL teardown_fem(cfield,rfield,coord_field)
  ENDDO
  CALL timer%end_timer_l0(iftn,idepth)
  CALL timer%report
  CALL timer%finalize
  CALL fch5_dealloc
  CALL par%nim_stop('Normal termination.',clean_shutdown=.TRUE.)
CONTAINS

!-------------------------------------------------------------------------------
!* helper routine to print usage
!-------------------------------------------------------------------------------
  SUBROUTINE print_usage
    CALL par%nim_write('Usage: <test program> <test_type> <test name>')
    CALL par%nim_write('  where <test name> is one of')
    CALL par%nim_write('    alloc_xfer')
    CALL par%nim_write('    alloc_mat')
    CALL par%nim_write('    integrate')
    CALL print_types
    CALL par%nim_stop('check input')
  END SUBROUTINE print_usage

END PROGRAM test_fem_linalg_xfer
