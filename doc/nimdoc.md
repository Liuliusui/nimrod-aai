src_dir: ../example
         ../nimblk
         ../nimfem
         ../nimgrid
         ../nimiter
         ../nimlib
         ../nimlinalg
         ../nimloc
         ../nimseam
         preproc
exclude: **/vec_rect_2D.f
         **/vec_rect_2D_real.f
         **/vec_rect_2D_cv1m.f
         **/vec_rect_2D_comp.f
         **/vec_rect_2D_acc.f
         **/vec_rect_2D_real_acc.f
         **/vec_rect_2D_cv1m_acc.f
         **/vec_rect_2D_comp_acc.f
         **/pardata.old_mpi.f
exclude_dir: ../nimblk/tests
             ../nimfem/tests
             ../nimiter/tests
             ../nimlib/tests
             ../nimlinalg/tests
             ../nimseam/tests
             pages/uploads
output_dir: ../public
extensions: f
            fpp
fixed_extensions: 
project: NIMROD-abstract-accelerated-infrastructure
author: the NIMROD team
summary: NonIdeal Magnetohydrodynmics with Rotation Open Discussion Abstract Accelerated Infrastructure
docmark: !
predocmark: *
docmark_alt: <
predocmark_alt: >
display: public
source: true
graph: true 
search: true
warn: false
print_creation_date: true
creation_date: %Y-%m-%d %H:%M %z
favicon: ./disruption.png
version: (main)
coloured_edges: true
page_dir: pages_doc
include: .

## Browse the [GitLab source](https://gitlab.com/NIMRODteam/open/nimrod-aai/)
