!-------------------------------------------------------------------------------
!! define grid input
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* module that defines grid input
!-------------------------------------------------------------------------------
MODULE grid_input_mod
  USE local
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: grid_in

!-------------------------------------------------------------------------------
!: namelist grid_input
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!> The type of grid is set by `gridshape`.
!
! `gridshape='rect'`: rectangular cross section. Dimensions are set by `xmin`,
! `xmax`, `ymin` and `ymax`.
!
! `gridshape='circ'`: circular or annular cross section. Dimensions and offset
! are set by `xmin`, `xmax`, `xo` and `yo`. When not an annulus an unstructured
! butterfly mesh is used.
!-------------------------------------------------------------------------------
  CHARACTER(8) :: gridshape="rect"
!-------------------------------------------------------------------------------
!> for `gridshape=rect`, sets the periodicity of the x and y directions may be
! specified as 'none', 'y-dir', or 'both'.
!-------------------------------------------------------------------------------
  CHARACTER(8) :: periodicity="none"
!-------------------------------------------------------------------------------
!> minimum x extent (`gridshape='rect'`) or radii (`gridshape='circ'`). For the
! latter if `xmin/=0` then the grid is annular and a logically rectangular grid
! is used.
!-------------------------------------------------------------------------------
  REAL(r8) :: xmin=0
!-------------------------------------------------------------------------------
!* maximum x extent (`gridshape='rect'`) or radii (`gridshape='circ'`).
!-------------------------------------------------------------------------------
  REAL(r8) :: xmax=1
!-------------------------------------------------------------------------------
!* minimum y extent (`gridshape='rect'`).
!-------------------------------------------------------------------------------
  REAL(r8) :: ymin=0
!-------------------------------------------------------------------------------
!* maximum y extent (`gridshape='rect'`).
!-------------------------------------------------------------------------------
  REAL(r8) :: ymax=1
!-------------------------------------------------------------------------------
!* x-offset in the radial, R, coordinate when `gridshape='circ'`.
!-------------------------------------------------------------------------------
  REAL(r8) :: xo=1
!-------------------------------------------------------------------------------
!* y-offset in the axial, Z, coordinate when `gridshape='circ'`.
!-------------------------------------------------------------------------------
  REAL(r8) :: yo=0
!-------------------------------------------------------------------------------
!> Selects a periodic linear system when `torgeom=F` and a toroidal periodic
! system when `torgeom=T`.
!-------------------------------------------------------------------------------
  LOGICAL :: torgeom=.TRUE.
!-------------------------------------------------------------------------------
!> When `torgeom=F`, `per_length` is the length of the Fourier, \(\Phi\),
! direction.
!-------------------------------------------------------------------------------
  REAL(r8) :: per_length=1
!-------------------------------------------------------------------------------
!> Set `zperiod` to simulate only a fraction of a periodic device. `zperiod=1`
! simulates the full device and `zperiod=n` simulates 1/n of the device. This
! factor just multiplies the wavenumber array keff.
!-------------------------------------------------------------------------------
  INTEGER(i4) :: zperiod=1
!-------------------------------------------------------------------------------
!> The following parameters control the mesh resolution. The two parameters `mx`
! and `my` set the number of cells in the central, logically rectangular part of
! the grid.  For gridshape=rect grids, mx and my are the number of cells across
! the x and y directions.
!-------------------------------------------------------------------------------
  INTEGER(i4) :: mx=32
!-------------------------------------------------------------------------------
!* See `mx`.
!-------------------------------------------------------------------------------
  INTEGER(i4) :: my=32
!-------------------------------------------------------------------------------
!> For non-annular circular butterfly grids, `mx` and `my` are the number of
! cells in the core rectangular blocks, while the polar region is gridded by
! rectangles with `mr` radial elements. The number of azimuthal elements is
! constrained to be `mtheta=2*(mx+my)`.
!-------------------------------------------------------------------------------
  INTEGER(i4) :: mr=32
!-------------------------------------------------------------------------------
!> `poly_degree` is the degree of the polynomials used in the Lagrange elements
! for dependent variables. The amount of information for a rectangular grid is
! then given by `(mx*poly_degree)*(my*poly_degree)`.
!-------------------------------------------------------------------------------
  INTEGER(i4) :: poly_degree=3
!-------------------------------------------------------------------------------
!> The block decomposition of the region of rectangular cells is controlled by
! `nxbl` and `nybl` [the number of rblocks in the x and y (or radial and
! poloidal) directions, respectively].
!-------------------------------------------------------------------------------
  INTEGER(i4) :: nxbl=1
!-------------------------------------------------------------------------------
!* See `nxbl`.
!-------------------------------------------------------------------------------
  INTEGER(i4) :: nybl=1
!-------------------------------------------------------------------------------
!> For circular butterfly grids the radial and azimuthal elements set by `mr`
! and `mtheta=2*(mx+my)`, are decomposed into `nrbl` by `ntbl` blocks in the
! radial and azimuthal directions, respectively.
!-------------------------------------------------------------------------------
  INTEGER(i4) :: nrbl=1
!-------------------------------------------------------------------------------
!* See `nrbl`.
!-------------------------------------------------------------------------------
  INTEGER(i4) :: ntbl=1
!-------------------------------------------------------------------------------
!: computed
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* type for grid input parameter namespacing
!-------------------------------------------------------------------------------
  TYPE :: grid_input
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST TYPE
!-------------------------------------------------------------------------------
    CHARACTER(8) :: gridshape
    CHARACTER(8) :: periodicity
    REAL(r8) :: xmin
    REAL(r8) :: xmax
    REAL(r8) :: ymin
    REAL(r8) :: ymax
    REAL(r8) :: xo
    REAL(r8) :: yo
    LOGICAL :: torgeom
    REAL(r8) :: per_length
    INTEGER(i4) :: zperiod
    INTEGER(i4) :: mx
    INTEGER(i4) :: my
    INTEGER(i4) :: mr
    INTEGER(i4) :: poly_degree
    INTEGER(i4) :: nxbl
    INTEGER(i4) :: nybl
    INTEGER(i4) :: nrbl
    INTEGER(i4) :: ntbl
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST TYPE
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   total number of blocks
!-------------------------------------------------------------------------------
    INTEGER(i4) :: nbl_total=1
!-------------------------------------------------------------------------------
!   true with an annular grid
!-------------------------------------------------------------------------------
    LOGICAL :: annulus=.FALSE.
  CONTAINS

    PROCEDURE, PASS(gridin) :: compute => grid_compute
    PROCEDURE, PASS(gridin) :: read_namelist => grid_read_namelist
    PROCEDURE, PASS(gridin) :: copy_namelist_to_type                            &
                               => grid_copy_namelist_to_type
    PROCEDURE, PASS(gridin) :: h5read => grid_h5read
    PROCEDURE, PASS(gridin) :: h5write => grid_h5write
    PROCEDURE, PASS(gridin) :: bcast_input => grid_bcast_input
  END TYPE grid_input

  !* grid input singleton type
  TYPE(grid_input) :: grid_in
CONTAINS

!-------------------------------------------------------------------------------
!> compute secondary parameters and check input
!-------------------------------------------------------------------------------
  SUBROUTINE grid_compute(gridin)
    USE pardata_mod
    IMPLICIT NONE

    !> grid_input type
    CLASS(grid_input), INTENT(INOUT) :: gridin

!-------------------------------------------------------------------------------
!   initialize annulus
!-------------------------------------------------------------------------------
    IF (gridin%gridshape=="rect") THEN
      IF (gridin%torgeom.AND. gridin%xmin/=0) gridin%annulus=.TRUE.
    ELSE ! circ
      IF (gridin%xmin/=0) gridin%annulus=.TRUE.
      gridin%ymin=0._r8
      gridin%ymax=twopi ! theta for transform
    ENDIF
!-------------------------------------------------------------------------------
!   initialize nbl_total
!-------------------------------------------------------------------------------
    gridin%nbl_total=gridin%nxbl*gridin%nybl
    IF (gridin%gridshape=="circ".AND..NOT.gridin%annulus)                       &
      gridin%nbl_total=gridin%nbl_total+gridin%nrbl*gridin%ntbl
  END SUBROUTINE grid_compute

!-------------------------------------------------------------------------------
!> open and read the namelist input.
!-------------------------------------------------------------------------------
  SUBROUTINE grid_read_namelist(gridin,infile)
    USE local
    USE read_namelist_mod
    IMPLICIT NONE

    !> grid_input type
    CLASS(grid_input), TARGET, INTENT(INOUT) :: gridin
    !> input file name
    CHARACTER(*), INTENT(IN) :: infile

    INTEGER(i4) :: read_stat
    CHARACTER(64) :: tempfile
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST BLOCK
!-------------------------------------------------------------------------------
    NAMELIST/grid_input/gridshape,periodicity,xmin,xmax,ymin,ymax,xo,yo,torgeom,&
          per_length,zperiod,mx,my,mr,poly_degree,nxbl,nybl,nrbl,ntbl
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST BLOCK
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   Open input file. Remove comments from input file and put into temporary
!   file.
!-------------------------------------------------------------------------------
    tempfile='temp'//ADJUSTL(infile)
    CALL rmcomment(infile,tempfile)
    OPEN(UNIT=in_unit,FILE=tempfile,STATUS='OLD',POSITION='REWIND')
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST FILE READ
!-------------------------------------------------------------------------------
    CALL position_at_namelist('grid_input', read_stat)
    IF (read_stat==0) THEN
      READ(UNIT=in_unit,NML=grid_input,IOSTAT=read_stat)
      IF (read_stat/=0) THEN
        CALL print_namelist_error('grid_input')
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST FILE READ
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   close and delete temporary input file.
!-------------------------------------------------------------------------------
    CLOSE(in_unit,STATUS='DELETE')
!-------------------------------------------------------------------------------
!   compute secondary parameters.
!-------------------------------------------------------------------------------
    CALL gridin%copy_namelist_to_type
    CALL gridin%compute
  END SUBROUTINE grid_read_namelist

!-------------------------------------------------------------------------------
!> copy the namelist values into the namespaced type
!-------------------------------------------------------------------------------
  SUBROUTINE grid_copy_namelist_to_type(gridin)
    USE io
    IMPLICIT NONE

    !> grid_input type
    CLASS(grid_input), INTENT(INOUT) :: gridin
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST COPY
!-------------------------------------------------------------------------------
    gridin%gridshape=gridshape
    gridin%periodicity=periodicity
    gridin%xmin=xmin
    gridin%xmax=xmax
    gridin%ymin=ymin
    gridin%ymax=ymax
    gridin%xo=xo
    gridin%yo=yo
    gridin%torgeom=torgeom
    gridin%per_length=per_length
    gridin%zperiod=zperiod
    gridin%mx=mx
    gridin%my=my
    gridin%mr=mr
    gridin%poly_degree=poly_degree
    gridin%nxbl=nxbl
    gridin%nybl=nybl
    gridin%nrbl=nrbl
    gridin%ntbl=ntbl
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST COPY
!-------------------------------------------------------------------------------
  END SUBROUTINE grid_copy_namelist_to_type

!-------------------------------------------------------------------------------
!> read h5 namelist from dump file. the h5 file should be opened outside of this
! routine
!-------------------------------------------------------------------------------
  SUBROUTINE grid_h5read(gridin)
    USE io
    IMPLICIT NONE

    !> grid_input type
    CLASS(grid_input), INTENT(INOUT) :: gridin

    INTEGER(HID_T) :: gid

    CALL open_group(rootgid,"grid_input",gid,h5err)
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST H5 READ
!-------------------------------------------------------------------------------
    CALL read_attribute(gid,"gridin%gridshape",gridin%gridshape,h5in,h5err)
    CALL read_attribute(gid,"gridin%periodicity",gridin%periodicity,h5in,h5err)
    CALL read_attribute(gid,"gridin%xmin",gridin%xmin,h5in,h5err)
    CALL read_attribute(gid,"gridin%xmax",gridin%xmax,h5in,h5err)
    CALL read_attribute(gid,"gridin%ymin",gridin%ymin,h5in,h5err)
    CALL read_attribute(gid,"gridin%ymax",gridin%ymax,h5in,h5err)
    CALL read_attribute(gid,"gridin%xo",gridin%xo,h5in,h5err)
    CALL read_attribute(gid,"gridin%yo",gridin%yo,h5in,h5err)
    CALL read_attribute(gid,"gridin%torgeom",gridin%torgeom,h5in,h5err)
    CALL read_attribute(gid,"gridin%per_length",gridin%per_length,h5in,h5err)
    CALL read_attribute(gid,"gridin%zperiod",gridin%zperiod,h5in,h5err)
    CALL read_attribute(gid,"gridin%mx",gridin%mx,h5in,h5err)
    CALL read_attribute(gid,"gridin%my",gridin%my,h5in,h5err)
    CALL read_attribute(gid,"gridin%mr",gridin%mr,h5in,h5err)
    CALL read_attribute(gid,"gridin%poly_degree",gridin%poly_degree,h5in,h5err)
    CALL read_attribute(gid,"gridin%nxbl",gridin%nxbl,h5in,h5err)
    CALL read_attribute(gid,"gridin%nybl",gridin%nybl,h5in,h5err)
    CALL read_attribute(gid,"gridin%nrbl",gridin%nrbl,h5in,h5err)
    CALL read_attribute(gid,"gridin%ntbl",gridin%ntbl,h5in,h5err)
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST H5 READ
!-------------------------------------------------------------------------------
    CALL close_group("grid_input",gid,h5err)
    CALL gridin%compute
  END SUBROUTINE grid_h5read

!-------------------------------------------------------------------------------
!> write h5 namelist to dump file. the h5 file should be opened outside of this
! routine
!-------------------------------------------------------------------------------
  SUBROUTINE grid_h5write(gridin)
    USE io
    IMPLICIT NONE

    !> grid_input type
    CLASS(grid_input), INTENT(INOUT) :: gridin

    INTEGER(HID_T) :: gid

    CALL make_group(rootgid,"grid_input",gid,h5in,h5err)
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST H5 WRITE
!-------------------------------------------------------------------------------
    CALL write_attribute(gid,"gridin%gridshape",gridin%gridshape,h5in,h5err)
    CALL write_attribute(gid,"gridin%periodicity",gridin%periodicity,h5in,h5err)
    CALL write_attribute(gid,"gridin%xmin",gridin%xmin,h5in,h5err)
    CALL write_attribute(gid,"gridin%xmax",gridin%xmax,h5in,h5err)
    CALL write_attribute(gid,"gridin%ymin",gridin%ymin,h5in,h5err)
    CALL write_attribute(gid,"gridin%ymax",gridin%ymax,h5in,h5err)
    CALL write_attribute(gid,"gridin%xo",gridin%xo,h5in,h5err)
    CALL write_attribute(gid,"gridin%yo",gridin%yo,h5in,h5err)
    CALL write_attribute(gid,"gridin%torgeom",gridin%torgeom,h5in,h5err)
    CALL write_attribute(gid,"gridin%per_length",gridin%per_length,h5in,h5err)
    CALL write_attribute(gid,"gridin%zperiod",gridin%zperiod,h5in,h5err)
    CALL write_attribute(gid,"gridin%mx",gridin%mx,h5in,h5err)
    CALL write_attribute(gid,"gridin%my",gridin%my,h5in,h5err)
    CALL write_attribute(gid,"gridin%mr",gridin%mr,h5in,h5err)
    CALL write_attribute(gid,"gridin%poly_degree",gridin%poly_degree,h5in,h5err)
    CALL write_attribute(gid,"gridin%nxbl",gridin%nxbl,h5in,h5err)
    CALL write_attribute(gid,"gridin%nybl",gridin%nybl,h5in,h5err)
    CALL write_attribute(gid,"gridin%nrbl",gridin%nrbl,h5in,h5err)
    CALL write_attribute(gid,"gridin%ntbl",gridin%ntbl,h5in,h5err)
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST H5 WRITE
!-------------------------------------------------------------------------------
    CALL close_group("grid_input",gid,h5err)
  END SUBROUTINE grid_h5write

!-------------------------------------------------------------------------------
!> broadcast info read by proc 0 out of nimrod.in (namelist reads) to all
! processors. every quantity in input module is broadcast in case it was read.
!-------------------------------------------------------------------------------
  SUBROUTINE grid_bcast_input(gridin)
    USE pardata_mod
    IMPLICIT NONE

    !> grid_input type
    CLASS(grid_input), INTENT(INOUT) :: gridin

    INTEGER(i4) :: nn
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST BCAST
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   grid_input
!-------------------------------------------------------------------------------
    nn=LEN(gridin%gridshape)
    CALL par%all_bcast(gridin%gridshape,nn,0)
    nn=LEN(gridin%periodicity)
    CALL par%all_bcast(gridin%periodicity,nn,0)
    CALL par%all_bcast(gridin%xmin,0)
    CALL par%all_bcast(gridin%xmax,0)
    CALL par%all_bcast(gridin%ymin,0)
    CALL par%all_bcast(gridin%ymax,0)
    CALL par%all_bcast(gridin%xo,0)
    CALL par%all_bcast(gridin%yo,0)
    CALL par%all_bcast(gridin%torgeom,0)
    CALL par%all_bcast(gridin%per_length,0)
    CALL par%all_bcast(gridin%zperiod,0)
    CALL par%all_bcast(gridin%mx,0)
    CALL par%all_bcast(gridin%my,0)
    CALL par%all_bcast(gridin%mr,0)
    CALL par%all_bcast(gridin%poly_degree,0)
    CALL par%all_bcast(gridin%nxbl,0)
    CALL par%all_bcast(gridin%nybl,0)
    CALL par%all_bcast(gridin%nrbl,0)
    CALL par%all_bcast(gridin%ntbl,0)
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST BCAST
!-------------------------------------------------------------------------------
    CALL gridin%compute
  END SUBROUTINE grid_bcast_input

END MODULE grid_input_mod
