!-------------------------------------------------------------------------------
!< module containing shared routines that performs standard operations for
!  boundary computations.
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!> module containing shared routines that performs standard operations for
!  boundary computations.
!-------------------------------------------------------------------------------
MODULE boundary_ftns_mod
  USE local
  USE timer_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: bcflag_parse,bcdir_set

  INTEGER(i4), PARAMETER :: max3v=32_i4
  INTEGER(i4), DIMENSION(max3v) :: start3v
  CHARACTER(*), PARAMETER :: mod_name='boundary_ftns'
CONTAINS

!-------------------------------------------------------------------------------
!> this routine parses the component flag that is sent into one of
! the dirichlet_ routines.  input character strings are expected
! to have a set of substrings separated by spaces, where each
! substring indicates which components of a 3-vector or whether
! a scalar needs to be set to zero.  valid substrings and their
! meanings are:
!
!   "3vn" - zero-out the normal component of a 3-vector
!   "3vt" - zero-out the tangential components of a 3-vector
!   "3vf" - 3-vector without a Dichlet condition is applied (free)
!   "sd"  - zero-out a scalar
!   "sf"  - scalar without a Dirichlet condition
!
! the output is the two-dimensional integer array bciarr that codes
! which components have Dirichlet conditions.  bciarr(:,1) is the
! coding for scalars, and bciarr(:,2) is the coding for vectors.
! bciarr(:,1) is set as:
!
!   0 = do not alter (scalar or vector quantity index)
!   1 = set this scalar to zero
!
! bciarr(:,2) has zero for each scalar quantity, and for each
! 3-vector, it can have the following sequences:
!
!   000 = do not alter
!   100 = set the normal component to zero
!   011 = set all tangential components to zero
!   010 = set the in-plane tangential components to zero (not used)
!   001 = set the the out-of plane (third) component to zero (ditto)
!
! this routine also sets the module array start3v, which is a list
! of starting indices for all 3-vectors within bciarr(:,2).
!-------------------------------------------------------------------------------
  SUBROUTINE bcflag_parse(bcfl,nqty,bciarr,diag)
    USE pardata_mod
    IMPLICIT NONE

    CHARACTER(*), INTENT(IN) :: bcfl
    INTEGER(i4), INTENT(IN) :: nqty
    INTEGER(i4), DIMENSION(nqty,2), INTENT(OUT) :: bciarr
    LOGICAL, INTENT(OUT), OPTIONAL :: diag

    INTEGER(i4) :: ivec,ichar,lenstr,iind
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'bcflag_parse',iftn,idepth)
!-------------------------------------------------------------------------------
!   intialize local and output variables.
!-------------------------------------------------------------------------------
    lenstr=LEN_TRIM(bcfl)
    ichar=0
    iind=0
    ivec=0
    bciarr=0
    start3v=0
!-------------------------------------------------------------------------------
!   set the diag logical for calls from routines for matrices.  the
!   choice is based on the last characters of bcfl.
!-------------------------------------------------------------------------------
    IF (PRESENT(diag)) THEN
      diag=.true.
      IF (lenstr>6) THEN
        IF (bcfl(lenstr-6:lenstr)=="offdiag") THEN
          diag=.false.
          lenstr=lenstr-7
        ENDIF
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   if bcflag indicates that all components have Dirichlet conditions,
!   treat all system components as scalars.
!-------------------------------------------------------------------------------
    IF (lenstr>=3) THEN
      IF (bcfl(1:3)=="all") THEN
        bciarr(:,1)=1_i4
        RETURN
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   determine the number of characters in bcfl and start a loop for
!   parsing individual characters.
!-------------------------------------------------------------------------------
    char_loop: DO
      ichar=ichar+1
      SELECT CASE (bcfl(ichar:ichar))
      CASE (" ")
        CYCLE char_loop
      CASE ("s")
        ichar=ichar+1
        iind=iind+1
        IF (bcfl(ichar:ichar)=="d") bciarr(iind,1)=1_i4
      CASE ("3")
        ivec=ivec+1
        start3v(ivec)=iind+1
        ichar=ichar+2
        iind=iind+3
        IF (bcfl(ichar:ichar)=="n") THEN
          bciarr(iind-2:iind,2)=[1_i4,0_i4,0_i4]
        ELSE IF (bcfl(ichar:ichar)=="t") THEN
          bciarr(iind-2:iind,2)=[0_i4,1_i4,1_i4]
        ENDIF
      CASE DEFAULT
        CALL par%nim_stop("Bcflag_parse: "//bcfl(ichar:ichar)//                 &
                          " not recognized.")
      END SELECT
      IF (iind==nqty) EXIT char_loop
    ENDDO char_loop
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE bcflag_parse

!-------------------------------------------------------------------------------
!> this routine sets the algebraic projection matrix
! for projecting the components of the system that have essential,
! i.e. Dirichlet conditions.
!-------------------------------------------------------------------------------
  SUBROUTINE bcdir_set(nqty,bciarr,excorner,isymm,norm,tang,bcpmat)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: nqty,isymm
    INTEGER(i4), DIMENSION(nqty,2), INTENT(IN) :: bciarr
    LOGICAL, INTENT(IN) :: excorner
    REAL(r8), DIMENSION(2), INTENT(IN) :: norm,tang
    REAL(r8), DIMENSION(nqty,nqty), INTENT(OUT) :: bcpmat

    INTEGER(i4) :: ivec,ivst,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'bcdir_set',iftn,idepth)
!-------------------------------------------------------------------------------
!   intialize output.
!-------------------------------------------------------------------------------
    bcpmat=0._r8
!-------------------------------------------------------------------------------
!   catch the symmetry conditions for scalar systems.
!-------------------------------------------------------------------------------
    IF (isymm/=0.AND.norm(2)==REAL(isymm,r8).AND.                               &
        MINVAL(bciarr(:,1))==1_i4) RETURN
!-------------------------------------------------------------------------------
!   set the directions for scalars and cases with "all".
!-------------------------------------------------------------------------------
    DO iq=1,nqty
      bcpmat(iq,iq)=bciarr(iq,1)
    ENDDO
!-------------------------------------------------------------------------------
!   set directions for 3-vectors.
!-------------------------------------------------------------------------------
    DO ivec=1,SUM(MIN(start3v,1_i4))
      ivst=start3v(ivec)
      bcpmat(ivst+2,ivst+2)=bciarr(ivst+2,2)
      IF (excorner) THEN
        bcpmat(ivst,ivst)=1._r8
        bcpmat(ivst+1,ivst+1)=1._r8
        CYCLE
      ENDIF
      bcpmat(ivst:ivst+1,ivst)=                                                 &
        norm*norm(1)*bciarr(ivst,2)+tang*tang(1)*bciarr(ivst+1,2)
      bcpmat(ivst:ivst+1,ivst+1)=                                               &
        norm*norm(2)*bciarr(ivst,2)+tang*tang(2)*bciarr(ivst+1,2)
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE bcdir_set

END MODULE boundary_ftns_mod
