######################################################################          
#                                                                               
# CMakeLists.txt for example
#                                                                               
######################################################################

add_executable(exset exset.f exset_input.f init_function.f)
target_link_libraries(exset PRIVATE nimblk nimlinalg nimgrid nimfft)
install(TARGETS exset
    DESTINATION bin/
    PERMISSIONS OWNER_READ OWNER_EXECUTE OWNER_WRITE
    GROUP_READ GROUP_EXECUTE GROUP_WRITE
    WORLD_READ WORLD_EXECUTE)

set(LAPLACE_SOURCES
    laplace_input.f
)

add_library(laplace-obj OBJECT ${LAPLACE_SOURCES})
list(APPEND OBJ_TARGETS $<TARGET_OBJECTS:laplace-obj>)
target_link_libraries(laplace-obj PUBLIC nimblk nimiter nimgrid nimfft)
target_include_directories(laplace-obj PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/>
  $<INSTALL_INTERFACE:include/>
)
add_library(laplace STATIC $<TARGET_OBJECTS:laplace-obj>)
target_link_libraries(laplace PUBLIC laplace-obj)

add_executable(laplace_real laplace_real.f)
target_link_libraries(laplace_real PRIVATE laplace)
install(TARGETS laplace_real
    DESTINATION bin/
    PERMISSIONS OWNER_READ OWNER_EXECUTE OWNER_WRITE
    GROUP_READ GROUP_EXECUTE GROUP_WRITE
    WORLD_READ WORLD_EXECUTE)

add_executable(laplace_comp laplace_comp.f)
target_link_libraries(laplace_comp PRIVATE laplace)
install(TARGETS laplace_comp
    DESTINATION bin/
    PERMISSIONS OWNER_READ OWNER_EXECUTE OWNER_WRITE
    GROUP_READ GROUP_EXECUTE GROUP_WRITE
    WORLD_READ WORLD_EXECUTE)

# Define test cleanup
set(files_test1 "dump.00000.h5 dump.00004.h5")
set(files_test2 "dump.00005.h5 dump.00008.h5")
set(files_test3 "dump.00010.h5 dump.00013.h5")
set(files_test4 "dump.00015.h5 dump.00018.h5")
set(files_test5 "dump.00020.h5 dump.00023.h5")
set(files_test6 "dump.00025.h5 dump.00028.h5")
set(files_test7 "dump.00030.h5 dump.00033.h5")
set(files_test8 "dump.00035.h5 dump.00039.h5")
set(files_test9 "dump.00040.h5 dump.00043.h5")

# Create the tests
foreach(ind RANGE 1 9)
  addNimTest(EXEC laplace_real
      CPFILES "example_real_${ind}.in"
      PREPROC exset
      PREPROCARGS "example_real_${ind}.in"
      TESTARGS "example_real_${ind}.in"
      RMFILES ${files_test_${ind}}
      LABELS laplace
      USE_MPI)

  # Complex tests to use dump.100xx.h5
  string(REPLACE "dump.000" "dump.100" comp_rm_files "${files_test_${ind}}")
  addNimTest(EXEC laplace_comp
      CPFILES "example_comp_${ind}.in"
      PREPROC exset
      PREPROCARGS "example_comp_${ind}.in"
      TESTARGS "example_comp_${ind}.in"
      RMFILES ${comp_rm_files}
      LABELS laplace
      USE_MPI)
endforeach()
