!-------------------------------------------------------------------------------
!! deforine fourier input
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* module that deforines fourier input
!-------------------------------------------------------------------------------
MODULE fourier_input_mod
  USE local
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: fourier_in

!-------------------------------------------------------------------------------
!: namelist fourier_input
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!> For nonlinear run, set the number of cells in the phi-direction directly
! through `nphi`. The number of toroidal modes after dealiasing is
! `nphi/dealiase + 1` (see below). Although any `nphi` may be used, optimal
! values are the products of small primes: e.g.
! `nphi=`\(2^l \times 3^n \times 5^m \times 7^p\)
!-------------------------------------------------------------------------------
  INTEGER(i4) :: nphi=-1
!-------------------------------------------------------------------------------
!> For linear runs, the number of Fourier modes is set directly with
! `lin_nmodes`.
!-------------------------------------------------------------------------------
  INTEGER(i4) :: lin_nmodes=1
!-------------------------------------------------------------------------------
!> One may specify the maximum toroidal mode number for these
! cases to get the range: `lin_nmax-(lin_nmodes-1) <= n <= lin_nmax`,
! otherwise the default gives: `0 <= n <= lin_nmodes-1`.
!-------------------------------------------------------------------------------
  INTEGER(i4) :: lin_nmax=0
!-------------------------------------------------------------------------------
!> The following option allows nimrod to skip the dealiasing in nonlinear
! computations (when `dealiase` is less than 3).  In this case, there are
! `nphi/2 + 1` Fourier components. Otherwise, there are `nphi/dealiase + 1`
! Fourier components. The default value, 3, dealiases for quadratic quantites.
!-------------------------------------------------------------------------------
  INTEGER(i4) :: dealiase=3
!-------------------------------------------------------------------------------
!: computed
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* type for fourier input parameter namespacing
!-------------------------------------------------------------------------------
  TYPE :: fourier_input
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST TYPE
!-------------------------------------------------------------------------------
    INTEGER(i4) :: nphi
    INTEGER(i4) :: lin_nmodes
    INTEGER(i4) :: lin_nmax
    INTEGER(i4) :: dealiase
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST TYPE
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   total number of modes
!-------------------------------------------------------------------------------
    INTEGER(i4) :: nmodes_total=0
  CONTAINS

    PROCEDURE, PASS(forin) :: compute => fourier_compute
    PROCEDURE, PASS(forin) :: read_namelist => fourier_read_namelist
    PROCEDURE, PASS(forin) :: copy_namelist_to_type                            &
                               => fourier_copy_namelist_to_type
    PROCEDURE, PASS(forin) :: h5read => fourier_h5read
    PROCEDURE, PASS(forin) :: h5write => fourier_h5write
    PROCEDURE, PASS(forin) :: bcast_input => fourier_bcast_input
  END TYPE fourier_input

  !* fourier input singleton type
  TYPE(fourier_input) :: fourier_in
CONTAINS

!-------------------------------------------------------------------------------
!> compute secondary parameters and check input
!-------------------------------------------------------------------------------
  SUBROUTINE fourier_compute(forin)
    USE pardata_mod
    IMPLICIT NONE

    !> fourier_input type
    CLASS(fourier_input), INTENT(INOUT) :: forin

!-------------------------------------------------------------------------------
!   initialize nmodes
!-------------------------------------------------------------------------------
    IF (forin%nphi>0) THEN
      IF (forin%dealiase>=3) THEN
        forin%nmodes_total=forin%nphi/forin%dealiase+1
      ELSE
        forin%nmodes_total=forin%nphi/2+1
      ENDIF
    ELSE
      forin%nmodes_total=forin%lin_nmodes
    ENDIF
  END SUBROUTINE fourier_compute

!-------------------------------------------------------------------------------
!> open and read the namelist input.
!-------------------------------------------------------------------------------
  SUBROUTINE fourier_read_namelist(forin,infile)
    USE local
    USE read_namelist_mod
    IMPLICIT NONE

    !> fourier_input type
    CLASS(fourier_input), TARGET, INTENT(INOUT) :: forin
    !> input file name
    CHARACTER(*), INTENT(IN) :: infile

    INTEGER(i4) :: read_stat
    CHARACTER(64) :: tempfile
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST BLOCK
!-------------------------------------------------------------------------------
    NAMELIST/fourier_input/nphi,lin_nmodes,lin_nmax,dealiase
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST BLOCK
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   Open input file. Remove comments from input file and put into temporary
!   file.
!-------------------------------------------------------------------------------
    tempfile='temp'//ADJUSTL(infile)
    CALL rmcomment(infile,tempfile)
    OPEN(UNIT=in_unit,FILE=tempfile,STATUS='OLD',POSITION='REWIND')
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST FILE READ
!-------------------------------------------------------------------------------
    CALL position_at_namelist('fourier_input', read_stat)
    IF (read_stat==0) THEN
      READ(UNIT=in_unit,NML=fourier_input,IOSTAT=read_stat)
      IF (read_stat/=0) THEN
        CALL print_namelist_error('fourier_input')
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST FILE READ
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   close and delete temporary input file.
!-------------------------------------------------------------------------------
    CLOSE(in_unit,STATUS='DELETE')
!-------------------------------------------------------------------------------
!   compute secondary parameters.
!-------------------------------------------------------------------------------
    CALL forin%copy_namelist_to_type
    CALL forin%compute
  END SUBROUTINE fourier_read_namelist

!-------------------------------------------------------------------------------
!> copy the namelist values into the namespaced type
!-------------------------------------------------------------------------------
  SUBROUTINE fourier_copy_namelist_to_type(forin)
    USE io
    IMPLICIT NONE

    !> fourier_input type
    CLASS(fourier_input), INTENT(INOUT) :: forin
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST COPY
!-------------------------------------------------------------------------------
    forin%nphi=nphi
    forin%lin_nmodes=lin_nmodes
    forin%lin_nmax=lin_nmax
    forin%dealiase=dealiase
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST COPY
!-------------------------------------------------------------------------------
  END SUBROUTINE fourier_copy_namelist_to_type

!-------------------------------------------------------------------------------
!> read h5 namelist from dump file. the h5 file should be opened outside of this
! routine
!-------------------------------------------------------------------------------
  SUBROUTINE fourier_h5read(forin)
    USE io
    IMPLICIT NONE

    !> fourier_input type
    CLASS(fourier_input), INTENT(INOUT) :: forin

    INTEGER(HID_T) :: gid

    CALL open_group(rootgid,"fourier_input",gid,h5err)
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST H5 READ
!-------------------------------------------------------------------------------
    CALL read_attribute(gid,"forin%nphi",forin%nphi,h5in,h5err)
    CALL read_attribute(gid,"forin%lin_nmodes",forin%lin_nmodes,h5in,h5err)
    CALL read_attribute(gid,"forin%lin_nmax",forin%lin_nmax,h5in,h5err)
    CALL read_attribute(gid,"forin%dealiase",forin%dealiase,h5in,h5err)
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST H5 READ
!-------------------------------------------------------------------------------
    CALL close_group("fourier_input",gid,h5err)
    CALL forin%compute
  END SUBROUTINE fourier_h5read

!-------------------------------------------------------------------------------
!> write h5 namelist to dump file. the h5 file should be opened outside of this
! routine
!-------------------------------------------------------------------------------
  SUBROUTINE fourier_h5write(forin)
    USE io
    IMPLICIT NONE

    !> fourier_input type
    CLASS(fourier_input), INTENT(INOUT) :: forin

    INTEGER(HID_T) :: gid

    CALL make_group(rootgid,"fourier_input",gid,h5in,h5err)
!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST H5 WRITE
!-------------------------------------------------------------------------------
    CALL write_attribute(gid,"forin%nphi",forin%nphi,h5in,h5err)
    CALL write_attribute(gid,"forin%lin_nmodes",forin%lin_nmodes,h5in,h5err)
    CALL write_attribute(gid,"forin%lin_nmax",forin%lin_nmax,h5in,h5err)
    CALL write_attribute(gid,"forin%dealiase",forin%dealiase,h5in,h5err)
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST H5 WRITE
!-------------------------------------------------------------------------------
    CALL close_group("fourier_input",gid,h5err)
  END SUBROUTINE fourier_h5write

!-------------------------------------------------------------------------------
!> broadcast info read by proc 0 out of nimrod.in (namelist reads) to all
! processors. every quantity in input module is broadcast in case it was read.
!-------------------------------------------------------------------------------
  SUBROUTINE fourier_bcast_input(forin)
    USE pardata_mod
    IMPLICIT NONE

    !> fourier_input type
    CLASS(fourier_input), INTENT(INOUT) :: forin

!-------------------------------------------------------------------------------
!   Do not modify, use codegen/updateNamelists.py
!   BEGIN GENERATED NAMELIST BCAST
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   fourier_input
!-------------------------------------------------------------------------------
    CALL par%all_bcast(forin%nphi,0)
    CALL par%all_bcast(forin%lin_nmodes,0)
    CALL par%all_bcast(forin%lin_nmax,0)
    CALL par%all_bcast(forin%dealiase,0)
!-------------------------------------------------------------------------------
!   END GENERATED NAMELIST BCAST
!-------------------------------------------------------------------------------
    CALL forin%compute
  END SUBROUTINE fourier_bcast_input

END MODULE fourier_input_mod
