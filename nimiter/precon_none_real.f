!-------------------------------------------------------------------------------
!! Defines the interface for no preconditioner
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* Defines the interface for no preconditioner.
!-------------------------------------------------------------------------------
MODULE precon_none_real_mod
  USE local
  USE matrix_mod
  USE preconditioner_mod
  USE vector_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: precon_none_real

  CHARACTER(*), PARAMETER :: mod_name='precon_none_real_mod'
!-------------------------------------------------------------------------------
!> Type that defines a interface for no preconditioner for real data
!-------------------------------------------------------------------------------
  TYPE, EXTENDS(precon_real) :: precon_none_real
  CONTAINS

    ! Type-bound extensions
    PROCEDURE, PASS(precon) :: alloc => alloc_real
    ! Abstract class deferred functions
    PROCEDURE, PASS(precon) :: dealloc => dealloc_real
    PROCEDURE, PASS(precon) :: update => update_real
    PROCEDURE, PASS(precon) :: apply_precon => apply_precon_real
  END TYPE precon_none_real

CONTAINS

!-------------------------------------------------------------------------------
!* Allocate the preconditioner
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_real(precon)
    IMPLICIT NONE

    !> preconditioner to alloc
    CLASS(precon_none_real), INTENT(INOUT) :: precon

    NULLIFY(precon%f_precon_init)
    NULLIFY(precon%f_precon_finalize)
  END SUBROUTINE alloc_real

!-------------------------------------------------------------------------------
!* Deallocate the preconditioner
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_real(precon)
    IMPLICIT NONE

    !> preconditioner to dealloc
    CLASS(precon_none_real), INTENT(INOUT) :: precon

  END SUBROUTINE dealloc_real

!-------------------------------------------------------------------------------
!* Update the preconditioner after the matrix update
!-------------------------------------------------------------------------------
  SUBROUTINE update_real(precon,mat,seam)
    USE seam_mod
    IMPLICIT NONE

    !> preconditioner to update
    CLASS(precon_none_real), INTENT(INOUT) :: precon
    !> matrix M to be used for preconditioning
    CLASS(rmat_storage), DIMENSION(:), INTENT(IN) :: mat
    !> associated seam
    TYPE(seam_type), INTENT(INOUT) :: seam

  END SUBROUTINE update_real

!-------------------------------------------------------------------------------
!* Call the core preconditioner (note: use the apply interface directly)
!-------------------------------------------------------------------------------
  SUBROUTINE apply_precon_real(precon,mat,res,zee,adr)
    USE pardata_mod
    USE timer_mod
    IMPLICIT NONE

    !> preconditioner to apply
    CLASS(precon_none_real), INTENT(INOUT) :: precon
    !> matrix M to be used for preconditioning
    CLASS(rmat_storage), DIMENSION(:), INTENT(IN) :: mat
    !> the residual res = rhs - A x_k
    TYPE(rvec_storage), DIMENSION(:), INTENT(IN) :: res
    !> the preconditioned residual, zee = M^-1 res
    TYPE(rvec_storage), DIMENSION(:), INTENT(INOUT) :: zee
    !> the full matrix vector product, A x_k
    TYPE(rvec_storage), DIMENSION(:), INTENT(IN) :: adr

    INTEGER(i4) :: ibl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'apply_precon_real',iftn,idepth)
    DO ibl=1,par%nbl
      zee(ibl)%v=res(ibl)%v
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE apply_precon_real

END MODULE precon_none_real_mod
