######################################################################
#
# CMakeLists.txt for nimseam/tests
#
######################################################################

# Make sure we run locally
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

addNimUnitTest(SOURCEFILES test_seam.f
               TESTARGS alloc_dealloc init edge_network h5io
	       LINK_LIBS nimseam
               RMFILES *.h5;
               LABELS seam
	       USE_MPI)
