#!/usr/bin/env python3
import process_namelist
import os

# (File name relative to the root directory, type name inside functions)
updateFiles = [['nimgrid/grid_input.f', 'gridin'],
               ['nimlib/pardata_input.f', 'parin'],
               ['nimfft/fft_input.f', 'forin'],
               ['example/exset_input.f', None],
               ['example/laplace_input.f', None]]

namelistdir = os.path.dirname(os.path.realpath(__file__))+'/../'

for cmd_input_uq in updateFiles:
    cmd_input = cmd_input_uq
    cmd_input[0] = namelistdir + cmd_input[0]
    process_namelist.process_namelist.updateNamelist(*cmd_input)
