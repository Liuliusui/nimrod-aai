!-------------------------------------------------------------------------------
!! Routines for handling a generic block type. It contains the
! necessary information for block integration, boundary conditions and I/O.
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!* Defines the generic block type and the associated procedures.
!-------------------------------------------------------------------------------
MODULE gblock_mod
  USE local
  USE nodal_mod
  USE pardata_mod
  USE quadrature_mod
  USE timer_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: gblock,field_list_pointer,block_storage

  CHARACTER(*), PARAMETER :: mod_name='gblock'
!-------------------------------------------------------------------------------
!* Generic pointer array used to store a list of nodal fields for I/O
!-------------------------------------------------------------------------------
  TYPE :: field_list_pointer
    !> field name
    CHARACTER(64) :: name="unknown"
    !> field type
    CHARACTER(64) :: type="unknown" ! rfem_storage or cfem_storage
    !> pointer to field storage type, rfem_storage(1:nbl) or cfem_storage(1:nbl)
    CLASS(*), POINTER, DIMENSION(:) :: p=>NULL()
  END TYPE field_list_pointer

!-------------------------------------------------------------------------------
!> gblock defines a generic interface common to all blocks. It contains the
!  necessary information for block integration, boundary conditions  and I/O.
!-------------------------------------------------------------------------------
  TYPE, ABSTRACT :: gblock
    !> Block type
    CHARACTER(8) :: bl_type="unset"
    !> Name of block
    CHARACTER(64) :: name="unknown"
    !> Global id
    INTEGER(i4) :: id=0
    !> true if data on GPU
    LOGICAL :: on_gpu=.FALSE.
    !> Local storage index
    INTEGER(i4) :: ibl=0
    !> Number of elements
    INTEGER(i4) :: nel=0
    !> Number of Gaussian quadrature points
    INTEGER(i4) :: ng=0
    !> Number of Gaussian quadrature points on boundary
    INTEGER(i4) :: ng_bdry=0
    !> Max polynomial degree of elements
    INTEGER(i4) :: poly_degree=0
    !> Volume integration formula
    CHARACTER(8) :: int_formula="unknown"
    !> Surface integration formula
    CHARACTER(8) :: surf_int_formula="unknown"
    !> True if block touches R=0 in toroidal geometry
    LOGICAL :: r0block
    !> Metric information at quadrature points.
    TYPE(qjac_real) :: metric
    !> Logical coordinates of the quadrature points;
    !  size (ng,ndim)
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: xg
    !> Logical coordinates of the quadrature points on the boundary;
    !  size (ng_bdry,ndim)
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: xg_bdry
    !> Quadrature point weights for integration; size (ng,ndim)
    REAL(r8), DIMENSION(:), ALLOCATABLE :: wg
    !> Quadrature point weights for integration on the boundary; size (ng,ndim)
    REAL(r8), DIMENSION(:), ALLOCATABLE :: wg_bdry
    !> Mesh structure
    CLASS(nodal_field_real), ALLOCATABLE :: mesh
    !> VizSchema mesh type
    CHARACTER(16) :: vsmesh_type
    !> VizSchema mesh labels
    CHARACTER(16) :: vsmesh_labels
  CONTAINS

    PROCEDURE(dealloc), PASS(bl), DEFERRED :: dealloc
    PROCEDURE(block_intg_formula_set), PASS(bl), DEFERRED :: block_intg_formula_set
    PROCEDURE(block_metric_set), PASS(bl), DEFERRED :: block_metric_set
    PROCEDURE(h5_read),  PASS(bl), DEFERRED  :: h5_read
    PROCEDURE(h5_write), PASS(bl), DEFERRED  :: h5_write
    PROCEDURE(distribute), PASS(bl), DEFERRED :: distribute
    PROCEDURE(collect), PASS(bl), DEFERRED :: collect
    PROCEDURE, PASS(bl) :: dealloc_global_block_data
    PROCEDURE, NOPASS :: alloc_real_fld
    PROCEDURE, NOPASS :: alloc_comp_fld
    PROCEDURE, PASS(bl) :: h5_read_global_block_data
    PROCEDURE, PASS(bl) :: h5_write_global_block_data
  END TYPE gblock

!-------------------------------------------------------------------------------
!* container for gblock
!-------------------------------------------------------------------------------
  TYPE :: block_storage
    CLASS(gblock), ALLOCATABLE :: b
  END TYPE block_storage

  ABSTRACT INTERFACE
!-------------------------------------------------------------------------------
!>  deallocate the block
!-------------------------------------------------------------------------------
    SUBROUTINE dealloc(bl)
      IMPORT gblock
      !> block to deallocate
      CLASS(gblock), INTENT(INOUT) :: bl
    END SUBROUTINE dealloc

!-------------------------------------------------------------------------------
!>  Deferred procedure for setting the quadrature locations and weights
!-------------------------------------------------------------------------------
    SUBROUTINE block_intg_formula_set(bl,ngr,int_formula,surf_int_formula)
      USE local, ONLY: i4
      IMPORT gblock
      !> block to set
      CLASS(gblock), INTENT(INOUT) :: bl
      !> total number of gaussian quadrature points is (ngr+poly_degree-1)**ndim
      INTEGER(i4), INTENT(IN) :: ngr
      !> integration formula (gaussian or lobatto)
      CHARACTER(8), INTENT(IN) :: int_formula
      !> boundary integration formula (gaussian or lobatto)
      CHARACTER(8), INTENT(IN) :: surf_int_formula
    END SUBROUTINE block_intg_formula_set

!-------------------------------------------------------------------------------
!*  calculate the metric-realted derivatives for the finite element grid.
!-------------------------------------------------------------------------------
    SUBROUTINE block_metric_set(bl,torgeom)
      IMPORT gblock
      !> block to set
      CLASS(gblock), INTENT(INOUT) :: bl
      !> true if in toroidal geometry
      LOGICAL, INTENT(IN) :: torgeom
    END SUBROUTINE block_metric_set

!-------------------------------------------------------------------------------
!*  read block data from h5 file
!-------------------------------------------------------------------------------
    SUBROUTINE h5_read(bl,id,h5gid,io_field_list)
      USE io
      USE local
      IMPORT gblock
      IMPORT field_list_pointer
      !> block to read
      CLASS(gblock), INTENT(INOUT) :: bl
      !> block global id
      INTEGER(i4), INTENT(IN) :: id
      !> h5 group to read from
      INTEGER(HID_T), INTENT(IN) :: h5gid
      !> I/O field list pointing to data to read
      TYPE(field_list_pointer), INTENT(INOUT) :: io_field_list(:)
    END SUBROUTINE h5_read

!-------------------------------------------------------------------------------
!*  write block to an h5 file
!-------------------------------------------------------------------------------
    SUBROUTINE h5_write(bl,h5gid,io_field_list)
      USE io
      IMPORT gblock
      IMPORT field_list_pointer
      !> block to write
      CLASS(gblock), INTENT(IN) :: bl
      !> h5 group to write in
      INTEGER(HID_T), INTENT(IN) :: h5gid
      !> I/O field list pointing to data to write to block
      TYPE(field_list_pointer), INTENT(IN) :: io_field_list(:)
    END SUBROUTINE h5_write

!-------------------------------------------------------------------------------
!*  distribute block data with layer decomposition
!-------------------------------------------------------------------------------
    SUBROUTINE distribute(bl,io_field_list,inode)
      USE local
      IMPORT gblock
      IMPORT field_list_pointer
      !> block to distribute
      CLASS(gblock), INTENT(INOUT) :: bl
      !> I/O field list to distribute
      TYPE(field_list_pointer), INTENT(INOUT) :: io_field_list(:)
      !> mpi process to broadcast from
      INTEGER(i4), INTENT(IN) :: inode
    END SUBROUTINE distribute

!-------------------------------------------------------------------------------
!*  collect layer-distributed block data
!-------------------------------------------------------------------------------
    SUBROUTINE collect(bl,io_field_list,io_field_write,inode)
      USE local
      IMPORT gblock
      IMPORT field_list_pointer
      !> block to collect
      CLASS(gblock), INTENT(IN) :: bl
      !> I/O field list pointing to data to collect
      TYPE(field_list_pointer), INTENT(IN) :: io_field_list(:)
      !> I/O field list pointing to data to collect to on layer zero
      TYPE(field_list_pointer), INTENT(IN) :: io_field_write(:)
      !> mpi process to collect fields on
      INTEGER(i4), INTENT(IN) :: inode
    END SUBROUTINE collect
  END INTERFACE

CONTAINS

!-------------------------------------------------------------------------------
!> deallocate data common to all blocks. This function should be called
!  from the child block deallocate function.
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_global_block_data(bl)
    IMPLICIT NONE

    !> block to deallocate
    CLASS(gblock), INTENT(INOUT) :: bl

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc_global_block_data',iftn,idepth)
!-------------------------------------------------------------------------------
!   Deallocate arrays and resest variables common to all blocks
!-------------------------------------------------------------------------------
    bl%name="unknown"
    bl%id=0
    bl%nel=0
    bl%ng=0
    bl%ng_bdry=0
    bl%poly_degree=0
    bl%int_formula="unknown"
    bl%surf_int_formula="unknown"
    IF (ALLOCATED(bl%mesh)) THEN
      CALL bl%mesh%dealloc
      DEALLOCATE(bl%mesh)
    ENDIF
    IF (ALLOCATED(bl%xg)) DEALLOCATE (bl%xg)
    IF (ALLOCATED(bl%xg_bdry)) DEALLOCATE (bl%xg_bdry)
    IF (ALLOCATED(bl%wg)) DEALLOCATE (bl%wg)
    IF (ALLOCATED(bl%wg_bdry)) DEALLOCATE (bl%wg_bdry)
    IF (ALLOCATED(bl%metric%jac)) DEALLOCATE(bl%metric%jac)
    IF (ALLOCATED(bl%metric%ijac)) DEALLOCATE(bl%metric%ijac)
    IF (ALLOCATED(bl%metric%bigr)) DEALLOCATE(bl%metric%bigr)
    IF (ALLOCATED(bl%metric%detj)) DEALLOCATE(bl%metric%detj)
    IF (ALLOCATED(bl%metric%wdetj)) DEALLOCATE(bl%metric%wdetj)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc_global_block_data

!-------------------------------------------------------------------------------
!*  Allocate a real nodal field based on a field type string.
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_real_fld(fld,field_type,on_gpu)
    USE h1_rect_2D_mod
    USE hcurl_rect_2D_mod
    IMPLICIT NONE

    !> nodal field to alloc
    CLASS(nodal_field_real), INTENT(INOUT), ALLOCATABLE :: fld
    !> field type
    CHARACTER(64), INTENT(IN) :: field_type
    !> true if GPU-enabled types are to be used
    LOGICAL, INTENT(IN) :: on_gpu

!-------------------------------------------------------------------------------
!   Allocate field to have the given type
!-------------------------------------------------------------------------------
    SELECT CASE(TRIM(field_type))
    CASE ("h1_rect_2D_real","h1_rect_2D_real_acc")
      IF (on_gpu) THEN
        ALLOCATE(h1_rect_2D_real_acc::fld)
        fld%on_gpu=.TRUE.
      ELSE
        ALLOCATE(h1_rect_2D_real::fld)
      ENDIF
    CASE ("hcurl_rect_2D_real")
      ALLOCATE(hcurl_rect_2D_real::fld)
    CASE DEFAULT
      CALL par%nim_stop("in gblock alloc_real_fld could not find fieldtype="    &
                        //TRIM(field_type))
    END SELECT
  END SUBROUTINE alloc_real_fld

!-------------------------------------------------------------------------------
!*  Allocate a comp nodal field based on a field type string.
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_comp_fld(fld,field_type,on_gpu)
    USE h1_rect_2D_mod
    USE hcurl_rect_2D_mod
    IMPLICIT NONE

    !> nodal field to alloc
    CLASS(nodal_field_comp), INTENT(INOUT), ALLOCATABLE :: fld
    !> field type
    CHARACTER(64), INTENT(IN) :: field_type
    !> true if GPU-enabled types are to be used
    LOGICAL, INTENT(IN) :: on_gpu

!-------------------------------------------------------------------------------
!   Allocate field to have the given type
!-------------------------------------------------------------------------------
    SELECT CASE(TRIM(field_type))
    CASE ("h1_rect_2D_comp","h1_rect_2D_comp_acc")
      IF (on_gpu) THEN
        ALLOCATE(h1_rect_2D_comp_acc::fld)
        fld%on_gpu=.TRUE.
      ELSE
        ALLOCATE(h1_rect_2D_comp::fld)
      ENDIF
    CASE DEFAULT
      CALL par%nim_stop("in gblock alloc_comp_fld could not find fieldtype="    &
                        //TRIM(field_type))
    END SELECT
  END SUBROUTINE alloc_comp_fld

!-------------------------------------------------------------------------------
!> Read data that is common to all block types from an hdf5 file
!  This subroutine should be called from the the child block type
!  h5_read procedure
!-------------------------------------------------------------------------------
  SUBROUTINE h5_read_global_block_data(bl,h5blid,blid,io_field_list)
    USE io
    USE pardata_mod
    IMPLICIT NONE

    !> block to read
    CLASS(gblock), INTENT(INOUT) :: bl
    !> block h5 id
    INTEGER(HID_T), INTENT(IN) :: h5blid
    !> bl%id stored as a string
    CHARACTER(4), INTENT(IN) :: blid
    !> I/O field list pointing to data to read
    TYPE(field_list_pointer), INTENT(INOUT) :: io_field_list(:)

    INTEGER(i4) :: ifld,r0block
    CHARACTER(64) :: fldname
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'h5_read_global_block_data',iftn,idepth)
!-------------------------------------------------------------------------------
!   read gblock descriptors.
!-------------------------------------------------------------------------------
    CALL read_attribute(h5blid,"block_type",bl%bl_type,h5in,h5err)
    CALL read_attribute(h5blid,"id",bl%id,h5in,h5err)
    bl%ibl=par%global2local(bl%id)
    CALL read_attribute(h5blid,"poly_degree",bl%poly_degree,h5in,h5err)
    CALL read_attribute(h5blid,"nel",bl%nel,h5in,h5err)
    CALL read_attribute(h5blid,"r0block",r0block,h5in,h5err)
    IF (r0block>0) THEN
      bl%r0block=.true.
    ELSE
      bl%r0block=.false.
    ENDIF
!-------------------------------------------------------------------------------
!   read the mesh
!-------------------------------------------------------------------------------
    CALL read_real_fld(bl%mesh,'mesh',h5blid,blid,bl%on_gpu)
!-------------------------------------------------------------------------------
!   read all fields in io field list
!-------------------------------------------------------------------------------
    DO ifld=1,SIZE(io_field_list)
      fldname=TRIM(io_field_list(ifld)%name)
      SELECT TYPE (iofield=>io_field_list(ifld)%p(bl%ibl))
      CLASS IS (rfem_storage)
        CALL read_real_fld(iofield%f,fldname,h5blid,blid,bl%on_gpu)
      CLASS IS (cfem_storage)
        CALL read_comp_fld(iofield%f,fldname,h5blid,blid,bl%on_gpu)
      END SELECT
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  CONTAINS

!-------------------------------------------------------------------------------
!*  Read a real nodal field.
!-------------------------------------------------------------------------------
    SUBROUTINE read_real_fld(fld,fldname,h5blid,blid,on_gpu)
      IMPLICIT NONE

      !> nodal field to read
      CLASS(nodal_field_real), INTENT(INOUT), ALLOCATABLE, TARGET :: fld
      !> field name
      CHARACTER(*), INTENT(IN) :: fldname
      !> block h5 id
      INTEGER(HID_T), INTENT(IN) :: h5blid
      !> bl%id stored as a string
      CHARACTER(4), INTENT(IN) :: blid
      !> true if GPU-enabled types are to be used
      LOGICAL, INTENT(IN) :: on_gpu

      INTEGER(HID_T) :: dsetid
      INTEGER :: error
      CHARACTER(64) :: field_type
!-------------------------------------------------------------------------------
!     open h5 data structure and get field type
!-------------------------------------------------------------------------------
      CALL h5dopen_f(h5blid,TRIM(fldname)//TRIM(blid),dsetid,error)
      CALL read_attribute(dsetid,"field_type",field_type,h5in,h5err)
      CALL h5dclose_f(dsetid,error)
!-------------------------------------------------------------------------------
!     allocate field to have the given type
!-------------------------------------------------------------------------------
      CALL alloc_real_fld(fld,field_type,on_gpu)
!-------------------------------------------------------------------------------
!     read in the field
!-------------------------------------------------------------------------------
      CALL fld%h5_read(fldname,h5blid,bl%id,blid)
    END SUBROUTINE read_real_fld

!-------------------------------------------------------------------------------
!*  Read a complex nodal field.
!-------------------------------------------------------------------------------
    SUBROUTINE read_comp_fld(fld,fldname,h5blid,blid,on_gpu)
      USE h1_rect_2D_mod
      IMPLICIT NONE

      !>nodal field to read
      CLASS(nodal_field_comp), INTENT(INOUT), ALLOCATABLE, TARGET :: fld
      !> field name
      CHARACTER(*), INTENT(IN) :: fldname
      !> block h5 id
      INTEGER(HID_T), INTENT(IN) :: h5blid
      !> bl%id stored as a string
      CHARACTER(4), INTENT(IN) :: blid
      !> true if GPU-enabled types are to be used
      LOGICAL, INTENT(IN) :: on_gpu

      INTEGER(HID_T) :: dsetid
      INTEGER :: error
      CHARACTER(64) :: field_type
!-------------------------------------------------------------------------------
!     open h5 data structure and get field type
!-------------------------------------------------------------------------------
      CALL h5dopen_f(h5blid,"re"//TRIM(fldname)//TRIM(blid),dsetid,error)
      CALL read_attribute(dsetid,"field_type",field_type,h5in,h5err)
      CALL h5dclose_f(dsetid,error)
!-------------------------------------------------------------------------------
!     allocate field to have the given type
!-------------------------------------------------------------------------------
      CALL alloc_comp_fld(fld,field_type,on_gpu)
!-------------------------------------------------------------------------------
!     read in the field
!-------------------------------------------------------------------------------
      CALL fld%h5_read(fldname,h5blid,bl%id,blid)
    END SUBROUTINE read_comp_fld

  END SUBROUTINE h5_read_global_block_data

!-------------------------------------------------------------------------------
!> Write data that is common to all block types from an hdf5 file
!  This subroutine should be called from the the child block type
!  h5_write procedure
!-------------------------------------------------------------------------------
  SUBROUTINE h5_write_global_block_data(bl,h5blid,blid,io_field_list)
    USE io
    IMPLICIT NONE

    !> block to write
    CLASS(gblock), INTENT(IN) :: bl
    !> h5 group to write to
    INTEGER(HID_T), INTENT(IN) :: h5blid
    !> bl%id stored as a string
    CHARACTER(4), INTENT(IN) :: blid
    !> I/O field list pointing to data to write to block
    TYPE(field_list_pointer), INTENT(IN) :: io_field_list(:)

    INTEGER(i4) :: ifld
    CHARACTER(64) :: fldname
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'h5_write_global_block_data',iftn,idepth)
!-------------------------------------------------------------------------------
!   write block descriptors.
!-------------------------------------------------------------------------------
    CALL write_attribute(h5blid,"block_type",bl%bl_type,h5in,h5err)
    CALL write_attribute(h5blid,"id",bl%id,h5in,h5err)
    CALL write_attribute(h5blid,"nel",bl%nel,h5in,h5err)
    CALL write_attribute(h5blid,"poly_degree",bl%poly_degree,h5in,h5err)
    IF (bl%r0block) THEN
      CALL write_attribute(h5blid,"r0block",1_i4,h5in,h5err)
    ELSE
      CALL write_attribute(h5blid,"r0block",0_i4,h5in,h5err)
    ENDIF
!-------------------------------------------------------------------------------
!   write the mesh
!-------------------------------------------------------------------------------
    h5in%mesh=bl%vsmesh_type !"mesh-structured"
    h5in%vsAxisLabels=bl%vsmesh_labels !"R,Z"
    CALL bl%mesh%h5_dump('mesh',h5blid,blid)
!-------------------------------------------------------------------------------
!   write the nodal fields in io field list
!-------------------------------------------------------------------------------
    h5in%mesh="/blocks/"//TRIM(blid)//"/mesh"//TRIM(blid)
    DO ifld=1,SIZE(io_field_list)
      fldname=TRIM(io_field_list(ifld)%name)
      SELECT TYPE (iofield=>io_field_list(ifld)%p(bl%ibl))
      CLASS IS (rfem_storage)
        CALL iofield%f%h5_dump(fldname,h5blid,blid)
      CLASS IS (cfem_storage)
        CALL iofield%f%h5_dump(fldname,h5blid,blid)
      CLASS DEFAULT
        CALL par%nim_stop("fem field storage type not found in gblock write")
      END SELECT
    ENDDO
!-------------------------------------------------------------------------------
!   reset h5in structure
!-------------------------------------------------------------------------------
    h5in%mesh=" "
    h5in%vsAxisLabels=" "
    h5in%vsIndexOrder=" "
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE  h5_write_global_block_data

END MODULE
