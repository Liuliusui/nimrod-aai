#!/bin/bash

# Get large packages with the script to avoid repetitive downloads
wget https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.12/hdf5-1.12.2/src/CMake-hdf5-1.12.2.tar.gz
#wget https://github.com/Kitware/CMake/releases/download/v3.24.3/cmake-3.24.3-Linux-x86_64.tar.gz 
# get aocc from https://developer.amd.com/amd-aocc/

