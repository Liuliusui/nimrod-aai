!-------------------------------------------------------------------------------
!! Routines to streamline block-wise finite element computations
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* Routines to streamline block-wise finite element computations
!-------------------------------------------------------------------------------
MODULE finite_element_mod
  USE local
  USE timer_mod
  USE integrand_mod
  USE seam_mod
  USE gblock_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: create_matrix,create_vector

  INTERFACE create_matrix
    MODULE PROCEDURE create_matrix_real,create_matrix_comp
  END INTERFACE create_matrix

  INTERFACE create_vector
    MODULE PROCEDURE create_vector_real,create_vector_comp
  END INTERFACE create_vector

  CHARACTER(*), PARAMETER :: mod_name='finite_element'
CONTAINS

!-------------------------------------------------------------------------------
!* call block-wise finite element computations to create a matrix
!-------------------------------------------------------------------------------
  SUBROUTINE create_matrix_real(bl,matrix,precon,seam,integrand_func,           &
                                dirichlet_bc,bc_component,interior_elim)
    USE matrix_mod
    USE preconditioner_mod
    IMPLICIT NONE

    !> block storage
    TYPE(block_storage), INTENT(IN) :: bl(:)
    !> matrix to create stored in array by block
    TYPE(rmat_storage), INTENT(INOUT) :: matrix(:)
    !> preconditioner for matrix
    CLASS(precon_real), INTENT(INOUT) :: precon
    !> seam
    TYPE(seam_type), INTENT(INOUT) :: seam
    !> integrand procedure to use
    PROCEDURE(integrand_mat_real) :: integrand_func
    !> if true a dirichlet BC is applied to bc_component
    LOGICAL, INTENT(IN) :: dirichlet_bc
    !> components to use for dirichlet BC
    CHARACTER(*), INTENT(IN) :: bc_component
    !> if true interior elimination is applied to the matrix
    LOGICAL, INTENT(IN) :: interior_elim

    REAL(r8), ALLOCATABLE :: int_mat(:,:,:,:,:)
    INTEGER(i4) :: ibl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l1(mod_name,'create_matrix_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   loop over blocks and integrate
!-------------------------------------------------------------------------------
    DO ibl=1,SIZE(bl)
      ASSOCIATE (mat=>matrix(ibl)%m)
        ALLOCATE(int_mat(mat%nqty,mat%nqty,mat%nel,mat%u_ndof,mat%u_ndof))
        !$acc enter data create(int_mat) async(mat%id) if(mat%on_gpu)
        !$acc kernels present(int_mat) async(mat%id) if(mat%on_gpu)
        int_mat=0._r8
        !$acc end kernels
        CALL integrand_func(bl(ibl)%b,int_mat)
        CALL mat%zero
        CALL mat%assemble(int_mat)
        !$acc exit data delete(int_mat) finalize async(mat%id) if(mat%on_gpu)
        DEALLOCATE(int_mat)
!-------------------------------------------------------------------------------
!       determine a scaling factor for diagonal matrix elements that is based on
!       grid-vertex entries.
!-------------------------------------------------------------------------------
        CALL mat%find_diag_scale
!-------------------------------------------------------------------------------
!       call the regularity condition and the boundary condition routines.
!-------------------------------------------------------------------------------
        IF (bl(ibl)%b%r0block) CALL mat%regularity(seam%s(ibl))
        IF (dirichlet_bc) CALL mat%dirichlet_bc(bc_component,seam%s(ibl))
!-------------------------------------------------------------------------------
!       TODO surface integrand contributions
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!       eliminate connections to cell-centered data (basis functions of degree
!       greater than linear) and save the inverse of the interior block.
!-------------------------------------------------------------------------------
        IF (interior_elim) CALL mat%elim_inv_int
      END ASSOCIATE
    ENDDO
!-------------------------------------------------------------------------------
!   update the preconditioner
!-------------------------------------------------------------------------------
    CALL precon%update(matrix,seam)
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE create_matrix_real

!-------------------------------------------------------------------------------
!* call block-wise finite element computations to create a matrix
!-------------------------------------------------------------------------------
  SUBROUTINE create_matrix_comp(bl,matrix,precon,seam,integrand_func,           &
                                dirichlet_bc,bc_component,interior_elim)
    USE matrix_mod
    USE preconditioner_mod
    IMPLICIT NONE

    !> block storage
    TYPE(block_storage), INTENT(IN) :: bl(:)
    !> matrix to create stored in array by block
    TYPE(cmat_storage), INTENT(INOUT) :: matrix(:,:)
    !> preconditioner for matrix
    CLASS(precon_comp), INTENT(INOUT) :: precon
    !> seam
    TYPE(seam_type), INTENT(INOUT) :: seam
    !> integrand procedure to use
    PROCEDURE(integrand_mat_comp) :: integrand_func
    !> if true a dirichlet BC is applied to bc_component
    LOGICAL, INTENT(IN) :: dirichlet_bc
    !> components to use for dirichlet BC
    CHARACTER(*), INTENT(IN) :: bc_component
    !> if true interior elimination is applied to the matrix
    LOGICAL, INTENT(IN) :: interior_elim

    COMPLEX(r8), ALLOCATABLE :: int_mat(:,:,:,:,:)
    INTEGER(i4) :: ibl,imode
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l1(mod_name,'create_matrix_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   loop over blocks and integrate
!-------------------------------------------------------------------------------
    DO imode=1,SIZE(matrix,2)
      DO ibl=1,SIZE(bl)
        ASSOCIATE (mat=>matrix(ibl,imode)%m)
          ALLOCATE(int_mat(mat%nqty,mat%nqty,mat%nel,mat%u_ndof,mat%u_ndof))
          !$acc enter data create(int_mat) async(mat%id) if(mat%on_gpu)
          !$acc kernels present(int_mat) async(mat%id) if(mat%on_gpu)
          int_mat=0._r8
          !$acc end kernels
          CALL integrand_func(bl(ibl)%b,int_mat,imode,imode)
          CALL mat%zero
          CALL mat%assemble(int_mat)
          !$acc exit data delete(int_mat) finalize async(mat%id) if(mat%on_gpu)
          DEALLOCATE(int_mat)
!-------------------------------------------------------------------------------
!         determine a scaling factor for diagonal matrix elements that is based
!         on grid-vertex entries.
!-------------------------------------------------------------------------------
          CALL mat%find_diag_scale
!-------------------------------------------------------------------------------
!         call the regularity condition and the boundary condition routines.
!-------------------------------------------------------------------------------
          IF (bl(ibl)%b%r0block) CALL mat%regularity(seam%s(ibl))
          IF (dirichlet_bc) CALL mat%dirichlet_bc(bc_component,seam%s(ibl))
!-------------------------------------------------------------------------------
!         TODO surface integrand contributions
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!         eliminate connections to cell-centered data (basis functions of degree
!         greater than linear) and save the inverse of the interior block.
!-------------------------------------------------------------------------------
          IF (interior_elim) CALL mat%elim_inv_int
        END ASSOCIATE
      ENDDO
    ENDDO
!-------------------------------------------------------------------------------
!   update the preconditioner
!-------------------------------------------------------------------------------
    CALL precon%update(matrix,seam)
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE create_matrix_comp

!-------------------------------------------------------------------------------
!* call block-wise finite element computations to create a vector
!-------------------------------------------------------------------------------
  SUBROUTINE create_vector_real(bl,vector,seam,integrand_func,dirichlet_bc,     &
                                bc_component,elim_matrix)
    USE matrix_mod
    USE vector_mod
    IMPLICIT NONE

    !> block storage
    TYPE(block_storage), INTENT(IN) :: bl(:)
    !> vector to create stored in array by block
    TYPE(rvec_storage), INTENT(INOUT) :: vector(:)
    !* seam
    TYPE(seam_type), INTENT(INOUT) :: seam
    !> integrand procedure to use
    PROCEDURE(integrand_vec_real) :: integrand_func
    !> if true a dirichlet BC is applied to bc_component
    LOGICAL, INTENT(IN) :: dirichlet_bc
    !> components to use for dirichlet BC
    CHARACTER(*), INTENT(IN) :: bc_component
    !> matrix to use to eliminate interior DOFs stored in array by block
    TYPE(rmat_storage), OPTIONAL, INTENT(IN) :: elim_matrix(:)

    REAL(r8), ALLOCATABLE :: int_arr(:,:,:)
    INTEGER(i4) :: ibl
    CLASS(rvector), ALLOCATABLE :: temp_vec
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l1(mod_name,'create_vector_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   loop over blocks and integrate
!-------------------------------------------------------------------------------
    DO ibl=1,SIZE(bl)
      ASSOCIATE (vec=>vector(ibl)%v)
        ALLOCATE(int_arr(vec%nqty,vec%nel,vec%u_ndof))
        !$acc enter data create(int_arr) async(vec%id) if(vec%on_gpu)
        !$acc kernels present(int_arr) async(vec%id) if(vec%on_gpu)
        int_arr=0._r8
        !$acc end kernels
        CALL integrand_func(bl(ibl)%b,int_arr)
        CALL vec%zero
        CALL vec%assemble(int_arr)
        !$acc exit data delete(int_arr) finalize async(vec%id) if(vec%on_gpu)
        DEALLOCATE(int_arr)
!-------------------------------------------------------------------------------
!       call the regularity condition and the boundary condition routines.
!-------------------------------------------------------------------------------
        IF (bl(ibl)%b%r0block) CALL vec%regularity(seam%s(ibl),'all')
        IF (dirichlet_bc) CALL vec%dirichlet_bc(bc_component,seam%s(ibl))
!-------------------------------------------------------------------------------
!       TODO surface integrand contributions
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!       eliminate connections to cell-centered data (basis functions of degree
!       greater than linear) and save the inverse of the interior block.
!-------------------------------------------------------------------------------
        IF (PRESENT(elim_matrix)) THEN
          IF (elim_matrix(ibl)%m%eliminated) THEN
            CALL vec%alloc_with_mold(temp_vec)
            temp_vec=vector(ibl)%v
            CALL elim_matrix(ibl)%m%elim_presolve(temp_vec,vec)
            CALL temp_vec%dealloc
          ENDIF
        ENDIF
      END ASSOCIATE
    ENDDO
!-------------------------------------------------------------------------------
!   network seams
!-------------------------------------------------------------------------------
    DO ibl=1,SIZE(bl)
      CALL vector(ibl)%v%edge_load_arr(seam%s(ibl))
    ENDDO
    CALL seam%edge_network
    DO ibl=1,SIZE(bl)
      CALL vector(ibl)%v%edge_unload_arr(seam%s(ibl))
    ENDDO
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE create_vector_real

!-------------------------------------------------------------------------------
!* call block-wise finite element computations to create a vector
!-------------------------------------------------------------------------------
  SUBROUTINE create_vector_comp(bl,vector,seam,integrand_func,dirichlet_bc,     &
                                bc_component,elim_matrix)
    USE matrix_mod
    USE pardata_mod
    USE vector_mod
    IMPLICIT NONE

    !> block storage
    TYPE(block_storage), INTENT(IN) :: bl(:)
    !> vector to create stored in array by block
    TYPE(cvec_storage), INTENT(INOUT) :: vector(:)
    !* seam
    TYPE(seam_type), INTENT(INOUT) :: seam
    !> integrand procedure to use
    PROCEDURE(integrand_vec_comp) :: integrand_func
    !> if true a dirichlet BC is applied to bc_component
    LOGICAL, INTENT(IN) :: dirichlet_bc
    !> components to use for dirichlet BC
    CHARACTER(*), INTENT(IN) :: bc_component
    !> matrix to use to eliminate interior DOFs stored in array by block
    TYPE(cmat_storage), OPTIONAL, INTENT(IN) :: elim_matrix(:,:)

    COMPLEX(r8), ALLOCATABLE :: int_arr(:,:,:,:)
    INTEGER(i4) :: ibl,imode
    CLASS(cvector), ALLOCATABLE :: temp_vec
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l1(mod_name,'create_vector_comp',iftn,idepth)
!-------------------------------------------------------------------------------
!   loop over blocks and integrate
!-------------------------------------------------------------------------------
    DO ibl=1,SIZE(bl)
      ASSOCIATE (vec=>vector(ibl)%v)
        ALLOCATE(int_arr(vec%nqty,vec%nel,vec%u_ndof,vec%nmodes))
        !$acc enter data create(int_arr) async(vec%id) if(vec%on_gpu)
        !$acc kernels present(int_arr) async(vec%id) if(vec%on_gpu)
        int_arr=0._r8
        !$acc end kernels
        CALL integrand_func(bl(ibl)%b,int_arr)
        CALL vec%zero
        CALL vec%assemble(int_arr)
        !$acc exit data delete(int_arr) finalize async(vec%id) if(vec%on_gpu)
        DEALLOCATE(int_arr)
!-------------------------------------------------------------------------------
!       call the regularity condition and the boundary condition routines.
!-------------------------------------------------------------------------------
        IF (bl(ibl)%b%r0block) CALL vec%regularity(seam%s(ibl),par%nindex,'all')
        IF (dirichlet_bc) CALL vec%dirichlet_bc(bc_component,seam%s(ibl))
!-------------------------------------------------------------------------------
!       TODO surface integrand contributions
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!       eliminate connections to cell-centered data (basis functions of degree
!       greater than linear) and save the inverse of the interior block.
!-------------------------------------------------------------------------------
        IF (PRESENT(elim_matrix)) THEN
          IF (elim_matrix(ibl,1)%m%eliminated) THEN
            CALL vec%alloc_with_mold(temp_vec)
            temp_vec=vector(ibl)%v
            DO imode=1,vec%nmodes
              CALL elim_matrix(ibl,imode)%m%elim_presolve(temp_vec,vec,imode)
            ENDDO
            CALL temp_vec%dealloc
          ENDIF
        ENDIF
      END ASSOCIATE
    ENDDO
!-------------------------------------------------------------------------------
!   network seams
!-------------------------------------------------------------------------------
    DO ibl=1,SIZE(bl)
      CALL vector(ibl)%v%edge_load_arr(seam%s(ibl))
    ENDDO
    CALL seam%edge_network
    DO ibl=1,SIZE(bl)
      CALL vector(ibl)%v%edge_unload_arr(seam%s(ibl))
    ENDDO
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE create_vector_comp

END MODULE finite_element_mod
