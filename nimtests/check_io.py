#!/usr/bin/env python3
import optparse
import os
import sys


def processFile(fileName):
    '''
    Check that write statements use par%nim_write and not nim_wr, out_unit or
    WRITE(*,*) directly.

    Recommend par%nim_write if not.
    '''
    infile = open(fileName, "r")
    lineNumber = 0
    errCount = 0

    for line in infile:
        lineNumber += 1
        if line.strip().lower().startswith(
                ('write(nim_wr', 'write(out_unit', 'write(*')):
            print('Check ' + fileName + ' line ' + str(lineNumber) + ':')
            print(line[0:-1])
            errCount += 1

    return errCount


def main():
    parser = optparse.OptionParser(usage='%prog [options] file.f')
    options, args = parser.parse_args()
    fileNames = []
    if len(args) == 1:
        fileNames.append(args[0])
    else:
        for root, dirs, files in os.walk("./", topdown=True):
            dirs[:] = [d for d in dirs
                       if not d.startswith('build')
                       if not d.startswith('extlibs')
                       if not d.startswith('nimtests')
                       and not d.startswith('public')]
            for file in files:
                if file.endswith(".f") and 'pardata' not in file:
                    fileNames.append(os.path.join(root, file))

    errCount = 0
    for fileName in fileNames:
        errCount += processFile(fileName)

    print('Found '+str(errCount)+' errors')
    if errCount > 0:
        print('Use par%nim_write() for stdout and output file I/O.')
    return errCount


if __name__ == "__main__":
    sys.exit(main())
