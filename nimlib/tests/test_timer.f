!-------------------------------------------------------------------------------
!! Test for timer.
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!* Test for timer.
!-------------------------------------------------------------------------------
PROGRAM test_timer
  USE local
  USE timer_mod
  USE nimtest_utils
  USE pardata_mod
  IMPLICIT NONE

  INTEGER(i4) :: iftn=0,idepth
  REAL(r8), PARAMETER :: tol=5.e-3_r8 ! allow 5ms errors
  CHARACTER(8) :: tag_list(3)

  CALL par%init
  CALL timer%init
  CALL timer%start_timer_l0('program','test timer program',iftn,idepth)
  CALL test_level0
  CALL test_level0_recurse
  CALL timer%end_timer_l0(iftn,idepth)
  CALL timer%report
  tag_list(1)='program'
  tag_list(2)='sub'
  tag_list(3)='kernel'
  CALL timer%report(tag_list)
#ifdef TIME_LEVEL1
#ifdef TIME_LEVEL2
!-------------------------------------------------------------------------------
!* Results including all levels
!-------------------------------------------------------------------------------
  CALL wrequal(timer%nfunc,6_i4,'timer nfunc')
  CALL wrequal(timer%ndepth,0_i4,'timer ndepth')
  CALL wrequal(timer%time_by_function_inclusive(1:6),                           &
               [5.e-2_r8,3.e-2_r8,5.e-2_r8,1.e-2_r8,2.e-2_r8,3.e-2_r8],         &
               'timer inclusive',tol)
  CALL wrequal(timer%time_by_function_exclusive(1:6),                           &
               [0._r8,0._r8,5.e-2_r8,0._r8,0._r8,0._r8],                        &
               'timer exclusive',tol)
  CALL wrequal(timer%calls_by_function(1:6),                                    &
               [1_i4,1_i4,8_i4,2_i4,1_i4,3_i4],'timer ndepth')
#else
!-------------------------------------------------------------------------------
!* Results including only levels 0 and 1
!-------------------------------------------------------------------------------
  CALL wrequal(timer%nfunc,5_i4,'timer nfunc')
  CALL wrequal(timer%ndepth,0_i4,'timer ndepth')
  CALL wrequal(timer%time_by_function_inclusive(1:5),                           &
               [5.e-2_r8,3.e-2_r8,1.e-2_r8,2.e-2_r8,3.e-2_r8],                  &
               'timer inclusive',tol)
  CALL wrequal(timer%time_by_function_exclusive(1:5),                           &
               [0._r8,2.e-2_r8,1.e-2_r8,5.e-3_r8,1.5e-2_r8],                    &
               'timer exclusive',tol)
  CALL wrequal(timer%calls_by_function(1:5),                                    &
               [1_i4,1_i4,2_i4,1_i4,3_i4],'timer ndepth')
#endif
#else
#ifdef TIME_LEVEL2
!-------------------------------------------------------------------------------
!* Results including only levels 0 and 2
!-------------------------------------------------------------------------------
  CALL wrequal(timer%nfunc,4_i4,'timer nfunc')
  CALL wrequal(timer%ndepth,0_i4,'timer ndepth')
  CALL wrequal(timer%time_by_function_inclusive(1:4),                           &
               [5.e-2_r8,3.e-2_r8,5.e-2_r8,2.e-2_r8],'timer inclusive',tol)
  CALL wrequal(timer%time_by_function_exclusive(1:4),                           &
               [0._r8,0._r8,5.e-2_r8,0._r8],'timer exclusive',tol)
  CALL wrequal(timer%calls_by_function(1:4),                                    &
               [1_i4,1_i4,8_i4,1_i4],'timer ndepth')
#else
!-------------------------------------------------------------------------------
!* Results including only level 0
!-------------------------------------------------------------------------------
  CALL wrequal(timer%nfunc,3_i4,'timer nfunc')
  CALL wrequal(timer%ndepth,0_i4,'timer ndepth')
  CALL wrequal(timer%time_by_function_inclusive(1:3),                           &
               [5.e-2_r8,3.e-2_r8,2.e-2_r8],'timer inclusive',tol)
  CALL wrequal(timer%time_by_function_exclusive(1:3),                           &
               [0._r8,3.e-2_r8,2.e-2_r8],'timer exclusive',tol)
  CALL wrequal(timer%calls_by_function(1:3),                                    &
               [1_i4,1_i4,1_i4],'timer ndepth')
#endif
#endif
  CALL timer%finalize
  CALL par%nim_stop('Normal termination.',clean_shutdown=.TRUE.)
CONTAINS

  SUBROUTINE busy_wait(wait_ms)
    IMPLICIT NONE

    !> wait time is ms
    REAL(r8), INTENT(IN) :: wait_ms

    REAL(r8) :: tstart,tend,ttarget
    INTEGER(i4) :: iftn=0,idepth

    CALL timer%start_timer_l2('kernel','busy_wait',iftn,idepth)
    CALL system_timer(tstart)
    ttarget=tstart+wait_ms/1000._r8
    tend=tstart
    DO WHILE(tend < ttarget)
      CALL system_timer(tend)
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE busy_wait

  SUBROUTINE test_level1
    IMPLICIT NONE

    INTEGER(i4) :: iftn=0,idepth

    CALL timer%start_timer_l1('sub','test_level1',iftn,idepth)
    CALL busy_wait(5._r8)
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE test_level1

  SUBROUTINE test_level0
    IMPLICIT NONE

    INTEGER(i4) :: iftn=0,idepth

    CALL timer%start_timer_l0('sub','test_level0',iftn,idepth)
    CALL busy_wait(10._r8)
    CALL test_level1
    CALL busy_wait(10._r8)
    CALL test_level1
    CALL timer%end_timer_l0(iftn,idepth)
  END SUBROUTINE test_level0

  RECURSIVE SUBROUTINE test_level1_recurse
    IMPLICIT NONE

    INTEGER(i4) :: iftn=0,idepth,icalls=0

    CALL timer%start_timer_l1('sub','test_level1_recurse',iftn,idepth)
    CALL busy_wait(5._r8)
    IF (icalls<2) THEN
      icalls=icalls+1
      CALL test_level1_recurse
    ENDIF
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE test_level1_recurse

  SUBROUTINE test_level0_recurse
    IMPLICIT NONE

    INTEGER(i4) :: iftn=0,idepth

    CALL timer%start_timer_l0('sub','test_level0_recurse',iftn,idepth)
    CALL busy_wait(5._r8)
    CALL test_level1_recurse
    CALL timer%end_timer_l0(iftn,idepth)
  END SUBROUTINE test_level0_recurse

END PROGRAM test_timer
