!-------------------------------------------------------------------------------
!< Defines data for i/o
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!* module containing data for i/o
!-------------------------------------------------------------------------------
MODULE io
  USE ISO_Fortran_env
  USE hdf5_api
  IMPLICIT NONE

  INTEGER, PARAMETER :: in_unit = 1          !! input file unit
  INTEGER, PARAMETER :: out_unit = 2         !! output file unit
  INTEGER, PARAMETER :: nim_rd = INPUT_UNIT  !! stdin unit for reads
  INTEGER, PARAMETER :: nim_wr = OUTPUT_UNIT !! stdout unit for writes
  INTEGER, PARAMETER :: nim_log = ERROR_UNIT !! stderr unit for writes
  CHARACTER(64) :: ofname="unset"            !! output file name
  LOGICAL :: ofopen = .FALSE.                !! true if output file is open
  TYPE(hdf5ErrorType) :: h5err               !! h5 error
  TYPE(hdf5InOpts), SAVE :: h5in             !! h5in
  INTEGER(HID_T) :: fileid                   !! h5 file identifier
  INTEGER(HID_T) :: rootgid                  !! h5 root directory id
CONTAINS

!-------------------------------------------------------------------------------
!* initialize fcio h5 information for writes and reads.
!-------------------------------------------------------------------------------
  SUBROUTINE fch5_init
    IMPLICIT NONE

    LOGICAL, SAVE :: h5init=.FALSE.

    IF (.NOT.h5init) THEN
      CALL vshdf5_fcinit
      CALL vshdf5_inith5vars(h5in, h5err)
      !h5in%comm=mpi_comm_world
      !h5in%info=mpi_info_null
      ! h5in%data_xfer_mode=H5FD_MPIO_COLLECTIVE_F does not work
      h5in%data_xfer_mode=H5FD_MPIO_INDEPENDENT_F
      h5in%verbose=.FALSE.
      h5in%debug=.FALSE.
      h5init=.TRUE.
    ENDIF
  END SUBROUTINE fch5_init

!-------------------------------------------------------------------------------
!* finalize fcio h5
!-------------------------------------------------------------------------------
  SUBROUTINE fch5_dealloc
    IMPLICIT NONE

    CALL vshdf5_fcdealloc
  END SUBROUTINE fch5_dealloc

END MODULE io
