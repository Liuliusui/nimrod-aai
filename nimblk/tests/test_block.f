!-------------------------------------------------------------------------------
!! Program to test block implementations
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!* Program to test block implementations
!-------------------------------------------------------------------------------
PROGRAM test_block
  USE block_registry_mod
  USE fem_registry_mod
  USE gblock_mod
  USE io
  USE local
  USE nodal_mod
  USE pardata_mod
  USE timer_mod
  IMPLICIT NONE

  TYPE(block_storage), ALLOCATABLE :: bl(:)
  TYPE(field_list_pointer), ALLOCATABLE :: io_field_list(:)
  CHARACTER(64) :: blk_test_types(SIZE(field_test_types))
  CLASS(nodal_field_comp), ALLOCATABLE :: cfield
  CLASS(nodal_field_real), ALLOCATABLE :: rfield
  REAL(r8), ALLOCATABLE :: rfield_dof(:,:)
  COMPLEX(r8), ALLOCATABLE :: cfield_dof(:,:,:)
  CHARACTER(64) :: test_name,test_type,cnmodes_total,cnlayers,filename
  INTEGER(i4) :: nmodes_total,im,ib,ifld,indx,nfield,nargs,itest
  INTEGER(i4) :: idepth
  INTEGER(i4), SAVE :: iftn=-1

!-------------------------------------------------------------------------------
! determine which test to run from the command line
!-------------------------------------------------------------------------------
  nargs=command_argument_count()
  IF (nargs/=2.AND.nargs/=4) THEN
    CALL print_usage
    CALL par%nim_stop('Argument error')
  ENDIF
  CALL get_command_argument(1,test_type)
  CALL get_command_argument(2,test_name)
  CALL par%init
  IF (nargs==4) THEN
    CALL get_command_argument(3,cnmodes_total)
    READ(cnmodes_total,'(i4)') nmodes_total
    CALL get_command_argument(4,cnlayers)
    READ(cnlayers,'(i4)') par%nlayers
  ELSE
    nmodes_total=3_i4*par%nprocs
    par%nlayers=par%nprocs
  ENDIF
  CALL fch5_init
  CALL timer%init
  CALL timer%start_timer_l0('test','test_gblock',iftn,idepth)
!-------------------------------------------------------------------------------
! setup registry based on test type
!-------------------------------------------------------------------------------
  CALL set_block_test_type(test_type)
  ALLOCATE (par%keff_total(nmodes_total))
  DO im=1,nmodes_total
    par%keff_total(im)=im
  ENDDO
  CALL par%set_decomp(nmodes_total,nbl*par%nprocs/par%nlayers)
!-------------------------------------------------------------------------------
! determine which fields types from the fem_registry
!-------------------------------------------------------------------------------
  nfield=0_i4
  DO ifld=1,SIZE(field_test_types)
    indx=INDEX(field_test_types(ifld),TRIM(block_field_id_string))
    IF (indx>0) THEN
      nfield=nfield+1_i4
      blk_test_types(nfield)=field_test_types(ifld)
    ENDIF
  ENDDO
  ALLOCATE(io_field_list(nfield))
  DO ifld=1,nfield
    io_field_list(ifld)%name=blk_test_types(ifld)
    indx=INDEX(blk_test_types(ifld),'comp')
    IF (indx>0) THEN
      io_field_list(ifld)%type="cfem_storage"
      ALLOCATE(cfem_storage::io_field_list(ifld)%p(nbl))
    ELSE
      io_field_list(ifld)%type="rfem_storage"
      ALLOCATE(rfem_storage::io_field_list(ifld)%p(nbl))
    ENDIF
  ENDDO
!-------------------------------------------------------------------------------
! allocate test blocks from registry
!-------------------------------------------------------------------------------
  ALLOCATE(bl(nbl))
  CALL setup_block(test_type,bl)
!-------------------------------------------------------------------------------
! set fields
!-------------------------------------------------------------------------------
  DO ib=1,nbl
    DO ifld=1,nfield
      SELECT TYPE (iofield=>io_field_list(ifld)%p)
      TYPE IS (cfem_storage)
        CALL setup_fem(blk_test_types(ifld),iofield(ib)%f,rfield,               &
                       bl(ib)%b%mesh,ib)
        ALLOCATE(cfield_dof(iofield(ib)%f%nqty,iofield(ib)%f%ndof,              &
                 iofield(ib)%f%nfour))
        cfield_dof=CMPLX(ifld*ib,-ifld*ib,r8)
        CALL iofield(ib)%f%set_field(cfield_dof)
        DEALLOCATE(cfield_dof)
      TYPE IS (rfem_storage)
        CALL setup_fem(blk_test_types(ifld),cfield,iofield(ib)%f,               &
                       bl(ib)%b%mesh,ib)
        ALLOCATE(rfield_dof(iofield(ib)%f%nqty,iofield(ib)%f%ndof))
        rfield_dof=REAL(ifld*ib,r8)
        CALL iofield(ib)%f%set_field(rfield_dof)
        DEALLOCATE(rfield_dof)
      END SELECT
    ENDDO
  ENDDO
!-------------------------------------------------------------------------------
! call test
!-------------------------------------------------------------------------------
  SELECT CASE(TRIM(test_name))
  CASE ("alloc_dealloc")
    ! do nothing
  CASE ("set")
    CALL test_block_set(bl)
  CASE ("h5io")
    CALL test_block_h5io(bl,TRIM(test_type)//"_test.h5",io_field_list)
  CASE ("dump")
    filename=TRIM(test_type)//"_test_"//TRIM(cnmodes_total)                     &
             //"_"//TRIM(cnlayers)//".h5"
    CALL test_dump(bl,TRIM(filename),io_field_list)
  CASE DEFAULT
    CALL par%nim_write('No test named '//TRIM(test_name))
    CALL print_usage
  END SELECT
!-------------------------------------------------------------------------------
! deallocate fields/blocks
!-------------------------------------------------------------------------------
  DO ifld=1,nfield
    DO ib=1,nbl
      SELECT TYPE (iofield=>io_field_list(ifld)%p)
      CLASS IS (rfem_storage)
        CALL iofield(ib)%f%dealloc
        DEALLOCATE(iofield(ib)%f)
      CLASS IS (cfem_storage)
        CALL iofield(ib)%f%dealloc
        DEALLOCATE(iofield(ib)%f)
      END SELECT
    ENDDO
    DEALLOCATE(io_field_list(ifld)%p)
  ENDDO
  DO ib=1,nbl
    CALL bl(ib)%b%dealloc
    DEALLOCATE(bl(ib)%b)
  ENDDO
  DEALLOCATE(io_field_list,bl)
  CALL timer%end_timer_l0(iftn,idepth)
  CALL timer%report
  CALL timer%finalize
  CALL fch5_dealloc
  CALL par%nim_stop('Normal termination.',clean_shutdown=.TRUE.)
CONTAINS

!-------------------------------------------------------------------------------
!* Helper routine to print usage
!-------------------------------------------------------------------------------
  SUBROUTINE print_usage
    CALL par%nim_write('Usage: <test program> <test name> <nmodes> <nlayers>')
    CALL par%nim_write('  where <test name> is one of')
    CALL par%nim_write('    alloc_dealloc')
    CALL par%nim_write('    set')
    CALL par%nim_write('    h5io')
    CALL par%nim_write('    dump')
    CALL par%nim_write('  where <test type> is one of')
    DO itest=1,SIZE(block_test_types)
      CALL par%nim_write('    '//TRIM(block_test_types(itest)))
    ENDDO
    CALL par%nim_write(                                                         &
      '  and <nmodes> and <nlayers> are optional and only appropriate')
    CALL par%nim_write('  for the dump test.')
  END SUBROUTINE print_usage

!-------------------------------------------------------------------------------
!* Unit test for block set functions
!-------------------------------------------------------------------------------
  SUBROUTINE test_block_set(bl)
    IMPLICIT NONE

    !> collection of blocks to test
    TYPE(block_storage), DIMENSION(:), INTENT(INOUT) :: bl

    INTEGER(i4) :: nblocks,ib,iarr
    INTEGER(i4), PARAMETER :: ngr_arr(8)=                                       &
      [2_i4,2_i4,2_i4,2_i4,4_i4,4_i4,4_i4,4_i4]
    CHARACTER(8), PARAMETER :: int_arr(8)=['gaussian','lobatto ',               &
      'gaussian','lobatto ','gaussian','lobatto ','gaussian','lobatto ']
    CHARACTER(8), PARAMETER :: sint_arr(8)=['gaussian','gaussian',              &
      'lobatto ','lobatto ','gaussian','gaussian','lobatto ','lobatto ']
    LOGICAL, PARAMETER :: torgeom_arr(8)=                                       &
      [.FALSE.,.TRUE.,.TRUE.,.FALSE.,.FALSE.,.TRUE.,.TRUE.,.FALSE.]

    nblocks = SIZE(bl)
!-------------------------------------------------------------------------------
!   just call the functions
!-------------------------------------------------------------------------------
    DO ib=1,nblocks
      iarr=MOD(ib-1,8)+1
      CALL bl(ib)%b%block_intg_formula_set(ngr_arr(iarr),int_arr(iarr),         &
                                           sint_arr(iarr))
      CALL bl(ib)%b%block_metric_set(torgeom_arr(iarr))
    ENDDO
  END SUBROUTINE test_block_set

!-------------------------------------------------------------------------------
!* Unit test for HDF5 I/O. It writes then reads and compares the results.
!-------------------------------------------------------------------------------
  SUBROUTINE test_block_h5io(bl,file_name,io_field_list)
    USE rblock_mod
    USE nimtest_utils
    IMPLICIT NONE

    !> collection of blocks to test
    TYPE(block_storage), INTENT(IN) :: bl(:)
    !> h5 file name
    CHARACTER(*), INTENT(IN) :: file_name
    !> I/O field list pointing to data to write to block
    TYPE(field_list_pointer), INTENT(IN) :: io_field_list(:)

    INTEGER(HID_T) :: blid
    LOGICAL :: file_stat
    CHARACTER(64) :: bl_type
    TYPE(block_storage), ALLOCATABLE :: bl_read(:)
    TYPE(field_list_pointer), ALLOCATABLE :: read_field_list(:)
    CHARACTER(4) :: idnm
    INTEGER(i4) :: nblocks,ib,ifld

    nblocks=SIZE(bl)
!-------------------------------------------------------------------------------
!   write block to a file
!-------------------------------------------------------------------------------
    CALL open_newh5file(TRIM(file_name),fileid,                                 &
                        "NIMROD test block file",rootgid,h5in,h5err)
    DO ib=1,nblocks
      CALL bl(ib)%b%h5_write(rootgid,io_field_list)
    ENDDO
    CALL close_h5file(fileid,rootgid,h5err)
!-------------------------------------------------------------------------------
!   setup read_field_list
!-------------------------------------------------------------------------------
    ALLOCATE(bl_read(nblocks))
    ALLOCATE(read_field_list(SIZE(io_field_list)))
    DO ifld=1,SIZE(io_field_list)
      read_field_list(ifld)%name=io_field_list(ifld)%name
      read_field_list(ifld)%type=io_field_list(ifld)%type
      IF (read_field_list(ifld)%type=="rfem_storage") THEN
        ALLOCATE(rfem_storage::read_field_list(ifld)%p(nblocks))
      ELSE ! cfem_storage
        ALLOCATE(cfem_storage::read_field_list(ifld)%p(nblocks))
      ENDIF
    ENDDO
!-------------------------------------------------------------------------------
!   check to see if the file exists. If so open it
!-------------------------------------------------------------------------------
    INQUIRE(FILE=TRIM(file_name),EXIST=file_stat)
    CALL wrtrue(file_stat,"gblock h5io file_stat")
!-------------------------------------------------------------------------------
!   open in the h5 file and get block type
!-------------------------------------------------------------------------------
    CALL open_oldh5file(TRIM(file_name),fileid,rootgid,h5in,h5err)
    DO ib=1,nblocks
      WRITE(idnm,fmt='(i4.4)') ib
      CALL open_group(rootgid,idnm,blid,h5err)
      CALL read_attribute(blid,"block_type",bl_type,h5in,h5err)
      CALL wrequal(bl(ib)%b%bl_type,bl_type,"gblock h5io bl_type")
      CALL close_group(idnm,blid,h5err)
!-------------------------------------------------------------------------------
!     Allocate a block of the correct type
!-------------------------------------------------------------------------------
      SELECT CASE (TRIM(bl_type(1:6)))
      CASE ("rblock")
        ALLOCATE(rblock::bl_read(ib)%b)
        CALL wrtrue(.TRUE.,"gblock h5io bl select")
      CASE DEFAULT
        CALL wrtrue(.FALSE.,"gblock h5io bl select")
      END SELECT
!-------------------------------------------------------------------------------
!     read in the block
!-------------------------------------------------------------------------------
      CALL bl_read(ib)%b%h5_read(ib,rootgid,read_field_list)
    ENDDO
!-------------------------------------------------------------------------------
!   close file
!-------------------------------------------------------------------------------
    CALL close_h5file(fileid,rootgid,h5err)
!-------------------------------------------------------------------------------
!   compare the results
!-------------------------------------------------------------------------------
    CALL compare_field_lists(io_field_list,read_field_list,"h5io")
    CALL compare_blocks(bl,bl_read,"h5io")
!-------------------------------------------------------------------------------
!   deallocate the read data after is has been tested
!-------------------------------------------------------------------------------
    DO ib=1,nblocks
      CALL bl_read(ib)%b%dealloc()
    ENDDO
    DO ifld=1,SIZE(read_field_list)
      DO ib=1,nblocks
        SELECT TYPE (readfield=>read_field_list(ifld)%p)
        CLASS IS (rfem_storage)
          CALL readfield(ib)%f%dealloc
        CLASS IS (cfem_storage)
          CALL readfield(ib)%f%dealloc
        END SELECT
      ENDDO
      DEALLOCATE(read_field_list(ifld)%p)
    ENDDO
    DEALLOCATE(read_field_list,bl_read)
  END SUBROUTINE test_block_h5io

!-------------------------------------------------------------------------------
!* Unit test for dump files. It reads and compares dump files.
!-------------------------------------------------------------------------------
  SUBROUTINE test_dump(bl,file_name,io_field_list)
    USE dump_mod
    USE nimtest_utils
    USE seam_mod
    USE simple_seam_mod
    IMPLICIT NONE

    !> collection of blocks to test
    TYPE(block_storage), INTENT(IN) :: bl(:)
    !> h5 file name
    CHARACTER(*), INTENT(IN) :: file_name
    !> I/O field list pointing to data to write to block
    TYPE(field_list_pointer), INTENT(IN) :: io_field_list(:)

    TYPE(seam_type) :: seam,seam_read
    TYPE(block_storage), ALLOCATABLE :: bl_read(:)
    TYPE(field_list_pointer) :: read_field_list(nfield)
    INTEGER(i4) :: dump_step,dump_step_read,nxbl,nybl
    INTEGER(i4), ALLOCATABLE :: mxbl_arr(:),mybl_arr(:)
    REAL(r8) :: dump_time,dump_time_read

    dump_time=25.5_r8
    dump_step=14_i4
!-------------------------------------------------------------------------------
!   setup a seam structure
!-------------------------------------------------------------------------------
    nxbl=nbl/2_i4
    nybl=2_i4*par%nprocs/par%nlayers
    ALLOCATE(mxbl_arr(nxbl),mybl_arr(nybl))
    mxbl_arr=2_i4
    mybl_arr=2_i4
    CALL seam_rect_alloc(seam,nxbl,nybl,mxbl_arr,mybl_arr,'none',.FALSE.)
    DEALLOCATE(mxbl_arr,mybl_arr)
    CALL seam%trim_to_local
!-------------------------------------------------------------------------------
!   setup read_field_list
!-------------------------------------------------------------------------------
    DO ifld=1,nfield
      read_field_list(ifld)%name=io_field_list(ifld)%name
      read_field_list(ifld)%type=io_field_list(ifld)%type
    ENDDO
!-------------------------------------------------------------------------------
!   call dump write and read
!-------------------------------------------------------------------------------
    CALL dump_write(dump_time,dump_step,bl,seam,io_field_list,file_name)
    CALL par%all_barrier
    CALL par%dealloc ! par%set_decomp is called in dump_read
    CALL dump_read(file_name,dump_time_read,dump_step_read,bl_read,seam_read,   &
                   read_field_list)
!-------------------------------------------------------------------------------
!   compare the results
!-------------------------------------------------------------------------------
    CALL compare_field_lists(io_field_list,read_field_list,"dump")
    CALL compare_blocks(bl,bl_read,"dump")
    CALL wrequal(dump_time,dump_time_read,"dump_time")
    CALL wrequal(dump_step,dump_step_read,"dump_step")
!-------------------------------------------------------------------------------
!   deallocate the read data after is has been tested
!-------------------------------------------------------------------------------
    CALL seam%dealloc
    CALL seam_read%dealloc
    DO ib=1,SIZE(bl_read)
      CALL bl_read(ib)%b%dealloc()
    ENDDO
    DO ifld=1,SIZE(read_field_list)
      DO ib=1,SIZE(bl_read)
        SELECT TYPE (readfield=>read_field_list(ifld)%p)
        CLASS IS (rfem_storage)
          CALL readfield(ib)%f%dealloc
        CLASS IS (cfem_storage)
          CALL readfield(ib)%f%dealloc
        END SELECT
      ENDDO
      DEALLOCATE(read_field_list(ifld)%p)
    ENDDO
    DEALLOCATE(bl_read)
  END SUBROUTINE test_dump

!-------------------------------------------------------------------------------
!* compare two field lists
!-------------------------------------------------------------------------------
  SUBROUTINE compare_field_lists(fld_list1,fld_list2,id_str)
    USE nimtest_utils
    IMPLICIT NONE

    !> field list 1 to compare
    TYPE(field_list_pointer), INTENT(IN) :: fld_list1(:)
    !> field list 2 to compare
    TYPE(field_list_pointer), INTENT(IN) :: fld_list2(:)
    !> string to use in test output
    CHARACTER(*), INTENT(IN) :: id_str

    INTEGER(i4) :: ib,ifld

    DO ib=1,SIZE(fld_list1(1)%p)
      DO ifld=1,SIZE(fld_list1)
        SELECT TYPE (iofield=>fld_list1(ifld)%p)
        CLASS IS (rfem_storage)
          SELECT TYPE (readfield=>fld_list2(ifld)%p)
          CLASS IS (rfem_storage)
            CALL compare_real_fld(iofield(ib)%f,readfield(ib)%f,id_str)
          CLASS DEFAULT
            CALL wrtrue(.FALSE.,TRIM(id_str)//" rfem incompatible field: "      &
                                //fld_list1(ifld)%name)
          END SELECT
        CLASS IS (cfem_storage)
          SELECT TYPE (readfield=>fld_list2(ifld)%p)
          CLASS IS (cfem_storage)
            CALL compare_comp_fld(iofield(ib)%f,readfield(ib)%f,id_str)
          CLASS DEFAULT
            CALL wrtrue(.FALSE.,TRIM(id_str)//" cfem incompatible field: "      &
                                //fld_list1(ifld)%name)
          END SELECT
        END SELECT
      ENDDO
    ENDDO
  END SUBROUTINE compare_field_lists

!-------------------------------------------------------------------------------
!* compare two blocks
!-------------------------------------------------------------------------------
  SUBROUTINE compare_blocks(bl1,bl2,id_str)
    USE nimtest_utils
    USE rblock_mod
    IMPLICIT NONE

    !> block 1 to compare
    TYPE(block_storage), DIMENSION(:), INTENT(IN) :: bl1
    !> block 2 to compare
    TYPE(block_storage), DIMENSION(:), INTENT(IN) :: bl2
    !> string to use in test output
    CHARACTER(*), INTENT(IN) :: id_str

    INTEGER(i4) :: ib

    DO ib=1,SIZE(bl1)
!-------------------------------------------------------------------------------
!     Test data common to all gblocks
!-------------------------------------------------------------------------------
      CALL wrequal(bl1(ib)%b%id,bl2(ib)%b%id,TRIM(id_str)//"gblock id")
      CALL wrequal(bl1(ib)%b%poly_degree,bl2(ib)%b%poly_degree,                 &
                   TRIM(id_str)//"gblock pd")
      CALL wrequal(bl1(ib)%b%nel,bl2(ib)%b%nel,TRIM(id_str)//"gblock nel")
      CALL wrequal(bl1(ib)%b%r0block,bl2(ib)%b%r0block,                         &
                   TRIM(id_str)//"gblock r0block")
!-------------------------------------------------------------------------------
!     test the mesh
!-------------------------------------------------------------------------------
      CALL compare_real_fld(bl1(ib)%b%mesh,bl2(ib)%b%mesh,id_str)
!-------------------------------------------------------------------------------
!     Test data specific to a child block type
!-------------------------------------------------------------------------------
      SELECT TYPE(blwrite=>bl1(ib)%b)
      CLASS IS (rblock)
        SELECT TYPE(blread=>bl2(ib)%b)
        CLASS IS (rblock)
          CALL wrequal(blwrite%mx,blread%mx,TRIM(id_str)//"gblock mx")
          CALL wrequal(blwrite%my,blread%my,TRIM(id_str)//"gblock my")
        CLASS DEFAULT
          CALL wrtrue(.FALSE.,                                                  &
                      TRIM(id_str)//"gblock bl and bl2 are different types")
        END SELECT
      CLASS DEFAULT
        CALL wrtrue(.FALSE.,TRIM(id_str)//"gblock bl type not recognized")
      END SELECT
    ENDDO
  END SUBROUTINE compare_blocks

!-------------------------------------------------------------------------------
!* compare two real fields
!-------------------------------------------------------------------------------
  SUBROUTINE compare_real_fld(fld1,fld2,id_str)
    USE nimtest_utils
    IMPLICIT NONE

    !> field 1 to compare
    CLASS(nodal_field_real), ALLOCATABLE, INTENT(IN) :: fld1
    !> field 2 to compare
    CLASS(nodal_field_real), ALLOCATABLE, INTENT(IN) :: fld2
    !> string to use in test output
    CHARACTER(*), INTENT(IN) :: id_str

    REAL(r8), ALLOCATABLE, DIMENSION(:,:) :: fv1,fv2
!-------------------------------------------------------------------------------
!   check to see if the fields have the same allocated status
!-------------------------------------------------------------------------------
    IF (ALLOCATED(fld1)) THEN
      CALL wrtrue(ALLOCATED(fld2),                                              &
                  TRIM(id_str)//" "//TRIM(fld1%name)//" associated")
      IF (ALLOCATED(fld2)) THEN
!-------------------------------------------------------------------------------
!       check the field generic meta-data and values
!-------------------------------------------------------------------------------
        CALL wrequal(fld1%pd,fld2%pd,TRIM(id_str)//" "//TRIM(fld1%name)//" pd")
        CALL wrequal(fld1%nqty,fld2%nqty,                                       &
                     TRIM(id_str)//" "//TRIM(fld1%name)//" nqty")
        CALL wrequal(fld1%ndof,fld2%ndof,                                       &
                     TRIM(id_str)//" "//TRIM(fld1%name)//" ndof")
        ALLOCATE(fv1(fld1%nqty,fld1%ndof))
        ALLOCATE(fv2(fld2%nqty,fld2%ndof))
        CALL fld1%get_field(fv1)
        CALL fld2%get_field(fv2)
        CALL wrequal(fv1,fv2,TRIM(id_str)//" "//TRIM(fld1%name)//" fval")
        DEALLOCATE(fv1,fv2)
      ENDIF
    ELSE
      CALL wrfalse(ALLOCATED(fld1),                                             &
                   TRIM(id_str)//" "//TRIM(fld1%name)//" associated")
    ENDIF
  END SUBROUTINE compare_real_fld

!-------------------------------------------------------------------------------
!* compare two complex fields
!-------------------------------------------------------------------------------
  SUBROUTINE compare_comp_fld(fld1,fld2,id_str)
    USE nimtest_utils
    IMPLICIT NONE

    !> field 1 to compare
    CLASS(nodal_field_comp), ALLOCATABLE, INTENT(INOUT) :: fld1
    !> field 2 to compare
    CLASS(nodal_field_comp), ALLOCATABLE, INTENT(INOUT) :: fld2
    !> string to use in test output
    CHARACTER(*), INTENT(IN) :: id_str

    COMPLEX(r8), ALLOCATABLE, DIMENSION(:,:,:) :: fv1,fv2
!-------------------------------------------------------------------------------
!   check to see if the fields have the same allocated status
!-------------------------------------------------------------------------------
    IF (ALLOCATED(fld1)) THEN
      CALL wrtrue(ALLOCATED(fld2),                                              &
                  TRIM(id_str)//" "//TRIM(fld1%name)//" associated")
      IF (ALLOCATED(fld2)) THEN
!-------------------------------------------------------------------------------
!       check the field generic meta-data and values
!-------------------------------------------------------------------------------
        CALL wrequal(fld1%pd,fld2%pd,TRIM(id_str)//" "//TRIM(fld1%name)//" pd")
        CALL wrequal(fld1%nfour,fld2%nfour,                                     &
                     TRIM(id_str)//" "//TRIM(fld1%name)//" nfour")
        CALL wrequal(fld1%nqty,fld2%nqty,                                       &
                     TRIM(id_str)//" "//TRIM(fld1%name)//" nqty")
        CALL wrequal(fld1%ndof,fld2%ndof,                                       &
                     TRIM(id_str)//" "//TRIM(fld1%name)//" ndof")
        ALLOCATE(fv1(fld1%nqty,fld1%ndof,fld1%nfour))
        ALLOCATE(fv2(fld2%nqty,fld2%ndof,fld2%nfour))
        CALL fld1%get_field(fv1)
        CALL fld2%get_field(fv2)
        CALL wrequal(fv1,fv2,TRIM(id_str)//" "//TRIM(fld1%name)//" fval")
        DEALLOCATE(fv1,fv2)
      ENDIF
    ELSE
      CALL wrfalse(ALLOCATED(fld1),                                             &
                   TRIM(id_str)//" "//TRIM(fld1%name)//" associated")
    ENDIF
  END SUBROUTINE compare_comp_fld

END PROGRAM test_block
