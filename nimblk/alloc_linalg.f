!-------------------------------------------------------------------------------
!! Defines routines to allocate linear algebra spaces from FEM spaces
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* Defines routines to allocate linear algebra spaces from FEM spaces
!-------------------------------------------------------------------------------
MODULE alloc_linalg_mod
  USE h1_rect_2D_mod
  USE local
  USE matrix_mod
  USE mat_rect_2D_mod
  USE mat_rect_2D_mod_acc
  USE nodal_mod
  USE pardata_mod
  USE timer_mod
  USE vector_mod
  USE vec_rect_2D_mod
  USE vec_rect_2D_mod_acc
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: alloc_vector_for_fem,alloc_matrix_for_fem

  INTERFACE alloc_vector_for_fem
    MODULE PROCEDURE alloc_rvector_for_1fem,alloc_rvector_for_2fem,             &
                     alloc_cvector1m_for_1fem,alloc_cvector1m_for_2fem,         &
                     alloc_cvector_for_1fem,alloc_cvector_for_2fem
  END INTERFACE alloc_vector_for_fem

  INTERFACE alloc_matrix_for_fem
    MODULE PROCEDURE alloc_rmatrix_for_1fem,alloc_rmatrix_for_2fem,             &
                     alloc_cmatrix_for_1fem,alloc_cmatrix_for_2fem
  END INTERFACE alloc_matrix_for_fem

  CHARACTER(*), PARAMETER :: mod_name='alloc_linalg'
CONTAINS

!-------------------------------------------------------------------------------
!* call nim_stop with a failure message
!-------------------------------------------------------------------------------
  SUBROUTINE fail_at_vector
    IMPLICIT NONE

    CALL par%nim_stop('alloc_vector_for_fem: '//                                &
                      'Could not allocate vector from fem_space')
  END SUBROUTINE

!-------------------------------------------------------------------------------
!* allocate a vector from one FEM space
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_rvector_for_1fem(new_vec,fem_space)
    IMPLICIT NONE

    !> vector space to create
    CLASS(rvector), ALLOCATABLE, INTENT(INOUT) :: new_vec
    !> FEM space to use as basis for vector space
    CLASS(nodal_field_real), INTENT(IN) :: fem_space

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_rvector_for_1fem',iftn,idepth)
    SELECT TYPE (fem_space)
    TYPE IS (h1_rect_2D_real)
      ALLOCATE(vec_rect_2D_real::new_vec)
      SELECT TYPE (new_vec)
      TYPE IS (vec_rect_2D_real)
        CALL new_vec%alloc(fem_space%pd,fem_space%mx,fem_space%my,              &
                           fem_space%nqty,fem_space%id)
      END SELECT
    TYPE IS (h1_rect_2D_real_acc)
      ALLOCATE(vec_rect_2D_real_acc::new_vec)
      SELECT TYPE (new_vec)
      TYPE IS (vec_rect_2D_real_acc)
        CALL new_vec%alloc(fem_space%pd,fem_space%mx,fem_space%my,              &
                           fem_space%nqty,fem_space%id,fem_space%on_gpu)
      END SELECT
    CLASS DEFAULT
      CALL fail_at_vector
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_rvector_for_1fem

!-------------------------------------------------------------------------------
!> allocate a vector from two FEM spaces;
!  acceptable combinations (order matters):
!  - h1+h1
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_rvector_for_2fem(new_vec,fem_space1,fem_space2)
    IMPLICIT NONE

    !> vector space to create
    CLASS(rvector), ALLOCATABLE, INTENT(INOUT) :: new_vec
    !> FEM spaces to use as basis for vector space
    CLASS(nodal_field_real), INTENT(IN) :: fem_space1,fem_space2

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_rvector_for_2fem',iftn,idepth)
    SELECT TYPE (fem_space1)
    TYPE IS (h1_rect_2D_real)
      ALLOCATE(vec_rect_2D_real::new_vec)
      SELECT TYPE (new_vec)
      TYPE IS (vec_rect_2D_real)
        SELECT TYPE (fem_space2)
        TYPE IS (h1_rect_2D_real)
#ifdef DEBUG
          IF (fem_space1%pd/=fem_space2%pd.OR.fem_space1%mx/=fem_space2%mx.OR.  &
              fem_space1%my/=fem_space2%my)                                     &
            CALL par%nim_stop('alloc_rvector_for_2fem:'//                       &
                              ' incompatible fem spaces')
#endif
          CALL new_vec%alloc(fem_space1%pd,fem_space1%mx,fem_space1%my,         &
                             fem_space1%nqty+fem_space2%nqty,fem_space1%id)
        CLASS DEFAULT
          CALL fail_at_vector
        END SELECT
      END SELECT
    TYPE IS (h1_rect_2D_real_acc)
      ALLOCATE(vec_rect_2D_real_acc::new_vec)
      SELECT TYPE (new_vec)
      TYPE IS (vec_rect_2D_real_acc)
        SELECT TYPE (fem_space2)
        TYPE IS (h1_rect_2D_real_acc)
#ifdef DEBUG
          IF (fem_space1%pd/=fem_space2%pd.OR.fem_space1%mx/=fem_space2%mx.OR.  &
              fem_space1%my/=fem_space2%my)                                     &
            CALL par%nim_stop('alloc_rvector_for_2fem:'//                       &
                              ' incompatible fem spaces')
#endif
          CALL new_vec%alloc(fem_space1%pd,fem_space1%mx,fem_space1%my,         &
                             fem_space1%nqty+fem_space2%nqty,fem_space1%id,     &
                             fem_space1%on_gpu)
        CLASS DEFAULT
          CALL fail_at_vector
        END SELECT
      END SELECT
    CLASS DEFAULT
      CALL fail_at_vector
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_rvector_for_2fem

!-------------------------------------------------------------------------------
!* allocate a vector from one FEM space
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_cvector1m_for_1fem(new_vec,fem_space)
    IMPLICIT NONE

    !> vector space to create
    CLASS(cvector1m), ALLOCATABLE, INTENT(INOUT) :: new_vec
    !> FEM space to use as basis for vector space
    CLASS(nodal_field_comp), INTENT(IN) :: fem_space

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_cvector1m_for_1fem',iftn,idepth)
    SELECT TYPE (fem_space)
    TYPE IS (h1_rect_2D_comp)
      ALLOCATE(vec_rect_2D_cv1m::new_vec)
      SELECT TYPE (new_vec)
      TYPE IS (vec_rect_2D_cv1m)
        CALL new_vec%alloc(fem_space%pd,fem_space%mx,fem_space%my,              &
                           fem_space%nqty,fem_space%id)
      END SELECT
    TYPE IS (h1_rect_2D_comp_acc)
      ALLOCATE(vec_rect_2D_cv1m_acc::new_vec)
      SELECT TYPE (new_vec)
      TYPE IS (vec_rect_2D_cv1m_acc)
        CALL new_vec%alloc(fem_space%pd,fem_space%mx,fem_space%my,              &
                           fem_space%nqty,fem_space%id,fem_space%on_gpu)
      END SELECT
    CLASS DEFAULT
      CALL fail_at_vector
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_cvector1m_for_1fem

!-------------------------------------------------------------------------------
!> allocate a vector from two FEM spaces;
!  acceptable combinations (order matters):
!  - h1+h1
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_cvector1m_for_2fem(new_vec,fem_space1,fem_space2)
    IMPLICIT NONE

    !> vector space to create
    CLASS(cvector1m), ALLOCATABLE, INTENT(INOUT) :: new_vec
    !> FEM spaces to use as basis for vector space
    CLASS(nodal_field_comp), INTENT(IN) :: fem_space1,fem_space2

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_cvector1m_for_2fem',iftn,idepth)
    SELECT TYPE (fem_space1)
    TYPE IS (h1_rect_2D_comp)
      ALLOCATE(vec_rect_2D_cv1m::new_vec)
      SELECT TYPE (new_vec)
      TYPE IS (vec_rect_2D_cv1m)
        SELECT TYPE (fem_space2)
        TYPE IS (h1_rect_2D_comp)
#ifdef DEBUG
          IF (fem_space1%pd/=fem_space2%pd.OR.fem_space1%mx/=fem_space2%mx.OR.  &
              fem_space1%my/=fem_space2%my)                                     &
            CALL par%nim_stop('alloc_cvector1m_for_2fem:'//                     &
                              ' incompatible fem spaces')
          IF (fem_space1%nqty/=1_i4)                                            &
            CALL par%nim_stop('alloc_cvector1m_for_2fem:'//                     &
                              ' fem_space has more than one mode')
#endif
          CALL new_vec%alloc(fem_space1%pd,fem_space1%mx,fem_space1%my,         &
                             fem_space1%nqty+fem_space2%nqty,fem_space1%id)
        CLASS DEFAULT
          CALL fail_at_vector
        END SELECT
      END SELECT
    TYPE IS (h1_rect_2D_comp_acc)
      ALLOCATE(vec_rect_2D_cv1m_acc::new_vec)
      SELECT TYPE (new_vec)
      TYPE IS (vec_rect_2D_cv1m_acc)
        SELECT TYPE (fem_space2)
        TYPE IS (h1_rect_2D_comp_acc)
#ifdef DEBUG
          IF (fem_space1%pd/=fem_space2%pd.OR.fem_space1%mx/=fem_space2%mx.OR.  &
              fem_space1%my/=fem_space2%my)                                     &
            CALL par%nim_stop('alloc_rvector_for_2fem:'//                       &
                              ' incompatible fem spaces')
#endif
          CALL new_vec%alloc(fem_space1%pd,fem_space1%mx,fem_space1%my,         &
                             fem_space1%nqty+fem_space2%nqty,fem_space1%id,     &
                             fem_space1%on_gpu)
        CLASS DEFAULT
          CALL fail_at_vector
        END SELECT
      END SELECT
    CLASS DEFAULT
      CALL fail_at_vector
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_cvector1m_for_2fem

!-------------------------------------------------------------------------------
!* allocate a vector from one FEM space
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_cvector_for_1fem(new_vec,fem_space)
    IMPLICIT NONE

    !> vector space to create
    CLASS(cvector), ALLOCATABLE, INTENT(INOUT) :: new_vec
    !> FEM space to use as basis for vector space
    CLASS(nodal_field_comp), INTENT(IN) :: fem_space

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_cvector_for_1fem',iftn,idepth)
    SELECT TYPE (fem_space)
    TYPE IS (h1_rect_2D_comp)
      ALLOCATE(vec_rect_2D_comp::new_vec)
      SELECT TYPE (new_vec)
      TYPE IS (vec_rect_2D_comp)
        CALL new_vec%alloc(fem_space%pd,fem_space%mx,fem_space%my,              &
                           fem_space%nqty,fem_space%nfour,fem_space%id)
      END SELECT
    TYPE IS (h1_rect_2D_comp_acc)
      ALLOCATE(vec_rect_2D_comp_acc::new_vec)
      SELECT TYPE (new_vec)
      TYPE IS (vec_rect_2D_comp_acc)
        CALL new_vec%alloc(fem_space%pd,fem_space%mx,fem_space%my,              &
                           fem_space%nqty,fem_space%nfour,fem_space%id,         &
                           fem_space%on_gpu)
      END SELECT
    CLASS DEFAULT
      CALL fail_at_vector
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_cvector_for_1fem

!-------------------------------------------------------------------------------
!> allocate a vector from two FEM spaces;
!  acceptable combinations (order matters):
!  - h1+h1
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_cvector_for_2fem(new_vec,fem_space1,fem_space2)
    IMPLICIT NONE

    !> vector space to create
    CLASS(cvector), ALLOCATABLE, INTENT(INOUT) :: new_vec
    !> FEM spaces to use as basis for vector space
    CLASS(nodal_field_comp), INTENT(IN) :: fem_space1,fem_space2

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_cvector_for_2fem',iftn,idepth)
    SELECT TYPE (fem_space1)
    TYPE IS (h1_rect_2D_comp)
      ALLOCATE(vec_rect_2D_comp::new_vec)
      SELECT TYPE (new_vec)
      TYPE IS (vec_rect_2D_comp)
        SELECT TYPE (fem_space2)
        TYPE IS (h1_rect_2D_comp)
#ifdef DEBUG
          IF (fem_space1%pd/=fem_space2%pd.OR.fem_space1%mx/=fem_space2%mx.OR.  &
              fem_space1%my/=fem_space2%my.OR.                                  &
              fem_space1%nfour/=fem_space2%nfour)                               &
            CALL par%nim_stop('alloc_cvector_for_2fem:'//                       &
                              ' incompatible fem spaces')
#endif
          CALL new_vec%alloc(fem_space1%pd,fem_space1%mx,fem_space1%my,         &
                             fem_space1%nqty+fem_space2%nqty,fem_space1%nfour,  &
                             fem_space1%id)
        CLASS DEFAULT
          CALL fail_at_vector
        END SELECT
      END SELECT
    TYPE IS (h1_rect_2D_comp_acc)
      ALLOCATE(vec_rect_2D_comp_acc::new_vec)
      SELECT TYPE (new_vec)
      TYPE IS (vec_rect_2D_comp_acc)
        SELECT TYPE (fem_space2)
        TYPE IS (h1_rect_2D_comp_acc)
#ifdef DEBUG
          IF (fem_space1%pd/=fem_space2%pd.OR.fem_space1%mx/=fem_space2%mx.OR.  &
              fem_space1%my/=fem_space2%my.OR.                                  &
              fem_space1%nfour/=fem_space2%nfour)                               &
            CALL par%nim_stop('alloc_rvector_for_2fem:'//                       &
                              ' incompatible fem spaces')
#endif
          CALL new_vec%alloc(fem_space1%pd,fem_space1%mx,fem_space1%my,         &
                             fem_space1%nqty+fem_space2%nqty,fem_space1%nfour,  &
                             fem_space1%id,fem_space1%on_gpu)
        CLASS DEFAULT
          CALL fail_at_vector
        END SELECT
      END SELECT
    CLASS DEFAULT
      CALL fail_at_vector
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_cvector_for_2fem

!-------------------------------------------------------------------------------
!* call nim_stop with a failure message
!-------------------------------------------------------------------------------
  SUBROUTINE fail_at_matrix
    IMPLICIT NONE

    CALL par%nim_stop('alloc_matrix_for_fem: '//                                &
                      'Could not allocate matrix from fem_space')
  END SUBROUTINE

!-------------------------------------------------------------------------------
!* allocate a matrix from one FEM space
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_rmatrix_for_1fem(new_mat,fem_space,vcomp)
    IMPLICIT NONE

    !> matrix to create
    CLASS(rmatrix), ALLOCATABLE, INTENT(INOUT) :: new_mat
    !> FEM space to use as basis for matrix
    CLASS(nodal_field_real), INTENT(IN) :: fem_space
    !> component names, e.g. 's' for scalar or 'rzp' for a vector
    CHARACTER(1), INTENT(IN) :: vcomp(fem_space%nqty)

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_rmatrix_for_1fem',iftn,idepth)
    SELECT TYPE (fem_space)
    TYPE IS (h1_rect_2D_real)
      ALLOCATE(mat_rect_2D_real::new_mat)
      SELECT TYPE (new_mat)
      TYPE IS (mat_rect_2D_real)
        CALL new_mat%alloc(fem_space%pd,fem_space%mx,fem_space%my,              &
                           fem_space%nqty,fem_space%id,vcomp)
      END SELECT
    TYPE IS (h1_rect_2D_real_acc)
      ALLOCATE(mat_rect_2D_real_acc::new_mat)
      SELECT TYPE (new_mat)
      TYPE IS (mat_rect_2D_real_acc)
        CALL new_mat%alloc(fem_space%pd,fem_space%mx,fem_space%my,              &
                           fem_space%nqty,fem_space%id,fem_space%on_gpu,vcomp)
      END SELECT
    CLASS DEFAULT
      CALL fail_at_matrix
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_rmatrix_for_1fem

!-------------------------------------------------------------------------------
!> allocate a matrix from two FEM spaces;
!  acceptable combinations (order matters):
!  - h1+h1
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_rmatrix_for_2fem(new_mat,fem_space1,fem_space2,vcomp)
    IMPLICIT NONE

    !> matrix to create
    CLASS(rmatrix), ALLOCATABLE, INTENT(INOUT) :: new_mat
    !> FEM spaces to use as basis for matrix
    CLASS(nodal_field_real), INTENT(IN) :: fem_space1,fem_space2
    !> component names, e.g. 's' for scalar or 'rzp' for a vector
    CHARACTER(1), INTENT(IN) :: vcomp(fem_space1%nqty+fem_space2%nqty)

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_rmatrix_for_2fem',iftn,idepth)
    SELECT TYPE (fem_space1)
    TYPE IS (h1_rect_2D_real)
      ALLOCATE(mat_rect_2D_real::new_mat)
      SELECT TYPE (new_mat)
      TYPE IS (mat_rect_2D_real)
        SELECT TYPE (fem_space2)
        TYPE IS (h1_rect_2D_real)
          CALL new_mat%alloc(fem_space1%pd,fem_space1%mx,fem_space1%my,         &
                             fem_space1%nqty+fem_space2%nqty,                   &
                             fem_space1%id,vcomp)
        CLASS DEFAULT
          CALL fail_at_matrix
        END SELECT
      END SELECT
    TYPE IS (h1_rect_2D_real_acc)
      ALLOCATE(mat_rect_2D_real_acc::new_mat)
      SELECT TYPE (new_mat)
      TYPE IS (mat_rect_2D_real_acc)
        SELECT TYPE (fem_space2)
        TYPE IS (h1_rect_2D_real_acc)
          CALL new_mat%alloc(fem_space1%pd,fem_space1%mx,fem_space1%my,         &
                             fem_space1%nqty+fem_space2%nqty,                   &
                             fem_space1%id,fem_space1%on_gpu,vcomp)
        CLASS DEFAULT
          CALL fail_at_matrix
        END SELECT
      END SELECT
    CLASS DEFAULT
      CALL fail_at_matrix
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_rmatrix_for_2fem

!-------------------------------------------------------------------------------
!* allocate a matrix from one FEM space
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_cmatrix_for_1fem(new_mat,fem_space,vcomp,fcomp)
    IMPLICIT NONE

    !> matrix to create
    CLASS(cmatrix), ALLOCATABLE, INTENT(INOUT) :: new_mat
    !> FEM space to use as basis for matrix
    CLASS(nodal_field_comp), INTENT(IN) :: fem_space
    !> component names, e.g. 's' for scalar or 'rzp' for a vector
    CHARACTER(1), INTENT(IN) :: vcomp(fem_space%nqty)
    !> Fourier index associated with matrix
    INTEGER(i4), INTENT(IN) :: fcomp

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_cmatrix_for_1fem',iftn,idepth)
    SELECT TYPE (fem_space)
    TYPE IS (h1_rect_2D_comp)
      ALLOCATE(mat_rect_2D_cm1m::new_mat)
      SELECT TYPE (new_mat)
      TYPE IS (mat_rect_2D_cm1m)
        CALL new_mat%alloc(fem_space%pd,fem_space%mx,fem_space%my,              &
                           fem_space%nqty,fem_space%id,vcomp)
      END SELECT
    TYPE IS (h1_rect_2D_comp_acc)
      ALLOCATE(mat_rect_2D_cm1m_acc::new_mat)
      SELECT TYPE (new_mat)
      TYPE IS (mat_rect_2D_cm1m_acc)
        CALL new_mat%alloc(fem_space%pd,fem_space%mx,fem_space%my,              &
                           fem_space%nqty,fem_space%id,fem_space%on_gpu,vcomp)
      END SELECT
    CLASS DEFAULT
      CALL fail_at_matrix
    END SELECT
    new_mat%fcomp=fcomp
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_cmatrix_for_1fem

!-------------------------------------------------------------------------------
!> allocate a matrix from two FEM spaces;
!  acceptable combinations (order matters):
!  - h1+h1
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_cmatrix_for_2fem(new_mat,fem_space1,fem_space2,vcomp,fcomp)
    IMPLICIT NONE

    !> matrix to create
    CLASS(cmatrix), ALLOCATABLE, INTENT(INOUT) :: new_mat
    !> FEM spaces to use as basis for matrix
    CLASS(nodal_field_comp), INTENT(IN) :: fem_space1,fem_space2
    !> component names, e.g. 's' for scalar or 'rzp' for a vector
    CHARACTER(1), INTENT(IN) :: vcomp(fem_space1%nqty+fem_space2%nqty)
    !> Fourier index associated with matrix
    INTEGER(i4), INTENT(IN) :: fcomp

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_cmatrix_for_2fem',iftn,idepth)
    SELECT TYPE (fem_space1)
    TYPE IS (h1_rect_2D_comp)
      ALLOCATE(mat_rect_2D_cm1m::new_mat)
      SELECT TYPE (new_mat)
      TYPE IS (mat_rect_2D_cm1m)
        SELECT TYPE (fem_space2)
        TYPE IS (h1_rect_2D_comp)
          CALL new_mat%alloc(fem_space1%pd,fem_space1%mx,fem_space1%my,         &
                             fem_space1%nqty+fem_space2%nqty,                   &
                             fem_space1%id,vcomp)
        CLASS DEFAULT
          CALL fail_at_matrix
        END SELECT
      END SELECT
    TYPE IS (h1_rect_2D_comp_acc)
      ALLOCATE(mat_rect_2D_cm1m_acc::new_mat)
      SELECT TYPE (new_mat)
      TYPE IS (mat_rect_2D_cm1m_acc)
        SELECT TYPE (fem_space2)
        TYPE IS (h1_rect_2D_comp_acc)
          CALL new_mat%alloc(fem_space1%pd,fem_space1%mx,fem_space1%my,         &
                             fem_space1%nqty+fem_space2%nqty,                   &
                             fem_space1%id,fem_space1%on_gpu,vcomp)
        CLASS DEFAULT
          CALL fail_at_matrix
        END SELECT
      END SELECT
    CLASS DEFAULT
      CALL fail_at_matrix
    END SELECT
    new_mat%fcomp=fcomp
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_cmatrix_for_2fem

END MODULE alloc_linalg_mod
