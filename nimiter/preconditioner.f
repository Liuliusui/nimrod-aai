!-------------------------------------------------------------------------------
!! defines the abstract interfaces to preconditioners
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* defines the abstract interfaces to preconditioners
!-------------------------------------------------------------------------------
MODULE preconditioner_mod
  USE local
  USE matrix_mod
  USE vector_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: precon_real,precon_func_real

  PUBLIC :: precon_cv1m,precon_func_cv1m

  PUBLIC :: precon_comp,precon_func_comp

  ABSTRACT INTERFACE
!-------------------------------------------------------------------------------
!*  custom function signature for call during preconditioning
!-------------------------------------------------------------------------------
    SUBROUTINE precon_func_real(mat,res,zee,adr)
      USE matrix_mod
      USE vector_mod
      !* matrix M to be used for preconditioning
      CLASS(rmat_storage), DIMENSION(:), INTENT(IN) :: mat
      !* the residual res = rhs - A x_k
      TYPE(rvec_storage), DIMENSION(:), INTENT(IN) :: res
      !* the preconditioned residual, zee = M^-1 res
      TYPE(rvec_storage), DIMENSION(:), INTENT(INOUT) :: zee
      !* the full matrix vector product, A x_k
      TYPE(rvec_storage), DIMENSION(:), INTENT(IN) :: adr
    END SUBROUTINE precon_func_real
  END INTERFACE

  ABSTRACT INTERFACE
!-------------------------------------------------------------------------------
!*  custom function signature for call during preconditioning
!-------------------------------------------------------------------------------
    SUBROUTINE precon_func_cv1m(mat,res,zee,adr)
      USE matrix_mod
      USE vector_mod
      !* matrix M to be used for preconditioning
      CLASS(cmat_storage), DIMENSION(:), INTENT(IN) :: mat
      !* the residual res = rhs - A x_k
      TYPE(cv1m_storage), DIMENSION(:), INTENT(IN) :: res
      !* the preconditioned residual, zee = M^-1 res
      TYPE(cv1m_storage), DIMENSION(:), INTENT(INOUT) :: zee
      !* the full matrix vector product, A x_k
      TYPE(cv1m_storage), DIMENSION(:), INTENT(IN) :: adr
    END SUBROUTINE precon_func_cv1m
  END INTERFACE

  ABSTRACT INTERFACE
!-------------------------------------------------------------------------------
!*  custom function signature for call during preconditioning
!-------------------------------------------------------------------------------
    SUBROUTINE precon_func_comp(mat,res,zee,adr)
      USE matrix_mod
      USE vector_mod
      !* matrix M to be used for preconditioning
      CLASS(cmat_storage), DIMENSION(:,:), INTENT(IN) :: mat
      !* the residual res = rhs - A x_k
      TYPE(cvec_storage), DIMENSION(:), INTENT(IN) :: res
      !* the preconditioned residual, zee = M^-1 res
      TYPE(cvec_storage), DIMENSION(:), INTENT(INOUT) :: zee
      !* the full matrix vector product, A x_k
      TYPE(cvec_storage), DIMENSION(:), INTENT(IN) :: adr
    END SUBROUTINE precon_func_comp
  END INTERFACE

!-------------------------------------------------------------------------------
!> Abstract base type that defines implementation requirements for a
!  preconditioner for real types
!-------------------------------------------------------------------------------
  TYPE, ABSTRACT :: precon_real
!-------------------------------------------------------------------------------
!   Input
!-------------------------------------------------------------------------------
    !* custom function to call before preconditioning (e.g. to save state)
    PROCEDURE(precon_func_real), POINTER, NOPASS :: f_precon_init
    !* custom function to call after preconditioning (e.g. for BC/regularity)
    PROCEDURE(precon_func_real), POINTER, NOPASS :: f_precon_finalize
!-------------------------------------------------------------------------------
!   Diagnostic output
!-------------------------------------------------------------------------------
    !* return code (for external packages)
    INTEGER(i4) :: return_code
    !* error message
    CHARACTER(128) :: error_msg
  CONTAINS

    ! Deferred routines
    PROCEDURE(dealloc_real), PASS(precon), DEFERRED :: dealloc
    PROCEDURE(update_real), PASS(precon), DEFERRED :: update
    PROCEDURE(apply_precon_real), PASS(precon), DEFERRED :: apply_precon
    ! Defined routines
    PROCEDURE, PASS(precon) :: apply => apply_real
  END TYPE precon_real

!-------------------------------------------------------------------------------
!> Abstract base type that defines implementation requirements for a
!  preconditioner for complex types with a single Fourier mode
!-------------------------------------------------------------------------------
  TYPE, ABSTRACT :: precon_cv1m
!-------------------------------------------------------------------------------
!   Input
!-------------------------------------------------------------------------------
    !* custom function to call before preconditioning (e.g. to save state)
    PROCEDURE(precon_func_cv1m), POINTER, NOPASS :: f_precon_init
    !* custom function to call after preconditioning (e.g. for BC/regularity)
    PROCEDURE(precon_func_cv1m), POINTER, NOPASS :: f_precon_finalize
!-------------------------------------------------------------------------------
!   Diagnostic output
!-------------------------------------------------------------------------------
    !* return code (for external packages)
    INTEGER(i4) :: return_code
    !* error message
    CHARACTER(128) :: error_msg
  CONTAINS

    ! Deferred routines
    PROCEDURE(dealloc_cv1m), PASS(precon), DEFERRED :: dealloc
    PROCEDURE(update_cv1m), PASS(precon), DEFERRED :: update
    PROCEDURE(apply_precon_cv1m), PASS(precon), DEFERRED :: apply_precon
    ! Defined routines
    PROCEDURE, PASS(precon) :: apply => apply_cv1m
  END TYPE precon_cv1m

!-------------------------------------------------------------------------------
!> Abstract base type that defines implementation requirements for a
!  preconditioner for complex types with multiple Fourier mode
!-------------------------------------------------------------------------------
  TYPE, ABSTRACT :: precon_comp
!-------------------------------------------------------------------------------
!   Input
!-------------------------------------------------------------------------------
    !* custom function to call before preconditioning (e.g. to save state)
    PROCEDURE(precon_func_comp), POINTER, NOPASS :: f_precon_init
    !* custom function to call after preconditioning (e.g. for BC/regularity)
    PROCEDURE(precon_func_comp), POINTER, NOPASS :: f_precon_finalize
!-------------------------------------------------------------------------------
!   Diagnostic output
!-------------------------------------------------------------------------------
    !* return code (for external packages)
    INTEGER(i4) :: return_code
    !* error message
    CHARACTER(128) :: error_msg
  CONTAINS

    ! Deferred routines
    PROCEDURE(dealloc_comp), PASS(precon), DEFERRED :: dealloc
    PROCEDURE(update_comp), PASS(precon), DEFERRED :: update
    PROCEDURE(apply_precon_comp), PASS(precon), DEFERRED :: apply_precon
    ! Defined routines
    PROCEDURE, PASS(precon) :: apply => apply_comp
  END TYPE precon_comp

  ABSTRACT INTERFACE
!-------------------------------------------------------------------------------
!*  Deallocate the preconditioner
!-------------------------------------------------------------------------------
    SUBROUTINE dealloc_real(precon)
      IMPORT precon_real
      !> preconditioner to dealloc
      CLASS(precon_real), INTENT(INOUT) :: precon
    END SUBROUTINE dealloc_real

!-------------------------------------------------------------------------------
!*  Update the preconditioner after the matrix update
!-------------------------------------------------------------------------------
    SUBROUTINE update_real(precon,mat,seam)
      USE matrix_mod
      USE seam_mod
      IMPORT precon_real
      !> preconditioner to update
      CLASS(precon_real), INTENT(INOUT) :: precon
      !> matrix M to be used for preconditioning
      CLASS(rmat_storage), DIMENSION(:), INTENT(IN) :: mat
      !> associated seam
      TYPE(seam_type), INTENT(INOUT) :: seam
    END SUBROUTINE update_real

!-------------------------------------------------------------------------------
!*  Call the core preconditioner (note: use the apply interface directly)
!-------------------------------------------------------------------------------
    SUBROUTINE apply_precon_real(precon,mat,res,zee,adr)
      USE matrix_mod
      USE vector_mod
      IMPORT precon_real
      !> preconditioner to apply
      CLASS(precon_real), INTENT(INOUT) :: precon
      !> matrix M to be used for preconditioning
      CLASS(rmat_storage), DIMENSION(:), INTENT(IN) :: mat
      !> the residual res = rhs - A x_k
      TYPE(rvec_storage), DIMENSION(:), INTENT(IN) :: res
      !> the preconditioned residual, zee = M^-1 res
      TYPE(rvec_storage), DIMENSION(:), INTENT(INOUT) :: zee
      !> the full matrix vector product, A x_k
      TYPE(rvec_storage), DIMENSION(:), INTENT(IN) :: adr
    END SUBROUTINE apply_precon_real

!-------------------------------------------------------------------------------
!*  Deallocate the preconditioner
!-------------------------------------------------------------------------------
    SUBROUTINE dealloc_cv1m(precon)
      IMPORT precon_cv1m
      !> preconditioner to dealloc
      CLASS(precon_cv1m), INTENT(INOUT) :: precon
    END SUBROUTINE dealloc_cv1m

!-------------------------------------------------------------------------------
!*  Update the preconditioner after the matrix update
!-------------------------------------------------------------------------------
    SUBROUTINE update_cv1m(precon,mat,seam)
      USE matrix_mod
      USE seam_mod
      IMPORT precon_cv1m
      !> preconditioner to update
      CLASS(precon_cv1m), INTENT(INOUT) :: precon
      !> matrix M to be used for preconditioning
      CLASS(cmat_storage), DIMENSION(:), INTENT(IN) :: mat
      !> associated seam
      TYPE(seam_type), INTENT(INOUT) :: seam
    END SUBROUTINE update_cv1m

!-------------------------------------------------------------------------------
!*  Call the core preconditioner (note: use the apply interface directly)
!-------------------------------------------------------------------------------
    SUBROUTINE apply_precon_cv1m(precon,mat,res,zee,adr)
      USE matrix_mod
      USE vector_mod
      IMPORT precon_cv1m
      !> preconditioner to apply
      CLASS(precon_cv1m), INTENT(INOUT) :: precon
      !> matrix M to be used for preconditioning
      CLASS(cmat_storage), DIMENSION(:), INTENT(IN) :: mat
      !> the residual res = rhs - A x_k
      TYPE(cv1m_storage), DIMENSION(:), INTENT(IN) :: res
      !> the preconditioned residual, zee = M^-1 res
      TYPE(cv1m_storage), DIMENSION(:), INTENT(INOUT) :: zee
      !> the full matrix vector product, A x_k
      TYPE(cv1m_storage), DIMENSION(:), INTENT(IN) :: adr
    END SUBROUTINE apply_precon_cv1m

!-------------------------------------------------------------------------------
!*  Deallocate the preconditioner
!-------------------------------------------------------------------------------
    SUBROUTINE dealloc_comp(precon)
      IMPORT precon_comp
      !> preconditioner to dealloc
      CLASS(precon_comp), INTENT(INOUT) :: precon
    END SUBROUTINE dealloc_comp

!-------------------------------------------------------------------------------
!*  Update the preconditioner after the matrix update
!-------------------------------------------------------------------------------
    SUBROUTINE update_comp(precon,mat,seam)
      USE matrix_mod
      USE seam_mod
      IMPORT precon_comp
      !> preconditioner to update
      CLASS(precon_comp), INTENT(INOUT) :: precon
      !> matrix M to be used for preconditioning
      CLASS(cmat_storage), DIMENSION(:,:), INTENT(IN) :: mat
      !> associated seam
      TYPE(seam_type), INTENT(INOUT) :: seam
    END SUBROUTINE update_comp

!-------------------------------------------------------------------------------
!*  Call the core preconditioner (note: use the apply interface directly)
!-------------------------------------------------------------------------------
    SUBROUTINE apply_precon_comp(precon,mat,res,zee,adr)
      USE matrix_mod
      USE vector_mod
      IMPORT precon_comp
      !> preconditioner to apply
      CLASS(precon_comp), INTENT(INOUT) :: precon
      !> matrix M to be used for preconditioning
      CLASS(cmat_storage), DIMENSION(:,:), INTENT(IN) :: mat
      !> the residual res = rhs - A x_k
      TYPE(cvec_storage), DIMENSION(:), INTENT(IN) :: res
      !> the preconditioned residual, zee = M^-1 res
      TYPE(cvec_storage), DIMENSION(:), INTENT(INOUT) :: zee
      !> the full matrix vector product, A x_k
      TYPE(cvec_storage), DIMENSION(:), INTENT(IN) :: adr
    END SUBROUTINE apply_precon_comp
  END INTERFACE

CONTAINS

!-------------------------------------------------------------------------------
!>  Call the core preconditioner (note: use the apply interface directly)
!-------------------------------------------------------------------------------
  SUBROUTINE apply_real(precon,mat,res,zee,adr)
    USE matrix_mod
    USE vector_mod
    IMPLICIT NONE

    !> preconditioner to apply
    CLASS(precon_real), INTENT(INOUT) :: precon
    !> matrix M to be used for preconditioning
    CLASS(rmat_storage), DIMENSION(:), INTENT(IN) :: mat
    !> the residual res = rhs - A x_k
    TYPE(rvec_storage), DIMENSION(:), INTENT(IN) :: res
    !> the preconditioned residual, zee = M^-1 res
    TYPE(rvec_storage), DIMENSION(:), INTENT(INOUT) :: zee
    !> the full matrix vector product, A x_k
    TYPE(rvec_storage), DIMENSION(:), INTENT(IN) :: adr

!-------------------------------------------------------------------------------
!   call init
!-------------------------------------------------------------------------------
    IF (ASSOCIATED(precon%f_precon_init))                                       &
      CALL precon%f_precon_init(mat,res,zee,adr)
!-------------------------------------------------------------------------------
!   call apply
!-------------------------------------------------------------------------------
    CALL precon%apply_precon(mat,res,zee,adr)
!-------------------------------------------------------------------------------
!   call finalize
!-------------------------------------------------------------------------------
    IF (ASSOCIATED(precon%f_precon_finalize))                                   &
      CALL precon%f_precon_finalize(mat,res,zee,adr)
  END SUBROUTINE apply_real

!-------------------------------------------------------------------------------
!>  Call the core preconditioner (note: use the apply interface directly)
!-------------------------------------------------------------------------------
  SUBROUTINE apply_cv1m(precon,mat,res,zee,adr)
    USE matrix_mod
    USE vector_mod
    IMPLICIT NONE

    !> preconditioner to apply
    CLASS(precon_cv1m), INTENT(INOUT) :: precon
    !> matrix M to be used for preconditioning
    CLASS(cmat_storage), DIMENSION(:), INTENT(IN) :: mat
    !> the residual res = rhs - A x_k
    TYPE(cv1m_storage), DIMENSION(:), INTENT(IN) :: res
    !> the preconditioned residual, zee = M^-1 res
    TYPE(cv1m_storage), DIMENSION(:), INTENT(INOUT) :: zee
    !> the full matrix vector product, A x_k
    TYPE(cv1m_storage), DIMENSION(:), INTENT(IN) :: adr

!-------------------------------------------------------------------------------
!   call init
!-------------------------------------------------------------------------------
    IF (ASSOCIATED(precon%f_precon_init))                                       &
      CALL precon%f_precon_init(mat,res,zee,adr)
!-------------------------------------------------------------------------------
!   call apply
!-------------------------------------------------------------------------------
    CALL precon%apply_precon(mat,res,zee,adr)
!-------------------------------------------------------------------------------
!   call finalize
!-------------------------------------------------------------------------------
    IF (ASSOCIATED(precon%f_precon_finalize))                                   &
      CALL precon%f_precon_finalize(mat,res,zee,adr)
  END SUBROUTINE apply_cv1m

!-------------------------------------------------------------------------------
!>  Call the core preconditioner (note: use the apply interface directly)
!-------------------------------------------------------------------------------
  SUBROUTINE apply_comp(precon,mat,res,zee,adr)
    USE matrix_mod
    USE vector_mod
    IMPLICIT NONE

    !> preconditioner to apply
    CLASS(precon_comp), INTENT(INOUT) :: precon
    !> matrix M to be used for preconditioning
    CLASS(cmat_storage), DIMENSION(:,:), INTENT(IN) :: mat
    !> the residual res = rhs - A x_k
    TYPE(cvec_storage), DIMENSION(:), INTENT(IN) :: res
    !> the preconditioned residual, zee = M^-1 res
    TYPE(cvec_storage), DIMENSION(:), INTENT(INOUT) :: zee
    !> the full matrix vector product, A x_k
    TYPE(cvec_storage), DIMENSION(:), INTENT(IN) :: adr

!-------------------------------------------------------------------------------
!   call init
!-------------------------------------------------------------------------------
    IF (ASSOCIATED(precon%f_precon_init))                                       &
      CALL precon%f_precon_init(mat,res,zee,adr)
!-------------------------------------------------------------------------------
!   call apply
!-------------------------------------------------------------------------------
    CALL precon%apply_precon(mat,res,zee,adr)
!-------------------------------------------------------------------------------
!   call finalize
!-------------------------------------------------------------------------------
    IF (ASSOCIATED(precon%f_precon_finalize))                                   &
      CALL precon%f_precon_finalize(mat,res,zee,adr)
  END SUBROUTINE apply_comp

END MODULE preconditioner_mod
