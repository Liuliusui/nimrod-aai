!-------------------------------------------------------------------------------
!< mathematical operations and coordinate transformations
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> module containing mathematical operations and coordinate transformations.
!-------------------------------------------------------------------------------
MODULE math_tran
  USE local
  IMPLICIT NONE

CONTAINS

!-------------------------------------------------------------------------------
!* invert a 2D array of 2x2 matrices.
!-------------------------------------------------------------------------------
  SUBROUTINE math_inv_2x2(out11,out12,out21,out22,in11,in12,in21,in22)
    IMPLICIT NONE

    !* output array elements
    REAL(r8), DIMENSION(:,:), INTENT(OUT) :: out11,out12,out21,out22
    !* input array elements
    REAL(r8), DIMENSION(:,:), INTENT(IN) :: in11,in12,in21,in22

    REAL(r8), DIMENSION(SIZE(out11,1),SIZE(out11,2)) :: det
!-------------------------------------------------------------------------------
!   find determinant.
!-------------------------------------------------------------------------------
    det=in11*in22-in12*in21
!-------------------------------------------------------------------------------
!   invert in-matrix.
!-------------------------------------------------------------------------------
    out11= in22/det
    out12=-in12/det
    out21=-in21/det
    out22= in11/det
    RETURN
  END SUBROUTINE math_inv_2x2

!-------------------------------------------------------------------------------
!* use lu factorization to solve a 3x3 system.
!-------------------------------------------------------------------------------
  SUBROUTINE math_solve_3x3(matrix,xx,bb,instruction)
    IMPLICIT NONE

    !* if instruction is factor, place lu factors in matrix
    REAL(r8), DIMENSION(:,:,:,:), INTENT(INOUT) :: matrix
    !* vector on which to apply inverse matrix if instruction is solve
    REAL(r8), DIMENSION(:,:,:), INTENT(IN) :: bb
    !* $x = matrix^-1 b$ if instruction is solve
    REAL(r8), DIMENSION(:,:,:), INTENT(OUT) :: xx
    !* factor or solve system, factor must be called before solve
    CHARACTER(6), INTENT(IN) :: instruction

    REAL(r8), DIMENSION(SIZE(matrix,1),SIZE(matrix,2)) :: y2,y3,                &
      lu21,lu22,lu23,lu31,lu32,lu33
!-------------------------------------------------------------------------------
!   factor matrix into L*U (first row is identical to matrix).
!-------------------------------------------------------------------------------
    IF (instruction(1:5)/='solve') THEN
      lu21 = matrix(:,:,2,1)/matrix(:,:,1,1)
      lu31 = matrix(:,:,3,1)/matrix(:,:,1,1)
      lu22 = matrix(:,:,2,2)-lu21*matrix(:,:,1,2)
      lu23 = matrix(:,:,2,3)-lu21*matrix(:,:,1,3)
      lu32 =(matrix(:,:,3,2)-lu31*matrix(:,:,1,2))/lu22
      lu33 = matrix(:,:,3,3)-lu31*matrix(:,:,1,3)-lu32*lu23
    ENDIF
    IF (instruction=='factor') THEN
      matrix(:,:,2,1)=lu21
      matrix(:,:,3,1)=lu31
      matrix(:,:,2,2)=lu22
      matrix(:,:,3,2)=lu32
      matrix(:,:,2,3)=lu23
      matrix(:,:,3,3)=lu33
      RETURN
    ENDIF
!-------------------------------------------------------------------------------
!   solve for xx by getting y first.
!   {Ly=bb then Uxx=y}  Note that L has ones on the diagonal.
!-------------------------------------------------------------------------------
    IF (instruction(1:5)=='solve') THEN
      lu21=matrix(:,:,2,1)
      lu31=matrix(:,:,3,1)
      lu22=matrix(:,:,2,2)
      lu32=matrix(:,:,3,2)
      lu23=matrix(:,:,2,3)
      lu33=matrix(:,:,3,3)
    ENDIF
    y2 = bb(:,:,2)-lu21*bb(:,:,1)
    y3 = bb(:,:,3)-lu31*bb(:,:,1)-lu32*y2
    xx(:,:,3) = y3/lu33
    xx(:,:,2) =(y2-lu23*xx(:,:,3))/lu22
    xx(:,:,1) =(bb(:,:,1)-matrix(:,:,1,2)*xx(:,:,2)                             &
                         -matrix(:,:,1,3)*xx(:,:,3))/matrix(:,:,1,1)
    RETURN
  END SUBROUTINE math_solve_3x3

!-------------------------------------------------------------------------------
!> use lu factorization to solve a nxn system.
!
!  note that L has ones on the diagonal.  if the instruction is 'factor', the
!  decomposition is returned in matrix.  if the instruction is 'solve', the
!  incoming matrix is expected to be the decomposition.  if the instruction is
!  'both', matrix is not changed, but the decomposition is not saved after the
!  solution vector is found.
!-------------------------------------------------------------------------------
  SUBROUTINE math_solve_nxn(n,matrix,xx,bb,instruction)
    IMPLICIT NONE

    !* number of row/columns in each matrix
    INTEGER(i4), INTENT(IN) :: n
    !* matrix to factor or solve
    REAL(r8), DIMENSION(:,:,:,:), INTENT(INOUT) :: matrix
    !* vector on which to apply inverse matrix if instruction is solve
    REAL(r8), DIMENSION(:,:,:), INTENT(IN) :: bb
    !* $x = matrix^-1 b$ if instruction is solve
    REAL(r8), DIMENSION(:,:,:), INTENT(OUT) :: xx
    !* factor or solve system, factor must be called before solve
    CHARACTER(*), INTENT(IN) :: instruction

    REAL(r8), DIMENSION(SIZE(matrix,1),SIZE(matrix,2),n) :: y
    REAL(r8), DIMENSION(SIZE(matrix,1),SIZE(matrix,2),n,n) :: lu
    INTEGER(i4) :: isub,j
!-------------------------------------------------------------------------------
!   factor matrix into L*U (first row and column are treated first).
!-------------------------------------------------------------------------------
    IF (instruction/='solve') THEN
      lu(:,:,1,:)=matrix(:,:,1,:)
      DO j=2,n
        lu(:,:,j,1)=matrix(:,:,j,1)/lu(:,:,1,1)
      ENDDO
!-------------------------------------------------------------------------------
!     submatrix loop: for each find the row of L then the column of U.
!-------------------------------------------------------------------------------
      DO isub=2,n
        DO j=2,isub-1
          lu(:,:,isub,j)=(matrix(:,:,isub,j)                                    &
                         -SUM(lu(:,:,isub,1:j-1)*lu(:,:,1:j-1,j),3))/lu(:,:,j,j)
        ENDDO
        DO j=2,isub
          lu(:,:,j,isub)=matrix(:,:,j,isub)                                     &
                         -SUM(lu(:,:,j,1:j-1)*lu(:,:,1:j-1,isub),3)
        ENDDO
      ENDDO
    ENDIF
!-------------------------------------------------------------------------------
!   copy matrix and return if solution is not required.
!-------------------------------------------------------------------------------
    IF (instruction=='factor') THEN
      matrix=lu
      RETURN
    ENDIF
!-------------------------------------------------------------------------------
!   copy LU decomposition if already factored.
!-------------------------------------------------------------------------------
    IF (instruction=='solve') THEN
      lu=matrix
    ENDIF
!-------------------------------------------------------------------------------
!   solve for xx by getting y first.  {Ly=bb then Uxx=y}
!-------------------------------------------------------------------------------
    y(:,:,1)=bb(:,:,1)
    DO j=2,n
      y(:,:,j)=bb(:,:,j)-SUM(lu(:,:,j,1:j-1)*y(:,:,1:j-1),3)
    ENDDO
    xx(:,:,n)=y(:,:,n)/lu(:,:,n,n)
    DO j=n-1,1,-1
      xx(:,:,j)=(y(:,:,j)-SUM(lu(:,:,j,j+1:n)*xx(:,:,j+1:n),3))/lu(:,:,j,j)
    ENDDO
    RETURN
  END SUBROUTINE math_solve_nxn

!-------------------------------------------------------------------------------
!> use lu factorization to solve real, batched nxn systems.
!
!  note that L has ones on the diagonal.  if the instruction is 'factor', the
!  decomposition is returned in matrix.  if the instruction is 'solve', the
!  incoming matrix is expected to be the decomposition.  if the instruction is
!  'both', matrix is not changed, but the decomposition is not saved after the
!  solution vector is found.
!-------------------------------------------------------------------------------
  SUBROUTINE math_solve_q1_nxn(n,na,matrix,xx,bb,instruction,singular)
    IMPLICIT NONE

    !* number of row/columns in each matrix
    INTEGER(i4), INTENT(IN) :: n
    !* number of matrices
    INTEGER(i4), INTENT(IN) :: na
    !* matrix to factor or solve
    REAL(r8), DIMENSION(n,n,*), INTENT(INOUT) :: matrix
    !* vector on which to apply inverse matrix if instruction is solve
    REAL(r8), DIMENSION(n,*), INTENT(IN) :: bb
    !* $x = matrix^-1 b$ if instruction is solve
    REAL(r8), DIMENSION(n,*), INTENT(OUT) :: xx
    !* factor or solve system, factor must be called before solve
    CHARACTER(*), INTENT(IN) :: instruction
    !* returns true if matrix is singular
    LOGICAL, INTENT(OUT) :: singular

    REAL(r8), DIMENSION(n) :: y
    REAL(r8), DIMENSION(n,n) :: lu
    INTEGER(i4) :: isub,i,ia
!-------------------------------------------------------------------------------
!   factor matrix into L*U (first row and column are treated first).
!-------------------------------------------------------------------------------
    singular=.false.
    DO ia=1,na
      IF (instruction/='solve') THEN
        lu=matrix(:,:,ia)
        IF (lu(1,1)==0) THEN
          singular=.true.
          RETURN
        ENDIF
        DO i=2,n
          lu(1,i)=lu(1,i)/lu(1,1)
        ENDDO
!-------------------------------------------------------------------------------
!       submatrix loop: for each find the row of L then the column of U.
!-------------------------------------------------------------------------------
        DO isub=2,n
          DO i=2,isub-1
            lu(i,isub)=(lu(i,isub)-SUM(lu(1:i-1,isub)*lu(i,1:i-1)))/lu(i,i)
          ENDDO
          DO i=2,isub
            lu(isub,i)=lu(isub,i)-SUM(lu(1:i-1,i)*lu(isub,1:i-1))
          ENDDO
          IF (lu(isub,isub)==0) THEN
            singular=.true.
            RETURN
          ENDIF
        ENDDO
      ENDIF
!-------------------------------------------------------------------------------
!     copy matrix and return if solution is not required.
!-------------------------------------------------------------------------------
      IF (instruction=='factor') THEN
        matrix(:,:,ia)=lu
        CYCLE
      ENDIF
!-------------------------------------------------------------------------------
!     copy LU decomposition if already factored.
!-------------------------------------------------------------------------------
      IF (instruction=='solve') THEN
        lu=matrix(:,:,ia)
      ENDIF
!-------------------------------------------------------------------------------
!     solve for xx by getting y first.  {Ly=bb then Uxx=y}
!-------------------------------------------------------------------------------
      y(1)=bb(1,ia)
      DO i=2,n
        y(i)=bb(i,ia)-SUM(lu(1:i-1,i)*y(1:i-1))
      ENDDO
      xx(n,ia)=y(n)/lu(n,n)
      DO i=n-1,1,-1
        xx(i,ia)=(y(i)-SUM(lu(i+1:n,i)*xx(i+1:n,ia)))/lu(i,i)
      ENDDO
    ENDDO
    RETURN
  END SUBROUTINE math_solve_q1_nxn

!-------------------------------------------------------------------------------
!> use lu factorization to solve complex, batched nxn systems.
!
!  note that L has ones on the diagonal.  if the instruction is 'factor', the
!  decomposition is returned in matrix.  if the instruction is 'solve', the
!  incoming matrix is expected to be the decomposition.  if the instruction is
!  'both', matrix is not changed, but the decomposition is not saved after the
!  solution vector is found.
!-------------------------------------------------------------------------------
  SUBROUTINE math_solve_q1_cnxn(n,na,matrix,xx,bb,instruction,singular)
    IMPLICIT NONE

    !* number of row/columns in each matrix
    INTEGER(i4), INTENT(IN) :: n
    !* number of matrices
    INTEGER(i4), INTENT(IN) :: na
    !* matrix to factor or solve
    COMPLEX(r8), DIMENSION(n,n,*), INTENT(INOUT) :: matrix
    !* vector on which to apply inverse matrix if instruction is solve
    COMPLEX(r8), DIMENSION(n,*), INTENT(IN) :: bb
    !* $x = matrix^-1 b$ if instruction is solve
    COMPLEX(r8), DIMENSION(n,*), INTENT(OUT) :: xx
    !* factor or solve system, factor must be called before solve
    CHARACTER(*), INTENT(IN) :: instruction
    !* returns true if matrix is singular
    LOGICAL, INTENT(OUT) :: singular

    COMPLEX(r8), DIMENSION(n) :: y
    COMPLEX(r8), DIMENSION(n,n) :: lu
    INTEGER(i4) :: isub,i,ia
!-------------------------------------------------------------------------------
!   factor matrix into L*U (first row and column are treated first).
!-------------------------------------------------------------------------------
    singular=.false.
    DO ia=1,na
      IF (instruction/='solve') THEN
        lu=matrix(:,:,ia)
        IF (lu(1,1)==0) THEN
          singular=.true.
          RETURN
        ENDIF
        DO i=2,n
          lu(1,i)=lu(1,i)/lu(1,1)
        ENDDO
!-------------------------------------------------------------------------------
!       submatrix loop: for each find the row of L then the column
!       of U.
!-------------------------------------------------------------------------------
        DO isub=2,n
          DO i=2,isub-1
            lu(i,isub)=(lu(i,isub)-SUM(lu(1:i-1,isub)*lu(i,1:i-1)))/lu(i,i)
          ENDDO
          DO i=2,isub
            lu(isub,i)=lu(isub,i)-SUM(lu(1:i-1,i)*lu(isub,1:i-1))
          ENDDO
          IF (lu(isub,isub)==0) THEN
            singular=.true.
            RETURN
          ENDIF
        ENDDO
      ENDIF
!-------------------------------------------------------------------------------
!     copy matrix and return if solution is not required.
!-------------------------------------------------------------------------------
      IF (instruction=='factor') THEN
        matrix(:,:,ia)=lu
        CYCLE
      ENDIF
!-------------------------------------------------------------------------------
!     copy LU decomposition if already factored.
!-------------------------------------------------------------------------------
      IF (instruction=='solve') THEN
        lu=matrix(:,:,ia)
      ENDIF
!-------------------------------------------------------------------------------
!     solve for xx by getting y first.  {Ly=bb then Uxx=y}
!-------------------------------------------------------------------------------
      y(1)=bb(1,ia)
      DO i=2,n
        y(i)=bb(i,ia)-SUM(lu(1:i-1,i)*y(1:i-1))
      ENDDO
      xx(n,ia)=y(n)/lu(n,n)
      DO i=n-1,1,-1
        xx(i,ia)=(y(i)-SUM(lu(i+1:n,i)*xx(i+1:n,ia)))/lu(i,i)
      ENDDO
    ENDDO
    RETURN
  END SUBROUTINE math_solve_q1_cnxn

!-------------------------------------------------------------------------------
!> use Cholesky factorization to solve a symmetric nxn system.
!
!  if the instruction is 'factor', the lower triangular part of the
!  decomposition is returned in matrix.  if the instruction is
!  'solve', the incoming matrix is expected to be the decomposition.
!  if the instruction is 'both', matrix is not changed, but the
!  decomposition is not saved after the solution vector is found.
!-------------------------------------------------------------------------------
  SUBROUTINE math_solve_sym(n,matrix,xx,bb,instruction,singular)
    IMPLICIT NONE

    !* number of row/columns in each matrix
    INTEGER(i4), INTENT(IN) :: n
    !* matrix to factor or solve
    REAL(r8), DIMENSION(:,:,:,:), INTENT(INOUT) :: matrix
    !* vector on which to apply inverse matrix if instruction is solve
    REAL(r8), DIMENSION(:,:,:), INTENT(IN) :: bb
    !* $x = matrix^-1 b$ if instruction is solve
    REAL(r8), DIMENSION(:,:,:), INTENT(OUT) :: xx
    !* factor or solve system, factor must be called before solve
    CHARACTER(*), INTENT(IN) :: instruction
    !* returns true if matrix is singular
    LOGICAL, INTENT(OUT) :: singular

    REAL(r8), DIMENSION(SIZE(matrix,1),SIZE(matrix,2),n) :: y
    REAL(r8), DIMENSION(SIZE(matrix,1),SIZE(matrix,2),n,n) :: lu
    INTEGER(i4) :: jcol,i
!-------------------------------------------------------------------------------
!   factor matrix into L*transpose(L) (first column is treated first).
!   if a singularity is encountered, return the pivot in the iq=jq=1
!   position of matrix.
!-------------------------------------------------------------------------------
    singular=.false.
    IF (instruction/='solve') THEN
      lu=0
      IF (MINVAL(matrix(:,:,1,1))<=0) THEN
        singular=.true.
        RETURN
      ENDIF
      lu(:,:,1,1)=SQRT(matrix(:,:,1,1))
      DO i=2,n
        lu(:,:,i,1)=matrix(:,:,i,1)/lu(:,:,1,1)
      ENDDO
!-------------------------------------------------------------------------------
!     column loop for L.
!-------------------------------------------------------------------------------
      DO jcol=2,n
        lu(:,:,jcol,jcol)=matrix(:,:,jcol,jcol)                                 &
                          -SUM(lu(:,:,jcol,1:jcol-1)*lu(:,:,jcol,1:jcol-1),3)
        IF (MINVAL(lu(:,:,jcol,jcol))<=0) THEN
          singular=.true.
          matrix(:,:,1,1)=lu(:,:,jcol,jcol)
          RETURN
        ENDIF
        lu(:,:,jcol,jcol)=SQRT(lu(:,:,jcol,jcol))
        DO i=jcol+1,n
          lu(:,:,i,jcol)=(matrix(:,:,i,jcol)                                    &
             -SUM(lu(:,:,i,1:jcol-1)*lu(:,:,jcol,1:jcol-1),3))/lu(:,:,jcol,jcol)
        ENDDO
      ENDDO
    ENDIF
!-------------------------------------------------------------------------------
!   copy matrix and return if solution is not required.
!-------------------------------------------------------------------------------
    IF (instruction=='factor') THEN
      matrix=lu
      RETURN
    ENDIF
!-------------------------------------------------------------------------------
!   copy decomposition if already factored.
!-------------------------------------------------------------------------------
    IF (instruction=='solve') THEN
      lu=matrix
    ENDIF
!-------------------------------------------------------------------------------
!   solve for xx by getting y first.  {Ly=bb then transpose(L)xx=y}
!-------------------------------------------------------------------------------
    y(:,:,1)=bb(:,:,1)/lu(:,:,1,1)
    DO i=2,n
      y(:,:,i)=(bb(:,:,i)-SUM(lu(:,:,i,1:i-1)*y(:,:,1:i-1),3))/lu(:,:,i,i)
    ENDDO
    xx(:,:,n)=y(:,:,n)/lu(:,:,n,n)
    DO i=n-1,1,-1
      xx(:,:,i)=(y(:,:,i)-SUM(lu(:,:,i+1:n,i)*xx(:,:,i+1:n),3))/lu(:,:,i,i)
    ENDDO
    RETURN
  END SUBROUTINE math_solve_sym

!-------------------------------------------------------------------------------
!> use Cholesky factorization to solve symmetric, batched nxn systems.
!
!  if the instruction is 'factor', the lower triangular part of the
!  decomposition is returned in matrix.  if the instruction is
!  'solve', the incoming matrix is expected to be the decomposition.
!  if the instruction is 'both', matrix is not changed, but the
!  decomposition is not saved after the solution vector is found.
!-------------------------------------------------------------------------------
  SUBROUTINE math_solve_q1_sym(n,na,matrix,xx,bb,instruction,singular)
    IMPLICIT NONE

    !* number of row/columns in each matrix
    INTEGER(i4), INTENT(IN) :: n
    !* number of matrices
    INTEGER(i4), INTENT(IN) :: na
    !* matrix to factor or solve
    REAL(r8), DIMENSION(n,n,*), INTENT(INOUT) :: matrix
    !* vector on which to apply inverse matrix if instruction is solve
    REAL(r8), DIMENSION(n,*), INTENT(IN) :: bb
    !* $x = matrix^-1 b$ if instruction is solve
    REAL(r8), DIMENSION(n,*), INTENT(OUT) :: xx
    !* factor or solve system, factor must be called before solve
    CHARACTER(*), INTENT(IN) :: instruction
    !* returns true if matrix is singular
    LOGICAL, INTENT(OUT) :: singular

    REAL(r8), DIMENSION(n) :: y
    REAL(r8), DIMENSION(n,n) :: lu
    INTEGER(i4) :: jcol,i,ia
!-------------------------------------------------------------------------------
!   factor matrix into L*transpose(L) (first column is treated first).
!   if a singularity is encountered, return the pivot in the iq=jq=1
!   position of matrix.
!-------------------------------------------------------------------------------
    singular=.false.
    DO ia=1,na
      IF (instruction=='solve') THEN
        lu=matrix(:,:,ia)
      ELSE
        lu=0
        IF (matrix(1,1,ia)<=0) THEN
          singular=.true.
          RETURN
        ENDIF
        lu(1,1)=SQRT(matrix(1,1,ia))
        DO i=2,n
          lu(1,i)=matrix(1,i,ia)/lu(1,1)
        ENDDO
!-------------------------------------------------------------------------------
!       column loop for L.
!-------------------------------------------------------------------------------
        DO jcol=2,n
          lu(jcol,jcol)=matrix(jcol,jcol,ia)                                    &
                        -SUM(lu(1:jcol-1,jcol)*lu(1:jcol-1,jcol))
          IF (lu(jcol,jcol)<=0) THEN
            singular=.true.
            matrix(1,1,ia)=lu(jcol,jcol)
            RETURN
          ENDIF
          lu(jcol,jcol)=SQRT(lu(jcol,jcol))
          DO i=jcol+1,n
            lu(jcol,i)=(matrix(jcol,i,ia)                                       &
                       -SUM(lu(1:jcol-1,i)*lu(1:jcol-1,jcol)))/lu(jcol,jcol)
          ENDDO
        ENDDO
      ENDIF
!-------------------------------------------------------------------------------
!     copy matrix if solution is not required.
!-------------------------------------------------------------------------------
      IF (instruction=='factor') THEN
        matrix(:,:,ia)=lu
        CYCLE
      ENDIF
!-------------------------------------------------------------------------------
!     solve for xx by getting y first.  {Ly=bb then transpose(L)xx=y}
!-------------------------------------------------------------------------------
      y(1)=bb(1,ia)/lu(1,1)
      DO i=2,n
        y(i)=(bb(i,ia)-SUM(lu(1:i-1,i)*y(1:i-1)))/lu(i,i)
      ENDDO
      xx(n,ia)=y(n)/lu(n,n)
      DO i=n-1,1,-1
        xx(i,ia)=(y(i)-SUM(lu(i,i+1:n)*xx(i+1:n,ia)))/lu(i,i)
      ENDDO
    ENDDO
    RETURN
  END SUBROUTINE math_solve_q1_sym

!-------------------------------------------------------------------------------
!> use Cholesky factorization to solve a Hermitian nxn system.
!
!  if the instruction is 'factor', the lower triangular part of the
!  decomposition is returned in matrix.  if the instruction is
!  'solve', the incoming matrix is expected to be the decomposition.
!  if the instruction is 'both', matrix is not changed, but the
!  decomposition is not saved after the solution vector is found.
!-------------------------------------------------------------------------------
  SUBROUTINE math_solve_herm(n,matrix,xx,bb,instruction,singular)
    IMPLICIT NONE

    !* number of row/columns in each matrix
    INTEGER(i4), INTENT(IN) :: n
    !* matrix to factor or solve
    COMPLEX(r8), DIMENSION(:,:,:,:), INTENT(INOUT) :: matrix
    !* vector on which to apply inverse matrix if instruction is solve
    COMPLEX(r8), DIMENSION(:,:,:), INTENT(IN) :: bb
    !* $x = matrix^-1 b$ if instruction is solve
    COMPLEX(r8), DIMENSION(:,:,:), INTENT(OUT) :: xx
    !* factor or solve system, factor must be called before solve
    CHARACTER(*), INTENT(IN) :: instruction
    !* returns true if matrix is singular
    LOGICAL, INTENT(OUT) :: singular

    COMPLEX(r8), DIMENSION(SIZE(matrix,1),SIZE(matrix,2),n) :: y
    COMPLEX(r8), DIMENSION(SIZE(matrix,1),SIZE(matrix,2),n,n) :: lu
    INTEGER(i4) :: jcol,i
!-------------------------------------------------------------------------------
!   factor matrix into L*transpose(L^*) (first column is treated
!   first).  if a singularity is encountered, return the pivot in
!   the iq=jq=1 position of matrix.
!-------------------------------------------------------------------------------
    singular=.false.
    IF (instruction/='solve') THEN
      lu=0
      IF (MINVAL(REAL(matrix(:,:,1,1),r8))<=0) THEN
        singular=.true.
        RETURN
      ENDIF
      lu(:,:,1,1)=SQRT(REAL(matrix(:,:,1,1),r8))
      DO i=2,n
        lu(:,:,i,1)=matrix(:,:,i,1)/lu(:,:,1,1)
      ENDDO
!-------------------------------------------------------------------------------
!     column loop for L.
!-------------------------------------------------------------------------------
      DO jcol=2,n
        lu(:,:,jcol,jcol)=matrix(:,:,jcol,jcol)                                 &
                      -SUM(lu(:,:,jcol,1:jcol-1)*CONJG(lu(:,:,jcol,1:jcol-1)),3)
        IF (MINVAL(REAL(lu(:,:,jcol,jcol),r8))<=0) THEN
          singular=.true.
          matrix(:,:,1,1)=lu(:,:,jcol,jcol)
          RETURN
        ENDIF
        lu(:,:,jcol,jcol)=SQRT(REAL(lu(:,:,jcol,jcol),r8))
        DO i=jcol+1,n
          lu(:,:,i,jcol)=(matrix(:,:,i,jcol)                                    &
                        -SUM(lu(:,:,i,1:jcol-1)*CONJG(lu(:,:,jcol,1:jcol-1)),3))&
                         /lu(:,:,jcol,jcol)
        ENDDO
      ENDDO
    ENDIF
!-------------------------------------------------------------------------------
!   copy matrix and return if solution is not required.
!-------------------------------------------------------------------------------
    IF (instruction=='factor') THEN
      matrix=lu
      RETURN
    ENDIF
!-------------------------------------------------------------------------------
!   copy decomposition if already factored.
!-------------------------------------------------------------------------------
    IF (instruction=='solve') THEN
      lu=matrix
    ENDIF
!-------------------------------------------------------------------------------
!   solve for xx by getting y first.  {Ly=bb then transpose(L^*)xx=y}
!-------------------------------------------------------------------------------
    y(:,:,1)=bb(:,:,1)/lu(:,:,1,1)
    DO i=2,n
      y(:,:,i)=(bb(:,:,i)-SUM(lu(:,:,i,1:i-1)*y(:,:,1:i-1),3))/lu(:,:,i,i)
    ENDDO
    xx(:,:,n)=y(:,:,n)/lu(:,:,n,n)
    DO i=n-1,1,-1
      xx(:,:,i)=(y(:,:,i)-SUM(CONJG(lu(:,:,i+1:n,i))*xx(:,:,i+1:n),3))          &
                                   /lu(:,:,i,i)
    ENDDO
    RETURN
  END SUBROUTINE math_solve_herm

!-------------------------------------------------------------------------------
!> use Cholesky factorization to solve Hermitian, batched nxn systems.
!
!  if the instruction is 'factor', the lower triangular part of the
!  decomposition is returned in matrix.  if the instruction is
!  'solve', the incoming matrix is expected to be the decomposition.
!  if the instruction is 'both', matrix is not changed, but the
!  decomposition is not saved after the solution vector is found.
!-------------------------------------------------------------------------------
  SUBROUTINE math_solve_q1_herm(n,na,matrix,xx,bb,instruction,singular)
    IMPLICIT NONE

    !* number of row/columns in each matrix
    INTEGER(i4), INTENT(IN) :: n
    !* number of matrices
    INTEGER(i4), INTENT(IN) :: na
    !* matrix to factor or solve
    COMPLEX(r8), DIMENSION(n,n,*), INTENT(INOUT) :: matrix
    !* vector on which to apply inverse matrix if instruction is solve
    COMPLEX(r8), DIMENSION(n,*), INTENT(IN) :: bb
    !* $x = matrix^-1 b$ if instruction is solve
    COMPLEX(r8), DIMENSION(n,*), INTENT(OUT) :: xx
    !* factor or solve system, factor must be called before solve
    CHARACTER(*), INTENT(IN) :: instruction
    !* returns true if matrix is singular
    LOGICAL, INTENT(OUT) :: singular

    COMPLEX(r8), DIMENSION(n) :: y
    COMPLEX(r8), DIMENSION(n,n) :: lu
    INTEGER(i4) :: jcol,i,ia
!-------------------------------------------------------------------------------
!   factor matrix into L*transpose(L^*) (first column is treated
!   first).  if a singularity is encountered, return the pivot in
!   the iq=jq=1 position of matrix.
!-------------------------------------------------------------------------------
    singular=.false.
    DO ia=1,na
      IF (instruction=='solve') THEN
        lu=matrix(:,:,ia)
      ELSE
        lu=0
        IF (REAL(matrix(1,1,ia),r8)<=0) THEN
          singular=.true.
          RETURN
        ENDIF
        lu(1,1)=SQRT(REAL(matrix(1,1,ia),r8))
        DO i=2,n
          lu(1,i)=matrix(1,i,ia)/lu(1,1)
        ENDDO
!-------------------------------------------------------------------------------
!       column loop for L.
!-------------------------------------------------------------------------------
        DO jcol=2,n
          lu(jcol,jcol)=matrix(jcol,jcol,ia)                                    &
                        -SUM(lu(1:jcol-1,jcol)*CONJG(lu(1:jcol-1,jcol)))
          IF (REAL(lu(jcol,jcol),r8)<=0) THEN
            singular=.true.
            matrix(1,1,ia)=lu(jcol,jcol)
            RETURN
          ENDIF
          lu(jcol,jcol)=SQRT(REAL(lu(jcol,jcol),r8))
          DO i=jcol+1,n
            lu(jcol,i)=(matrix(jcol,i,ia)                                       &
                    -SUM(lu(1:jcol-1,i)*CONJG(lu(1:jcol-1,jcol))))/lu(jcol,jcol)
          ENDDO
        ENDDO
      ENDIF
!-------------------------------------------------------------------------------
!     copy matrix if solution is not required.
!-------------------------------------------------------------------------------
      IF (instruction=='factor') THEN
        matrix(:,:,ia)=lu
        CYCLE
      ENDIF
!-------------------------------------------------------------------------------
!     solve for xx by getting y first. {Ly=bb then transpose(L^*)xx=y}
!-------------------------------------------------------------------------------
      y(1)=bb(1,ia)/lu(1,1)
      DO i=2,n
        y(i)=(bb(i,ia)-SUM(lu(1:i-1,i)*y(1:i-1)))/lu(i,i)
      ENDDO
      xx(n,ia)=y(n)/lu(n,n)
      DO i=n-1,1,-1
        xx(i,ia)=(y(i)-SUM(CONJG(lu(i,i+1:n))*xx(i+1:n,ia)))/lu(i,i)
      ENDDO
    ENDDO
    RETURN
  END SUBROUTINE math_solve_q1_herm

!-------------------------------------------------------------------------------
!> use Cholesky or LU factorization to invert batched nxn systems.
!-------------------------------------------------------------------------------
  SUBROUTINE math_invert_q1(n,na,matrix,id,symmetric,singular)
    USE pardata_mod
    IMPLICIT NONE

    !* number of row/columns in each matrix
    INTEGER(i4), INTENT(IN) :: n
    !* number of matrices
    INTEGER(i4), INTENT(IN) :: na
    !* matrix to invert in place
    REAL(r8), DIMENSION(n,n,na), INTENT(INOUT) :: matrix
    !* kernel async stream ID
    INTEGER(i4), INTENT(IN) :: id
    !* if true use Cholesky otherwise LU
    LOGICAL, INTENT(IN) :: symmetric
    !* returns true if matrix is singular
    LOGICAL, INTENT(OUT) :: singular

    REAL(r8), DIMENSION(:,:,:), ALLOCATABLE :: matinv
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: xx,ident
    INTEGER(i4) :: jq

    singular=.FALSE.
!-------------------------------------------------------------------------------
!   factor the matrix
!-------------------------------------------------------------------------------
    ALLOCATE(xx(n,na))
    ALLOCATE(ident(n,na))
    IF (symmetric) THEN
      CALL math_solve_q1_sym(n,na,matrix,xx,ident,'factor',singular)
    ELSE
      CALL math_solve_q1_nxn(n,na,matrix,xx,ident,'factor',singular)
    ENDIF
    IF (singular) THEN
      DEALLOCATE(xx,ident)
      RETURN
    ENDIF
!-------------------------------------------------------------------------------
!   find and save each column of the inverse.
!-------------------------------------------------------------------------------
    ALLOCATE(matinv(n,n,na))
    DO jq=1,n
      ident=0
      ident(jq,:)=1
      IF (symmetric) THEN
        CALL math_solve_q1_sym(n,na,matrix,xx,ident,'solve',singular)
      ELSE
        CALL math_solve_q1_nxn(n,na,matrix,xx,ident,'solve',singular)
      ENDIF
      matinv(jq,:,:)=xx
    ENDDO
    matrix=matinv
    DEALLOCATE(matinv,ident,xx)
  END SUBROUTINE math_invert_q1

!-------------------------------------------------------------------------------
!> use Cholesky or LU factorization to invert batched complex nxn systems.
!-------------------------------------------------------------------------------
  SUBROUTINE math_invert_comp_q1(n,na,matrix,id,hermitian,singular)
    USE pardata_mod
    IMPLICIT NONE

    !* number of row/columns in each matrix
    INTEGER(i4), INTENT(IN) :: n
    !* number of matrices
    INTEGER(i4), INTENT(IN) :: na
    !* matrix to invert in place
    COMPLEX(r8), DIMENSION(n,n,na), INTENT(INOUT) :: matrix
    !* kernel async stream ID
    INTEGER(i4), INTENT(IN) :: id
    !* if true use Cholesky otherwise LU
    LOGICAL, INTENT(IN) :: hermitian
    !* returns true if matrix is singular
    LOGICAL, INTENT(OUT) :: singular

    COMPLEX(r8), DIMENSION(:,:,:), ALLOCATABLE :: matinv
    COMPLEX(r8), DIMENSION(:,:), ALLOCATABLE :: xx,ident
    INTEGER(i4) :: jq

    singular=.FALSE.
!-------------------------------------------------------------------------------
!   factor the matrix
!-------------------------------------------------------------------------------
    ALLOCATE(xx(n,na))
    ALLOCATE(ident(n,na))
    IF (hermitian) THEN
      CALL math_solve_q1_herm(n,na,matrix,xx,ident,'factor',singular)
    ELSE
      CALL math_solve_q1_cnxn(n,na,matrix,xx,ident,'factor',singular)
    ENDIF
    IF (singular) THEN
      DEALLOCATE(xx,ident)
      RETURN
    ENDIF
!-------------------------------------------------------------------------------
!   find and save each column of the inverse.
!-------------------------------------------------------------------------------
    ALLOCATE(matinv(n,n,na))
    DO jq=1,n
      ident=0
      ident(jq,:)=1
      IF (hermitian) THEN
        CALL math_solve_q1_herm(n,na,matrix,xx,ident,'solve',singular)
      ELSE
        CALL math_solve_q1_cnxn(n,na,matrix,xx,ident,'solve',singular)
      ENDIF
      matinv(jq,:,:)=xx
    ENDDO
    matrix=matinv
    DEALLOCATE(matinv,ident,xx)
  END SUBROUTINE math_invert_comp_q1

!-------------------------------------------------------------------------------
!> find the determinant and inverse Jacobian from the Jacobian
!-------------------------------------------------------------------------------
  SUBROUTINE math_metric(jac, ijac, detj)
    USE pardata_mod
    IMPLICIT NONE

    !* input Jacobian
    REAL(r8), DIMENSION(:,:,:), INTENT(IN) :: jac
    !* output inverse Jacobian
    REAL(r8), DIMENSION(:,:,:), INTENT(OUT) :: ijac
    !* output determinant of the Jacobian
    REAL(r8), DIMENSION(:), INTENT(OUT) :: detj

    SELECT CASE (SIZE(jac,3))
    CASE (1)
      detj=jac(:,1,1)
      ijac(:,1,1)=1._r8/detj
    CASE (2)
      detj=jac(:,1,1)*jac(:,2,2)-jac(:,2,1)*jac(:,1,2)
      ijac(:,1,1)=jac(:,2,2)/detj
      ijac(:,2,1)=-jac(:,2,1)/detj
      ijac(:,1,2)=-jac(:,1,2)/detj
      ijac(:,2,2)=jac(:,1,1)/detj
    CASE (3)
      CALL par%nim_stop("math_metric not implemented for n=3 case")
    CASE DEFAULT
      CALL par%nim_stop("math_metric not implemented for general case")
    END SELECT
  END SUBROUTINE math_metric

!-------------------------------------------------------------------------------
!> Make an array copy, useful for OpenACC optimization with high dimensional
!  arrays
!-------------------------------------------------------------------------------
  SUBROUTINE acc_array_copy(n,arr_out,arr_in,id)
    IMPLICIT NONE

    !* size of arrays
    INTEGER(i4), INTENT(IN) :: n
    !* array out
    REAL(r8), DIMENSION(n), INTENT(INOUT) :: arr_out
    !* array in
    REAL(r8), DIMENSION(n), INTENT(IN) :: arr_in
    !* kernel async stream ID
    INTEGER(i4), INTENT(IN) :: id

    !$acc kernels present(arr_out,arr_in) async(id)
    arr_out=arr_in
    !$acc end kernels
  END SUBROUTINE acc_array_copy

!-------------------------------------------------------------------------------
!> Make an array copy, useful for OpenACC optimization with high dimensional
!  arrays
!-------------------------------------------------------------------------------
  SUBROUTINE acc_carray_copy(n,arr_out,arr_in,id)
    IMPLICIT NONE

    !* size of arrays
    INTEGER(i4), INTENT(IN) :: n
    !* array out
    COMPLEX(r8), DIMENSION(n), INTENT(INOUT) :: arr_out
    !* array in
    COMPLEX(r8), DIMENSION(n), INTENT(IN) :: arr_in
    !* kernel async stream ID
    INTEGER(i4), INTENT(IN) :: id

    !$acc kernels present(arr_out,arr_in) async(id)
    arr_out=arr_in
    !$acc end kernels
  END SUBROUTINE acc_carray_copy

END MODULE math_tran
