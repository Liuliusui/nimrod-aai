!-------------------------------------------------------------------------------
!! Defines the abstract interface for linear algebra vectors
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> Defines the abstract interface for linear algebra vectors.
!
!  Operations performed are assignment, addition, multiplication, dot products,
!  finding norms, application of seams, and enforcement of boundary and
!  regularity conditions. Assignment of one is provided for testing.
!
!  Naming conventions for derived types should use the format
!  vec\_{shape}\_{dim}\_{type} where, for example, shape is rect or tri, dim
!  is 1D or 2D, and type is real or comp.
!-------------------------------------------------------------------------------
MODULE vector_mod
  USE local
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: rvec_storage, cv1m_storage, cvec_storage

  PUBLIC :: rvector, cvector1m, cvector

!-------------------------------------------------------------------------------
!> Abstract base type that defines implementation requirements for
!  vector operations with a real data type
!-------------------------------------------------------------------------------
  TYPE, ABSTRACT :: rvector
    !> Number of quantities (e.g. 3 for vector)
    INTEGER(i4) :: nqty=0
    !> Unit number of degrees of freedom (ndof per element for a scalar)
    INTEGER(i4) :: u_ndof=0
    !> Number of elements in field
    INTEGER(i4) :: nel=0
    !> dimensionality of field (e.g. 1D, 2D, etc.)
    INTEGER(i4) :: ndim=0
    !> associated Fourier component
    INTEGER(i4) :: fcomp=0
    !> if this vector is eliminated through static condensation
    !  do not operate on interior DOFs
    LOGICAL :: skip_elim_interior=.FALSE.
    !> ID used for parallel streams. Mirrors the block id.
    INTEGER(i4) :: id=0
    !> true if data on GPU
    LOGICAL :: on_gpu=.FALSE.
    !> Size needed for returning values of inf_norm
    INTEGER(i4) :: inf_norm_size=0
    !> Size needed for returning values of l2_norm2 or dot
    INTEGER(i4) :: l2_dot_size=0
    !> Memory profiler ID
    INTEGER(i4), POINTER :: mem_id=>NULL()
  CONTAINS

    PROCEDURE(dealloc_real), PASS(rvec), DEFERRED :: dealloc
    PROCEDURE(alloc_with_mold_real), PASS(rvec), DEFERRED :: alloc_with_mold
    PROCEDURE(zero_real), PASS(rvec), DEFERRED :: zero
    PROCEDURE(assign_rvec_real), PASS(rvec), DEFERRED :: assign_rvec
    PROCEDURE(assignp_rvec_real), PASS(rvec), DEFERRED :: assignp_rvec
    PROCEDURE(assign_cv1m_real), PASS(rvec), DEFERRED :: assign_cv1m
    PROCEDURE(assignp_cv1m_real), PASS(rvec), DEFERRED :: assignp_cv1m
    PROCEDURE(assign_cvec_real), PASS(rvec), DEFERRED :: assign_cvec
    PROCEDURE(assignp_cvec_real), PASS(rvec), DEFERRED :: assignp_cvec
    PROCEDURE(add_vec_real), PASS(rvec), DEFERRED :: add_vec
    PROCEDURE(mult_rsc_real), PASS(rvec), DEFERRED :: mult_rsc
    PROCEDURE(mult_csc_real), PASS(rvec), DEFERRED :: mult_csc
    PROCEDURE(mult_int_real), PASS(rvec), DEFERRED :: mult_int
    PROCEDURE(inf_norm_real), PASS(rvec), DEFERRED :: inf_norm
    PROCEDURE(l2_norm2_real), PASS(rvec), DEFERRED :: l2_norm2
    PROCEDURE(dot_real), PASS(rvec), DEFERRED :: dot
    PROCEDURE(assemble_real), PASS(rvec), DEFERRED :: assemble
    PROCEDURE(dirichlet_bc_real), PASS(rvec), DEFERRED :: dirichlet_bc
    PROCEDURE(regularity_real), PASS(rvec), DEFERRED :: regularity
    !PROCEDURE(regularity_ave_real), PASS(rvec), DEFERRED :: regularity_ave
    !PROCEDURE(regularity_zero_phi_real), PASS(rvec), DEFERRED ::               &
    !                                                        regularity_zero_phi
    PROCEDURE(set_edge_vars_real), PASS(rvec), DEFERRED ::  set_edge_vars
    PROCEDURE(edge_load_arr_real), PASS(rvec), DEFERRED :: edge_load_arr
    PROCEDURE(edge_unload_arr_real), PASS(rvec), DEFERRED :: edge_unload_arr
    PROCEDURE(edge_load_save_real), PASS(rvec), DEFERRED :: edge_load_save
    PROCEDURE(edge_add_save_real), PASS(rvec), DEFERRED :: edge_add_save
    PROCEDURE(apply_diag_matvec_real), PASS(rvec), DEFERRED :: apply_diag_matvec
    PROCEDURE(invert_real), PASS(rvec), DEFERRED :: invert
    PROCEDURE(assign_for_testing_real), PASS(rvec), DEFERRED ::                 &
                                                              assign_for_testing
    PROCEDURE(test_if_equal_real), PASS(rvec), DEFERRED :: test_if_equal
    GENERIC :: ASSIGNMENT (=) => assign_rvec
    GENERIC :: mult => mult_rsc, mult_csc, mult_int
  END TYPE rvector

!-------------------------------------------------------------------------------
!> Abstract base type that defines implementation requirements for
!  vector operations with a complex data type with a single mode.
!  This type is designed for intermediate computations.
!-------------------------------------------------------------------------------
  TYPE, ABSTRACT :: cvector1m
    !> Number of quantities (e.g. 3 for vector)
    INTEGER(i4) :: nqty=0
    !> Unit number of degrees of freedom (ndof per element for a scalar)
    INTEGER(i4) :: u_ndof=0
    !> Number of elements in field
    INTEGER(i4) :: nel=0
    !> dimensionality of field (e.g. 1D, 2D, etc.)
    INTEGER(i4) :: ndim=0
    !> associated Fourier component
    INTEGER(i4) :: fcomp=0
    !> if this vector is eliminated through static condensation
    !  do not operate on interior DOFs
    LOGICAL :: skip_elim_interior=.FALSE.
    !> ID used for parallel streams. Mirrors the block id.
    INTEGER(i4) :: id=0
    !> true if data on GPU
    LOGICAL :: on_gpu=.FALSE.
    !> Size needed for returning values of inf_norm
    INTEGER(i4) :: inf_norm_size=0
    !> Size needed for returning values of l2_norm2 or dot
    INTEGER(i4) :: l2_dot_size=0
    !> Memory profiler ID
    INTEGER(i4), POINTER :: mem_id=>NULL()
  CONTAINS

    PROCEDURE(dealloc_cv1m), PASS(cv1m), DEFERRED :: dealloc
    PROCEDURE(alloc_with_mold_cv1m), PASS(cv1m), DEFERRED :: alloc_with_mold
    PROCEDURE(zero_cv1m), PASS(cv1m), DEFERRED :: zero
    PROCEDURE(assign_rvec_cv1m), PASS(cv1m), DEFERRED :: assign_rvec
    PROCEDURE(assignp_rvec_cv1m), PASS(cv1m), DEFERRED :: assignp_rvec
    PROCEDURE(assign_cv1m_cv1m), PASS(cv1m), DEFERRED :: assign_cv1m
    PROCEDURE(assignp_cv1m_cv1m), PASS(cv1m), DEFERRED :: assignp_cv1m
    PROCEDURE(assign_cvec_cv1m), PASS(cv1m), DEFERRED :: assign_cvec
    PROCEDURE(assignp_cvec_cv1m), PASS(cv1m), DEFERRED :: assignp_cvec
    PROCEDURE(add_vec_cv1m), PASS(cv1m), DEFERRED :: add_vec
    PROCEDURE(mult_rsc_cv1m), PASS(cv1m), DEFERRED :: mult_rsc
    PROCEDURE(mult_csc_cv1m), PASS(cv1m), DEFERRED :: mult_csc
    PROCEDURE(mult_int_cv1m), PASS(cv1m), DEFERRED :: mult_int
    PROCEDURE(inf_norm_cv1m), PASS(cv1m), DEFERRED :: inf_norm
    PROCEDURE(l2_norm2_cv1m), PASS(cv1m), DEFERRED :: l2_norm2
    PROCEDURE(dot_cv1m), PASS(cv1m), DEFERRED :: dot
    PROCEDURE(assemble_cv1m), PASS(cv1m), DEFERRED :: assemble
    PROCEDURE(dirichlet_bc_cv1m), PASS(cv1m), DEFERRED :: dirichlet_bc
    PROCEDURE(regularity_cv1m), PASS(cv1m), DEFERRED :: regularity
    !PROCEDURE(regularity_ave_cv1m), PASS(cv1m), DEFERRED :: regularity_ave
    !PROCEDURE(regularity_zero_phi_cv1m), PASS(cv1m), DEFERRED ::               &
    !                                                        regularity_zero_phi
    PROCEDURE(set_edge_vars_cv1m), PASS(cv1m), DEFERRED :: set_edge_vars
    PROCEDURE(edge_load_arr_cv1m), PASS(cv1m), DEFERRED :: edge_load_arr
    PROCEDURE(edge_unload_arr_cv1m), PASS(cv1m), DEFERRED :: edge_unload_arr
    PROCEDURE(edge_load_save_cv1m), PASS(cv1m), DEFERRED :: edge_load_save
    PROCEDURE(edge_add_save_cv1m), PASS(cv1m), DEFERRED :: edge_add_save
    PROCEDURE(apply_diag_matvec_cv1m), PASS(cv1m), DEFERRED :: apply_diag_matvec
    PROCEDURE(invert_cv1m), PASS(cv1m), DEFERRED :: invert
    PROCEDURE(assign_for_testing_cv1m), PASS(cv1m), DEFERRED ::                 &
                                                              assign_for_testing
    PROCEDURE(test_if_equal_cv1m), PASS(cv1m), DEFERRED :: test_if_equal
    GENERIC :: ASSIGNMENT (=) => assign_cv1m
    GENERIC :: mult => mult_rsc, mult_csc, mult_int
  END TYPE cvector1m

!-------------------------------------------------------------------------------
!> Abstract base type that defines implementation requirements for
!  vector operations with a complex data type.
!-------------------------------------------------------------------------------
  TYPE, ABSTRACT :: cvector
    !> Number of quantities (e.g. 3 for vector)
    INTEGER(i4) :: nqty=0
    !> Unit number of degrees of freedom (ndof per element for a scalar)
    INTEGER(i4) :: u_ndof=0
    !> Number of elements in field
    INTEGER(i4) :: nel=0
    !> dimensionality of field (e.g. 1D, 2D, etc.)
    INTEGER(i4) :: ndim=0
    !> number of Fourier modes in field
    INTEGER(i4) :: nmodes=0
    !> if this vector is eliminated through static condensation
    !  do not operate on interior DOFs
    LOGICAL :: skip_elim_interior=.FALSE.
    !> ID used for parallel streams. Mirrors the block id.
    INTEGER(i4) :: id=0
    !> true if data on GPU
    LOGICAL :: on_gpu=.FALSE.
    !> Size needed for returning values of inf_norm
    INTEGER(i4) :: inf_norm_size=0
    !> Size needed for returning values of l2_norm2 or dot
    INTEGER(i4) :: l2_dot_size=0
    !> Memory profiler ID
    INTEGER(i4), POINTER :: mem_id=>NULL()
  CONTAINS

    PROCEDURE(dealloc_comp), PASS(cvec), DEFERRED :: dealloc
    PROCEDURE(alloc_with_mold_comp), PASS(cvec), DEFERRED ::                    &
                                                          alloc_with_mold_comp
    PROCEDURE(alloc_with_mold_cv1m_comp), PASS(cvec), DEFERRED ::               &
                                                     alloc_with_mold_cv1m_comp
    GENERIC :: alloc_with_mold => alloc_with_mold_comp,                         &
                                  alloc_with_mold_cv1m_comp
    PROCEDURE(zero_comp), PASS(cvec), DEFERRED :: zero
    PROCEDURE(assign_rvec_comp), PASS(cvec), DEFERRED :: assign_rvec
    PROCEDURE(assignp_rvec_comp), PASS(cvec), DEFERRED :: assignp_rvec
    PROCEDURE(assign_cv1m_comp), PASS(cvec), DEFERRED :: assign_cv1m
    PROCEDURE(assignp_cv1m_comp), PASS(cvec), DEFERRED :: assignp_cv1m
    PROCEDURE(assign_cvec_comp), PASS(cvec), DEFERRED :: assign_cvec
    PROCEDURE(assignp_cvec_comp), PASS(cvec), DEFERRED :: assignp_cvec
    PROCEDURE(add_vec_comp), PASS(cvec), DEFERRED :: add_vec
    PROCEDURE(mult_rsc_comp), PASS(cvec), DEFERRED :: mult_rsc
    PROCEDURE(mult_csc_comp), PASS(cvec), DEFERRED :: mult_csc
    PROCEDURE(mult_int_comp), PASS(cvec), DEFERRED :: mult_int
    PROCEDURE(inf_norm_comp), PASS(cvec), DEFERRED :: inf_norm
    PROCEDURE(l2_norm2_comp), PASS(cvec), DEFERRED :: l2_norm2
    PROCEDURE(dot_comp), PASS(cvec), DEFERRED :: dot
    PROCEDURE(assemble_comp), PASS(cvec), DEFERRED :: assemble
    PROCEDURE(dirichlet_bc_comp), PASS(cvec), DEFERRED :: dirichlet_bc
    PROCEDURE(regularity_comp), PASS(cvec), DEFERRED :: regularity
    !PROCEDURE(regularity_ave_comp), PASS(cvec), DEFERRED :: regularity_ave
    !PROCEDURE(regularity_zero_phi_comp), PASS(cvec), DEFERRED ::               &
    !                                                        regularity_zero_phi
    PROCEDURE(set_edge_vars_comp), PASS(cvec), DEFERRED :: set_edge_vars
    PROCEDURE(edge_load_arr_comp), PASS(cvec), DEFERRED :: edge_load_arr
    PROCEDURE(edge_unload_arr_comp), PASS(cvec), DEFERRED :: edge_unload_arr
    PROCEDURE(edge_load_save_comp), PASS(cvec), DEFERRED :: edge_load_save
    PROCEDURE(edge_add_save_comp), PASS(cvec), DEFERRED :: edge_add_save
    PROCEDURE(apply_diag_matvec_comp), PASS(cvec), DEFERRED :: apply_diag_matvec
    PROCEDURE(invert_comp), PASS(cvec), DEFERRED :: invert
    PROCEDURE(assign_for_testing_comp), PASS(cvec), DEFERRED ::                 &
                                                              assign_for_testing
    PROCEDURE(test_if_equal_comp), PASS(cvec), DEFERRED :: test_if_equal
    GENERIC :: ASSIGNMENT (=) => assign_cvec
    GENERIC :: mult => mult_rsc, mult_csc, mult_int
  END TYPE cvector

!-------------------------------------------------------------------------------
!* container for rvector
!-------------------------------------------------------------------------------
  TYPE :: rvec_storage
    CLASS(rvector), ALLOCATABLE :: v
  END TYPE rvec_storage

!-------------------------------------------------------------------------------
!* container for cvector1m
!-------------------------------------------------------------------------------
  TYPE :: cv1m_storage
    CLASS(cvector1m), ALLOCATABLE :: v
  END TYPE cv1m_storage

!-------------------------------------------------------------------------------
!* container for cvector
!-------------------------------------------------------------------------------
  TYPE :: cvec_storage
    CLASS(cvector), ALLOCATABLE :: v
  END TYPE cvec_storage

  ABSTRACT INTERFACE
!-------------------------------------------------------------------------------
!*  Deallocate the vector
!-------------------------------------------------------------------------------
    SUBROUTINE dealloc_real(rvec)
      IMPORT rvector
      !> vector to dealloc
      CLASS(rvector), INTENT(INOUT) :: rvec
    END SUBROUTINE dealloc_real

!-------------------------------------------------------------------------------
!*  Create a new vector of the same type
!-------------------------------------------------------------------------------
    SUBROUTINE alloc_with_mold_real(rvec,new_rvec,nqty,pd)
      USE local
      IMPORT rvector
      !> reference vector to mold
      CLASS(rvector), INTENT(IN) :: rvec
      !> new vector to be created
      CLASS(rvector), ALLOCATABLE, INTENT(OUT) :: new_rvec
      !> number of quantities, vec%nqty is used if unspecified
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
      !> polynomial degree, vec%pd is used if unspecified
      INTEGER(i4), OPTIONAL, INTENT(IN) :: pd
    END SUBROUTINE alloc_with_mold_real

!-------------------------------------------------------------------------------
!*  Assign zero to the vector
!-------------------------------------------------------------------------------
    SUBROUTINE zero_real(rvec)
      IMPORT rvector
      !> vector to assign to
      CLASS(rvector), INTENT(INOUT) :: rvec
    END SUBROUTINE zero_real

!-------------------------------------------------------------------------------
!*  Assign a vector to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assign_rvec_real(rvec,rvec2)
      IMPORT rvector
      !> vector to assign to
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> vector to assign
      CLASS(rvector), INTENT(IN) :: rvec2
    END SUBROUTINE assign_rvec_real

!-------------------------------------------------------------------------------
!*  Partially assign a vector to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assignp_rvec_real(rvec,rvec2,v1st,v2st,nq)
      USE local
      IMPORT rvector
      !> vector to assign to
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> vector to assign
      CLASS(rvector), INTENT(IN) :: rvec2
      !> quantity start for the 1st vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
      !> quantity start for the 2nd vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
      !> number of quantities to assign (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
    END SUBROUTINE assignp_rvec_real

!-------------------------------------------------------------------------------
!*  Assign a cvector1m to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assign_cv1m_real(rvec,cv1m,r_i)
      IMPORT rvector
      IMPORT cvector1m
      !> vector to assign to
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> vector to assign
      CLASS(cvector1m), INTENT(IN) :: cv1m
      !> 'real' for the real part of cvec, 'imag' for the imaginary part
      CHARACTER(*), INTENT(IN) :: r_i
    END SUBROUTINE assign_cv1m_real

!-------------------------------------------------------------------------------
!*  Partially assign a cvector1m to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assignp_cv1m_real(rvec,cv1m,r_i,v1st,v2st,nq)
      USE local
      IMPORT rvector
      IMPORT cvector1m
      !> vector to assign to
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> 'real' for the real part of cvec, 'imag' for the imaginary part
      CHARACTER(*), INTENT(IN) :: r_i
      !> vector to assign
      CLASS(cvector1m), INTENT(IN) :: cv1m
      !> quantity start for the 1st vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
      !> quantity start for the 2nd vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
      !> number of quantities to assign (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
    END SUBROUTINE

!-------------------------------------------------------------------------------
!*  Assign a cvector to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assign_cvec_real(rvec,cvec,imode,r_i)
      USE local
      IMPORT rvector
      IMPORT cvector
      !> vector to assign to
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> vector to assign
      CLASS(cvector), INTENT(IN) :: cvec
      !> mode to assign
      INTEGER(i4), INTENT(IN) :: imode
      !> 'real' for the real part of cvec, 'imag' for the imaginary part
      CHARACTER(*), INTENT(IN) :: r_i
    END SUBROUTINE assign_cvec_real

!-------------------------------------------------------------------------------
!*  Partially assign a cvector to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assignp_cvec_real(rvec,cvec,imode,r_i,v1st,v2st,nq)
      USE local
      IMPORT rvector
      IMPORT cvector
      !> vector to assign to
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> vector to assign
      CLASS(cvector), INTENT(IN) :: cvec
      !> mode to assign
      INTEGER(i4), INTENT(IN) :: imode
      !> 'real' for the real part of cvec, 'imag' for the imaginary part
      CHARACTER(*), INTENT(IN) :: r_i
      !> quantity start for the 1st vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
      !> quantity start for the 2nd vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
      !> number of quantities to assign (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
    END SUBROUTINE assignp_cvec_real

!-------------------------------------------------------------------------------
!*  add a vector to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE add_vec_real(rvec,rvec2,v1st,v2st,nq,v1fac,v2fac)
      USE local
      IMPORT rvector
      !> vector to assign to
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> vector to assign
      CLASS(rvector), INTENT(IN) :: rvec2
      !> quantity start for the 1st vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
      !> quantity start for the 2nd vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
      !> number of quantities to assign (default 1 when v1st or v2st is set
      !> default is all when neither v1st or v2st is set)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
      !> factor to multiply vec by (default 1)
      CLASS(*), OPTIONAL, INTENT(IN) :: v1fac
      !> factor to multiply vec2 by (default 1)
      CLASS(*), OPTIONAL, INTENT(IN) :: v2fac
    END SUBROUTINE add_vec_real

!-------------------------------------------------------------------------------
!*  multiply a vector by a real scalar
!-------------------------------------------------------------------------------
    SUBROUTINE mult_rsc_real(rvec,rscalar)
      USE local
      IMPORT rvector
      !> vector to be multiplied
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> scalar to multiply
      REAL(r8), INTENT(IN) :: rscalar
    END SUBROUTINE mult_rsc_real

!-------------------------------------------------------------------------------
!*  multiply a vector by a complex scalar
!-------------------------------------------------------------------------------
    SUBROUTINE mult_csc_real(rvec,cscalar)
      USE local
      IMPORT rvector
      !> vector to be multiplied
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> scalar to multiply
      COMPLEX(r8), INTENT(IN) :: cscalar
    END SUBROUTINE mult_csc_real

!-------------------------------------------------------------------------------
!*  multiply a vector by a integer scalar
!-------------------------------------------------------------------------------
    SUBROUTINE mult_int_real(rvec,iscalar)
      USE local
      IMPORT rvector
      !> vector to be multiplied
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> scalar to multiply
      INTEGER(i4), INTENT(IN) :: iscalar
    END SUBROUTINE mult_int_real

!-------------------------------------------------------------------------------
!*  compute the inf norm
!-------------------------------------------------------------------------------
    SUBROUTINE inf_norm_real(rvec,infnorm)
      USE local
      IMPORT rvector
      !> vector
      CLASS(rvector), INTENT(IN) :: rvec
      !> inf norm contributions from the vector (size vec%inf_norm_size)
      REAL(r8), INTENT(INOUT) :: infnorm(:)
    END SUBROUTINE inf_norm_real

!-------------------------------------------------------------------------------
!*  compute the L2 norm squared
!-------------------------------------------------------------------------------
    SUBROUTINE l2_norm2_real(rvec,l2norm,edge)
      USE local
      USE edge_mod
      IMPORT rvector
      !> vector
      CLASS(rvector), INTENT(IN) :: rvec
      !> L2 norm contributions from the vector (size vec%l2_dot_size)
      REAL(r8), INTENT(INOUT) :: l2norm(:)
      !> edge data to weight edge value contribution
      TYPE(edge_type), INTENT(IN) :: edge
    END SUBROUTINE l2_norm2_real

!-------------------------------------------------------------------------------
!*  compute the dot product of vec and vec2
!-------------------------------------------------------------------------------
    SUBROUTINE dot_real(rvec,rvec2,dot,edge)
      USE local
      USE edge_mod
      IMPORT rvector
      !> vector
      CLASS(rvector), INTENT(IN) :: rvec
      !> vector to dot
      CLASS(rvector), INTENT(IN) :: rvec2
      !> dot contributions from the vector (size vec%l2_dot_size)
      REAL(r8), INTENT(INOUT) :: dot(:)
      !> edge data to weight edge value contribution
      TYPE(edge_type), INTENT(IN) :: edge
    END SUBROUTINE dot_real

!-------------------------------------------------------------------------------
!>  transfer contributions from integrand into internal storage as a plus
!   equals operation (the user must call %zero as needed).
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_real(rvec,integrand)
      USE local
      IMPORT rvector
      !> vector
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:)
    END SUBROUTINE assemble_real

!-------------------------------------------------------------------------------
!*  Applies dirichlet boundary conditions
!-------------------------------------------------------------------------------
    SUBROUTINE dirichlet_bc_real(rvec,component,edge,symm,seam_save)
      USE edge_mod
      IMPORT rvector
      !> vector
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> flag to determine normal/tangential/scalar behavior
      CHARACTER(*), INTENT(IN) :: component
      !> associated edge
      TYPE(edge_type), INTENT(INOUT) :: edge
      !> flag for symmetric boundary
      CHARACTER(*), OPTIONAL, INTENT(IN) :: symm
      !> save the existing boundary data in seam_save
      LOGICAL, OPTIONAL, INTENT(IN) :: seam_save
    END SUBROUTINE dirichlet_bc_real

!-------------------------------------------------------------------------------
!*  Applies regularity conditions at R=0 in toroidal geometry
!-------------------------------------------------------------------------------
    SUBROUTINE regularity_real(rvec,edge,flag)
      USE edge_mod
      IMPORT rvector
      !> vector
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> associated edge
      TYPE(edge_type), INTENT(IN) :: edge
      !> flag
      CHARACTER(*), INTENT(IN) :: flag
    END SUBROUTINE regularity_real

!-------------------------------------------------------------------------------
!*  set variables needed by edge routines
!-------------------------------------------------------------------------------
    SUBROUTINE set_edge_vars_real(rvec,edge)
      USE local
      USE edge_mod
      IMPORT rvector
      !> vector
      CLASS(rvector), INTENT(IN) :: rvec
      !> edge to set vars in
      TYPE(edge_type), INTENT(INOUT) :: edge
    END SUBROUTINE set_edge_vars_real

!-------------------------------------------------------------------------------
!*  load a edge communitcation array with vector data
!-------------------------------------------------------------------------------
    SUBROUTINE edge_load_arr_real(rvec,edge,nqty,n_side,do_avg)
      USE local
      USE edge_mod
      IMPORT rvector
      !> vector to unload
      CLASS(rvector), INTENT(IN) :: rvec
      !> edge to load
      TYPE(edge_type), INTENT(INOUT) :: edge
      !> number of quantities to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
      !> number of side points to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
      !> apply edge average if true
      LOGICAL, OPTIONAL, INTENT(IN) :: do_avg
    END SUBROUTINE edge_load_arr_real

!-------------------------------------------------------------------------------
!*  unload a edge communication array with vector data
!-------------------------------------------------------------------------------
    SUBROUTINE edge_unload_arr_real(rvec,edge)
      USE local
      USE edge_mod
      IMPORT rvector
      !> vector to loaded
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> edge to unload
      TYPE(edge_type), INTENT(INOUT) :: edge
    END SUBROUTINE edge_unload_arr_real

!-------------------------------------------------------------------------------
!*  load a edge save array with vector data
!-------------------------------------------------------------------------------
    SUBROUTINE edge_load_save_real(rvec,edge,nqty,n_side)
      USE local
      USE edge_mod
      IMPORT rvector
      !> vector to be unloaded
      CLASS(rvector), INTENT(IN) :: rvec
      !> edge to unload
      TYPE(edge_type), INTENT(INOUT) :: edge
      !> number of quantities to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
      !> number of side points to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
    END SUBROUTINE edge_load_save_real

!-------------------------------------------------------------------------------
!*  unload a edge save array and add to vector data
!-------------------------------------------------------------------------------
    SUBROUTINE edge_add_save_real(rvec,edge,nqty,n_side)
      USE local
      USE edge_mod
      IMPORT rvector
      !> vector to add to
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> edge to add from
      TYPE(edge_type), INTENT(INOUT) :: edge
      !> number of quantities to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
      !> number of side points to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
    END SUBROUTINE edge_add_save_real

!-------------------------------------------------------------------------------
!>  Assume vector data represents the diagonal of a matrix and apply a matrix
!   multiply.
!-------------------------------------------------------------------------------
    SUBROUTINE apply_diag_matvec_real(rvec,input_vec,output_vec)
      IMPORT rvector
      !> vector that represents a diagonal matrix
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> vector to multiply
      CLASS(rvector), INTENT(IN) :: input_vec
      !> output vector = rvec (matrix diagonal) dot input_vec
      CLASS(rvector), INTENT(INOUT) :: output_vec
    END SUBROUTINE apply_diag_matvec_real

!-------------------------------------------------------------------------------
!>  Invert the values of the vector component-wise
!-------------------------------------------------------------------------------
    SUBROUTINE invert_real(rvec)
      IMPORT rvector
      !> vector to invert
      CLASS(rvector), INTENT(INOUT) :: rvec
    END SUBROUTINE invert_real

!-------------------------------------------------------------------------------
!*  Assign to the vector for testing
!-------------------------------------------------------------------------------
    SUBROUTINE assign_for_testing_real(rvec,operation,const)
      USE local
      IMPORT rvector
      !> vector to assign to
      CLASS(rvector), INTENT(INOUT) :: rvec
      !> operation: 'constant' or 'index'
      CHARACTER(*), INTENT(IN) :: operation
      !> optional constant value for operation='constant'
      REAL(r8), INTENT(IN), OPTIONAL :: const
    END SUBROUTINE assign_for_testing_real

!-------------------------------------------------------------------------------
!*  Test if one vector is equal to another
!-------------------------------------------------------------------------------
    LOGICAL FUNCTION test_if_equal_real(rvec,rvec2) RESULT(output)
      IMPORT rvector
      !> vector
      CLASS(rvector), INTENT(IN) :: rvec
      !> vector to test against
      CLASS(rvector), INTENT(IN) :: rvec2
    END FUNCTION test_if_equal_real

!-------------------------------------------------------------------------------
!*  Deallocate the vector
!-------------------------------------------------------------------------------
    SUBROUTINE dealloc_cv1m(cv1m)
      IMPORT cvector1m
      !> vector to dealloc
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
    END SUBROUTINE dealloc_cv1m

!-------------------------------------------------------------------------------
!*  Create a new vector of the same type
!-------------------------------------------------------------------------------
    SUBROUTINE alloc_with_mold_cv1m(cv1m,new_cv1m,nqty,pd)
      USE local
      IMPORT cvector1m
      !> reference vector to mold
      CLASS(cvector1m), INTENT(IN) :: cv1m
      !> new vector to be created
      CLASS(cvector1m), ALLOCATABLE, INTENT(OUT) :: new_cv1m
      !> number of quantities, cv1m%nqty is used if unspecified
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
      !> polynomial degree, cv1m%pd is used if unspecified
      INTEGER(i4), OPTIONAL, INTENT(IN) :: pd
    END SUBROUTINE alloc_with_mold_cv1m

!-------------------------------------------------------------------------------
!*  Assign zero the vector
!-------------------------------------------------------------------------------
    SUBROUTINE zero_cv1m(cv1m)
      IMPORT cvector1m
      !> vector to assign to
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
    END SUBROUTINE zero_cv1m

!-------------------------------------------------------------------------------
!*  Assign a vector to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assign_rvec_cv1m(cv1m,rvec,r_i)
      IMPORT cvector1m
      IMPORT rvector
      !> vector to assign to
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> vector to assign
      CLASS(rvector), INTENT(IN) :: rvec
      !> 'real' for the real part of cvec, 'imag' for the imaginary part
      CHARACTER(*), INTENT(IN) :: r_i
    END SUBROUTINE assign_rvec_cv1m

!-------------------------------------------------------------------------------
!*  Partially assign a vector to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assignp_rvec_cv1m(cv1m,rvec,r_i,v1st,v2st,nq)
      USE local
      IMPORT cvector1m
      IMPORT rvector
      !> vector to assign to
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> vector to assign
      CLASS(rvector), INTENT(IN) :: rvec
      !> 'real' for the real part of cvec, 'imag' for the imaginary part
      CHARACTER(*), INTENT(IN) :: r_i
      !> quantity start for the 1st vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
      !> quantity start for the 2nd vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
      !> number of quantities to assign (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
    END SUBROUTINE assignp_rvec_cv1m

!-------------------------------------------------------------------------------
!*  Assign a cvector1m to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assign_cv1m_cv1m(cv1m,cv1m2)
      IMPORT cvector1m
      !> vector to assign to
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> vector to assign
      CLASS(cvector1m), INTENT(IN) :: cv1m2
    END SUBROUTINE assign_cv1m_cv1m

!-------------------------------------------------------------------------------
!*  Partially assign a cvector1m to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assignp_cv1m_cv1m(cv1m,cv1m2,v1st,v2st,nq)
      USE local
      IMPORT cvector1m
      !> vector to assign to
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> vector to assign
      CLASS(cvector1m), INTENT(IN) :: cv1m2
      !> quantity start for the 1st vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
      !> quantity start for the 2nd vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
      !> number of quantities to assign (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
    END SUBROUTINE

!-------------------------------------------------------------------------------
!*  Assign a cvector to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assign_cvec_cv1m(cv1m,cvec,imode)
      USE local
      IMPORT cvector1m
      IMPORT cvector
      !> vector to assign to
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> vector to assign
      CLASS(cvector), INTENT(IN) :: cvec
      !> mode to assign
      INTEGER(i4), INTENT(IN) :: imode
    END SUBROUTINE assign_cvec_cv1m

!-------------------------------------------------------------------------------
!*  Partially assign a cvector to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assignp_cvec_cv1m(cv1m,cvec,imode,v1st,v2st,nq)
      USE local
      IMPORT cvector1m
      IMPORT cvector
      !> vector to assign to
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> vector to assign
      CLASS(cvector), INTENT(IN) :: cvec
      !> mode to assign
      INTEGER(i4), INTENT(IN) :: imode
      !> quantity start for the 1st vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
      !> quantity start for the 2nd vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
      !> number of quantities to assign (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
    END SUBROUTINE assignp_cvec_cv1m

!-------------------------------------------------------------------------------
!*  add a vector to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE add_vec_cv1m(cv1m,cv1m2,v1st,v2st,nq,v1fac,v2fac)
      USE local
      IMPORT cvector1m
      !> vector to assign to
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> vector to assign
      CLASS(cvector1m), INTENT(IN) :: cv1m2
      !> quantity start for the 1st vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
      !> quantity start for the 2nd vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
      !> number of quantities to assign (default 1 when v1st or v2st is set
      !> default is all when neither v1st or v2st is set)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
      !> factor to multiply vec by (default 1)
      CLASS(*), OPTIONAL, INTENT(IN) :: v1fac
      !> factor to multiply vec2 by (default 1)
      CLASS(*), OPTIONAL, INTENT(IN) :: v2fac
    END SUBROUTINE add_vec_cv1m

!-------------------------------------------------------------------------------
!*  multiply a vector by a real scalar
!-------------------------------------------------------------------------------
    SUBROUTINE mult_rsc_cv1m(cv1m,rscalar)
      USE local
      IMPORT cvector1m
      !> vector to be multiplied
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> scalar to multiply
      REAL(r8), INTENT(IN) :: rscalar
    END SUBROUTINE mult_rsc_cv1m

!-------------------------------------------------------------------------------
!*  multiply a vector by a complex scalar
!-------------------------------------------------------------------------------
    SUBROUTINE mult_csc_cv1m(cv1m,cscalar)
      USE local
      IMPORT cvector1m
      !> vector to be multiplied
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> scalar to multiply
      COMPLEX(r8), INTENT(IN) :: cscalar
    END SUBROUTINE mult_csc_cv1m

!-------------------------------------------------------------------------------
!*  multiply a vector by a integer scalar
!-------------------------------------------------------------------------------
    SUBROUTINE mult_int_cv1m(cv1m,iscalar)
      USE local
      IMPORT cvector1m
      !> vector to be multiplied
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> scalar to multiply
      INTEGER(i4), INTENT(IN) :: iscalar
    END SUBROUTINE mult_int_cv1m

!-------------------------------------------------------------------------------
!*  compute the inf norm
!-------------------------------------------------------------------------------
    SUBROUTINE inf_norm_cv1m(cv1m,infnorm)
      USE local
      IMPORT cvector1m
      !> vector
      CLASS(cvector1m), INTENT(IN) :: cv1m
      !> inf norm contributions from the vector (size vec%inf_norm_size)
      REAL(r8), INTENT(INOUT) :: infnorm(:)
    END SUBROUTINE inf_norm_cv1m

!-------------------------------------------------------------------------------
!*  compute the L2 norm squared
!-------------------------------------------------------------------------------
    SUBROUTINE l2_norm2_cv1m(cv1m,l2norm,edge)
      USE local
      USE edge_mod
      IMPORT cvector1m
      !> vector
      CLASS(cvector1m), INTENT(IN) :: cv1m
      !> L2 norm contributions from the vector (size vec%l2_dot_size)
      REAL(r8), INTENT(INOUT) :: l2norm(:)
      !> edge data to weight edge value contribution
      TYPE(edge_type), INTENT(IN) :: edge
    END SUBROUTINE l2_norm2_cv1m

!-------------------------------------------------------------------------------
!*  compute the dot product of cv1m and cv1m2
!-------------------------------------------------------------------------------
    SUBROUTINE dot_cv1m(cv1m,cv1m2,dot,edge)
      USE local
      USE edge_mod
      IMPORT cvector1m
      !> vector
      CLASS(cvector1m), INTENT(IN) :: cv1m
      !> vector to dot
      CLASS(cvector1m), INTENT(IN) :: cv1m2
      !> dot contributions from the vector (size vec%l2_dot_size)
      COMPLEX(r8), INTENT(INOUT) :: dot(:)
      !> edge data to weight edge value contribution
      TYPE(edge_type), INTENT(IN) :: edge
    END SUBROUTINE dot_cv1m

!-------------------------------------------------------------------------------
!>  transfer contributions from integrand into internal storage as a plus
!   equals operation (the user must call %zero as needed).
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_cv1m(cv1m,integrand)
      USE local
      IMPORT cvector1m
      !> vector
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> integrand array
      COMPLEX(r8), INTENT(IN) :: integrand(:,:,:)
    END SUBROUTINE assemble_cv1m

!-------------------------------------------------------------------------------
!*  Applies dirichlet boundary conditions
!-------------------------------------------------------------------------------
    SUBROUTINE dirichlet_bc_cv1m(cv1m,component,edge,symm,seam_save)
      USE edge_mod
      IMPORT cvector1m
      !> vector
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> flag to determine normal/tangential/scalar behavior
      CHARACTER(*), INTENT(IN) :: component
      !> associated edge
      TYPE(edge_type), INTENT(INOUT) :: edge
      !> flag for symmetric boundary
      CHARACTER(*), OPTIONAL, INTENT(IN) :: symm
      !> save the existing boundary data in seam_save
      LOGICAL, OPTIONAL, INTENT(IN) :: seam_save
    END SUBROUTINE dirichlet_bc_cv1m

!-------------------------------------------------------------------------------
!*  Applies regularity conditions at R=0 in toroidal geometry
!-------------------------------------------------------------------------------
    SUBROUTINE regularity_cv1m(cv1m,edge,flag)
      USE edge_mod
      IMPORT cvector1m
      !> vector
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> associated edge
      TYPE(edge_type), INTENT(IN) :: edge
      !> flag
      CHARACTER(*), INTENT(IN) :: flag
    END SUBROUTINE regularity_cv1m

!-------------------------------------------------------------------------------
!*  set variables needed by edge routines
!-------------------------------------------------------------------------------
    SUBROUTINE set_edge_vars_cv1m(cv1m,edge)
      USE local
      USE edge_mod
      IMPORT cvector1m
      !> vector
      CLASS(cvector1m), INTENT(IN) :: cv1m
      !> edge to set vars in
      TYPE(edge_type), INTENT(INOUT) :: edge
    END SUBROUTINE set_edge_vars_cv1m

!-------------------------------------------------------------------------------
!*  load a edge communitcation array with vector data
!-------------------------------------------------------------------------------
    SUBROUTINE edge_load_arr_cv1m(cv1m,edge,nqty,n_side,do_avg)
      USE local
      USE edge_mod
      IMPORT cvector1m
      !> vector to unload
      CLASS(cvector1m), INTENT(IN) :: cv1m
      !> edge to load
      TYPE(edge_type), INTENT(INOUT) :: edge
      !> number of quantities to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
      !> number of side points to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
      !> apply edge average if true
      LOGICAL, OPTIONAL, INTENT(IN) :: do_avg
    END SUBROUTINE edge_load_arr_cv1m

!-------------------------------------------------------------------------------
!*  unload a edge communication array with vector data
!-------------------------------------------------------------------------------
    SUBROUTINE edge_unload_arr_cv1m(cv1m,edge)
      USE local
      USE edge_mod
      IMPORT cvector1m
      !> vector to load
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> edge to unload
      TYPE(edge_type), INTENT(INOUT) :: edge
    END SUBROUTINE edge_unload_arr_cv1m

!-------------------------------------------------------------------------------
!*  load a edge save array with vector data
!-------------------------------------------------------------------------------
    SUBROUTINE edge_load_save_cv1m(cv1m,edge,nqty,n_side)
      USE local
      USE edge_mod
      IMPORT cvector1m
      !> vector to unload
      CLASS(cvector1m), INTENT(IN) :: cv1m
      !> edge to load
      TYPE(edge_type), INTENT(INOUT) :: edge
      !> number of quantities to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
      !> number of side points to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
    END SUBROUTINE edge_load_save_cv1m

!-------------------------------------------------------------------------------
!*  unload a edge save array and add to vector data
!-------------------------------------------------------------------------------
    SUBROUTINE edge_add_save_cv1m(cv1m,edge,nqty,n_side)
      USE local
      USE edge_mod
      IMPORT cvector1m
      !> vector to add to
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> edge to add from
      TYPE(edge_type), INTENT(INOUT) :: edge
      !> number of quantities to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
      !> number of side points to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
    END SUBROUTINE edge_add_save_cv1m

!-------------------------------------------------------------------------------
!>  Assume vector data represents the diagonal of a matrix and apply a matrix
!   multiply.
!-------------------------------------------------------------------------------
    SUBROUTINE apply_diag_matvec_cv1m(cv1m,input_vec,output_vec)
      IMPORT cvector1m
      !> vector that represents a diagonal matrix
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> vector to multiply
      CLASS(cvector1m), INTENT(IN) :: input_vec
      !> output vector = rvec (matrix diagonal) dot input_vec
      CLASS(cvector1m), INTENT(INOUT) :: output_vec
    END SUBROUTINE apply_diag_matvec_cv1m

!-------------------------------------------------------------------------------
!>  Invert the values of the vector component-wise
!-------------------------------------------------------------------------------
    SUBROUTINE invert_cv1m(cv1m)
      IMPORT cvector1m
      !> vector to invert
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
    END SUBROUTINE invert_cv1m

!-------------------------------------------------------------------------------
!*  Assign one the vector
!-------------------------------------------------------------------------------
    SUBROUTINE assign_for_testing_cv1m(cv1m,operation,const)
      USE local
      IMPORT cvector1m
      !> vector to assign to
      CLASS(cvector1m), INTENT(INOUT) :: cv1m
      !> operation: 'constant' or 'index'
      CHARACTER(*), INTENT(IN) :: operation
      !> optional constant value for operation='constant'
      COMPLEX(r8), INTENT(IN), OPTIONAL :: const
    END SUBROUTINE assign_for_testing_cv1m

!-------------------------------------------------------------------------------
!*  Test if one vector is equal to another
!-------------------------------------------------------------------------------
    LOGICAL FUNCTION test_if_equal_cv1m(cv1m,cv1m2) RESULT(output)
      IMPORT cvector1m
      !> vector
      CLASS(cvector1m), INTENT(IN) :: cv1m
      !> vector to test against
      CLASS(cvector1m), INTENT(IN) :: cv1m2
    END FUNCTION test_if_equal_cv1m

!-------------------------------------------------------------------------------
!*  Deallocate the vector
!-------------------------------------------------------------------------------
    SUBROUTINE dealloc_comp(cvec)
      IMPORT cvector
      !> vector to dealloc
      CLASS(cvector), INTENT(INOUT) :: cvec
    END SUBROUTINE dealloc_comp

!-------------------------------------------------------------------------------
!*  Create a new vector of the same type
!-------------------------------------------------------------------------------
    SUBROUTINE alloc_with_mold_comp(cvec,new_cvec,nqty,pd,nmodes)
      USE local
      IMPORT cvector
      !> reference cvector to mold
      CLASS(cvector), INTENT(IN) :: cvec
      !> new cvector to be created
      CLASS(cvector), ALLOCATABLE, INTENT(OUT) :: new_cvec
      !> number of quantities, cvec%nqty is used if unspecified
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
      !> polynomial degree, cvec%pd is used if unspecified
      INTEGER(i4), OPTIONAL, INTENT(IN) :: pd
      !> number of Fourier components, cvec%nmodes is used if unspecified
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nmodes
    END SUBROUTINE alloc_with_mold_comp

!-------------------------------------------------------------------------------
!*  Create a new cvector1m from this cvector
!-------------------------------------------------------------------------------
    SUBROUTINE alloc_with_mold_cv1m_comp(cvec,new_cv1m,nqty,pd)
      USE local
      IMPORT cvector
      IMPORT cvector1m
      !> reference cvector to mold
      CLASS(cvector), INTENT(IN) :: cvec
      !> new cvector1m to be created
      CLASS(cvector1m), ALLOCATABLE, INTENT(OUT) :: new_cv1m
      !> number of quantities, cvec%nqty is used if unspecified
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
      !> polynomial degree, cvec%pd is used if unspecified
      INTEGER(i4), OPTIONAL, INTENT(IN) :: pd
    END SUBROUTINE alloc_with_mold_cv1m_comp

!-------------------------------------------------------------------------------
!*  Assign zero to the vector
!-------------------------------------------------------------------------------
    SUBROUTINE zero_comp(cvec)
      IMPORT cvector
      !> vector to assign to
      CLASS(cvector), INTENT(INOUT) :: cvec
    END SUBROUTINE zero_comp

!-------------------------------------------------------------------------------
!*  Assign a complex scalar to the vector
!-------------------------------------------------------------------------------
    SUBROUTINE assign_csc_comp(cvec,cscalar)
      USE local
      IMPORT cvector
      !> vector to assign to
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> scalar to assign
      COMPLEX(r8), INTENT(IN) :: cscalar
    END SUBROUTINE assign_csc_comp

!-------------------------------------------------------------------------------
!*  Assign an integer scalar to the vector
!-------------------------------------------------------------------------------
    SUBROUTINE assign_int_comp(cvec,iscalar)
      USE local
      IMPORT cvector
      !> vector to assign to
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> scalar to assign
      INTEGER(i4), INTENT(IN) :: iscalar
    END SUBROUTINE assign_int_comp

!-------------------------------------------------------------------------------
!*  Assign a vector to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assign_rvec_comp(cvec,rvec,imode,r_i)
      USE local
      IMPORT cvector
      IMPORT rvector
      !> vector to assign to
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> vector to assign
      CLASS(rvector), INTENT(IN) :: rvec
      !> mode to assign to
      INTEGER(i4), INTENT(IN) :: imode
      !> 'real' for the real part of cvec, 'imag' for the imaginary part
      CHARACTER(*), INTENT(IN) :: r_i
    END SUBROUTINE assign_rvec_comp

!-------------------------------------------------------------------------------
!*  Partially assign a vector to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assignp_rvec_comp(cvec,rvec,imode,r_i,v1st,v2st,nq)
      USE local
      IMPORT cvector
      IMPORT rvector
      !> vector to assign to
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> vector to assign
      CLASS(rvector), INTENT(IN) :: rvec
      !> mode to assign to
      INTEGER(i4), INTENT(IN) :: imode
      !> 'real' for the real part of cvec, 'imag' for the imaginary part
      CHARACTER(*), INTENT(IN) :: r_i
      !> quantity start for the 1st vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
      !> quantity start for the 2nd vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
      !> number of quantities to assign (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
    END SUBROUTINE assignp_rvec_comp

!-------------------------------------------------------------------------------
!*  Assign a cvector1m to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assign_cv1m_comp(cvec,cv1m,imode)
      USE local
      IMPORT cvector
      IMPORT cvector1m
      !> vector to assign to
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> vector to assign
      CLASS(cvector1m), INTENT(IN) :: cv1m
      !> mode to assign to
      INTEGER(i4), INTENT(IN) :: imode
    END SUBROUTINE assign_cv1m_comp

!-------------------------------------------------------------------------------
!*  Partially assign a cvector1m to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assignp_cv1m_comp(cvec,cv1m,imode,v1st,v2st,nq)
      USE local
      IMPORT cvector
      IMPORT cvector1m
      !> vector to assign to
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> vector to assign
      CLASS(cvector1m), INTENT(IN) :: cv1m
      !> mode to assign to
      INTEGER(i4), INTENT(IN) :: imode
      !> quantity start for the 1st vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
      !> quantity start for the 2nd vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
      !> number of quantities to assign (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
    END SUBROUTINE

!-------------------------------------------------------------------------------
!*  Assign a cvector to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assign_cvec_comp(cvec,cvec2)
      IMPORT cvector
      !> vector to assign to
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> vector to assign
      CLASS(cvector), INTENT(IN) :: cvec2
    END SUBROUTINE assign_cvec_comp

!-------------------------------------------------------------------------------
!*  Partially assign a cvector to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE assignp_cvec_comp(cvec,cvec2,v1st,v2st,nq)
      USE local
      IMPORT cvector
      !> vector to assign to
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> vector to assign
      CLASS(cvector), INTENT(IN) :: cvec2
      !> quantity start for the 1st vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
      !> quantity start for the 2nd vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
      !> number of quantities to assign (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
    END SUBROUTINE assignp_cvec_comp

!-------------------------------------------------------------------------------
!*  add a vector to a vector
!-------------------------------------------------------------------------------
    SUBROUTINE add_vec_comp(cvec,cvec2,v1st,v2st,nq,v1fac,v2fac)
      USE local
      IMPORT cvector
      !> vector to assign to
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> vector to assign
      CLASS(cvector), INTENT(IN) :: cvec2
      !> quantity start for the 1st vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
      !> quantity start for the 2nd vector (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
      !> number of quantities to assign (default 1 when v1st or v2st is set
      !> default is all when neither v1st or v2st is set)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
      !> factor to multiply vec by (default 1)
      CLASS(*), OPTIONAL, INTENT(IN) :: v1fac
      !> factor to multiply vec2 by (default 1)
      CLASS(*), OPTIONAL, INTENT(IN) :: v2fac
    END SUBROUTINE add_vec_comp

!-------------------------------------------------------------------------------
!*  multiply a vector by a real scalar
!-------------------------------------------------------------------------------
    SUBROUTINE mult_rsc_comp(cvec,rscalar)
      USE local
      IMPORT cvector
      !> vector to be multiplied
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> scalar to multiply
      REAL(r8), INTENT(IN) :: rscalar
    END SUBROUTINE mult_rsc_comp

!-------------------------------------------------------------------------------
!*  multiply a vector by a complex scalar
!-------------------------------------------------------------------------------
    SUBROUTINE mult_csc_comp(cvec,cscalar)
      USE local
      IMPORT cvector
      !> vector to be multiplied
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> scalar to multiply
      COMPLEX(r8), INTENT(IN) :: cscalar
    END SUBROUTINE mult_csc_comp

!-------------------------------------------------------------------------------
!*  multiply a vector by a integer scalar
!-------------------------------------------------------------------------------
    SUBROUTINE mult_int_comp(cvec,iscalar)
      USE local
      IMPORT cvector
      !> vector to be multiplied
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> scalar to multiply
      INTEGER(i4), INTENT(IN) :: iscalar
    END SUBROUTINE mult_int_comp

!-------------------------------------------------------------------------------
!*  compute the inf norm
!-------------------------------------------------------------------------------
    SUBROUTINE inf_norm_comp(cvec,infnorm)
      USE local
      IMPORT cvector
      !> vector
      CLASS(cvector), INTENT(IN) :: cvec
      !> inf norm contributions from the vector (size vec%inf_norm_size)
      REAL(r8), INTENT(INOUT) :: infnorm(:)
    END SUBROUTINE inf_norm_comp

!-------------------------------------------------------------------------------
!*  compute the L2 norm squared
!-------------------------------------------------------------------------------
    SUBROUTINE l2_norm2_comp(cvec,l2norm,edge)
      USE local
      USE edge_mod
      IMPORT cvector
      !> vector
      CLASS(cvector), INTENT(IN) :: cvec
      !> L2 norm contributions from the vector (size vec%l2_dot_size)
      REAL(r8), INTENT(INOUT) :: l2norm(:)
      !> edge data to weight edge value contribution
      TYPE(edge_type), INTENT(IN) :: edge
    END SUBROUTINE l2_norm2_comp

!-------------------------------------------------------------------------------
!*  compute the dot product of cvec and cvec2
!-------------------------------------------------------------------------------
    SUBROUTINE dot_comp(cvec,cvec2,dot,edge)
      USE local
      USE edge_mod
      IMPORT cvector
      !> vector
      CLASS(cvector), INTENT(IN) :: cvec
      !> vector to dot
      CLASS(cvector), INTENT(IN) :: cvec2
      !> dot contributions from the vector (size vec%l2_dot_size)
      COMPLEX(r8), INTENT(INOUT) :: dot(:)
      !> edge data to weight edge value contribution
      TYPE(edge_type), INTENT(IN) :: edge
    END SUBROUTINE dot_comp

!-------------------------------------------------------------------------------
!>  transfer contributions from integrand into internal storage as a plus
!   equals operation (the user must call %zero as needed).
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_comp(cvec,integrand)
      USE local
      IMPORT cvector
      !> vector
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> integrand array
      COMPLEX(r8), INTENT(IN) :: integrand(:,:,:,:)
    END SUBROUTINE assemble_comp

!-------------------------------------------------------------------------------
!*  Applies dirichlet boundary conditions
!-------------------------------------------------------------------------------
    SUBROUTINE dirichlet_bc_comp(cvec,component,edge,symm,seam_save)
      USE edge_mod
      IMPORT cvector
      !> vector
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> flag to determine normal/tangential/scalar behavior
      CHARACTER(*), INTENT(IN) :: component
      !> associated edge
      TYPE(edge_type), INTENT(INOUT) :: edge
      !> flag for symmetric boundary
      CHARACTER(*), OPTIONAL, INTENT(IN) :: symm
      !> save the existing boundary data in seam_save
      LOGICAL, OPTIONAL, INTENT(IN) :: seam_save
    END SUBROUTINE dirichlet_bc_comp

!-------------------------------------------------------------------------------
!*  Applies regularity conditions at R=0 in toroidal geometry
!-------------------------------------------------------------------------------
    SUBROUTINE regularity_comp(cvec,edge,nindex,flag)
      USE local
      USE edge_mod
      IMPORT cvector
      !> vector
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> associated edge
      TYPE(edge_type), INTENT(IN) :: edge
      !> mode number array associated with vector
      INTEGER(i4), INTENT(IN) :: nindex(cvec%nmodes)
      !> flag
      CHARACTER(*), INTENT(IN) :: flag
    END SUBROUTINE regularity_comp

!-------------------------------------------------------------------------------
!*  set variables needed by edge routines
!-------------------------------------------------------------------------------
    SUBROUTINE set_edge_vars_comp(cvec,edge)
      USE local
      USE edge_mod
      IMPORT cvector
      !> vector
      CLASS(cvector), INTENT(IN) :: cvec
      !> edge to set vars in
      TYPE(edge_type), INTENT(INOUT) :: edge
    END SUBROUTINE set_edge_vars_comp

!-------------------------------------------------------------------------------
!*  load a edge communitcation array with vector data
!-------------------------------------------------------------------------------
    SUBROUTINE edge_load_arr_comp(cvec,edge,nqty,n_side,ifs,ife,do_avg)
      USE local
      USE edge_mod
      IMPORT cvector
      !> vector to unload
      CLASS(cvector), INTENT(IN) :: cvec
      !> edge to load
      TYPE(edge_type), INTENT(INOUT) :: edge
      !> number of quantities to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
      !> number of side points to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
      !> first Fourier mode to load (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: ifs
      !> last Fourier mode to load (default nmodes)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: ife
      !> apply edge average if true
      LOGICAL, OPTIONAL, INTENT(IN) :: do_avg
    END SUBROUTINE edge_load_arr_comp

!-------------------------------------------------------------------------------
!*  unload a edge communication array with vector data
!-------------------------------------------------------------------------------
    SUBROUTINE edge_unload_arr_comp(cvec,edge)
      USE local
      USE edge_mod
      IMPORT cvector
      !> vector to load
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> edge to unload
      TYPE(edge_type), INTENT(INOUT) :: edge
    END SUBROUTINE edge_unload_arr_comp

!-------------------------------------------------------------------------------
!*  load a edge save array with vector data
!-------------------------------------------------------------------------------
    SUBROUTINE edge_load_save_comp(cvec,edge,nqty,n_side,ifs,ife)
      USE local
      USE edge_mod
      IMPORT cvector
      !> vector to unload
      CLASS(cvector), INTENT(IN) :: cvec
      !> edge to load
      TYPE(edge_type), INTENT(INOUT) :: edge
      !> number of quantities to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
      !> number of side points to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
      !> first Fourier mode to load (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: ifs
      !> last Fourier mode to load (default nmodes)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: ife
    END SUBROUTINE edge_load_save_comp

!-------------------------------------------------------------------------------
!*  unload a edge save array and add to vector data
!-------------------------------------------------------------------------------
    SUBROUTINE edge_add_save_comp(cvec,edge,nqty,n_side,ifs,ife)
      USE local
      USE edge_mod
      IMPORT cvector
      !> vector to add to
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> edge to add from
      TYPE(edge_type), INTENT(INOUT) :: edge
      !> number of quantities to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
      !> number of side points to load (default all)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
      !> first Fourier mode to load (default 1)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: ifs
      !> last Fourier mode to load (default nmodes)
      INTEGER(i4), OPTIONAL, INTENT(IN) :: ife
    END SUBROUTINE edge_add_save_comp

!-------------------------------------------------------------------------------
!>  Assume vector data represents the diagonal of a matrix and apply a matrix
!   multiply.
!-------------------------------------------------------------------------------
    SUBROUTINE apply_diag_matvec_comp(cvec,input_vec,output_vec)
      IMPORT cvector
      !> vector that represents a diagonal matrix
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> vector to multiply
      CLASS(cvector), INTENT(IN) :: input_vec
      !> output vector = rvec (matrix diagonal) dot input_vec
      CLASS(cvector), INTENT(INOUT) :: output_vec
    END SUBROUTINE apply_diag_matvec_comp

!-------------------------------------------------------------------------------
!>  Invert the values of the vector component-wise
!-------------------------------------------------------------------------------
    SUBROUTINE invert_comp(cvec)
      IMPORT cvector
      !> vector to invert
      CLASS(cvector), INTENT(INOUT) :: cvec
    END SUBROUTINE invert_comp

!-------------------------------------------------------------------------------
!*  Assign one to the vector
!-------------------------------------------------------------------------------
    SUBROUTINE assign_for_testing_comp(cvec,operation,const)
      USE local
      IMPORT cvector
      !> vector to assign to
      CLASS(cvector), INTENT(INOUT) :: cvec
      !> operation: 'constant' or 'index'
      CHARACTER(*), INTENT(IN) :: operation
      !> optional constant value for operation='constant'
      COMPLEX(r8), INTENT(IN), OPTIONAL :: const
    END SUBROUTINE assign_for_testing_comp

!-------------------------------------------------------------------------------
!*  Test if one vector is equal to another
!-------------------------------------------------------------------------------
    LOGICAL FUNCTION test_if_equal_comp(cvec,cvec2) RESULT(output)
      IMPORT cvector
      !> vector
      CLASS(cvector), INTENT(IN) :: cvec
      !> vector to test against
      CLASS(cvector), INTENT(IN) :: cvec2
    END FUNCTION test_if_equal_comp
  END INTERFACE

END MODULE vector_mod
