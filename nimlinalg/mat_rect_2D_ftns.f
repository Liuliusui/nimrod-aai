!-------------------------------------------------------------------------------
!< module containing shared routines that performs standard operations on
!  rectilinear matrices.
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!> module containing shared routines that performs standard operations on
!  rectilinear matrices.
!-------------------------------------------------------------------------------
MODULE mat_rect_2D_ftns_mod
  USE timer_mod
  IMPLICIT NONE

  CHARACTER(*), PARAMETER, PRIVATE :: mod_name='mat_rect_2D_ftns_mod'
CONTAINS

!-------------------------------------------------------------------------------
!> initialize arrays that help translate element-based data.
!-------------------------------------------------------------------------------
  SUBROUTINE matrix_rbl_dof_init(dib,div,dix,diy,diq,nqc,pdc,nqd,nbd)
    USE local
    IMPLICIT NONE

    INTEGER(i4), DIMENSION(:), INTENT(OUT) :: dib,div,dix,diy,diq
    INTEGER(i4), INTENT(IN) :: nqc,pdc,nqd,nbd

    INTEGER(i4) :: ix,iy,iq,ii,id
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matrix_rbl_dof_init',iftn,idepth)
!-------------------------------------------------------------------------------
!   fill the dof arrays that are used to generate indices
!   when transferring information into rmat arrays.
!-------------------------------------------------------------------------------
    ii=1
    DO iy=0,1        !     4 continuous grid-centered bases
      DO ix=0,1
        DO iq=1,nqc
          dib(ii)=ix+2*iy+1
          div(ii)=iq
          dix(ii)=ix-1
          diy(ii)=iy-1
          diq(ii)=iq
          ii=ii+1
        ENDDO
      ENDDO
    ENDDO
    DO id=0,pdc-2    !     continuous horizontal side bases
      DO iy=0,1
        DO iq=1,nqc
          dib(ii)=iy+2*id+5
          div(ii)=iq
          dix(ii)=0
          diy(ii)=iy-1
          diq(ii)=iq+nqc*id
          ii=ii+1
        ENDDO
      ENDDO
    ENDDO
    DO id=0,pdc-2    !     continuous vertical side bases
      DO ix=0,1
        DO iq=1,nqc
          dib(ii)=ix+2*id+5+2*(pdc-1)
          div(ii)=iq
          dix(ii)=ix-1
          diy(ii)=0
          diq(ii)=iq+nqc*id
          ii=ii+1
        ENDDO
      ENDDO
    ENDDO
    DO id=0,(pdc-1)**2-1  !  interior bases for continuous expansions
      DO iq=1,nqc
        dib(ii)=id+1+4*pdc
        div(ii)=iq
        dix(ii)=0
        diy(ii)=0
        diq(ii)=iq+nqc*id
        ii=ii+1
      ENDDO
    ENDDO
    DO id=0,nbd-1    !  bases for discontinuous expansions
      DO iq=1,nqd
        dib(ii)=id+1+MIN(nqc,1_i4)*(pdc+1)**2  ! continue interior #s
        div(ii)=iq
        dix(ii)=0
        diy(ii)=0
        diq(ii)=iq+nqd*id+nqc*(pdc-1)**2
        ii=ii+1
      ENDDO
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matrix_rbl_dof_init

END MODULE mat_rect_2D_ftns_mod
