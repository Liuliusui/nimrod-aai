!-------------------------------------------------------------------------------
!! Test for rblock
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!> Program to test rblock implementation
!-------------------------------------------------------------------------------
MODULE block_registry_mod
  USE local
  USE pardata_mod
  IMPLICIT NONE

  CHARACTER(64) :: block_field_id_string
  INTEGER(i4) :: nbl=-1 ! must have factor of 2 for dump test
  CHARACTER(64) :: block_test_types(1)= ["rblock"]
  INTEGER(i4), PRIVATE, PARAMETER :: nbl_rblock=8
CONTAINS

!-------------------------------------------------------------------------------
!* set internal variables for test_type
!-------------------------------------------------------------------------------
  SUBROUTINE set_block_test_type(test_type)
    IMPLICIT NONE

    !* block identification string
    CHARACTER(*), INTENT(IN) :: test_type

    SELECT CASE(test_type)
    CASE ("rblock")
      nbl=nbl_rblock
      block_field_id_string="rect_2D"
    CASE DEFAULT
      CALL par%nim_stop('setup_block:: invalid test_type')
    END SELECT
  END SUBROUTINE set_block_test_type

!-------------------------------------------------------------------------------
!> routine to set up block structures for testing, par%init should be
!  called first.
!-------------------------------------------------------------------------------
  SUBROUTINE setup_block(test_type,bl)
    USE gblock_mod
    IMPLICIT NONE

    !* block identification string
    CHARACTER(*), INTENT(IN) :: test_type
    !* test block to setup
    CLASS(block_storage), INTENT(INOUT) :: bl(nbl)

    SELECT CASE(test_type)
    CASE ("rblock")
      CALL setup_rblock
    CASE DEFAULT
      CALL par%nim_stop('setup_block:: invalid test_type')
    END SELECT
  CONTAINS

!-------------------------------------------------------------------------------
!*  set up rblock structures for testing
!-------------------------------------------------------------------------------
    SUBROUTINE setup_rblock
      USE fem_registry_mod
      USE nodal_mod
      USE rblock_mod
      IMPLICIT NONE

      INTEGER(i4), PARAMETER :: mx(nbl_rblock)=                                 &
        [2_i4,4_i4,4_i4,9_i4,6_i4,7_i4,3_i4,4_i4]
      INTEGER(i4), PARAMETER :: my(nbl_rblock)=                                 &
        [3_i4,5_i4,7_i4,2_i4,5_i4,7_i4,8_i4,5_i4]
      INTEGER(i4), PARAMETER :: pd(nbl_rblock)=                                 &
        [3_i4,5_i4,7_i4,6_i4,4_i4,2_i4,1_i4,4_i4]
      LOGICAL, PARAMETER :: r0block(nbl_rblock)=                                &
        [.TRUE.,.FALSE.,.TRUE.,.FALSE.,.TRUE.,.FALSE.,.TRUE.,.FALSE.]
      INTEGER(i4), PARAMETER :: nqty(nbl_rblock)=                               &
        [2_i4,3_i4,3_i4,2_i4,1_i4,1_i4,2_i4,3_i4]
      CLASS(nodal_field_comp), ALLOCATABLE :: cfield
      CLASS(nodal_field_real), ALLOCATABLE :: rfield
      INTEGER(i4) :: ib
!-------------------------------------------------------------------------------
!     Allocate test rblocks
!-------------------------------------------------------------------------------
      ntests=nbl
      DO ib=1,nbl
        ALLOCATE(rblock::bl(ib)%b)
        SELECT TYPE(bl=>bl(ib)%b)
        TYPE IS (rblock)
          CALL bl%alloc(par%loc2glob(ib),ib,mx(ib),my(ib),pd(ib),r0block(ib))
        END SELECT
        CALL set_fem_input_vars(nqty(ib),mx(ib),my(ib),pd(ib),par%nmodes)
!-------------------------------------------------------------------------------
!       setup the mesh
!-------------------------------------------------------------------------------
        CALL setup_fem('coord_field_2D',cfield,rfield,bl(ib)%b%mesh,ib)
      ENDDO
    END SUBROUTINE setup_rblock

  END SUBROUTINE setup_block

END MODULE block_registry_mod
