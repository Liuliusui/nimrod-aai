!-------------------------------------------------------------------------------
!! Module to log timing information
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> module to log timing information
!
! The timer has three levels. Level 0 is reserved for application timings based
! on physical-equation advance routines and should not be used in the
! infrastructure. Level 1 should be used for high-level functions that are
! anticipated consume 1%+ of the loop time. Level 2 should be used for should
! be used for everything else.
!
! It is anticipated that the code will typically be run without level 2 timings
! and that these can be enabled at compile time to identify hot spots.
!
! The interface requires three lines per function and no preprocessor
! statements. By making the timer calls an empty pure subroutine when they are
! not enabled they can be optimized out (this may require interprocedure
! optimization though).
!-------------------------------------------------------------------------------
MODULE timer_mod
  USE local
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: timer ! static instance

  INTEGER(i4), PARAMETER :: max_depth=1024_i4
  INTEGER(i4), PARAMETER :: max_functions=16384_i4
!-------------------------------------------------------------------------------
!* container for timing information
!-------------------------------------------------------------------------------
  TYPE :: timer_type
    REAL(r8), ALLOCATABLE :: time_array(:)
    REAL(r8), ALLOCATABLE :: exclude_time(:)
    CHARACTER(8), ALLOCATABLE :: tag_name(:)
    CHARACTER(32), ALLOCATABLE :: function_name(:)
    REAL(r8), ALLOCATABLE :: time_by_function_inclusive(:)
    REAL(r8), ALLOCATABLE :: time_by_function_exclusive(:)
    INTEGER(i4), ALLOCATABLE :: calls_by_function(:)
    INTEGER(i4) :: nfunc=0_i4
    INTEGER(i4) :: ndepth=0_i4
    LOGICAL :: call_directive_wait=.FALSE.
  CONTAINS

    PROCEDURE, PASS :: init
    PROCEDURE, PASS :: finalize
    PROCEDURE, PASS :: report
    PROCEDURE, PASS :: start_timer_l0
    PROCEDURE, PASS :: end_timer_l0
    PROCEDURE, PASS :: start_timer_l1
    PROCEDURE, PASS :: end_timer_l1
    PROCEDURE, PASS :: start_timer_l2
    PROCEDURE, PASS :: end_timer_l2
  END TYPE timer_type

!-------------------------------------------------------------------------------
!* static timer instance
!-------------------------------------------------------------------------------
  TYPE(timer_type) :: timer
CONTAINS

!-------------------------------------------------------------------------------
!> Initialize timer
!  Static variable sizes with large arrays can cause timer to be moved from
!  stack to static storage with gcc. Do this to avoid.
!-------------------------------------------------------------------------------
  SUBROUTINE init(tt,directive_wait)
    IMPLICIT NONE

    !> timer instance
    CLASS(timer_type), INTENT(INOUT) :: tt
    !> call directive based wait before end_timer
    LOGICAL, OPTIONAL, INTENT(IN) :: directive_wait

    ALLOCATE(tt%time_array(max_depth))
    ALLOCATE(tt%exclude_time(max_depth))
    tt%exclude_time=0._r8
    ALLOCATE(tt%tag_name(max_functions))
    ALLOCATE(tt%function_name(max_functions))
    ALLOCATE(tt%time_by_function_inclusive(max_functions))
    tt%time_by_function_inclusive=0._r8
    ALLOCATE(tt%time_by_function_exclusive(max_functions))
    tt%time_by_function_exclusive=0._r8
    ALLOCATE(tt%calls_by_function(max_functions))
    tt%calls_by_function=0_i4
    IF (PRESENT(directive_wait)) THEN
      IF (directive_wait) tt%call_directive_wait=.TRUE.
    ENDIF
  END SUBROUTINE init

!-------------------------------------------------------------------------------
!> Finalize timer
!  Deallocate variables
!-------------------------------------------------------------------------------
  SUBROUTINE finalize(tt)
    USE pardata_mod
    IMPLICIT NONE

    !> timer instance
    CLASS(timer_type), INTENT(INOUT) :: tt

#ifdef DEBUG
    IF (tt%ndepth/=0) THEN
      CALL par%nim_stop('not ok, timer not at depth zero')
    ENDIF
#endif
    DEALLOCATE(tt%time_array,tt%exclude_time,tt%tag_name,tt%function_name)
    DEALLOCATE(tt%time_by_function_inclusive,tt%time_by_function_exclusive)
    DEALLOCATE(tt%calls_by_function)
  END SUBROUTINE finalize

!-------------------------------------------------------------------------------
!> report timings to output. This outputs function names, their associated tag
!  classification, the number of times they have been called, the function run
!  time inclusive of all subsequent internal timed calls, and the function run
!  time exclusive of all subsequent timed calls.
!
!  The output can be reduced and ordered by the tag_list
!-------------------------------------------------------------------------------
  SUBROUTINE report(tt,tag_list)
    USE pardata_mod
    IMPLICIT NONE

    !> timer instance
    CLASS(timer_type), INTENT(INOUT) :: tt
    !> list of tags to output in reverse order (max 8 charaters each)
    CHARACTER(*), OPTIONAL, INTENT(IN) :: tag_list(:)

    INTEGER(i4) :: iftn,itag
    CHARACTER(128) :: msg

    CALL par%nim_write('timing statistics in seconds:')
    CALL par%nim_write('function name                   '//                     &
                       'tag     # calls  inclusive   exclusive')
    IF (PRESENT(tag_list)) THEN
      DO itag=SIZE(tag_list),1,-1
        DO iftn=1,tt%nfunc
          IF (tt%tag_name(iftn)==tag_list(itag)) THEN
            WRITE(msg,'(2a,i8,2es12.5)') tt%function_name(iftn),                &
                       tt%tag_name(iftn),                                       &
                       tt%calls_by_function(iftn),                              &
                       tt%time_by_function_inclusive(iftn),                     &
                       tt%time_by_function_exclusive(iftn)
            CALL par%nim_write(msg)
          ENDIF
        ENDDO
      ENDDO
    ELSE
      DO iftn=1,tt%nfunc
        WRITE(msg,'(2a,i8,2es12.5)') tt%function_name(iftn),                    &
                   tt%tag_name(iftn),                                           &
                   tt%calls_by_function(iftn),                                  &
                   tt%time_by_function_inclusive(iftn),                         &
                   tt%time_by_function_exclusive(iftn)
        CALL par%nim_write(msg)
      ENDDO
    ENDIF
  END SUBROUTINE report

!-------------------------------------------------------------------------------
!* start timing for high-level calls
!-------------------------------------------------------------------------------
  SUBROUTINE start_timer_l0(tt,tag,ftn_name,iftn,idepth)
#ifdef NVTX_PROFILE
    USE nvtx
#endif
    IMPLICIT NONE

    !> timer instance
    CLASS(timer_type), INTENT(INOUT) :: tt
    !> tag to classify grouping for output (max 8 characters)
    CHARACTER(*), INTENT(IN) :: tag
    !> function name (max 32 characters)
    CHARACTER(*), INTENT(IN) :: ftn_name
    !> function index, must be zero on first call, must have SAVE attribute
    !  in caller
    INTEGER(i4), INTENT(INOUT) :: iftn
    !> function call depth to pass to end_timer call
    INTEGER(i4), INTENT(INOUT) :: idepth

    IF (iftn<=0) THEN
      tt%nfunc=tt%nfunc+1
      iftn=tt%nfunc
      tt%function_name(iftn)=TRIM(ftn_name)
      tt%tag_name(iftn)=TRIM(tag)
    ENDIF
    tt%ndepth=tt%ndepth+1
    idepth=tt%ndepth
    CALL system_timer(tt%time_array(idepth))
    tt%calls_by_function(iftn)=tt%calls_by_function(iftn)+1
#ifdef NVTX_PROFILE
    CALL nvtxstartrange(TRIM(tag)//"_"//TRIM(ftn_name))
#endif
  END SUBROUTINE start_timer_l0

!-------------------------------------------------------------------------------
!* end timing for high-level calls
!-------------------------------------------------------------------------------
  SUBROUTINE end_timer_l0(tt,iftn,idepth)
#ifdef NVTX_PROFILE
    USE nvtx
#endif
    IMPLICIT NONE

    !> timer instance
    CLASS(timer_type), INTENT(INOUT) :: tt
    !> function index
    INTEGER(i4), INTENT(IN) :: iftn
    !> function call depth to pass to end_timer call
    INTEGER(i4), INTENT(IN) :: idepth

    REAL(r8) :: end_time,tot_time

    IF (tt%call_directive_wait) THEN
      !$acc wait
    ENDIF
    tt%ndepth=tt%ndepth-1
    CALL system_timer(end_time)
    tot_time=end_time-tt%time_array(idepth)
    tt%time_by_function_inclusive(iftn)=                                        &
      tt%time_by_function_inclusive(iftn)+tot_time
    IF (idepth-1 > 0) tt%exclude_time(idepth-1)=                                &
                        tt%exclude_time(idepth-1)+tot_time
    tot_time=tot_time-tt%exclude_time(idepth)
    tt%exclude_time(idepth)=0._r8
    tt%time_by_function_exclusive(iftn)=                                        &
      tt%time_by_function_exclusive(iftn)+tot_time
#ifdef NVTX_PROFILE
    CALL nvtxEndRange
#endif
  END SUBROUTINE end_timer_l0

#ifdef TIME_LEVEL1
!-------------------------------------------------------------------------------
!* start timing for mid-level calls
!-------------------------------------------------------------------------------
  SUBROUTINE start_timer_l1(tt,tag,ftn_name,iftn,idepth)
    IMPLICIT NONE

    !> timer instance
    CLASS(timer_type), INTENT(INOUT) :: tt
    !> tag to classify grouping for output (max 8 characters)
    CHARACTER(*), INTENT(IN) :: tag
    !> function name (max 32 characters)
    CHARACTER(*), INTENT(IN) :: ftn_name
    !> function index (must be zero on first call)
    INTEGER(i4), INTENT(INOUT) :: iftn
    !> function call depth to pass to end_timer call
    INTEGER(i4), INTENT(INOUT) :: idepth

    CALL tt%start_timer_l0(tag,ftn_name,iftn,idepth)
  END SUBROUTINE start_timer_l1

!-------------------------------------------------------------------------------
!* end timing for mid-level calls
!-------------------------------------------------------------------------------
  SUBROUTINE end_timer_l1(tt,iftn,idepth)
    IMPLICIT NONE

    !> timer instance
    CLASS(timer_type), INTENT(INOUT) :: tt
    !> function index
    INTEGER(i4), INTENT(IN) :: iftn
    !> function call depth to pass to end_timer call
    INTEGER(i4), INTENT(IN) :: idepth

    CALL tt%end_timer_l0(iftn,idepth)
  END SUBROUTINE end_timer_l1

#else /* TIME_LEVEL1 */
!-------------------------------------------------------------------------------
!* skip timing for mid-level calls, should be optimized out
!-------------------------------------------------------------------------------
  PURE SUBROUTINE start_timer_l1(tt,tag,ftn_name,iftn,idepth)
    IMPLICIT NONE

    !> timer instance
    CLASS(timer_type), INTENT(INOUT) :: tt
    !> tag to classify grouping for output (max 8 characters)
    CHARACTER(*), INTENT(IN) :: tag
    !> function name (max 32 characters)
    CHARACTER(*), INTENT(IN) :: ftn_name
    !> function index (must be zero on first call)
    INTEGER(i4), INTENT(INOUT) :: iftn
    !> function call depth to pass to end_timer call
    INTEGER(i4), INTENT(INOUT) :: idepth

  END SUBROUTINE start_timer_l1

!-------------------------------------------------------------------------------
!* skip timing for mid-level calls, should be optimized out
!-------------------------------------------------------------------------------
  PURE SUBROUTINE end_timer_l1(tt,iftn,idepth)
    IMPLICIT NONE

    !> timer instance
    CLASS(timer_type), INTENT(INOUT) :: tt
    !> function index
    INTEGER(i4), INTENT(IN) :: iftn
    !> function call depth to pass to end_timer call
    INTEGER(i4), INTENT(IN) :: idepth

  END SUBROUTINE end_timer_l1
#endif /* TIME_LEVEL1 */

#ifdef TIME_LEVEL2
!-------------------------------------------------------------------------------
!* start timing for low-level calls
!-------------------------------------------------------------------------------
  SUBROUTINE start_timer_l2(tt,tag,ftn_name,iftn,idepth)
    IMPLICIT NONE

    !> timer instance
    CLASS(timer_type), INTENT(INOUT) :: tt
    !> tag to classify grouping for output (max 8 characters)
    CHARACTER(*), INTENT(IN) :: tag
    !> function name (max 32 characters)
    CHARACTER(*), INTENT(IN) :: ftn_name
    !> function index (must be zero on first call)
    INTEGER(i4), INTENT(INOUT) :: iftn
    !> function call depth to pass to end_timer call
    INTEGER(i4), INTENT(INOUT) :: idepth

    CALL tt%start_timer_l0(tag,ftn_name,iftn,idepth)
  END SUBROUTINE start_timer_l2

!-------------------------------------------------------------------------------
!* end timing for low-level calls
!-------------------------------------------------------------------------------
  SUBROUTINE end_timer_l2(tt,iftn,idepth)
    IMPLICIT NONE

    !> timer instance
    CLASS(timer_type), INTENT(INOUT) :: tt
    !> function index
    INTEGER(i4), INTENT(IN) :: iftn
    !> function call depth to pass to end_timer call
    INTEGER(i4), INTENT(IN) :: idepth

    CALL tt%end_timer_l0(iftn,idepth)
  END SUBROUTINE end_timer_l2

#else /* TIME_LEVEL2 */
!-------------------------------------------------------------------------------
!* skip timing for low-level calls, should be optimized out
!-------------------------------------------------------------------------------
  PURE SUBROUTINE start_timer_l2(tt,tag,ftn_name,iftn,idepth)
    IMPLICIT NONE

    !> timer instance
    CLASS(timer_type), INTENT(INOUT) :: tt
    !> tag to classify grouping for output (max 8 characters)
    CHARACTER(*), INTENT(IN) :: tag
    !> function name (max 32 characters)
    CHARACTER(*), INTENT(IN) :: ftn_name
    !> function index (must be zero on first call)
    INTEGER(i4), INTENT(INOUT) :: iftn
    !> function call depth to pass to end_timer call
    INTEGER(i4), INTENT(INOUT) :: idepth

  END SUBROUTINE start_timer_l2

!-------------------------------------------------------------------------------
!* skip timing for low-level calls, should be optimized out
!-------------------------------------------------------------------------------
  PURE SUBROUTINE end_timer_l2(tt,iftn,idepth)
    IMPLICIT NONE

    !> timer instance
    CLASS(timer_type), INTENT(INOUT) :: tt
    !> function index
    INTEGER(i4), INTENT(IN) :: iftn
    !> function call depth to pass to end_timer call
    INTEGER(i4), INTENT(IN) :: idepth

  END SUBROUTINE end_timer_l2
#endif /* TIME_LEVEL2 */

END MODULE timer_mod
