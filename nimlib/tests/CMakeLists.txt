######################################################################
#
# CMakeLists.txt for nimlib/tests
#
######################################################################

# Make sure we run locally
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

set(NIMLIBTEST_SOURCES
    test_poly.f fake_function.f
)

add_library(nimlibtest ${NIMLIBTEST_SOURCES})
target_link_libraries(nimlibtest PUBLIC nimlib nimutest)
target_include_directories(nimlibtest PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/>
  $<INSTALL_INTERFACE:include/>
)

# Order tests consistent with class hierarchy

addNimUnitTest(SOURCEFILES test_gauleg.f
               TESTARGS nodes weights eval
               LINK_LIBS nimlibtest
               LABELS gauleg polynomial)

addNimUnitTest(SOURCEFILES test_lobleg.f
               TESTARGS nodes weights eval
               LINK_LIBS nimlibtest
               LABELS lobleg polynomial)

addNimUnitTest(SOURCEFILES test_pardata.f
               TESTARGS alloc_dealloc init set_decomp
               LINK_LIBS nimlib nimutest
               LABELS pardata
	       USE_MPI)

addNimUnitTest(SOURCEFILES test_timer.f
               TESTARGS timer_test
               LINK_LIBS nimlib nimutest
               LABELS timer)
