!-------------------------------------------------------------------------------
!! utilities to set up grids
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* utilities to set up grids
!-------------------------------------------------------------------------------
MODULE grid_utils_mod
  USE local
  USE timer_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: setup_bl_mesh

  CHARACTER(*), PARAMETER :: mod_name='grid_utils_mod'
CONTAINS

!-------------------------------------------------------------------------------
!> Setup a grid based on grid_input. This initializes blocks and the RZ
! coordinates of the mesh.
!
! Make sure that a call to read the input is done before calling this routine.
!-------------------------------------------------------------------------------
  SUBROUTINE setup_bl_mesh(bl,seam)
    USE h1_rect_2D_mod
    USE gblock_mod
    USE grid_input_mod
    USE rblock_mod
    USE seam_mod
    USE simple_mesh_mod
    USE simple_seam_mod
    IMPLICIT NONE

    !> block array to allocate and initialize mesh
    TYPE(block_storage), ALLOCATABLE, INTENT(INOUT) :: bl(:)
    !> associated seam data to allocate and initialize
    TYPE(seam_type), INTENT(INOUT) :: seam

    INTEGER(i4) :: ixbl,iybl,irbl,itbl,ibl,m1,m2,mt,mxbl,mybl
    INTEGER(i4) :: mx_blk(grid_in%nxbl),my_blk(grid_in%nybl)
    INTEGER(i4) :: mr_blk(grid_in%nrbl),mt_blk(grid_in%ntbl)
    REAL(r8) :: xmin_core=1,xmax_core=1,ymin_core=1,ymax_core=1
    LOGICAL :: r0blk
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l1('setup_grid','setup_grid',iftn,idepth)
!-------------------------------------------------------------------------------
!   set everything up
!-------------------------------------------------------------------------------
    m2=0
    DO ibl=1,grid_in%nxbl
      m1=m2
      m2=grid_in%mx*ibl/grid_in%nxbl
      mx_blk(ibl)=m2-m1
    ENDDO
    m2=0
    DO ibl=1,grid_in%nybl
      m1=m2
      m2=grid_in%my*ibl/grid_in%nybl
      my_blk(ibl)=m2-m1
    ENDDO
    IF (grid_in%gridshape=="rect") THEN
      CALL seam_rect_alloc(seam,grid_in%nxbl,grid_in%nybl,mx_blk,my_blk,        &
                           grid_in%periodicity,                                 &
                           grid_in%torgeom.AND..NOT.grid_in%annulus)
    ELSE ! circ
      IF (grid_in%xmin>0._r8) THEN
        CALL seam_rect_alloc(seam,grid_in%nxbl,grid_in%nybl,mx_blk,my_blk,      &
                             'y-dir',.FALSE.)
      ELSE
        m2=0
        DO ibl=1,grid_in%nrbl
          m1=m2
          m2=grid_in%mr*ibl/grid_in%nrbl
          mr_blk(ibl)=m2-m1
        ENDDO
        mt=0
        DO ibl=1,grid_in%nxbl
          mt=mt+2*mx_blk(ibl)
        ENDDO
        DO ibl=1,grid_in%nybl
          mt=mt+2*my_blk(ibl)
        ENDDO
        m2=0
        DO ibl=1,grid_in%ntbl
          m1=m2
          m2=mt*ibl/grid_in%ntbl
          mt_blk(ibl)=m2-m1
        ENDDO
        CALL seam_circ_alloc(seam,grid_in%nxbl,grid_in%nybl,grid_in%nrbl,       &
                             grid_in%ntbl,mx_blk,my_blk,mr_blk,mt_blk)
      ENDIF
      r0blk=.FALSE.
      xmax_core=0.5_r8*REAL(grid_in%mx,r8)/REAL(grid_in%mx+grid_in%mr,r8)
      xmin_core=-xmax_core
      ymax_core=0.5_r8*REAL(grid_in%my,r8)/REAL(grid_in%my+grid_in%mr,r8)
      ymin_core=-ymax_core
    ENDIF
    ALLOCATE(bl(grid_in%nbl_total))
    DO ibl=1,grid_in%nbl_total
      IF (ibl<=grid_in%nxbl*grid_in%nybl) THEN
        ixbl=(ibl-1)/grid_in%nybl+1
        iybl=MOD(ibl-1,grid_in%nybl)+1
        mxbl=mx_blk(ixbl)
        mybl=my_blk(iybl)
      ELSE
        irbl=(ibl-grid_in%nxbl*grid_in%nybl-1)/grid_in%ntbl+1
        itbl=MOD(ibl-grid_in%nxbl*grid_in%nybl-1,grid_in%ntbl)+1
        mxbl=mr_blk(irbl)
        mybl=mt_blk(itbl)
      ENDIF
      r0blk=.FALSE.
      IF (grid_in%torgeom.AND..NOT.grid_in%annulus.AND.                         &
          grid_in%gridshape=="rect".AND.ixbl==1) r0blk=.TRUE.
!-------------------------------------------------------------------------------
!     allocate blocks
!-------------------------------------------------------------------------------
      ALLOCATE(rblock::bl(ibl)%b)
      SELECT TYPE(blk => bl(ibl)%b)
      TYPE IS (rblock)
        CALL blk%alloc(ibl,ibl,mxbl,mybl,grid_in%poly_degree,r0blk)
      END SELECT
!-------------------------------------------------------------------------------
!     allocate/set mesh
!-------------------------------------------------------------------------------
      ALLOCATE(simple_rect_2D_mesh::bl(ibl)%b%mesh)
      SELECT TYPE(mesh => bl(ibl)%b%mesh)
      TYPE IS (simple_rect_2D_mesh)
        CALL mesh%alloc(mxbl,mybl,2,grid_in%poly_degree,ibl,'RZ')
        IF (grid_in%gridshape=="rect".OR.grid_in%annulus) THEN
          CALL mesh%setup_rect(grid_in%nxbl,grid_in%nybl,ixbl,iybl,             &
                               grid_in%xmin,grid_in%xmax,                       &
                               grid_in%ymin,grid_in%ymax)
          IF (grid_in%gridshape=="circ".AND.grid_in%annulus) THEN
            CALL mesh%transform_rt_rz(grid_in%xo,grid_in%yo)
          ENDIF
        ELSE ! circ
          IF (ibl<=grid_in%nxbl*grid_in%nybl) THEN
            CALL mesh%setup_rect(grid_in%nxbl,grid_in%nybl,ixbl,iybl,           &
                                 xmin_core*grid_in%xmax+grid_in%xo,             &
                                 xmax_core*grid_in%xmax+grid_in%xo,             &
                                 ymin_core*grid_in%xmax+grid_in%yo,             &
                                 ymax_core*grid_in%xmax+grid_in%yo)
          ELSE
            CALL mesh%setup_leaf(grid_in%nrbl,grid_in%ntbl,irbl,itbl,           &
                                 xmax_core,ymax_core,grid_in%mx,grid_in%my,     &
                                 grid_in%xmax,grid_in%xo,grid_in%yo)
          ENDIF
        ENDIF
      END SELECT
    ENDDO
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE setup_bl_mesh

END MODULE grid_utils_mod
