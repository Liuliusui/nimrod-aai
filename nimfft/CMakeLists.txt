######################################################################          
#                                                                               
# CMakeLists.txt for nimfft
#                                                                               
######################################################################

set(NIMFFT_SOURCES
    fft_input.f
   )

add_library(nimfft-obj OBJECT ${NIMFFT_SOURCES})
list(APPEND OBJ_TARGETS $<TARGET_OBJECTS:nimfft-obj>)
target_link_libraries(nimfft-obj PUBLIC nimfem)
target_include_directories(nimfft-obj PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/>
  $<INSTALL_INTERFACE:include/>
)
add_library(nimfft STATIC $<TARGET_OBJECTS:nimfft-obj>)
target_link_libraries(nimfft PUBLIC nimfft-obj)

# TODO
#add_subdirectory(tests) 

set(OBJ_TARGETS ${OBJ_TARGETS} PARENT_SCOPE)
