!-------------------------------------------------------------------------------
!! Conjugate gradient solver for complex multiple-mode data
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> module containing the conjugate gradient solver for complex multiple-mode
!  data
!-------------------------------------------------------------------------------
MODULE iter_cg_comp_mod
  USE local
  USE vector_mod, ONLY: cvec_storage
  USE timer_mod
  USE iterdata_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: iter_cg_comp_dir_solve

  CHARACTER(*), PARAMETER :: mod_name='iter_cg_comp'
!-------------------------------------------------------------------------------
! module-specific internal variables
!-------------------------------------------------------------------------------
  INTEGER(i4), PRIVATE :: ntotb
  TYPE(cvec_storage), DIMENSION(:), ALLOCATABLE :: res,zee,dir,adr,slnl
CONTAINS

!-------------------------------------------------------------------------------
!* use the conjugate gradient method to solve Ax=b for x
!-------------------------------------------------------------------------------
  SUBROUTINE iter_cg_comp_dir_solve(mat,precon,rhs,sln,seam,itdat)
    USE matrix_mod, ONLY: cmat_storage
    USE io, ONLY: nim_wr
    USE seam_mod
    USE linalg_utils_mod
    USE pardata_mod
    USE preconditioner_mod
    IMPLICIT NONE

    !* matrix A
    CLASS(cmat_storage), DIMENSION(:,:), INTENT(IN) :: mat
    !* preconditioner for matrix A
    CLASS(precon_comp), INTENT(INOUT) :: precon
    !* RHS b
    TYPE(cvec_storage), DIMENSION(:), INTENT(IN) :: rhs
    !* solution x
    TYPE(cvec_storage), DIMENSION(:), INTENT(INOUT) :: sln
    !* seam
    TYPE(seam_type), INTENT(INOUT) :: seam
    !* iterdata type for input and diagnostic output
    TYPE(iterdata), INTENT(INOUT) :: itdat

    INTEGER(i4) :: ibl,it,irst
    INTEGER(i4), PARAMETER :: nrst=10000
    REAL(r8) :: erl,ercheck
    COMPLEX(r8) :: alpha,beta,ztr,ztro
    REAL(r8), PARAMETER :: resid_tol=1.,small=TINY(1._r4)
    CHARACTER(512) :: msg
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l1(mod_name,'iter_cg_comp_dir_solve',iftn,idepth)
!-------------------------------------------------------------------------------
!   allocate storage; determine best guess and initial error.
!-------------------------------------------------------------------------------
    ntotb=SIZE(sln)
    CALL iter_init(mat,precon,rhs,sln,seam,itdat)
!-------------------------------------------------------------------------------
!   begin cg iterations.
!-------------------------------------------------------------------------------
    erl=10*itdat%err
    ztro=1
    beta=0
    irst=0
    itloop: DO it=1,itdat%maxit
!-------------------------------------------------------------------------------
!     check actual residual and restart condition. if both itdat%err and
!     ercheck < tol, there is no need to make corrections.
!-------------------------------------------------------------------------------
      IF (MODULO(irst,nrst)==0.OR.itdat%err<=itdat%tol) THEN
        CALL resid_norm(mat,sln,rhs,adr,ercheck,seam,'inf',itdat%er0)
      ENDIF
      IF (MODULO(irst,nrst)==0                                                  &
          .OR.(itdat%err<=itdat%tol.AND.ercheck>itdat%tol)) THEN
        IF (ercheck>erl) THEN
          DO ibl=1,ntotb
            sln(ibl)%v=slnl(ibl)%v
            CALL dir(ibl)%v%zero
            ztro=1
            beta=0
          ENDDO
          CALL resid_norm(mat,sln,rhs,res,itdat%err,seam,'inf',itdat%er0)
        ELSE
          IF (ABS(ercheck-itdat%err)/MAX(MIN(itdat%err,ercheck),small)>resid_tol&
              .OR.ercheck<itdat%err/(1+resid_tol)) THEN
            DO ibl=1,ntotb
              CALL dir(ibl)%v%zero
              res(ibl)%v=adr(ibl)%v
            ENDDO
            ztro=1
            beta=0
            itdat%err=ercheck
          ENDIF
          DO ibl=1,ntotb
            slnl(ibl)%v=sln(ibl)%v
          ENDDO
        ENDIF
        erl=ercheck
        irst=0
      ENDIF
      irst=irst+1
!-------------------------------------------------------------------------------
!     check tolerance.
!-------------------------------------------------------------------------------
      IF (itdat%report) THEN
        WRITE(msg,'(a,i6,a,es10.3)') 'it=',it,' err=',itdat%err
        CALL par%nim_write(msg)
      ENDIF
      IF (itdat%err<=itdat%tol) THEN
        itdat%its=it-1
        CALL iter_dealloc
        itdat%converged=.TRUE.
        CALL timer%end_timer_l1(iftn,idepth)
        RETURN
      ENDIF
!-------------------------------------------------------------------------------
!     find preconditioned residual.
!-------------------------------------------------------------------------------
      CALL precon%apply(mat,res,zee,adr)
!-------------------------------------------------------------------------------
!     find the new orthogonality coefficient.
!-------------------------------------------------------------------------------
      ztr=dot(zee,res,seam)
      beta=ztr/ztro
      ztro=ztr
!-------------------------------------------------------------------------------
!     find the new orthogonal direction.
!-------------------------------------------------------------------------------
      DO ibl=1,ntotb
        CALL dir(ibl)%v%add_vec(zee(ibl)%v,v1fac=beta)
      ENDDO
!-------------------------------------------------------------------------------
!     find the product of the full matrix and the new direction.
!-------------------------------------------------------------------------------
      CALL matvec(mat,dir,adr,seam)
!-------------------------------------------------------------------------------
!     find the step size for the new direction.
!-------------------------------------------------------------------------------
      alpha=dot(dir,adr,seam)
      alpha=ztr/alpha
!-------------------------------------------------------------------------------
!     update the residual, error, and solution.
!-------------------------------------------------------------------------------
      DO ibl=1,ntotb
        CALL sln(ibl)%v%add_vec(dir(ibl)%v,v2fac=alpha)
        CALL res(ibl)%v%add_vec(adr(ibl)%v,v2fac=-alpha)
      ENDDO
      itdat%err=norm(res,seam,'inf',itdat%er0)
    ENDDO itloop
    itdat%its=itdat%maxit
!-------------------------------------------------------------------------------
!   if err>tol return the residual in the solution vector for diagnosis.
!-------------------------------------------------------------------------------
    CALL resid_norm(mat,sln,rhs,adr,itdat%err,seam,'inf',itdat%er0)
    IF (itdat%err>itdat%tol) THEN
      DO ibl=1,ntotb
        sln(ibl)%v=res(ibl)%v
      ENDDO
      itdat%converged=.FALSE.
    ELSE
      itdat%converged=.TRUE.
    ENDIF
    CALL iter_dealloc
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE iter_cg_comp_dir_solve

!-------------------------------------------------------------------------------
!* allocates storage used during conjugate gradient solve.
!-------------------------------------------------------------------------------
  SUBROUTINE iter_init(mat,precon,rhs,sln,seam,itdat)
    USE matrix_mod, ONLY: cmat_storage
    USE seam_mod
    USE linalg_utils_mod
    USE pardata_mod
    USE preconditioner_mod
    IMPLICIT NONE

    !* matrix A
    CLASS(cmat_storage), DIMENSION(:,:), INTENT(IN) :: mat
    !* preconditioner for matrix A
    CLASS(precon_comp), INTENT(INOUT) :: precon
    !* RHS b
    TYPE(cvec_storage), DIMENSION(:), INTENT(IN) :: rhs
    !* solution x
    TYPE(cvec_storage), DIMENSION(:), INTENT(INOUT) :: sln
    !* seam
    TYPE(seam_type), INTENT(INOUT) :: seam
    !* iterdata type for input and diagnostic output
    TYPE(iterdata), INTENT(INOUT) :: itdat

    INTEGER(i4) :: ibl
    REAL(r8) :: er_sl,er_pc,er_0v
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'iter_init',iftn,idepth)
!-------------------------------------------------------------------------------
!   ensure that the guess has exactly the same value in different
!   images of the same node along block borders.
!-------------------------------------------------------------------------------
    DO ibl=1,ntotb
      CALL sln(ibl)%v%edge_load_arr(seam%s(ibl),do_avg=.TRUE.)
    ENDDO
    CALL seam%edge_network
    DO ibl=1,ntotb
      CALL sln(ibl)%v%edge_unload_arr(seam%s(ibl))
    ENDDO
!-------------------------------------------------------------------------------
!   allocate storage for the cg operations.
!-------------------------------------------------------------------------------
    ALLOCATE(res(ntotb),zee(ntotb),dir(ntotb),adr(ntotb),slnl(ntotb))
    DO ibl=1,ntotb
#ifdef DEBUG
      IF (rhs(ibl)%v%nqty/=sln(ibl)%v%nqty)                                     &
        CALL par%nim_stop('iter_init: inconsistent nqty')
#endif
      CALL rhs(ibl)%v%alloc_with_mold(res(ibl)%v)
      CALL rhs(ibl)%v%alloc_with_mold(zee(ibl)%v)
      CALL rhs(ibl)%v%alloc_with_mold(dir(ibl)%v)
      CALL dir(ibl)%v%zero
      CALL rhs(ibl)%v%alloc_with_mold(adr(ibl)%v)
      CALL rhs(ibl)%v%alloc_with_mold(slnl(ibl)%v)
    ENDDO
!-------------------------------------------------------------------------------
!   determine the best initial guess from
!   1) the zero vector
!-------------------------------------------------------------------------------
    er_0v=norm(rhs,seam,'inf',1._r8)
    itdat%rhs_norm=er_0v
!-------------------------------------------------------------------------------
!   2) what is supplied
!-------------------------------------------------------------------------------
    CALL resid_norm(mat,sln,rhs,res,er_sl,seam,'inf',1._r8)
!-------------------------------------------------------------------------------
!   3) applying the preconditioner to the rhs.
!   use the preconditioner to find a first guess, then find the respective
!   residual (the guess is in zee and the respective residual is in adr).
!-------------------------------------------------------------------------------
    CALL precon%apply(mat,rhs,zee,adr)
    CALL resid_norm(mat,zee,rhs,adr,er_pc,seam,'inf',1._r8)
!-------------------------------------------------------------------------------
!   select the best guess and copy respective residual.
!-------------------------------------------------------------------------------
    itdat%err=MIN(er_sl,er_pc,er_0v)
    IF (itdat%err==er_sl) THEN
      ! do nothing but keep this solution and residual.
      itdat%seed='guess'
    ELSE IF (itdat%err==er_pc) THEN
      itdat%seed='pinv.rhs'
      DO ibl=1,ntotb
        sln(ibl)%v=zee(ibl)%v
        res(ibl)%v=adr(ibl)%v
      ENDDO
    ELSE IF (itdat%err==er_0v) THEN
      itdat%seed='0 vector'
      DO ibl=1,ntotb
        CALL sln(ibl)%v%zero
        res(ibl)%v=rhs(ibl)%v
      ENDDO
    ENDIF
!-------------------------------------------------------------------------------
!   set a normalization for the error and normalize it
!-------------------------------------------------------------------------------
    itdat%er0=MAX(er_sl,er_0v,SQRT(TINY(itdat%er0)))
    itdat%err=itdat%err/itdat%er0
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE iter_init

!-------------------------------------------------------------------------------
!* deallocates storage used during conjugate gradient solve.
!-------------------------------------------------------------------------------
  SUBROUTINE iter_dealloc
    IMPLICIT NONE

    INTEGER(i4) :: ibl
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'iter_dealloc',iftn,idepth)
!-------------------------------------------------------------------------------
!   deallocate storage.
!-------------------------------------------------------------------------------
    DO ibl=1,SIZE(res)
      CALL res(ibl)%v%dealloc
      CALL zee(ibl)%v%dealloc
      CALL dir(ibl)%v%dealloc
      CALL adr(ibl)%v%dealloc
      CALL slnl(ibl)%v%dealloc
    ENDDO
    DEALLOCATE(res,zee,dir,adr,slnl)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE iter_dealloc

!-------------------------------------------------------------------------------
! close module.
!-------------------------------------------------------------------------------
END MODULE iter_cg_comp_mod
