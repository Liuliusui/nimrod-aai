######################################################################
#
# CMake to find IO dependencies
#
######################################################################

message(STATUS "")
message(STATUS "--------- Finding I/O libraries ---------")

#---------------------------------------------------------------------
#
# Find HDF5
# https://cmake.org/cmake/help/latest/module/FindHDF5.html
#
#---------------------------------------------------------------------

message(STATUS "Using h5cc in PATH to find HDF5, the package must be")
message(STATUS "built with same compiler and fortran flag enabled.")
message(STATUS "")
if (NOT ENABLE_SHARED)
  set(HDF5_USE_STATIC_LIBRARIES TRUE)
endif ()
find_package(HDF5 COMPONENTS Fortran REQUIRED)
set(HAVE_HDF5 TRUE)

get_target_property(include_dir HDF5::HDF5 INTERFACE_INCLUDE_DIRECTORIES)
if (NOT EXISTS ${include_dir}/hdf5.mod AND EXISTS ${include_dir}/static/hdf5.mod)
  message(STATUS "Adding include dir = ${include_dir}/static")
  target_include_directories(HDF5::HDF5 INTERFACE ${include_dir}/static)
endif ()

print_target_properties(HDF5::HDF5)
