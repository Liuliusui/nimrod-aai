######################################################################
#
# CMakeLists.txt for nimlinalg/tests
#
######################################################################

# Make sure we run locally
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

set(NIMLINALGTEST_SOURCES
    linalg_registry.f
)

add_library(nimlinalgtest STATIC ${NIMLINALGTEST_SOURCES})
target_link_libraries(nimlinalgtest PUBLIC nimlinalg nimutest)
target_include_directories(nimlinalgtest PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/>
  $<INSTALL_INTERFACE:include/>
)

# Order tests consistent with class hierarchy

addNimUnitTest(SOURCEFILES test_vector.f
               TESTARGS alloc_dealloc alloc_with_mold zero assign_vector mult 
	                add l2norm_dot assemble bc_regularity load_unload
               TEST_TYPE rect_2D rect_2D_acc
               LINK_LIBS nimlinalgtest
	       LABELS vector)

addNimUnitTest(SOURCEFILES test_matrix.f
               TESTARGS alloc_dealloc matvec static_condensation assemble
                        bc_regularity jacobi_infrastructure
               TEST_TYPE rect_2D rect_2D_acc
               LINK_LIBS nimlinalgtest
	       LABELS matrix)

addNimUnitTest(SOURCEFILES test_utils.f
               TESTARGS matvec resid_norm norm dot
               TEST_TYPE rect_2D rect_2D_acc
               LINK_LIBS nimlinalgtest
	       LABELS linalg_utils)
