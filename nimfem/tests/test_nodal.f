!-------------------------------------------------------------------------------
!! General interface to test nodal FEM implementations
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!* General module to test nodal FEM implementations
!-------------------------------------------------------------------------------
PROGRAM test_nodal
  USE fem_registry_mod
  USE io
  USE local
  USE nodal_mod
  USE pardata_mod
  USE test_nodal_comp_mod
  USE test_nodal_real_mod
  USE timer_mod
  IMPLICIT NONE

  CLASS(nodal_field_comp), ALLOCATABLE :: cfield
  CLASS(nodal_field_real), ALLOCATABLE :: rfield
  CLASS(nodal_field_real), ALLOCATABLE :: coord_field
  INTEGER(i4) :: nargs,itest
  CHARACTER(64) :: test_name,test_type
  INTEGER(i4) :: idepth
  INTEGER(i4), SAVE :: iftn=-1

!-------------------------------------------------------------------------------
! determine which test to run from the command line
!-------------------------------------------------------------------------------
  nargs=command_argument_count()
  IF (nargs /= 2) THEN
    CALL print_usage
    CALL par%nim_stop('Argument error')
  ENDIF
  CALL get_command_argument(1,test_type)
  CALL get_command_argument(2,test_name)
  CALL par%init
  CALL fch5_init
  CALL timer%init
  CALL timer%start_timer_l0('test','test_nodal',iftn,idepth)
!-------------------------------------------------------------------------------
! setup registry based on test type
!-------------------------------------------------------------------------------
  CALL set_field_test_type(test_type)
!-------------------------------------------------------------------------------
! loop over test parameters
!-------------------------------------------------------------------------------
  DO itest=1,ntests
    CALL setup_fem(test_type,cfield,rfield,coord_field,itest)
    IF (comp_test) THEN
      CALL run_test_comp(cfield,coord_field,test_name)
    ELSE
      CALL run_test_real(rfield,coord_field,test_name)
    ENDIF
    CALL teardown_fem(cfield,rfield,coord_field)
  ENDDO
  CALL timer%end_timer_l0(iftn,idepth)
  CALL timer%report
  CALL timer%finalize
  CALL fch5_dealloc
  CALL par%nim_stop('Normal termination.',clean_shutdown=.TRUE.)
CONTAINS

!-------------------------------------------------------------------------------
!* helper routine to print usage
!-------------------------------------------------------------------------------
  SUBROUTINE print_usage
    CALL par%nim_write('Usage: <test program> <test_type> <test name>')
    CALL par%nim_write('  where <test name> is one of')
    CALL par%nim_write('    alloc_dealloc')
    CALL par%nim_write('    alloc_with_mold_assign')
    CALL par%nim_write('    eval')
    CALL par%nim_write('    alpha_eval')
    CALL par%nim_write('    qp_update')
    CALL par%nim_write('    h5io')
    CALL print_types
    CALL par%nim_stop('check input')
  END SUBROUTINE print_usage

END PROGRAM test_nodal
