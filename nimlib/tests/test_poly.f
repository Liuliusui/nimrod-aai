!-------------------------------------------------------------------------------
!! Tests for poly_mod.
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* Tests for poly_mod.
!-------------------------------------------------------------------------------
MODULE test_poly_mod
  USE local
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: run_test_gauleg, run_test_lobleg

CONTAINS

!-------------------------------------------------------------------------------
!* Driver routine for gauleg tests.
!-------------------------------------------------------------------------------
  SUBROUTINE run_test_gauleg(basis)
    USE poly_mod
    USE pardata_mod
    IMPLICIT NONE

    TYPE(gauleg), INTENT(INOUT) :: basis

    INTEGER(i4) :: nargs
    CHARACTER(len=64) :: test_type

    nargs=command_argument_count()
    IF (nargs /= 1) THEN
      CALL print_usage
      CALL par%nim_stop('Argument error')
    ENDIF
    CALL get_command_argument(1,test_type)
!-------------------------------------------------------------------------------
!   Call different test cases.
!-------------------------------------------------------------------------------
    SELECT CASE(trim(test_type))
    CASE ("nodes")
      CALL test_ftn_gauleg_nodes(basis)
    CASE ("weights")
      CALL test_ftn_gauleg_weights(basis)
    CASE ("eval")
      CALL test_ftn_eval(basis)
    CASE DEFAULT
      CALL par%nim_write('No test name '//TRIM(test_type))
      CALL print_usage
    END SELECT
!-------------------------------------------------------------------------------
!   Deallocate basis
!-------------------------------------------------------------------------------
    CALL basis%dealloc
  CONTAINS

    SUBROUTINE print_usage
      CALL par%nim_write('Usage: <test program> <test name>')
      CALL par%nim_write('  where <test name> is one of')
      CALL par%nim_write('    nodes')
      CALL par%nim_write('    weights')
      CALL par%nim_write('    eval')
    END SUBROUTINE print_usage

  END SUBROUTINE run_test_gauleg

!-------------------------------------------------------------------------------
!* Driver routine for lobleg tests.
!-------------------------------------------------------------------------------
  SUBROUTINE run_test_lobleg(basis)
    USE pardata_mod
    USE poly_mod
    IMPLICIT NONE

    TYPE(lobleg), INTENT(INOUT) :: basis

    INTEGER(i4) :: nargs
    CHARACTER(len=64) :: test_type

    nargs=command_argument_count()
    IF (nargs /= 1) THEN
      CALL print_usage
      RETURN
    ENDIF
    CALL get_command_argument(1,test_type)
!-------------------------------------------------------------------------------
!   Call different test cases.
!-------------------------------------------------------------------------------
    SELECT CASE(trim(test_type))
    CASE ("nodes")
      CALL test_ftn_lobleg_nodes(basis)
    CASE ("weights")
      CALL test_ftn_lobleg_weights(basis)
    CASE ("eval")
      CALL test_ftn_eval(basis)
    CASE DEFAULT
      CALL par%nim_write('No test name '//trim(test_type))
      CALL print_usage
    END SELECT
!-------------------------------------------------------------------------------
!   Deallocate basis
!-------------------------------------------------------------------------------
    CALL basis%dealloc
  CONTAINS

    SUBROUTINE print_usage
      CALL par%nim_write('Usage: <test program> <test name>')
      CALL par%nim_write('  where <test name> is one of')
      CALL par%nim_write('    nodes')
      CALL par%nim_write('    weights')
      CALL par%nim_write('    eval')
    END SUBROUTINE print_usage

  END SUBROUTINE run_test_lobleg

!-------------------------------------------------------------------------------
!* Tests abscissae/nodal locations for Gauss-LegENDre quadrature.
!-------------------------------------------------------------------------------
  SUBROUTINE test_ftn_gauleg_nodes(basis)
    USE nimtest_utils
    USE pardata_mod
    USE poly_mod
    IMPLICIT NONE

    TYPE(gauleg), INTENT(INOUT) :: basis

    REAL(r8), DIMENSION(1:basis%getpd()+1) :: test_nodes
    CHARACTER(512) :: msg

    CALL basis%getnodes(test_nodes, -1._r8, 1._r8)
    SELECT CASE(basis%getpd())
    CASE (0)
      CALL wrequal(test_nodes, [0._r8], 'gauleg nodes pd = 0', EPSILON(1._r8))
    CASE (1)
      CALL wrequal(test_nodes, [-0.5773502691896257_r8, 0.5773502691896257_r8], &
                   'gauleg nodes pd = 1', EPSILON(1._r8))
    CASE (2)
      CALL wrequal(test_nodes, [-0.7745966692414834_r8, 0._r8,                  &
                   0.7745966692414834_r8], 'gauleg nodes pd = 2', EPSILON(1._r8))
    CASE (3)
      CALL wrequal(test_nodes, [-0.8611363115940526_r8, -0.3399810435848563_r8, &
                   0.3399810435848563_r8, 0.8611363115940526_r8],               &
                   'gauleg nodes pd = 3', EPSILON(1._r8))
    CASE (4)
      CALL wrequal(test_nodes, [-0.9061798459386640_r8, -0.5384693101056831_r8, &
                   0._r8, 0.5384693101056831_r8, 0.9061798459386640_r8],        &
                   'gauleg nodes pd = 4', EPSILON(1._r8))
    CASE (5)
      CALL wrequal(test_nodes, [-0.9324695142031521_r8, -0.6612093864662645_r8, &
                   -0.2386191860831969_r8, 0.2386191860831969_r8,               &
                   0.6612093864662645_r8, 0.9324695142031521_r8],               &
                   'gauleg nodes pd = 5', EPSILON(1._r8))
    CASE (6)
      CALL wrequal(test_nodes, [-0.9491079123427585_r8, -0.7415311855993945_r8, &
                   -0.4058451513773972_r8, 0._r8, 0.4058451513773972_r8,        &
                   0.7415311855993945_r8, 0.9491079123427585_r8],               &
                   'gauleg nodes pd = 6', EPSILON(1._r8))
    CASE (7)
      CALL wrequal(test_nodes, [-0.9602898564975363_r8, -0.7966664774136267_r8, &
                   -0.5255324099163290_r8, -0.1834346424956498_r8,              &
                   0.1834346424956498_r8, 0.5255324099163290_r8,                &
                   0.7966664774136267_r8, 0.9602898564975363_r8],               &
                   'gauleg nodes pd = 7', EPSILON(1._r8))
    CASE (8)
      CALL wrequal(test_nodes, [-0.9681602395076261_r8, -0.8360311073266358_r8, &
                   -0.6133714327005904_r8, -0.3242534234038089_r8, 0._r8,       &
                   0.3242534234038089_r8, 0.6133714327005904_r8,                &
                   0.8360311073266358_r8, 0.9681602395076261_r8],               &
                   'gauleg nodes pd = 8', EPSILON(1._r8))
    CASE (9)
      CALL wrequal(test_nodes, [-0.9739065285171717_r8, -0.8650633666889845_r8, &
                   -0.6794095682990244_r8, -0.4333953941292472_r8,              &
                   -0.1488743389816312_r8, 0.1488743389816312_r8,               &
                   0.4333953941292472_r8, 0.6794095682990244_r8,                &
                   0.8650633666889845_r8, 0.9739065285171717_r8],               &
                   'gauleg nodes pd = 9', EPSILON(1._r8))
    CASE DEFAULT
      WRITE(msg,'(a,i4,a)') 'not ok : gauleg pd = ',basis%getpd(),              &
                            ' not able to be checked'
      CALL par%nim_write(msg)
    END SELECT
  END SUBROUTINE test_ftn_gauleg_nodes

!-------------------------------------------------------------------------------
!* Tests weights for Gauss-LegENDre quadrature.
!-------------------------------------------------------------------------------
  SUBROUTINE test_ftn_gauleg_weights(basis)
    USE nimtest_utils
    USE pardata_mod
    USE poly_mod
    IMPLICIT NONE

    TYPE(gauleg), INTENT(INOUT) :: basis

    REAL(r8), DIMENSION(1:basis%getpd()+1) :: test_weights
    CHARACTER(512) :: msg

    CALL basis%getweights(test_weights, -1._r8, 1._r8)
    SELECT CASE(basis%getpd())
    CASE (0)
      CALL wrequal(test_weights, [2._r8], 'gauleg weights pd = 0',              &
                   2._r8*EPSILON(1._r8))
    CASE (1)
      CALL wrequal(test_weights, [1._r8, 1._r8],                                &
                   'gauleg weights pd = 1', 2._r8*EPSILON(1._r8))
    CASE (2)
      CALL wrequal(test_weights, [0.5555555555555556_r8, 0.8888888888888888_r8, &
                   0.5555555555555556_r8], 'gauleg weights pd = 2',             &
                   2._r8*EPSILON(1._r8))
    CASE (3)
      CALL wrequal(test_weights, [0.3478548451374538_r8, 0.6521451548625461_r8, &
                   0.6521451548625461_r8, 0.3478548451374538_r8],               &
                   'gauleg weights pd = 3', 2._r8*EPSILON(1._r8))
    CASE (4)
      CALL wrequal(test_weights, [0.236926885056189_r8, 0.4786286704993665_r8,  &
                   0.5688888888888889_r8, 0.4786286704993665_r8,                &
                   0.236926885056189_r8], 'gauleg weights pd = 4',              &
                   2._r8*EPSILON(1._r8))
    CASE (5)
      CALL wrequal(test_weights, [0.1713244923791704_r8, 0.3607615730481386_r8, &
                   0.4679139345726910_r8, 0.4679139345726910_r8,                &
                   0.3607615730481386_r8, 0.1713244923791704_r8],               &
                   'gauleg weights pd = 5', 2._r8*EPSILON(1._r8))
    CASE (6)
      CALL wrequal(test_weights, [0.1294849661688697_r8, 0.2797053914892766_r8, &
                   0.3818300505051189_r8, 0.4179591836734694_r8,                &
                   0.3818300505051189_r8, 0.2797053914892766_r8,                &
                   0.1294849661688697_r8], 'gauleg weights pd = 6',             &
                   2._r8*EPSILON(1._r8))
    CASE (7)
      CALL wrequal(test_weights, [0.1012285362903763_r8, 0.2223810344533745_r8, &
                   0.3137066458778873_r8, 0.3626837833783620_r8,                &
                   0.3626837833783620_r8, 0.3137066458778873_r8,                &
                   0.2223810344533745_r8, 0.1012285362903763_r8],               &
                   'gauleg weights pd = 7', 2._r8*EPSILON(1._r8))
    CASE (8)
      CALL wrequal(test_weights, [0.0812743883615744_r8, 0.1806481606948574_r8, &
                   0.2606106964029354_r8, 0.3123470770400029_r8,                &
                   0.3302393550012598_r8, 0.3123470770400029_r8,                &
                   0.2606106964029354_r8, 0.1806481606948574_r8,                &
                   0.0812743883615744_r8], 'gauleg weights pd = 8',             &
                   2._r8*EPSILON(1._r8))
    CASE (9)
      CALL wrequal(test_weights, [0.0666713443086881_r8, 0.1494513491505806_r8, &
                   0.2190863625159820_r8, 0.2692667193099963_r8,                &
                   0.2955242247147529_r8, 0.2955242247147529_r8,                &
                   0.2692667193099963_r8, 0.2190863625159820_r8,                &
                   0.1494513491505806_r8, 0.0666713443086881_r8],               &
                   'gauleg weights pd = 9', 2._r8*EPSILON(1._r8))
    CASE DEFAULT
      WRITE(msg,'(a,i4,a)') 'not ok : gauleg pd = ',basis%getpd(),              &
                            ' not able to be checked'
      CALL par%nim_write(msg)
    END SELECT
  END SUBROUTINE test_ftn_gauleg_weights

!-------------------------------------------------------------------------------
!* Tests abscissae/nodal locations for Gauss-LegENDre quadrature.
!-------------------------------------------------------------------------------
  SUBROUTINE test_ftn_lobleg_nodes(basis)
    USE nimtest_utils
    USE pardata_mod
    USE poly_mod
    IMPLICIT NONE

    TYPE(lobleg), INTENT(INOUT) :: basis

    REAL(r8), DIMENSION(1:basis%getpd()+1) :: test_nodes
    CHARACTER(512) :: msg

    CALL basis%getnodes(test_nodes, -1._r8, 1._r8)
    SELECT CASE(basis%getpd())
    CASE (1)
      CALL wrequal(test_nodes, [-1._r8, 1._r8],                                 &
                   'lobleg nodes pd = 1', EPSILON(1._r8))
    CASE (2)
      CALL wrequal(test_nodes, [-1._r8, 0._r8, 1._r8],                          &
                   'lobleg nodes pd = 2', EPSILON(1._r8))
    CASE (3)
      CALL wrequal(test_nodes, [-1._r8, -0.447213595499957939_r8,               &
                   0.447213595499957939_r8, 1._r8],                             &
                   'lobleg nodes pd = 3', EPSILON(1._r8))
    CASE (4)
      CALL wrequal(test_nodes, [-1._r8, -0.654653670707977144_r8,               &
                   0._r8, 0.654653670707977144_r8, 1._r8],                      &
                   'lobleg nodes pd = 4', EPSILON(1._r8))
    CASE (5)
      CALL wrequal(test_nodes, [-1._r8, -0.76505532392946469_r8,                &
                   -0.285231516480645096_r8, 0.285231516480645096_r8,           &
                   0.76505532392946469_r8, 1._r8],                              &
                   'lobleg nodes pd = 5', EPSILON(1._r8))
    CASE (6)
      CALL wrequal(test_nodes, [-1._r8, -0.83022389627856693_r8,                &
                   -0.468848793470714214_r8, 0._r8, 0.468848793470714214_r8,    &
                   0.83022389627856693_r8, 1._r8],                              &
                   'lobleg nodes pd = 6', EPSILON(1._r8))
    CASE (7)
      CALL wrequal(test_nodes, [-1._r8, -0.871740148509606615_r8,               &
                   -0.591700181433142302_r8, -0.209299217902478869_r8,          &
                   0.209299217902478869_r8, 0.591700181433142302_r8,            &
                   0.871740148509606615_r8, 1._r8],                             &
                   'lobleg nodes pd = 7', EPSILON(1._r8))
    CASE (8)
      CALL wrequal(test_nodes, [-1._r8, -0.899757995411460157_r8,               &
                   -0.677186279510737753_r8, -0.363117463826178159_r8, 0._r8,   &
                   0.363117463826178159_r8, 0.677186279510737753_r8,            &
                   0.899757995411460157_r8, 1._r8],                             &
                   'lobleg nodes pd = 8', EPSILON(1._r8))
    CASE (9)
      CALL wrequal(test_nodes, [-1._r8, -0.919533908166458814_r8,               &
                   -0.738773865105505075_r8, -0.477924949810444496_r8,          &
                   -0.165278957666387025_r8, 0.165278957666387025_r8,           &
                   0.477924949810444496_r8, 0.738773865105505075_r8,            &
                   0.919533908166458814_r8, 1._r8],                             &
                   'lobleg nodes pd = 9', EPSILON(1._r8))
    CASE DEFAULT
      WRITE(msg,'(a,i4,a)') 'not ok : lobleg pd = ',basis%getpd(),              &
                            ' not able to be checked'
      CALL par%nim_write(msg)
    END SELECT
  END SUBROUTINE test_ftn_lobleg_nodes

!-------------------------------------------------------------------------------
!* Tests weights for Gauss-LegENDre quadrature.
!-------------------------------------------------------------------------------
  SUBROUTINE test_ftn_lobleg_weights(basis)
    USE nimtest_utils
    USE pardata_mod
    USE poly_mod
    IMPLICIT NONE

    TYPE(lobleg), INTENT(INOUT) :: basis

    REAL(r8), DIMENSION(1:basis%getpd()+1) :: test_weights
    CHARACTER(512) :: msg

    CALL basis%getweights(test_weights, -1._r8, 1._r8)
    SELECT CASE(basis%getpd())
    CASE (1)
      CALL wrequal(test_weights, [1._r8, 1._r8],                                &
                   'lobleg weights pd = 1', 2._r8*EPSILON(1._r8))
    CASE (2)
      CALL wrequal(test_weights, [0.3333333333333333_r8, 1.3333333333333333_r8, &
                   0.3333333333333333_r8], 'lobleg weights pd = 2',             &
                   2._r8*EPSILON(1._r8))
    CASE (3)
      CALL wrequal(test_weights, [0.1666666666666667_r8, 0.8333333333333333_r8, &
                   0.8333333333333333_r8, 0.1666666666666667_r8],               &
                   'lobleg weights pd = 3', 2._r8*EPSILON(1._r8))
    CASE (4)
      CALL wrequal(test_weights, [0.1_r8, 0.5444444444444444_r8,                &
                   0.7111111111111111_r8, 0.5444444444444444_r8, 0.1_r8],       &
                   'lobleg weights pd = 4', 2._r8*EPSILON(1._r8))
    CASE (5)
      CALL wrequal(test_weights, [0.0666666666666667_r8, 0.3784749562978470_r8, &
                   0.554858377035486353_r8, 0.554858377035486353_r8,            &
                   0.3784749562978470_r8, 0.0666666666666667_r8],               &
                   'lobleg weights pd = 5', 2._r8*EPSILON(1._r8))
    CASE (6)
      CALL wrequal(test_weights, [0.0476190476190476_r8, 0.2768260473615659_r8, &
                   0.431745381209862623_r8, 0.48761904761904762_r8,             &
                   0.431745381209862623_r8, 0.2768260473615659_r8,              &
                   0.0476190476190476_r8], 'lobleg weights pd = 6',             &
                   2._r8*EPSILON(1._r8))
    CASE (7)
      CALL wrequal(test_weights, [0.0357142857142857_r8, 0.2107042271435060_r8, &
                   0.341122692483504365_r8, 0.412458794658703882_r8,            &
                   0.412458794658703882_r8, 0.341122692483504365_r8,            &
                   0.2107042271435060_r8, 0.0357142857142857_r8],               &
                   'lobleg weights pd = 7', 2._r8*EPSILON(1._r8))
    CASE (8)
      CALL wrequal(test_weights, [0.0277777777777778_r8, 0.1654953615608055_r8, &
                   0.274538712500161735_r8, 0.34642851097304635_r8,             &
                   0.371519274376417234_r8, 0.34642851097304635_r8,             &
                   0.274538712500161735_r8, 0.1654953615608055_r8,              &
                   0.0277777777777778_r8], 'lobleg weights pd = 8',             &
                   2._r8*EPSILON(1._r8))
    CASE (9)
      CALL wrequal(test_weights, [0.0222222222222222_r8, 0.1333059908510701_r8, &
                   0.22488934206312645_r8, 0.292042683679683758_r8,             &
                   0.327539761183897457_r8, 0.327539761183897457_r8,            &
                   0.292042683679683758_r8, 0.22488934206312645_r8,             &
                   0.1333059908510701_r8, 0.0222222222222222_r8],               &
                   'lobleg weights pd = 9', 2._r8*EPSILON(1._r8))
    CASE DEFAULT
      WRITE(msg,'(a,i4,a)') 'not ok : lobleg pd = ',basis%getpd(),              &
                            ' not able to be checked'
      CALL par%nim_write(msg)
    END SELECT
  END SUBROUTINE test_ftn_lobleg_weights

!-------------------------------------------------------------------------------
!> Tests evaluation of the any set of cardinal functions. This is acheived by
!  testing the fundamental property of the cardinal functions, i.e. have the
!  value 1 at the associated node and 0 for all other nodes.
!-------------------------------------------------------------------------------
  SUBROUTINE test_ftn_eval(basis)
    USE fake_function_mod
    USE nimtest_utils
    USE poly_mod
    IMPLICIT NONE

    CLASS(lagr_1d), INTENT(INOUT) :: basis

    INTEGER(i4) :: i
    REAL(r8) :: fudge
    REAL(r8), DIMENSION(0:basis%getpd()) :: x, sum_test_val, coefs
    REAL(r8), DIMENSION(0:basis%getpd(), 0:basis%getpd()) :: phi_cmp, phi_test
    REAL(r8), DIMENSION(1:9, 0:basis%getpd()) :: sum_test_many
    CHARACTER(len=64) :: message, message_d
    TYPE(fake_function_1D) :: func

    WRITE(message, '(A18, I1)') 'lagr_1d eval pd = ', basis%getpd()
    WRITE(message_d, '(A18, I1)') 'lagr_1d eval_d pd = ', basis%getpd()
    CALL func%set_pd(basis%getpd())
!-------------------------------------------------------------------------------
!   Test that evaluation at the collocation points gives delta_ij. This tests
!   single point evaluation.
!-------------------------------------------------------------------------------
    CALL basis%getnodes(x)
    IF (x(0) < EPSILON(x(0))) THEN
      IF (x(1) > 1._r8 - EPSILON(x(1))) THEN
        fudge = 2._r8
      ELSE
        fudge = 2._r8/x(1)
      ENDIF
    ELSE
      fudge = 2._r8/x(0)
    ENDIF
    coefs = func%eval(x)
    phi_cmp = 0._r8
    DO i = 0, basis%getpd()
      phi_cmp(i, i) = 1._r8
      CALL basis%eval(x(i), phi_test(i,:))
    END DO
    CALL wrequal(phi_cmp, phi_test, trim(message),                              &
                 EPSILON(1._r8)*fudge)
!-------------------------------------------------------------------------------
!   Test that evaluation at an arbitrary point sums to one and reproduces fake.
!-------------------------------------------------------------------------------
    CALL basis%eval(0.123456789_r8, sum_test_val)
    CALL wrequal(1._r8, SUM(sum_test_val), trim(message), EPSILON(1._r8)*fudge)
    CALL wrequal(func%eval(0.123456789_r8), SUM(coefs*sum_test_val),            &
                 trim(message), EPSILON(1._r8)*fudge)
    CALL basis%eval_d(0.123456789_r8, sum_test_val)
    CALL wrequal(func%eval_d(0.123456789_r8), SUM(coefs*sum_test_val),          &
                 trim(message_d), EPSILON(1._r8)*fudge**2)
!-------------------------------------------------------------------------------
!   Test that evaluation at the collocation points gives delta_ij. This tests
!   many point evaluation.
!-------------------------------------------------------------------------------
    CALL basis%eval(x, phi_test)
    CALL wrequal(phi_cmp, phi_test, trim(message), EPSILON(1._r8)*fudge)
!-------------------------------------------------------------------------------
!   Test that evaluation at a set of arbitrary points sums to one .
!-------------------------------------------------------------------------------
    CALL basis%eval([0.1_r8, 0.2_r8, 0.3_r8, 0.4_r8, 0.5_r8, 0.6_r8, 0.7_r8,    &
                     0.8_r8, 0.9_r8], sum_test_many)
    CALL wrequal([1._r8, 1._r8, 1._r8, 1._r8, 1._r8, 1._r8, 1._r8, 1._r8, 1._r8]&
                 , SUM(sum_test_many, DIM=2), trim(message),                    &
                 EPSILON(1._r8)*fudge)
    CALL wrequal(func%eval([0.1_r8, 0.2_r8, 0.3_r8, 0.4_r8, 0.5_r8, 0.6_r8,     &
                            0.7_r8, 0.8_r8, 0.9_r8]),                           &
                 MATMUL(sum_test_many,coefs), trim(message),                    &
                 EPSILON(1._r8)*fudge)
    CALL basis%eval_d([0.1_r8, 0.2_r8, 0.3_r8, 0.4_r8, 0.5_r8, 0.6_r8, 0.7_r8,  &
                       0.8_r8, 0.9_r8], sum_test_many)
    CALL wrequal(func%eval_d([0.1_r8, 0.2_r8, 0.3_r8, 0.4_r8, 0.5_r8, 0.6_r8,   &
                              0.7_r8, 0.8_r8, 0.9_r8]),                         &
                MATMUL(sum_test_many,coefs), trim(message_d),                   &
                EPSILON(1._r8)*fudge**2)
  END SUBROUTINE test_ftn_eval

END MODULE test_poly_mod
