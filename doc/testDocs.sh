#!/bin/bash
# This script runs the ford documentation and checks for warnings/errors
# It returns the number of warnings + the number of errors

# copy pages without the uploads (figures) directory
cp -r pages pages_doc
rm -rf pages_doc/uploads
# link source files to include in pages
if test ! -e pages_doc/Contributing.md; then
  ln -s ../../Contributing.md pages_doc/Contributing.md
fi
if test ! -e pages_doc/code_of_conduct.md; then
  ln -s ../../code_of_conduct.md pages_doc/code_of_conduct.md
fi
# format nimdoc.md with the README
cat nimdoc.md ../README.md > nimdoc.md.2
mv nimdoc.md.2 nimdoc.md
# preprocess vector includes
mkdir preproc
gfortran -I. -E -cpp ../nimlinalg/vec_rect_2D.f > preproc/vec_rect_2D.fpp
gfortran -I. -E -cpp ../nimlinalg/vec_rect_2D_acc.f > preproc/vec_rect_2D_acc.fpp
# run ford
ford nimdoc.md |& tee ford.out
# copy uploads (figures) directory and fix figure link html
cp -r pages/uploads ../public/page/
sed -i "s/src=\"uploads/src=\"..\/uploads/g"  ../public/page/*/*.html
# clean up
rm -rf preproc pages_doc
git restore nimdoc.md
# output warnings
NWARNS=`cat ford.out | grep 'Warning:' | wc -l`
echo "== Number of FORD Warnings: $NWARNS"
NERROR=`cat ford.out | grep 'Error:' | wc -l`
echo "== Number of FORD Errors: $NERROR"

# Fail if either warnings or errors are generated
RETVAL=`echo "$NWARNS + $NERROR" | bc`
exit $RETVAL
