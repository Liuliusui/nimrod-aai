!-------------------------------------------------------------------------------
!! Defines a 2D rectangular vector algebra implementation
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!* Defines a 2D rectangular vector algebra implementation
!-------------------------------------------------------------------------------
MODULE vec_rect_2D_mod_acc
  USE local
  USE pardata_mod
  USE timer_mod
  USE vector_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: vec_rect_2D_real_acc, vec_rect_2D_cv1m_acc, vec_rect_2D_comp_acc

  CHARACTER(*), PARAMETER :: mod_name='vec_rect_2D_acc'
!-------------------------------------------------------------------------------
!* 2D rectangular vector algebra implementation with a real data type
!-------------------------------------------------------------------------------
  TYPE, EXTENDS(rvector) :: vec_rect_2D_real_acc
    !> number of elements in the logical x direction
    INTEGER(i4) :: mx
    !> number of elements in the logical y direction
    INTEGER(i4) :: my
    !> number of dofs on each side side per element
    INTEGER(i4) :: n_side
    !> number of dofs in interior of element
    INTEGER(i4) :: n_int
    !> polynomial degree of field
    INTEGER(i4) :: pd
    !> Vertex (corner) data array
    REAL(r8), DIMENSION(:,:,:), POINTER :: arr
    !> Horizontal side data array
    REAL(r8), DIMENSION(:,:,:,:), POINTER :: arrh
    !> Vertical side data array
    REAL(r8), DIMENSION(:,:,:,:), POINTER :: arrv
    !> Interior data array
    REAL(r8), DIMENSION(:,:,:,:), POINTER :: arri
  CONTAINS

    ! Type-bound extensions
    PROCEDURE, PASS(rvec) :: alloc => alloc_real
    ! Abstract class deferred functions
    PROCEDURE, PASS(rvec) :: dealloc => dealloc_real
    PROCEDURE, PASS(rvec) :: alloc_with_mold => alloc_with_mold_real
    PROCEDURE, PASS(rvec) :: zero => zero_real
    PROCEDURE, PASS(rvec) :: assign_rvec => assign_rvec_real
    PROCEDURE, PASS(rvec) :: assignp_rvec => assignp_rvec_real
    PROCEDURE, PASS(rvec) :: assign_cv1m => assign_cv1m_real
    PROCEDURE, PASS(rvec) :: assignp_cv1m => assignp_cv1m_real
    PROCEDURE, PASS(rvec) :: assign_cvec => assign_cvec_real
    PROCEDURE, PASS(rvec) :: assignp_cvec => assignp_cvec_real
    PROCEDURE, PASS(rvec) :: add_vec => add_vec_real
    PROCEDURE, PASS(rvec) :: mult_rsc => mult_rsc_real
    PROCEDURE, PASS(rvec) :: mult_csc => mult_csc_real
    PROCEDURE, PASS(rvec) :: mult_int => mult_int_real
    PROCEDURE, PASS(rvec) :: inf_norm => inf_norm_real
    PROCEDURE, PASS(rvec) :: l2_norm2 => l2_norm2_real
    PROCEDURE, PASS(rvec) :: dot => dot_real
    PROCEDURE, PASS(rvec) :: assemble => assemble_real
    PROCEDURE, PASS(rvec) :: dirichlet_bc => dirichlet_bc_real
    PROCEDURE, PASS(rvec) :: regularity => regularity_real
    PROCEDURE, PASS(rvec) :: set_edge_vars => set_edge_vars_real
    PROCEDURE, PASS(rvec) :: edge_load_arr => edge_load_arr_real
    PROCEDURE, PASS(rvec) :: edge_unload_arr => edge_unload_arr_real
    PROCEDURE, PASS(rvec) :: edge_load_save => edge_load_save_real
    PROCEDURE, PASS(rvec) :: edge_add_save => edge_add_save_real
    PROCEDURE, PASS(rvec) :: apply_diag_matvec => apply_diag_matvec_real
    PROCEDURE, PASS(rvec) :: invert => invert_real
    PROCEDURE, PASS(rvec) :: assign_for_testing => assign_for_testing_real
    PROCEDURE, PASS(rvec) :: test_if_equal => test_if_equal_real
  END TYPE vec_rect_2D_real_acc

!-------------------------------------------------------------------------------
!> 2D rectangular vector algebra implementation with a complex data type with
!  with a single mode. This type is designed for intermediate computations.
!-------------------------------------------------------------------------------
  TYPE, EXTENDS(cvector1m) :: vec_rect_2D_cv1m_acc
    !> number of elements in the logical x direction
    INTEGER(i4) :: mx
    !> number of elements in the logical y direction
    INTEGER(i4) :: my
    !> number of dofs on each side side per element
    INTEGER(i4) :: n_side
    !> number of dofs in interior of element
    INTEGER(i4) :: n_int
    !> polynomial degree of field
    INTEGER(i4) :: pd
    !> Vertex (corner) data array
    COMPLEX(r8), DIMENSION(:,:,:), POINTER :: arr
    !> Horizontal side data array
    COMPLEX(r8), DIMENSION(:,:,:,:), POINTER :: arrh
    !> Vertical side data array
    COMPLEX(r8), DIMENSION(:,:,:,:), POINTER :: arrv
    !> Interior data array
    COMPLEX(r8), DIMENSION(:,:,:,:), POINTER :: arri
  CONTAINS

    ! Type-bound extensions
    PROCEDURE, PASS(cv1m) :: alloc => alloc_cv1m
    ! Abstract class deferred functions
    PROCEDURE, PASS(cv1m) :: dealloc => dealloc_cv1m
    PROCEDURE, PASS(cv1m) :: alloc_with_mold => alloc_with_mold_cv1m
    PROCEDURE, PASS(cv1m) :: zero => zero_cv1m
    PROCEDURE, PASS(cv1m) :: assign_rvec => assign_rvec_cv1m
    PROCEDURE, PASS(cv1m) :: assignp_rvec => assignp_rvec_cv1m
    PROCEDURE, PASS(cv1m) :: assign_cv1m => assign_cv1m_cv1m
    PROCEDURE, PASS(cv1m) :: assignp_cv1m => assignp_cv1m_cv1m
    PROCEDURE, PASS(cv1m) :: assign_cvec => assign_cvec_cv1m
    PROCEDURE, PASS(cv1m) :: assignp_cvec => assignp_cvec_cv1m
    PROCEDURE, PASS(cv1m) :: add_vec => add_vec_cv1m
    PROCEDURE, PASS(cv1m) :: mult_rsc => mult_rsc_cv1m
    PROCEDURE, PASS(cv1m) :: mult_csc => mult_csc_cv1m
    PROCEDURE, PASS(cv1m) :: mult_int => mult_int_cv1m
    PROCEDURE, PASS(cv1m) :: inf_norm => inf_norm_cv1m
    PROCEDURE, PASS(cv1m) :: l2_norm2 => l2_norm2_cv1m
    PROCEDURE, PASS(cv1m) :: dot => dot_cv1m
    PROCEDURE, PASS(cv1m) :: assemble => assemble_cv1m
    PROCEDURE, PASS(cv1m) :: dirichlet_bc => dirichlet_bc_cv1m
    PROCEDURE, PASS(cv1m) :: regularity => regularity_cv1m
    PROCEDURE, PASS(cv1m) :: set_edge_vars => set_edge_vars_cv1m
    PROCEDURE, PASS(cv1m) :: edge_load_arr => edge_load_arr_cv1m
    PROCEDURE, PASS(cv1m) :: edge_unload_arr => edge_unload_arr_cv1m
    PROCEDURE, PASS(cv1m) :: edge_load_save => edge_load_save_cv1m
    PROCEDURE, PASS(cv1m) :: edge_add_save => edge_add_save_cv1m
    PROCEDURE, PASS(cv1m) :: apply_diag_matvec => apply_diag_matvec_cv1m
    PROCEDURE, PASS(cv1m) :: invert => invert_cv1m
    PROCEDURE, PASS(cv1m) :: assign_for_testing => assign_for_testing_cv1m
    PROCEDURE, PASS(cv1m) :: test_if_equal => test_if_equal_cv1m
  END TYPE vec_rect_2D_cv1m_acc

!-------------------------------------------------------------------------------
!* 2D rectangular vector algebra implementation with a complex data type.
!-------------------------------------------------------------------------------
  TYPE, EXTENDS(cvector) :: vec_rect_2D_comp_acc
    !> number of elements in the logical x direction
    INTEGER(i4) :: mx
    !> number of elements in the logical y direction
    INTEGER(i4) :: my
    !> number of dofs on each side side per element
    INTEGER(i4) :: n_side
    !> number of dofs in interior of element
    INTEGER(i4) :: n_int
    !> polynomial degree of field
    INTEGER(i4) :: pd
    !> Vertex (corner) data array
    COMPLEX(r8), DIMENSION(:,:,:,:), POINTER :: arr
    !> Horizontal side data array
    COMPLEX(r8), DIMENSION(:,:,:,:,:), POINTER :: arrh
    !> Vertical side data array
    COMPLEX(r8), DIMENSION(:,:,:,:,:), POINTER :: arrv
    !> Interior data array
    COMPLEX(r8), DIMENSION(:,:,:,:,:), POINTER :: arri
  CONTAINS

    ! Type-bound extensions
    PROCEDURE, PASS(cvec) :: alloc => alloc_comp
    ! Abstract class deferred functions
    PROCEDURE, PASS(cvec) :: dealloc => dealloc_comp
    PROCEDURE, PASS(cvec) :: alloc_with_mold_comp => alloc_with_mold_comp
    PROCEDURE, PASS(cvec) :: alloc_with_mold_cv1m_comp =>                       &
                                                       alloc_with_mold_cv1m_comp
    PROCEDURE, PASS(cvec) :: zero => zero_comp
    PROCEDURE, PASS(cvec) :: assign_rvec => assign_rvec_comp
    PROCEDURE, PASS(cvec) :: assignp_rvec => assignp_rvec_comp
    PROCEDURE, PASS(cvec) :: assign_cv1m => assign_cv1m_comp
    PROCEDURE, PASS(cvec) :: assignp_cv1m => assignp_cv1m_comp
    PROCEDURE, PASS(cvec) :: assign_cvec => assign_cvec_comp
    PROCEDURE, PASS(cvec) :: assignp_cvec => assignp_cvec_comp
    PROCEDURE, PASS(cvec) :: add_vec => add_vec_comp
    PROCEDURE, PASS(cvec) :: mult_rsc => mult_rsc_comp
    PROCEDURE, PASS(cvec) :: mult_csc => mult_csc_comp
    PROCEDURE, PASS(cvec) :: mult_int => mult_int_comp
    PROCEDURE, PASS(cvec) :: inf_norm => inf_norm_comp
    PROCEDURE, PASS(cvec) :: l2_norm2 => l2_norm2_comp
    PROCEDURE, PASS(cvec) :: dot => dot_comp
    PROCEDURE, PASS(cvec) :: assemble => assemble_comp
    PROCEDURE, PASS(cvec) :: dirichlet_bc => dirichlet_bc_comp
    PROCEDURE, PASS(cvec) :: regularity => regularity_comp
    PROCEDURE, PASS(cvec) :: set_edge_vars => set_edge_vars_comp
    PROCEDURE, PASS(cvec) :: edge_load_arr => edge_load_arr_comp
    PROCEDURE, PASS(cvec) :: edge_unload_arr => edge_unload_arr_comp
    PROCEDURE, PASS(cvec) :: edge_load_save => edge_load_save_comp
    PROCEDURE, PASS(cvec) :: edge_add_save => edge_add_save_comp
    PROCEDURE, PASS(cvec) :: apply_diag_matvec => apply_diag_matvec_comp
    PROCEDURE, PASS(cvec) :: invert => invert_comp
    PROCEDURE, PASS(cvec) :: assign_for_testing => assign_for_testing_comp
    PROCEDURE, PASS(cvec) :: test_if_equal => test_if_equal_comp
  END TYPE vec_rect_2D_comp_acc

CONTAINS

!-------------------------------------------------------------------------------
! function definitions are broken up by file to limit file sizes and allow for
! for easy comparisons between similar functions of different types.
!-------------------------------------------------------------------------------
#include "vec_rect_2D_real_acc.f"

#include "vec_rect_2D_cv1m_acc.f"

#include "vec_rect_2D_comp_acc.f"

END MODULE vec_rect_2D_mod_acc
