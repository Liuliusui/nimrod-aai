!-------------------------------------------------------------------------------
!! module containing types for block boundary communication
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* module containing types for block boundary communication
!-------------------------------------------------------------------------------
MODULE edge_mod
  USE local
  USE timer_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: edge_type

  CHARACTER(*), PARAMETER :: mod_name='edge'
!-------------------------------------------------------------------------------
!* Type pointing to element vertices
!-------------------------------------------------------------------------------
  TYPE :: vertex_type
    !> indexing associated with vertex (used by vector routines)
    INTEGER(i4), DIMENSION(2) :: intxy
    !> number of seams that contribute to this vertex
    INTEGER(i4) :: nimage
    !> order(0:nimage) array for consistent sums across processors
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: order
    !> array that holds indexing information, size(2,nimage)
    !  ptr(1,:) = block index, ptr(2,:) = vertex index
    INTEGER(i4), DIMENSION(:,:), ALLOCATABLE :: ptr
    !> version of ptr without references to seam 0
    INTEGER(i4), DIMENSION(:,:), ALLOCATABLE :: ptr2
    !> seam input data array
    REAL(r8), DIMENSION(:), ALLOCATABLE :: seam_in
    !> seam output data array
    REAL(r8), DIMENSION(:), ALLOCATABLE :: seam_out
    !> seam save data array
    REAL(r8), DIMENSION(:), ALLOCATABLE :: seam_save
    !> seam hold data array used for ordering
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: seam_hold
    !> seam input complex data array
    COMPLEX(r8), DIMENSION(:), ALLOCATABLE :: seam_cin
    !> seam output complex data array
    COMPLEX(r8), DIMENSION(:), ALLOCATABLE :: seam_cout
    !> seam complex save data array
    COMPLEX(r8), DIMENSION(:), ALLOCATABLE :: seam_csave
    !> seam hold complex data array used for ordering
    COMPLEX(r8), DIMENSION(:,:), ALLOCATABLE :: seam_chold
    !> normal unit vector from vertex
    REAL(r8), DIMENSION(:), ALLOCATABLE :: norm
    !> tangential unit vector from vertex
    REAL(r8), DIMENSION(:), ALLOCATABLE :: tang
    !> averaging factor to apply to avoid double counting contributions
    REAL(r8) :: ave_factor
  END TYPE vertex_type

!-------------------------------------------------------------------------------
!* Type pointing to element edges/sides between element vertices
!-------------------------------------------------------------------------------
  TYPE :: segment_type
    !> indexing associated with segment (used by vector routines)
    INTEGER(i4), DIMENSION(2) :: intxys
    !> indexing associated with "next" vertex
    INTEGER(i4), DIMENSION(2) :: intxyn
    !> indexing associated with "previous" vertex
    INTEGER(i4), DIMENSION(2) :: intxyp
    !> array that holds indexing information, size(2,nimage)
    !  ptr(1,:) = block index, ptr(2,:) = segment index
    INTEGER(i4), DIMENSION(2) :: ptr
    !> direction to load segment (used by vector routines)
    INTEGER(i4) :: load_dir
    !> boolean to switch between horizontal and vertical sides
    LOGICAL :: h_side
    !> seam input data array
    REAL(r8), DIMENSION(:), ALLOCATABLE :: seam_in
    !> seam output data array
    REAL(r8), DIMENSION(:), ALLOCATABLE :: seam_out
    !> seam save data array
    REAL(r8), DIMENSION(:), ALLOCATABLE :: seam_save
    !> seam input complex data array
    COMPLEX(r8), DIMENSION(:), ALLOCATABLE :: seam_cin
    !> seam output complex data array
    COMPLEX(r8), DIMENSION(:), ALLOCATABLE :: seam_cout
    !> seam complex save data array
    COMPLEX(r8), DIMENSION(:), ALLOCATABLE :: seam_csave
    !> normal unit vector from segment
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: norm
    !> tangential unit vector from segment
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: tang
    !> averaging factor to apply to avoid double counting contributions
    REAL(r8) :: ave_factor
  END TYPE segment_type

!-------------------------------------------------------------------------------
! Basic edge type associated with each block
!-------------------------------------------------------------------------------
  TYPE :: edge_type
    !> block index associated with edge_type
    INTEGER(i4) :: id
    !> true if data on GPU
    LOGICAL :: on_gpu=.FALSE.
    !> number of vertices
    INTEGER(i4) :: nvert
    !> vertex(1:nvert) stores vertex edge information
    TYPE(vertex_type), DIMENSION(:), ALLOCATABLE :: vertex
    !> segment(1:nvert) stores segment edge information
    TYPE(segment_type), DIMENSION(:), ALLOCATABLE :: segment
    !> nqty loaded set by vector%edge_load_arr
    INTEGER(i4) :: nqty_loaded
    !> nside loaded set by vector%edge_load_arr
    INTEGER(i4) :: nside_loaded
    !> nmodes loaded set by vector%edge_load_arr
    INTEGER(i4) :: nmodes_loaded
    !> imode starting index set by vector%edge_load_arr
    INTEGER(i4) :: imode_start
    !> expoint(1:nvert), true if exterior point
    LOGICAL, DIMENSION(:), ALLOCATABLE :: expoint
    !> excorner(1:nvert), true if exterior corner
    LOGICAL, DIMENSION(:), ALLOCATABLE :: excorner
    !> r0point(1:nvert), true if point is at R=0
    LOGICAL, DIMENSION(:), ALLOCATABLE :: r0point
    !> exsegment(1:nvert), true if exterior segment
    LOGICAL, DIMENSION(:), ALLOCATABLE :: exsegment
    !> r0segment(1:nvert), true segment is at R=0
    LOGICAL, DIMENSION(:), ALLOCATABLE :: r0segment
    !> seam vertex input data array
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: vert_in
    !> seam vertex output data array
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: vert_out
    !> seam vertex save data array
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: vert_save
    !> seam vertex input complex data array
    COMPLEX(r8), DIMENSION(:,:), ALLOCATABLE :: vert_cin
    !> seam vertex output complex data array
    COMPLEX(r8), DIMENSION(:,:), ALLOCATABLE :: vert_cout
    !> seam vertex complex save data array
    COMPLEX(r8), DIMENSION(:,:,:), ALLOCATABLE :: vert_csave
    !> seam segment input data array
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: seg_in
    !> seam segment output data array
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: seg_out
    !> seam segment save data array
    REAL(r8), DIMENSION(:,:,:), ALLOCATABLE :: seg_save
    !> seam segment input complex data array
    COMPLEX(r8), DIMENSION(:,:), ALLOCATABLE :: seg_cin
    !> seam segment output complex data array
    COMPLEX(r8), DIMENSION(:,:), ALLOCATABLE :: seg_cout
    !> seam segment complex save data array
    COMPLEX(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: seg_csave
  CONTAINS

    PROCEDURE, PASS(edge) :: init
    PROCEDURE, PASS(edge) :: alloc_gpu_norm_tang
    PROCEDURE, PASS(edge) :: dealloc
    PROCEDURE, PASS(edge) :: copy
    PROCEDURE, PASS(edge) :: h5_read
    PROCEDURE, PASS(edge) :: h5_dump
    PROCEDURE, PASS(edge) :: distribute
    PROCEDURE, PASS(edge) :: zero_save
    PROCEDURE, PASS(edge) :: zero_csave
    PROCEDURE, NOPASS :: load_limits
  END TYPE edge_type

CONTAINS

!-------------------------------------------------------------------------------
!> initializes arrays used for border communication. note that this routine
!  requires having nvert, ptr, expoint and r0point defined within the seam
!  structure.
!-------------------------------------------------------------------------------
  SUBROUTINE init(edge,max_nqty,max_nqty_save,nside,nmodes,glob_nvert)
    USE pardata_mod
    IMPLICIT NONE

    !> seam to initialize
    CLASS(edge_type), INTENT(INOUT) :: edge
    !> max quantity size (for allocation)
    INTEGER(i4), INTENT(IN) :: max_nqty
    !> max save size (for allocation)
    INTEGER(i4), INTENT(IN) :: max_nqty_save
    !> number of dof on each segment (for allocation)
    INTEGER(i4), INTENT(IN) :: nside
    !> number of modes (for allocation)
    INTEGER(i4), INTENT(IN) :: nmodes
    !> nvert for all other seams with global block indexing
    INTEGER(i4), INTENT(IN) :: glob_nvert(:)

    INTEGER(i4) :: iv,nv,np,npb,ipb,ip,n0,in,jbv,jvv,max_imags
    INTEGER(i4) :: ivp,jbpr,jvpr,ippr
    INTEGER(i4), DIMENSION(1) :: loc
    LOGICAL :: match
    LOGICAL, DIMENSION(:), ALLOCATABLE :: ord_mask
    REAL(r8), DIMENSION(:), ALLOCATABLE :: ord_arr
    CHARACTER(64) :: msg
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'init',iftn,idepth)
!-------------------------------------------------------------------------------
!   create a second ptr array without references to seam0 and with
!   interior vertex label cross references for parallel coding.
!-------------------------------------------------------------------------------
    max_imags=0
    DO iv=1,edge%nvert
      np=SIZE(edge%vertex(iv)%ptr,2)
      n0=0
      DO ip=1,np
        IF (edge%vertex(iv)%ptr(1,ip)==0) n0=n0+1
      ENDDO
      npb=np-n0
      edge%vertex(iv)%nimage = npb
      max_imags=MAX(npb,max_imags)
      ALLOCATE(edge%vertex(iv)%ptr2(2,npb))
      ipb=0
      DO ip=1,np
        IF (edge%vertex(iv)%ptr(1,ip)==0) CYCLE
        ipb=ipb+1
        edge%vertex(iv)%ptr2(1:2,ipb)=edge%vertex(iv)%ptr(1:2,ip)
      ENDDO
      IF (ipb/=npb) CALL par%nim_stop('Error in edge_init.')
      np=edge%vertex(iv)%nimage+1
      edge%vertex(iv)%ave_factor=1._r8/REAL(np,r8)
      ALLOCATE(edge%vertex(iv)%seam_in(max_nqty))
      ALLOCATE(edge%vertex(iv)%seam_cin(max_nqty*nmodes))
      ALLOCATE(edge%vertex(iv)%seam_out(max_nqty))
      ALLOCATE(edge%vertex(iv)%seam_cout(max_nqty*nmodes))
      ALLOCATE(edge%vertex(iv)%seam_hold(np,max_nqty))
      ALLOCATE(edge%vertex(iv)%seam_chold(np,max_nqty*nmodes))
      ALLOCATE(edge%vertex(iv)%seam_save(max_nqty_save))
      ALLOCATE(edge%vertex(iv)%seam_csave(max_nqty_save*nmodes))
    ENDDO
!-------------------------------------------------------------------------------
!   determine a unique order for summing seam vertices with more than
!   two images to prevent the occurrence of different round-off errors
!   on different blocks, which leads to an instability.
!-------------------------------------------------------------------------------
    ALLOCATE(ord_mask(max_imags+1),ord_arr(max_imags+1))
    nv=edge%nvert
    DO iv=1,nv
      np=edge%vertex(iv)%nimage
      ALLOCATE(edge%vertex(iv)%order(0:np))
      ord_arr(1)=edge%id+1.e-9_r8*REAL(iv,r8)
      DO ip=1,np
        ord_arr(ip+1)=edge%vertex(iv)%ptr2(1,ip)                                &
               +1.e-9_r8*REAL(edge%vertex(iv)%ptr2(2,ip),r8)
      ENDDO
      ord_mask(1:np+1)=.true.
      DO ip=1,np+1
        loc=MINLOC(ord_arr(1:np+1),ord_mask(1:np+1))
        in=loc(1)
        edge%vertex(iv)%order(in-1)=ip
        ord_mask(in)=.false.
      ENDDO
      ord_mask(1:np+1)=.true.
      DO ip=0,np
        ord_mask(edge%vertex(iv)%order(ip))=.false.
      ENDDO
      DO ip=1,np+1
        IF (ord_mask(ip))                                                       &
          CALL par%nim_stop('Edge_type::init: vertex order not unique.')
      ENDDO
    ENDDO
    DEALLOCATE(ord_mask,ord_arr)
!-------------------------------------------------------------------------------
!   create internal references and communication pointers for
!   edge-segment centered quantities.  the latter uses the fact that
!   seams running around two adjacent blocks always advance in
!   opposite directions.
!-------------------------------------------------------------------------------
    ALLOCATE(edge%segment(nv))
    ALLOCATE(edge%exsegment(nv))
    edge%exsegment=.FALSE.
    ALLOCATE(edge%r0segment(nv))
    edge%r0segment=.FALSE.
    DO iv=1,nv
      ivp=iv-1
      IF (ivp==0) ivp=nv
      edge%segment(iv)%intxyn=edge%vertex(iv)%intxy
      edge%segment(iv)%intxyp=edge%vertex(ivp)%intxy
      edge%segment(iv)%ptr=0
      IF (edge%expoint(iv).AND.edge%expoint(ivp)) edge%exsegment(iv)=.TRUE.
      IF (edge%r0point(iv).AND.edge%r0point(ivp)) edge%r0segment(iv)=.TRUE.
      IF ((edge%expoint(iv).OR.edge%r0point(iv)).AND.                           &
          (edge%expoint(ivp).OR.edge%r0point(ivp))) THEN
        edge%segment(iv)%ave_factor=1._r8
        CYCLE
      ELSE
        edge%segment(iv)%ave_factor=0.5_r8
      ENDIF
      match=.false.
      ptr_loop: DO ip=1,SIZE(edge%vertex(iv)%ptr2,2)
        jbv=edge%vertex(iv)%ptr2(1,ip)
        jvv=edge%vertex(iv)%ptr2(2,ip)
        IF (jvv==glob_nvert(jbv)) jvv=0
        DO ippr=1,SIZE(edge%vertex(ivp)%ptr2,2)
          jbpr=edge%vertex(ivp)%ptr2(1,ippr)
          jvpr=edge%vertex(ivp)%ptr2(2,ippr)
          IF (jbpr==jbv.AND.jvv==jvpr-1) THEN
            match=.true.
            edge%segment(iv)%ptr(1)=jbv
            edge%segment(iv)%ptr(2)=jvpr
            EXIT ptr_loop
          ENDIF
        ENDDO
      ENDDO ptr_loop
      IF (.NOT.match) THEN
        WRITE(msg,'(a,i3,a,i3)')                                                &
          'No edge match in edge_type::init, id=', edge%id,' iv=',iv
        CALL par%nim_stop(msg)
      ENDIF
    ENDDO
!-------------------------------------------------------------------------------
!   allocate segment arrays
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      ALLOCATE(edge%segment(iv)%seam_in(max_nqty*nside))
      ALLOCATE(edge%segment(iv)%seam_cin(max_nqty*nside*nmodes))
      ALLOCATE(edge%segment(iv)%seam_out(max_nqty*nside))
      ALLOCATE(edge%segment(iv)%seam_cout(max_nqty*nside*nmodes))
      ALLOCATE(edge%segment(iv)%seam_save(max_nqty_save*nside))
      ALLOCATE(edge%segment(iv)%seam_csave(max_nqty_save*nside*nmodes))
    ENDDO
!-------------------------------------------------------------------------------
!   allocate fused versions of in/out/save arrays for GPU kernels
!-------------------------------------------------------------------------------
    ALLOCATE(edge%vert_in(max_nqty,nv))
    ALLOCATE(edge%vert_cin(max_nqty*nmodes,nv))
    ALLOCATE(edge%vert_out(max_nqty,nv))
    ALLOCATE(edge%vert_cout(max_nqty*nmodes,nv))
    ALLOCATE(edge%vert_save(max_nqty_save,nv))
    ALLOCATE(edge%vert_csave(max_nqty_save,nmodes,nv))
    ALLOCATE(edge%seg_in(max_nqty*nside,nv))
    ALLOCATE(edge%seg_cin(max_nqty*nside*nmodes,nv))
    ALLOCATE(edge%seg_out(max_nqty*nside,nv))
    ALLOCATE(edge%seg_cout(max_nqty*nside*nmodes,nv))
    ALLOCATE(edge%seg_save(max_nqty_save,nside,nv))
    ALLOCATE(edge%seg_csave(max_nqty_save,nside,nmodes,nv))
!-------------------------------------------------------------------------------
!   copy/create seam to/on gpu, norm and tang are copied later
!-------------------------------------------------------------------------------
    !$acc enter data copyin(edge) async(edge%id) if(edge%on_gpu)
    !$acc enter data copyin(edge%vertex,edge%segment)                           &
    !$acc copyin(edge%expoint,edge%excorner,edge%r0point)                       &
    !$acc copyin(edge%exsegment,edge%r0segment)                                 &
    !$acc copyin(edge%vert_in,edge%vert_cin,edge%vert_out,edge%vert_cout)       &
    !$acc copyin(edge%vert_save,edge%vert_csave)                                &
    !$acc copyin(edge%seg_in,edge%seg_cin,edge%seg_out,edge%seg_cout)           &
    !$acc copyin(edge%seg_save,edge%seg_csave)                                  &
    !$acc async(edge%id) if(edge%on_gpu)
    DO iv=1,edge%nvert
      ASSOCIATE(vertex=>edge%vertex(iv),segment=>edge%segment(iv))
        !$acc enter data create(vertex%seam_in,vertex%seam_cin)                 &
        !$acc create(vertex%seam_out,vertex%seam_cout)                          &
        !$acc create(vertex%seam_hold,vertex%seam_chold)                        &
        !$acc create(vertex%seam_save,vertex%seam_csave)                        &
        !$acc create(segment%seam_in,segment%seam_cin)                          &
        !$acc create(segment%seam_out,segment%seam_cout)                        &
        !$acc create(segment%seam_save,segment%seam_csave)                      &
        !$acc async(edge%id) if(edge%on_gpu)
      END ASSOCIATE
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE init

!-------------------------------------------------------------------------------
!>  allocate and transfer norm/tang to the device
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_gpu_norm_tang(edge)
    IMPLICIT NONE

    !> seam to transfer norm/tang in
    CLASS(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_gpu_norm_tang',iftn,idepth)
    DO iv=1,edge%nvert
      ASSOCIATE(vertex=>edge%vertex(iv),segment=>edge%segment(iv))
        IF (ALLOCATED(vertex%tang)) THEN
          !$acc enter data copyin(vertex%tang) async(edge%id) if(edge%on_gpu)
          !$acc enter data copyin(vertex%norm) async(edge%id) if(edge%on_gpu)
        ENDIF
        IF (ALLOCATED(segment%tang)) THEN
          !$acc enter data copyin(segment%tang) async(edge%id) if(edge%on_gpu)
          !$acc enter data copyin(segment%norm) async(edge%id) if(edge%on_gpu)
        ENDIF
      END ASSOCIATE
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_gpu_norm_tang

!-------------------------------------------------------------------------------
!>  deallocate edge_type
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc(edge)
    IMPLICIT NONE

    !> seam to dealloc
    CLASS(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: iv
    LOGICAL :: do_dealloc
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc',iftn,idepth)
!-------------------------------------------------------------------------------
!   seam zero is never on gpu
!-------------------------------------------------------------------------------
    do_dealloc=.FALSE.
    IF (edge%id/=0.AND.edge%on_gpu) do_dealloc=.TRUE.
!-------------------------------------------------------------------------------
!   deallocate edge_type
!-------------------------------------------------------------------------------
    IF (ALLOCATED(edge%vertex)) THEN
      DO iv=1,edge%nvert
        ASSOCIATE(vertex=>edge%vertex(iv))
          IF (ALLOCATED(vertex%order))                                          &
            DEALLOCATE(vertex%order)
          IF (ALLOCATED(vertex%ptr))                                            &
            DEALLOCATE(vertex%ptr)
          IF (ALLOCATED(vertex%ptr2))                                           &
            DEALLOCATE(vertex%ptr2)
          IF (ALLOCATED(vertex%seam_in)) THEN
            !$acc exit data delete(vertex%seam_in) finalize async(edge%id)      &
            !$acc if(do_dealloc)
            DEALLOCATE(vertex%seam_in)
          ENDIF
          IF (ALLOCATED(vertex%seam_cin)) THEN
            !$acc exit data delete(vertex%seam_cin) finalize async(edge%id)     &
            !$acc if(do_dealloc)
            DEALLOCATE(vertex%seam_cin)
          ENDIF
          IF (ALLOCATED(vertex%seam_out)) THEN
            !$acc exit data delete(vertex%seam_out) finalize async(edge%id)     &
            !$acc if(do_dealloc)
            DEALLOCATE(vertex%seam_out)
          ENDIF
          IF (ALLOCATED(vertex%seam_cout)) THEN
            !$acc exit data delete(vertex%seam_cout) finalize async(edge%id)    &
            !$acc if(do_dealloc)
            DEALLOCATE(vertex%seam_cout)
          ENDIF
          IF (ALLOCATED(vertex%seam_hold)) THEN
            !$acc exit data delete(vertex%seam_hold) finalize async(edge%id)    &
            !$acc if(do_dealloc)
            DEALLOCATE(vertex%seam_hold)
          ENDIF
          IF (ALLOCATED(vertex%seam_chold)) THEN
            !$acc exit data delete(vertex%seam_chold) finalize async(edge%id)   &
            !$acc if(do_dealloc)
            DEALLOCATE(vertex%seam_chold)
          ENDIF
          IF (ALLOCATED(vertex%seam_save)) THEN
            !$acc exit data delete(vertex%seam_save) finalize async(edge%id)    &
            !$acc if(do_dealloc)
            DEALLOCATE(vertex%seam_save)
          ENDIF
          IF (ALLOCATED(vertex%seam_csave)) THEN
            !$acc exit data delete(vertex%seam_csave) finalize async(edge%id)   &
            !$acc if(do_dealloc)
            DEALLOCATE(vertex%seam_csave)
          ENDIF
          IF (ALLOCATED(vertex%norm)) THEN
            !$acc exit data delete(vertex%norm) finalize async(edge%id)         &
            !$acc if(do_dealloc)
            DEALLOCATE(vertex%norm)
          ENDIF
          IF (ALLOCATED(vertex%tang)) THEN
            !$acc exit data delete(vertex%tang) finalize async(edge%id)         &
            !$acc if(do_dealloc)
            DEALLOCATE(vertex%tang)
          ENDIF
        END ASSOCIATE
      ENDDO
    ENDIF
    IF (ALLOCATED(edge%segment)) THEN
      DO iv=1,edge%nvert
        ASSOCIATE(segment=>edge%segment(iv))
          IF (ALLOCATED(segment%seam_in)) THEN
            !$acc exit data delete(segment%seam_in) finalize async(edge%id)     &
            !$acc if(do_dealloc)
            DEALLOCATE(segment%seam_in)
          ENDIF
          IF (ALLOCATED(segment%seam_cin)) THEN
            !$acc exit data delete(segment%seam_cin) finalize async(edge%id)    &
            !$acc if(do_dealloc)
            DEALLOCATE(segment%seam_cin)
          ENDIF
          IF (ALLOCATED(segment%seam_out)) THEN
            !$acc exit data delete(segment%seam_out) finalize async(edge%id)    &
            !$acc if(do_dealloc)
            DEALLOCATE(segment%seam_out)
          ENDIF
          IF (ALLOCATED(segment%seam_cout)) THEN
            !$acc exit data delete(segment%seam_cout) finalize async(edge%id)   &
            !$acc if(do_dealloc)
            DEALLOCATE(segment%seam_cout)
          ENDIF
          IF (ALLOCATED(segment%seam_save)) THEN
            !$acc exit data delete(segment%seam_save) finalize async(edge%id)   &
            !$acc if(do_dealloc)
            DEALLOCATE(segment%seam_save)
          ENDIF
          IF (ALLOCATED(segment%seam_csave)) THEN
            !$acc exit data delete(segment%seam_csave) finalize async(edge%id)  &
            !$acc if(do_dealloc)
            DEALLOCATE(segment%seam_csave)
          ENDIF
          IF (ALLOCATED(segment%norm)) THEN
            !$acc exit data delete(segment%norm) finalize async(edge%id)        &
            !$acc if(do_dealloc)
            DEALLOCATE(segment%norm)
          ENDIF
          IF (ALLOCATED(segment%tang)) THEN
            !$acc exit data delete(segment%tang) finalize async(edge%id)        &
            !$acc if(do_dealloc)
            DEALLOCATE(segment%tang)
          ENDIF
        END ASSOCIATE
      ENDDO
      IF (ALLOCATED(edge%vert_in)) THEN
        !$acc exit data delete(edge%vert_in) finalize async(edge%id)            &
        !$acc if(do_dealloc)
        DEALLOCATE(edge%vert_in)
      ENDIF
      IF (ALLOCATED(edge%vert_cin)) THEN
        !$acc exit data delete(edge%vert_cin) finalize async(edge%id)           &
        !$acc if(do_dealloc)
        DEALLOCATE(edge%vert_cin)
      ENDIF
      IF (ALLOCATED(edge%vert_out)) THEN
        !$acc exit data delete(edge%vert_out) finalize async(edge%id)           &
        !$acc if(do_dealloc)
        DEALLOCATE(edge%vert_out)
      ENDIF
      IF (ALLOCATED(edge%vert_cout)) THEN
        !$acc exit data delete(edge%vert_cout) finalize async(edge%id)          &
        !$acc if(do_dealloc)
        DEALLOCATE(edge%vert_cout)
      ENDIF
      IF (ALLOCATED(edge%vert_save)) THEN
        !$acc exit data delete(edge%vert_save) finalize async(edge%id)          &
        !$acc if(do_dealloc)
        DEALLOCATE(edge%vert_save)
      ENDIF
      IF (ALLOCATED(edge%vert_csave)) THEN
        !$acc exit data delete(edge%vert_csave) finalize async(edge%id)         &
        !$acc if(do_dealloc)
        DEALLOCATE(edge%vert_csave)
      ENDIF
      IF (ALLOCATED(edge%seg_in)) THEN
        !$acc exit data delete(edge%seg_in) finalize async(edge%id)             &
        !$acc if(do_dealloc)
        DEALLOCATE(edge%seg_in)
      ENDIF
      IF (ALLOCATED(edge%seg_cin)) THEN
        !$acc exit data delete(edge%seg_cin) finalize async(edge%id)            &
        !$acc if(do_dealloc)
        DEALLOCATE(edge%seg_cin)
      ENDIF
      IF (ALLOCATED(edge%seg_out)) THEN
        !$acc exit data delete(edge%seg_out) finalize async(edge%id)            &
        !$acc if(do_dealloc)
        DEALLOCATE(edge%seg_out)
      ENDIF
      IF (ALLOCATED(edge%seg_cout)) THEN
        !$acc exit data delete(edge%seg_cout) finalize async(edge%id)           &
        !$acc if(do_dealloc)
        DEALLOCATE(edge%seg_cout)
      ENDIF
      IF (ALLOCATED(edge%seg_save)) THEN
        !$acc exit data delete(edge%seg_save) finalize async(edge%id)           &
        !$acc if(do_dealloc)
        DEALLOCATE(edge%seg_save)
      ENDIF
      IF (ALLOCATED(edge%seg_csave)) THEN
        !$acc exit data delete(edge%seg_csave) finalize async(edge%id)          &
        !$acc if(do_dealloc)
        DEALLOCATE(edge%seg_csave)
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!>  deallocate edge_type gpu data before cpu
!-------------------------------------------------------------------------------
    !$acc exit data delete(edge%vertex,edge%segment)                            &
    !$acc delete(edge%expoint,edge%excorner,edge%r0point)                       &
    !$acc delete(edge%exsegment,edge%r0segment)                                 &
    !$acc finalize async(edge%id) if(do_dealloc)
    !$acc exit data delete(edge) finalize async(edge%id) if(do_dealloc)
!-------------------------------------------------------------------------------
!   final deallocations
!-------------------------------------------------------------------------------
    IF (ALLOCATED(edge%vertex)) DEALLOCATE(edge%vertex)
    IF (ALLOCATED(edge%segment)) DEALLOCATE(edge%segment)
    IF (ALLOCATED(edge%expoint)) DEALLOCATE(edge%expoint)
    IF (ALLOCATED(edge%excorner)) DEALLOCATE(edge%excorner)
    IF (ALLOCATED(edge%r0point)) DEALLOCATE(edge%r0point)
    IF (ALLOCATED(edge%exsegment)) DEALLOCATE(edge%exsegment)
    IF (ALLOCATED(edge%r0segment)) DEALLOCATE(edge%r0segment)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc

!-------------------------------------------------------------------------------
!>  copy edge_type, init must be called after the copy
!-------------------------------------------------------------------------------
  SUBROUTINE copy(edge,edge_in)
    IMPLICIT NONE

    !> seam to copy too
    CLASS(edge_type), INTENT(INOUT) :: edge
    !> seam to copy from
    CLASS(edge_type), INTENT(IN) :: edge_in

    INTEGER(i4) :: iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'copy',iftn,idepth)
    edge%id=edge_in%id
    edge%nvert=edge_in%nvert
    ALLOCATE(edge%vertex(edge%nvert))
    DO iv=1,edge%nvert
      ALLOCATE(edge%vertex(iv)%ptr(2,SIZE(edge_in%vertex(iv)%ptr,2)))
      edge%vertex(iv)%ptr=edge_in%vertex(iv)%ptr
      IF (edge%id>0) edge%vertex(iv)%intxy=edge_in%vertex(iv)%intxy
    ENDDO
    IF (edge%id==0.AND.edge%nvert>0) THEN
      ALLOCATE(edge%excorner(edge%nvert))
      ALLOCATE(edge%r0point(edge%nvert))
      edge%excorner=edge_in%excorner
      edge%r0point=edge_in%r0point
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE copy

!-------------------------------------------------------------------------------
!> read seam data from dump file
!-------------------------------------------------------------------------------
  SUBROUTINE h5_read(edge,id,gid)
    USE io
    IMPLICIT NONE

    !> seam to write
    CLASS(edge_type), INTENT(INOUT) :: edge
    !> seam global index number
    INTEGER(i4), INTENT(IN) :: id
    !> h5 group to read seam from
    INTEGER(HID_T), INTENT(IN) :: gid

    INTEGER(i4) :: iv,np,ipt,npt
    INTEGER(i4), ALLOCATABLE :: exc(:),r0pt(:),bptr(:,:),bxy(:,:)
    CHARACTER(64) :: seam_name
    INTEGER(HID_T) :: sid
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'h5_read',iftn,idepth)
!-------------------------------------------------------------------------------
!   open seam group.
!-------------------------------------------------------------------------------
    WRITE(seam_name,fmt='(i4.4)') id
    CALL open_group(gid,TRIM(seam_name),sid,h5err)
!-------------------------------------------------------------------------------
!   read block descriptors.
!-------------------------------------------------------------------------------
    CALL read_attribute(sid,"id",edge%id,h5in,h5err)
    CALL read_attribute(sid,"nvert",edge%nvert,h5in,h5err)
!-------------------------------------------------------------------------------
!   read seam data.
!-------------------------------------------------------------------------------
    ALLOCATE(exc(edge%nvert),edge%vertex(edge%nvert))
    CALL read_h5(sid,"np",exc,h5in,h5err)
    npt=SUM(exc)
    ALLOCATE(bptr(2,npt))
    CALL read_h5(sid,"vertex",bptr,h5in,h5err)
    IF (edge%id>0) THEN
      ALLOCATE(bxy(2,edge%nvert))
      CALL read_h5(sid,"intxy",bxy,h5in,h5err)
    ENDIF
    ipt=1_i4
    DO iv=1,edge%nvert
      np=exc(iv)
      ALLOCATE(edge%vertex(iv)%ptr(2,np))
      edge%vertex(iv)%ptr=bptr(:,ipt:ipt+np-1)
      ipt=ipt+np
      IF (edge%id>0) edge%vertex(iv)%intxy=bxy(:,iv)
    ENDDO
!-------------------------------------------------------------------------------
!   read seam0 additional data
!-------------------------------------------------------------------------------
    IF (edge%id==0.AND.edge%nvert>0) THEN
      ALLOCATE(edge%excorner(edge%nvert))
      ALLOCATE(edge%r0point(edge%nvert))
      ALLOCATE(r0pt(edge%nvert))
      CALL read_h5(sid,"excorner",exc,h5in,h5err)
      CALL read_h5(sid,"r0point",r0pt,h5in,h5err)
      DO iv=1,edge%nvert
        edge%excorner(iv)=(exc(iv)>0)
        edge%r0point(iv)=(r0pt(iv)>0)
      ENDDO
      DEALLOCATE(r0pt)
    ENDIF
    DEALLOCATE(exc)
!-------------------------------------------------------------------------------
!   close group
!-------------------------------------------------------------------------------
    CALL close_group(TRIM(seam_name),sid,h5err)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE h5_read

!-------------------------------------------------------------------------------
!> write seam data to dump file
!-------------------------------------------------------------------------------
  SUBROUTINE h5_dump(edge,gid)
    USE io
    IMPLICIT NONE

    !> seam to write
    CLASS(edge_type), INTENT(IN) :: edge
    !> h5 group to write seam to
    INTEGER(HID_T), INTENT(IN) :: gid

    INTEGER(i4) :: iv,np,npt,ipt
    INTEGER(i4), ALLOCATABLE :: exc(:),r0pt(:),bptr(:,:),bxy(:,:)
    CHARACTER(64) :: seam_name
    INTEGER(HID_T) :: sid
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'h5_dump',iftn,idepth)
!-------------------------------------------------------------------------------
!   setup seam group.
!-------------------------------------------------------------------------------
    WRITE(seam_name,fmt='(i4.4)') edge%id
    CALL make_group(gid,TRIM(seam_name),sid,h5in,h5err)
!-------------------------------------------------------------------------------
!   write block descriptors.
!-------------------------------------------------------------------------------
    CALL write_attribute(sid,"id",edge%id,h5in,h5err)
    CALL write_attribute(sid,"nvert",edge%nvert,h5in,h5err)
!-------------------------------------------------------------------------------
!   write seam data, combine to write larger arrays.
!-------------------------------------------------------------------------------
    ALLOCATE(exc(edge%nvert))
    DO iv=1,edge%nvert
      exc(iv)=SIZE(edge%vertex(iv)%ptr,2)
    ENDDO
    CALL dump_h5(sid,"np",exc,h5in,h5err)
    npt=SUM(exc)
    ALLOCATE(bptr(2,npt))
    IF (edge%id>0) ALLOCATE(bxy(2,edge%nvert))
    ipt=1_i4
    DO iv=1,edge%nvert
      np=SIZE(edge%vertex(iv)%ptr,2)
      bptr(:,ipt:ipt+np-1)=edge%vertex(iv)%ptr
      ipt=ipt+np
      IF (edge%id>0) bxy(:,iv)=edge%vertex(iv)%intxy
    ENDDO
    CALL dump_h5(sid,"vertex",bptr,h5in,h5err)
    DEALLOCATE(bptr)
    IF (edge%id>0) THEN
      CALL dump_h5(sid,"intxy",bxy,h5in,h5err)
      DEALLOCATE(bxy)
    ENDIF
!-------------------------------------------------------------------------------
!   write seam0 additional data
!-------------------------------------------------------------------------------
    IF (edge%id==0.AND.edge%nvert>0) THEN
      ALLOCATE(r0pt(edge%nvert))
      exc=0_i4
      r0pt=0_i4
      DO iv=1,edge%nvert
        IF (edge%excorner(iv)) exc(iv) = 1_i4
        IF (edge%r0point(iv)) r0pt(iv) = 1_i4
      ENDDO
      CALL dump_h5(sid,"excorner",exc,h5in,h5err)
      CALL dump_h5(sid,"r0point",r0pt,h5in,h5err)
      DEALLOCATE(r0pt)
    ENDIF
    DEALLOCATE(exc)
!-------------------------------------------------------------------------------
!   close group
!-------------------------------------------------------------------------------
    CALL close_group(TRIM(seam_name),sid,h5err)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE h5_dump

!-------------------------------------------------------------------------------
!> distribute edge to all mode processors from ilayer zero
!-------------------------------------------------------------------------------
  SUBROUTINE distribute(edge,inode)
    USE pardata_mod
    IMPLICIT NONE

    !> seam to distribute from ilayer 0
    CLASS(edge_type), INTENT(INOUT) :: edge
    !> mpi mode process to broadcast from
    INTEGER(i4), INTENT(IN) :: inode

    INTEGER(i4) :: iv,np
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'distribute',iftn,idepth)
!-------------------------------------------------------------------------------
!   send seam data
!-------------------------------------------------------------------------------
    CALL par%mode_bcast(edge%id,inode)
    CALL par%mode_bcast(edge%nvert,inode)
    CALL par%mode_bcast(edge%on_gpu,inode)
    IF (.NOT.ALLOCATED(edge%vertex)) ALLOCATE(edge%vertex(edge%nvert))
    DO iv=1,edge%nvert
      IF (par%ilayer==inode) np=SIZE(edge%vertex(iv)%ptr,2)
      CALL par%mode_bcast(np,inode)
      IF (.NOT.ALLOCATED(edge%vertex(iv)%ptr))                                  &
        ALLOCATE(edge%vertex(iv)%ptr(2,np))
      CALL par%mode_bcast(edge%vertex(iv)%ptr,2_i4*np,inode)
      IF (edge%id>0) CALL par%mode_bcast(edge%vertex(iv)%intxy,2_i4,inode)
    ENDDO
!-------------------------------------------------------------------------------
!   send seam0 additional data
!-------------------------------------------------------------------------------
    IF (edge%id==0.AND.edge%nvert>0) THEN
      IF (.NOT.ALLOCATED(edge%excorner)) ALLOCATE(edge%excorner(edge%nvert))
      IF (.NOT.ALLOCATED(edge%r0point)) ALLOCATE(edge%r0point(edge%nvert))
      CALL par%mode_bcast(edge%excorner,edge%nvert,inode)
      CALL par%mode_bcast(edge%r0point,edge%nvert,inode)
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE distribute

!-------------------------------------------------------------------------------
!> set the seam_save data arrays to 0.
!-------------------------------------------------------------------------------
  SUBROUTINE zero_save(edge)
    IMPLICIT NONE

    !> seam to zero save array
    CLASS(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'zero_save',iftn,idepth)
!-------------------------------------------------------------------------------
!   loop over vertex and segment structures to clear the seam_save
!   arrays.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      edge%vertex(iv)%seam_save=0._r8
      edge%segment(iv)%seam_save=0._r8
    ENDDO
    ASSOCIATE (vert_save=>edge%vert_save,seg_save=>edge%seg_save)
      !$acc kernels present(vert_save,seg_save) async(edge%id) if(edge%on_gpu)
      vert_save=0._r8
      seg_save=0._r8
      !$acc end kernels
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE zero_save

!-------------------------------------------------------------------------------
!> set the seam_csave data arrays to 0.
!-------------------------------------------------------------------------------
  SUBROUTINE zero_csave(edge)
    IMPLICIT NONE

    !> seam to zero csave array
    CLASS(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'zero_csave',iftn,idepth)
!-------------------------------------------------------------------------------
!   loop over vertex and segment structures to clear the seam_csave
!   arrays.
!-------------------------------------------------------------------------------
    DO iv=1,edge%nvert
      edge%vertex(iv)%seam_csave=0._r8
      edge%segment(iv)%seam_csave=0._r8
    ENDDO
    ASSOCIATE (vert_csave=>edge%vert_csave,seg_csave=>edge%seg_csave)
      !$acc kernels present(vert_csave,seg_csave) async(edge%id) if(edge%on_gpu)
      vert_csave=0._r8
      seg_csave=0._r8
      !$acc end kernels
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE zero_csave

!-------------------------------------------------------------------------------
!> use dir_in (+1 or -1) to set starting and ending indices for
!  the loops over side bases.  this routine avoids duplicating code.
!-------------------------------------------------------------------------------
  PURE SUBROUTINE load_limits(dir_in,nbases,istart,iend)
    IMPLICIT NONE

    !> direction to set istart to iend
    INTEGER(i4), INTENT(IN) :: dir_in
    !> length: iend-start+1
    INTEGER(i4), INTENT(IN) :: nbases
    !> output start index
    INTEGER(i4), INTENT(OUT) :: istart
    !> output end index
    INTEGER(i4), INTENT(OUT) :: iend

    IF (dir_in>0) THEN
      istart=1
      iend=nbases
    ELSE
      istart=nbases
      iend=1
    ENDIF
  END SUBROUTINE load_limits

END MODULE edge_mod
