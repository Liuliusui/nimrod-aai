! Decay of sine and cosine waves with periodic BCs on a rectangular mesh.
!
! This big example is for performance testing and runs with many features
! of the code.
&grid_input
  gridshape='rect'
  torgeom=F
  periodicity='both'
  mx=256
  my=256
  poly_degree=6
  nxbl=2
  nybl=2
  ! For CPU runs set below for more parallelism
  !nxbl=32
  !nybl=32
/
&exset_input
  field_type='real'
  init_type='waves'
  istep0=0
  nqty=1
/
&parallel_input
  decompflag=0
  single_stream=F
/
&laplace_input
  nstep=5
  dt=0.1
  diff=1.
  dirichlet_boundary_condition=F
  dump_file='dump.00000.h5'
  theta=0.5
  int_formula='gaussian'
  ngr=2
  static_condensation=T
  ! For GPU runs
  on_gpu=T
  timer_directive_wait=T ! For accurate kernel timings, faster overall if false
  ! For CPU runs
  !on_gpu=F
  !timer_directive_wait=F
  compute_integrand_alpha=T
  reset_timers_at_step2=T ! Don't consider initialization costs
  maxit=150
  tol=1.e-8
  precon_type='jacobi'
/
