!-------------------------------------------------------------------------------
!! General interface to test nodal FEM implementations
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!* General module to test nodal FEM implementations
!-------------------------------------------------------------------------------
MODULE test_nodal_real_mod
  USE local
  USE pardata_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: run_test_real

CONTAINS

!-------------------------------------------------------------------------------
!* driver routine for nodal_field_real to run tests based on input
!-------------------------------------------------------------------------------
  SUBROUTINE run_test_real(field,coord_field,test_name)
    USE nodal_mod
    IMPLICIT NONE

    CLASS(nodal_field_real), INTENT(INOUT) :: field
    CLASS(nodal_field_real), INTENT(IN) :: coord_field
    CHARACTER(64), INTENT(IN) :: test_name

!-------------------------------------------------------------------------------
!   Call different test cases.
!-------------------------------------------------------------------------------
    SELECT CASE(TRIM(test_name))
    CASE ("alloc_dealloc")
      ! do nothing
    CASE ("alloc_with_mold_assign")
      CALL test_ftn_alloc_with_mold_assign_real(field)
    CASE ("eval")
      CALL test_ftn_eval_real(field,coord_field)
    CASE ("alpha_eval")
      CALL test_alpha_eval_real(field,coord_field)
    CASE ("qp_update")
      CALL test_qp_update_real(field,coord_field)
    CASE ("h5io")
      CALL test_ftn_h5io_real(field)
    CASE DEFAULT
      CALL par%nim_write('No test named '//TRIM(test_name))
    END SELECT
  END SUBROUTINE run_test_real

!-------------------------------------------------------------------------------
!* test alloc_with_mold and assignment
!-------------------------------------------------------------------------------
  SUBROUTINE test_ftn_alloc_with_mold_assign_real(field)
    USE nodal_mod
    IMPLICIT NONE

    CLASS(nodal_field_real), INTENT(INOUT) :: field

    CLASS(nodal_field_real), ALLOCATABLE :: field2

    CALL field%alloc_with_mold(field2)
    field2=field ! nothing to compare but test assignment
    CALL field2%dealloc
  END SUBROUTINE test_ftn_alloc_with_mold_assign_real

!-------------------------------------------------------------------------------
!> Test evalutation of single points and batched evalutation in an element.
!  This relies on collocation working correctly, which is covered by this test.
!-------------------------------------------------------------------------------
  SUBROUTINE test_ftn_eval_real(field,coord_field)
    USE nodal_mod
    USE quadrature_mod
    USE fake_function_mod
    USE nimtest_utils
    IMPLICIT NONE

    CLASS(nodal_field_real), INTENT(INOUT) :: field
    CLASS(nodal_field_real), INTENT(IN) :: coord_field

    TYPE(interp_real) :: eval_out,eval_ref,coord
    TYPE(batch_interp_real) :: eval_batch_out,eval_batch_ref,coord_batch
    REAL(r8), DIMENSION(coord_field%nqty) :: xi
    INTEGER(i4), PARAMETER :: nbatch=2
    REAL(r8), DIMENSION(nbatch,coord_field%nqty) :: xi_batch
    INTEGER(i4) :: nf,ndf,ndim,ie,ib
    TYPE(fake_function_real) :: fake_function
    REAL(r8), ALLOCATABLE :: df(:,:),df_batch(:,:,:)

!-------------------------------------------------------------------------------
!   allocate output for checking
!-------------------------------------------------------------------------------
    nf=field%fqty*field%nqty
    ndf=field%dfqty*field%nqty
    ndim=coord_field%nqty
    ALLOCATE(eval_out%f(nf),eval_ref%f(nf))
    ALLOCATE(eval_out%df(ndf),eval_ref%df(ndf))
    ALLOCATE(eval_batch_out%f(nbatch,nf),eval_batch_ref%f(nbatch,nf))
    ALLOCATE(eval_batch_out%df(nbatch,ndf),eval_batch_ref%df(nbatch,ndf))
    ALLOCATE(df(field%nqty,field%dfqty),df_batch(nbatch,field%nqty,field%dfqty))
    ALLOCATE(coord%f(ndim),coord_batch%f(nbatch,ndim))
!-------------------------------------------------------------------------------
!   project the testing polynomial onto the field
!-------------------------------------------------------------------------------
    CALL fake_function%init(field%pd-1,field%deriv_type)
    CALL field%collocation(fake_function,coord_field)
!-------------------------------------------------------------------------------
!   test single point evaluation in each element of the field
!-------------------------------------------------------------------------------
    xi=0.5_r8
    DO ie=1,field%nel
      CALL coord_field%eval(xi,ie,coord,dorder=0,transform=.FALSE.)
      CALL field%eval(xi,ie,eval_out,dorder=1,                                  &
                      transform=.TRUE.,coord=coord_field)
      CALL fake_function%eval(coord%f,eval_ref%f)
      CALL fake_function%eval_d(coord%f,df)
      eval_ref%df=RESHAPE(TRANSPOSE(df),[ndf])
      CALL wrequal(eval_ref%f,eval_out%f,TRIM(field%name)//' eval_val'          &
                   //itoa(ie),tolerance=2.e-14_r8)
      CALL wrequal(eval_ref%df,eval_out%df,TRIM(field%name)//' eval_d_val'      &
                   //itoa(ie),tolerance=5.e-13_r8)
    ENDDO
!-------------------------------------------------------------------------------
!   test multi-point evaluation in the first element of the field
!-------------------------------------------------------------------------------
    DO ib=1,nbatch
      xi_batch(ib,:)=1.0_r8/REAL(ib+1,r8)
    ENDDO
    CALL coord_field%eval(xi_batch,1,coord_batch,dorder=0,transform=.FALSE.)
    DO ib=1,nbatch
      CALL fake_function%eval(coord_batch%f(ib,:),eval_batch_ref%f(ib,:))
      CALL fake_function%eval_d(coord_batch%f(ib,:),df_batch(ib,:,:))
      eval_batch_ref%df(ib,:)=RESHAPE(TRANSPOSE(df_batch(ib,:,:)),[ndf])
    ENDDO
    CALL field%eval(xi_batch,1,eval_batch_out,dorder=1,                         &
                    transform=.TRUE.,coord=coord_field)
    CALL wrequal(eval_batch_ref%f,eval_batch_out%f,TRIM(field%name)             &
                 //' eval_many',tolerance=2.e-14_r8)
    CALL wrequal(eval_batch_ref%df,eval_batch_out%df,TRIM(field%name)           &
                 //' eval_d_many',tolerance=3.e-13_r8)
  CONTAINS

    FUNCTION itoa(i) RESULT(res)
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: i

      CHARACTER(:), ALLOCATABLE :: res
      CHARACTER(range(i)+2) :: tmp

      WRITE(tmp,'(i0)') i
      res=TRIM(tmp)
    END FUNCTION itoa

  END SUBROUTINE test_ftn_eval_real

!-------------------------------------------------------------------------------
!* Test evalutation of the basis function evaluation in a shaped element.
!-------------------------------------------------------------------------------
  SUBROUTINE test_alpha_eval_real(field,coord_field)
    USE nodal_mod
    USE quadrature_mod
    USE poly_mod, ONLY: gauleg
    USE math_tran, ONLY: math_metric
    USE nimtest_utils
    IMPLICIT NONE

    CLASS(nodal_field_real), INTENT(INOUT) :: field
    CLASS(nodal_field_real), INTENT(IN) :: coord_field

    TYPE(qjac_real) :: metric
    TYPE(batch_interp_real) :: coord
    INTEGER(i4),PARAMETER :: ng=9
    INTEGER(i4) :: ig,iel,iq,nf,ndf,nqty
    TYPE(interp_real) :: eval_ref
    REAL(r8), DIMENSION(ng,field%ndim) :: xg
    REAL(r8), DIMENSION(ng,field%fqty) :: alpha_ref
    REAL(r8), DIMENSION(ng,field%dfqty) :: dalpha_ref
    REAL(r8), DIMENSION(field%nqty,field%ndof) :: field_dof
    REAL(r8) :: jac(ng,field%ndim,field%ndim),ijac(ng,field%ndim,field%ndim)

    nf=field%fqty*field%nqty
    ndf=field%dfqty*field%nqty
    nqty=field%nqty
    DO ig=1,ng
      xg(ig,:)=REAL(ig,r8)/(ng+1)
    ENDDO
    CALL field%init_basis_ftn(xg)
    ALLOCATE(coord%f(ng,coord_field%nqty*coord_field%fqty))
    ALLOCATE(coord%df(ng,coord_field%nqty*coord_field%dfqty))
    ALLOCATE(metric%jac(ng,field%nel,field%ndim,field%ndim))
    ALLOCATE(metric%ijac(ng,field%nel,field%ndim,field%ndim))
    ALLOCATE(metric%detj(ng,field%nel))
    ALLOCATE(eval_ref%f(nf),eval_ref%df(ndf))
!-------------------------------------------------------------------------------
!   set up a fake Jacobian matrix
!-------------------------------------------------------------------------------
    DO iel=1,field%nel
      CALL coord_field%eval(xg,iel,coord,dorder=1,transform=.FALSE.)
      metric%jac(:,iel,:,:)=RESHAPE(coord%df,[ng,field%ndim,field%ndim])
!-------------------------------------------------------------------------------
!     invert the Jacobian matrix and save partial derivatives.
!-------------------------------------------------------------------------------
      jac=metric%jac(:,iel,:,:)
      CALL math_metric(jac,ijac,metric%detj(:,iel))
      metric%ijac(:,iel,:,:)=ijac
    ENDDO
!-------------------------------------------------------------------------------
!   evaluate the first basis function, which is common between alpha and field
!-------------------------------------------------------------------------------
    field_dof=0._r8
    field_dof(:,1)=1._r8
    CALL field%set_field(field_dof)
    CALL field%init_block_basis_ftn(metric,ng)
    alpha_ref=0._r8
    dalpha_ref=0._r8
    DO ig=1,ng
      CALL field%eval(xg(ig,:),1,eval_ref,dorder=1,                             &
                      transform=.TRUE.,coord=coord_field)
      DO iq=1,nqty
        alpha_ref(ig,:)=alpha_ref(ig,:)                                         &
                       +eval_ref%f((iq-1)*field%fqty+1:iq*field%fqty)/nqty
        dalpha_ref(ig,:)=dalpha_ref(ig,:)                                       &
                        +eval_ref%df((iq-1)*field%dfqty+1:iq*field%dfqty)/nqty
      ENDDO
    ENDDO
    CALL wrequal(field%qab%alf(:,1,1,:),alpha_ref,                              &
                  TRIM(field%name)//'alpha_eval_real',tolerance=1.e-14_r8)
    CALL wrequal(field%qab%aldf(:,1,1,:),dalpha_ref,                            &
                 TRIM(field%name)//'alpha_deriv_real',tolerance=1.e-13_r8)
  END SUBROUTINE test_alpha_eval_real

!-------------------------------------------------------------------------------
!* test quadrature point update routines
!-------------------------------------------------------------------------------
  SUBROUTINE test_qp_update_real(field,coord_field)
    USE nodal_mod
    USE quadrature_mod
    USE fake_function_mod
    USE math_tran, ONLY: math_metric
    USE nimtest_utils
    IMPLICIT NONE

    CLASS(nodal_field_real), INTENT(INOUT) :: field
    CLASS(nodal_field_real), INTENT(IN) :: coord_field

    TYPE(qp_real) :: qp_out,qp_ref
    TYPE(batch_interp_real) :: eval_ref,coord
    TYPE(qjac_real) :: metric
    INTEGER(i4),PARAMETER :: ng=9
    REAL(r8), DIMENSION(ng,field%ndim) :: xg
    INTEGER(i4) :: iel,ig
    TYPE(fake_function_real) :: fake_function
    REAL(r8) :: jac(ng,2,2),ijac(ng,2,2)

    DO ig=1,ng
      xg(ig,:)=REAL(ig,r8)/(ng+1)
    ENDDO
    CALL field%init_basis_ftn(xg)
!-------------------------------------------------------------------------------
!   set up a fake Jacobian matrix
!-------------------------------------------------------------------------------
    ALLOCATE(coord%f(ng,coord_field%nqty*coord_field%fqty))
    ALLOCATE(coord%df(ng,coord_field%nqty*coord_field%dfqty))
    ALLOCATE(metric%jac(ng,field%nel,field%ndim,field%ndim))
    ALLOCATE(metric%ijac(ng,field%nel,field%ndim,field%ndim))
    ALLOCATE(metric%detj(ng,field%nel))
    DO iel=1,field%nel
      CALL coord_field%eval(xg,iel,coord,dorder=1,transform=.FALSE.)
      metric%jac(:,iel,:,:)=RESHAPE(coord%df,[ng,field%ndim,field%ndim])
!-------------------------------------------------------------------------------
!     invert the Jacobian matrix and save partial derivatives.
!-------------------------------------------------------------------------------
      jac=metric%jac(:,iel,:,:)
      CALL math_metric(jac,ijac,metric%detj(:,iel))
      metric%ijac(:,iel,:,:)=ijac
    ENDDO
    !$acc enter data copyin(metric%jac,metric%ijac,metric%detj)                 &
    !$acc async(field%id) if(field%on_gpu)
    CALL field%qp_alloc(qp_out,ng)
    CALL field%qp_alloc(qp_ref,ng)
    ALLOCATE(eval_ref%f(ng,field%nqty*field%fqty))
    ALLOCATE(eval_ref%df(ng,field%nqty*field%dfqty))
!-------------------------------------------------------------------------------
!   project the testing polynomial onto the field
!-------------------------------------------------------------------------------
    CALL fake_function%init(field%pd,'none')
    CALL field%collocation(fake_function,coord_field)
    CALL field%qp_update(qp_out,transform=.TRUE.,metric=metric)
    !$acc update self(qp_out%qpf) async(field%id) if(field%on_gpu)
    !$acc update self(qp_out%qpdf) async(field%id) if(field%on_gpu)
    DO iel=1,field%nel
      CALL field%eval(xg,iel,eval_ref,dorder=1,                                 &
                      transform=.TRUE.,coord=coord_field)
      qp_ref%qpf(:,iel,:,:)=RESHAPE(eval_ref%f,[ng,field%fqty,field%nqty])
      qp_ref%qpdf(:,iel,:,:)=RESHAPE(eval_ref%df,[ng,field%dfqty,field%nqty])
    ENDDO
    CALL wrequal(qp_out%qpf,qp_ref%qpf,TRIM(field%name)//'qp_update_real',      &
                 tolerance=3.e-14_r8)
    CALL wrequal(qp_out%qpdf,qp_ref%qpdf,TRIM(field%name)//'qp_update_d_real',  &
                 tolerance=5.e-13_r8)
    CALL field%qp_dealloc(qp_out)
    CALL field%qp_dealloc(qp_ref)
    !$acc exit data delete(metric%jac,metric%ijac,metric%detj) finalize         &
    !$acc async(field%id) if(field%on_gpu)
  END SUBROUTINE test_qp_update_real

!-------------------------------------------------------------------------------
!* test Input/Output with write, read then compare
!-------------------------------------------------------------------------------
  SUBROUTINE test_ftn_h5io_real(field)
    USE io
    USE nodal_mod
    USE nimtest_utils
    IMPLICIT NONE

    CLASS(nodal_field_real), INTENT(INOUT) :: field

    REAL(r8), ALLOCATABLE, DIMENSION(:,:) :: fvalue,fcomp
    INTEGER(i4) :: id,iq,ioff
    CHARACTER(32) :: fieldname
    CHARACTER(128) :: filename
!-------------------------------------------------------------------------------
!   set field values
!-------------------------------------------------------------------------------
    ALLOCATE(fvalue(field%nqty,field%ndof))
    fvalue=0._r8
    ioff=0_i4
    DO id=1,field%ndof
      DO iq=1,field%nqty
        fvalue(iq,id)=iq+ioff
      ENDDO
      ioff=ioff+field%nqty
    ENDDO
    CALL field%set_field(fvalue)
!-------------------------------------------------------------------------------
!   initialize file
!-------------------------------------------------------------------------------
    filename=TRIM(field%name)//"_nodal_real_test.h5"
    CALL open_newh5file(filename,fileid,"Unit test field",rootgid,h5in,h5err)
!-------------------------------------------------------------------------------
!   write field
!-------------------------------------------------------------------------------
    fieldname=TRIM(field%name)
    CALL field%h5_dump(TRIM(fieldname),rootgid,"0001")
    CALL close_h5file(fileid,rootgid,h5err)
!-------------------------------------------------------------------------------
!   deallocate
!-------------------------------------------------------------------------------
    CALL field%dealloc
!-------------------------------------------------------------------------------
!   read it from the file
!-------------------------------------------------------------------------------
    CALL open_oldh5file(filename,fileid,rootgid,h5in,h5err)
    CALL field%h5_read(TRIM(fieldname),rootgid,1,"0001")
    CALL close_h5file(fileid,rootgid,h5err)
!-------------------------------------------------------------------------------
!   compare
!-------------------------------------------------------------------------------
    ALLOCATE(fcomp(field%nqty,field%ndof))
    CALL field%get_field(fcomp)
    CALL wrequal(fvalue,fcomp,TRIM(fieldname)//' h5 I/O')
    DEALLOCATE(fvalue,fcomp)
  END SUBROUTINE test_ftn_h5io_real

END MODULE test_nodal_real_mod
