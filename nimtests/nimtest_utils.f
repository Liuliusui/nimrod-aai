!-------------------------------------------------------------------------------
!< Contains routines for simplifying the writing of unit tests.
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!> Contains routines for simplifying the writing of unit tests.
!  Tests follow the Test Anything Protocol (TAP) https://testanything.org
!-------------------------------------------------------------------------------
MODULE nimtest_utils
  USE local, ONLY: i4,r8
  IMPLICIT NONE

  INTERFACE wrequal
    MODULE PROCEDURE wrequal_real,wrequal_real_1D,wrequal_real_2D,              &
                     wrequal_real_3D,wrequal_real_4D,                           &
                     wrequal_cplx,wrequal_cplx_1D,wrequal_cplx_2D,              &
                     wrequal_cplx_3D,wrequal_cplx_4D,wrequal_cplx_5D,           &
                     wrequal_int,wrequal_int_1D,wrequal_int_2D,wrequal_int_3D,  &
                     wrequal_log,wrequal_log_1D,wrequal_char
  END INTERFACE

  !> Default global test tolerance
  REAL(r8) :: global_test_tolerance=0._r8
CONTAINS

!-------------------------------------------------------------------------------
!> wrtrue: passes if the passed in boolean is true
!-------------------------------------------------------------------------------
  SUBROUTINE wrtrue(testval, label)
    IMPLICIT NONE

    LOGICAL, INTENT(IN) :: testval
    CHARACTER(*), INTENT(IN) :: label

    IF (.NOT. testval) THEN
      WRITE(*,*) 'not ok - wrtrue: ',label
    ELSE
      WRITE(*,*) 'ok - wrtrue: ',label
    ENDIF
  END SUBROUTINE wrtrue

!-------------------------------------------------------------------------------
!> wrfalse: passes if the passed in boolean is false
!-------------------------------------------------------------------------------
  SUBROUTINE wrfalse(testval,label)
    IMPLICIT NONE

    LOGICAL, INTENT(IN) :: testval
    CHARACTER(*), INTENT(IN) :: label

    IF (testval) THEN
      WRITE(*,*) 'not ok - wrtfalse: ',label
    ELSE
      WRITE(*,*) 'ok - wrtfalse: ',label
    ENDIF
  END SUBROUTINE wrfalse

!-------------------------------------------------------------------------------
!>  wrequal_real: passes if the passed in r8 values are equal, or if
!   tolerance is specified if equal within tolerance
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_real(in1, in2, label, tolerance)
    IMPLICIT NONE

    REAL(r8), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label
    REAL(r8), INTENT(IN), OPTIONAL :: tolerance

    IF (PRESENT(tolerance)) THEN
      IF (ABS(in1-in2)<=tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' diff is: ', ABS(in1-in2)
      ENDIF
    ELSE
      IF (ABS(in1-in2)<=global_test_tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' values: ', in1,in2
      ENDIF
    ENDIF
  END SUBROUTINE wrequal_real

!-------------------------------------------------------------------------------
!>  wrequal_real_1D: passes if the passed in r8 values are equal, or if
!   tolerance is specified if equal within tolerance
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_real_1D(in1, in2, label, tolerance)
    IMPLICIT NONE

    REAL(r8), DIMENSION(:), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label
    REAL(r8), INTENT(IN), OPTIONAL :: tolerance

    REAL(r8) :: maxvalue

    maxvalue=MAXVAL(ABS(in1-in2))
    IF (PRESENT(tolerance)) THEN
      IF (maxvalue<=tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ELSE
      IF (maxvalue<=global_test_tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ENDIF
  END SUBROUTINE wrequal_real_1D

!-------------------------------------------------------------------------------
!>  wrequal_real_2D: passes if the passed in r8 values are equal, or if
!   tolerance is specified if equal within tolerance
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_real_2D(in1, in2, label, tolerance)
    IMPLICIT NONE

    REAL(r8), DIMENSION(:,:), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label
    REAL(r8), INTENT(IN), OPTIONAL :: tolerance

    REAL(r8) :: maxvalue

    maxvalue=MAXVAL(ABS(in1-in2))
    IF (PRESENT(tolerance)) THEN
      IF (maxvalue<=tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ELSE
      IF (maxvalue<=global_test_tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ENDIF
  END SUBROUTINE wrequal_real_2D

!-------------------------------------------------------------------------------
!>  wrequal_real_3D: passes if the passed in r8 values are equal, or if
!   tolerance is specified if equal within tolerance
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_real_3D(in1, in2, label, tolerance)
    IMPLICIT NONE

    REAL(r8), DIMENSION(:,:,:), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label
    REAL(r8), INTENT(IN), OPTIONAL :: tolerance

    REAL(r8) :: maxvalue

    maxvalue=MAXVAL(ABS(in1-in2))
    IF (PRESENT(tolerance)) THEN
      IF (maxvalue<=tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ELSE
      IF (maxvalue<=global_test_tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ENDIF
  END SUBROUTINE wrequal_real_3D

!-------------------------------------------------------------------------------
!>  wrequal_real_4D: passes if the passed in r8 values are equal, or if
!   tolerance is specified if equal within tolerance
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_real_4D(in1, in2, label, tolerance)
    IMPLICIT NONE

    REAL(r8), DIMENSION(:,:,:,:), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label
    REAL(r8), INTENT(IN), OPTIONAL :: tolerance

    REAL(r8) :: maxvalue

    maxvalue=MAXVAL(ABS(in1-in2))
    IF (PRESENT(tolerance)) THEN
      IF (maxvalue<=tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ELSE
      IF (maxvalue<=global_test_tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ENDIF
  END SUBROUTINE wrequal_real_4D

!-------------------------------------------------------------------------------
!>  wrequal_cplx: passes if the passed in r8 values are equal, or if
!   tolerance is specified if equal within tolerance
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_cplx(in1, in2, label, tolerance)
    IMPLICIT NONE

    COMPLEX(r8), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label
    REAL(r8), INTENT(IN), OPTIONAL :: tolerance

    COMPLEX(r8) :: ind
    REAL(r8) :: indnorm

    ind=in1-in2
    indnorm=ABS(ind)
    IF (PRESENT(tolerance)) THEN
      IF (indnorm<=tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' diff is: ', indnorm
      ENDIF
    ELSE
      IF (indnorm<=global_test_tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' values: ', in1,in2
      ENDIF
    ENDIF
  END SUBROUTINE wrequal_cplx

!-------------------------------------------------------------------------------
!>  wrequal_cplx_1D: passes if the passed in r8 values are equal, or if
!   tolerance is specified if equal within tolerance. It's better to compare
!   the norm of the difference, than the difference of the norms
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_cplx_1D(in1, in2, label, tolerance)
    IMPLICIT NONE

    COMPLEX(r8), DIMENSION(:), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label
    REAL(r8), INTENT(IN), OPTIONAL :: tolerance

    COMPLEX(r8), DIMENSION(:), ALLOCATABLE :: ind
    REAL(r8) :: maxvalue

    ALLOCATE(ind(SIZE(in1)))
    ind=in1-in2
    maxvalue=MAXVAL(ABS(ind))
    IF (PRESENT(tolerance)) THEN
      IF (maxvalue<=tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ELSE
      IF (maxvalue==0.) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ENDIF
  END SUBROUTINE wrequal_cplx_1D

!-------------------------------------------------------------------------------
!>  wrequal_cplx_2D: passes if the passed in r8 values are equal, or if
!   tolerance is specified if equal within tolerance
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_cplx_2D(in1, in2, label, tolerance)
    IMPLICIT NONE

    COMPLEX(r8), DIMENSION(:,:), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label
    REAL(r8), INTENT(IN), OPTIONAL :: tolerance

    COMPLEX(r8), DIMENSION(:,:), ALLOCATABLE :: ind
    REAL(r8) :: maxvalue

    ALLOCATE(ind(SIZE(in1,1),SIZE(in1,2)))
    ind=in1-in2
    maxvalue=MAXVAL(ABS(ind))
    IF (PRESENT(tolerance)) THEN
      IF (maxvalue<=tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ELSE
      IF (maxvalue<=global_test_tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ENDIF
  END SUBROUTINE wrequal_cplx_2D

!-------------------------------------------------------------------------------
!>  wrequal_cplx_3D: passes if the passed in r8 values are equal, or if
!   tolerance is specified if equal within tolerance
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_cplx_3D(in1, in2, label, tolerance)
    IMPLICIT NONE

    COMPLEX(r8), DIMENSION(:,:,:), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label
    REAL(r8), INTENT(IN), OPTIONAL :: tolerance

    COMPLEX(r8), DIMENSION(:,:,:), ALLOCATABLE :: ind
    REAL(r8) :: maxvalue

    ALLOCATE(ind(SIZE(in1,1),SIZE(in1,2),SIZE(in1,3)))
    ind=in1-in2
    maxvalue=MAXVAL(ABS(ind))
    IF (PRESENT(tolerance)) THEN
      IF (maxvalue<=tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ELSE
      IF (maxvalue<=global_test_tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ENDIF
  END SUBROUTINE wrequal_cplx_3D

!-------------------------------------------------------------------------------
!>  wrequal_cplx_4D: passes if the passed in r8 values are equal, or if
!   tolerance is specified if equal within tolerance
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_cplx_4D(in1, in2, label, tolerance)
    IMPLICIT NONE

    COMPLEX(r8), DIMENSION(:,:,:,:), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label
    REAL(r8), INTENT(IN), OPTIONAL :: tolerance

    COMPLEX(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: ind
    REAL(r8) :: maxvalue

    ALLOCATE(ind(SIZE(in1,1),SIZE(in1,2),SIZE(in1,3),SIZE(in1,4)))
    ind=in1-in2
    maxvalue=MAXVAL(ABS(ind))
    IF (PRESENT(tolerance)) THEN
      IF (maxvalue<=tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ELSE
      IF (maxvalue<=global_test_tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ENDIF
  END SUBROUTINE wrequal_cplx_4D

!-------------------------------------------------------------------------------
!>  wrequal_cplx_5D: passes if the passed in r8 values are equal, or if
!   tolerance is specified if equal within tolerance
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_cplx_5D(in1, in2, label, tolerance)
    IMPLICIT NONE

    COMPLEX(r8), DIMENSION(:,:,:,:,:), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label
    REAL(r8), INTENT(IN), OPTIONAL :: tolerance

    COMPLEX(r8), DIMENSION(:,:,:,:,:), ALLOCATABLE :: ind
    REAL(r8) :: maxvalue

    ALLOCATE(ind(SIZE(in1,1),SIZE(in1,2),SIZE(in1,3),SIZE(in1,4),SIZE(in1,5)))
    ind=in1-in2
    maxvalue=MAXVAL(ABS(ind))
    IF (PRESENT(tolerance)) THEN
      IF (maxvalue<=tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ELSE
      IF (maxvalue<=global_test_tolerance) THEN
        WRITE(*,*) 'ok - wrequal: ',label
      ELSE
        WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
      ENDIF
    ENDIF
  END SUBROUTINE wrequal_cplx_5D

!-------------------------------------------------------------------------------
!*  wrequal_int: passes if the passed in integers are equal
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_int(in1, in2, label)
    IMPLICIT NONE

    INTEGER(i4), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label

    IF (in1==in2) THEN
      WRITE(*,*) 'ok - wrequal: ',label
    ELSE
      WRITE(*,*) 'not ok - wrequal: ',label, in1, ', ',in2
    ENDIF
  END SUBROUTINE wrequal_int

!-------------------------------------------------------------------------------
!*  wrequal_int_1D: passes if the passed in i4 values are equal
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_int_1D(in1, in2, label)
    IMPLICIT NONE

    INTEGER(i4), DIMENSION(:), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label

    REAL(r8) :: maxvalue

    maxvalue=MAXVAL(ABS(in1-in2))
    IF (maxvalue==0) THEN
      WRITE(*,*) 'ok - wrequal: ',label
    ELSE
      WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
    ENDIF
  END SUBROUTINE wrequal_int_1D

!-------------------------------------------------------------------------------
!*  wrequal_int_2D: passes if the passed in i4 values are equal
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_int_2D(in1, in2, label)
    IMPLICIT NONE

    INTEGER(i4), DIMENSION(:,:), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label

    REAL(r8) :: maxvalue

    maxvalue=MAXVAL(ABS(in1-in2))
    IF (maxvalue==0) THEN
      WRITE(*,*) 'ok - wrequal: ',label
    ELSE
      WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
    ENDIF
  END SUBROUTINE wrequal_int_2D

!-------------------------------------------------------------------------------
!*  wrequal_int_3D: passes if the passed in i4 values are equal
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_int_3D(in1, in2, label)
    IMPLICIT NONE

    INTEGER(i4), DIMENSION(:,:,:), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label

    REAL(r8) :: maxvalue

    maxvalue=MAXVAL(ABS(in1-in2))
    IF (maxvalue==0) THEN
      WRITE(*,*) 'ok - wrequal: ',label
    ELSE
      WRITE(*,*) 'not ok - wrequal: ',label, ' max diff value: ', maxvalue
    ENDIF
  END SUBROUTINE wrequal_int_3D

!-------------------------------------------------------------------------------
!*  wrequal_log: passes if the passed in logicals are equal
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_log(in1, in2, label)
    IMPLICIT NONE

    LOGICAL(i4), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label

    IF (in1.EQV.in2) THEN
      WRITE(*,*) 'ok - wrequal: ',label
    ELSE
      WRITE(*,*) 'not ok - wrequal: ',label, in1, ', ',in2
    ENDIF
  END SUBROUTINE wrequal_log

!-------------------------------------------------------------------------------
!*  wrequal_log_1D: passes if the passed in logicals are equal
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_log_1D(in1, in2, label)
    IMPLICIT NONE

    LOGICAL(i4), DIMENSION(:), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label

    IF (ALL(in1.EQV.in2)) THEN
      WRITE(*,*) 'ok - wrequal: ',label
    ELSE
      WRITE(*,*) 'not ok - wrequal: ',label, in1, ', ',in2
    ENDIF
  END SUBROUTINE wrequal_log_1D

!-------------------------------------------------------------------------------
!>  wrequal_char: passes if the passed in character stings are equal
!   ignoring white space
!-------------------------------------------------------------------------------
  SUBROUTINE wrequal_char(in1, in2, label)
    IMPLICIT NONE

    CHARACTER(*), INTENT(IN) :: in1, in2
    CHARACTER(*), INTENT(IN) :: label

    IF (ADJUSTL(TRIM(in1))==(ADJUSTL(TRIM(in2)))) THEN
      WRITE(*,*) 'ok - wrequal: ',label
    ELSE
      WRITE(*,*) 'not ok - wrequal: ',label, TRIM(in1), ', ', TRIM(in2)
    ENDIF
  END SUBROUTINE wrequal_char

END MODULE nimtest_utils
