!-------------------------------------------------------------------------------
!< Defines the abstract interfaces for generic functions.
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> Defines the abstract interfaces for generic functions. We require separate
!  interfaces for REAL and COMPLEX output.
!-------------------------------------------------------------------------------
MODULE function_mod
  USE local
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: abstract_function_real, abstract_function_comp, function_1D

!-------------------------------------------------------------------------------
!> Abstract base type that defines a general function with real dependent
!  variables and real output. Because some of these functions may want to store
!  references or state, e.g. an arbitrary constant or reference to a field,
!  this is implemented as a type rather than just an abstract interface.
!-------------------------------------------------------------------------------
  TYPE, ABSTRACT :: abstract_function_real
  CONTAINS

    PROCEDURE(eval_real), PASS(func), DEFERRED :: eval
    PROCEDURE, PASS(func) :: eval_d=>eval_d_real
    PROCEDURE, PASS(func) :: find_root=>find_root_real
  END TYPE abstract_function_real

!-------------------------------------------------------------------------------
!> Abstract base type that defines a general function with real dependent
!  variables and complex output. Because some of these functions may want to
!  store references or state, e.g. an arbitrary constant or reference to a
!  field, this is implemented as a type rather than just an abstract interface.
!-------------------------------------------------------------------------------
  TYPE, ABSTRACT :: abstract_function_comp
  CONTAINS

    PROCEDURE(eval_comp), PASS(func), DEFERRED :: eval
  END TYPE abstract_function_comp

!-------------------------------------------------------------------------------
!> Abstract base type that defines a simple 1D function. Since some functions
!  require saved state, this is implemented as a type rather than interface.
!-------------------------------------------------------------------------------
  TYPE, ABSTRACT :: function_1D
  CONTAINS

    PROCEDURE(eval_1D), PASS(func), DEFERRED :: eval
    PROCEDURE(eval_1D), PASS(func), DEFERRED :: eval_d
  END TYPE function_1D

  ABSTRACT INTERFACE
!-------------------------------------------------------------------------------
!>  Real function evaluator.
!-------------------------------------------------------------------------------
    SUBROUTINE eval_real(func, in_var, out_var)
      USE local
      IMPORT abstract_function_real
      !> function being evaluated
      CLASS(abstract_function_real), INTENT(IN) :: func
      !> array of independent input variable(s)
      REAL(r8), DIMENSION(:), INTENT(IN) :: in_var
      !> array of dependent output variable(s)
      REAL(r8), DIMENSION(:), INTENT(OUT) :: out_var
    END SUBROUTINE eval_real

!-------------------------------------------------------------------------------
!>  Complex function evaluator.
!-------------------------------------------------------------------------------
    SUBROUTINE eval_comp(func, in_var, out_var)
      USE local
      IMPORT abstract_function_comp
      !> function being evaluated
      CLASS(abstract_function_comp), INTENT(IN) :: func
      !> array of independent input variable(s)
      REAL(r8), DIMENSION(:), INTENT(IN) :: in_var
      !> array of dependent output variable(s)
      COMPLEX(r8), DIMENSION(:,:), INTENT(OUT) :: out_var
    END SUBROUTINE eval_comp

!-------------------------------------------------------------------------------
!>  Simple function evaluator.
!-------------------------------------------------------------------------------
    ELEMENTAL REAL(r8) FUNCTION eval_1D(func, input) RESULT(output)
      USE local
      IMPORT function_1D
      !> function being evaluated
      CLASS(function_1D), INTENT(IN) :: func
      !> scalar input variable
      REAL(r8), INTENT(IN) :: input
    END FUNCTION eval_1D
  END INTERFACE

CONTAINS

!-------------------------------------------------------------------------------
!> Define a default derivate function, can be overridden with analytic version
!  as appropriate
!-------------------------------------------------------------------------------
  SUBROUTINE eval_d_real(func,in_var,doutdin_var)
    USE local
    IMPLICIT NONE

    !> function being evaluated
    CLASS(abstract_function_real), INTENT(IN) :: func
    !> array of independent input variable(s)
    REAL(r8), DIMENSION(:), INTENT(IN) :: in_var
    !> array of dependent output variable(s) for function derivative, df/dx
    !> size is (out_var_size,in_var_size) as appropriate for gradients
    REAL(r8), DIMENSION(:,:), INTENT(OUT) :: doutdin_var

    REAL(r8), DIMENSION(SIZE(in_var,1)) :: in_var_d1,in_var_d2
    REAL(r8), DIMENSION(SIZE(doutdin_var,1)) :: out_var1,out_var2
    REAL(r8) :: denom
    REAL(r8), PARAMETER :: eps_d=1.e-8
    INTEGER(i4) :: id

!-------------------------------------------------------------------------------
!   evaluate the derivative using a central difference
!-------------------------------------------------------------------------------
    DO id=1,SIZE(in_var,1)
      IF (ABS(in_var(id))<TINY(eps_d)/eps_d) THEN
        in_var_d1(id)=in_var(id)+eps_d
        in_var_d2(id)=in_var(id)-eps_d
        denom=2.0*eps_d
      ELSE
        in_var_d1(id)=in_var(id)*(1.0+eps_d)
        in_var_d2(id)=in_var(id)*(1.0-eps_d)
        denom=2.0*eps_d*in_var(id)
      ENDIF
      CALL func%eval(in_var_d1,out_var1)
      CALL func%eval(in_var_d2,out_var2)
      doutdin_var(:,id)=(out_var1-out_var2)/denom
    ENDDO
  END SUBROUTINE eval_d_real

!-------------------------------------------------------------------------------
!>  Use a Newton method to find the root of the function
!-------------------------------------------------------------------------------
  SUBROUTINE find_root_real(func,in_var,out_var)
    USE local
    USE pardata_mod
    IMPLICIT NONE

    !> function being evaluated
    CLASS(abstract_function_real), INTENT(IN) :: func
    !> array of independent input variable(s) to use as guess and return as root
    REAL(r8), DIMENSION(:), INTENT(INOUT) :: in_var
    !> array of dependent output variable(s), sets size of output
    REAL(r8), DIMENSION(:), INTENT(OUT) :: out_var

    REAL(r8), DIMENSION(SIZE(in_var,1)) :: delta
    REAL(r8), DIMENSION(SIZE(out_var,1),SIZE(in_var,1)) :: doutdin
    REAL(r8), PARAMETER :: rtol=1.e-14,atol=1.e-40
    INTEGER(i4) :: istep
    CHARACTER(512) :: msg

    istep=0
    delta=0._r8
    CALL func%eval(in_var,out_var)
    CALL func%eval_d(in_var,doutdin)
!-------------------------------------------------------------------------------
!   write routines for certain size variables as jacobian inversion could be
!   requried
!-------------------------------------------------------------------------------
    IF (SIZE(out_var,1)==1) THEN
      DO WHILE(NORM2(delta) >= MAX(rtol*NORM2(in_var),atol).OR.                 &
               NORM2(out_var) >= MAX(rtol*NORM2(doutdin(1,:)*delta),atol))
        delta(:)=out_var(1)/doutdin(1,:)
        in_var=in_var-delta
        CALL func%eval(in_var,out_var)
        istep=istep+1
        IF (istep==10000) THEN
          WRITE(msg,*) 'Newton iteration failed at ',in_var,delta,out_var
          CALL par%nim_write(msg)
          EXIT
        ENDIF
        CALL func%eval_d(in_var,doutdin)
      ENDDO
    ELSE
      CALL par%nim_stop('implementation needed for array sizes in find_root')
    ENDIF
  END SUBROUTINE find_root_real

END MODULE function_mod
