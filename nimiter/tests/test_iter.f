!-------------------------------------------------------------------------------
!* test iter routines driver
!-------------------------------------------------------------------------------
PROGRAM test_iter
  USE local
  USE vector_mod
  USE matrix_mod
  USE seam_mod
  USE pardata_mod
  USE timer_mod
  USE linalg_utils_mod, ONLY: apply_ave_factor
  USE linalg_registry_mod
  IMPLICIT NONE

  TYPE(rvec_storage), ALLOCATABLE :: rvec(:)
  TYPE(cv1m_storage), ALLOCATABLE :: cv1m(:)
  TYPE(cvec_storage), ALLOCATABLE :: cvec(:)
  TYPE(rmat_storage), ALLOCATABLE :: rmat(:)
  TYPE(cmat_storage), ALLOCATABLE :: cmat(:,:)
  TYPE(seam_type) :: seam
  INTEGER(i4) :: nargs,itest
  CHARACTER(64) :: test_name
  INTEGER(i4) :: idepth
  INTEGER(i4), SAVE :: iftn=-1
!-------------------------------------------------------------------------------
! determine which test to run from the command line
!-------------------------------------------------------------------------------
  nargs=command_argument_count()
  IF (nargs /= 2) THEN
    CALL print_usage
    CALL par%nim_stop('Argument error')
  ENDIF
  on_gpu=.TRUE.
  CALL get_command_argument(1,test_type)
  CALL get_command_argument(2,test_name)
  CALL par%init
  CALL timer%init
  CALL timer%start_timer_l0('test','test_iter',iftn,idepth)
!-------------------------------------------------------------------------------
! setup registry based on test type
!-------------------------------------------------------------------------------
  CALL set_test_type
!-------------------------------------------------------------------------------
! for the purposes of testing set apply_ave_factor=.TRUE. in linalg_utils_mod
!-------------------------------------------------------------------------------
  apply_ave_factor=.TRUE.
!-------------------------------------------------------------------------------
! loop over test parameters
!-------------------------------------------------------------------------------
  DO itest=1,ntests
    CALL setup_linalg(rvec,cv1m,cvec,rmat,cmat,seam,itest)
!-------------------------------------------------------------------------------
!   run the test
!-------------------------------------------------------------------------------
    SELECT CASE(TRIM(test_name))
    CASE ("cg_no_precon")
      CALL test_no_precon(rvec,cv1m,cvec,rmat,cmat,seam,'cg')
    CASE ("cg_jacobi")
      CALL test_cg_jacobi(rvec,cv1m,cvec,rmat,cmat,seam)
    CASE DEFAULT
      CALL par%nim_write('No test named '//TRIM(test_type))
      CALL print_usage
    END SELECT
    CALL teardown_linalg(rvec,cv1m,cvec,rmat,cmat,seam)
  ENDDO
  CALL timer%end_timer_l0(iftn,idepth)
  CALL timer%report
  CALL timer%finalize
  CALL par%nim_stop('Normal termination.',clean_shutdown=.TRUE.)
CONTAINS

!-------------------------------------------------------------------------------
! helper routine to print usage
!-------------------------------------------------------------------------------
  SUBROUTINE print_usage
    CALL par%nim_write('Usage: ./test_iter <test type> <test name>')
    CALL print_types
    CALL par%nim_write('  where <test name> is one of')
    CALL par%nim_write('    cg_no_precon')
    CALL par%nim_write('    cg_jacobi')
    CALL par%nim_stop('check input')
  END SUBROUTINE print_usage

!-------------------------------------------------------------------------------
!* test a real iterative solve of I x = b for x.
!-------------------------------------------------------------------------------
  SUBROUTINE test_solve_real(rrhs,rmat,seam,itdat,precon,solve_type,err_check)
    USE iterdata_mod
    USE linalg_utils_mod, ONLY: norm,resid_norm
    USE iter_all_mod
    USE preconditioner_mod
    IMPLICIT NONE

    !* test rvec
    TYPE(rvec_storage), INTENT(INOUT) :: rrhs(par%nbl)
    !* test rmat
    TYPE(rmat_storage), INTENT(INOUT) :: rmat(par%nbl)
    !* test seam
    TYPE(seam_type), INTENT(INOUT) :: seam
    !* test iteration data
    TYPE(iterdata), INTENT(INOUT) :: itdat
    !* preconditioner to apply
    CLASS(precon_real), INTENT(INOUT) :: precon
    !* solve type (e.g. cg, gmres)
    CHARACTER(*), INTENT(IN) :: solve_type
    !* residual error (independently computed)
    REAL(r8), INTENT(OUT) :: err_check

    TYPE(rvec_storage) :: rsln(par%nbl),rres(par%nbl)
    INTEGER(i4) :: ibl
    REAL(r8) :: err0,er_0v,er_sl
    CHARACTER(512) :: msg
!-------------------------------------------------------------------------------
!   create space for the solution and set the guess to 1, initialize the
!   the matrix to the identity, and set the RHS to the -1.
!   (non-uniform values can not be used with seaming)
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL rrhs(ibl)%v%alloc_with_mold(rsln(ibl)%v)
      CALL rrhs(ibl)%v%alloc_with_mold(rres(ibl)%v)
      CALL rsln(ibl)%v%assign_for_testing('constant',1._r8)
      CALL rrhs(ibl)%v%assign_for_testing('constant',-1._r8)
      CALL rmat(ibl)%m%set_identity
    ENDDO
    CALL precon%update(rmat,seam)
!-------------------------------------------------------------------------------
!   get err0 for reference
!-------------------------------------------------------------------------------
    er_0v=norm(rrhs,seam,'inf',1._r8)
    CALL resid_norm(rmat,rsln,rrhs,rres,er_sl,seam,'inf',1._r8)
    err0=MAX(er_sl,er_0v,SQRT(TINY(err0)))
!-------------------------------------------------------------------------------
!   solve the system
!-------------------------------------------------------------------------------
    SELECT CASE(TRIM(solve_type))
    CASE ('cg')
      CALL iter_cg_real_dir_solve(rmat,precon,rrhs,rsln,seam,itdat)
    CASE DEFAULT
      CALL par%nim_stop('test_solve_real::solve type not recognized')
    END SELECT
    WRITE(msg,*) 'Solved system to err=',itdat%err,' in ',itdat%its,            &
                 ' iterations using seed=',itdat%seed
    CALL par%nim_write(msg)
!-------------------------------------------------------------------------------
!   check solution
!-------------------------------------------------------------------------------
    CALL resid_norm(rmat,rsln,rrhs,rres,err_check,seam,'inf',err0)
    DO ibl=1,par%nbl
      CALL rsln(ibl)%v%dealloc
      CALL rres(ibl)%v%dealloc
    ENDDO
    CALL precon%dealloc
  END SUBROUTINE test_solve_real

!-------------------------------------------------------------------------------
!* test a single-mode complex iterative solve of I x = b for x.
!-------------------------------------------------------------------------------
  SUBROUTINE test_solve_cv1m(crhs,cmat,seam,itdat,precon,solve_type,err_check)
    USE iterdata_mod
    USE linalg_utils_mod, ONLY: norm,resid_norm
    USE iter_all_mod
    USE preconditioner_mod
    IMPLICIT NONE

    !* test cv1m
    TYPE(cv1m_storage), INTENT(INOUT) :: crhs(par%nbl)
    !* test cmat
    TYPE(cmat_storage), INTENT(INOUT) :: cmat(par%nbl)
    !* test seam
    TYPE(seam_type), INTENT(INOUT) :: seam
    !* test iteration data
    TYPE(iterdata), INTENT(INOUT) :: itdat
    !* preconditioner to apply
    CLASS(precon_cv1m), INTENT(INOUT) :: precon
    !* solve type (e.g. cg, gmres)
    CHARACTER(*), INTENT(IN) :: solve_type
    !* residual error (independently computed)
    REAL(r8), INTENT(OUT) :: err_check

    TYPE(cv1m_storage) :: csln(par%nbl),cres(par%nbl)
    INTEGER(i4) :: ibl
    REAL(r8) :: err0,er_0v,er_sl
    CHARACTER(512) :: msg
!-------------------------------------------------------------------------------
!   create space for the solution and set the guess to 1, initialize the
!   the matrix to the identity, and set the RHS to the -1.
!   (non-uniform values can not be used with seaming)
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL crhs(ibl)%v%alloc_with_mold(csln(ibl)%v)
      CALL crhs(ibl)%v%alloc_with_mold(cres(ibl)%v)
      CALL csln(ibl)%v%assign_for_testing('constant',CMPLX(1._r8,1._r8,r8))
      CALL crhs(ibl)%v%assign_for_testing('constant',CMPLX(-1._r8,-1._r8,r8))
      CALL cmat(ibl)%m%set_identity
    ENDDO
    CALL precon%update(cmat,seam)
!-------------------------------------------------------------------------------
!   get err0 for reference
!-------------------------------------------------------------------------------
    er_0v=norm(crhs,seam,'inf',1._r8)
    CALL resid_norm(cmat,csln,crhs,cres,er_sl,seam,'inf',1._r8)
    err0=MAX(er_sl,er_0v,SQRT(TINY(err0)))
!-------------------------------------------------------------------------------
!   solve the system
!-------------------------------------------------------------------------------
    SELECT CASE(TRIM(solve_type))
    CASE ('cg')
      CALL iter_cg_cv1m_dir_solve(cmat,precon,crhs,csln,seam,itdat)
    CASE DEFAULT
      CALL par%nim_stop('test_solve_cv1m::solve type not recognized')
    END SELECT
    WRITE(msg,*) 'Solved system to err=',itdat%err,' in ',itdat%its,            &
                 ' iterations using seed=',itdat%seed
    CALL par%nim_write(msg)
!-------------------------------------------------------------------------------
!   check solution
!-------------------------------------------------------------------------------
    CALL resid_norm(cmat,csln,crhs,cres,err_check,seam,'inf',err0)
    DO ibl=1,par%nbl
      CALL csln(ibl)%v%dealloc
      CALL cres(ibl)%v%dealloc
    ENDDO
    CALL precon%dealloc
  END SUBROUTINE test_solve_cv1m

!-------------------------------------------------------------------------------
!* test a complex multi-mode iterative solve of I x = b for x.
!-------------------------------------------------------------------------------
  SUBROUTINE test_solve_comp(crhs,cmat,seam,itdat,precon,solve_type,err_check)
    USE iterdata_mod
    USE linalg_utils_mod, ONLY: norm,resid_norm
    USE iter_all_mod
    USE preconditioner_mod
    IMPLICIT NONE

    !* test cvec
    TYPE(cvec_storage), INTENT(INOUT) :: crhs(par%nbl)
    !* test cmat
    TYPE(cmat_storage), INTENT(INOUT) :: cmat(par%nbl,par%nmodes)
    !* test seam
    TYPE(seam_type), INTENT(INOUT) :: seam
    !* test iteration data
    TYPE(iterdata), INTENT(INOUT) :: itdat
    !* preconditioner to apply
    CLASS(precon_comp), INTENT(INOUT) :: precon
    !* solve type (e.g. cg, gmres)
    CHARACTER(*), INTENT(IN) :: solve_type
    !* residual error (independently computed)
    REAL(r8), INTENT(OUT) :: err_check

    TYPE(cvec_storage) :: csln(par%nbl),cres(par%nbl)
    INTEGER(i4) :: ibl,imode
    REAL(r8) :: err0,er_0v,er_sl
    CHARACTER(512) :: msg
!-------------------------------------------------------------------------------
!   create space for the solution and set the guess to 1, initialize the
!   the matrix to the identity, and set the RHS to the -1.
!   (non-uniform values can not be used with seaming)
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL crhs(ibl)%v%alloc_with_mold(csln(ibl)%v)
      CALL crhs(ibl)%v%alloc_with_mold(cres(ibl)%v)
      CALL csln(ibl)%v%assign_for_testing('constant',CMPLX(1._r8,1._r8,r8))
      CALL crhs(ibl)%v%assign_for_testing('constant',CMPLX(-1._r8,-1._r8,r8))
      DO imode=1,par%nmodes
        CALL cmat(ibl,imode)%m%set_identity
      ENDDO
    ENDDO
    CALL precon%update(cmat,seam)
!-------------------------------------------------------------------------------
!   get err0 for reference
!-------------------------------------------------------------------------------
    er_0v=norm(crhs,seam,'inf',1._r8)
    CALL resid_norm(cmat,csln,crhs,cres,er_sl,seam,'inf',1._r8)
    err0=MAX(er_sl,er_0v,SQRT(TINY(err0)))
!-------------------------------------------------------------------------------
!   solve the system
!-------------------------------------------------------------------------------
    SELECT CASE(TRIM(solve_type))
    CASE ('cg')
      CALL iter_cg_comp_dir_solve(cmat,precon,crhs,csln,seam,itdat)
    CASE DEFAULT
      CALL par%nim_stop('test_solve_comp::solve type not recognized')
    END SELECT
    WRITE(msg,*) 'Solved system to err=',itdat%err,' in ',itdat%its,            &
                 ' iterations using seed=',itdat%seed
    CALL par%nim_write(msg)
!-------------------------------------------------------------------------------
!   check solution
!-------------------------------------------------------------------------------
    CALL resid_norm(cmat,csln,crhs,cres,err_check,seam,'inf',err0)
    DO ibl=1,par%nbl
      CALL csln(ibl)%v%dealloc
      CALL cres(ibl)%v%dealloc
    ENDDO
    CALL precon%dealloc
  END SUBROUTINE test_solve_comp

!-------------------------------------------------------------------------------
!* test the cg routine without a preconditioner by solving I x = b for x.
!-------------------------------------------------------------------------------
  SUBROUTINE test_no_precon(rrhs,crhs1m,crhs,rmat,cmat,seam,solve_type)
    USE iterdata_mod
    USE nimtest_utils
    USE precon_all_mod
    IMPLICIT NONE

    !* test rvec
    TYPE(rvec_storage), INTENT(INOUT) :: rrhs(par%nbl)
    !* test cv1m
    TYPE(cv1m_storage), INTENT(INOUT) :: crhs1m(par%nbl)
    !* test cvec
    TYPE(cvec_storage), INTENT(INOUT) :: crhs(par%nbl)
    !* test rmat
    TYPE(rmat_storage), INTENT(INOUT) :: rmat(par%nbl)
    !* test cmat
    TYPE(cmat_storage), INTENT(INOUT) :: cmat(par%nbl,par%nmodes)
    !* test seam
    TYPE(seam_type), INTENT(INOUT) :: seam
    !* solve type (e.g. cg, gmres)
    CHARACTER(*), INTENT(IN) :: solve_type

    TYPE(precon_none_real) :: precon_real
    TYPE(precon_none_cv1m) :: precon_cv1m
    TYPE(precon_none_comp) :: precon_comp
    INTEGER(i4), PARAMETER :: maxit=100
    REAL(r8), PARAMETER :: tol=1.e-6
    REAL(r8) :: err_check
    TYPE(iterdata) :: itdat
!-------------------------------------------------------------------------------
!   set iterdata input
!-------------------------------------------------------------------------------
    itdat%maxit=maxit
    itdat%tol=tol
!-------------------------------------------------------------------------------
!   call the real test
!-------------------------------------------------------------------------------
    CALL precon_real%alloc
    CALL test_solve_real(rrhs,rmat,seam,itdat,precon_real,solve_type,err_check)
    CALL wrequal(err_check,itdat%err,solve_type//'_no_precon real',itdat%tol)
!-------------------------------------------------------------------------------
!   call the complex single-mode test
!-------------------------------------------------------------------------------
    CALL precon_cv1m%alloc
    CALL test_solve_cv1m(crhs1m,cmat(:,1_i4),seam,itdat,precon_cv1m,            &
                         solve_type,err_check)
    CALL wrequal(err_check,itdat%err,solve_type//'_no_precon cv1m',itdat%tol)
!-------------------------------------------------------------------------------
!   call the complex multiple-mode test
!-------------------------------------------------------------------------------
    CALL precon_comp%alloc
    CALL test_solve_comp(crhs,cmat,seam,itdat,precon_comp,solve_type,err_check)
    CALL wrequal(err_check,itdat%err,solve_type//'_no_precon comp',itdat%tol)
  END SUBROUTINE test_no_precon

!-------------------------------------------------------------------------------
!* test the cg routine with a Jacobi preconditioner by solving I x = b for x.
!-------------------------------------------------------------------------------
  SUBROUTINE test_cg_jacobi(rrhs,crhs1m,crhs,rmat,cmat,seam)
    USE iterdata_mod
    USE nimtest_utils
    USE precon_all_mod
    IMPLICIT NONE

    !* test rvec
    TYPE(rvec_storage), INTENT(INOUT) :: rrhs(par%nbl)
    !* test cv1m
    TYPE(cv1m_storage), INTENT(INOUT) :: crhs1m(par%nbl)
    !* test cvec
    TYPE(cvec_storage), INTENT(INOUT) :: crhs(par%nbl)
    !* test rmat
    TYPE(rmat_storage), INTENT(INOUT) :: rmat(par%nbl)
    !* test cmat
    TYPE(cmat_storage), INTENT(INOUT) :: cmat(par%nbl,par%nmodes)
    !* test seam
    TYPE(seam_type), INTENT(INOUT) :: seam

    TYPE(precon_jacobi_real) :: precon
    INTEGER(i4), PARAMETER :: maxit=100
    REAL(r8), PARAMETER :: tol=1.e-6
    REAL(r8) :: err_check
    TYPE(iterdata) :: itdat
!-------------------------------------------------------------------------------
!   set iterdata input
!-------------------------------------------------------------------------------
    itdat%maxit=maxit
    itdat%tol=tol
!-------------------------------------------------------------------------------
!   call the real test
!-------------------------------------------------------------------------------
    CALL precon%alloc(rrhs)
    CALL test_solve_real(rrhs,rmat,seam,itdat,precon,'cg',err_check)
    CALL wrequal(err_check,itdat%err,'cg_jacobi real',itdat%tol)
!-------------------------------------------------------------------------------
!   TODO: complex version
!-------------------------------------------------------------------------------
  END SUBROUTINE test_cg_jacobi

END PROGRAM test_iter
