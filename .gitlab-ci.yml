stages:
  - basic
  - build-first-pass
  - build-serial
  - build-mpi/acc
  - deploy

variables:
  GIT_SUBMODULE_STRATEGY: recursive

ford-wiki:
  stage: basic
  image: registry.gitlab.com/nimrodteam/open/nimrod-aai:trixie-base
  script:
    - git submodule update --remote # Synchronize with the wiki
    - cd doc # Build docs in place
    - ./testDocs.sh
  artifacts:
    expose_as: 'artifacts'
    paths:
      - public/
 
linkchecker:
  stage: basic
  image: registry.gitlab.com/nimrodteam/open/nimrod-aai:trixie-base
  script:
    - git submodule update --remote # Synchronize with the wiki
    - cd doc # Build docs in place
    - ./testDocs.sh
    - cd ..
    - linkchecker --check-extern public/index.html # check for broken links
  when: manual

format:
  stage: basic
  image: registry.gitlab.com/nimrodteam/open/nimrod-aai:trixie-base
  script:
    - ./nimtests/check_format.sh
    - ./nimtests/check_acc.py
    - ./nimtests/check_io.py
    - ./codegen/updateNamelists.py # Will modify source if needed
    - git update-index --really-refresh # Check if repo is clean
    - git diff-index --quiet HEAD

coverage:
  stage: build-first-pass
  image: registry.gitlab.com/nimrodteam/open/nimrod-aai:trixie-openmpi
  script:
    - mkdir build-cov && cd build-cov
    - cmake -DENABLE_MPI:BOOL=TRUE -DTRAP_FP_EXCEPTIONS:BOOL=TRUE -DENABLE_CODE_COVERAGE:BOOL=TRUE -DTIME_LEVEL1:BOOL=TRUE -DTIME_LEVEL2:BOOL=TRUE -DOBJ_MEM_PROF:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING="Debug" ..
    - make -j4 2> >(tee make.error)
    - make ctest_coverage 
    - lcov_cobertura ctest_coverage.info -b ../
    - fastcov_summary ctest_coverage.json
    - nwarnings=`cat make.error | grep -o 'Warning:' | wc -l` && echo "Warnings = $nwarnings"
    - anybadge -l 'gcc warnings' -v `cat make.error | grep -o 'Warning:' | wc -l` -f gccwarn.svg 1=green 4=yellow 10=orange 25=red
  coverage: '/line\s*: \d+\.\d+/'
  artifacts:
    expose_as: 'artifacts'
    paths:
      - build-cov/make.error
      - build-cov/ctest_coverage/  
      - build-cov/gccwarn.svg
    expire_in: 2 week
    reports:
      coverage_report:
        coverage_format: cobertura
        path: build-cov/coverage.xml

gcc-shared:
  stage: build-first-pass
  image: registry.gitlab.com/nimrodteam/open/nimrod-aai:trixie-base
  script:
    - mkdir build-opt && cd build-opt
    - cmake -DTRAP_FP_EXCEPTIONS:BOOL=TRUE -DENABLE_SHARED:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING="RelWithDebInfo" ..
    - make -j4
    - ctest -j 4 -D ExperimentalTest --force-new-ctest-process --output-on-failure
    - ctest -j 4 -D ExperimentalMemCheck --force-new-ctest-process --output-on-failure -E timer
    - make install
    - ls install/*
  artifacts:
    when: on_failure
    paths:
      - build-opt/Testing/Temporary/
    expire_in: 1 week

intel:
  stage: build-serial
  image: registry.gitlab.com/nimrodteam/open/nimrod-aai:intel-slim
  script:
    - mkdir build-opt && cd build-opt
    - cmake -DTRAP_FP_EXCEPTIONS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING="RelWithDebInfo" ..
    - make -j4
    - ctest -j 4 -D ExperimentalTest --force-new-ctest-process --output-on-failure
    - ctest -j 4 -D ExperimentalMemCheck --force-new-ctest-process --output-on-failure -E timer\|h5io
  artifacts:
    when: on_failure
    paths:
      - build-opt/Testing/Temporary/
    expire_in: 1 week

nvhpc:
  stage: build-serial
  image: registry.gitlab.com/nimrodteam/open/nimrod-aai:nvhpc-slim
  script:
    - mkdir build-opt && cd build-opt
    - cmake -DTRAP_FP_EXCEPTIONS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING="RelWithDebInfo" ..
    - make -j4
    - ctest -j 4 -D ExperimentalTest --force-new-ctest-process --output-on-failure
  artifacts:
    when: on_failure
    paths:
      - build-opt/Testing/Temporary/
    expire_in: 1 week

# nvhpc memcheck takes too long to run every time
nvhpc-memcheck:
  stage: build-serial
  image: registry.gitlab.com/nimrodteam/open/nimrod-aai:nvhpc-slim
  script:
    - mkdir build-opt && cd build-opt
    - cmake -DTRAP_FP_EXCEPTIONS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING="RelWithDebInfo" ..
    - make -j4
    - ctest -j 4 -D ExperimentalMemCheck --force-new-ctest-process --output-on-failure -E timer
  when: manual
  artifacts:
    when: on_failure
    paths:
      - build-opt/Testing/Temporary/
    expire_in: 1 week

# requires libnvidia-container-tools nvidia-docker2
# and gpus parameter in /etc/gitlab-runner/config.toml
nvhpc-acc:
  stage: build-mpi/acc
  image: registry.gitlab.com/nimrodteam/open/nimrod-aai:nvhpc-slim
  tags:
    - nvidia-gpu
  script:
    - nvidia-smi
    - mkdir build-opt && cd build-opt
    - cmake -DENABLE_OPENACC:BOOL=TRUE -DENABLE_OPENACC_AUTOCOMPARE:BOOL=FALSE -DTRAP_FP_EXCEPTIONS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING="RelWithDebInfo" ..
    - make -j4
    - ctest -j 4 -D ExperimentalTest --force-new-ctest-process --output-on-failure
    # Exclude as racecheck reductions fail; described in issue #108
    # https://gitlab.com/NIMRODteam/nimrod-abstract/-/issues/108
    #- ctest -j 1 -D ExperimentalMemCheck --force-new-ctest-process --output-on-failure -E timer
  artifacts:
    when: on_failure
    paths:
      - build-opt/Testing/Temporary/
    expire_in: 1 week

nvhpc-acc-autocompare:
  stage: build-mpi/acc
  image: registry.gitlab.com/nimrodteam/open/nimrod-aai:nvhpc-slim
  tags:
    - nvidia-gpu
  script:
    - nvidia-smi
    - mkdir build-opt && cd build-opt
    - cmake -DENABLE_OPENACC:BOOL=TRUE -DENABLE_OPENACC_AUTOCOMPARE:BOOL=TRUE -DTRAP_FP_EXCEPTIONS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING="RelWithDebInfo" ..
    - make -j4
     # Disable test_utils_rect_2D_acc_dot, see issue #136.
    - PCAST_COMPARE=rel=10 ctest -j 4 -D ExperimentalTest --force-new-ctest-process --output-on-failure -E laplace\|test_utils_rect_2D_acc_dot
    # Use an absolute tolerance for the laplace tests
    - PCAST_COMPARE=abs=8 ctest -j 4 -D ExperimentalTest --force-new-ctest-process --output-on-failure -R laplace
  artifacts:
    when: on_failure
    paths:
      - build-opt/Testing/Temporary/
    expire_in: 1 week

gcc-openmpi:
  stage: build-mpi/acc
  image: registry.gitlab.com/nimrodteam/open/nimrod-aai:trixie-openmpi
  script:
    - mpirun --version
    - mkdir build-opt && cd build-opt
    - cmake -DENABLE_MPI:BOOL=TRUE -DTRAP_FP_EXCEPTIONS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING="RelWithDebInfo" ..
    - make -j4
    - ctest -j 4 -D ExperimentalTest --force-new-ctest-process --output-on-failure
    #- ctest -j 4 -D ExperimentalMemCheck --force-new-ctest-process --output-on-failure -E timer

gcc-mpich:
  stage: build-mpi/acc
  image: registry.gitlab.com/nimrodteam/open/nimrod-aai:trixie-mpich
  tags:
    - nvidia-gpu # restrict to ionize, not enough memory in /dev/shm on JM_NwDm6
  script:
    - mpirun --version
    - mkdir build-opt && cd build-opt
    - cmake -DENABLE_MPI:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING="RelWithDebInfo" ..
    - make -j4
    - ctest -j 4 -D ExperimentalTest --force-new-ctest-process --output-on-failure
    #- ctest -j 4 -D ExperimentalMemCheck --force-new-ctest-process --output-on-failure -E timer

intel-mpi:
  stage: build-mpi/acc
  image: registry.gitlab.com/nimrodteam/open/nimrod-aai:intel-slim
  tags:
    - nvidia-gpu # restrict to ionize, bus error on JM_NwDm6
  script:
    - mpirun --version
    - mkdir build-opt && cd build-opt
    - cmake -DENABLE_MPI:BOOL=TRUE -DTRAP_FP_EXCEPTIONS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING="RelWithDebInfo" ..
    - make -j4
    - ctest -j 2 -D ExperimentalTest --force-new-ctest-process --output-on-failure
    #- ctest -j 1 -D ExperimentalMemCheck --force-new-ctest-process --output-on-failure -E timer

pages:
  stage: deploy
  image: registry.gitlab.com/nimrodteam/open/nimrod-aai:trixie-base
  script:
    - git submodule update --remote # Synchronize with the wiki
    - cd doc && ./testDocs.sh && cd .. # generate docs in doc folder
    - cp -r build-cov/ctest_coverage public/coverage
    - cp build-cov/gccwarn.svg public/gccwarn.svg
  dependencies:
    - coverage
  artifacts:
    paths:
      - public
  only:
    - main
