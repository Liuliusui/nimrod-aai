!-------------------------------------------------------------------------------
!! Register fem configuration for tests downstream
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!* Register fem configuration for tests downstream
!-------------------------------------------------------------------------------
MODULE fem_registry_mod
  USE local
  USE pardata_mod
  IMPLICIT NONE

  INTEGER(i4) :: ntests=-1
  LOGICAL :: comp_test=.FALSE.
  CHARACTER(64) :: field_test_types(5)=                                         &
    ["h1_rect_2D_comp                 ",                                        &
     "h1_rect_2D_comp_acc             ",                                        &
     "h1_rect_2D_real                 ",                                        &
     "h1_rect_2D_real_acc             ",                                        &
     "hcurl_rect_2D_real              "]
  INTEGER(i4), PRIVATE :: nq_in=1,mx_in=1,my_in=1,pd_in=1,nm_in=1
  LOGICAL, PRIVATE :: use_in_vars=.FALSE.
#ifdef HAVE_OPENACC
  LOGICAL :: on_gpu=.TRUE.
#else
  LOGICAL :: on_gpu=.FALSE.
#endif
CONTAINS

!-------------------------------------------------------------------------------
!* set internal variables for test_type
!-------------------------------------------------------------------------------
  SUBROUTINE set_field_test_type(test_type)
    IMPLICIT NONE

    !* field identification string
    CHARACTER(*), INTENT(IN) :: test_type

    SELECT CASE(test_type)
    CASE ("h1_rect_2D_comp")
      ntests=7
      comp_test=.TRUE.
    CASE ("h1_rect_2D_comp_acc")
      ntests=7
      comp_test=.TRUE.
    CASE ("h1_rect_2D_real")
      ntests=9
    CASE ("h1_rect_2D_real_acc")
      ntests=9
    CASE ("hcurl_rect_2D_real")
      ntests=9
    CASE DEFAULT
      CALL par%nim_stop('setup_fem:: invalid test_type')
    END SELECT
  END SUBROUTINE set_field_test_type

!-------------------------------------------------------------------------------
!* output valid test type strings for print_usage
!-------------------------------------------------------------------------------
  SUBROUTINE print_types
    IMPLICIT NONE

    INTEGER(i4) :: itest

    CALL par%nim_write('  where <test type> is one of')
    DO itest=1,SIZE(field_test_types)
      CALL par%nim_write('    '//TRIM(field_test_types(itest)))
    ENDDO
  END SUBROUTINE print_types

!-------------------------------------------------------------------------------
!* set input tests types to be used in setup_fem instead of defaults
!-------------------------------------------------------------------------------
  SUBROUTINE set_fem_input_vars(nqty,mx,my,poly_degree,nmodes)
    IMPLICIT NONE

    !* input nqty
    INTEGER(i4), INTENT(IN) :: nqty
    !* input mx
    INTEGER(i4), INTENT(IN) :: mx
    !* input my
    INTEGER(i4), INTENT(IN) :: my
    !* input poly_degree
    INTEGER(i4), INTENT(IN) :: poly_degree
    !* input nmodes
    INTEGER(i4), INTENT(IN) :: nmodes

    nq_in=nqty
    mx_in=mx
    my_in=my
    pd_in=poly_degree
    nm_in=nmodes
    use_in_vars=.TRUE.
  END SUBROUTINE set_fem_input_vars

!-------------------------------------------------------------------------------
!> routine to set up finite element structures for testing, par%init should be
!  called first.
!-------------------------------------------------------------------------------
  SUBROUTINE setup_fem(test_type,cfield,rfield,coord_field,itest)
    USE nodal_mod
    IMPLICIT NONE

    !* field identification string
    CHARACTER(*), INTENT(IN) :: test_type
    !* test complex fem field
    CLASS(nodal_field_comp), ALLOCATABLE, INTENT(INOUT) :: cfield
    !* test real fem field
    CLASS(nodal_field_real), ALLOCATABLE, INTENT(INOUT) :: rfield
    !* test fem coordinate field
    CLASS(nodal_field_real), ALLOCATABLE, INTENT(INOUT) :: coord_field
    !* test id to set up
    INTEGER(i4), INTENT(IN) :: itest

    SELECT CASE(test_type)
    CASE ("h1_rect_2D_comp")
      CALL setup_h1_rect_2D_comp
    CASE ("h1_rect_2D_comp_acc")
      CALL setup_h1_rect_2D_comp_acc
    CASE ("h1_rect_2D_real")
      CALL setup_h1_rect_2D_real
    CASE ("h1_rect_2D_real_acc")
      CALL setup_h1_rect_2D_real_acc
    CASE ("hcurl_rect_2D_real")
      CALL setup_hcurl_rect_2D_real
    CASE ("coord_field_2D")
      CALL setup_coord_field_2D
    CASE DEFAULT
      CALL par%nim_stop('setup_fem:: invalid test_type')
    END SELECT
  CONTAINS

!-------------------------------------------------------------------------------
!*  set up h1_rect_2D_comp structures for testing
!-------------------------------------------------------------------------------
    SUBROUTINE setup_h1_rect_2D_comp
      USE h1_rect_2D_mod
      IMPLICIT NONE

      INTEGER(i4), DIMENSION(ntests) :: mx,my,nqty,nfour,poly_degree
      CHARACTER(64) :: fieldname,basename,indexname
!-------------------------------------------------------------------------------
!     test parameters
!-------------------------------------------------------------------------------
      IF (use_in_vars) THEN
        mx=mx_in
        my=my_in
        nqty=nq_in
        nfour=nm_in
        poly_degree=pd_in
      ELSE
        mx=[9,8,7,6,5,4,3]
        my=[12,11,10,9,8,7,6]
        nqty=[2,1,4,3,3,1,1]
        nfour=[1,6,1,3,2,2,2]
        poly_degree=[2,3,4,5,6,7,1]
      ENDIF
      basename='h1_rect_2D_comp'
!-------------------------------------------------------------------------------
!     setup test
!-------------------------------------------------------------------------------
      WRITE(indexname,'(I4)') itest
      fieldname=TRIM(basename)//"_"//TRIM(ADJUSTL(indexname))
      ALLOCATE(h1_rect_2D_comp::cfield)
      SELECT TYPE (cfield)
      TYPE IS (h1_rect_2D_comp)
        CALL cfield%alloc(mx(itest),my(itest),nqty(itest),nfour(itest),         &
                          poly_degree(itest),itest,fieldname)
      END SELECT
      IF (.NOT.ALLOCATED(coord_field))                                          &
        CALL setup_coord_field_2D(mx,my,poly_degree)
    END SUBROUTINE setup_h1_rect_2D_comp

!-------------------------------------------------------------------------------
!*  set up h1_rect_2D_comp_acc structures for testing
!-------------------------------------------------------------------------------
    SUBROUTINE setup_h1_rect_2D_comp_acc
      USE h1_rect_2D_mod
      IMPLICIT NONE

      INTEGER(i4), DIMENSION(ntests) :: mx,my,nqty,nfour,poly_degree
      CHARACTER(64) :: fieldname,basename,indexname
!-------------------------------------------------------------------------------
!     test parameters
!-------------------------------------------------------------------------------
      IF (use_in_vars) THEN
        mx=mx_in
        my=my_in
        nqty=nq_in
        nfour=nm_in
        poly_degree=pd_in
      ELSE
        mx=[9,8,7,6,5,4,3]
        my=[12,11,10,9,8,7,6]
        nqty=[2,1,4,3,3,1,1]
        nfour=[1,6,1,3,2,2,2]
        poly_degree=[2,3,4,5,6,7,1]
      ENDIF
      basename='h1_rect_2D_comp_acc'
!-------------------------------------------------------------------------------
!     setup test
!-------------------------------------------------------------------------------
      WRITE(indexname,'(I4)') itest
      fieldname=TRIM(basename)//"_"//TRIM(ADJUSTL(indexname))
      ALLOCATE(h1_rect_2D_comp_acc::cfield)
      SELECT TYPE (cfield)
      TYPE IS (h1_rect_2D_comp_acc)
        CALL cfield%alloc(mx(itest),my(itest),nqty(itest),nfour(itest),         &
                          poly_degree(itest),itest,on_gpu,fieldname)
      END SELECT
      IF (.NOT.ALLOCATED(coord_field))                                          &
        CALL setup_coord_field_2D(mx,my,poly_degree)
    END SUBROUTINE setup_h1_rect_2D_comp_acc

!-------------------------------------------------------------------------------
!*  set up h1_rect_2D_real structures for testing
!-------------------------------------------------------------------------------
    SUBROUTINE setup_h1_rect_2D_real
      USE h1_rect_2D_mod
      IMPLICIT NONE

      INTEGER(i4), DIMENSION(ntests) :: mx,my,nqty,poly_degree
      CHARACTER(64) :: fieldname,basename,indexname
!-------------------------------------------------------------------------------
!     test parameters
!-------------------------------------------------------------------------------
      IF (use_in_vars) THEN
        mx=mx_in
        my=my_in
        nqty=nq_in
        poly_degree=pd_in
      ELSE
        mx=[1,2,3,1,2,3,1,2,3]
        my=[1,1,1,2,2,2,3,3,3]
        nqty=[3,2,1,2,1,3,1,3,2]
        poly_degree=[9,8,7,6,5,4,3,2,1]
      ENDIF
      basename='h1_rect_2D_real'
!-------------------------------------------------------------------------------
!     setup test
!-------------------------------------------------------------------------------
      WRITE(indexname,'(I4)') itest
      fieldname=TRIM(basename)//"_"//TRIM(ADJUSTL(indexname))
      ALLOCATE(h1_rect_2D_real::rfield)
      SELECT TYPE (rfield)
      TYPE IS (h1_rect_2D_real)
        CALL rfield%alloc(mx(itest),my(itest),nqty(itest),                      &
                          poly_degree(itest),itest,fieldname)
      END SELECT
      IF (.NOT.ALLOCATED(coord_field))                                          &
        CALL setup_coord_field_2D(mx,my,poly_degree)
    END SUBROUTINE setup_h1_rect_2D_real

!-------------------------------------------------------------------------------
!*  set up h1_rect_2D_real_acc structures for testing
!-------------------------------------------------------------------------------
    SUBROUTINE setup_h1_rect_2D_real_acc
      USE h1_rect_2D_mod
      IMPLICIT NONE

      INTEGER(i4), DIMENSION(ntests) :: mx,my,nqty,poly_degree
      CHARACTER(64) :: fieldname,basename,indexname
!-------------------------------------------------------------------------------
!     test parameters
!-------------------------------------------------------------------------------
      IF (use_in_vars) THEN
        mx=mx_in
        my=my_in
        nqty=nq_in
        poly_degree=pd_in
      ELSE
        mx=[1,2,3,1,2,3,1,2,3]
        my=[1,1,1,2,2,2,3,3,3]
        nqty=[3,2,1,2,1,3,1,3,2]
        poly_degree=[9,8,7,6,5,4,3,2,1]
      ENDIF
      basename='h1_rect_2D_real_acc'
!-------------------------------------------------------------------------------
!     setup test
!-------------------------------------------------------------------------------
      WRITE(indexname,'(I4)') itest
      fieldname=TRIM(basename)//"_"//TRIM(ADJUSTL(indexname))
      ALLOCATE(h1_rect_2D_real_acc::rfield)
      SELECT TYPE (rfield)
      TYPE IS (h1_rect_2D_real_acc)
        CALL rfield%alloc(mx(itest),my(itest),nqty(itest),                      &
                          poly_degree(itest),itest,on_gpu,fieldname)
      END SELECT
      IF (.NOT.ALLOCATED(coord_field))                                          &
        CALL setup_coord_field_2D(mx,my,poly_degree)
    END SUBROUTINE setup_h1_rect_2D_real_acc

!-------------------------------------------------------------------------------
!*  set up hcurl_rect_2D_real structures for testing
!-------------------------------------------------------------------------------
    SUBROUTINE setup_hcurl_rect_2D_real
      USE hcurl_rect_2D_real_mod
      IMPLICIT NONE

      INTEGER(i4), DIMENSION(ntests) :: mx,my,nqty,poly_degree
      CHARACTER(64) :: fieldname,basename,indexname
!-------------------------------------------------------------------------------
!     test parameters
!-------------------------------------------------------------------------------
      IF (use_in_vars) THEN
        mx=mx_in
        my=my_in
        nqty=nq_in
        poly_degree=pd_in
      ELSE
        mx=[1,2,3,1,2,3,1,2,3]
        my=[1,1,1,2,2,2,3,3,3]
        nqty=[3,2,1,2,1,3,1,3,2]
        poly_degree=[9,8,7,6,5,4,3,2,1]
      ENDIF
      basename='hcurl_rect_2D_real'
!-------------------------------------------------------------------------------
!     setup test
!-------------------------------------------------------------------------------
      WRITE(indexname,'(I4)') itest
      fieldname=TRIM(basename)//"_"//TRIM(ADJUSTL(indexname))
      ALLOCATE(hcurl_rect_2D_real::rfield)
      SELECT TYPE (rfield)
      TYPE IS (hcurl_rect_2D_real)
        CALL rfield%alloc(mx(itest),my(itest),nqty(itest),                      &
                          poly_degree(itest),itest,fieldname)
      END SELECT
      IF (.NOT.ALLOCATED(coord_field))                                          &
        CALL setup_coord_field_2D(mx,my,poly_degree)
    END SUBROUTINE setup_hcurl_rect_2D_real

!-------------------------------------------------------------------------------
!*  set up setup_coord_field_2D structures for testing
!-------------------------------------------------------------------------------
    SUBROUTINE setup_coord_field_2D(mx_oin,my_oin,pd_oin)
      USE simple_mesh_mod, ONLY: simple_rect_2D_mesh
      IMPLICIT NONE

      INTEGER(i4), DIMENSION(ntests), OPTIONAL, INTENT(IN) :: mx_oin
      INTEGER(i4), DIMENSION(ntests), OPTIONAL, INTENT(IN) :: my_oin
      INTEGER(i4), DIMENSION(ntests), OPTIONAL, INTENT(IN) :: pd_oin

      INTEGER(i4), DIMENSION(ntests) :: mx,my,poly_degree
!-------------------------------------------------------------------------------
!     test parameters
!-------------------------------------------------------------------------------
      IF (use_in_vars) THEN
        mx=mx_in
        my=my_in
        poly_degree=pd_in
      ELSE
        IF (PRESENT(mx_oin)) mx=mx_oin
        IF (PRESENT(my_oin)) my=my_oin
        IF (PRESENT(pd_oin)) poly_degree=pd_oin
      ENDIF
!-------------------------------------------------------------------------------
!     allocate and set coord_field
!-------------------------------------------------------------------------------
      ALLOCATE(simple_rect_2D_mesh::coord_field)
      SELECT TYPE (coord_field)
      TYPE IS (simple_rect_2D_mesh)
        CALL coord_field%alloc(mx(itest),my(itest),2,poly_degree(itest),        &
                               itest,'mesh')
        CALL coord_field%setup_rect
      END SELECT
    END SUBROUTINE setup_coord_field_2D

  END SUBROUTINE setup_fem

!-------------------------------------------------------------------------------
!* routine to tear down finite element structures for testing
!-------------------------------------------------------------------------------
  SUBROUTINE teardown_fem(cfield,rfield,coord_field)
    USE nodal_mod
    IMPLICIT NONE

    !* test complex fem field
    CLASS(nodal_field_comp), ALLOCATABLE, INTENT(INOUT) :: cfield
    !* test real fem field
    CLASS(nodal_field_real), ALLOCATABLE, INTENT(INOUT) :: rfield
    !* test fem coordinate field
    CLASS(nodal_field_real), ALLOCATABLE, INTENT(INOUT) :: coord_field

    IF (ALLOCATED(cfield)) THEN
      CALL cfield%dealloc
      DEALLOCATE(cfield)
    ENDIF
    IF (ALLOCATED(rfield)) THEN
      CALL rfield%dealloc
      DEALLOCATE(rfield)
    ENDIF
    CALL coord_field%dealloc
    DEALLOCATE(coord_field)
  END SUBROUTINE teardown_fem

END MODULE fem_registry_mod
