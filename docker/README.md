Docker files for the project are collected here.

Basic commands:

```
$ docker image pull registry.gitlab.com/nimrodteam/open/nimrod-aai:<image name>

$ docker build -t registry.gitlab.com/nimrodteam/open/nimrod-aai:<image name> .  

$ docker push registry.gitlab.com/nimrodteam/open/nimrod-aai:<image name>

$ docker image ls --all

$ docker run -it --rm -v $(pwd):/source -w /source <image name> 
```

Add `--gpus '"device=1"'` to the run command to enable a selected GPU with nvidia-docker2.

A sample daemon.json file is available for the docker configuration.

Some packages require dependencies which can be downloaded as
```
$ wget https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.14/hdf5-1.14.2/src/CMake-hdf5-1.14.2.tar.gz
```

Further information on the [wiki](https://gitlab.com/NIMRODteam/open/nimrod-aai/-/wikis/testing/docker).
