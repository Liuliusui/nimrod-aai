!-------------------------------------------------------------------------------
!! seam test driver
!-------------------------------------------------------------------------------
PROGRAM test_seams
  USE io
  USE local
  USE seam_mod
  USE simple_seam_mod, ONLY: seam_rect_alloc
  USE pardata_mod
  USE timer_mod
  IMPLICIT NONE

  TYPE(seam_type) :: seam
  INTEGER(i4) :: nargs,it
  CHARACTER(64) :: test_type
  INTEGER(i4), PARAMETER :: ntests=7
  INTEGER(i4), PARAMETER :: nq=3_i4,ns=2_i4,nm=1_i4
  INTEGER(i4), DIMENSION(ntests) :: mxbl,mybl,nxbl,nybl
  INTEGER(i4), ALLOCATABLE :: mxbl_arr(:),mybl_arr(:)
  CHARACTER(5) :: periodicity(ntests)
  LOGICAL :: r0left(ntests)
  INTEGER(i4) :: idepth
  INTEGER(i4), SAVE :: iftn=-1

  mxbl=[1,2,2,4,5,5,7]
  mybl=[1,3,2,4,8,4,6]
  nxbl=[1,1,2,1,2,4,4]
  nybl=[1,1,1,2,2,2,5]
  periodicity=['none ','both ','y-dir','none ','both ','y-dir','none ']
  r0left=[.true.,.false.,.true.,.false.,.false.,.true.,.true.]
!-------------------------------------------------------------------------------
! determine which test to run from the command line
!-------------------------------------------------------------------------------
  nargs=command_argument_count()
  IF (nargs /= 1) THEN
    CALL print_usage
    CALL par%nim_stop('Argument error')
  ENDIF
  CALL get_command_argument(1,test_type)
  CALL par%init
  CALL fch5_init
  CALL timer%init
  CALL timer%start_timer_l0('test','test_seam',iftn,idepth)
!-------------------------------------------------------------------------------
! loop over test parameters
!-------------------------------------------------------------------------------
  DO it=1,ntests
    CALL par%set_decomp(nm,nxbl(it)*nybl(it)*par%nprocs)
!-------------------------------------------------------------------------------
!   setup a simple rect seam
!-------------------------------------------------------------------------------
    ALLOCATE(mxbl_arr(nxbl(it)),mybl_arr(nybl(it)*par%nprocs))
    mxbl_arr=mxbl(it)
    mybl_arr=mybl(it)
    CALL seam_rect_alloc(seam,nxbl(it),nybl(it)*par%nprocs,mxbl_arr,mybl_arr,   &
                         TRIM(periodicity(it)),r0left(it))
    DEALLOCATE(mxbl_arr,mybl_arr)
!-------------------------------------------------------------------------------
!   call different test cases.
!-------------------------------------------------------------------------------
    SELECT CASE(TRIM(test_type))
    CASE ("alloc_dealloc")
      ! do nothing
    CASE ("init")
      IF (par%nprocs>1) CALL seam%trim_to_local
      CALL seam%init(nq,nq,ns,1_i4)
    CASE ("edge_network")
      IF (par%nprocs>1) CALL seam%trim_to_local
      CALL seam%init(nq,nq,ns,1_i4)
      CALL test_edge_network
    CASE ("h5io")
      IF (par%nprocs>1) CALL seam%trim_to_local
      CALL test_h5io
    CASE DEFAULT
      CALL par%nim_write('No test named '//TRIM(test_type))
      CALL print_usage
    END SELECT
!-------------------------------------------------------------------------------
!   deallocate
!-------------------------------------------------------------------------------
    CALL seam%dealloc
    CALL par%dealloc
  ENDDO
  CALL timer%end_timer_l0(iftn,idepth)
  CALL timer%report
  CALL timer%finalize
  CALL fch5_dealloc
  CALL par%nim_stop('Normal termination.',clean_shutdown=.TRUE.)
CONTAINS

!-------------------------------------------------------------------------------
!* helper routine to print usage
!-------------------------------------------------------------------------------
  SUBROUTINE print_usage
    IMPLICIT NONE

    CALL par%nim_write('Usage: <test program> <test name>')
    CALL par%nim_write('  where <test name> is one of')
    CALL par%nim_write('    alloc_dealloc')
    CALL par%nim_write('    init')
    CALL par%nim_write('    edge_network')
    CALL par%nim_write('    h5io')
    CALL par%nim_stop('check input')
  END SUBROUTINE print_usage

!-------------------------------------------------------------------------------
!* helper routine to test seam edge_network
!-------------------------------------------------------------------------------
  SUBROUTINE test_edge_network
    USE nimtest_utils
    IMPLICIT NONE

    INTEGER(i4) :: ibl,iv,ivp

!-------------------------------------------------------------------------------
!   check edge_network for reals
!-------------------------------------------------------------------------------
    DO ibl=1,UBOUND(seam%s,1)
      DO iv=1,seam%s(ibl)%nvert
        seam%s(ibl)%vertex(iv)%seam_in=1._r8
        seam%s(ibl)%segment(iv)%seam_in=1._r8
      ENDDO
      seam%s(ibl)%nqty_loaded=nq
      seam%s(ibl)%nside_loaded=ns
      seam%s(ibl)%nmodes_loaded=0_i4
    ENDDO
    CALL seam%edge_network
!-------------------------------------------------------------------------------
!   vertices should be nimage+1 and segments 2 unless edge boundary (then 1)
!-------------------------------------------------------------------------------
    DO ibl=1,UBOUND(seam%s,1)
      DO iv=1,seam%s(ibl)%nvert
        seam%s(ibl)%vertex(iv)%seam_in=REAL(seam%s(ibl)%vertex(iv)%nimage+1,r8)
        CALL wrequal(seam%s(ibl)%vertex(iv)%seam_out(1:nq),                     &
                     seam%s(ibl)%vertex(iv)%seam_in(1:nq),                      &
                     "edge_network vertex real")
        ivp=iv-1
        IF (ivp==0) ivp=seam%s(ibl)%nvert
        IF ((seam%s(ibl)%expoint(iv).OR.seam%s(ibl)%r0point(iv)).AND.           &
          (seam%s(ibl)%expoint(ivp).OR.seam%s(ibl)%r0point(ivp))) THEN
          seam%s(ibl)%segment(iv)%seam_in=1._r8
        ELSE
          seam%s(ibl)%segment(iv)%seam_in=2._r8
        ENDIF
        CALL wrequal(seam%s(ibl)%segment(iv)%seam_out(1:nq*ns),                 &
                     seam%s(ibl)%segment(iv)%seam_in(1:nq*ns),                  &
                     "edge_network segment real")
      ENDDO
    ENDDO
!-------------------------------------------------------------------------------
!   check edge_network for complex types
!-------------------------------------------------------------------------------
    DO ibl=1,UBOUND(seam%s,1)
      DO iv=1,seam%s(ibl)%nvert
        seam%s(ibl)%vertex(iv)%seam_cin=CMPLX(1._r8,2._r8,r8)
        seam%s(ibl)%segment(iv)%seam_cin=CMPLX(3._r8,4._r8,r8)
      ENDDO
      seam%s(ibl)%nqty_loaded=nq
      seam%s(ibl)%nside_loaded=ns
      seam%s(ibl)%nmodes_loaded=nm
    ENDDO
    CALL seam%edge_network
!-------------------------------------------------------------------------------
!   vertices should be nimage+1 and segments 2 unless edge boundary (then 1)
!-------------------------------------------------------------------------------
    DO ibl=1,UBOUND(seam%s,1)
      DO iv=1,seam%s(ibl)%nvert
        seam%s(ibl)%vertex(iv)%seam_cin=                                        &
          CMPLX(1._r8,2._r8,r8)*REAL(seam%s(ibl)%vertex(iv)%nimage+1,r8)
        CALL wrequal(seam%s(ibl)%vertex(iv)%seam_cout(1:nq),                    &
                     seam%s(ibl)%vertex(iv)%seam_cin(1:nq),                     &
                     "edge_network vertex real")
        ivp=iv-1
        IF (ivp==0) ivp=seam%s(ibl)%nvert
        IF ((seam%s(ibl)%expoint(iv).OR.seam%s(ibl)%r0point(iv)).AND.           &
          (seam%s(ibl)%expoint(ivp).OR.seam%s(ibl)%r0point(ivp))) THEN
          seam%s(ibl)%segment(iv)%seam_cin=CMPLX(3._r8,4._r8,r8)
        ELSE
          seam%s(ibl)%segment(iv)%seam_cin=2._r8*CMPLX(3._r8,4._r8,r8)
        ENDIF
        CALL wrequal(seam%s(ibl)%segment(iv)%seam_cout(1:nq*ns),                &
                     seam%s(ibl)%segment(iv)%seam_cin(1:nq*ns),                 &
                     "edge_network segment complex")
      ENDDO
    ENDDO
  END SUBROUTINE test_edge_network

!-------------------------------------------------------------------------------
!* helper routine to test seam I/O
!-------------------------------------------------------------------------------
  SUBROUTINE test_h5io
    USE nimtest_utils
    IMPLICIT NONE

    TYPE(seam_type) :: seam_read
    INTEGER(i4) :: ibl,nbl,iv
!-------------------------------------------------------------------------------
!   only node 0 writes
!-------------------------------------------------------------------------------
    IF (par%node>0) RETURN
    nbl=nxbl(it)*nybl(it)
!-------------------------------------------------------------------------------
!   write to file
!-------------------------------------------------------------------------------
    CALL open_newh5file('seam_test.h5',fileid,"NIMROD test seam file",rootgid,  &
                        h5in,h5err)
    CALL seam%h5_dump(rootgid)
    CALL close_h5file(fileid,rootgid,h5err)
!-------------------------------------------------------------------------------
!   read from file
!-------------------------------------------------------------------------------
    CALL open_oldh5file('seam_test.h5',fileid,rootgid,h5in,h5err)
    CALL seam_read%h5_read(rootgid)
    CALL close_h5file(fileid,rootgid,h5err)
!-------------------------------------------------------------------------------
!   compare seam and seam_read
!-------------------------------------------------------------------------------
    DO ibl=0,par%nbl
      CALL wrequal(seam%s(ibl)%nvert,seam_read%s(ibl)%nvert,"s%nvert")
      DO iv=1,seam%s(ibl)%nvert
        CALL wrequal(seam%s(ibl)%vertex(iv)%ptr,                                &
                     seam_read%s(ibl)%vertex(iv)%ptr,"s%vertex%ptr")
        IF (ibl>0) THEN
          CALL wrequal(seam%s(ibl)%vertex(iv)%intxy,                            &
                       seam_read%s(ibl)%vertex(iv)%intxy,"s%vertex(iv)%intxy")
        ENDIF
      ENDDO
    ENDDO
    IF (seam%s(0)%nvert>0) THEN
      CALL wrequal(seam%s(0)%excorner,seam_read%s(0)%excorner,"s(0)%excorner")
      CALL wrequal(seam%s(0)%r0point,seam_read%s(0)%r0point,"s(0)%r0point")
    ENDIF
    CALL wrequal(seam%glob_nvert,seam_read%glob_nvert,"glob_nvert")
!-------------------------------------------------------------------------------
!   dealloc seam_read
!-------------------------------------------------------------------------------
    CALL seam_read%dealloc
  END SUBROUTINE test_h5io

END PROGRAM test_seams
