!-------------------------------------------------------------------------------
!! data structures for performing block and mode decomposition communication
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
! define DEBUG_MPI to check MPI error codes
!-------------------------------------------------------------------------------
#ifdef DEBUG
#define DEBUG_MPI
#endif
!-------------------------------------------------------------------------------
!* data structures for performing block and mode decomposition communication
!-------------------------------------------------------------------------------
MODULE pardata_mod
  USE local
#ifdef HAVE_MPI
  USE mpi_f08
#endif
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: par

!-------------------------------------------------------------------------------
! expose some members of mpi_f08 for non-blocking communication calls
!-------------------------------------------------------------------------------
  PUBLIC :: mpi_status,mpi_request

!-------------------------------------------------------------------------------
! define dummy types and variables without MPI
!-------------------------------------------------------------------------------
#ifndef HAVE_MPI
  PUBLIC :: mpi_comm,mpi_op,mpi_datatype,mpi_integer,mpi_double_precision

  PUBLIC :: mpi_sum,mpi_max,mpi_any_source

  TYPE :: mpi_comm
  END TYPE

  TYPE :: mpi_op
  END TYPE

  TYPE :: mpi_datatype
  END TYPE

  TYPE :: mpi_status
    INTEGER(i4) :: mpi_source
  END TYPE

  TYPE :: mpi_request
  END TYPE

  TYPE(mpi_datatype) :: mpi_integer
  TYPE(mpi_datatype) :: mpi_double_precision
  TYPE(mpi_op) :: mpi_sum
  TYPE(mpi_op) :: mpi_max
  INTEGER(i4), PARAMETER :: mpi_any_source=0_i4
#endif
!-------------------------------------------------------------------------------
!> Type that defines a container for data need to perform block and mode
!  decomposition communication. Most bound routines use assumed-size arguments
!  to take advantage of sequence association so that we can accept arrays of
!  any dimensionality.
!-------------------------------------------------------------------------------
  TYPE :: pardata
!-------------------------------------------------------------------------------
!   Application-global
!-------------------------------------------------------------------------------
    !* total # of processors (assigned to blocks and layers)
    INTEGER(i4) :: nprocs=1_i4
    !* processor id of me (0 to nprocs-1)
    INTEGER(i4) :: node=0_i4
    !* communicator for nimrod. typically is mpi_comm_world.
    TYPE(mpi_comm) :: comm_nimrod
!-------------------------------------------------------------------------------
!   Layer decomposition - collection of blocks sharing mode #s
!-------------------------------------------------------------------------------
    !* # of procs assigned to each layer
    INTEGER(i4) :: nprocs_layer=1_i4
    !* I am this proc within the layer (0 to nprocs_layer-1)
    INTEGER(i4) :: node_layer=0_i4
    !* communicator within a layer, across blocks
    TYPE(mpi_comm) :: comm_layer
!-------------------------------------------------------------------------------
!   Mode (Fourier) decomposition - collection of mode #s sharing blocks
!-------------------------------------------------------------------------------
    !* which layer this proc belong to (0 to nlayers-1)
    INTEGER(i4) :: ilayer=0_i4
    !* total # of layers
    INTEGER(i4) :: nlayers=1_i4
    !* within a block, across modes (layers)
    TYPE(mpi_comm) :: comm_mode
    !* total # of Fourier modes
    INTEGER(i4) :: nmodes_total
    !* # of Fourier modes owned by this process
    INTEGER(i4) :: nmodes
    !* keff of all Fourier modes
    REAL(r8), DIMENSION(:), ALLOCATABLE :: keff_total
    !* keff of modes owned by the process
    REAL(r8), DIMENSION(:), ALLOCATABLE :: keff
    !* starting global mode index offset (for linear cases)
    !* mode index of modes owned by the process (0 to nmodes_total-1)
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: nindex
    !* my 1st mode corresponds to this global mode (1 to nmodes_total)
    INTEGER(i4) :: mode_lo
    !* my last mode corresponds to this global mode (1 to nmodes_total)
    INTEGER(i4) :: mode_hi
    !* layer2proc(0:nlayers-1) = proc ids of all layers for the local grid blocks.
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: layer2proc
    !* maps each global mode (1 to nmodes_total) to a unique layer
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: mode2layer
    !* total # of blocks
    INTEGER(i4) :: nbl_total
    !* # of blocks owned by this process
    INTEGER(i4) :: nbl
    !* proc id of owner of global block i (1:nbl_total) (within my layer)
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: block2proc
    !* local index (1:nbl) of global block i on proc who owns it (within my layer)
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: global2local
    !* global index (1:nbl_total) of local block i (1:nbl)
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: loc2glob
    !> The distribution of blocks on parallel machines is controlled by
    !  `decompflag` (0 = clumped, 1 = strided).
    INTEGER(i4) :: decompflag=0
!-------------------------------------------------------------------------------
!   Expose aspects of the mpi_f08 module
!-------------------------------------------------------------------------------
    INTEGER(i4) :: mpi_any_source=mpi_any_source
!-------------------------------------------------------------------------------
!   Set to true when init is called
!-------------------------------------------------------------------------------
    LOGICAL :: mpi_init_called=.FALSE.
!-------------------------------------------------------------------------------
!   Determines if streams launched by get_streams are forks
!-------------------------------------------------------------------------------
    LOGICAL :: single_stream=.TRUE.
  CONTAINS

    PROCEDURE, PASS(prd) :: init
    PROCEDURE, PASS(prd) :: nim_write
    PROCEDURE, PASS(prd) :: nim_stop
    PROCEDURE, PASS(prd) :: set_decomp
    PROCEDURE, PASS(prd) :: dealloc
    PROCEDURE, PASS(prd) :: get_stream
    PROCEDURE, PASS(prd) :: wait_streams
    PROCEDURE, PASS(prd), PRIVATE :: check_error
!-------------------------------------------------------------------------------
!   mpi_barrier
!-------------------------------------------------------------------------------
    PROCEDURE, PASS(prd) :: all_barrier
    PROCEDURE, PASS(prd) :: layer_barrier
    PROCEDURE, PASS(prd) :: mode_barrier
!-------------------------------------------------------------------------------
!   mpi_bcast
!-------------------------------------------------------------------------------
    PROCEDURE, PASS(prd), PRIVATE :: all_bcast_int
    PROCEDURE, PASS(prd), PRIVATE :: all_bcast_real
    PROCEDURE, PASS(prd), PRIVATE :: all_bcast_comp
    PROCEDURE, PASS(prd), PRIVATE :: all_bcast_logical
    PROCEDURE, PASS(prd), PRIVATE :: all_bcast_char
    PROCEDURE, PASS(prd), PRIVATE :: all_bcast_int_sc
    PROCEDURE, PASS(prd), PRIVATE :: all_bcast_real_sc
    PROCEDURE, PASS(prd), PRIVATE :: all_bcast_comp_sc
    PROCEDURE, PASS(prd), PRIVATE :: all_bcast_logical_sc
    GENERIC :: all_bcast => all_bcast_int,all_bcast_real,all_bcast_comp,        &
                            all_bcast_logical,all_bcast_char,all_bcast_int_sc,  &
                            all_bcast_real_sc,all_bcast_comp_sc,                &
                            all_bcast_logical_sc
    PROCEDURE, PASS(prd), PRIVATE :: layer_bcast_int
    PROCEDURE, PASS(prd), PRIVATE :: layer_bcast_real
    PROCEDURE, PASS(prd), PRIVATE :: layer_bcast_comp
    PROCEDURE, PASS(prd), PRIVATE :: layer_bcast_logical
    PROCEDURE, PASS(prd), PRIVATE :: layer_bcast_char
    PROCEDURE, PASS(prd), PRIVATE :: layer_bcast_int_sc
    PROCEDURE, PASS(prd), PRIVATE :: layer_bcast_real_sc
    PROCEDURE, PASS(prd), PRIVATE :: layer_bcast_comp_sc
    PROCEDURE, PASS(prd), PRIVATE :: layer_bcast_logical_sc
    GENERIC :: layer_bcast => layer_bcast_int,layer_bcast_real,layer_bcast_comp,&
                              layer_bcast_logical,layer_bcast_char,             &
                              layer_bcast_int_sc,layer_bcast_real_sc,           &
                              layer_bcast_comp_sc,layer_bcast_logical_sc
    PROCEDURE, PASS(prd), PRIVATE :: mode_bcast_int
    PROCEDURE, PASS(prd), PRIVATE :: mode_bcast_real
    PROCEDURE, PASS(prd), PRIVATE :: mode_bcast_comp
    PROCEDURE, PASS(prd), PRIVATE :: mode_bcast_logical
    PROCEDURE, PASS(prd), PRIVATE :: mode_bcast_char
    PROCEDURE, PASS(prd), PRIVATE :: mode_bcast_int_sc
    PROCEDURE, PASS(prd), PRIVATE :: mode_bcast_real_sc
    PROCEDURE, PASS(prd), PRIVATE :: mode_bcast_comp_sc
    PROCEDURE, PASS(prd), PRIVATE :: mode_bcast_logical_sc
    GENERIC :: mode_bcast => mode_bcast_int,mode_bcast_real,mode_bcast_comp,    &
                             mode_bcast_logical,mode_bcast_char,                &
                             mode_bcast_int_sc,mode_bcast_real_sc,              &
                             mode_bcast_comp_sc,mode_bcast_logical_sc
!-------------------------------------------------------------------------------
!   mpi_allreduce
!-------------------------------------------------------------------------------
    PROCEDURE, PASS(prd), PRIVATE :: all_sum_int
    PROCEDURE, PASS(prd), PRIVATE :: all_max_int
    PROCEDURE, PASS(prd), PRIVATE :: all_sum_real
    PROCEDURE, PASS(prd), PRIVATE :: all_max_real
    PROCEDURE, PASS(prd), PRIVATE :: all_sum_comp
    PROCEDURE, PASS(prd), PRIVATE :: all_max_comp
    PROCEDURE, PASS(prd), PRIVATE :: all_sum_int_sc
    PROCEDURE, PASS(prd), PRIVATE :: all_max_int_sc
    PROCEDURE, PASS(prd), PRIVATE :: all_sum_real_sc
    PROCEDURE, PASS(prd), PRIVATE :: all_max_real_sc
    PROCEDURE, PASS(prd), PRIVATE :: all_sum_comp_sc
    PROCEDURE, PASS(prd), PRIVATE :: all_max_comp_sc
    GENERIC :: all_sum => all_sum_int,all_sum_real,all_sum_comp,                &
                          all_sum_int_sc,all_sum_real_sc,all_sum_comp_sc
    GENERIC :: all_max => all_max_int,all_max_real,all_max_comp,                &
                          all_max_int_sc,all_max_real_sc,all_max_comp_sc
    PROCEDURE, PASS(prd), PRIVATE :: layer_sum_int
    PROCEDURE, PASS(prd), PRIVATE :: layer_max_int
    PROCEDURE, PASS(prd), PRIVATE :: layer_sum_real
    PROCEDURE, PASS(prd), PRIVATE :: layer_max_real
    PROCEDURE, PASS(prd), PRIVATE :: layer_sum_comp
    PROCEDURE, PASS(prd), PRIVATE :: layer_max_comp
    PROCEDURE, PASS(prd), PRIVATE :: layer_sum_int_sc
    PROCEDURE, PASS(prd), PRIVATE :: layer_max_int_sc
    PROCEDURE, PASS(prd), PRIVATE :: layer_sum_real_sc
    PROCEDURE, PASS(prd), PRIVATE :: layer_max_real_sc
    PROCEDURE, PASS(prd), PRIVATE :: layer_sum_comp_sc
    PROCEDURE, PASS(prd), PRIVATE :: layer_max_comp_sc
    GENERIC :: layer_sum => layer_sum_int,layer_sum_real,layer_sum_comp,        &
                            layer_sum_int_sc,layer_sum_real_sc,layer_sum_comp_sc
    GENERIC :: layer_max => layer_max_int,layer_max_real,layer_max_comp,        &
                            layer_max_int_sc,layer_max_real_sc,layer_max_comp_sc
    PROCEDURE, PASS(prd), PRIVATE :: mode_sum_int
    PROCEDURE, PASS(prd), PRIVATE :: mode_max_int
    PROCEDURE, PASS(prd), PRIVATE :: mode_sum_real
    PROCEDURE, PASS(prd), PRIVATE :: mode_max_real
    PROCEDURE, PASS(prd), PRIVATE :: mode_sum_comp
    PROCEDURE, PASS(prd), PRIVATE :: mode_max_comp
    PROCEDURE, PASS(prd), PRIVATE :: mode_sum_int_sc
    PROCEDURE, PASS(prd), PRIVATE :: mode_max_int_sc
    PROCEDURE, PASS(prd), PRIVATE :: mode_sum_real_sc
    PROCEDURE, PASS(prd), PRIVATE :: mode_max_real_sc
    PROCEDURE, PASS(prd), PRIVATE :: mode_sum_comp_sc
    PROCEDURE, PASS(prd), PRIVATE :: mode_max_comp_sc
    GENERIC :: mode_sum => mode_sum_int,mode_sum_real,mode_sum_comp,            &
                           mode_sum_int_sc,mode_sum_real_sc,mode_sum_comp_sc
    GENERIC :: mode_max => mode_max_int,mode_max_real,mode_max_comp,            &
                           mode_max_int_sc,mode_max_real_sc,mode_max_comp_sc
!-------------------------------------------------------------------------------
!   mpi_send
!-------------------------------------------------------------------------------
    PROCEDURE, PASS(prd), PRIVATE :: all_send_int
    PROCEDURE, PASS(prd), PRIVATE :: all_send_real
    PROCEDURE, PASS(prd), PRIVATE :: all_send_comp
    PROCEDURE, PASS(prd), PRIVATE :: all_send_int_sc
    PROCEDURE, PASS(prd), PRIVATE :: all_send_real_sc
    PROCEDURE, PASS(prd), PRIVATE :: all_send_comp_sc
    GENERIC :: all_send => all_send_int,all_send_real,all_send_comp,            &
                           all_send_int_sc,all_send_real_sc,all_send_comp_sc
    PROCEDURE, PASS(prd), PRIVATE :: mode_send_int
    PROCEDURE, PASS(prd), PRIVATE :: mode_send_real
#if defined(__gfortran) || defined(__ifort)
    PROCEDURE, PASS(prd) :: mode_send_comp
#else
    PROCEDURE, PASS(prd), PRIVATE :: mode_send_comp
#endif
    PROCEDURE, PASS(prd), PRIVATE :: mode_send_int_sc
    PROCEDURE, PASS(prd), PRIVATE :: mode_send_real_sc
    PROCEDURE, PASS(prd), PRIVATE :: mode_send_comp_sc
    GENERIC :: mode_send => mode_send_int,mode_send_real,mode_send_comp,        &
                            mode_send_int_sc,mode_send_real_sc,mode_send_comp_sc
    PROCEDURE, PASS(prd), PRIVATE :: layer_send_int
    PROCEDURE, PASS(prd), PRIVATE :: layer_send_real
    PROCEDURE, PASS(prd), PRIVATE :: layer_send_comp
    PROCEDURE, PASS(prd), PRIVATE :: layer_send_int_sc
    PROCEDURE, PASS(prd), PRIVATE :: layer_send_real_sc
    PROCEDURE, PASS(prd), PRIVATE :: layer_send_comp_sc
    GENERIC :: layer_send => layer_send_int,layer_send_real,layer_send_comp,    &
                             layer_send_int_sc,layer_send_real_sc,              &
                             layer_send_comp_sc
!-------------------------------------------------------------------------------
!   mpi_recv
!-------------------------------------------------------------------------------
    PROCEDURE, PASS(prd), PRIVATE :: all_recv_int
    PROCEDURE, PASS(prd), PRIVATE :: all_recv_real
    PROCEDURE, PASS(prd), PRIVATE :: all_recv_comp
    PROCEDURE, PASS(prd), PRIVATE :: all_recv_int_sc
    PROCEDURE, PASS(prd), PRIVATE :: all_recv_real_sc
    PROCEDURE, PASS(prd), PRIVATE :: all_recv_comp_sc
    GENERIC :: all_recv => all_recv_int,all_recv_real,all_recv_comp,            &
                           all_recv_int_sc,all_recv_real_sc,all_recv_comp_sc
    PROCEDURE, PASS(prd), PRIVATE :: mode_recv_int
    PROCEDURE, PASS(prd), PRIVATE :: mode_recv_real
#if defined(__gfortran) || defined(__ifort)
    PROCEDURE, PASS(prd) :: mode_recv_comp
#else
    PROCEDURE, PASS(prd), PRIVATE :: mode_recv_comp
#endif
    PROCEDURE, PASS(prd), PRIVATE :: mode_recv_int_sc
    PROCEDURE, PASS(prd), PRIVATE :: mode_recv_real_sc
    PROCEDURE, PASS(prd), PRIVATE :: mode_recv_comp_sc
    GENERIC :: mode_recv => mode_recv_int,mode_recv_real,mode_recv_comp,        &
                            mode_recv_int_sc,mode_recv_real_sc,mode_recv_comp_sc
    PROCEDURE, PASS(prd), PRIVATE :: layer_recv_int
    PROCEDURE, PASS(prd), PRIVATE :: layer_recv_real
    PROCEDURE, PASS(prd), PRIVATE :: layer_recv_comp
    PROCEDURE, PASS(prd), PRIVATE :: layer_recv_int_sc
    PROCEDURE, PASS(prd), PRIVATE :: layer_recv_real_sc
    PROCEDURE, PASS(prd), PRIVATE :: layer_recv_comp_sc
    GENERIC :: layer_recv => layer_recv_int,layer_recv_real,layer_recv_comp,    &
                             layer_recv_int_sc,layer_recv_real_sc,              &
                             layer_recv_comp_sc
!-------------------------------------------------------------------------------
!   mpi_irecv
!-------------------------------------------------------------------------------
    PROCEDURE, PASS(prd), PRIVATE :: all_irecv_int
    PROCEDURE, PASS(prd), PRIVATE :: all_irecv_real
    PROCEDURE, PASS(prd), PRIVATE :: all_irecv_comp
    PROCEDURE, PASS(prd), PRIVATE :: all_irecv_int_sc
    PROCEDURE, PASS(prd), PRIVATE :: all_irecv_real_sc
    PROCEDURE, PASS(prd), PRIVATE :: all_irecv_comp_sc
    GENERIC :: all_irecv => all_irecv_int,all_irecv_real,all_irecv_comp,        &
                            all_irecv_int_sc,all_irecv_real_sc,all_irecv_comp_sc
!-------------------------------------------------------------------------------
!   mpi_wait
!-------------------------------------------------------------------------------
    PROCEDURE, PASS(prd) :: wait
!-------------------------------------------------------------------------------
!   mpi_waitall
!-------------------------------------------------------------------------------
    PROCEDURE, PASS(prd) :: waitany
!-------------------------------------------------------------------------------
!   mpi_waitany
!-------------------------------------------------------------------------------
    PROCEDURE, PASS(prd) :: waitall
!-------------------------------------------------------------------------------
!   mpi_gather
!-------------------------------------------------------------------------------
    PROCEDURE, PASS(prd), PRIVATE :: all_gather_int
    PROCEDURE, PASS(prd), PRIVATE :: all_gather_real
    PROCEDURE, PASS(prd), PRIVATE :: all_gather_char
    GENERIC :: all_gather => all_gather_int,all_gather_real,all_gather_char
!-------------------------------------------------------------------------------
!   mpi_reduce_scatter
!-------------------------------------------------------------------------------
    PROCEDURE, PASS(prd), PRIVATE :: all_sum_scatter_int
    GENERIC :: all_sum_scatter => all_sum_scatter_int
  END TYPE pardata

  !* parallel communication singleton type
  TYPE(pardata) :: par
CONTAINS

!-------------------------------------------------------------------------------
!* initialization for parallel computations
!-------------------------------------------------------------------------------
  SUBROUTINE init(prd)
#ifdef HAVE_ACC_BLAS
    USE acc_blas_mod
#endif
    USE io
    IMPLICIT NONE

    !* pardata type to initialize
    CLASS(pardata), INTENT(INOUT) :: prd

#ifdef HAVE_MPI
    INTEGER(i4) :: tlvl,ierror
#endif
#ifdef HAVE_MPI
!-------------------------------------------------------------------------------
!   initialize mpi. use the lowest needed thread level
!-------------------------------------------------------------------------------
    CALL mpi_init_thread(mpi_thread_serialized,tlvl,ierror)
    IF (tlvl < mpi_thread_serialized)                                           &
      CALL prd%nim_stop("Insufficient MPI thread level")
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_init_thread','comm_nimrod',prd%node)
#endif
!-------------------------------------------------------------------------------
!   set comm_nimrod to mpi_comm_world. This is a potentially useful abstraction
!   to build in but isn't used now.
!-------------------------------------------------------------------------------
    prd%comm_nimrod=mpi_comm_world
    CALL mpi_comm_rank(prd%comm_nimrod,prd%node,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_comm_rank','comm_nimrod',prd%node)
#endif
    CALL mpi_comm_size(prd%comm_nimrod,prd%nprocs,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_comm_size','comm_nimrod',prd%node)
#endif
#endif
#ifdef HAVE_ACC_BLAS
    CALL acc_blas%init
#endif
    prd%mpi_init_called=.TRUE.
!-------------------------------------------------------------------------------
!   open the output file if node zero
!-------------------------------------------------------------------------------
    IF (prd%node==0) THEN
      IF (ofname/="unset") THEN
        OPEN(UNIT=out_unit,FILE=TRIM(ofname),STATUS='UNKNOWN',POSITION='APPEND')
        ofopen=.TRUE.
      ENDIF
    ENDIF
  END SUBROUTINE init

!-------------------------------------------------------------------------------
!* write to stdout and to file from node 0
!-------------------------------------------------------------------------------
  SUBROUTINE nim_write(prd,msg,force_flush,any_node)
    USE io, ONLY: nim_wr,out_unit,ofopen
    IMPLICIT NONE

    !* pardata type
    CLASS(pardata), INTENT(IN) :: prd
    !* message to write
    CHARACTER(*), INTENT(IN) :: msg
    !* if true, flush the buffer
    LOGICAL, OPTIONAL, INTENT(IN) :: force_flush
    !* if true, any node can write to nim_wr
    LOGICAL, OPTIONAL, INTENT(IN) :: any_node

    LOGICAL :: any_node_write

    IF (PRESENT(any_node)) THEN
      any_node_write=any_node
    ELSE
      any_node_write=.FALSE.
    ENDIF
!-------------------------------------------------------------------------------
!   only node zero writes unless any_node_write=T
!-------------------------------------------------------------------------------
    IF (prd%node==0) THEN
      WRITE(nim_wr,*) TRIM(msg)
      IF (ofopen) WRITE(out_unit,*) TRIM(msg)
#ifdef DEBUG
      FLUSH(nim_wr)
      IF (ofopen) FLUSH(out_unit)
#else
      IF (PRESENT(force_flush)) THEN
        IF (force_flush) THEN
          FLUSH(nim_wr)
          IF (ofopen) FLUSH(out_unit)
        ENDIF
      ENDIF
#endif
    ELSEIF (any_node_write) THEN
      WRITE(nim_wr,*) TRIM(msg)
#ifdef DEBUG
      FLUSH(nim_wr)
#else
      IF (PRESENT(force_flush)) THEN
        IF (force_flush) THEN
          FLUSH(nim_wr)
        ENDIF
      ENDIF
#endif
    ENDIF
  END SUBROUTINE nim_write

!-------------------------------------------------------------------------------
!> simple routine to end the program with mpi_abort or mpi_finalize
!-------------------------------------------------------------------------------
  SUBROUTINE nim_stop(prd,message,clean_shutdown)
#ifdef HAVE_ACC_BLAS
    USE acc_blas_mod
#endif
    USE local
    USE io
    IMPLICIT NONE

    !* pardata type
    CLASS(pardata), INTENT(INOUT) :: prd
    !* final output message
    CHARACTER(*), INTENT(IN) :: message
    !* if true, call mpi_finalize instead of abort for clean shutdown
    !  (default is false)
    LOGICAL, INTENT(IN), OPTIONAL :: clean_shutdown

    LOGICAL :: no_abort
#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
#endif

    no_abort=.FALSE.
    IF (PRESENT(clean_shutdown)) no_abort=clean_shutdown
!-------------------------------------------------------------------------------
!   write completion message and close file
!-------------------------------------------------------------------------------
    IF (no_abort) THEN
      CALL prd%nim_write('NIM_STOP => '//TRIM(message))
    ELSE
      CALL prd%nim_write('NIM_STOP (ERROR) => '//TRIM(message))
    ENDIF
    IF (prd%node==0) THEN
      CLOSE(UNIT=out_unit)
    ENDIF
#ifdef HAVE_MPI
    IF (prd%mpi_init_called) THEN
      IF (no_abort) THEN
#ifdef HAVE_ACC_BLAS
        CALL acc_blas%destroy
#endif
        CALL prd%dealloc
        CALL mpi_finalize(ierror)
        STOP
      ELSE
        CALL mpi_abort(prd%comm_nimrod,prd%node,ierror)
      ENDIF
    ELSE
      STOP
    ENDIF
#else
#ifdef HAVE_ACC_BLAS
    CALL acc_blas%destroy
#endif
    CALL prd%dealloc
    STOP
#endif
  END SUBROUTINE nim_stop

!-------------------------------------------------------------------------------
!> set variables that determine parallel decomposition. this is done after the
!  init call as some variables must be read from file and globally communicated
!
!  If the namelist parallel_input parameters should be used they should be read
!  and set before this call.
!-------------------------------------------------------------------------------
  SUBROUTINE set_decomp(prd,nmodes_total,nbl_total)
    IMPLICIT NONE

    !* pardata type to initialize
    CLASS(pardata), INTENT(INOUT) :: prd
    !* total number of modes for this computation
    INTEGER(i4), INTENT(IN) :: nmodes_total
    !* total number of blocks for this computation
    INTEGER(i4), INTENT(IN) :: nbl_total

    INTEGER(i4) :: ib,imode
#ifdef HAVE_MPI
    INTEGER(i4) :: ibl,remainder,blocklo,blockhi,ierror,itmp,jtmp
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: workvec
    CHARACTER(512) :: msg
    CHARACTER(128), PARAMETER :: fmt1="( i3,' processor layers have ',i3,"//    &
                                      "' Fourier comps, and ',i3,' have ',i3)"
    CHARACTER(128), PARAMETER ::                                                &
                      fmt2="( i3,' layers have ',i3,' Fourier comps.')"
    CHARACTER(128), PARAMETER ::                                                &
                      fmt3="(' Number of procs (',i4,')',' > nlayers (',i4,')',"&
                           //"' * number of grid blocks (',i4,').')"
#endif
!-------------------------------------------------------------------------------
!   transfer input to internal variables
!-------------------------------------------------------------------------------
    prd%nmodes_total=nmodes_total
    prd%nbl_total=nbl_total
#ifdef HAVE_MPI
!-------------------------------------------------------------------------------
!   check input before dividing by prd%nlayers
!-------------------------------------------------------------------------------
    IF (prd%nlayers <= 0) CALL prd%nim_stop('nlayers must be > 0.')
!-------------------------------------------------------------------------------
!   check that prd%nlayers divides into nprocs evenly
!-------------------------------------------------------------------------------
    IF (MOD(prd%nprocs,prd%nlayers)/=0)                                         &
      CALL prd%nim_stop('Number of procs is not divisible by nlayers.')
!-------------------------------------------------------------------------------
!   check for too many processors
!-------------------------------------------------------------------------------
    IF (prd%nprocs>prd%nlayers*prd%nbl_total) THEN
      WRITE(msg,fmt=fmt3) prd%nprocs,prd%nlayers,prd%nbl_total
      CALL prd%nim_stop(msg)
    ENDIF
!-------------------------------------------------------------------------------
!   check for too many layers
!-------------------------------------------------------------------------------
    IF (prd%nlayers>prd%nmodes_total) THEN
      CALL prd%nim_stop('prd%nlayers must be <= the number of '//               &
                        'Fourier components.')
    ENDIF
!-------------------------------------------------------------------------------
!   assign each layer to a group of procs
!   1st few procs get layer 1, next few get layer 2, etc
!-------------------------------------------------------------------------------
    prd%nprocs_layer = prd%nprocs/prd%nlayers
    prd%node_layer = mod(prd%node,prd%nprocs_layer)
    prd%ilayer = prd%node / prd%nprocs_layer
!-------------------------------------------------------------------------------
!   compute nmodes = # of Fourier modes per processor
!-------------------------------------------------------------------------------
    prd%nmodes = prd%nmodes_total / prd%nlayers
    remainder = mod(prd%nmodes_total,prd%nlayers)
!-------------------------------------------------------------------------------
!   report the distribution of Fourier components.
!-------------------------------------------------------------------------------
    IF (prd%node == 0 .AND. prd%nprocs > 1) THEN
      IF (remainder/=0) THEN
        WRITE(msg,fmt1) remainder,prd%nmodes+1,prd%nlayers-remainder,prd%nmodes
        CALL prd%nim_write(msg)
      ELSE
        WRITE(msg,fmt2) prd%nlayers,prd%nmodes
        CALL prd%nim_write(msg)
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   1st proc (within a block) gets 1st few modes, next proc gets next few, etc
!   all procs in layer < remainder get one extra mode
!-------------------------------------------------------------------------------
    IF (prd%ilayer < remainder) prd%nmodes = prd%nmodes + 1
    prd%mode_lo = prd%ilayer * prd%nmodes + 1
    IF (prd%ilayer >= remainder) prd%mode_lo = remainder*(prd%nmodes+1) +       &
         (prd%ilayer - remainder)*prd%nmodes + 1
    prd%mode_hi = prd%mode_lo + prd%nmodes - 1
!-------------------------------------------------------------------------------
!   group procs into 2 MPI communicators
!   prd%comm_layer = within a layer, across blocks
!   prd%comm_mode = within a block, across modes (layers)
!-------------------------------------------------------------------------------
    CALL mpi_comm_split(prd%comm_nimrod,prd%ilayer,0,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_comm_split','comm_layer',prd%node_layer)
#endif
    CALL mpi_comm_split(prd%comm_nimrod,prd%node_layer,0,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_comm_split','comm_mode',prd%ilayer)
#endif
!-------------------------------------------------------------------------------
!   create the mode to layer map
!-------------------------------------------------------------------------------
    ALLOCATE(prd%mode2layer(prd%nmodes_total))
    ALLOCATE(workvec(prd%nmodes_total))
    prd%mode2layer=0
    DO ib=prd%mode_lo,prd%mode_hi
      prd%mode2layer(ib)=prd%ilayer
    ENDDO
    CALL prd%mode_sum(prd%mode2layer,workvec,prd%nmodes_total)
    prd%mode2layer=workvec
    DEALLOCATE(workvec)
!-------------------------------------------------------------------------------
!   compute nbl = # of rblocks+tblocks on this processor
!   all procs < remainder get one extra block
!-------------------------------------------------------------------------------
    prd%nbl = prd%nbl_total / prd%nprocs_layer
    remainder = mod(prd%nbl_total,prd%nprocs_layer)
    IF (prd%node_layer < remainder) prd%nbl = prd%nbl + 1
!-------------------------------------------------------------------------------
!   allocate space for global block vectors
!-------------------------------------------------------------------------------
    ALLOCATE(prd%block2proc(prd%nbl_total))
    ALLOCATE(prd%global2local(prd%nbl_total))
    prd%block2proc = 0
    prd%global2local = 0
!-------------------------------------------------------------------------------
!   assign rblocks & tblocks to procs (within a layer),
!   all layers do these computations at the same time
!   for decompflag = 0, assign in clumps
!   for decompflag = 1, assign in strided fashion
!   mark the global vectors according to block assignment
!-------------------------------------------------------------------------------
    ibl = 0
    IF (prd%decompflag == 0 .AND. prd%nprocs>1) THEN
      blocklo = prd%node_layer * prd%nbl + 1
      IF (prd%node_layer >= remainder) blocklo = remainder*(prd%nbl+1) +        &
           (prd%node_layer - remainder)*prd%nbl + 1
      blockhi = blocklo + prd%nbl - 1
      DO ib = 1,prd%nbl_total
        IF (ib>=blocklo.AND.ib<=blockhi) THEN
          ibl = ibl + 1
          prd%block2proc(ib) = prd%node
          prd%global2local(ib) = ibl
        ENDIF
      ENDDO
    ELSE
      DO ib = 1,prd%nbl_total
        IF (mod(ib-1_i4,prd%nprocs_layer) == prd%node_layer) THEN
          ibl = ibl + 1
          prd%block2proc(ib) = prd%node
          prd%global2local(ib) = ibl
        ENDIF
      ENDDO
    ENDIF
!-------------------------------------------------------------------------------
!   error checking within and across layers
!-------------------------------------------------------------------------------
    ib = 0
    IF (ibl /= prd%nbl) THEN
      ib = 1
      WRITE(msg,*) 'ibl /= nbl on proc',prd%node,ibl,prd%nbl
      CALL prd%nim_stop(msg)
    ENDIF
    CALL prd%all_max(ib,itmp)
    IF (itmp == 1) CALL prd%nim_stop('Decomposition incorrect.')
    CALL prd%layer_sum(prd%nbl,itmp)
    jtmp = 0
    IF (itmp /= prd%nbl_total) jtmp = 1
    CALL prd%all_max(jtmp,itmp)
    IF (itmp /= 0_i4) CALL prd%nim_stop('Block total incorrect.')
!-------------------------------------------------------------------------------
!   merge 2 global block vectors within each layer
!-------------------------------------------------------------------------------
    ALLOCATE(workvec(prd%nbl_total))
    CALL prd%layer_sum(prd%block2proc,workvec,prd%nbl_total)
    prd%block2proc = workvec
    CALL prd%layer_sum(prd%global2local,workvec,prd%nbl_total)
    prd%global2local = workvec
    DEALLOCATE(workvec)
!-------------------------------------------------------------------------------
!   create the local to global index conversion for convenience.
!-------------------------------------------------------------------------------
    ALLOCATE(prd%loc2glob(prd%nbl))
    DO ib=1,prd%nbl_total
      IF (prd%block2proc(ib)==prd%node) prd%loc2glob(prd%global2local(ib))=ib
    ENDDO
!-------------------------------------------------------------------------------
!   create a layer vector for the local blocks.
!-------------------------------------------------------------------------------
    ALLOCATE(prd%layer2proc(0:prd%nlayers-1),workvec(prd%nlayers))
    prd%layer2proc = 0
    prd%layer2proc(prd%ilayer) = prd%node
    CALL prd%layer_sum(prd%layer2proc,workvec,prd%nlayers)
    prd%layer2proc = workvec
    DEALLOCATE(workvec)
#else
    IF (prd%nlayers > 1) CALL prd%nim_stop('nlayers > 1 requires MPI.')
    prd%nmodes=prd%nmodes_total
    prd%mode_lo=1
    prd%mode_hi=prd%nmodes_total
    prd%nbl=prd%nbl_total
    ALLOCATE(prd%layer2proc(0:prd%nlayers-1))
    prd%layer2proc=0_i4
    ALLOCATE(prd%mode2layer(prd%nmodes_total))
    prd%mode2layer=0_i4
    ALLOCATE(prd%block2proc(prd%nbl_total))
    prd%block2proc=0_i4
    ALLOCATE(prd%global2local(prd%nbl))
    ALLOCATE(prd%loc2glob(prd%nbl))
    DO ib=1,prd%nbl
      prd%global2local(ib)=ib
      prd%loc2glob(ib)=ib
    ENDDO
#endif
!-------------------------------------------------------------------------------
!   set keff if keff_total is allocated
!-------------------------------------------------------------------------------
    IF (ALLOCATED(prd%keff_total)) THEN
      ALLOCATE(prd%keff(prd%nmodes))
      prd%keff=prd%keff_total(prd%mode_lo:prd%mode_hi)
    ENDIF
!-------------------------------------------------------------------------------
!   allocate nindex
!-------------------------------------------------------------------------------
    ALLOCATE(prd%nindex(prd%nmodes))
    DO imode=1,prd%nmodes
      prd%nindex(imode)=prd%mode_lo+imode-2
    ENDDO
  END SUBROUTINE set_decomp

!-------------------------------------------------------------------------------
!* deallocate pardata data
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc(prd)
    IMPLICIT NONE

    !* pardata type to deallocate
    CLASS(pardata), INTENT(INOUT) :: prd

    IF (ALLOCATED(prd%keff_total)) DEALLOCATE(prd%keff_total)
    IF (ALLOCATED(prd%keff)) DEALLOCATE(prd%keff)
    IF (ALLOCATED(prd%nindex)) DEALLOCATE(prd%nindex)
    IF (ALLOCATED(prd%mode2layer)) DEALLOCATE(prd%mode2layer)
    IF (ALLOCATED(prd%block2proc)) DEALLOCATE(prd%block2proc)
    IF (ALLOCATED(prd%global2local)) DEALLOCATE(prd%global2local)
    IF (ALLOCATED(prd%loc2glob)) DEALLOCATE(prd%loc2glob)
    IF (ALLOCATED(prd%layer2proc)) DEALLOCATE(prd%layer2proc)
  END SUBROUTINE dealloc

!-------------------------------------------------------------------------------
!* get unique device stream for async execution based a permutation of id
!-------------------------------------------------------------------------------
  PURE INTEGER(i4) FUNCTION get_stream(prd,id,istream,nstream)
    IMPLICIT NONE

    !* pardata type
    CLASS(pardata), INTENT(IN) :: prd
    !* base execution stream
    INTEGER(i4), INTENT(IN) :: id
    !* forked identifier of execution stream
    INTEGER(i4), INTENT(IN) :: istream
    !* total number of unique additional streams
    INTEGER(i4), INTENT(IN) :: nstream

    IF (prd%single_stream) THEN
      get_stream=id
    ELSE
      get_stream=prd%nbl+(id-1)*nstream+istream
    ENDIF
  END FUNCTION get_stream

!-------------------------------------------------------------------------------
!* wait on unique device streams created by get_stream
!-------------------------------------------------------------------------------
  SUBROUTINE wait_streams(prd,id,nstream)
    IMPLICIT NONE

    !* pardata type
    CLASS(pardata), INTENT(IN) :: prd
    !* base execution stream
    INTEGER(i4), INTENT(IN) :: id
    !* total number of unique additional streams
    INTEGER(i4), INTENT(IN) :: nstream

    INTEGER(i4) :: istream,id_wait

    IF (.NOT.prd%single_stream) THEN
      DO istream=1,nstream
        id_wait=prd%get_stream(id,istream,nstream)
        !$acc wait(id_wait) async(id)
      ENDDO
    ENDIF
  END SUBROUTINE wait_streams

!-------------------------------------------------------------------------------
!* check the mpi return code for errors
!-------------------------------------------------------------------------------
  SUBROUTINE check_error(prd,ierror,mpi_routine,comm_str,node)
    IMPLICIT NONE

    !* pardata type to deallocate
    CLASS(pardata), INTENT(IN) :: prd
    !* mpi error code
    INTEGER(i4), INTENT(IN) :: ierror
    !* mpi_routine called
    CHARACTER(*), INTENT(IN) :: mpi_routine
    !* communicator used
    CHARACTER(*), INTENT(IN) :: comm_str
    !* node on communicator
    INTEGER(i4), INTENT(IN) :: node

    CHARACTER(512) :: msg

    IF (ierror>0) THEN
      WRITE(msg,'(a,i8,i8)') 'MPI error! '//TRIM(mpi_routine)//' '              &
                             //TRIM(comm_str)//'error :',ierror,' on node ',node
      CALL prd%nim_write(msg,force_flush=.TRUE.,any_node=.TRUE.)
    ENDIF
  END SUBROUTINE check_error

!-------------------------------------------------------------------------------
!* call mpi barrier for all application procs
!-------------------------------------------------------------------------------
  SUBROUTINE all_barrier(prd)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_barrier(prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_barrier','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE all_barrier

!-------------------------------------------------------------------------------
!* call mpi barrier for procs over layer decomposition
!-------------------------------------------------------------------------------
  SUBROUTINE layer_barrier(prd)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_barrier(prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_barrier','comm_layer',prd%node_layer)
#endif
#endif
  END SUBROUTINE layer_barrier

!-------------------------------------------------------------------------------
!* call mpi barrier for procs over mode decomposition
!-------------------------------------------------------------------------------
  SUBROUTINE mode_barrier(prd)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_barrier(prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_barrier','comm_mode',prd%ilayer)
#endif
#endif
  END SUBROUTINE mode_barrier

!-------------------------------------------------------------------------------
!* call bcast for all application procs with integers
!-------------------------------------------------------------------------------
  SUBROUTINE all_bcast_int(prd,databuf,arr_size,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(INOUT) :: databuf(..)
    !* broadcast array size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,arr_size,mpi_integer,bcast_node,                     &
                   prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_int','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE all_bcast_int

!-------------------------------------------------------------------------------
!* call bcast for all application procs with reals
!-------------------------------------------------------------------------------
  SUBROUTINE all_bcast_real(prd,databuf,arr_size,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(INOUT) :: databuf(..)
    !* broadcast array size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,arr_size,mpi_double_precision,bcast_node,            &
                   prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_real','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE all_bcast_real

!-------------------------------------------------------------------------------
!* call bcast for all application procs with complex types
!-------------------------------------------------------------------------------
  SUBROUTINE all_bcast_comp(prd,databuf,arr_size,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(INOUT) :: databuf(..)
    !* broadcast array size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,arr_size,mpi_double_complex,bcast_node,              &
                   prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_comp','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE all_bcast_comp

!-------------------------------------------------------------------------------
!* call bcast for all application procs with logicals
!-------------------------------------------------------------------------------
  SUBROUTINE all_bcast_logical(prd,databuf,arr_size,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    LOGICAL, INTENT(INOUT) :: databuf(..)
    !* broadcast array size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,arr_size,mpi_logical,bcast_node,                     &
                   prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_logical','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE all_bcast_logical

!-------------------------------------------------------------------------------
!* call bcast for all application procs with chars
!-------------------------------------------------------------------------------
  SUBROUTINE all_bcast_char(prd,databuf,arr_size,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    CHARACTER(*), INTENT(INOUT) :: databuf
    !* broadcast array size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,arr_size,mpi_character,bcast_node,                   &
                   prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_char','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE all_bcast_char

!-------------------------------------------------------------------------------
!* call bcast for all application procs with an integer
!-------------------------------------------------------------------------------
  SUBROUTINE all_bcast_int_sc(prd,databuf,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(INOUT) :: databuf
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,1_i4,mpi_integer,bcast_node,                         &
                   prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_int_sc','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE all_bcast_int_sc

!-------------------------------------------------------------------------------
!* call bcast for all application procs with a real
!-------------------------------------------------------------------------------
  SUBROUTINE all_bcast_real_sc(prd,databuf,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(INOUT) :: databuf
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,1_i4,mpi_double_precision,bcast_node,                &
                   prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_real_sc','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE all_bcast_real_sc

!-------------------------------------------------------------------------------
!* call bcast for all application procs with a complex type
!-------------------------------------------------------------------------------
  SUBROUTINE all_bcast_comp_sc(prd,databuf,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(INOUT) :: databuf
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,1_i4,mpi_double_complex,bcast_node,                  &
                   prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_comp_sc','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE all_bcast_comp_sc

!-------------------------------------------------------------------------------
!* call bcast for all application procs with a logical
!-------------------------------------------------------------------------------
  SUBROUTINE all_bcast_logical_sc(prd,databuf,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    LOGICAL, INTENT(INOUT) :: databuf
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,1_i4,mpi_logical,bcast_node,                         &
                   prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_logical_sc','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE all_bcast_logical_sc

!-------------------------------------------------------------------------------
!* call bcast for over layer decomposition with integers
!-------------------------------------------------------------------------------
  SUBROUTINE layer_bcast_int(prd,databuf,arr_size,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(INOUT) :: databuf(..)
    !* broadcast array size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,arr_size,mpi_integer,bcast_node,                     &
                   prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_int','comm_layer',prd%node)
#endif
#endif
  END SUBROUTINE layer_bcast_int

!-------------------------------------------------------------------------------
!* call bcast for over layer decomposition with reals
!-------------------------------------------------------------------------------
  SUBROUTINE layer_bcast_real(prd,databuf,arr_size,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(INOUT) :: databuf(..)
    !* broadcast array size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,arr_size,mpi_double_precision,bcast_node,            &
                   prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_real','comm_layer',prd%node)
#endif
#endif
  END SUBROUTINE layer_bcast_real

!-------------------------------------------------------------------------------
!* call bcast for over layer decomposition with complex types
!-------------------------------------------------------------------------------
  SUBROUTINE layer_bcast_comp(prd,databuf,arr_size,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(INOUT) :: databuf(..)
    !* broadcast array size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,arr_size,mpi_double_complex,bcast_node,              &
                   prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_comp','comm_layer',prd%node)
#endif
#endif
  END SUBROUTINE layer_bcast_comp

!-------------------------------------------------------------------------------
!* call bcast for over layer decomposition with logicals
!-------------------------------------------------------------------------------
  SUBROUTINE layer_bcast_logical(prd,databuf,arr_size,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    LOGICAL, INTENT(INOUT) :: databuf(..)
    !* broadcast array size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,arr_size,mpi_logical,bcast_node,                     &
                   prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_logical','comm_layer',prd%node)
#endif
#endif
  END SUBROUTINE layer_bcast_logical

!-------------------------------------------------------------------------------
!* call bcast for over layer decomposition with chars
!-------------------------------------------------------------------------------
  SUBROUTINE layer_bcast_char(prd,databuf,arr_size,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    CHARACTER(*), INTENT(INOUT) :: databuf
    !* broadcast array size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,arr_size,mpi_character,bcast_node,                   &
                   prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_char','comm_layer',prd%node)
#endif
#endif
  END SUBROUTINE layer_bcast_char

!-------------------------------------------------------------------------------
!* call bcast for over layer decomposition with an integer
!-------------------------------------------------------------------------------
  SUBROUTINE layer_bcast_int_sc(prd,databuf,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(INOUT) :: databuf
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,1_i4,mpi_integer,bcast_node,                         &
                   prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_int_sc','comm_layer',prd%node)
#endif
#endif
  END SUBROUTINE layer_bcast_int_sc

!-------------------------------------------------------------------------------
!* call bcast for over layer decomposition with a real
!-------------------------------------------------------------------------------
  SUBROUTINE layer_bcast_real_sc(prd,databuf,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(INOUT) :: databuf
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,1_i4,mpi_double_precision,bcast_node,                &
                   prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_real_sc','comm_layer',prd%node)
#endif
#endif
  END SUBROUTINE layer_bcast_real_sc

!-------------------------------------------------------------------------------
!* call bcast for over layer decomposition with a complex type
!-------------------------------------------------------------------------------
  SUBROUTINE layer_bcast_comp_sc(prd,databuf,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(INOUT) :: databuf
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,1_i4,mpi_double_complex,bcast_node,                  &
                   prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_comp_sc','comm_layer',prd%node)
#endif
#endif
  END SUBROUTINE layer_bcast_comp_sc

!-------------------------------------------------------------------------------
!* call bcast for over layer decomposition with a logical
!-------------------------------------------------------------------------------
  SUBROUTINE layer_bcast_logical_sc(prd,databuf,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    LOGICAL, INTENT(INOUT) :: databuf
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,1_i4,mpi_logical,bcast_node,                         &
                   prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_logical_sc','comm_layer',prd%node)
#endif
#endif
  END SUBROUTINE layer_bcast_logical_sc

!-------------------------------------------------------------------------------
!* call bcast for over mode decomposition with integers
!-------------------------------------------------------------------------------
  SUBROUTINE mode_bcast_int(prd,databuf,arr_size,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(INOUT) :: databuf(..)
    !* broadcast array size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,arr_size,mpi_integer,bcast_node,                     &
                   prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_int','comm_mode',prd%node)
#endif
#endif
  END SUBROUTINE mode_bcast_int

!-------------------------------------------------------------------------------
!* call bcast for over mode decomposition with reals
!-------------------------------------------------------------------------------
  SUBROUTINE mode_bcast_real(prd,databuf,arr_size,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(INOUT) :: databuf(..)
    !* broadcast array size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,arr_size,mpi_double_precision,bcast_node,            &
                   prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_real','comm_mode',prd%node)
#endif
#endif
  END SUBROUTINE mode_bcast_real

!-------------------------------------------------------------------------------
!* call bcast for over mode decomposition with complex types
!-------------------------------------------------------------------------------
  SUBROUTINE mode_bcast_comp(prd,databuf,arr_size,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(INOUT) :: databuf(..)
    !* broadcast array size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,arr_size,mpi_double_complex,bcast_node,              &
                   prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_comp','comm_mode',prd%node)
#endif
#endif
  END SUBROUTINE mode_bcast_comp

!-------------------------------------------------------------------------------
!* call bcast for over mode decomposition with logicals
!-------------------------------------------------------------------------------
  SUBROUTINE mode_bcast_logical(prd,databuf,arr_size,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    LOGICAL, INTENT(INOUT) :: databuf(..)
    !* broadcast array size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,arr_size,mpi_logical,bcast_node,                     &
                   prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_logical','comm_mode',prd%node)
#endif
#endif
  END SUBROUTINE mode_bcast_logical

!-------------------------------------------------------------------------------
!* call bcast for over mode decomposition with chars
!-------------------------------------------------------------------------------
  SUBROUTINE mode_bcast_char(prd,databuf,arr_size,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    CHARACTER(*), INTENT(INOUT) :: databuf
    !* broadcast array size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,arr_size,mpi_character,bcast_node,                   &
                   prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_char','comm_mode',prd%node)
#endif
#endif
  END SUBROUTINE mode_bcast_char

!-------------------------------------------------------------------------------
!* call bcast for over mode decomposition with an integer
!-------------------------------------------------------------------------------
  SUBROUTINE mode_bcast_int_sc(prd,databuf,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(INOUT) :: databuf
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,1_i4,mpi_integer,bcast_node,                         &
                   prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_int_sc','comm_mode',prd%node)
#endif
#endif
  END SUBROUTINE mode_bcast_int_sc

!-------------------------------------------------------------------------------
!* call bcast for over mode decomposition with a real
!-------------------------------------------------------------------------------
  SUBROUTINE mode_bcast_real_sc(prd,databuf,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(INOUT) :: databuf
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,1_i4,mpi_double_precision,bcast_node,                &
                   prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_real_sc','comm_mode',prd%node)
#endif
#endif
  END SUBROUTINE mode_bcast_real_sc

!-------------------------------------------------------------------------------
!* call bcast for over mode decomposition with a complex type
!-------------------------------------------------------------------------------
  SUBROUTINE mode_bcast_comp_sc(prd,databuf,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(INOUT) :: databuf
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,1_i4,mpi_double_complex,bcast_node,                  &
                   prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_comp_sc','comm_mode',prd%node)
#endif
#endif
  END SUBROUTINE mode_bcast_comp_sc

!-------------------------------------------------------------------------------
!* call bcast for over mode decomposition with a logical
!-------------------------------------------------------------------------------
  SUBROUTINE mode_bcast_logical_sc(prd,databuf,bcast_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    LOGICAL, INTENT(INOUT) :: databuf
    !* node to broadcast from
    INTEGER(i4), INTENT(IN) :: bcast_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_bcast(databuf,1_i4,mpi_logical,bcast_node,                         &
                   prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_bcast_logical_sc','comm_mode',prd%node)
#endif
#endif
  END SUBROUTINE mode_bcast_logical_sc

!-------------------------------------------------------------------------------
!* call allreduce for all application procs with mpi_sum and integers
!-------------------------------------------------------------------------------
  SUBROUTINE all_sum_int(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf(*)
    !* received data
    INTEGER(i4), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_integer,                    &
                       mpi_sum,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_int','comm_nimrod',prd%node)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE all_sum_int

!-------------------------------------------------------------------------------
!* call allreduce for all application procs with mpi_max and integers
!-------------------------------------------------------------------------------
  SUBROUTINE all_max_int(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf(*)
    !* received data
    INTEGER(i4), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_integer,                    &
                       mpi_max,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_int','comm_nimrod',prd%node)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE all_max_int

!-------------------------------------------------------------------------------
!* call allreduce for all application procs with mpi_sum and reals
!-------------------------------------------------------------------------------
  SUBROUTINE all_sum_real(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf(*)
    !* received data
    REAL(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_double_precision,           &
                       mpi_sum,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_real','comm_nimrod',prd%node)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE all_sum_real

!-------------------------------------------------------------------------------
!* call allreduce for all application procs with mpi_max and reals
!-------------------------------------------------------------------------------
  SUBROUTINE all_max_real(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf(*)
    !* received data
    REAL(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_double_precision,           &
                       mpi_max,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_real','comm_nimrod',prd%node)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE all_max_real

!-------------------------------------------------------------------------------
!* call allreduce for all application procs with mpi_sum and complex
!-------------------------------------------------------------------------------
  SUBROUTINE all_sum_comp(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf(*)
    !* received data
    COMPLEX(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_double_complex,             &
                       mpi_sum,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_comp','comm_nimrod',prd%node)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE all_sum_comp

!-------------------------------------------------------------------------------
!* call allreduce for all application procs with mpi_max and complex
!-------------------------------------------------------------------------------
  SUBROUTINE all_max_comp(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf(*)
    !* received data
    COMPLEX(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_double_complex,             &
                       mpi_max,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_comp','comm_nimrod',prd%node)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE all_max_comp

!-------------------------------------------------------------------------------
!* call allreduce for all application procs with mpi_sum and integers
!-------------------------------------------------------------------------------
  SUBROUTINE all_sum_int_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf
    !* received data
    INTEGER(i4), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_integer,                        &
                       mpi_sum,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_int_sc',                     &
                         'comm_nimrod',prd%node)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE all_sum_int_sc

!-------------------------------------------------------------------------------
!* call allreduce for all application procs with mpi_max and integers
!-------------------------------------------------------------------------------
  SUBROUTINE all_max_int_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf
    !* received data
    INTEGER(i4), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_integer,                        &
                       mpi_max,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_int_sc',                     &
                         'comm_nimrod',prd%node)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE all_max_int_sc

!-------------------------------------------------------------------------------
!* call allreduce for all application procs with mpi_sum and reals
!-------------------------------------------------------------------------------
  SUBROUTINE all_sum_real_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf
    !* received data
    REAL(r8), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_double_precision,               &
                       mpi_sum,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_real_sc',                    &
                         'comm_nimrod',prd%node)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE all_sum_real_sc

!-------------------------------------------------------------------------------
!* call allreduce for all application procs with mpi_max and reals
!-------------------------------------------------------------------------------
  SUBROUTINE all_max_real_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf
    !* received data
    REAL(r8), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_double_precision,               &
                       mpi_max,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_real_sc',                    &
                         'comm_nimrod',prd%node)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE all_max_real_sc

!-------------------------------------------------------------------------------
!* call allreduce for all application procs with mpi_sum and complex
!-------------------------------------------------------------------------------
  SUBROUTINE all_sum_comp_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf
    !* received data
    COMPLEX(r8), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_double_complex,                 &
                       mpi_sum,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_comp_sc',                    &
                         'comm_nimrod',prd%node)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE all_sum_comp_sc

!-------------------------------------------------------------------------------
!* call allreduce for all application procs with mpi_max and complex
!-------------------------------------------------------------------------------
  SUBROUTINE all_max_comp_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf
    !* received data
    COMPLEX(r8), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_double_complex,                 &
                       mpi_max,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_comp_sc',                    &
                         'comm_nimrod',prd%node)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE all_max_comp_sc

!-------------------------------------------------------------------------------
!* call allreduce over layer decomposition with mpi_sum and integers
!-------------------------------------------------------------------------------
  SUBROUTINE layer_sum_int(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf(*)
    !* received data
    INTEGER(i4), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_integer,                    &
                       mpi_sum,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_int',                        &
                         'comm_layer',prd%node_layer)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE layer_sum_int

!-------------------------------------------------------------------------------
!* call allreduce over layer decomposition with mpi_max and integers
!-------------------------------------------------------------------------------
  SUBROUTINE layer_max_int(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf(*)
    !* received data
    INTEGER(i4), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_integer,                    &
                       mpi_max,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_int',                        &
                         'comm_layer',prd%node_layer)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE layer_max_int

!-------------------------------------------------------------------------------
!* call allreduce over layer decomposition with mpi_sum and reals
!-------------------------------------------------------------------------------
  SUBROUTINE layer_sum_real(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf(*)
    !* received data
    REAL(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_double_precision,           &
                       mpi_sum,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_real',                       &
                         'comm_layer',prd%node_layer)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE layer_sum_real

!-------------------------------------------------------------------------------
!* call allreduce over layer decomposition with mpi_max and reals
!-------------------------------------------------------------------------------
  SUBROUTINE layer_max_real(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf(*)
    !* received data
    REAL(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_double_precision,           &
                       mpi_max,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_real',                       &
                         'comm_layer',prd%node_layer)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE layer_max_real

!-------------------------------------------------------------------------------
!* call allreduce over layer decomposition with mpi_sum and complex
!-------------------------------------------------------------------------------
  SUBROUTINE layer_sum_comp(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf(*)
    !* received data
    COMPLEX(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_double_complex,             &
                       mpi_sum,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_comp',                       &
                         'comm_layer',prd%node_layer)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE layer_sum_comp

!-------------------------------------------------------------------------------
!* call allreduce over layer decomposition with mpi_max and complex
!-------------------------------------------------------------------------------
  SUBROUTINE layer_max_comp(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf(*)
    !* received data
    COMPLEX(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_double_complex,             &
                       mpi_max,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_comp',                       &
                         'comm_layer',prd%node_layer)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE layer_max_comp

!-------------------------------------------------------------------------------
!* call allreduce over layer decomposition with mpi_sum and integers
!-------------------------------------------------------------------------------
  SUBROUTINE layer_sum_int_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf
    !* received data
    INTEGER(i4), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_integer,                        &
                       mpi_sum,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_int_sc',                     &
                         'comm_layer',prd%node_layer)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE layer_sum_int_sc

!-------------------------------------------------------------------------------
!* call allreduce over layer decomposition with mpi_max and integers
!-------------------------------------------------------------------------------
  SUBROUTINE layer_max_int_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf
    !* received data
    INTEGER(i4), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_integer,                        &
                       mpi_max,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_int_sc',                     &
                         'comm_layer',prd%node_layer)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE layer_max_int_sc

!-------------------------------------------------------------------------------
!* call allreduce over layer decomposition with mpi_sum and reals
!-------------------------------------------------------------------------------
  SUBROUTINE layer_sum_real_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf
    !* received data
    REAL(r8), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_double_precision,               &
                       mpi_sum,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_real_sc',                    &
                         'comm_layer',prd%node_layer)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE layer_sum_real_sc

!-------------------------------------------------------------------------------
!* call allreduce over layer decomposition with mpi_max and reals
!-------------------------------------------------------------------------------
  SUBROUTINE layer_max_real_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf
    !* received data
    REAL(r8), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_double_precision,               &
                       mpi_max,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_real_sc',                    &
                         'comm_layer',prd%node_layer)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE layer_max_real_sc

!-------------------------------------------------------------------------------
!* call allreduce over layer decomposition with mpi_sum and complex
!-------------------------------------------------------------------------------
  SUBROUTINE layer_sum_comp_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf
    !* received data
    COMPLEX(r8), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_double_complex,                 &
                       mpi_sum,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_comp_sc',                    &
                         'comm_layer',prd%node_layer)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE layer_sum_comp_sc

!-------------------------------------------------------------------------------
!* call allreduce over layer decomposition with mpi_max and complex
!-------------------------------------------------------------------------------
  SUBROUTINE layer_max_comp_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf
    !* received data
    COMPLEX(r8), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_double_complex,                 &
                       mpi_max,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_comp_sc',                    &
                         'comm_layer',prd%node_layer)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE layer_max_comp_sc

!-------------------------------------------------------------------------------
!* call allreduce over mode decomposition with mpi_sum and integers
!-------------------------------------------------------------------------------
  SUBROUTINE mode_sum_int(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf(*)
    !* received data
    INTEGER(i4), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_integer,                    &
                       mpi_sum,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_int','comm_mode',prd%ilayer)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE mode_sum_int

!-------------------------------------------------------------------------------
!* call allreduce over mode decomposition with mpi_max and integers
!-------------------------------------------------------------------------------
  SUBROUTINE mode_max_int(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf(*)
    !* received data
    INTEGER(i4), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_integer,                    &
                       mpi_max,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_int','comm_mode',prd%ilayer)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE mode_max_int

!-------------------------------------------------------------------------------
!* call allreduce over mode decomposition with mpi_sum and reals
!-------------------------------------------------------------------------------
  SUBROUTINE mode_sum_real(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf(*)
    !* received data
    REAL(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_double_precision,           &
                       mpi_sum,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_real','comm_mode',prd%ilayer)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE mode_sum_real

!-------------------------------------------------------------------------------
!* call allreduce over mode decomposition with mpi_max and reals
!-------------------------------------------------------------------------------
  SUBROUTINE mode_max_real(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf(*)
    !* received data
    REAL(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_double_precision,           &
                       mpi_max,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_real','comm_mode',prd%ilayer)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE mode_max_real

!-------------------------------------------------------------------------------
!* call allreduce over mode decomposition with mpi_sum and complex
!-------------------------------------------------------------------------------
  SUBROUTINE mode_sum_comp(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf(*)
    !* received data
    COMPLEX(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_double_complex,             &
                       mpi_sum,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_comp','comm_mode',prd%ilayer)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE mode_sum_comp

!-------------------------------------------------------------------------------
!* call allreduce over mode decomposition with mpi_max and complex
!-------------------------------------------------------------------------------
  SUBROUTINE mode_max_comp(prd,sendbuf,recvbuf,arr_size)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf(*)
    !* received data
    COMPLEX(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,arr_size,mpi_double_complex,             &
                       mpi_max,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_comp','comm_mode',prd%ilayer)
#endif
#else
    recvbuf(1:arr_size)=sendbuf(1:arr_size)
#endif
  END SUBROUTINE mode_max_comp

!-------------------------------------------------------------------------------
!* call allreduce over mode decomposition with mpi_sum and integers
!-------------------------------------------------------------------------------
  SUBROUTINE mode_sum_int_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf
    !* received data
    INTEGER(i4), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_integer,                        &
                       mpi_sum,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_int_sc',                     &
                         'comm_mode',prd%ilayer)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE mode_sum_int_sc

!-------------------------------------------------------------------------------
!* call allreduce over mode decomposition with mpi_max and integers
!-------------------------------------------------------------------------------
  SUBROUTINE mode_max_int_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf
    !* received data
    INTEGER(i4), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_integer,                        &
                       mpi_max,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_int_sc',                     &
                         'comm_mode',prd%ilayer)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE mode_max_int_sc

!-------------------------------------------------------------------------------
!* call allreduce over mode decomposition with mpi_sum and reals
!-------------------------------------------------------------------------------
  SUBROUTINE mode_sum_real_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf
    !* received data
    REAL(r8), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_double_precision,               &
                       mpi_sum,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_real_sc',                    &
                         'comm_mode',prd%ilayer)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE mode_sum_real_sc

!-------------------------------------------------------------------------------
!* call allreduce over mode decomposition with mpi_max and reals
!-------------------------------------------------------------------------------
  SUBROUTINE mode_max_real_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf
    !* received data
    REAL(r8), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_double_precision,               &
                       mpi_max,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_real_sc',                    &
                         'comm_mode',prd%ilayer)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE mode_max_real_sc

!-------------------------------------------------------------------------------
!* call allreduce over mode decomposition with mpi_sum and complex
!-------------------------------------------------------------------------------
  SUBROUTINE mode_sum_comp_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf
    !* received data
    COMPLEX(r8), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_double_complex,                 &
                       mpi_sum,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_sum_comp_sc',                    &
                         'comm_mode',prd%ilayer)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE mode_sum_comp_sc

!-------------------------------------------------------------------------------
!* call allreduce over mode decomposition with mpi_max and complex
!-------------------------------------------------------------------------------
  SUBROUTINE mode_max_comp_sc(prd,sendbuf,recvbuf)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf
    !* received data
    COMPLEX(r8), INTENT(OUT) :: recvbuf

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_allreduce(sendbuf,recvbuf,1_i4,mpi_double_complex,                 &
                       mpi_max,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_allreduce_max_comp_sc',                    &
                         'comm_mode',prd%ilayer)
#endif
#else
    recvbuf=sendbuf
#endif
  END SUBROUTINE mode_max_comp_sc

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of all application procs for integers
!-------------------------------------------------------------------------------
  SUBROUTINE all_send_int(prd,sendbuf,arr_size,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,arr_size,mpi_integer,                                 &
                  proc,0_i4,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_int','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE all_send_int

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of all application procs for reals
!-------------------------------------------------------------------------------
  SUBROUTINE all_send_real(prd,sendbuf,arr_size,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,arr_size,mpi_double_precision,                        &
                  proc,0_i4,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_real','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE all_send_real

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of all application procs for complex types
!-------------------------------------------------------------------------------
  SUBROUTINE all_send_comp(prd,sendbuf,arr_size,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,arr_size,mpi_double_complex,                          &
                  proc,0_i4,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_comp','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE all_send_comp

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of all application procs for integers
!-------------------------------------------------------------------------------
  SUBROUTINE all_send_int_sc(prd,sendbuf,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,1_i4,mpi_integer,                                     &
                  proc,0_i4,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_int_sc','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE all_send_int_sc

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of all application procs for reals
!-------------------------------------------------------------------------------
  SUBROUTINE all_send_real_sc(prd,sendbuf,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,1_i4,mpi_double_precision,                            &
                  proc,0_i4,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_real_sc','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE all_send_real_sc

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of all application procs for complex types
!-------------------------------------------------------------------------------
  SUBROUTINE all_send_comp_sc(prd,sendbuf,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,1_i4,mpi_double_complex,                              &
                  proc,0_i4,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_comp_sc','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE all_send_comp_sc

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of mode decomposition procs for integers
!-------------------------------------------------------------------------------
  SUBROUTINE mode_send_int(prd,sendbuf,arr_size,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,arr_size,mpi_integer,                                 &
                  proc,0_i4,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_int','comm_mode',prd%node)
#endif
#endif
  END SUBROUTINE mode_send_int

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of mode decomposition procs for reals
!-------------------------------------------------------------------------------
  SUBROUTINE mode_send_real(prd,sendbuf,arr_size,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,arr_size,mpi_double_precision,                        &
                  proc,0_i4,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_real','comm_mode',prd%node)
#endif
#endif
  END SUBROUTINE mode_send_real

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of mode decomposition procs for complex types
!-------------------------------------------------------------------------------
  SUBROUTINE mode_send_comp(prd,sendbuf,arr_size,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,arr_size,mpi_double_complex,                          &
                  proc,0_i4,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_comp','comm_mode',prd%node)
#endif
#endif
  END SUBROUTINE mode_send_comp

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of mode decomposition procs for integers
!-------------------------------------------------------------------------------
  SUBROUTINE mode_send_int_sc(prd,sendbuf,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,1_i4,mpi_integer,                                     &
                  proc,0_i4,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_int_sc','comm_mode',prd%node)
#endif
#endif
  END SUBROUTINE mode_send_int_sc

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of mode decomposition procs for reals
!-------------------------------------------------------------------------------
  SUBROUTINE mode_send_real_sc(prd,sendbuf,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,1_i4,mpi_double_precision,                            &
                  proc,0_i4,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_real_sc','comm_mode',prd%node)
#endif
#endif
  END SUBROUTINE mode_send_real_sc

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of mode decomposition procs for
!  complex types
!-------------------------------------------------------------------------------
  SUBROUTINE mode_send_comp_sc(prd,sendbuf,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,1_i4,mpi_double_complex,                              &
                  proc,0_i4,prd%comm_mode,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_comp_sc','comm_mode',prd%node)
#endif
#endif
  END SUBROUTINE mode_send_comp_sc

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of layer decomposition procs for integers
!-------------------------------------------------------------------------------
  SUBROUTINE layer_send_int(prd,sendbuf,arr_size,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,arr_size,mpi_integer,                                 &
                  proc,0_i4,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_int','comm_layer',prd%node)
#endif
#endif
  END SUBROUTINE layer_send_int

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of layer decomposition procs for reals
!-------------------------------------------------------------------------------
  SUBROUTINE layer_send_real(prd,sendbuf,arr_size,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,arr_size,mpi_double_precision,                        &
                  proc,0_i4,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_real','comm_layer',prd%node)
#endif
#endif
  END SUBROUTINE layer_send_real

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of layer decomposition procs for
!  complex types
!-------------------------------------------------------------------------------
  SUBROUTINE layer_send_comp(prd,sendbuf,arr_size,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,arr_size,mpi_double_complex,                          &
                  proc,0_i4,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_comp','comm_layer',prd%node)
#endif
#endif
  END SUBROUTINE layer_send_comp

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of layer decomposition procs for integers
!-------------------------------------------------------------------------------
  SUBROUTINE layer_send_int_sc(prd,sendbuf,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,1_i4,mpi_integer,                                     &
                  proc,0_i4,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_int_sc','comm_layer',prd%node)
#endif
#endif
  END SUBROUTINE layer_send_int_sc

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of layer decomposition procs for reals
!-------------------------------------------------------------------------------
  SUBROUTINE layer_send_real_sc(prd,sendbuf,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    REAL(r8), INTENT(IN) :: sendbuf
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,1_i4,mpi_double_precision,                            &
                  proc,0_i4,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_real_sc','comm_layer',prd%node)
#endif
#endif
  END SUBROUTINE layer_send_real_sc

!-------------------------------------------------------------------------------
!* call send to mpi processor proc of layer decomposition procs for
!  complex types
!-------------------------------------------------------------------------------
  SUBROUTINE layer_send_comp_sc(prd,sendbuf,proc)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    COMPLEX(r8), INTENT(IN) :: sendbuf
    !* mpi processor to which to send the data
    INTEGER(i4), INTENT(IN) :: proc

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_send(sendbuf,1_i4,mpi_double_complex,                              &
                  proc,0_i4,prd%comm_layer,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_send_comp_sc','comm_layer',prd%node)
#endif
#endif
  END SUBROUTINE layer_send_comp_sc

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of all application procs for integers
!-------------------------------------------------------------------------------
  SUBROUTINE all_recv_int(prd,recvbuf,arr_size,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    INTEGER(i4), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,arr_size,mpi_integer,                               &
                    proc,0_i4,prd%comm_nimrod,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,arr_size,mpi_integer,                               &
                    proc,0_i4,prd%comm_nimrod,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_int','comm_nimrod',prd%node)
#endif
#else
    recvbuf(1:arr_size)=0_i4
#endif
  END SUBROUTINE all_recv_int

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of all application procs for reals
!-------------------------------------------------------------------------------
  SUBROUTINE all_recv_real(prd,recvbuf,arr_size,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    REAL(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,arr_size,mpi_double_precision,                      &
                    proc,0_i4,prd%comm_nimrod,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,arr_size,mpi_double_precision,                      &
                    proc,0_i4,prd%comm_nimrod,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_real','comm_nimrod',prd%node)
#endif
#else
    recvbuf(1:arr_size)=0._r8
#endif
  END SUBROUTINE all_recv_real

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of all application procs for complex types
!-------------------------------------------------------------------------------
  SUBROUTINE all_recv_comp(prd,recvbuf,arr_size,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    COMPLEX(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,arr_size,mpi_double_complex,                        &
                    proc,0_i4,prd%comm_nimrod,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,arr_size,mpi_double_complex,                        &
                    proc,0_i4,prd%comm_nimrod,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_comp','comm_nimrod',prd%node)
#endif
#else
    recvbuf(1:arr_size)=0._r8
#endif
  END SUBROUTINE all_recv_comp

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of all application procs for integers
!-------------------------------------------------------------------------------
  SUBROUTINE all_recv_int_sc(prd,recvbuf,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    INTEGER(i4), INTENT(OUT) :: recvbuf
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,1_i4,mpi_integer,                                   &
                    proc,0_i4,prd%comm_nimrod,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,1_i4,mpi_integer,                                   &
                    proc,0_i4,prd%comm_nimrod,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_int_sc','comm_nimrod',prd%node)
#endif
#else
    recvbuf=0_i4
#endif
  END SUBROUTINE all_recv_int_sc

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of all application procs for reals
!-------------------------------------------------------------------------------
  SUBROUTINE all_recv_real_sc(prd,recvbuf,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    REAL(r8), INTENT(OUT) :: recvbuf
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,1_i4,mpi_double_precision,                          &
                    proc,0_i4,prd%comm_nimrod,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,1_i4,mpi_double_precision,                          &
                    proc,0_i4,prd%comm_nimrod,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_real_sc','comm_nimrod',prd%node)
#endif
#else
    recvbuf=0._r8
#endif
  END SUBROUTINE all_recv_real_sc

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of all application procs for complex types
!-------------------------------------------------------------------------------
  SUBROUTINE all_recv_comp_sc(prd,recvbuf,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    COMPLEX(r8), INTENT(OUT) :: recvbuf
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,1_i4,mpi_double_complex,                            &
                    proc,0_i4,prd%comm_nimrod,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,1_i4,mpi_double_complex,                            &
                    proc,0_i4,prd%comm_nimrod,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_comp_sc','comm_nimrod',prd%node)
#endif
#else
    recvbuf=0._r8
#endif
  END SUBROUTINE all_recv_comp_sc

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of mode decomposition procs for integers
!-------------------------------------------------------------------------------
  SUBROUTINE mode_recv_int(prd,recvbuf,arr_size,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    INTEGER(i4), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,arr_size,mpi_integer,                               &
                    proc,0_i4,prd%comm_mode,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,arr_size,mpi_integer,                               &
                    proc,0_i4,prd%comm_mode,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_int','comm_mode',prd%node)
#endif
#else
    recvbuf(1:arr_size)=0_i4
#endif
  END SUBROUTINE mode_recv_int

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of mode decomposition procs for reals
!-------------------------------------------------------------------------------
  SUBROUTINE mode_recv_real(prd,recvbuf,arr_size,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    REAL(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,arr_size,mpi_double_precision,                      &
                    proc,0_i4,prd%comm_mode,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,arr_size,mpi_double_precision,                      &
                    proc,0_i4,prd%comm_mode,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_real','comm_mode',prd%node)
#endif
#else
    recvbuf(1:arr_size)=0._r8
#endif
  END SUBROUTINE mode_recv_real

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of mode decomposition procs for
!  complex types
!-------------------------------------------------------------------------------
  SUBROUTINE mode_recv_comp(prd,recvbuf,arr_size,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    COMPLEX(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,arr_size,mpi_double_complex,                        &
                    proc,0_i4,prd%comm_mode,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,arr_size,mpi_double_complex,                        &
                    proc,0_i4,prd%comm_mode,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_comp','comm_mode',prd%node)
#endif
#else
    recvbuf(1:arr_size)=0._r8
#endif
  END SUBROUTINE mode_recv_comp

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of mode decomposition procs for integers
!-------------------------------------------------------------------------------
  SUBROUTINE mode_recv_int_sc(prd,recvbuf,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    INTEGER(i4), INTENT(OUT) :: recvbuf
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,1_i4,mpi_integer,                                   &
                    proc,0_i4,prd%comm_mode,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,1_i4,mpi_integer,                                   &
                    proc,0_i4,prd%comm_mode,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_int_sc','comm_mode',prd%node)
#endif
#else
    recvbuf=0_i4
#endif
  END SUBROUTINE mode_recv_int_sc

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of mode decomposition procs for reals
!-------------------------------------------------------------------------------
  SUBROUTINE mode_recv_real_sc(prd,recvbuf,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    REAL(r8), INTENT(OUT) :: recvbuf
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,1_i4,mpi_double_precision,                          &
                    proc,0_i4,prd%comm_mode,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,1_i4,mpi_double_precision,                          &
                    proc,0_i4,prd%comm_mode,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_real_sc','comm_mode',prd%node)
#endif
#else
    recvbuf=0._r8
#endif
  END SUBROUTINE mode_recv_real_sc

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of mode decomposition procs for
!  complex types
!-------------------------------------------------------------------------------
  SUBROUTINE mode_recv_comp_sc(prd,recvbuf,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    COMPLEX(r8), INTENT(OUT) :: recvbuf
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,1_i4,mpi_double_complex,                            &
                    proc,0_i4,prd%comm_mode,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,1_i4,mpi_double_complex,                            &
                    proc,0_i4,prd%comm_mode,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_comp_sc','comm_mode',prd%node)
#endif
#else
    recvbuf=0._r8
#endif
  END SUBROUTINE mode_recv_comp_sc

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of layer decomposition procs for integers
!-------------------------------------------------------------------------------
  SUBROUTINE layer_recv_int(prd,recvbuf,arr_size,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    INTEGER(i4), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,arr_size,mpi_integer,                               &
                    proc,0_i4,prd%comm_layer,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,arr_size,mpi_integer,                               &
                    proc,0_i4,prd%comm_layer,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_int','comm_layer',prd%node)
#endif
#else
    recvbuf(1:arr_size)=0_i4
#endif
  END SUBROUTINE layer_recv_int

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of layer decomposition procs for reals
!-------------------------------------------------------------------------------
  SUBROUTINE layer_recv_real(prd,recvbuf,arr_size,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    REAL(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,arr_size,mpi_double_precision,                      &
                    proc,0_i4,prd%comm_layer,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,arr_size,mpi_double_precision,                      &
                    proc,0_i4,prd%comm_layer,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_real','comm_layer',prd%node)
#endif
#else
    recvbuf(1:arr_size)=0._r8
#endif
  END SUBROUTINE layer_recv_real

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of layer decomposition procs for
!  complex types
!-------------------------------------------------------------------------------
  SUBROUTINE layer_recv_comp(prd,recvbuf,arr_size,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    COMPLEX(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror,nlayers
    TYPE(mpi_status) :: ftn_status

    CALL mpi_comm_size(prd%comm_layer,nlayers,ierror)
    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,arr_size,mpi_double_complex,                        &
                    proc,0_i4,prd%comm_layer,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,arr_size,mpi_double_complex,                        &
                    proc,0_i4,prd%comm_layer,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_comp','comm_layer',prd%node)
#endif
#else
    recvbuf(1:arr_size)=0._r8
#endif
  END SUBROUTINE layer_recv_comp

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of layer decomposition procs for integers
!-------------------------------------------------------------------------------
  SUBROUTINE layer_recv_int_sc(prd,recvbuf,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    INTEGER(i4), INTENT(OUT) :: recvbuf
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,1_i4,mpi_integer,                                   &
                    proc,0_i4,prd%comm_layer,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,1_i4,mpi_integer,                                   &
                    proc,0_i4,prd%comm_layer,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_int_sc','comm_layer',prd%node)
#endif
#else
    recvbuf=0_i4
#endif
  END SUBROUTINE layer_recv_int_sc

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of layer decomposition procs for reals
!-------------------------------------------------------------------------------
  SUBROUTINE layer_recv_real_sc(prd,recvbuf,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    REAL(r8), INTENT(OUT) :: recvbuf
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,1_i4,mpi_double_precision,                          &
                    proc,0_i4,prd%comm_layer,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,1_i4,mpi_double_precision,                          &
                    proc,0_i4,prd%comm_layer,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_real_sc','comm_layer',prd%node)
#endif
#else
    recvbuf=0._r8
#endif
  END SUBROUTINE layer_recv_real_sc

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of layer decomposition procs for
!  complex types
!-------------------------------------------------------------------------------
  SUBROUTINE layer_recv_comp_sc(prd,recvbuf,proc,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    COMPLEX(r8), INTENT(OUT) :: recvbuf
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_recv(recvbuf,1_i4,mpi_double_complex,                            &
                    proc,0_i4,prd%comm_layer,in_status,ierror)
    ELSE
      CALL mpi_recv(recvbuf,1_i4,mpi_double_complex,                            &
                    proc,0_i4,prd%comm_layer,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_recv_comp_sc','comm_layer',prd%node)
#endif
#else
    recvbuf=0._r8
#endif
  END SUBROUTINE layer_recv_comp_sc

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of all application procs for integers
!-------------------------------------------------------------------------------
  SUBROUTINE all_irecv_int(prd,recvbuf,arr_size,proc,request)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    INTEGER(i4), INTENT(OUT) :: recvbuf(*)
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi request
    TYPE(mpi_request), INTENT(INOUT) :: request

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_irecv(recvbuf,arr_size,mpi_integer,                                &
                   proc,0_i4,prd%comm_nimrod,request,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_irecv_int','comm_nimrod',prd%node)
#endif
#else
    recvbuf(1:arr_size)=0_i4
#endif
  END SUBROUTINE all_irecv_int

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of all application procs for reals
!-------------------------------------------------------------------------------
  SUBROUTINE all_irecv_real(prd,recvbuf,arr_size,proc,request)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    REAL(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi request
    TYPE(mpi_request), INTENT(INOUT) :: request

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_irecv(recvbuf,arr_size,mpi_double_precision,                       &
                   proc,0_i4,prd%comm_nimrod,request,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_irecv_real','comm_nimrod',prd%node)
#endif
#else
    recvbuf(1:arr_size)=0._r8
#endif
  END SUBROUTINE all_irecv_real

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of all application procs for complex types
!-------------------------------------------------------------------------------
  SUBROUTINE all_irecv_comp(prd,recvbuf,arr_size,proc,request)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    COMPLEX(r8), INTENT(OUT) :: recvbuf(*)
    !* reduction size
    INTEGER(i4), INTENT(IN) :: arr_size
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi request
    TYPE(mpi_request), INTENT(INOUT) :: request

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_irecv(recvbuf,arr_size,mpi_double_complex,                         &
                   proc,0_i4,prd%comm_nimrod,request,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_irecv_comp','comm_nimrod',prd%node)
#endif
#else
    recvbuf(1:arr_size)=0._r8
#endif
  END SUBROUTINE all_irecv_comp

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of all application procs for integers
!-------------------------------------------------------------------------------
  SUBROUTINE all_irecv_int_sc(prd,recvbuf,proc,request)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    INTEGER(i4), INTENT(OUT) :: recvbuf
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi request
    TYPE(mpi_request), INTENT(INOUT) :: request

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_irecv(recvbuf,1_i4,mpi_integer,                                    &
                   proc,0_i4,prd%comm_nimrod,request,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_irecv_int_sc','comm_nimrod',prd%node)
#endif
#else
    recvbuf=0_i4
#endif
  END SUBROUTINE all_irecv_int_sc

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of all application procs for reals
!-------------------------------------------------------------------------------
  SUBROUTINE all_irecv_real_sc(prd,recvbuf,proc,request)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    REAL(r8), INTENT(OUT) :: recvbuf
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi request
    TYPE(mpi_request), INTENT(INOUT) :: request

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_irecv(recvbuf,1_i4,mpi_double_precision,                           &
                   proc,0_i4,prd%comm_nimrod,request,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_irecv_real_sc','comm_nimrod',prd%node)
#endif
#else
    recvbuf=0._r8
#endif
  END SUBROUTINE all_irecv_real_sc

!-------------------------------------------------------------------------------
!* call recv from mpi processor proc of all application procs for complex types
!-------------------------------------------------------------------------------
  SUBROUTINE all_irecv_comp_sc(prd,recvbuf,proc,request)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* recv data
    COMPLEX(r8), INTENT(OUT) :: recvbuf
    !* mpi processor which sent the data
    INTEGER(i4), INTENT(IN) :: proc
    !* mpi request
    TYPE(mpi_request), INTENT(INOUT) :: request

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_irecv(recvbuf,1_i4,mpi_double_complex,                             &
                   proc,0_i4,prd%comm_nimrod,request,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_irecv_comp_sc','comm_nimrod',prd%node)
#endif
#else
    recvbuf=0._r8
#endif
  END SUBROUTINE all_irecv_comp_sc

!-------------------------------------------------------------------------------
!* call wait
!-------------------------------------------------------------------------------
  SUBROUTINE wait(prd,request,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* mpi request
    TYPE(mpi_request), INTENT(INOUT) :: request
    !* mpi status
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_wait(request,in_status,ierror)
    ELSE
      CALL mpi_wait(request,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_wait','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE wait

!-------------------------------------------------------------------------------
!* call waitany
!-------------------------------------------------------------------------------
  SUBROUTINE waitany(prd,ncount,requests,indx,in_status)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* number of requests
    INTEGER(i4), INTENT(IN) :: ncount
    !* mpi requests array
    TYPE(mpi_request), INTENT(INOUT) :: requests(*)
    !* index of completed request
    INTEGER(i4), INTENT(OUT) :: indx
    !* mpi status array
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_status

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status) :: ftn_status

    IF (PRESENT(in_status)) THEN
      CALL mpi_waitany(ncount,requests,indx,in_status,ierror)
    ELSE
      CALL mpi_waitany(ncount,requests,indx,ftn_status,ierror)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_waitany','comm_nimrod',prd%node)
#endif
#else
    indx=0_i4
#endif
  END SUBROUTINE waitany

!-------------------------------------------------------------------------------
!* call waitall
!-------------------------------------------------------------------------------
  SUBROUTINE waitall(prd,ncount,requests,in_statuses)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* number of requests
    INTEGER(i4), INTENT(IN) :: ncount
    !* mpi requests array
    TYPE(mpi_request), INTENT(INOUT) :: requests(*)
    !* mpi status array
    TYPE(mpi_status), INTENT(INOUT), OPTIONAL :: in_statuses(*)

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror
    TYPE(mpi_status), ALLOCATABLE :: ftn_statuses(:)

    IF (PRESENT(in_statuses)) THEN
      CALL mpi_waitall(ncount,requests,in_statuses,ierror)
    ELSE
      ALLOCATE(ftn_statuses(ncount))
      CALL mpi_waitall(ncount,requests,ftn_statuses,ierror)
      DEALLOCATE(ftn_statuses)
    ENDIF
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_waitall','comm_nimrod',prd%node)
#endif
#endif
  END SUBROUTINE waitall

!-------------------------------------------------------------------------------
!* call gather (NOT allgather) over all appliation procs with integers
!-------------------------------------------------------------------------------
  SUBROUTINE all_gather_int(prd,sendbuf,sendcount,recvbuf,gather_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data, size(sendcount)
    INTEGER(i4), INTENT(IN) :: sendbuf(*)
    !* send count
    INTEGER(i4), INTENT(IN) :: sendcount
    !* received data, size(sendcount*nprocs)
    INTEGER(i4), INTENT(OUT) :: recvbuf(*)
    !* node to gather the array to
    INTEGER(i4), INTENT(IN) :: gather_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_gather(sendbuf,sendcount,mpi_integer,                              &
                    recvbuf,sendcount,mpi_integer,                              &
                    gather_node,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_gather_int','comm_nimrod',prd%node)
#endif
#else
    recvbuf(1:sendcount)=sendbuf(1:sendcount)
#endif
  END SUBROUTINE all_gather_int

!-------------------------------------------------------------------------------
!* call gather (NOT allgather) over all appliation procs with reals
!-------------------------------------------------------------------------------
  SUBROUTINE all_gather_real(prd,sendbuf,sendcount,recvbuf,gather_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data, size(sendcount)
    REAL(r8), INTENT(IN) :: sendbuf(*)
    !* send count
    INTEGER(i4), INTENT(IN) :: sendcount
    !* received data, size(sendcount*nprocs)
    REAL(r8), INTENT(OUT) :: recvbuf(*)
    !* node to gather the array to
    INTEGER(i4), INTENT(IN) :: gather_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_gather(sendbuf,sendcount,mpi_double_precision,                     &
                    recvbuf,sendcount,mpi_double_precision,                     &
                    gather_node,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_gather_real','comm_nimrod',prd%node)
#endif
#else
!-------------------------------------------------------------------------------
!   buffer sizes must be equal with 1 proc
!-------------------------------------------------------------------------------
    recvbuf(1:sendcount)=sendbuf(1:sendcount)
#endif
  END SUBROUTINE all_gather_real

!-------------------------------------------------------------------------------
!* call gather (NOT allgather) over all appliation procs with characters
!-------------------------------------------------------------------------------
  SUBROUTINE all_gather_char(prd,sendbuf,sendcount,recvbuf,gather_node)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data, LEN(sendcount)
    CHARACTER(*), INTENT(IN) :: sendbuf
    !* send count
    INTEGER(i4), INTENT(IN) :: sendcount
    !* received data, LEN(sendcount) and SIZE(nprocs)
    CHARACTER(*), INTENT(OUT) :: recvbuf(*)
    !* node to gather the array to
    INTEGER(i4), INTENT(IN) :: gather_node

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_gather(sendbuf,sendcount,mpi_character,                            &
                    recvbuf,sendcount,mpi_character,                            &
                    gather_node,prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_gather_char','comm_nimrod',prd%node)
#endif
#else
!-------------------------------------------------------------------------------
!   buffer sizes must be equal with 1 proc
!-------------------------------------------------------------------------------
    recvbuf(1:sendcount)=sendbuf(1:sendcount)
#endif
  END SUBROUTINE all_gather_char

!-------------------------------------------------------------------------------
!* call reduce_scatter over all appliation procs with mpi_sum and ints
!-------------------------------------------------------------------------------
  SUBROUTINE all_sum_scatter_int(prd,sendbuf,recvbuf,recvcounts)
    IMPLICIT NONE

    !* pardata class instance
    CLASS(pardata), INTENT(IN) :: prd
    !* send data
    INTEGER(i4), INTENT(IN) :: sendbuf(*)
    !* received data
    INTEGER(i4), INTENT(OUT) :: recvbuf
    !* reduction size
    INTEGER(i4), INTENT(IN) :: recvcounts(*)

#ifdef HAVE_MPI
    INTEGER(i4) :: ierror

    CALL mpi_reduce_scatter(sendbuf,recvbuf,recvcounts,mpi_integer,mpi_sum,     &
                            prd%comm_nimrod,ierror)
#ifdef DEBUG_MPI
    CALL prd%check_error(ierror,'mpi_reduce_scatter_int','comm_nimrod',prd%node)
#endif
#else
!-------------------------------------------------------------------------------
!   buffer sizes must be 1 with 1 proc
!-------------------------------------------------------------------------------
    recvbuf=sendbuf(1)
#endif
  END SUBROUTINE all_sum_scatter_int

END MODULE pardata_mod
