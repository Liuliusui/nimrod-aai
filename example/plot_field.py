#!/usr/bin/env python3
import h5py
import argparse
import numpy as np
import matplotlib.pyplot as plt


def blockstr(bl):
    return str(bl+1).zfill(4)


def main(dumpfile, show_plt, plt_file, show_mesh, show_elements,
         only_vertex_nodes, component):
    meshlist = []
    fldlist = []
    pdlist = []
    maxfld = -np.inf
    minfld = np.inf
    with h5py.File(dumpfile, 'r') as dump:
        nbl = dump.attrs['nbl'][0]
        try:
            bldat = dump['/blocks/0001/field0001'] # noqa
            fldname = "field"
        except KeyError:
            try:
                bldat = dump['/blocks/0001/refield0001'] # noqa
                fldname = "refield"
            except KeyError:
                print("Unable to read field")
                raise
        for bl in range(nbl):
            blstr = blockstr(bl)
            meshstr = "mesh" + blstr
            fldstr = fldname + blstr
            mesh = np.array(dump[f'/blocks/{blstr}/{meshstr}'][:])
            field = np.array(dump[f'/blocks/{blstr}/{fldstr}'][:])
            meshlist.append(mesh)
            fldlist.append(field)
            blgrp = dump[f'/blocks/{blstr}']
            pd = blgrp.attrs.get('poly_degree')[0]
            pdlist.append(pd)
            if only_vertex_nodes:
                ie = pd
            else:
                ie = 1
            fldmax = np.amax(field[::ie, ::ie, component])
            fldmin = np.amin(field[::ie, ::ie, component])
            maxfld = np.amax([fldmax, maxfld])
            minfld = np.amin([fldmin, minfld])
    nlvls = 50
    levels = np.linspace(minfld, maxfld, nlvls, endpoint=True)
    fig, ax = plt.subplots()
    for mesh, field, pd in zip(meshlist, fldlist, pdlist):
        if only_vertex_nodes:
            ie = pd
        else:
            ie = 1
        contour = ax.contourf(mesh[::ie, ::ie, 0], mesh[::ie, ::ie, 1],
                              field[::ie, ::ie, component], levels)
        if show_elements:
            ie = pd
        else:
            ie = 1
        if show_mesh:
            ax.plot(mesh[::ie, ::ie, 0], mesh[::ie, ::ie, 1],
                    color='k', alpha=0.1)
            ax.plot(np.transpose(mesh[::ie, ::ie, 0]),
                    np.transpose(mesh[::ie, ::ie, 1]),
                    color='k', alpha=0.1)
    ax.set_xlabel('R')
    ax.set_ylabel('Z')
    cbar = fig.colorbar(contour)
    cbar.ax.set_ylabel('field')
    plt.title(dumpfile)
    if plt_file:
        fig.savefig(plt_file)
    if show_plt:
        plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
                    prog='PlotField',
                    description='Plot field with a contour plot')
    parser.add_argument('dumpfile', help="NIMROD dumpfile to read")
    parser.add_argument('-c', '--component', dest='component', default=0,
                        type=int,
                        help="Field component to plot (0 to nqty*nmodes)")
    parser.add_argument('-e', '--elements', dest='elements', default=False,
                        help="Plot finite elements on top of countour plot",
                        action='store_true')
    parser.add_argument('-f', '--file', dest='filename', default=None,
                        action='store_const',
                        help="Output plot to file filename")
    parser.add_argument('-n', '--noshow', dest='noshow', default=False,
                        help="No plot popup window",
                        action='store_true')
    parser.add_argument('-m', '--mesh', dest='mesh', default=False,
                        help="Plot mesh on top of countour plot",
                        action='store_true')
    parser.add_argument('-v', '--vertex_nodes', dest='vertex_only',
                        default=False,
                        help="Plot only finite element vertex nodes",
                        action='store_true')
    args = parser.parse_args()

    main(args.dumpfile, not args.noshow, args.filename, args.mesh,
         args.elements, args.vertex_only, args.component)
