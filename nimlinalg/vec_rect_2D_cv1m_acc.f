!-------------------------------------------------------------------------------
! Implementation of vec_rect_2D_cv1m_acc included in vec_rect_2D_acc.f
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* allocate a complex single-mode vector
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_cv1m(cv1m,poly_degree,mx,my,nqty,id,on_gpu)
    IMPLICIT NONE

    !> vector to allocate
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> polynomial degree
    INTEGER(i4), INTENT(IN) :: poly_degree
    !> number of elements in the horizontal direction
    INTEGER(i4), INTENT(IN) :: mx
    !> number of elements in the vertical direction
    INTEGER(i4), INTENT(IN) :: my
    !> number of quantities
    INTEGER(i4), INTENT(IN) :: nqty
    !> ID for parallel streams
    INTEGER(i4), INTENT(IN) :: id
    !> true if data on GPU
    LOGICAL, INTENT(IN) :: on_gpu

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   store grid and vector dimensions
!-------------------------------------------------------------------------------
    cv1m%nqty=nqty
    cv1m%mx=mx
    cv1m%my=my
    cv1m%n_side=poly_degree-1
    cv1m%n_int=(poly_degree-1)**2
    cv1m%u_ndof=(poly_degree+1)**2
    cv1m%pd=poly_degree
    cv1m%nel=mx*my
    cv1m%ndim=2
    cv1m%id=id
    cv1m%on_gpu=on_gpu
    cv1m%inf_norm_size=4
    cv1m%l2_dot_size=5
    !$acc enter data copyin(cv1m) async(cv1m%id) if(cv1m%on_gpu)
!-------------------------------------------------------------------------------
!   allocate space according to the basis functions needed.
!   pad all basis type arrays, meaningful element bounds are
!   arr(0:mx,0:my) arrh(1:mx,0:my) arrv(0:mx,1:my) and arri(1:mx,1:my)
!-------------------------------------------------------------------------------
    SELECT CASE(poly_degree)
    CASE(1)  !  linear elements
      ALLOCATE(cv1m%arr(nqty,-1:mx+1,-1:my+1))
      !$acc enter data create(cv1m%arr) async(cv1m%id) if(cv1m%on_gpu)
      NULLIFY(cv1m%arri,cv1m%arrh,cv1m%arrv)
    CASE(2:) !  higher-order elements
      ALLOCATE(cv1m%arr(nqty,-1:mx+1,-1:my+1))
      !$acc enter data create(cv1m%arr) async(cv1m%id) if(cv1m%on_gpu)
      ALLOCATE(cv1m%arrh(nqty,poly_degree-1,-1:mx+1,-1:my+1))
      !$acc enter data create(cv1m%arrh) async(cv1m%id) if(cv1m%on_gpu)
      ALLOCATE(cv1m%arrv(nqty,poly_degree-1,-1:mx+1,-1:my+1))
      !$acc enter data create(cv1m%arrv) async(cv1m%id) if(cv1m%on_gpu)
      ALLOCATE(cv1m%arri(nqty,(poly_degree-1)**2,-1:mx+1,-1:my+1))
      !$acc enter data create(cv1m%arri) async(cv1m%id) if(cv1m%on_gpu)
    END SELECT
!-------------------------------------------------------------------------------
!   register this object.
!-------------------------------------------------------------------------------
    NULLIFY(cv1m%mem_id)
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      INTEGER(i4) :: sz
      sz= INT(SIZEOF(cv1m%arr)+SIZEOF(cv1m%arrh)+SIZEOF(cv1m%arrv)              &
             +SIZEOF(cv1m%arri)+SIZEOF(cv1m),i4)
      CALL memlogger%update(cv1m%mem_id,'cv1m'//mod_name,'unknown',sz)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_cv1m

!-------------------------------------------------------------------------------
!* Deallocate the vector
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_cv1m(cv1m)
    IMPLICIT NONE

    !> vector to deallocate
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc_cv1m',iftn,idepth)
    IF (ASSOCIATED(cv1m%arr)) THEN
      !$acc exit data delete(cv1m%arr) async(cv1m%id) finalize if(cv1m%on_gpu)
      DEALLOCATE(cv1m%arr)
      NULLIFY(cv1m%arr)
    ENDIF
    IF (ASSOCIATED(cv1m%arrh)) THEN
      !$acc exit data delete(cv1m%arrh) async(cv1m%id) finalize if(cv1m%on_gpu)
      DEALLOCATE(cv1m%arrh)
      NULLIFY(cv1m%arrh)
    ENDIF
    IF (ASSOCIATED(cv1m%arrv)) THEN
      !$acc exit data delete(cv1m%arrv) async(cv1m%id) finalize if(cv1m%on_gpu)
      DEALLOCATE(cv1m%arrv)
      NULLIFY(cv1m%arrv)
    ENDIF
    IF (ASSOCIATED(cv1m%arri)) THEN
      !$acc exit data delete(cv1m%arri) async(cv1m%id) finalize if(cv1m%on_gpu)
      DEALLOCATE(cv1m%arri)
      NULLIFY(cv1m%arri)
    ENDIF
    !$acc exit data delete(cv1m) finalize async(cv1m%id) if(cv1m%on_gpu)
!-------------------------------------------------------------------------------
!   unregister this object.
!-------------------------------------------------------------------------------
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      CALL memlogger%update(cv1m%mem_id,'cv1m'//mod_name,' ',0,resize=.TRUE.)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc_cv1m

!-------------------------------------------------------------------------------
!* Create a new vector of the same type
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_with_mold_cv1m(cv1m,new_cv1m,nqty,pd)
    IMPLICIT NONE

    !> reference vector to mold
    CLASS(vec_rect_2D_cv1m_acc), INTENT(IN) :: cv1m
    !> new vector to be created
    CLASS(cvector1m), ALLOCATABLE, INTENT(OUT) :: new_cv1m
    !> number of quantities, cv1m%nqty is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> polynomial degree, cv1m%pd is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: pd

    INTEGER(i4) :: new_nqty,new_pd
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_with_mold_cv1m',iftn,idepth)
    new_nqty=cv1m%nqty
    IF (PRESENT(nqty)) THEN
      new_nqty=nqty
    ENDIF
    new_pd=cv1m%pd
    IF (PRESENT(pd)) THEN
      new_pd=pd
    ENDIF
!-------------------------------------------------------------------------------
!   do the allocation
!-------------------------------------------------------------------------------
    ALLOCATE(vec_rect_2D_cv1m_acc::new_cv1m)
!-------------------------------------------------------------------------------
!   copy the eliminated flag
!-------------------------------------------------------------------------------
    new_cv1m%skip_elim_interior=cv1m%skip_elim_interior
!-------------------------------------------------------------------------------
!   call the allocation routine
!-------------------------------------------------------------------------------
    SELECT TYPE (new_cv1m)
    TYPE IS (vec_rect_2D_cv1m_acc)
      CALL new_cv1m%alloc(new_pd,cv1m%mx,cv1m%my,new_nqty,cv1m%id,cv1m%on_gpu)
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_with_mold_cv1m

!-------------------------------------------------------------------------------
!* Assign zero to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE zero_cv1m(cv1m)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'zero_cv1m',iftn,idepth)
    IF (cv1m%pd>1) THEN
      ASSOCIATE (arr=>cv1m%arr,arrh=>cv1m%arrh,arrv=>cv1m%arrv,arri=>cv1m%arri)
        IF (cv1m%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv) async(cv1m%id) if(cv1m%on_gpu)
          arr=0.0_r8
          arrh=0.0_r8
          arrv=0.0_r8
          !$acc end kernels
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri) async(cv1m%id) if(cv1m%on_gpu)
          arr=0.0_r8
          arrh=0.0_r8
          arrv=0.0_r8
          arri=0.0_r8
          !$acc end kernels
        ENDIF
      END ASSOCIATE
    ELSE
      ASSOCIATE (arr=>cv1m%arr)
        !$acc kernels present(arr) async(cv1m%id) if(cv1m%on_gpu)
        arr=0.0_r8
        !$acc end kernels
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE zero_cv1m

!-------------------------------------------------------------------------------
!* Assign a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_rvec_cv1m(cv1m,rvec,r_i)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> vector to assign
    CLASS(rvector), INTENT(IN) :: rvec
    !> 'real' for the real part of cvec, 'imag' for the imaginary part
    CHARACTER(*), INTENT(IN) :: r_i

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_rvec_cv1m',iftn,idepth)
    cv1m%skip_elim_interior=rvec%skip_elim_interior
    SELECT TYPE (rvec)
    TYPE IS (vec_rect_2D_real_acc)
      ASSOCIATE(arr=>cv1m%arr,arr2=>rvec%arr,                                   &
                arrh=>cv1m%arrh,arrh2=>rvec%arrh,                               &
                arrv=>cv1m%arrv,arrv2=>rvec%arrv,                               &
                arri=>cv1m%arri,arri2=>rvec%arri)
        SELECT CASE(r_i)
        CASE ('real','REAL')
          IF (cv1m%pd>1) THEN
            IF (cv1m%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc async(cv1m%id) if(cv1m%on_gpu)
              arr(:,:,:)=arr2(:,:,:)+(0,1)*AIMAG(arr(:,:,:))
              arrh(:,:,:,:)=arrh2(:,:,:,:)+(0,1)*AIMAG(arrh(:,:,:,:))
              arrv(:,:,:,:)=arrv2(:,:,:,:)+(0,1)*AIMAG(arrv(:,:,:,:))
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc async(cv1m%id) if(cv1m%on_gpu)
              arr(:,:,:)=arr2(:,:,:)+(0,1)*AIMAG(arr(:,:,:))
              arrh(:,:,:,:)=arrh2(:,:,:,:)+(0,1)*AIMAG(arrh(:,:,:,:))
              arrv(:,:,:,:)=arrv2(:,:,:,:)+(0,1)*AIMAG(arrv(:,:,:,:))
              arri(:,:,:,:)=arri2(:,:,:,:)+(0,1)*AIMAG(arri(:,:,:,:))
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) async(cv1m%id) if(cv1m%on_gpu)
            arr(:,:,:)=arr2(:,:,:)+(0,1)*AIMAG(arr(:,:,:))
            !$acc end kernels
          ENDIF
        CASE ('imag','IMAG')
          IF (cv1m%pd>1) THEN
            IF (cv1m%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc async(cv1m%id) if(cv1m%on_gpu)
              arr(:,:,:)=(0,1)*arr2(:,:,:)+REAL(arr(:,:,:))
              arrh(:,:,:,:)=(0,1)*arrh2(:,:,:,:)+REAL(arrh(:,:,:,:))
              arrv(:,:,:,:)=(0,1)*arrv2(:,:,:,:)+REAL(arrv(:,:,:,:))
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc async(cv1m%id) if(cv1m%on_gpu)
              arr(:,:,:)=(0,1)*arr2(:,:,:)+REAL(arr(:,:,:))
              arrh(:,:,:,:)=(0,1)*arrh2(:,:,:,:)+REAL(arrh(:,:,:,:))
              arrv(:,:,:,:)=(0,1)*arrv2(:,:,:,:)+REAL(arrv(:,:,:,:))
              arri(:,:,:,:)=(0,1)*arri2(:,:,:,:)+REAL(arri(:,:,:,:))
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) async(cv1m%id) if(cv1m%on_gpu)
            arr(:,:,:)=(0,1)*arr2(:,:,:)+REAL(arr(:,:,:))
            !$acc end kernels
          ENDIF
        CASE DEFAULT
          CALL par%nim_stop('assign_rvec_cv1m: '//r_i//' flag not recognized.')
        END SELECT
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for vec in assign_rvec_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_rvec_cv1m

!-------------------------------------------------------------------------------
!* Partially assign a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_rvec_cv1m(cv1m,rvec,r_i,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> vector to assign
    CLASS(rvector), INTENT(IN) :: rvec
    !> 'real' for the real part of cvec, 'imag' for the imaginary part
    CHARACTER(*), INTENT(IN) :: r_i
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assignp_rvec_cv1m',iftn,idepth)
    cv1m%skip_elim_interior=rvec%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (rvec)
    TYPE IS (vec_rect_2D_real_acc)
      ASSOCIATE(arr=>cv1m%arr,arr2=>rvec%arr,                                   &
                arrh=>cv1m%arrh,arrh2=>rvec%arrh,                               &
                arrv=>cv1m%arrv,arrv2=>rvec%arrv,                               &
                arri=>cv1m%arri,arri2=>rvec%arri)
        SELECT CASE(r_i)
        CASE ('real','REAL')
          IF (cv1m%pd>1) THEN
            IF (cv1m%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc async(cv1m%id) if(cv1m%on_gpu)
              arr(v1s:v1e,:,:)=arr2(v2s:v2e,:,:)+(0,1)*AIMAG(arr(v1s:v1e,:,:))
              arrh(v1s:v1e,:,:,:)=                                              &
                arrh2(v2s:v2e,:,:,:)+(0,1)*AIMAG(arrh(v1s:v1e,:,:,:))
              arrv(v1s:v1e,:,:,:)=                                              &
                arrv2(v2s:v2e,:,:,:)+(0,1)*AIMAG(arrv(v1s:v1e,:,:,:))
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc async(cv1m%id) if(cv1m%on_gpu)
              arr(v1s:v1e,:,:)=arr2(v2s:v2e,:,:)+(0,1)*AIMAG(arr(v1s:v1e,:,:))
              arrh(v1s:v1e,:,:,:)=                                              &
                arrh2(v2s:v2e,:,:,:)+(0,1)*AIMAG(arrh(v1s:v1e,:,:,:))
              arrv(v1s:v1e,:,:,:)=                                              &
                arrv2(v2s:v2e,:,:,:)+(0,1)*AIMAG(arrv(v1s:v1e,:,:,:))
              arri(v1s:v1e,:,:,:)=                                              &
                arri2(v2s:v2e,:,:,:)+(0,1)*AIMAG(arri(v1s:v1e,:,:,:))
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) async(cv1m%id) if(cv1m%on_gpu)
            arr(v1s:v1e,:,:)=arr2(v2s:v2e,:,:)+(0,1)*AIMAG(arr(v1s:v1e,:,:))
            !$acc end kernels
          ENDIF
        CASE ('imag','IMAG')
          IF (cv1m%pd>1) THEN
            IF (cv1m%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc copyin(v1s,v1e,v2s,v2e) async(cv1m%id) if(cv1m%on_gpu)
              arr(v1s:v1e,:,:)=(0,1)*arr2(v2s:v2e,:,:)+REAL(arr(v1s:v1e,:,:))
              arrh(v1s:v1e,:,:,:)=                                              &
                (0,1)*arrh2(v2s:v2e,:,:,:)+REAL(arrh(v1s:v1e,:,:,:))
              arrv(v1s:v1e,:,:,:)=                                              &
                (0,1)*arrv2(v2s:v2e,:,:,:)+REAL(arrv(v1s:v1e,:,:,:))
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc copyin(v1s,v1e,v2s,v2e) async(cv1m%id) if(cv1m%on_gpu)
              arr(v1s:v1e,:,:)=(0,1)*arr2(v2s:v2e,:,:)+REAL(arr(v1s:v1e,:,:))
              arrh(v1s:v1e,:,:,:)=                                              &
                (0,1)*arrh2(v2s:v2e,:,:,:)+REAL(arrh(v1s:v1e,:,:,:))
              arrv(v1s:v1e,:,:,:)=                                              &
                (0,1)*arrv2(v2s:v2e,:,:,:)+REAL(arrv(v1s:v1e,:,:,:))
              arri(v1s:v1e,:,:,:)=                                              &
                (0,1)*arri2(v2s:v2e,:,:,:)+REAL(arri(v1s:v1e,:,:,:))
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) copyin(v1s,v1e,v2s,v2e)             &
            !$acc async(cv1m%id) if(cv1m%on_gpu)
            arr(v1s:v1e,:,:)=(0,1)*arr2(v2s:v2e,:,:)+REAL(arr(v1s:v1e,:,:))
            !$acc end kernels
          ENDIF
        CASE DEFAULT
          CALL par%nim_stop('assignp_rvec_cv1m: '//r_i//' flag not recognized.')
        END SELECT
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for vec in'//                &
                        ' assignp_rvec_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_rvec_cv1m

!-------------------------------------------------------------------------------
!* Assign a cvector1m to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_cv1m_cv1m(cv1m,cv1m2)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> vector to assign
    CLASS(cvector1m), INTENT(IN) :: cv1m2

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_cv1m_cv1m',iftn,idepth)
    cv1m%skip_elim_interior=cv1m2%skip_elim_interior
    SELECT TYPE (cv1m2)
    TYPE IS (vec_rect_2D_cv1m_acc)
      ASSOCIATE(arr=>cv1m%arr,arr2=>cv1m2%arr,                                  &
                arrh=>cv1m%arrh,arrh2=>cv1m2%arrh,                              &
                arrv=>cv1m%arrv,arrv2=>cv1m2%arrv,                              &
                arri=>cv1m%arri,arri2=>cv1m2%arri)
        IF (cv1m%pd>1) THEN
          IF (cv1m%skip_elim_interior) THEN
            !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)               &
            !$acc async(cv1m%id) if(cv1m%on_gpu)
            arr=arr2
            arrh=arrh2
            arrv=arrv2
            !$acc end kernels
          ELSE
            !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)    &
            !$acc async(cv1m%id) if(cv1m%on_gpu)
            arr=arr2
            arrh=arrh2
            arrv=arrv2
            arri=arri2
            !$acc end kernels
          ENDIF
        ELSE
          !$acc kernels present(arr,arr2) async(cv1m%id) if(cv1m%on_gpu)
          arr=arr2
          !$acc end kernels
        ENDIF
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for cv1m2 in'//              &
                        ' assign_cv1m_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_cv1m_cv1m

!-------------------------------------------------------------------------------
!* Partially assign a cvector1m to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_cv1m_cv1m(cv1m,cv1m2,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> vector to assign
    CLASS(cvector1m), INTENT(IN) :: cv1m2
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_real',iftn,idepth)
    cv1m%skip_elim_interior=cv1m2%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (cv1m2)
    TYPE IS (vec_rect_2D_cv1m_acc)
      ASSOCIATE(arr=>cv1m%arr,arr2=>cv1m2%arr,                                  &
                arrh=>cv1m%arrh,arrh2=>cv1m2%arrh,                              &
                arrv=>cv1m%arrv,arrv2=>cv1m2%arrv,                              &
                arri=>cv1m%arri,arri2=>cv1m2%arri)
        IF (cv1m%pd>1) THEN
          IF (cv1m%skip_elim_interior) THEN
            !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)               &
            !$acc copyin(v1s,v1e,v2s,v2e) async(cv1m%id) if(cv1m%on_gpu)
            arr(v1s:v1e,:,:)=arr2(v2s:v2e,:,:)
            arrh(v1s:v1e,:,:,:)=arrh2(v2s:v2e,:,:,:)
            arrv(v1s:v1e,:,:,:)=arrv2(v2s:v2e,:,:,:)
            !$acc end kernels
          ELSE
            !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)    &
            !$acc copyin(v1s,v1e,v2s,v2e) async(cv1m%id) if(cv1m%on_gpu)
            arr(v1s:v1e,:,:)=arr2(v2s:v2e,:,:)
            arrh(v1s:v1e,:,:,:)=arrh2(v2s:v2e,:,:,:)
            arrv(v1s:v1e,:,:,:)=arrv2(v2s:v2e,:,:,:)
            arri(v1s:v1e,:,:,:)=arri2(v2s:v2e,:,:,:)
            !$acc end kernels
          ENDIF
        ELSE
          !$acc kernels present(arr,arr2) copyin(v1s,v1e,v2s,v2e)               &
          !$acc async(cv1m%id) if(cv1m%on_gpu)
          arr(v1s:v1e,:,:)=arr2(v2s:v2e,:,:)
          !$acc end kernels
        ENDIF
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for cv1m2 in'//              &
                        ' assignp_cv1m_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_cv1m_cv1m

!-------------------------------------------------------------------------------
!* Assign a cvector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_cvec_cv1m(cv1m,cvec,imode)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> vector to assign
    CLASS(cvector), INTENT(IN) :: cvec
    !> mode to assign
    INTEGER(i4), INTENT(IN) :: imode

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_cvec_cv1m',iftn,idepth)
    cv1m%skip_elim_interior=cvec%skip_elim_interior
    SELECT TYPE (cvec)
    TYPE IS (vec_rect_2D_comp_acc)
      ASSOCIATE(arr=>cv1m%arr,arr2=>cvec%arr,                                   &
                arrh=>cv1m%arrh,arrh2=>cvec%arrh,                               &
                arrv=>cv1m%arrv,arrv2=>cvec%arrv,                               &
                arri=>cv1m%arri,arri2=>cvec%arri)
        IF (cv1m%pd>1) THEN
          IF (cv1m%skip_elim_interior) THEN
            !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)               &
            !$acc copyin(imode) async(cv1m%id) if(cv1m%on_gpu)
            arr(:,:,:)=arr2(:,:,:,imode)
            arrh(:,:,:,:)=arrh2(:,:,:,:,imode)
            arrv(:,:,:,:)=arrv2(:,:,:,:,imode)
            !$acc end kernels
          ELSE
            !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)    &
            !$acc copyin(imode) async(cv1m%id) if(cv1m%on_gpu)
            arr(:,:,:)=arr2(:,:,:,imode)
            arrh(:,:,:,:)=arrh2(:,:,:,:,imode)
            arrv(:,:,:,:)=arrv2(:,:,:,:,imode)
            arri(:,:,:,:)=arri2(:,:,:,:,imode)
            !$acc end kernels
          ENDIF
        ELSE
          !$acc kernels present(arr,arr2) copyin(imode) async(cv1m%id) if(cv1m%on_gpu)
          arr(:,:,:)=arr2(:,:,:,imode)
          !$acc end kernels
        ENDIF
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for cvec in'//               &
                        ' assign_cvec_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_cvec_cv1m

!-------------------------------------------------------------------------------
!* Partially assign a cvector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_cvec_cv1m(cv1m,cvec,imode,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> vector to assign
    CLASS(cvector), INTENT(IN) :: cvec
    !> mode to assign
    INTEGER(i4), INTENT(IN) :: imode
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assignp_cvec_cv1m',iftn,idepth)
    cv1m%skip_elim_interior=cvec%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (cvec)
    TYPE IS (vec_rect_2D_comp_acc)
      ASSOCIATE(arr=>cv1m%arr,arr2=>cvec%arr,                                   &
                arrh=>cv1m%arrh,arrh2=>cvec%arrh,                               &
                arrv=>cv1m%arrv,arrv2=>cvec%arrv,                               &
                arri=>cv1m%arri,arri2=>cvec%arri)
        IF (cv1m%pd>1) THEN
          IF (cv1m%skip_elim_interior) THEN
            !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)               &
            !$acc copyin(v1s,v1e,v2s,v2e,imode) async(cv1m%id) if(cv1m%on_gpu)
            arr(v1s:v1e,:,:)=arr2(v2s:v2e,:,:,imode)
            arrh(v1s:v1e,:,:,:)=arrh2(v2s:v2e,:,:,:,imode)
            arrv(v1s:v1e,:,:,:)=arrv2(v2s:v2e,:,:,:,imode)
            !$acc end kernels
          ELSE
            !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)    &
            !$acc copyin(v1s,v1e,v2s,v2e,imode) async(cv1m%id) if(cv1m%on_gpu)
            arr(v1s:v1e,:,:)=arr2(v2s:v2e,:,:,imode)
            arrh(v1s:v1e,:,:,:)=arrh2(v2s:v2e,:,:,:,imode)
            arrv(v1s:v1e,:,:,:)=arrv2(v2s:v2e,:,:,:,imode)
            arri(v1s:v1e,:,:,:)=arri2(v2s:v2e,:,:,:,imode)
            !$acc end kernels
          ENDIF
        ELSE
          !$acc kernels present(arr,arr2) copyin(v1s,v1e,v2s,v2e,imode)         &
          !$acc async(cv1m%id) if(cv1m%on_gpu)
          arr(v1s:v1e,:,:)=arr2(v2s:v2e,:,:,imode)
          !$acc end kernels
        ENDIF
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for cvec in'//               &
                        ' assignp_cvec_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_cvec_cv1m

!-------------------------------------------------------------------------------
!* add a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE add_vec_cv1m(cv1m,cv1m2,v1st,v2st,nq,v1fac,v2fac)
    USE convert_type
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> vector to assign
    CLASS(cvector1m), INTENT(IN) :: cv1m2
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1 when v1st or v2st is set
    !> default is all when neither v1st or v2st is set)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
    !> factor to multiply vec by (default 1)
    CLASS(*), OPTIONAL, INTENT(IN) :: v1fac
    !> factor to multiply vec2 by (default 1)
    CLASS(*), OPTIONAL, INTENT(IN) :: v2fac

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    COMPLEX(r8) :: v1f,v2f
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'add_vec_cv1m',iftn,idepth)
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    IF (PRESENT(v1fac)) THEN
      v1f=convert_to_cmplx_r8(v1fac)
    ELSE
      v1f=CMPLX(1._r8,0._r8,r8)
    ENDIF
    IF (PRESENT(v2fac)) THEN
      v2f=convert_to_cmplx_r8(v2fac)
    ELSE
      v2f=CMPLX(1._r8,0._r8,r8)
    ENDIF
    SELECT TYPE (cv1m2)
    TYPE IS (vec_rect_2D_cv1m_acc)
      ASSOCIATE(arr=>cv1m%arr,arr2=>cv1m2%arr,                                  &
                arrh=>cv1m%arrh,arrh2=>cv1m2%arrh,                              &
                arrv=>cv1m%arrv,arrv2=>cv1m2%arrv,                              &
                arri=>cv1m%arri,arri2=>cv1m2%arri)
        IF (.NOT.PRESENT(v1st).AND..NOT.PRESENT(v2st).AND..NOT.PRESENT(nq)) THEN
          IF (cv1m%pd>1) THEN
            IF (cv1m%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc copyin(v1f,v2f) async(cv1m%id) if(cv1m%on_gpu)
              arr=v1f*arr+v2f*arr2
              arrh=v1f*arrh+v2f*arrh2
              arrv=v1f*arrv+v2f*arrv2
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc copyin(v1f,v2f) async(cv1m%id) if(cv1m%on_gpu)
              arr=v1f*arr+v2f*arr2
              arrh=v1f*arrh+v2f*arrh2
              arrv=v1f*arrv+v2f*arrv2
              arri=v1f*arri+v2f*arri2
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) copyin(v1f,v2f)                     &
            !$acc async(cv1m%id) if(cv1m%on_gpu)
            arr=v1f*arr+v2f*arr2
            !$acc end kernels
          ENDIF
        ELSE
          IF (cv1m%pd>1) THEN
            IF (cv1m%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc copyin(v1f,v2f,v1s,v1e,v2s,v2e) async(cv1m%id) if(cv1m%on_gpu)
              arr(v1s:v1e,:,:)=v1f*arr(v1s:v1e,:,:)+v2f*arr2(v2s:v2e,:,:)
              arrh(v1s:v1e,:,:,:)=                                              &
                v1f*arrh(v1s:v1e,:,:,:)+v2f*arrh2(v2s:v2e,:,:,:)
              arrv(v1s:v1e,:,:,:)=                                              &
                v1f*arrv(v1s:v1e,:,:,:)+v2f*arrv2(v2s:v2e,:,:,:)
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc copyin(v1f,v2f,v1s,v1e,v2s,v2e) async(cv1m%id) if(cv1m%on_gpu)
              arr(v1s:v1e,:,:)=v1f*arr(v1s:v1e,:,:)+v2f*arr2(v2s:v2e,:,:)
              arrh(v1s:v1e,:,:,:)=                                              &
                v1f*arrh(v1s:v1e,:,:,:)+v2f*arrh2(v2s:v2e,:,:,:)
              arrv(v1s:v1e,:,:,:)=                                              &
                v1f*arrv(v1s:v1e,:,:,:)+v2f*arrv2(v2s:v2e,:,:,:)
              arri(v1s:v1e,:,:,:)=                                              &
                v1f*arri(v1s:v1e,:,:,:)+v2f*arri2(v2s:v2e,:,:,:)
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) copyin(v1f,v2f,v1s,v1e,v2s,v2e)     &
            !$acc async(cv1m%id) if(cv1m%on_gpu)
            arr(v1s:v1e,:,:)=v1f*arr(v1s:v1e,:,:)+v2f*arr2(v2s:v2e,:,:)
            !$acc end kernels
          ENDIF
        ENDIF
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for cvec in add_vec_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE add_vec_cv1m

!-------------------------------------------------------------------------------
!* multiply a vector by a real scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_rsc_cv1m(cv1m,rscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> scalar to multiply
    REAL(r8), INTENT(IN) :: rscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_rsc_cv1m',iftn,idepth)
    ASSOCIATE(arr=>cv1m%arr,arrh=>cv1m%arrh,                                    &
              arrv=>cv1m%arrv,arri=>cv1m%arri)
      IF (cv1m%pd>1) THEN
        IF (cv1m%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv)                                  &
          !$acc copyin(rscalar) async(cv1m%id) if(cv1m%on_gpu)
          arr=arr*rscalar
          arrh=arrh*rscalar
          arrv=arrv*rscalar
          !$acc end kernels
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri)                             &
          !$acc copyin(rscalar) async(cv1m%id) if(cv1m%on_gpu)
          arr=arr*rscalar
          arrh=arrh*rscalar
          arrv=arrv*rscalar
          arri=arri*rscalar
          !$acc end kernels
        ENDIF
      ELSE
        !$acc kernels present(arr) copyin(rscalar) async(cv1m%id) if(cv1m%on_gpu)
        arr=arr*rscalar
        !$acc end kernels
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_rsc_cv1m

!-------------------------------------------------------------------------------
!* multiply a vector by a complex scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_csc_cv1m(cv1m,cscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> scalar to multiply
    COMPLEX(r8), INTENT(IN) :: cscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_csc_cv1m',iftn,idepth)
    ASSOCIATE(arr=>cv1m%arr,arrh=>cv1m%arrh,                                    &
              arrv=>cv1m%arrv,arri=>cv1m%arri)
      IF (cv1m%pd>1) THEN
        IF (cv1m%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv)                                  &
          !$acc copyin(cscalar) async(cv1m%id) if(cv1m%on_gpu)
          arr=arr*cscalar
          arrh=arrh*cscalar
          arrv=arrv*cscalar
          !$acc end kernels
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri)                             &
          !$acc copyin(cscalar) async(cv1m%id) if(cv1m%on_gpu)
          arr=arr*cscalar
          arrh=arrh*cscalar
          arrv=arrv*cscalar
          arri=arri*cscalar
          !$acc end kernels
        ENDIF
      ELSE
        !$acc kernels present(arr) copyin(cscalar) async(cv1m%id) if(cv1m%on_gpu)
        arr=arr*cscalar
        !$acc end kernels
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_csc_cv1m

!-------------------------------------------------------------------------------
!* multiply a vector by a integer scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_int_cv1m(cv1m,iscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> scalar to multiply
    INTEGER(i4), INTENT(IN) :: iscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_int_cv1m',iftn,idepth)
    ASSOCIATE(arr=>cv1m%arr,arrh=>cv1m%arrh,                                    &
              arrv=>cv1m%arrv,arri=>cv1m%arri)
      IF (cv1m%pd>1) THEN
        IF (cv1m%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv)                                  &
          !$acc copyin(iscalar) async(cv1m%id) if(cv1m%on_gpu)
          arr=arr*iscalar
          arrh=arrh*iscalar
          arrv=arrv*iscalar
          !$acc end kernels
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri)                             &
          !$acc copyin(iscalar) async(cv1m%id) if(cv1m%on_gpu)
          arr=arr*iscalar
          arrh=arrh*iscalar
          arrv=arrv*iscalar
          arri=arri*iscalar
          !$acc end kernels
        ENDIF
      ELSE
        !$acc kernels present(arr) copyin(iscalar) async(cv1m%id) if(cv1m%on_gpu)
        arr=arr*iscalar
        !$acc end kernels
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_int_cv1m

!-------------------------------------------------------------------------------
!* compute the inf norm
!-------------------------------------------------------------------------------
  SUBROUTINE inf_norm_cv1m(cv1m,infnorm)
    USE local
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_cv1m_acc), INTENT(IN) :: cv1m
    !> inf norm contributions from the vector (size vec%inf_norm_size)
    REAL(r8), INTENT(INOUT) :: infnorm(:)

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'inf_norm_cv1m',iftn,idepth)
    ASSOCIATE (arr=>cv1m%arr,arrh=>cv1m%arrh,                                   &
               arrv=>cv1m%arrv,arri=>cv1m%arri)
      IF (cv1m%pd>1) THEN
        IF (cv1m%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv) copyout(infnorm(1:3))            &
          !$acc async(cv1m%id) if(cv1m%on_gpu)
          infnorm(1)=MAXVAL(ABS(arr))
          infnorm(2)=MAXVAL(ABS(arrh))
          infnorm(3)=MAXVAL(ABS(arrv))
          !$acc end kernels
          infnorm(4)=0._r8
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri) copyout(infnorm(1:4))       &
          !$acc async(cv1m%id) if(cv1m%on_gpu)
          infnorm(1)=MAXVAL(ABS(arr))
          infnorm(2)=MAXVAL(ABS(arrh))
          infnorm(3)=MAXVAL(ABS(arrv))
          infnorm(4)=MAXVAL(ABS(arri))
          !$acc end kernels
        ENDIF
      ELSE
        !$acc kernels present(arr) copyout(infnorm(1))                          &
        !$acc async(cv1m%id) if(cv1m%on_gpu)
        infnorm(1)=MAXVAL(ABS(arr))
        !$acc end kernels
        infnorm(2:4)=0._r8
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE inf_norm_cv1m

!-------------------------------------------------------------------------------
!* compute the L2 norm squared
!-------------------------------------------------------------------------------
  SUBROUTINE l2_norm2_cv1m(cv1m,l2norm,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_cv1m_acc), INTENT(IN) :: cv1m
    !> L2 norm contributions from the vector (size vec%l2_dot_size)
    REAL(r8), INTENT(INOUT) :: l2norm(:)
    !> edge data to weight edge value contribution
    TYPE(edge_type), INTENT(IN) :: edge

    INTEGER(i4) :: ix,iy,iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'l2_norm2_cv1m',iftn,idepth)
    ASSOCIATE (arr=>cv1m%arr,arrh=>cv1m%arrh,                                   &
               arrv=>cv1m%arrv,arri=>cv1m%arri,                                 &
               vertex=>edge%vertex,segment=>edge%segment)
      IF (cv1m%pd>1) THEN
        IF (cv1m%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv) copyout(l2norm(1:3))             &
          !$acc async(cv1m%id) if(cv1m%on_gpu)
          l2norm(1)=SUM(REAL(CONJG(arr)*arr))
          l2norm(2)=SUM(REAL(CONJG(arrh)*arrh))
          l2norm(3)=SUM(REAL(CONJG(arrv)*arrv))
          !$acc end kernels
          l2norm(4)=0._r8
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri) copyout(l2norm(1:4))        &
          !$acc async(cv1m%id) if(cv1m%on_gpu)
          l2norm(1)=SUM(REAL(CONJG(arr)*arr))
          l2norm(2)=SUM(REAL(CONJG(arrh)*arrh))
          l2norm(3)=SUM(REAL(CONJG(arrv)*arrv))
          l2norm(4)=SUM(REAL(CONJG(arri)*arri))
          !$acc end kernels
        ENDIF
!-------------------------------------------------------------------------------
!       boundary points must be divided by the number of internal
!       representations to get the correct sum over all blocks.
!-------------------------------------------------------------------------------
        !$acc parallel present(arr,arrh,arrv,edge,vertex,segment)               &
        !$acc copy(l2norm(5)) reduction(+:l2norm(5)) async(cv1m%id) if(cv1m%on_gpu)
        !$acc loop gang vector private(ix,iy)
        DO iv=1,edge%nvert
          ix=vertex(iv)%intxy(1)
          iy=vertex(iv)%intxy(2)
          l2norm(5)=l2norm(5)+(vertex(iv)%ave_factor-1._r8)*                    &
                              SUM(REAL(CONJG(arr(:,ix,iy))*arr(:,ix,iy)))
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          IF (segment(iv)%h_side) THEN
            l2norm(5)=l2norm(5)+(segment(iv)%ave_factor-1._r8)*                 &
                           SUM(REAL(CONJG(arrh(:,:,ix,iy))*arrh(:,:,ix,iy)))
          ELSE
            l2norm(5)=l2norm(5)+(segment(iv)%ave_factor-1._r8)*                 &
                           SUM(REAL(CONJG(arrv(:,:,ix,iy))*arrv(:,:,ix,iy)))
          ENDIF
        ENDDO
        !$acc end parallel
      ELSE
        !$acc kernels present(arr) copyout(l2norm(1))                           &
        !$acc async(cv1m%id) if(cv1m%on_gpu)
        l2norm(1)=SUM(REAL(CONJG(arr)*arr))
        !$acc end kernels
        !$acc parallel present(arr,edge,vertex) copy(l2norm(2))                 &
        !$acc reduction(+:l2norm(2)) async(cv1m%id) if(cv1m%on_gpu)
        !$acc loop gang vector private(ix,iy)
        DO iv=1,edge%nvert
          ix=vertex(iv)%intxy(1)
          iy=vertex(iv)%intxy(2)
          l2norm(2)=l2norm(2)+(vertex(iv)%ave_factor-1._r8)*                    &
                              SUM(REAL(CONJG(arr(:,ix,iy))*arr(:,ix,iy)))
        ENDDO
        !$acc end parallel
        l2norm(3:5)=0._r8
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE l2_norm2_cv1m

!-------------------------------------------------------------------------------
!*  compute the dot product of cv1m and cv1m2
!-------------------------------------------------------------------------------
  SUBROUTINE dot_cv1m(cv1m,cv1m2,dot,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_cv1m_acc), INTENT(IN) :: cv1m
    !> vector to dot
    CLASS(cvector1m), INTENT(IN) :: cv1m2
    !> dot contributions from the vector (size vec%l2_dot_size)
    COMPLEX(r8), INTENT(INOUT) :: dot(:)
    !> edge data to weight edge value contribution
    TYPE(edge_type), INTENT(IN) :: edge

    INTEGER(i4) :: ix,iy,iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dot_cv1m',iftn,idepth)
    SELECT TYPE (cv1m2)
    TYPE IS (vec_rect_2D_cv1m_acc)
      ASSOCIATE (arr=>cv1m%arr,arrh=>cv1m%arrh,                                 &
                 arrv=>cv1m%arrv,arri=>cv1m%arri,                               &
                 arr2=>cv1m2%arr,arrh2=>cv1m2%arrh,                             &
                 arrv2=>cv1m2%arrv,arri2=>cv1m2%arri,                           &
                 vertex=>edge%vertex,segment=>edge%segment)
        IF (cv1m%pd>1) THEN
          IF (cv1m%skip_elim_interior) THEN
            !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)               &
            !$acc copyout(dot(1:3)) async(cv1m%id) if(cv1m%on_gpu)
            dot(1)=SUM(CONJG(arr)*arr2)
            dot(2)=SUM(CONJG(arrh)*arrh2)
            dot(3)=SUM(CONJG(arrv)*arrv2)
            !$acc end kernels
            dot(4)=0._r8
          ELSE
            !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)    &
            !$acc copyout(dot(1:4)) async(cv1m%id) if(cv1m%on_gpu)
            dot(1)=SUM(CONJG(arr)*arr2)
            dot(2)=SUM(CONJG(arrh)*arrh2)
            dot(3)=SUM(CONJG(arrv)*arrv2)
            dot(4)=SUM(CONJG(arri)*arri2)
            !$acc end kernels
          ENDIF
!-------------------------------------------------------------------------------
!         boundary points must be divided by the number of internal
!         representations to get the correct sum over all blocks.
!-------------------------------------------------------------------------------
          !$acc parallel present(arr,arrh,arrv,arr2,arrh2,arrv2)                &
          !$acc present(edge,vertex,segment) copy(dot(5))                       &
          !$acc reduction(+:dot(5)) async(cv1m%id) if(cv1m%on_gpu)
          !$acc loop gang vector private(ix,iy)
          DO iv=1,edge%nvert
            ix=vertex(iv)%intxy(1)
            iy=vertex(iv)%intxy(2)
            dot(5)=dot(5)+(vertex(iv)%ave_factor-1._r8)*                        &
                                SUM(CONJG(arr(:,ix,iy))*arr2(:,ix,iy))
            ix=segment(iv)%intxys(1)
            iy=segment(iv)%intxys(2)
            IF (segment(iv)%h_side) THEN
              dot(5)=dot(5)+(segment(iv)%ave_factor-1._r8)*                     &
                             SUM(CONJG(arrh(:,:,ix,iy))*arrh2(:,:,ix,iy))
            ELSE
              dot(5)=dot(5)+(segment(iv)%ave_factor-1._r8)*                     &
                             SUM(CONJG(arrv(:,:,ix,iy))*arrv2(:,:,ix,iy))
            ENDIF
          ENDDO
          !$acc end parallel
        ELSE
          !$acc kernels present(arr) copyout(dot(1))                            &
          !$acc async(cv1m%id) if(cv1m%on_gpu)
          dot(1)=SUM(CONJG(arr)*arr2)
          !$acc end kernels
          !$acc parallel present(arr,edge,vertex) copy(dot(2))                  &
          !$acc reduction(+:dot(2)) async(cv1m%id) if(cv1m%on_gpu)
          !$acc loop gang vector private(ix,iy)
          DO iv=1,edge%nvert
            ix=vertex(iv)%intxy(1)
            iy=vertex(iv)%intxy(2)
            dot(2)=dot(2)+(vertex(iv)%ave_factor-1._r8)*                        &
                                SUM(CONJG(arr(:,ix,iy))*arr2(:,ix,iy))
          ENDDO
          !$acc end parallel
          dot(3:5)=0._r8
        ENDIF
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for vec2 in dot_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dot_cv1m

!-------------------------------------------------------------------------------
!> transfer contributions from integrand into internal storage as a plus
!  equals operation (the user must call %zero as needed).
!-------------------------------------------------------------------------------
  SUBROUTINE assemble_cv1m(cv1m,integrand)
    USE local
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> integrand array
    COMPLEX(r8), INTENT(IN) :: integrand(:,:,:)

    INTEGER(i4) :: iq,iel,iv,iy,ix,is,ii,start_horz,start_vert,start_int,id
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assemble_cv1m',iftn,idepth)
    start_horz=5
    start_vert=5+2*cv1m%n_side
    start_int=5+4*cv1m%n_side
!-------------------------------------------------------------------------------
!   assemble and accumulate the contributions from each element into the
!   correct arrays. factors of Jacobian and quadrature weight are already in
!   the test function arrays included in the integrand.
!-------------------------------------------------------------------------------
    IF (ASSOCIATED(cv1m%arr)) THEN
      ASSOCIATE (arr=>cv1m%arr)
        id=par%get_stream(cv1m%id,1,4)
        !$acc parallel present(arr,cv1m,integrand)                              &
        !$acc wait(cv1m%id) async(id) if(cv1m%on_gpu)
        !$acc loop gang
        DO iq=1,cv1m%nqty
          !$acc loop vector collapse(2) private(iel)
          DO iy=0,cv1m%my-1
            DO ix=0,cv1m%mx-1
              iel=1+ix+iy*cv1m%mx
              arr(iq,ix,iy)=arr(iq,ix,iy)+integrand(iq,iel,1)
            ENDDO
          ENDDO
          !$acc loop vector collapse(2) private(iel)
          DO iy=0,cv1m%my-1
            DO ix=0,cv1m%mx-1
              iel=1+ix+iy*cv1m%mx
              arr(iq,ix+1,iy)=arr(iq,ix+1,iy)+integrand(iq,iel,2)
            ENDDO
          ENDDO
          !$acc loop vector collapse(2) private(iel)
          DO iy=0,cv1m%my-1
            DO ix=0,cv1m%mx-1
              iel=1+ix+iy*cv1m%mx
              arr(iq,ix,iy+1)=arr(iq,ix,iy+1)+integrand(iq,iel,3)
            ENDDO
          ENDDO
          !$acc loop vector collapse(2) private(iel)
          DO iy=0,cv1m%my-1
            DO ix=0,cv1m%mx-1
              iel=1+ix+iy*cv1m%mx
              arr(iq,ix+1,iy+1)=arr(iq,ix+1,iy+1)+integrand(iq,iel,4)
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cv1m%arrh)) THEN
      ASSOCIATE (arrh=>cv1m%arrh)
        id=par%get_stream(cv1m%id,2,4)
        !$acc parallel present(arrh,cv1m,integrand)                             &
        !$acc copyin(start_horz) wait(cv1m%id) async(id) if(cv1m%on_gpu)
        !$acc loop gang collapse(2) private(iv)
        DO iq=1,cv1m%nqty
          DO is=1,cv1m%n_side
            iv=start_horz+2*(is-1)
            !$acc loop vector collapse(2) private(iel)
            DO iy=0,cv1m%my-1
              DO ix=1,cv1m%mx
                iel=ix+iy*cv1m%mx
                arrh(iq,is,ix,iy)=arrh(iq,is,ix,iy)+integrand(iq,iel,iv)
              ENDDO
            ENDDO
            !$acc loop vector collapse(2) private(iel)
            DO iy=0,cv1m%my-1
              DO ix=1,cv1m%mx
                iel=ix+iy*cv1m%mx
                arrh(iq,is,ix,iy+1)=arrh(iq,is,ix,iy+1)+integrand(iq,iel,iv+1)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cv1m%arrv)) THEN
      ASSOCIATE (arrv=>cv1m%arrv)
        id=par%get_stream(cv1m%id,3,4)
        !$acc parallel present(arrv,cv1m,integrand)                             &
        !$acc copyin(start_vert) wait(cv1m%id) async(id) if(cv1m%on_gpu)
        !$acc loop gang collapse(2) private(iv)
        DO iq=1,cv1m%nqty
          DO is=1,cv1m%n_side
            iv=start_vert+2*(is-1)
            !$acc loop vector collapse(2) private(iel)
            DO iy=1,cv1m%my
              DO ix=0,cv1m%mx-1
                iel=1+ix+(iy-1)*cv1m%mx
                arrv(iq,is,ix,iy)=arrv(iq,is,ix,iy)+integrand(iq,iel,iv)
              ENDDO
            ENDDO
            !$acc loop vector collapse(2) private(iel)
            DO iy=1,cv1m%my
              DO ix=0,cv1m%mx-1
                iel=1+ix+(iy-1)*cv1m%mx
                arrv(iq,is,ix+1,iy)=arrv(iq,is,ix+1,iy)+integrand(iq,iel,iv+1)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(cv1m%arri)) THEN
      ASSOCIATE (arri=>cv1m%arri)
        id=par%get_stream(cv1m%id,4,4)
        !$acc parallel present(arri,cv1m,integrand)                             &
        !$acc copyin(start_int) wait(cv1m%id) async(id) if(cv1m%on_gpu)
        !$acc loop gang collapse(2) private(iv)
        DO iq=1,cv1m%nqty
          DO ii=1,cv1m%n_int
            iv=start_int+ii-1
            !$acc loop vector collapse(2) private(iel)
            DO iy=1,cv1m%my
              DO ix=1,cv1m%mx
                iel=ix+(iy-1)*cv1m%mx
                arri(iq,ii,ix,iy)=arri(iq,ii,ix,iy)+integrand(iq,iel,iv)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    ENDIF
    IF (cv1m%on_gpu) CALL par%wait_streams(cv1m%id,4)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assemble_cv1m

!-------------------------------------------------------------------------------
!*  Applies dirichlet boundary conditions
!-------------------------------------------------------------------------------
  SUBROUTINE dirichlet_bc_cv1m(cv1m,component,edge,symm,seam_save)
    USE boundary_ftns_mod
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> flag to determine normal/tangential/scalar behavior
    CHARACTER(*), INTENT(IN) :: component
    !> associated edge
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> flag for symmetric boundary
    CHARACTER(*), OPTIONAL, INTENT(IN) :: symm
    !> save the existing boundary data in seam_save
    LOGICAL, OPTIONAL, INTENT(IN) :: seam_save

    COMPLEX(r8) :: proj
    REAL(r8), DIMENSION(cv1m%nqty,cv1m%nqty,edge%nvert) :: bcpmat
    REAL(r8), DIMENSION(cv1m%nqty,cv1m%nqty,cv1m%n_side,edge%nvert) :: bcpmats
    INTEGER(i4), DIMENSION(cv1m%nqty,2) :: bciarr
    INTEGER(i4) :: iv,ix,iy,is,isymm,iq1,iq2
    LOGICAL :: seam_save_loc,do_segment
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dirichlet_bc_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   set the symmetry flag.  if symm starts with "t", the top boundary
!   is a symmetry condition.  if symm starts with "b", the bottom
!   boundary is a symmetry condition.
!-------------------------------------------------------------------------------
    isymm=0
    IF (PRESENT(symm)) THEN
      SELECT CASE(symm(1:1))
      CASE('t','T')
        isymm=1
      CASE('b','B')
        isymm=-1
      END SELECT
    ENDIF
    IF (PRESENT(seam_save)) THEN
      seam_save_loc=seam_save
    ELSE
      seam_save_loc=.FALSE.
    ENDIF
!-------------------------------------------------------------------------------
!   parse the component flag to create an integer array, which
!   indicates which scalar and 3-vector components have essential
!   conditions.
!-------------------------------------------------------------------------------
    CALL bcflag_parse(component,cv1m%nqty,bciarr)
!-------------------------------------------------------------------------------
!   the bcdir_set routine combines the bciarr information with local
!   surface-normal and tangential unit directions.  mesh vertices
!   are treated first.
!-------------------------------------------------------------------------------
    do_segment=.FALSE.
    IF (ASSOCIATED(cv1m%arrh).OR.ASSOCIATED(cv1m%arrv)) do_segment=.TRUE.
    DO iv=1,edge%nvert
      IF (edge%expoint(iv)) THEN
        ix=edge%vertex(iv)%intxy(1)
        iy=edge%vertex(iv)%intxy(2)
        CALL bcdir_set(cv1m%nqty,bciarr,edge%excorner(iv),isymm,                &
                       edge%vertex(iv)%norm,edge%vertex(iv)%tang,bcpmat(:,:,iv))
      ENDIF
      IF (do_segment) THEN
        IF (edge%exsegment(iv)) THEN
          DO is=1,cv1m%n_side
            CALL bcdir_set(cv1m%nqty,bciarr,.false.,isymm,                      &
                           edge%segment(iv)%norm(:,is),                         &
                           edge%segment(iv)%tang(:,is),bcpmats(:,:,is,iv))
          ENDDO
        ENDIF
      ENDIF
    ENDDO
    ASSOCIATE (arr=>cv1m%arr,arrh=>cv1m%arrh,arrv=>cv1m%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               expoint=>edge%expoint,exsegment=>edge%exsegment,                 &
               vert_csave=>edge%vert_csave,seg_csave=>edge%seg_csave)
      !$acc parallel present(arr,arrh,arrv,cv1m,edge,vertex,segment)            &
      !$acc present(expoint,exsegment,vert_csave,seg_csave)                     &
      !$acc copyin(bcpmat,bcpmats,seam_save_loc,do_segment)                     &
      !$acc async(cv1m%id) if(cv1m%on_gpu)
      !$acc loop gang private(ix,iy)
      DO iv=1,edge%nvert
        IF (expoint(iv)) THEN
          ix=vertex(iv)%intxy(1)
          iy=vertex(iv)%intxy(2)
          !$acc loop vector private(proj)
          DO iq1=1,cv1m%nqty
            proj=0.0
            !$acc loop seq
            DO iq2=1,cv1m%nqty
              proj=proj+bcpmat(iq1,iq2,iv)*arr(iq2,ix,iy)
            ENDDO
            arr(iq1,ix,iy)=arr(iq1,ix,iy)-proj
            IF (seam_save_loc) vert_csave(iq1,1,iv)=vert_csave(iq1,1,iv)+proj
          ENDDO
        ENDIF
        IF (do_segment) THEN
          IF (exsegment(iv)) THEN
            ix=segment(iv)%intxys(1)
            iy=segment(iv)%intxys(2)
            IF (segment(iv)%h_side) THEN
              !$acc loop vector collapse(2) private(proj)
              DO is=1,cv1m%n_side
                DO iq1=1,cv1m%nqty
                  proj=0.0
                  !$acc loop seq
                  DO iq2=1,cv1m%nqty
                    proj=proj+bcpmats(iq1,iq2,is,iv)*arrh(iq2,is,ix,iy)
                  ENDDO
                  arrh(iq1,is,ix,iy)=arrh(iq1,is,ix,iy)-proj
                  IF (seam_save_loc)                                            &
                    seg_csave(iq1,is,1,iv)=seg_csave(iq1,is,1,iv)+proj
                ENDDO
              ENDDO
            ELSE
              !$acc loop vector collapse(2) private(proj)
              DO is=1,cv1m%n_side
                DO iq1=1,cv1m%nqty
                  proj=0.0
                  !$acc loop seq
                  DO iq2=1,cv1m%nqty
                    proj=proj+bcpmats(iq1,iq2,is,iv)*arrv(iq2,is,ix,iy)
                  ENDDO
                  arrv(iq1,is,ix,iy)=arrv(iq1,is,ix,iy)-proj
                  IF (seam_save_loc)                                            &
                    seg_csave(iq1,is,1,iv)=seg_csave(iq1,is,1,iv)+proj
                ENDDO
              ENDDO
            ENDIF
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dirichlet_bc_cv1m

!-------------------------------------------------------------------------------
!*  Applies regularity conditions at R=0 in toroidal geometry
!-------------------------------------------------------------------------------
  SUBROUTINE regularity_cv1m(cv1m,edge,flag)
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> associated edge
    TYPE(edge_type), INTENT(IN) :: edge
    !> flag
    CHARACTER(*), INTENT(IN) :: flag

    INTEGER(i4) :: iv,ix,iy,iside,ivec,nvec,imn1
    REAL(r8), DIMENSION(cv1m%nqty,1) :: mult
    LOGICAL :: do_segment
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'regularity_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   apply regularity conditions to the different Fourier components
!   of a vector.  for a scalar quantity, n>0 are set to 0.  for a
!   vector, thd following components are set to 0
!   n=0:  r and phi
!   n=1:  z
!   n>1:  r, z, and phi
!
!   create an array of 1s and 0s to zero out the appropriate
!   components.  the geometry is always toroidal at this point.
!
!   For n=1 vectors the r and phi components are degenerate, transfer the
!   phi component coefficient to r and zero phi
!-------------------------------------------------------------------------------
    mult=0
    imn1=0
    nvec=0
    SELECT CASE(cv1m%nqty)
    CASE(1,2,4,5)          !   scalars
      IF (cv1m%fcomp==0) THEN
        mult(:,1)=1
      ENDIF
    CASE(3,6,9,12,15,18)   !   3-vectors
      nvec=cv1m%nqty/3
      DO ivec=0,nvec-1
        IF (cv1m%fcomp==0) THEN
          mult(3*ivec+2,1)=1
        ELSEIF (cv1m%fcomp==1) THEN
          mult(3*ivec+1,1)=1
          IF (flag=='cyl_vec') imn1=cv1m%fcomp
          IF (flag=='init_cyl_vec') mult(3*ivec+3,1)=1
        ENDIF
      ENDDO
    CASE DEFAULT
      CALL par%nim_stop("vec_rect_2D::regularity_cv1m"//                        &
                        " inconsistent # of components.")
    END SELECT
!-------------------------------------------------------------------------------
!   loop over block border elements and apply mult to the vertices
!   at R=0.  also combine rhs for n=1 r and phi comps.
!-------------------------------------------------------------------------------
    do_segment=.FALSE.
    IF (ASSOCIATED(cv1m%arrh).OR.ASSOCIATED(cv1m%arrv)) do_segment=.TRUE.
    ASSOCIATE (arr=>cv1m%arr,arrh=>cv1m%arrh,arrv=>cv1m%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               r0point=>edge%r0point,r0segment=>edge%r0segment)
      !$acc parallel present(arr,arrh,arrv,cv1m,edge,vertex,segment)            &
      !$acc present(r0point,r0segment) copyin(mult,do_segment)                  &
      !$acc async(cv1m%id) if(cv1m%on_gpu)
      !$acc loop gang private(ix,iy)
      DO iv=1,edge%nvert
        IF (r0point(iv)) THEN
          ix=vertex(iv)%intxy(1)
          iy=vertex(iv)%intxy(2)
          IF (imn1/=0) THEN
            DO ivec=0,nvec-1
              arr(3*ivec+1,ix,iy)=arr(3*ivec+1,ix,iy)-(0,1)*arr(3*ivec+3,ix,iy)
            ENDDO
          ENDIF
          arr(:,ix,iy)=arr(:,ix,iy)*mult(:,1)
        ENDIF
        IF (do_segment) THEN
          IF (r0segment(iv)) THEN
            ix=segment(iv)%intxys(1)
            iy=segment(iv)%intxys(2)
            IF (segment(iv)%h_side) THEN
              IF (imn1/=0) THEN
                !$acc loop worker
                DO iside=1,cv1m%n_side
                  DO ivec=0,nvec-1
                    arrh(3*ivec+1,iside,ix,iy)=arrh(3*ivec+1,iside,ix,iy)       &
                                               -(0,1)*arrh(3*ivec+3,iside,ix,iy)
                  ENDDO
                ENDDO
              ENDIF
              !$acc loop worker
              DO iside=1,cv1m%n_side
                arrh(:,iside,ix,iy)=arrh(:,iside,ix,iy)*mult(:,1)
              ENDDO
            ELSE
              IF (imn1/=0) THEN
                !$acc loop worker
                DO iside=1,cv1m%n_side
                  DO ivec=0,nvec-1
                    arrv(3*ivec+1,iside,ix,iy)=arrv(3*ivec+1,iside,ix,iy)       &
                                               -(0,1)*arrv(3*ivec+3,iside,ix,iy)
                  ENDDO
                ENDDO
              ENDIF
              !$acc loop worker
              DO iside=1,cv1m%n_side
                arrv(:,iside,ix,iy)=arrv(:,iside,ix,iy)*mult(:,1)
              ENDDO
            ENDIF
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE regularity_cv1m

!-------------------------------------------------------------------------------
!* set variables needed by edge routines
!-------------------------------------------------------------------------------
  SUBROUTINE set_edge_vars_cv1m(cv1m,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_cv1m_acc), INTENT(IN) :: cv1m
    !> edge to set vars in
    TYPE(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'set_edge_vars_cv1m',iftn,idepth)
    DO iv=1,cv1m%mx
      edge%segment(iv)%h_side=.true.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyn
      edge%segment(iv)%load_dir=1_i4
    ENDDO
    DO iv=cv1m%mx+1,cv1m%mx+cv1m%my
      edge%segment(iv)%h_side=.false.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyn
      edge%segment(iv)%load_dir=1_i4
    ENDDO
    DO iv=cv1m%mx+cv1m%my+1,2*cv1m%mx+cv1m%my
      edge%segment(iv)%h_side=.true.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyp
      edge%segment(iv)%load_dir=-1_i4
    ENDDO
    DO iv=2*cv1m%mx+cv1m%my+1,edge%nvert
      edge%segment(iv)%h_side=.false.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyp
      edge%segment(iv)%load_dir=-1_i4
    ENDDO
    !$acc update device(edge%segment) async(edge%id) if(cv1m%on_gpu)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE set_edge_vars_cv1m

!-------------------------------------------------------------------------------
!* load a edge communitcation array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_load_arr_cv1m(cv1m,edge,nqty,n_side,do_avg)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be loaded
    CLASS(vec_rect_2D_cv1m_acc), INTENT(IN) :: cv1m
    !> edge to load
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
    !> apply edge average if true
    LOGICAL, OPTIONAL, INTENT(IN) :: do_avg

    INTEGER(i4) :: ix,iy,iv,is,nq,ns,ivs,ld
    LOGICAL :: do_avg_loc
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_load_arr_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=cv1m%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=cv1m%n_side
    ENDIF
    IF (PRESENT(do_avg)) THEN
      do_avg_loc=do_avg
    ELSE
      do_avg_loc=.FALSE.
    ENDIF
!-------------------------------------------------------------------------------
!   set internal flags for edge_network call
!-------------------------------------------------------------------------------
    edge%nqty_loaded=nq
    edge%nside_loaded=ns
    edge%nmodes_loaded=1_i4
    edge%on_gpu=.TRUE.
    ASSOCIATE (arr=>cv1m%arr,arrh=>cv1m%arrh,arrv=>cv1m%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               vert_cin=>edge%vert_cin,seg_cin=>edge%seg_cin)
      !$acc parallel present(arr,arrh,arrv,edge,vertex,segment)                 &
      !$acc present(vert_cin,seg_cin) copyin(nq,ns,do_avg_loc)                  &
      !$acc async(cv1m%id) if(cv1m%on_gpu)
      !$acc loop gang private(ix,iy,ld)
      DO iv=1,edge%nvert
!-------------------------------------------------------------------------------
!       copy block-internal data to edge storage.  vertex-centered data first.
!-------------------------------------------------------------------------------
        ix=vertex(iv)%intxy(1)
        iy=vertex(iv)%intxy(2)
        vert_cin(1:nq,iv)=arr(1:nq,ix,iy)
!-------------------------------------------------------------------------------
!       element side-centered data.  load side-centered nodes in the
!       direction of the edge (ccw around the block), as indicated by load_dir.
!-------------------------------------------------------------------------------
        IF (ns/=0) THEN
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          ld=segment(iv)%load_dir
          IF (segment(iv)%h_side) THEN
            !$acc loop worker private(ivs)
            DO is=1,ns
              ivs=(((1-ld)/2)*(ns+1)+ld*is-1)*nq+1
              seg_cin(ivs:ivs+nq-1,iv)=arrh(1:nq,is,ix,iy)
            ENDDO
          ELSE
            !$acc loop worker private(ivs)
            DO is=1,ns
              ivs=(((1-ld)/2)*(ns+1)+ld*is-1)*nq+1
              seg_cin(ivs:ivs+nq-1,iv)=arrv(1:nq,is,ix,iy)
            ENDDO
          ENDIF
        ENDIF
!-------------------------------------------------------------------------------
!       apply average factor
!-------------------------------------------------------------------------------
        IF (do_avg_loc) THEN
          vert_cin(1:nq,iv)=vert_cin(1:nq,iv)*vertex(iv)%ave_factor
          IF (ns/=0) THEN
            seg_cin(1:nq*ns,iv)=seg_cin(1:nq*ns,iv)*segment(iv)%ave_factor
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
!-------------------------------------------------------------------------------
!   copy data to CPU, wait/transfer in seam%edge_network
!   TODO: unify seam array structures to avoid extra copy/waits
!   TODO: use GPU RDMA in seam%edge_network
!-------------------------------------------------------------------------------
    ASSOCIATE (vert_cin=>edge%vert_cin,seg_cin=>edge%seg_cin)
      !$acc update self(vert_cin) async(cv1m%id) if(cv1m%on_gpu)
      IF (ns/=0) THEN
        !$acc update self(seg_cin) async(cv1m%id) if(cv1m%on_gpu)
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_load_arr_cv1m

!-------------------------------------------------------------------------------
!* unload a edge communication array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_unload_arr_cv1m(cv1m,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be unloaded
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> edge to unload
    TYPE(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: ix,iy,iv,is,nq,ns,ivs,ld
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_unload_arr_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   read edge internal flags
!-------------------------------------------------------------------------------
    nq=edge%nqty_loaded
    ns=edge%nside_loaded
    ASSOCIATE (arr=>cv1m%arr,arrh=>cv1m%arrh,arrv=>cv1m%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               vert_cout=>edge%vert_cout,seg_cout=>edge%seg_cout)
      !$acc parallel present(arr,arrh,arrv,edge,vertex,segment)                 &
      !$acc present(vert_cout,seg_cout) copyin(nq,ns)                           &
      !$acc async(cv1m%id) if(cv1m%on_gpu)
      !$acc loop gang private(ix,iy,ld)
      DO iv=1,edge%nvert
!-------------------------------------------------------------------------------
!       copy from edge storage to block-internal data.
!       vertex-centered data first.
!-------------------------------------------------------------------------------
        ix=vertex(iv)%intxy(1)
        iy=vertex(iv)%intxy(2)
        arr(1:nq,ix,iy)=vert_cout(1:nq,iv)
!-------------------------------------------------------------------------------
!       element side-centered data.  unload side-centered nodes in the
!       direction of the edge (ccw around the block), as indicated by load_dir.
!-------------------------------------------------------------------------------
        IF (ns/=0) THEN
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          ld=segment(iv)%load_dir
          IF (segment(iv)%h_side) THEN
            !$acc loop worker private(ivs)
            DO is=1,ns
              ivs=(((1-ld)/2)*(ns+1)+ld*is-1)*nq+1
              arrh(1:nq,is,ix,iy)=seg_cout(ivs:ivs+nq-1,iv)
            ENDDO
          ELSE
            !$acc loop worker private(ivs)
            DO is=1,ns
              ivs=(((1-ld)/2)*(ns+1)+ld*is-1)*nq+1
              arrv(1:nq,is,ix,iy)=seg_cout(ivs:ivs+nq-1,iv)
            ENDDO
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_unload_arr_cv1m

!-------------------------------------------------------------------------------
!* load a edge save array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_load_save_cv1m(cv1m,edge,nqty,n_side)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be loaded
    CLASS(vec_rect_2D_cv1m_acc), INTENT(IN) :: cv1m
    !> edge to load
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side

    INTEGER(i4) :: ix,iy,iv,is,nq,ns
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_load_save_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=cv1m%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=cv1m%n_side
    ENDIF
    ASSOCIATE (arr=>cv1m%arr,arrh=>cv1m%arrh,arrv=>cv1m%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               vert_csave=>edge%vert_csave,seg_csave=>edge%seg_csave)
      !$acc parallel present(arr,arrh,arrv,edge,vertex,segment)                 &
      !$acc present(vert_csave,seg_csave) copyin(nq,ns)                         &
      !$acc async(cv1m%id) if(cv1m%on_gpu)
      !$acc loop gang private(ix,iy)
      DO iv=1,edge%nvert
!-------------------------------------------------------------------------------
!       copy block-internal data to edge storage.  vertex-centered data first.
!-------------------------------------------------------------------------------
        ix=vertex(iv)%intxy(1)
        iy=vertex(iv)%intxy(2)
        vert_csave(1:nq,1,iv)=arr(1:nq,ix,iy)
!-------------------------------------------------------------------------------
!       element side-centered data.
!-------------------------------------------------------------------------------
        IF (ns/=0) THEN
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          IF (segment(iv)%h_side) THEN
            !$acc loop worker
            DO is=1,ns
              seg_csave(1:nq,is,1,iv)=arrh(1:nq,is,ix,iy)
            ENDDO
          ELSE
            !$acc loop worker
            DO is=1,ns
              seg_csave(1:nq,is,1,iv)=arrv(1:nq,is,ix,iy)
            ENDDO
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_load_save_cv1m

!-------------------------------------------------------------------------------
!* unload a edge save array and add to vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_add_save_cv1m(cv1m,edge,nqty,n_side)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be add to
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> edge to add from
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side

    INTEGER(i4) :: ix,iy,iv,is,nq,ns
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_add_save_cv1m',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=cv1m%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=cv1m%n_side
    ENDIF
    ASSOCIATE (arr=>cv1m%arr,arrh=>cv1m%arrh,arrv=>cv1m%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               vert_csave=>edge%vert_csave,seg_csave=>edge%seg_csave)
      !$acc parallel present(arr,arrh,arrv,edge,vertex,segment)                 &
      !$acc present(vert_csave,seg_csave) copyin(nq,ns)                         &
      !$acc async(cv1m%id) if(cv1m%on_gpu)
      !$acc loop gang private(ix,iy)
      DO iv=1,edge%nvert
!-------------------------------------------------------------------------------
!       copy from edge storage to block-internal data.
!       vertex-centered data first.
!-------------------------------------------------------------------------------
        ix=vertex(iv)%intxy(1)
        iy=vertex(iv)%intxy(2)
        arr(1:nq,ix,iy)=arr(1:nq,ix,iy)+vert_csave(1:nq,1,iv)
!-------------------------------------------------------------------------------
!       element side-centered data.
!-------------------------------------------------------------------------------
        IF (ns/=0) THEN
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          IF (segment(iv)%h_side) THEN
            !$acc loop worker
            DO is=1,ns
              arrh(1:nq,is,ix,iy)=arrh(1:nq,is,ix,iy)+seg_csave(1:nq,is,1,iv)
            ENDDO
          ELSE
            !$acc loop worker
            DO is=1,ns
              arrv(1:nq,is,ix,iy)=arrv(1:nq,is,ix,iy)+seg_csave(1:nq,is,1,iv)
            ENDDO
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_add_save_cv1m

!-------------------------------------------------------------------------------
!> Assume vector data represents the diagonal of a matrix and apply a matrix
!  multiply.
!-------------------------------------------------------------------------------
  SUBROUTINE apply_diag_matvec_cv1m(cv1m,input_vec,output_vec)
    IMPLICIT NONE

    !> vector that represents a diagonal matrix
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> vector to multiply
    CLASS(cvector1m), INTENT(IN) :: input_vec
    !> output vector = cv1m (matrix diagonal) dot input_vec
    CLASS(cvector1m), INTENT(INOUT) :: output_vec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'apply_diag_matvec_cv1m',iftn,idepth)
    SELECT TYPE (input_vec)
    TYPE IS (vec_rect_2D_cv1m_acc)
      SELECT TYPE (output_vec)
      TYPE IS (vec_rect_2D_cv1m_acc)
        ASSOCIATE (arr_mat=>cv1m%arr,arrh_mat=>cv1m%arrh,                       &
                   arrv_mat=>cv1m%arrv,arri_mat=>cv1m%arri,                     &
                   arr_in=>input_vec%arr,arrh_in=>input_vec%arrh,               &
                   arrv_in=>input_vec%arrv,arri_in=>input_vec%arri,             &
                   arr_out=>output_vec%arr,arrh_out=>output_vec%arrh,           &
                   arrv_out=>output_vec%arrv,arri_out=>output_vec%arri)
          IF (cv1m%pd>1) THEN
            IF (cv1m%skip_elim_interior) THEN
              !$acc kernels present(arr_mat,arrh_mat,arrv_mat)                  &
              !$acc present(arr_in,arrh_in,arrv_in)                             &
              !$acc present(arr_out,arrh_out,arrv_out)                          &
              !$acc async(cv1m%id) if(cv1m%on_gpu)
              arr_out=arr_mat*arr_in
              arrh_out=arrh_mat*arrh_in
              arrv_out=arrv_mat*arrv_in
              !$acc end kernels
            ELSE
              !$acc kernels present(arr_mat,arrh_mat,arrv_mat,arri_mat)         &
              !$acc present(arr_in,arrh_in,arrv_in,arri_in)                     &
              !$acc present(arr_out,arrh_out,arrv_out,arri_out)                 &
              !$acc async(cv1m%id) if(cv1m%on_gpu)
              arr_out=arr_mat*arr_in
              arrh_out=arrh_mat*arrh_in
              arrv_out=arrv_mat*arrv_in
              arri_out=arri_mat*arri_in
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr_mat,arr_in,arr_out)                       &
            !$acc async(cv1m%id) if(cv1m%on_gpu)
            arr_out=arr_mat*arr_in
            !$acc end kernels
          ENDIF
        END ASSOCIATE
      CLASS DEFAULT
        CALL par%nim_stop('Expected vec_rect_2D_cv1m_acc for output_vec'        &
                          //' in apply_diag_matvec_cv1m')
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m_acc for input_vec'           &
                        //' in apply_diag_matvec_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE apply_diag_matvec_cv1m

!-------------------------------------------------------------------------------
!> Invert the values of the vector component-wise
!-------------------------------------------------------------------------------
  SUBROUTINE invert_cv1m(cv1m)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'invert_cv1m',iftn,idepth)
    IF (cv1m%pd>1) THEN
      ASSOCIATE (arr=>cv1m%arr,arrh=>cv1m%arrh,arrv=>cv1m%arrv,arri=>cv1m%arri)
        IF (cv1m%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv,cv1m) async(cv1m%id) if(cv1m%on_gpu)
          arr(:,0:cv1m%mx,0:cv1m%my)=1._r8/arr(:,0:cv1m%mx,0:cv1m%my)
          arrh(:,:,1:cv1m%mx,0:cv1m%my)=1._r8/arrh(:,:,1:cv1m%mx,0:cv1m%my)
          arrv(:,:,0:cv1m%mx,1:cv1m%my)=1._r8/arrv(:,:,0:cv1m%mx,1:cv1m%my)
          !$acc end kernels
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri,cv1m)                        &
          !$acc async(cv1m%id) if(cv1m%on_gpu)
          arr(:,0:cv1m%mx,0:cv1m%my)=1._r8/arr(:,0:cv1m%mx,0:cv1m%my)
          arrh(:,:,1:cv1m%mx,0:cv1m%my)=1._r8/arrh(:,:,1:cv1m%mx,0:cv1m%my)
          arrv(:,:,0:cv1m%mx,1:cv1m%my)=1._r8/arrv(:,:,0:cv1m%mx,1:cv1m%my)
          arri(:,:,1:cv1m%mx,1:cv1m%my)=1._r8/arri(:,:,1:cv1m%mx,1:cv1m%my)
          !$acc end kernels
        ENDIF
      END ASSOCIATE
    ELSE
      ASSOCIATE (arr=>cv1m%arr)
        !$acc kernels present(arr,cv1m) async(cv1m%id) if(cv1m%on_gpu)
        arr(:,0:cv1m%mx,0:cv1m%my)=1._r8/arr(:,0:cv1m%mx,0:cv1m%my)
        !$acc end kernels
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE invert_cv1m

!-------------------------------------------------------------------------------
!* Assign to the vector for testing
!-------------------------------------------------------------------------------
  SUBROUTINE assign_for_testing_cv1m(cv1m,operation,const)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_cv1m_acc), INTENT(INOUT) :: cv1m
    !> operation: 'constant' or 'index'
    CHARACTER(*), INTENT(IN) :: operation
    !> optional constant value for operation='constant'
    COMPLEX(r8), INTENT(IN), OPTIONAL :: const

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1
    COMPLEX(r8) :: fac
    INTEGER(i4) :: ii1,ii2,ii3,ii4,ind

    CALL timer%start_timer_l2(mod_name,'assign_for_testing_cv1m',iftn,idepth)
    fac=1._r8
    IF (PRESENT(const)) fac=const
    CALL cv1m%zero
    SELECT CASE(operation)
    CASE("constant")
      IF (ASSOCIATED(cv1m%arr)) THEN
        ASSOCIATE (arr=>cv1m%arr)
          !$acc kernels copyin(fac) present(arr,cv1m) async(cv1m%id) if(cv1m%on_gpu)
          arr(:,0:cv1m%mx,0:cv1m%my)=fac
          !$acc end kernels
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cv1m%arrh)) THEN
        ASSOCIATE (arrh=>cv1m%arrh)
          !$acc kernels copyin(fac) present(arrh,cv1m) async(cv1m%id) if(cv1m%on_gpu)
          arrh(:,:,1:cv1m%mx,0:cv1m%my)=fac
          !$acc end kernels
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cv1m%arrv)) THEN
        ASSOCIATE (arrv=>cv1m%arrv)
          !$acc kernels copyin(fac) present(arrv,cv1m) async(cv1m%id) if(cv1m%on_gpu)
          arrv(:,:,0:cv1m%mx,1:cv1m%my)=fac
          !$acc end kernels
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior) THEN
        ASSOCIATE (arri=>cv1m%arri)
          !$acc kernels copyin(fac) present(arri,cv1m) async(cv1m%id) if(cv1m%on_gpu)
          arri(:,:,1:cv1m%mx,1:cv1m%my)=fac
          !$acc end kernels
        END ASSOCIATE
      ENDIF
    CASE("index")
      IF (ASSOCIATED(cv1m%arr)) THEN
        ASSOCIATE (arr=>cv1m%arr)
          !$acc parallel present(cv1m,arr) async(cv1m%id) if(cv1m%on_gpu)
          !$acc loop gang vector collapse(3) private(ind)
          DO ii1=1,cv1m%nqty
            DO ii2=0,cv1m%mx
              DO ii3=0,cv1m%my
                ind=ii1*SIZE(arr,2)*SIZE(arr,3)+ii2*SIZE(arr,3)+ii3
                arr(ii1,ii2,ii3)=ind
              ENDDO
            ENDDO
          ENDDO
          !$acc end parallel
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cv1m%arrh)) THEN
        ASSOCIATE (arrh=>cv1m%arrh)
          !$acc parallel present(cv1m,arrh) async(cv1m%id) if(cv1m%on_gpu)
          !$acc loop gang vector collapse(4) private(ind)
          DO ii1=1,cv1m%nqty
            DO ii2=1,cv1m%n_side
              DO ii3=1,cv1m%mx
                DO ii4=0,cv1m%my
                  ind=ii1*SIZE(arrh,2)*SIZE(arrh,3)*SIZE(arrh,4)+               &
                      ii2*SIZE(arrh,3)*SIZE(arrh,4)+ii3*SIZE(arrh,4)+ii4
                  arrh(ii1,ii2,ii3,ii4)=ind
                ENDDO
              ENDDO
            ENDDO
          ENDDO
          !$acc end parallel
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cv1m%arrv)) THEN
        ASSOCIATE (arrv=>cv1m%arrv)
          !$acc parallel present(cv1m,arrv) async(cv1m%id) if(cv1m%on_gpu)
          !$acc loop gang vector collapse(4) private(ind)
          DO ii1=1,cv1m%nqty
            DO ii2=1,cv1m%n_side
              DO ii3=0,cv1m%mx
                DO ii4=1,cv1m%my
                  ind=ii1*SIZE(arrv,2)*SIZE(arrv,3)*SIZE(arrv,4)+               &
                      ii2*SIZE(arrv,3)*SIZE(arrv,4)+ii3*SIZE(arrv,4)+ii4
                  arrv(ii1,ii2,ii3,ii4)=ind
                ENDDO
              ENDDO
            ENDDO
          ENDDO
          !$acc end parallel
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior) THEN
        ASSOCIATE (arri=>cv1m%arri)
          !$acc parallel present(cv1m,arri) async(cv1m%id) if(cv1m%on_gpu)
          !$acc loop gang vector collapse(4) private(ind)
          DO ii1=1,cv1m%nqty
            DO ii2=1,cv1m%n_int
              DO ii3=1,cv1m%mx
                DO ii4=1,cv1m%my
                  ind=ii1*SIZE(arri,2)*SIZE(arri,3)*SIZE(arri,4)+               &
                      ii2*SIZE(arri,3)*SIZE(arri,4)+ii3*SIZE(arri,4)+ii4
                  arri(ii1,ii2,ii3,ii4)=ind
                ENDDO
              ENDDO
            ENDDO
          ENDDO
          !$acc end parallel
        END ASSOCIATE
      ENDIF
    CASE DEFAULT
      CALL par%nim_stop('Expected operation=constant or index'                  &
                        //' in assign_for_testing_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_for_testing_cv1m

!-------------------------------------------------------------------------------
!*  Test if one vector is equal to another
!-------------------------------------------------------------------------------
  LOGICAL FUNCTION test_if_equal_cv1m(cv1m,cv1m2) RESULT(output)
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_cv1m_acc), INTENT(IN) :: cv1m
    !> vector to test against
    CLASS(cvector1m), INTENT(IN) :: cv1m2

    INTEGER(i4) :: shape3a(3),shape3b(3),shape4a(4),shape4b(4)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'test_if_equal_cv1m',iftn,idepth)
    output=.TRUE.
    SELECT TYPE (cv1m2)
    TYPE IS (vec_rect_2D_cv1m_acc)
      !$acc update self(cv1m%arr,cv1m%arrh,cv1m%arrv,cv1m%arri)                 &
      !$acc async(cv1m%id) if(cv1m%on_gpu)
      !$acc update self(cv1m2%arr,cv1m2%arrh,cv1m2%arrv,cv1m2%arri)             &
      !$acc async(cv1m%id) if(cv1m%on_gpu)
      !$acc wait(cv1m%id) if(cv1m%on_gpu)
      IF (ASSOCIATED(cv1m%arr)) THEN
        shape3a=SHAPE(cv1m%arr)
        shape3b=SHAPE(cv1m2%arr)
        IF (.NOT.ALL(shape3a == shape3b)) output=.FALSE.
        IF (output.AND..NOT.ALL(cv1m%arr == cv1m2%arr)) output=.FALSE.
      ELSE IF (ASSOCIATED(cv1m2%arr)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(cv1m%arrh)) THEN
        shape4a=SHAPE(cv1m%arrh)
        shape4b=SHAPE(cv1m2%arrh)
        IF (.NOT.ALL(shape4a == shape4b)) output=.FALSE.
        IF (output.AND..NOT.ALL(cv1m%arrh == cv1m2%arrh)) output=.FALSE.
      ELSE IF (ASSOCIATED(cv1m2%arrh)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(cv1m%arrv)) THEN
        shape4a=SHAPE(cv1m%arrv)
        shape4b=SHAPE(cv1m2%arrv)
        IF (.NOT.ALL(shape4a == shape4b)) output=.FALSE.
        IF (output.AND..NOT.ALL(cv1m%arrv == cv1m2%arrv)) output=.FALSE.
      ELSE IF (ASSOCIATED(cv1m2%arrv)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(cv1m%arri).AND..NOT.cv1m%skip_elim_interior) THEN
        shape4a=SHAPE(cv1m%arri)
        shape4b=SHAPE(cv1m2%arri)
        IF (.NOT.ALL(shape4a == shape4b)) output=.FALSE.
        IF (output.AND..NOT.ALL(cv1m%arri == cv1m2%arri)) output=.FALSE.
      ELSE IF (ASSOCIATED(cv1m2%arri)) THEN
        output=.FALSE.
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for cv1m2 in'//              &
                        ' test_if_equal_cv1m')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END FUNCTION test_if_equal_cv1m
