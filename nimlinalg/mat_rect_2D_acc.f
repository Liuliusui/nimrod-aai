!-------------------------------------------------------------------------------
!< module containing routines that performs standard operations on
!  rectilinear matrices.
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!> module containing routines that performs standard operations on
!  rectilinear matrices.
!-------------------------------------------------------------------------------
MODULE mat_rect_2D_mod_acc
  USE local
  USE mat_rect_2D_real_mod_acc
  USE mat_rect_2D_cm1m_mod_acc
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: mat_rect_2D_real_acc,mat_rect_2D_cm1m_acc

END MODULE mat_rect_2D_mod_acc
