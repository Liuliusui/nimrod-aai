!-------------------------------------------------------------------------------
!! Defines routines to transfer linear algebra solutions in FEM spaces
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* Defines routines to transfer linear algebra solutions in FEM spaces
!-------------------------------------------------------------------------------
MODULE xfer_vector_to_fem_mod
  USE h1_rect_2D_mod
  USE local
  USE nodal_mod
  USE pardata_mod
  USE timer_mod
  USE vector_mod
  USE vec_rect_2D_mod
  USE vec_rect_2D_mod_acc
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: xfer_vector_to_fem

  INTERFACE xfer_vector_to_fem
    MODULE PROCEDURE xfer_rvector_to_1fem,xfer_rvector_to_2fem,                 &
                     xfer_cvector1m_to_1fem,xfer_cvector1m_to_2fem,             &
                     xfer_cvector_to_1fem,xfer_cvector_to_2fem
  END INTERFACE xfer_vector_to_fem

  CHARACTER(*), PARAMETER :: mod_name='xfer_vector_to_fem_mod'
CONTAINS

!-------------------------------------------------------------------------------
!* call nim_stop with a failure message
!-------------------------------------------------------------------------------
  SUBROUTINE fail_at_vector
    IMPLICIT NONE

    CALL par%nim_stop('xfer_vector_to_fem: Could transfer vector to fem_space')
  END SUBROUTINE

!-------------------------------------------------------------------------------
!* transfer a vector to one FEM space
!-------------------------------------------------------------------------------
  SUBROUTINE xfer_rvector_to_1fem(vec,fem_space)
    IMPLICIT NONE

    !> vector to transfer data from
    CLASS(rvector), INTENT(IN) :: vec
    !> FEM space to transfer data to
    CLASS(nodal_field_real), INTENT(INOUT) :: fem_space

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'xfer_rvector_to_1fem',iftn,idepth)
    SELECT TYPE (fem_space)
    TYPE IS (h1_rect_2D_real)
      SELECT TYPE (vec)
      TYPE IS (vec_rect_2D_real)
        fem_space%fs=vec%arr(:,:,:)
        IF (fem_space%pd>1) THEN
          fem_space%fsh=vec%arrh(:,:,:,:)
          fem_space%fsv=vec%arrv(:,:,:,:)
          fem_space%fsi=vec%arri(:,:,:,:)
        ENDIF
      END SELECT
    TYPE IS (h1_rect_2D_real_acc)
      SELECT TYPE (vec)
      TYPE IS (vec_rect_2D_real_acc)
        IF (fem_space%pd>1) THEN
          ASSOCIATE (fs=>fem_space%fs,arr=>vec%arr,                             &
                     fsh=>fem_space%fsh,arrh=>vec%arrh,                         &
                     fsv=>fem_space%fsv,arrv=>vec%arrv,                         &
                     fsi=>fem_space%fsi,arri=>vec%arri)
            !$acc kernels present(fs,arr,fsh,arrh,fsv,arrv,fsi,arri,vec)        &
            !$acc async(vec%id) if(vec%on_gpu)
            fs=arr(:,0:vec%mx,0:vec%my)
            fsh=arrh(:,:,1:vec%mx,0:vec%my)
            fsv=arrv(:,:,0:vec%mx,1:vec%my)
            fsi=arri(:,:,1:vec%mx,1:vec%my)
            !$acc end kernels
          END ASSOCIATE
        ELSE
          ASSOCIATE (fs=>fem_space%fs,arr=>vec%arr)
            !$acc kernels present(fs,arr,vec) async(vec%id) if(vec%on_gpu)
            fs=arr(:,0:vec%mx,0:vec%my)
            !$acc end kernels
          END ASSOCIATE
        ENDIF
      END SELECT
    CLASS DEFAULT
      CALL fail_at_vector
    END SELECT
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE xfer_rvector_to_1fem

!-------------------------------------------------------------------------------
!> transfer a vector to two FEM spaces; acceptable combinations (order matters):
!  - h1+h1
!-------------------------------------------------------------------------------
  SUBROUTINE xfer_rvector_to_2fem(vec,fem_space1,fem_space2)
    IMPLICIT NONE

    !> vector to transfer data from
    CLASS(rvector), INTENT(IN) :: vec
    !> FEM spaces to transfer data to
    CLASS(nodal_field_real), INTENT(INOUT) :: fem_space1,fem_space2

    INTEGER(i4) :: nqs,nqe,nqs2,nqe2
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'xfer_rvector_to_2fem',iftn,idepth)
    SELECT TYPE (fem_space1)
    TYPE IS (h1_rect_2D_real)
      SELECT TYPE (vec)
      TYPE IS (vec_rect_2D_real)
        SELECT TYPE (fem_space2)
        TYPE IS (h1_rect_2D_real)
          nqs=1
          nqe=fem_space1%nqty
          fem_space1%fs=vec%arr(nqs:nqe,:,:)
          IF (fem_space1%pd>1) THEN
            fem_space1%fsh=vec%arrh(nqs:nqe,:,:,:)
            fem_space1%fsv=vec%arrv(nqs:nqe,:,:,:)
            fem_space1%fsi=vec%arri(nqs:nqe,:,:,:)
          ENDIF
          nqs=nqs+nqe
          nqe=nqe+fem_space2%nqty
          fem_space2%fs=vec%arr(nqs:nqe,:,:)
          IF (fem_space2%pd>1) THEN
            fem_space2%fsh=vec%arrh(nqs:nqe,:,:,:)
            fem_space2%fsv=vec%arrv(nqs:nqe,:,:,:)
            fem_space2%fsi=vec%arri(nqs:nqe,:,:,:)
          ENDIF
        CLASS DEFAULT
          CALL fail_at_vector
        END SELECT
      END SELECT
    TYPE IS (h1_rect_2D_real_acc)
      SELECT TYPE (vec)
      TYPE IS (vec_rect_2D_real_acc)
        SELECT TYPE (fem_space2)
        TYPE IS (h1_rect_2D_real_acc)
          nqs=1
          nqe=fem_space1%nqty
          nqs2=nqs+nqe
          nqe2=nqe+fem_space2%nqty
          IF (fem_space1%pd>1) THEN
            ASSOCIATE (fs=>fem_space1%fs,fs2=>fem_space2%fs,arr=>vec%arr,       &
                       fsh=>fem_space1%fsh,fsh2=>fem_space2%fsh,arrh=>vec%arrh, &
                       fsv=>fem_space1%fsv,fsv2=>fem_space2%fsv,arrv=>vec%arrv, &
                       fsi=>fem_space1%fsi,fsi2=>fem_space2%fsi,arri=>vec%arri)
              !$acc kernels present(fs,fs2,arr,fsh,fsh2,arrh,fsv,fsv2)          &
              !$acc present(arrv,fsi,fsi2,arri,vec) copyin(nqs,nqe,nqs2,nqe2)   &
              !$acc async(vec%id) if(vec%on_gpu)
              fs=arr(nqs:nqe,0:vec%mx,0:vec%my)
              fs2=arr(nqs2:nqe2,0:vec%mx,0:vec%my)
              fsh=arrh(nqs:nqe,:,1:vec%mx,0:vec%my)
              fsh2=arrh(nqs2:nqe2,:,1:vec%mx,0:vec%my)
              fsv=arrv(nqs:nqe,:,0:vec%mx,1:vec%my)
              fsv2=arrv(nqs2:nqe2,:,0:vec%mx,1:vec%my)
              fsi=arri(nqs:nqe,:,1:vec%mx,1:vec%my)
              fsi2=arri(nqs2:nqe2,:,1:vec%mx,1:vec%my)
              !$acc end kernels
            END ASSOCIATE
          ELSE
            ASSOCIATE (fs=>fem_space1%fs,fs2=>fem_space2%fs,arr=>vec%arr)
              !$acc kernels present(fs,fs2,arr,vec) copyin(nqs,nqe,nqs2,nqe2)   &
              !$acc async(vec%id) if(vec%on_gpu)
              fs=arr(nqs:nqe,0:vec%mx,0:vec%my)
              fs2=arr(nqs2:nqe2,0:vec%mx,0:vec%my)
              !$acc end kernels
            END ASSOCIATE
          ENDIF
        END SELECT
      END SELECT
    CLASS DEFAULT
      CALL fail_at_vector
    END SELECT
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE xfer_rvector_to_2fem

!-------------------------------------------------------------------------------
!* transfer a vector to one FEM space
!-------------------------------------------------------------------------------
  SUBROUTINE xfer_cvector1m_to_1fem(vec,fem_space,imode)
    IMPLICIT NONE

    !> vector to transfer data from
    CLASS(cvector1m), INTENT(IN) :: vec
    !> FEM space to transfer data to
    CLASS(nodal_field_comp), INTENT(INOUT) :: fem_space
    !> mode number is FEM space to transfer to
    INTEGER(i4), OPTIONAL, INTENT(IN) :: imode

    INTEGER(i4) :: im
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'xfer_cvector1m_to_1fem',iftn,idepth)
    IF (PRESENT(imode)) THEN
      im=imode
    ELSE
      im=1
    ENDIF
    SELECT TYPE (fem_space)
    TYPE IS (h1_rect_2D_comp)
      SELECT TYPE (vec)
      TYPE IS (vec_rect_2D_cv1m)
        fem_space%fs(:,:,:,im)=vec%arr(:,:,:)
        IF (fem_space%pd>1) THEN
          fem_space%fsh(:,:,:,:,im)=vec%arrh(:,:,:,:)
          fem_space%fsv(:,:,:,:,im)=vec%arrv(:,:,:,:)
          fem_space%fsi(:,:,:,:,im)=vec%arri(:,:,:,:)
        ENDIF
      END SELECT
    TYPE IS (h1_rect_2D_comp_acc)
      SELECT TYPE (vec)
      TYPE IS (vec_rect_2D_cv1m_acc)
        IF (fem_space%pd>1) THEN
          ASSOCIATE (fs=>fem_space%fs,arr=>vec%arr,                             &
                     fsh=>fem_space%fsh,arrh=>vec%arrh,                         &
                     fsv=>fem_space%fsv,arrv=>vec%arrv,                         &
                     fsi=>fem_space%fsi,arri=>vec%arri)
            !$acc kernels present(fs,arr,fsh,arrh,fsv,arrv,fsi,arri,vec)        &
            !$acc copyin(im) async(vec%id) if(vec%on_gpu)
            fs(:,:,:,im)=arr(:,0:vec%mx,0:vec%my)
            fsh(:,:,:,:,im)=arrh(:,:,1:vec%mx,0:vec%my)
            fsv(:,:,:,:,im)=arrv(:,:,0:vec%mx,1:vec%my)
            fsi(:,:,:,:,im)=arri(:,:,1:vec%mx,1:vec%my)
            !$acc end kernels
          END ASSOCIATE
        ELSE
          ASSOCIATE (fs=>fem_space%fs,arr=>vec%arr)
            !$acc kernels present(fs,arr,vec) copyin(im)                        &
            !$acc async(vec%id) if(vec%on_gpu)
            fs(:,:,:,im)=arr(:,0:vec%mx,0:vec%my)
            !$acc end kernels
          END ASSOCIATE
        ENDIF
      END SELECT
    CLASS DEFAULT
      CALL fail_at_vector
    END SELECT
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE xfer_cvector1m_to_1fem

!-------------------------------------------------------------------------------
!> transfer a vector to two FEM spaces; acceptable combinations (order matters):
!  - h1+h1
!-------------------------------------------------------------------------------
  SUBROUTINE xfer_cvector1m_to_2fem(vec,fem_space1,fem_space2,imode)
    IMPLICIT NONE

    !> vector to transfer data from
    CLASS(cvector1m), INTENT(IN) :: vec
    !> FEM spaces to transfer data to
    CLASS(nodal_field_comp), INTENT(INOUT) :: fem_space1,fem_space2
    !> mode number is FEM space to transfer to
    INTEGER(i4), OPTIONAL, INTENT(IN) :: imode

    INTEGER(i4) :: nqs,nqe,nqs2,nqe2,im
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'xfer_cvector1m_to_2fem',iftn,idepth)
    IF (PRESENT(imode)) THEN
      im=imode
    ELSE
      im=1
    ENDIF
    SELECT TYPE (fem_space1)
    TYPE IS (h1_rect_2D_comp)
      SELECT TYPE (vec)
      TYPE IS (vec_rect_2D_cv1m)
        SELECT TYPE (fem_space2)
        TYPE IS (h1_rect_2D_comp)
          nqs=1
          nqe=fem_space1%nqty
          fem_space1%fs(:,:,:,im)=vec%arr(nqs:nqe,:,:)
          IF (fem_space1%pd>1) THEN
            fem_space1%fsh(:,:,:,:,im)=vec%arrh(nqs:nqe,:,:,:)
            fem_space1%fsv(:,:,:,:,im)=vec%arrv(nqs:nqe,:,:,:)
            fem_space1%fsi(:,:,:,:,im)=vec%arri(nqs:nqe,:,:,:)
          ENDIF
          nqs=nqs+nqe
          nqe=nqe+fem_space2%nqty
          fem_space2%fs(:,:,:,im)=vec%arr(nqs:nqe,:,:)
          IF (fem_space2%pd>1) THEN
            fem_space2%fsh(:,:,:,:,im)=vec%arrh(nqs:nqe,:,:,:)
            fem_space2%fsv(:,:,:,:,im)=vec%arrv(nqs:nqe,:,:,:)
            fem_space2%fsi(:,:,:,:,im)=vec%arri(nqs:nqe,:,:,:)
          ENDIF
        CLASS DEFAULT
          CALL fail_at_vector
        END SELECT
      END SELECT
    TYPE IS (h1_rect_2D_comp_acc)
      SELECT TYPE (vec)
      TYPE IS (vec_rect_2D_cv1m_acc)
        SELECT TYPE (fem_space2)
        TYPE IS (h1_rect_2D_comp_acc)
          nqs=1
          nqe=fem_space1%nqty
          nqs2=nqs+nqe
          nqe2=nqe+fem_space2%nqty
          IF (fem_space1%pd>1) THEN
            ASSOCIATE (fs=>fem_space1%fs,fs2=>fem_space2%fs,arr=>vec%arr,       &
                       fsh=>fem_space1%fsh,fsh2=>fem_space2%fsh,arrh=>vec%arrh, &
                       fsv=>fem_space1%fsv,fsv2=>fem_space2%fsv,arrv=>vec%arrv, &
                       fsi=>fem_space1%fsi,fsi2=>fem_space2%fsi,arri=>vec%arri)
              !$acc kernels present(fs,fs2,arr,fsh,fsh2,arrh,fsv,fsv2)          &
              !$acc present(arrv,fsi,fsi2,arri,vec) copyin(nqs,nqe,nqs2,nqe2)   &
              !$acc async(vec%id) if(vec%on_gpu)
              fs(:,:,:,im)=arr(nqs:nqe,0:vec%mx,0:vec%my)
              fs2(:,:,:,im)=arr(nqs2:nqe2,0:vec%mx,0:vec%my)
              fsh(:,:,:,:,im)=arrh(nqs:nqe,:,1:vec%mx,0:vec%my)
              fsh2(:,:,:,:,im)=arrh(nqs2:nqe2,:,1:vec%mx,0:vec%my)
              fsv(:,:,:,:,im)=arrv(nqs:nqe,:,0:vec%mx,1:vec%my)
              fsv2(:,:,:,:,im)=arrv(nqs2:nqe2,:,0:vec%mx,1:vec%my)
              fsi(:,:,:,:,im)=arri(nqs:nqe,:,1:vec%mx,1:vec%my)
              fsi2(:,:,:,:,im)=arri(nqs2:nqe2,:,1:vec%mx,1:vec%my)
              !$acc end kernels
            END ASSOCIATE
          ELSE
            ASSOCIATE (fs=>fem_space1%fs,fs2=>fem_space2%fs,arr=>vec%arr)
              !$acc kernels present(fs,fs2,arr,vec) copyin(nqs,nqe,nqs2,nqe2)   &
              !$acc async(vec%id) if(vec%on_gpu)
              fs(:,:,:,im)=arr(nqs:nqe,0:vec%mx,0:vec%my)
              fs2(:,:,:,im)=arr(nqs2:nqe2,0:vec%mx,0:vec%my)
              !$acc end kernels
            END ASSOCIATE
          ENDIF
        END SELECT
      END SELECT
    CLASS DEFAULT
      CALL fail_at_vector
    END SELECT
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE xfer_cvector1m_to_2fem

!-------------------------------------------------------------------------------
!* transfer a vector to one FEM space
!-------------------------------------------------------------------------------
  SUBROUTINE xfer_cvector_to_1fem(vec,fem_space)
    IMPLICIT NONE

    !> vector to transfer data from
    CLASS(cvector), INTENT(IN) :: vec
    !> FEM space to transfer data to
    CLASS(nodal_field_comp), INTENT(INOUT) :: fem_space

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'xfer_cvector_to_1fem',iftn,idepth)
    SELECT TYPE (fem_space)
    TYPE IS (h1_rect_2D_comp)
      SELECT TYPE (vec)
      TYPE IS (vec_rect_2D_comp)
        fem_space%fs=vec%arr(:,:,:,:)
        IF (fem_space%pd>1) THEN
          fem_space%fsh=vec%arrh(:,:,:,:,:)
          fem_space%fsv=vec%arrv(:,:,:,:,:)
          fem_space%fsi=vec%arri(:,:,:,:,:)
        ENDIF
      END SELECT
    TYPE IS (h1_rect_2D_comp_acc)
      SELECT TYPE (vec)
      TYPE IS (vec_rect_2D_comp_acc)
        IF (fem_space%pd>1) THEN
          ASSOCIATE (fs=>fem_space%fs,arr=>vec%arr,                             &
                     fsh=>fem_space%fsh,arrh=>vec%arrh,                         &
                     fsv=>fem_space%fsv,arrv=>vec%arrv,                         &
                     fsi=>fem_space%fsi,arri=>vec%arri)
            !$acc kernels present(fs,arr,fsh,arrh,fsv,arrv,fsi,arri,vec)        &
            !$acc async(vec%id) if(vec%on_gpu)
            fs=arr(:,0:vec%mx,0:vec%my,:)
            fsh=arrh(:,:,1:vec%mx,0:vec%my,:)
            fsv=arrv(:,:,0:vec%mx,1:vec%my,:)
            fsi=arri(:,:,1:vec%mx,1:vec%my,:)
            !$acc end kernels
          END ASSOCIATE
        ELSE
          ASSOCIATE (fs=>fem_space%fs,arr=>vec%arr)
            !$acc kernels present(fs,arr,vec) async(vec%id) if(vec%on_gpu)
            fs=arr(:,0:vec%mx,0:vec%my,:)
            !$acc end kernels
          END ASSOCIATE
        ENDIF
      END SELECT
    CLASS DEFAULT
      CALL fail_at_vector
    END SELECT
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE xfer_cvector_to_1fem

!-------------------------------------------------------------------------------
!> transfer a vector to two FEM spaces; acceptable combinations (order matters):
!  - h1+h1
!-------------------------------------------------------------------------------
  SUBROUTINE xfer_cvector_to_2fem(vec,fem_space1,fem_space2)
    IMPLICIT NONE

    !> vector to transfer data from
    CLASS(cvector), INTENT(IN) :: vec
    !> FEM spaces to transfer data to
    CLASS(nodal_field_comp), INTENT(INOUT) :: fem_space1,fem_space2

    INTEGER(i4) :: nqs,nqe,nqs2,nqe2
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'xfer_cvector_to_2fem',iftn,idepth)
    SELECT TYPE (fem_space1)
    TYPE IS (h1_rect_2D_comp)
      SELECT TYPE (vec)
      TYPE IS (vec_rect_2D_comp)
        SELECT TYPE (fem_space2)
        TYPE IS (h1_rect_2D_comp)
          nqs=1
          nqe=fem_space1%nqty
          fem_space1%fs=vec%arr(nqs:nqe,:,:,:)
          IF (fem_space1%pd>1) THEN
            fem_space1%fsh=vec%arrh(nqs:nqe,:,:,:,:)
            fem_space1%fsv=vec%arrv(nqs:nqe,:,:,:,:)
            fem_space1%fsi=vec%arri(nqs:nqe,:,:,:,:)
          ENDIF
          nqs=nqs+nqe
          nqe=nqe+fem_space2%nqty
          fem_space2%fs=vec%arr(nqs:nqe,:,:,:)
          IF (fem_space2%pd>1) THEN
            fem_space2%fsh=vec%arrh(nqs:nqe,:,:,:,:)
            fem_space2%fsv=vec%arrv(nqs:nqe,:,:,:,:)
            fem_space2%fsi=vec%arri(nqs:nqe,:,:,:,:)
          ENDIF
        CLASS DEFAULT
          CALL fail_at_vector
        END SELECT
      END SELECT
    TYPE IS (h1_rect_2D_comp_acc)
      SELECT TYPE (vec)
      TYPE IS (vec_rect_2D_comp_acc)
        SELECT TYPE (fem_space2)
        TYPE IS (h1_rect_2D_comp_acc)
          nqs=1
          nqe=fem_space1%nqty
          nqs2=nqs+nqe
          nqe2=nqe+fem_space2%nqty
          IF (fem_space1%pd>1) THEN
            ASSOCIATE (fs=>fem_space1%fs,fs2=>fem_space2%fs,arr=>vec%arr,       &
                       fsh=>fem_space1%fsh,fsh2=>fem_space2%fsh,arrh=>vec%arrh, &
                       fsv=>fem_space1%fsv,fsv2=>fem_space2%fsv,arrv=>vec%arrv, &
                       fsi=>fem_space1%fsi,fsi2=>fem_space2%fsi,arri=>vec%arri)
              !$acc kernels present(fs,fs2,arr,fsh,fsh2,arrh,fsv,fsv2)          &
              !$acc present(arrv,fsi,fsi2,arri,vec) copyin(nqs,nqe,nqs2,nqe2)   &
              !$acc async(vec%id) if(vec%on_gpu)
              fs=arr(nqs:nqe,0:vec%mx,0:vec%my,:)
              fs2=arr(nqs2:nqe2,0:vec%mx,0:vec%my,:)
              fsh=arrh(nqs:nqe,:,1:vec%mx,0:vec%my,:)
              fsh2=arrh(nqs2:nqe2,:,1:vec%mx,0:vec%my,:)
              fsv=arrv(nqs:nqe,:,0:vec%mx,1:vec%my,:)
              fsv2=arrv(nqs2:nqe2,:,0:vec%mx,1:vec%my,:)
              fsi=arri(nqs:nqe,:,1:vec%mx,1:vec%my,:)
              fsi2=arri(nqs2:nqe2,:,1:vec%mx,1:vec%my,:)
              !$acc end kernels
            END ASSOCIATE
          ELSE
            ASSOCIATE (fs=>fem_space1%fs,fs2=>fem_space2%fs,arr=>vec%arr)
              !$acc kernels present(fs,fs2,arr,vec) copyin(nqs,nqe,nqs2,nqe2)   &
              !$acc async(vec%id) if(vec%on_gpu)
              fs=arr(nqs:nqe,0:vec%mx,0:vec%my,:)
              fs2=arr(nqs2:nqe2,0:vec%mx,0:vec%my,:)
              !$acc end kernels
            END ASSOCIATE
          ENDIF
        END SELECT
      END SELECT
    CLASS DEFAULT
      CALL fail_at_vector
    END SELECT
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE xfer_cvector_to_2fem

END MODULE xfer_vector_to_fem_mod
