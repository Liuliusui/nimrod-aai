!-------------------------------------------------------------------------------
!! Register linalg configuration for tests downstream
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!* Register linalg configuration for tests downstream
!-------------------------------------------------------------------------------
MODULE linalg_registry_mod
  USE local
  USE pardata_mod
  IMPLICIT NONE

  INTEGER(i4) :: ntests=-1
  INTEGER(i4), PARAMETER, PRIVATE :: ntest_rect_2D=7
  CHARACTER(64) :: test_type
  LOGICAL :: single_block=.FALSE. ! for vector/matrix tests with a single block
#ifdef HAVE_OPENACC
  LOGICAL :: on_gpu=.TRUE.
#else
  LOGICAL :: on_gpu=.FALSE.
#endif
CONTAINS

!-------------------------------------------------------------------------------
!* set internal variables for test_type
!-------------------------------------------------------------------------------
  SUBROUTINE set_test_type
    IMPLICIT NONE

    test_type=TRIM(test_type)
    SELECT CASE(test_type)
    CASE ("rect_2D")
      ntests=ntest_rect_2D
    CASE ("rect_2D_acc")
      ntests=ntest_rect_2D
    CASE DEFAULT
      CALL par%nim_stop('setup_linalg:: invalid test_type')
    END SELECT
  END SUBROUTINE set_test_type

!-------------------------------------------------------------------------------
!* output valid test type strings for print_usage
!-------------------------------------------------------------------------------
  SUBROUTINE print_types
    IMPLICIT NONE

    CALL par%nim_write('  where <test type> is one of')
    CALL par%nim_write('    rect_2D')
    CALL par%nim_write('    rect_2D_acc')
  END SUBROUTINE print_types

!-------------------------------------------------------------------------------
!> routine to set up linear algebra structures for testing, par%init should be
!  called first.
!-------------------------------------------------------------------------------
  SUBROUTINE setup_linalg(rvec,cv1m,cvec,rmat,cmat,seam,itest,skip_mat)
    USE vector_mod
    USE matrix_mod
    USE seam_mod
    USE simple_seam_mod
    IMPLICIT NONE

    !* test rvec
    TYPE(rvec_storage), ALLOCATABLE, INTENT(OUT) :: rvec(:)
    !* test cv1m
    TYPE(cv1m_storage), ALLOCATABLE, INTENT(OUT) :: cv1m(:)
    !* test cvec
    TYPE(cvec_storage), ALLOCATABLE, INTENT(OUT) :: cvec(:)
    !* test rmat
    TYPE(rmat_storage), ALLOCATABLE, INTENT(OUT) :: rmat(:)
    !* test cmat
    TYPE(cmat_storage), ALLOCATABLE, INTENT(OUT) :: cmat(:,:)
    !* test seam
    TYPE(seam_type), INTENT(OUT) :: seam
    !* test id to set up
    INTEGER(i4), INTENT(IN) :: itest
    !* if present skip matrix initialization
    LOGICAL, OPTIONAL, INTENT(IN) :: skip_mat

    SELECT CASE(test_type)
    CASE ("rect_2D")
      CALL setup_rect_2D
    CASE ("rect_2D_acc")
      CALL setup_rect_2D_acc
    CASE DEFAULT
      CALL par%nim_stop('setup_linalg:: invalid test_type')
    END SELECT
  CONTAINS

!-------------------------------------------------------------------------------
!*  set up rect_2D structures for testing
!-------------------------------------------------------------------------------
    SUBROUTINE setup_rect_2D
      USE mat_rect_2D_mod
      USE vec_rect_2D_mod
      IMPLICIT NONE

      INTEGER(i4), DIMENSION(ntest_rect_2D) :: mx,my,nxbl,nybl,nqty,nmodes,     &
                                               poly_degree,nlayers
      CHARACTER(5) :: periodicity(ntest_rect_2D)
      CHARACTER(1), ALLOCATABLE :: vcomp(:)
      LOGICAL :: r0left(ntest_rect_2D)
      INTEGER(i4) :: ibl,nbl,imode
      INTEGER(i4), ALLOCATABLE :: mxbl_arr(:),mybl_arr(:)

!-------------------------------------------------------------------------------
!     test parameters
!-------------------------------------------------------------------------------
      mx=[1,2,2,4,5,5,7]
      my=[1,3,2,4,8,4,6]
      nxbl=[1,1,2,1,2,4,4]
      nybl=[1,1,1,2,2,2,5]
      nqty=[2,1,4,3,3,1,1]
      nmodes=[1,6,1,3,2,4,2]
      poly_degree=[2,3,4,5,6,7,1]
      periodicity=['none ','both ','y-dir','none ','both ','y-dir','none ']
      r0left=[.true.,.false.,.true.,.false.,.false.,.true.,.true.]
      nlayers=[1,2,1,3,2,4,1]
!-------------------------------------------------------------------------------
!     if true restrict to a single block
!-------------------------------------------------------------------------------
      IF (single_block) THEN
        nxbl(itest)=1_i4
        nybl(itest)=1_i4
      ENDIF
!-------------------------------------------------------------------------------
!     ensure test parameters are compatible with number of MPI procs
!-------------------------------------------------------------------------------
      IF (MOD(par%nprocs,nlayers(itest))/=0) nlayers(itest)=1_i4
      IF (nxbl(itest)*nybl(itest)*nlayers(itest)<par%nprocs)                    &
        nybl(itest)=par%nprocs
!-------------------------------------------------------------------------------
!     set up the seam
!-------------------------------------------------------------------------------
      nbl=nxbl(itest)*nybl(itest)
      par%nlayers=nlayers(itest)
      par%decompflag=MOD(itest,2)
      CALL par%set_decomp(nmodes(itest),nbl)
      ALLOCATE(mxbl_arr(nxbl(itest)),mybl_arr(nybl(itest)))
      mxbl_arr=mx(itest)
      mybl_arr=my(itest)
      CALL seam_rect_alloc(seam,nxbl(itest),nybl(itest),mxbl_arr,mybl_arr,      &
                           TRIM(periodicity(itest)),r0left(itest))
      DEALLOCATE(mxbl_arr,mybl_arr)
      IF (par%nprocs>1) CALL seam%trim_to_local
      CALL seam%init(nqty(itest),nqty(itest),poly_degree(itest)-1_i4,           &
                     nmodes(itest))
!-------------------------------------------------------------------------------
!     set up the linear algebra structures
!-------------------------------------------------------------------------------
      ALLOCATE(rvec(par%nbl),cv1m(par%nbl),cvec(par%nbl))
      IF (.NOT.PRESENT(skip_mat)) THEN
        ALLOCATE(rmat(par%nbl),cmat(par%nbl,par%nmodes))
        ALLOCATE(vcomp(nqty(itest)))
        CALL set_test_vcomp(vcomp,nqty(itest))
      ENDIF
      DO ibl=1,nbl
        ALLOCATE(vec_rect_2D_real::rvec(ibl)%v)
        ASSOCIATE (rvector=>rvec(ibl)%v)
          SELECT TYPE (rvector)
          TYPE IS (vec_rect_2D_real)
            CALL rvector%alloc(poly_degree(itest),mx(itest),my(itest),          &
                               nqty(itest),ibl)
          END SELECT
        END ASSOCIATE
        ALLOCATE(vec_rect_2D_comp::cvec(ibl)%v)
        ASSOCIATE (cvector=>cvec(ibl)%v)
          SELECT TYPE (cvector)
          TYPE IS (vec_rect_2D_comp)
            CALL cvector%alloc(poly_degree(itest),mx(itest),my(itest),          &
                               nqty(itest),nmodes(itest),ibl)
          END SELECT
          CALL cvector%alloc_with_mold(cv1m(ibl)%v)
        END ASSOCIATE
        IF (.NOT.PRESENT(skip_mat)) THEN
          ALLOCATE(mat_rect_2D_real::rmat(ibl)%m)
          ASSOCIATE (rmatrix=>rmat(ibl)%m)
            SELECT TYPE (rmatrix)
            TYPE IS (mat_rect_2D_real)
              CALL rmatrix%alloc(poly_degree(itest),mx(itest),my(itest),        &
                                 nqty(itest),ibl,vcomp)
            END SELECT
          END ASSOCIATE
          DO imode=1,par%nmodes
            ALLOCATE(mat_rect_2D_cm1m::cmat(ibl,imode)%m)
            ASSOCIATE (cmatrix=>cmat(ibl,imode)%m)
              SELECT TYPE (cmatrix)
              TYPE IS (mat_rect_2D_cm1m)
                CALL cmatrix%alloc(poly_degree(itest),mx(itest),my(itest),      &
                                   nqty(itest),ibl,vcomp)
              END SELECT
            END ASSOCIATE
          ENDDO
        ENDIF
      ENDDO
!-------------------------------------------------------------------------------
!     set block-specific seam variables to finish seam initialization
!-------------------------------------------------------------------------------
      DO ibl=1,nbl
        CALL rvec(ibl)%v%set_edge_vars(seam%s(ibl))
        CALL set_edge_norm_tang_rect(seam%s(ibl),mx(itest),my(itest),           &
                                     poly_degree(itest)-1_i4)
      ENDDO
    END SUBROUTINE setup_rect_2D

!-------------------------------------------------------------------------------
!*  set up rect_2D_acc structures for testing
!-------------------------------------------------------------------------------
    SUBROUTINE setup_rect_2D_acc
      USE mat_rect_2D_mod_acc
      USE vec_rect_2D_mod_acc
      IMPLICIT NONE

      INTEGER(i4), DIMENSION(ntest_rect_2D) :: mx,my,nxbl,nybl,nqty,nmodes,     &
                                               poly_degree,nlayers
      CHARACTER(5) :: periodicity(ntest_rect_2D)
      CHARACTER(1), ALLOCATABLE :: vcomp(:)
      LOGICAL :: r0left(ntest_rect_2D)
      INTEGER(i4) :: ibl,nbl,imode
      INTEGER(i4), ALLOCATABLE :: mxbl_arr(:),mybl_arr(:)

!-------------------------------------------------------------------------------
!     test parameters
!-------------------------------------------------------------------------------
      mx=[1,2,2,4,5,5,7]
      my=[1,3,2,4,8,4,6]
      nxbl=[1,1,2,1,2,4,4]
      nybl=[1,1,1,2,2,2,5]
      nqty=[2,1,4,3,3,1,1]
      nmodes=[1,6,1,3,2,4,2]
      poly_degree=[2,3,4,5,6,7,1]
      periodicity=['none ','both ','y-dir','none ','both ','y-dir','none ']
      r0left=[.true.,.false.,.true.,.false.,.false.,.true.,.true.]
      nlayers=[1,2,1,3,2,4,1]
!-------------------------------------------------------------------------------
!     if true restrict to a single block
!-------------------------------------------------------------------------------
      IF (single_block) THEN
        nxbl(itest)=1_i4
        nybl(itest)=1_i4
      ENDIF
!-------------------------------------------------------------------------------
!     ensure test parameters are compatible with number of MPI procs
!-------------------------------------------------------------------------------
      IF (MOD(par%nprocs,nlayers(itest))/=0) nlayers(itest)=1_i4
      IF (nxbl(itest)*nybl(itest)*nlayers(itest)<par%nprocs)                    &
        nybl(itest)=par%nprocs
!-------------------------------------------------------------------------------
!     set up the seam
!-------------------------------------------------------------------------------
      nbl=nxbl(itest)*nybl(itest)
      par%nlayers=nlayers(itest)
      par%decompflag=MOD(itest,2)
      CALL par%set_decomp(nmodes(itest),nbl)
      ALLOCATE(mxbl_arr(nxbl(itest)),mybl_arr(nybl(itest)))
      mxbl_arr=mx(itest)
      mybl_arr=my(itest)
      CALL seam_rect_alloc(seam,nxbl(itest),nybl(itest),mxbl_arr,mybl_arr,      &
                           TRIM(periodicity(itest)),r0left(itest))
      DEALLOCATE(mxbl_arr,mybl_arr)
      IF (par%nprocs>1) CALL seam%trim_to_local
      DO ibl=1,nbl
        seam%s(ibl)%on_gpu=on_gpu
      ENDDO
      CALL seam%init(nqty(itest),nqty(itest),poly_degree(itest)-1_i4,           &
                     nmodes(itest))
!-------------------------------------------------------------------------------
!     set up the linear algebra structures
!-------------------------------------------------------------------------------
      ALLOCATE(rvec(par%nbl),cv1m(par%nbl),cvec(par%nbl))
      IF (.NOT.PRESENT(skip_mat)) THEN
        ALLOCATE(rmat(par%nbl),cmat(par%nbl,par%nmodes))
        ALLOCATE(vcomp(nqty(itest)))
        CALL set_test_vcomp(vcomp,nqty(itest))
      ENDIF
      DO ibl=1,nbl
        ALLOCATE(vec_rect_2D_real_acc::rvec(ibl)%v)
        ASSOCIATE (rvector=>rvec(ibl)%v)
          SELECT TYPE (rvector)
          TYPE IS (vec_rect_2D_real_acc)
            CALL rvector%alloc(poly_degree(itest),mx(itest),my(itest),          &
                               nqty(itest),ibl,on_gpu)
          END SELECT
        END ASSOCIATE
        ALLOCATE(vec_rect_2D_comp_acc::cvec(ibl)%v)
        ASSOCIATE (cvector=>cvec(ibl)%v)
          SELECT TYPE (cvector)
          TYPE IS (vec_rect_2D_comp_acc)
            CALL cvector%alloc(poly_degree(itest),mx(itest),my(itest),          &
                               nqty(itest),nmodes(itest),ibl,on_gpu)
          END SELECT
          CALL cvector%alloc_with_mold(cv1m(ibl)%v)
        END ASSOCIATE
        IF (.NOT.PRESENT(skip_mat)) THEN
          ALLOCATE(mat_rect_2D_real_acc::rmat(ibl)%m)
          ASSOCIATE (rmatrix=>rmat(ibl)%m)
            SELECT TYPE (rmatrix)
            TYPE IS (mat_rect_2D_real_acc)
              CALL rmatrix%alloc(poly_degree(itest),mx(itest),my(itest),        &
                                 nqty(itest),ibl,on_gpu,vcomp)
            END SELECT
          END ASSOCIATE
          DO imode=1,par%nmodes
            ALLOCATE(mat_rect_2D_cm1m_acc::cmat(ibl,imode)%m)
            ASSOCIATE (cmatrix=>cmat(ibl,imode)%m)
              SELECT TYPE (cmatrix)
              TYPE IS (mat_rect_2D_cm1m_acc)
                CALL cmatrix%alloc(poly_degree(itest),mx(itest),my(itest),      &
                                   nqty(itest),ibl,on_gpu,vcomp)
              END SELECT
            END ASSOCIATE
          ENDDO
        ENDIF
      ENDDO
!-------------------------------------------------------------------------------
!     set block-specific seam variables to finish seam initialization
!-------------------------------------------------------------------------------
      DO ibl=1,nbl
        CALL rvec(ibl)%v%set_edge_vars(seam%s(ibl))
        CALL set_edge_norm_tang_rect(seam%s(ibl),mx(itest),my(itest),           &
                                     poly_degree(itest)-1_i4)
      ENDDO
    END SUBROUTINE setup_rect_2D_acc

!-------------------------------------------------------------------------------
!*  set matrix vcomp based on nqty for testing
!-------------------------------------------------------------------------------
    SUBROUTINE set_test_vcomp(vcomp,nqty)
      IMPLICIT NONE

      !* vcomp array to set
      CHARACTER(1), INTENT(OUT) :: vcomp(nqty)
      !* size of vcomp array
      INTEGER(i4), INTENT(IN) :: nqty

      INTEGER(i4) :: iq

      IF (nqty>=3) THEN
        vcomp(1)='r'
        vcomp(2)='z'
        vcomp(3)='p'
        DO iq=4,nqty
          vcomp(iq)='s'
        ENDDO
      ELSE
        DO iq=1,nqty
          vcomp(iq)='s'
        ENDDO
      ENDIF
    END SUBROUTINE set_test_vcomp

  END SUBROUTINE setup_linalg

!-------------------------------------------------------------------------------
!* program to tear down linear algebra structures for testing
!-------------------------------------------------------------------------------
  SUBROUTINE teardown_linalg(rvec,cv1m,cvec,rmat,cmat,seam)
    USE vector_mod
    USE matrix_mod
    USE seam_mod
    IMPLICIT NONE

    !* test rvec
    TYPE(rvec_storage), ALLOCATABLE, INTENT(INOUT) :: rvec(:)
    !* test cv1m
    TYPE(cv1m_storage), ALLOCATABLE, INTENT(INOUT) :: cv1m(:)
    !* test cvec
    TYPE(cvec_storage), ALLOCATABLE, INTENT(INOUT) :: cvec(:)
    !* test rmat
    TYPE(rmat_storage), ALLOCATABLE, INTENT(INOUT) :: rmat(:)
    !* test cmat
    TYPE(cmat_storage), ALLOCATABLE, INTENT(INOUT) :: cmat(:,:)
    !* test seam
    TYPE(seam_type), INTENT(INOUT) :: seam

    INTEGER(i4) :: ibl,imode

    IF (ALLOCATED(rvec)) THEN
      DO ibl=1,par%nbl
        IF (ALLOCATED(rvec(ibl)%v)) CALL rvec(ibl)%v%dealloc
      ENDDO
      DEALLOCATE(rvec)
    ENDIF
    IF (ALLOCATED(cv1m)) THEN
      DO ibl=1,par%nbl
        IF (ALLOCATED(cv1m(ibl)%v)) CALL cv1m(ibl)%v%dealloc
      ENDDO
      DEALLOCATE(cv1m)
    ENDIF
    IF (ALLOCATED(cvec)) THEN
      DO ibl=1,par%nbl
        IF (ALLOCATED(cvec(ibl)%v)) CALL cvec(ibl)%v%dealloc
      ENDDO
      DEALLOCATE(cvec)
    ENDIF
    IF (ALLOCATED(rmat)) THEN
      DO ibl=1,par%nbl
        IF (ALLOCATED(rmat(ibl)%m)) CALL rmat(ibl)%m%dealloc
      ENDDO
      DEALLOCATE(rmat)
    ENDIF
    IF (ALLOCATED(cmat)) THEN
      DO imode=1,par%nmodes
        DO ibl=1,par%nbl
          IF (ALLOCATED(cmat(ibl,imode)%m)) CALL cmat(ibl,imode)%m%dealloc
        ENDDO
      ENDDO
      DEALLOCATE(cmat)
    ENDIF
    CALL seam%dealloc
    CALL par%dealloc
    !$acc wait
  END SUBROUTINE teardown_linalg

END MODULE linalg_registry_mod
