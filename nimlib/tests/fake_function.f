MODULE fake_function_mod
  USE local
  USE function_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: fake_function_1D

  TYPE, EXTENDS(function_1D) :: fake_function_1D
    PRIVATE

    INTEGER(i4) :: pd = 0
  CONTAINS

    PROCEDURE, PASS(func) :: eval => fake_eval_1D
    PROCEDURE, PASS(func) :: eval_d => fake_eval_d_1D
    PROCEDURE, PASS(func) :: set_pd => set_pd_1D
  END TYPE fake_function_1D

CONTAINS

  ELEMENTAL REAL(r8) FUNCTION fake_eval_1D(func, input) RESULT(output)
    IMPLICIT NONE

    CLASS(fake_function_1D), INTENT(IN) :: func
    REAL(r8), INTENT(IN) :: input

    REAL(r8), PARAMETER, DIMENSION(0:9) ::                                      &
      coef=[1.,0.5,0.2,0.1,0.05,0.02,0.01,0.005,0.002,0.001]
    INTEGER(i4) :: ip

    output = 0._r8
    DO ip = 0, MIN(func%pd,9)
      output = output + coef(ip)*input**ip
    ENDDO
  END FUNCTION fake_eval_1D

  ELEMENTAL REAL(r8) FUNCTION fake_eval_d_1D(func, input) RESULT(output)
    IMPLICIT NONE

    CLASS(fake_function_1D), INTENT(IN) :: func
    REAL(r8), INTENT(IN) :: input

    REAL(r8), PARAMETER, DIMENSION(0:9) ::                                      &
      coef=[1.,0.5,0.2,0.1,0.05,0.02,0.01,0.005,0.002,0.001]
    INTEGER(i4) :: ip

    output = 0._r8
    DO ip = 0, MIN(func%pd,9)
      output = output + ip*coef(ip)*input**(ip-1)
    ENDDO
  END FUNCTION fake_eval_d_1D

  SUBROUTINE set_pd_1D(func, poly_degree)
    IMPLICIT NONE

    CLASS(fake_function_1D), INTENT(INOUT) :: func
    INTEGER(i4), INTENT(IN) :: poly_degree

    func%pd = poly_degree
  END SUBROUTINE set_pd_1D

END MODULE fake_function_mod
