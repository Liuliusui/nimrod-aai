!-------------------------------------------------------------------------------
!< module containing routines that performs standard operations on
!  rectilinear matrices.
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!> module containing routines that performs standard operations on
!  rectilinear matrices.
!-------------------------------------------------------------------------------
MODULE mat_rect_2D_mod
  USE local
  USE mat_rect_2D_real_mod
  USE mat_rect_2D_cm1m_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: mat_rect_2D_real,mat_rect_2D_cm1m

END MODULE mat_rect_2D_mod
