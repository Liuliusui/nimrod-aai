#!/bin/bash

# trixie-base must be first
# bookworm-base only for nvhpc-slim and can be removed when nvhpc works with trixie
Dockerfile_list="Dockerfile-trixie-base Dockerfile-trixie-mpich Dockerfile-trixie-openmpi Dockerfile-aocc-slim Dockerfile-intel-slim Dockerfile-bookworm-base Dockerfile-nvhpc-slim"

if test -e "Dockerfile"
then
  rm Dockerfile
fi

for Dockerfile in $Dockerfile_list
do
  echo "Processing $Dockerfile"
  ln -s $Dockerfile Dockerfile
  image_tag=`echo $Dockerfile | sed -e 's/Dockerfile-//'`
  docker build -t registry.gitlab.com/nimrodteam/open/nimrod-aai:$image_tag .
  docker push registry.gitlab.com/nimrodteam/open/nimrod-aai:$image_tag
  rm Dockerfile
done
