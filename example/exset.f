!-------------------------------------------------------------------------------
!! setup an initial condition for the Laplace test
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* setup an initial condition for the Laplace test
!-------------------------------------------------------------------------------
PROGRAM exset
  USE dump_mod
  USE exset_input
  USE fourier_input_mod
  USE gblock_mod
  USE grid_input_mod
  USE grid_utils_mod
  USE h1_rect_2D_mod
  USE init_function_mod
  USE io
  USE local
  USE nodal_mod
  USE pardata_mod
  USE seam_mod
  USE timer_mod
  IMPLICIT NONE

  CHARACTER(128) :: input_file='example.in'
  INTEGER(i4) :: nargs,im,ibl
  TYPE(block_storage), ALLOCATABLE :: bl(:)
  TYPE(seam_type) :: seam
  TYPE(rfem_storage), ALLOCATABLE, TARGET :: rfield(:)
  TYPE(cfem_storage), ALLOCATABLE, TARGET :: cfield(:)
  TYPE(init_function_real) :: init_func_real
  TYPE(init_function_comp) :: init_func_comp
  TYPE(field_list_pointer) :: io_field_list(1)
  REAL(r8) :: bessel_root(2),bessel_val(2)
  REAL(r8), PARAMETER :: sqrt2=SQRT(2._r8)
  INTEGER(i4) :: idepth
  INTEGER(i4), SAVE :: iftn=-1

  ofname='exset.out'
  CALL par%init
  CALL fch5_init
  CALL timer%init
  CALL timer%start_timer_l0('exset','exset',iftn,idepth)
!-------------------------------------------------------------------------------
! determine configuration from the command line
!-------------------------------------------------------------------------------
  nargs=command_argument_count()
  IF (nargs==1) THEN
    CALL get_command_argument(1,input_file)
  ENDIF
  IF (input_file=='-h'.OR.nargs>1) THEN
    CALL print_usage
  ENDIF
!-------------------------------------------------------------------------------
! read input
!-------------------------------------------------------------------------------
  CALL par%nim_write('Reading input.')
  CALL exset_read_namelist(input_file)
  init_func_real%init_type=init_type
  init_func_real%kx=nx*twopi
  init_func_real%ky=ny*twopi
  init_func_real%bessel_order=bessel_order
  init_func_comp%init_type=init_type
  init_func_comp%kx=nx*twopi
  init_func_comp%ky=ny*twopi
  init_func_comp%bessel_order=bessel_order
!-------------------------------------------------------------------------------
! check input
!-------------------------------------------------------------------------------
  IF (field_type/='comp'.AND.fourier_in%nmodes_total>1)                         &
    CALL par%nim_stop('nmodes>1 requires field_type=comp',clean_shutdown=.TRUE.)
!-------------------------------------------------------------------------------
! find the zeros of the bessel function if using bessel init type
!-------------------------------------------------------------------------------
  bessel_root(1)=0._r8
  bessel_root(2)=1._r8
  IF (init_type(1:6)=="bessel") THEN
    CALL par%nim_write('Finding Bessel function cutoff radii for grid.')
    IF (grid_in%annulus) THEN
      bessel_root(1)=2.4048_r8/init_func_real%kx ! 1st zero crossing for J0
      CALL init_func_real%find_root(bessel_root(1:1),bessel_val(1:1))
    ELSE
      bessel_root(1)=0._r8
    ENDIF
    bessel_root(2)=11.7915_r8/init_func_real%kx ! 4th zero crossing for J0
    CALL init_func_real%find_root(bessel_root(2:2),bessel_val(2:2))
  ENDIF
  grid_in%xmin=bessel_root(1)
  grid_in%xmax=bessel_root(2)
!-------------------------------------------------------------------------------
! initialize pardata
!-------------------------------------------------------------------------------
  CALL par%nim_write('Setting up the mesh.')
  CALL par%set_decomp(fourier_in%nmodes_total,grid_in%nbl_total)
  ALLOCATE(par%keff_total(fourier_in%nmodes_total))
  DO im=1,fourier_in%nmodes_total
    par%keff_total(im)=(im-1)*twopi*grid_in%zperiod
    !TODO: initialize and write nindex here too, it is special for linear cases
    ! which may not start at nindex=0 and and zperiod/=1
  ENDDO
!-------------------------------------------------------------------------------
! setup the grid
!-------------------------------------------------------------------------------
  CALL setup_bl_mesh(bl,seam)
!-------------------------------------------------------------------------------
! set fields
!-------------------------------------------------------------------------------
  CALL par%nim_write('Initializing fields.')
  ALLOCATE(rfield(grid_in%nbl_total),cfield(grid_in%nbl_total))
  DO ibl=1,grid_in%nbl_total
    SELECT TYPE(mesh => bl(ibl)%b%mesh)
    CLASS IS (h1_rect_2D_real)
      SELECT CASE(field_type)
      CASE("real")
        ALLOCATE(h1_rect_2D_real::rfield(ibl)%f)
        SELECT TYPE(fld => rfield(ibl)%f)
        TYPE IS (h1_rect_2D_real)
          CALL fld%alloc(mesh%mx,mesh%my,nqty,grid_in%poly_degree,ibl,"field")
        END SELECT
        CALL rfield(ibl)%f%collocation(init_func_real,bl(ibl)%b%mesh)
      CASE("comp")
        ALLOCATE(h1_rect_2D_comp::cfield(ibl)%f)
        SELECT TYPE(fld => cfield(ibl)%f)
        TYPE IS (h1_rect_2D_comp)
          CALL fld%alloc(mesh%mx,mesh%my,nqty,fourier_in%nmodes_total,          &
                         grid_in%poly_degree,ibl,"field")
        END SELECT
        CALL cfield(ibl)%f%collocation(init_func_comp,bl(ibl)%b%mesh)
      END SELECT
    END SELECT
  ENDDO
!-------------------------------------------------------------------------------
! write dump file
!-------------------------------------------------------------------------------
  CALL par%nim_write('Writing initial dump file.')
  io_field_list(1)%name="field"
  SELECT CASE(field_type)
  CASE("real")
    io_field_list(1)%p=>rfield
  CASE("comp")
    io_field_list(1)%p=>cfield
  END SELECT
  CALL dump_write(0._r8,istep0,bl,seam,io_field_list)
!-------------------------------------------------------------------------------
! deallocate and finalize
!-------------------------------------------------------------------------------
  CALL seam%dealloc
  DO ibl=1,grid_in%nbl_total
    IF (ALLOCATED(rfield(ibl)%f)) CALL rfield(ibl)%f%dealloc
    IF (ALLOCATED(cfield(ibl)%f)) CALL cfield(ibl)%f%dealloc
    CALL bl(ibl)%b%dealloc
  ENDDO
  DEALLOCATE(bl,rfield,cfield)
  CALL timer%end_timer_l0(iftn,idepth)
  CALL timer%report
  CALL timer%finalize
  CALL fch5_dealloc
  CALL par%nim_stop('Normal termination.',clean_shutdown=.TRUE.)
CONTAINS

!-------------------------------------------------------------------------------
! helper routine to print usage
!-------------------------------------------------------------------------------
  SUBROUTINE print_usage
    CALL par%nim_write('Initialize a field f with a given grid decomposition')
    CALL par%nim_write('Usage: ./exset <input namelist file>')
    CALL par%nim_write(                                                         &
      'Default input namelist file name is example.in if not specified')
    CALL par%nim_stop('check input',clean_shutdown=.TRUE.)
  END SUBROUTINE print_usage

END PROGRAM exset
