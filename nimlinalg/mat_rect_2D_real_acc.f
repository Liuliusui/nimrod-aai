!-------------------------------------------------------------------------------
!< module containing routines that performs standard operations on
!  real rectilinear matrices.
!-------------------------------------------------------------------------------
#include "config.f"
!-------------------------------------------------------------------------------
!> module containing routines that performs standard operations on
!  real rectilinear matrices.
!-------------------------------------------------------------------------------
MODULE mat_rect_2D_real_mod_acc
  USE local
  USE matrix_mod
#ifdef HAVE_OPENACC
  USE openacc
#endif
  USE pardata_mod
  USE timer_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: mat_rect_2D_real_acc

  CHARACTER(*), PARAMETER :: mod_name='mat_rect_2D_real_acc'
!-------------------------------------------------------------------------------
!* holds rblock real matrix elements.
!-------------------------------------------------------------------------------
  TYPE :: arr_6d_type
    !> arrays of size (nq_type,x0off:x1off,y0off:y1off,nq_type,ix0:mx,iy0:my)
    REAL(r8), DIMENSION(:,:,:,:,:,:), ALLOCATABLE :: arr
  END TYPE arr_6d_type

!-------------------------------------------------------------------------------
!* 2D rectangular matrix algebra implementation with a real data type
!-------------------------------------------------------------------------------
  TYPE, EXTENDS(rmatrix) :: mat_rect_2D_real_acc
    !> number of continuous quantites
    INTEGER(i4) :: nqcon
    !> poly degree of continuous basis (rename)
    INTEGER(i4) :: pdcon
    !> number of discontinuous quantites
    INTEGER(i4) :: nqdis
    !> number of discontinuous basis
    INTEGER(i4) :: nbdisc
    !> unit number of continuous degrees of freedom (per element for a scalar)
    INTEGER(i4) :: u_ndof_cont
    !> unit number of discontinuous degrees of freedom
    !  (per element for a scalar)
    INTEGER(i4) :: u_ndof_disc
    !> number of basis types (1: interior only or 4: vert, side, int)
    INTEGER(i4) :: nbtype
    !> number of elements in the logical x direction
    INTEGER(i4) :: mx
    !> number of elements in the logical y direction
    INTEGER(i4) :: my
    !> starting logical-x element index by basis type
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: ix0
    !> starting logical-y element index by basis type
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: iy0
    !> starting x offset index by basis type pairs
    INTEGER(i4), DIMENSION(:,:), ALLOCATABLE :: x0off
    !> ending x offset index by basis type pairs
    INTEGER(i4), DIMENSION(:,:), ALLOCATABLE :: x1off
    !> starting y offset index by basis type pairs
    INTEGER(i4), DIMENSION(:,:), ALLOCATABLE :: y0off
    !> ending y offset index by basis type pairs
    INTEGER(i4), DIMENSION(:,:), ALLOCATABLE :: y1off
    !> number of degrees of freedom for each basis type; this is nqty for
    !  vertices, nqty*nside for sides and nqty*nint for interior nodes
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: nq_type
    !> number of boundary degrees of freedom for each basis type
    INTEGER(i4), DIMENSION(:), ALLOCATABLE :: nb_type
    !> matrix storage by basis type connectivity by basis type pairs
    TYPE(arr_6d_type), DIMENSION(:,:), ALLOCATABLE :: bsc
#ifdef HAVE_OPENACC
    !> interior inverse (nq_type,x0off:x1off,y0off:y1off,nq_type,ix0:mx,iy0:my)
    REAL(r8), DIMENSION(:,:,:,:,:,:), ALLOCATABLE :: imat44
    !* device pointers to interior matrices
    TYPE(c_devptr), ALLOCATABLE :: dptr_mat44(:)
    !* device pointers to interior matrices inverse
    TYPE(c_devptr), ALLOCATABLE :: dptr_imat44(:)
#endif
  CONTAINS

    ! Type-bound extensions
    PROCEDURE, PASS(mat) :: alloc => alloc_real
    ! Abstract class deferred functions
    PROCEDURE, PASS(mat) :: dealloc => dealloc_real
    PROCEDURE, PASS(mat) :: matvec => matvec_real
    PROCEDURE, PASS(mat) :: assemble => assemble_real
    PROCEDURE, PASS(mat) :: zero => zero_real
    PROCEDURE, PASS(mat) :: elim_inv_int => elim_inv_int_real
    PROCEDURE, PASS(mat) :: elim_presolve => elim_presolve_real
    PROCEDURE, PASS(mat) :: elim_postsolve => elim_postsolve_real
    PROCEDURE, PASS(mat) :: find_diag_scale => find_diag_scale_real
    PROCEDURE, PASS(mat) :: dirichlet_bc => dirichlet_bc_real
    PROCEDURE, PASS(mat) :: regularity => regularity_real
    PROCEDURE, PASS(mat) :: get_diag_as_vec => get_diag_as_vec_real
    PROCEDURE, PASS(mat) :: set_identity => set_identity_real
  END TYPE mat_rect_2D_real_acc

CONTAINS

!-------------------------------------------------------------------------------
!* allocate arrays needed for a real rblock matrix.
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_real(mat,pdc,mx,my,nqc,id,on_gpu,vcomp,nqdin,nbdin)
    IMPLICIT NONE

    !> matrix to allocate
    CLASS(mat_rect_2D_real_acc), INTENT(INOUT) :: mat
    !> polynomial degree (continuous basis)
    INTEGER(i4), INTENT(IN) :: pdc
    !> number of elements in the horizontal direction
    INTEGER(i4), INTENT(IN) :: mx
    !> number of elements in the vertical direction
    INTEGER(i4), INTENT(IN) :: my
    !> number of quantities (continuous basis)
    INTEGER(i4), INTENT(IN) :: nqc
    !> ID for parallel streams
    INTEGER(i4), INTENT(IN) :: id
    !> true if data on GPU
    LOGICAL, INTENT(IN) :: on_gpu
    !> character describing vector components / scalar
    CHARACTER(1), INTENT(IN) :: vcomp(:)
    !> number of quantities (discontinuous basis)
    INTEGER(i4), INTENT(IN), OPTIONAL :: nqdin
    !> number of discontinuous basis
    INTEGER(i4), INTENT(IN), OPTIONAL :: nbdin

    INTEGER(i4) :: ib,jb,ixst,iyst,jxst,jyst,nqd,nbd
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_real',iftn,idepth)
    IF (PRESENT(nqdin)) THEN
      nqd=nqdin
    ELSE
      nqd=0_i4
    ENDIF
    IF (PRESENT(nbdin)) THEN
      nbd=nbdin
    ELSE
      nbd=0_i4
    ENDIF
    mat%nqty=nqc+nqd
    mat%nqcon=nqc
    mat%pdcon=pdc
    mat%nel=mx*my
    mat%ndim=2
    mat%nqdis=nqd
    mat%nbdisc=nbd
    mat%id=id
    mat%on_gpu=on_gpu
    ALLOCATE(mat%vcomp(mat%nqty))
    mat%vcomp=vcomp
    mat%eliminated=.FALSE.
    mat%diag_scale=1._r8
    mat%essential_cond="none"
!-------------------------------------------------------------------------------
!   the mat array allows multiple basis function types (grid vertex,
!   horizontal element side, vertical element side, and interior-
!   centered).
!
!   the structures accommodate equations with nqc continuous fields
!   of polynomial degree pdc and nqd discontinuous fields with
!   nbd basis functions.  the storage for 'element interiors'
!   is used for both the interior coefficients of continuous fields
!   and for all coefficients of discontinuous fields.
!
!   if poly_degree=1, there is only one basis type.  otherwise, there
!   are 4.
!-------------------------------------------------------------------------------
    mat%u_ndof_cont=nqc*(pdc+1)**2
    mat%u_ndof_disc=nqd*nbd
    mat%u_ndof=mat%u_ndof_cont+mat%u_ndof_disc
    IF (pdc==1.AND.nqd==0) THEN
      mat%nbtype=1
    ELSE
      mat%nbtype=4
    ENDIF
    mat%mx=mx
    mat%my=my
    !$acc enter data copyin(mat) async(mat%id) if(mat%on_gpu)
    !$acc enter data copyin(mat%vcomp) async(mat%id) if(mat%on_gpu)
    ALLOCATE(mat%bsc(mat%nbtype,mat%nbtype))
    !$acc enter data create(mat%bsc) async(mat%id) if(mat%on_gpu)
    ALLOCATE(mat%ix0(mat%nbtype))
    ALLOCATE(mat%iy0(mat%nbtype))
    ALLOCATE(mat%x0off(mat%nbtype,mat%nbtype),mat%x1off(mat%nbtype,mat%nbtype))
    ALLOCATE(mat%y0off(mat%nbtype,mat%nbtype),mat%y1off(mat%nbtype,mat%nbtype))
    ALLOCATE(mat%nb_type(mat%nbtype))
    ALLOCATE(mat%nq_type(mat%nbtype))
!-------------------------------------------------------------------------------
!   logical indices for each of the basis types.
!   -  index 1 is grid vertex-centered.
!   -  index 2 is horizontal side-centered.
!   -  index 3 is vertical side-centered.
!   -  index 4 is interior-centered and all discontinuous bases.
!
!   this version has the 6D matrix array indices defined
!   (col_comp,col_x_off,col_y_off,row_comp,row_x_index,row_y_index),
!   where comp is vector component and basis for types with multiple
!   bases (vector component varying faster) and off is the offset
!   from the row index.
!-------------------------------------------------------------------------------
    DO ib=1,mat%nbtype
      SELECT CASE(ib)
      CASE(1)
        mat%ix0(ib)=0
        mat%iy0(ib)=0
        mat%nb_type(ib)=MIN(nqc,1_i4)
        mat%nq_type(ib)=nqc
      CASE(2)
        mat%ix0(ib)=1
        mat%iy0(ib)=0
        mat%nb_type(ib)=MIN(nqc,1_i4)*(pdc-1)
        mat%nq_type(ib)=nqc*(pdc-1)
      CASE(3)
        mat%ix0(ib)=0
        mat%iy0(ib)=1
        mat%nb_type(ib)=MIN(nqc,1_i4)*(pdc-1)
        mat%nq_type(ib)=nqc*(pdc-1)
      CASE(4)
        mat%ix0(ib)=1
        mat%iy0(ib)=1
        mat%nb_type(ib)=MIN(nqc,1_i4)*(pdc-1)**2+MIN(nqd,1_i4)*nbd
        mat%nq_type(ib)=nqc*(pdc-1)**2+nqd*nbd
      END SELECT
    ENDDO
    DO ib=1,mat%nbtype
      ixst=mat%ix0(ib)
      iyst=mat%iy0(ib)
      DO jb=1,mat%nbtype
        jxst=mat%ix0(jb)
        jyst=mat%iy0(jb)
        mat%x0off(jb,ib)=jxst-1
        mat%x1off(jb,ib)=1-ixst
        mat%y0off(jb,ib)=jyst-1
        mat%y1off(jb,ib)=1-iyst
        ALLOCATE(mat%bsc(jb,ib)%arr(mat%nq_type(jb),                            &
                                    mat%x0off(jb,ib):mat%x1off(jb,ib),          &
                                    mat%y0off(jb,ib):mat%y1off(jb,ib),          &
                                    mat%nq_type(ib),ixst:mx,iyst:my))
        ASSOCIATE (arr=>mat%bsc(jb,ib)%arr)
          !$acc enter data create(arr) async(mat%id) if(mat%on_gpu)
        END ASSOCIATE
        mat%bsc(jb,ib)%arr=0
      ENDDO
    ENDDO
    !$acc enter data copyin(mat%ix0) async(mat%id) if(mat%on_gpu)
    !$acc enter data copyin(mat%iy0) async(mat%id) if(mat%on_gpu)
    !$acc enter data copyin(mat%x0off) async(mat%id) if(mat%on_gpu)
    !$acc enter data copyin(mat%x1off) async(mat%id) if(mat%on_gpu)
    !$acc enter data copyin(mat%y0off) async(mat%id) if(mat%on_gpu)
    !$acc enter data copyin(mat%y1off) async(mat%id) if(mat%on_gpu)
    !$acc enter data copyin(mat%nb_type) async(mat%id) if(mat%on_gpu)
    !$acc enter data copyin(mat%nq_type) async(mat%id) if(mat%on_gpu)
!-------------------------------------------------------------------------------
!   create device pointers for batched inversion
!-------------------------------------------------------------------------------
#ifdef HAVE_OPENACC
    IF (mat%nbtype>1) THEN
      ALLOCATE(mat%imat44(mat%nq_type(4),mat%x0off(4,4):mat%x1off(4,4),         &
                          mat%y0off(4,4):mat%y1off(4,4),mat%nq_type(4),         &
                          1:mat%mx,1:mat%my))
      ALLOCATE(mat%dptr_mat44(mat%mx*mat%my))
      ALLOCATE(mat%dptr_imat44(mat%mx*mat%my))
      ASSOCIATE (mat44=>mat%bsc(4,4)%arr,imat44=>mat%imat44,                    &
                 dptr_mat44=>mat%dptr_mat44,dptr_imat44=>mat%dptr_imat44)
        BLOCK
          INTEGER(i4) :: ix,iy,iel
          !$acc enter data create(imat44) async(id) if(mat%on_gpu)
          !$acc wait(id)
          DO iy=1,mat%my
            DO ix=1,mat%mx
              iel=ix+(iy-1)*mat%mx
              dptr_mat44(iel)=acc_deviceptr(mat44(:,:,:,:,ix,iy))
              dptr_imat44(iel)=acc_deviceptr(imat44(:,:,:,:,ix,iy))
            ENDDO
          ENDDO
          !$acc enter data copyin(dptr_mat44,dptr_imat44) async(id) if(mat%on_gpu)
        END BLOCK

      END ASSOCIATE
    ENDIF
#endif
!-------------------------------------------------------------------------------
!   register this object.
!-------------------------------------------------------------------------------
    NULLIFY(mat%mem_id)
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      INTEGER(i4) :: sz
      sz= INT(SIZEOF(mat%ix0)+SIZEOF(mat%iy0)+SIZEOF(mat%x0off)                 &
             +SIZEOF(mat%x1off)+SIZEOF(mat%y0off)+SIZEOF(mat%y1off)             &
             +SIZEOF(mat%nb_type)+SIZEOF(mat%nq_type)+SIZEOF(mat%vcomp)         &
             +SIZEOF(mat),i4)
      CALL memlogger%update(mat%mem_id,mod_name,'unknown',sz)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_real

!-------------------------------------------------------------------------------
!* deallocate arrays needed for a real rblock matrix.
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_real(mat)
    IMPLICIT NONE

    !> matrix to dealloc
    CLASS(mat_rect_2D_real_acc), INTENT(INOUT) :: mat

    INTEGER(i4) :: id,jd
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   loop over different basis combinations and deallocte.
!-------------------------------------------------------------------------------
    DO jd=1,SIZE(mat%bsc,2)
      DO id=1,SIZE(mat%bsc,1)
        ASSOCIATE (arr=>mat%bsc(id,jd)%arr)
          !$acc exit data delete(arr) finalize async(mat%id) if(mat%on_gpu)
        END ASSOCIATE
        DEALLOCATE(mat%bsc(id,jd)%arr)
      ENDDO
    ENDDO
#ifdef HAVE_OPENACC
    IF (mat%nbtype>1) THEN
      ASSOCIATE (imat44=>mat%imat44)
        !$acc exit data delete(imat44) finalize async(id) if(mat%on_gpu)
        DEALLOCATE(mat%imat44)
      END ASSOCIATE
      ASSOCIATE (dptr_mat44=>mat%dptr_mat44,dptr_imat44=>mat%dptr_imat44)
        !$acc exit data delete(dptr_mat44) finalize async(id) if(mat%on_gpu)
        DEALLOCATE(mat%dptr_mat44)
        !$acc exit data delete(dptr_imat44) finalize async(id) if(mat%on_gpu)
        DEALLOCATE(mat%dptr_imat44)
      END ASSOCIATE
    ENDIF
#endif
    !$acc exit data delete(mat%ix0) finalize async(mat%id) if(mat%on_gpu)
    !$acc exit data delete(mat%iy0) finalize async(mat%id) if(mat%on_gpu)
    !$acc exit data delete(mat%x0off) finalize async(mat%id) if(mat%on_gpu)
    !$acc exit data delete(mat%x1off) finalize async(mat%id) if(mat%on_gpu)
    !$acc exit data delete(mat%y0off) finalize async(mat%id) if(mat%on_gpu)
    !$acc exit data delete(mat%y1off) finalize async(mat%id) if(mat%on_gpu)
    !$acc exit data delete(mat%nb_type) finalize async(mat%id) if(mat%on_gpu)
    !$acc exit data delete(mat%nq_type) finalize async(mat%id) if(mat%on_gpu)
    !$acc exit data delete(mat%bsc) finalize async(mat%id) if(mat%on_gpu)
    !$acc exit data delete(mat%vcomp) finalize async(mat%id) if(mat%on_gpu)
    !$acc exit data delete(mat) finalize async(mat%id) if(mat%on_gpu)
    DEALLOCATE(mat%bsc,mat%ix0,mat%iy0)
    DEALLOCATE(mat%x0off,mat%x1off,mat%y0off,mat%y1off)
    DEALLOCATE(mat%nb_type,mat%nq_type)
    DEALLOCATE(mat%vcomp)
!-------------------------------------------------------------------------------
!   unregister this object.
!-------------------------------------------------------------------------------
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      CALL memlogger%update(mat%mem_id,mod_name,' ',0,resize=.TRUE.)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc_real

!-------------------------------------------------------------------------------
!> multiply the real mat matrix by the real operand vector,
!  and return the product.  each matrix array contains all
!  connections between two sets of basis types, so operand and
!  product arrays have basis index lumped with quantity index.
!-------------------------------------------------------------------------------
  SUBROUTINE matvec_real(mat,operand,output)
    USE vec_rect_2D_mod_acc
    USE vector_mod
    IMPLICIT NONE

    !> matrix to multiply
    CLASS(mat_rect_2D_real_acc), INTENT(IN) :: mat
    !> vector to multiply
    CLASS(rvector), INTENT(IN) :: operand
    !> result
    CLASS(rvector), INTENT(INOUT) :: output

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'matvec_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   zero output
!-------------------------------------------------------------------------------
    CALL output%zero
!-------------------------------------------------------------------------------
!   call the structured-data matrix operation for each
!   basis type to basis type pair.
!-------------------------------------------------------------------------------
    SELECT TYPE(operand)
    TYPE IS(vec_rect_2D_real_acc)
      SELECT TYPE(output)
      TYPE IS(vec_rect_2D_real_acc)
!-------------------------------------------------------------------------------
!       loop over itype => grid vertex
!-------------------------------------------------------------------------------
        IF (mat%nq_type(1)>0) THEN
          CALL matvec_kernel(output%arr,mat%bsc(1,1)%arr,operand%arr,mat,1,1)
          IF (mat%nbtype>1) THEN
            IF (mat%nq_type(2)>0) THEN
              CALL matvec_kernel(output%arr,mat%bsc(2,1)%arr,operand%arrh,      &
                                 mat,2,1)
            ENDIF
            IF (mat%nq_type(3)>0) THEN
              CALL matvec_kernel(output%arr,mat%bsc(3,1)%arr,operand%arrv,      &
                                 mat,3,1)
            ENDIF
            IF (mat%nq_type(4)>0.AND..NOT.mat%eliminated) THEN
              CALL matvec_kernel(output%arr,mat%bsc(4,1)%arr,operand%arri,      &
                                 mat,4,1)
            ENDIF
          ENDIF
        ENDIF
!-------------------------------------------------------------------------------
!       loop over itype => horizontal vertex
!-------------------------------------------------------------------------------
        IF (mat%nbtype>1) THEN
          IF (mat%nq_type(2)>0) THEN
            IF (mat%nq_type(1)>0) THEN
              CALL matvec_kernel(output%arrh,mat%bsc(1,2)%arr,operand%arr,      &
                                 mat,1,2)
            ENDIF
            IF (mat%nq_type(2)>0) THEN
              CALL matvec_kernel(output%arrh,mat%bsc(2,2)%arr,operand%arrh,     &
                                 mat,2,2)
            ENDIF
            IF (mat%nq_type(3)>0) THEN
              CALL matvec_kernel(output%arrh,mat%bsc(3,2)%arr,operand%arrv,     &
                                 mat,3,2)
            ENDIF
            IF (mat%nq_type(4)>0.AND..NOT.mat%eliminated) THEN
              CALL matvec_kernel(output%arrh,mat%bsc(4,2)%arr,operand%arri,     &
                                 mat,4,2)
            ENDIF
          ENDIF
        ENDIF
!-------------------------------------------------------------------------------
!       loop over itype => vertical vertex
!-------------------------------------------------------------------------------
        IF (mat%nbtype>1) THEN
          IF (mat%nq_type(3)>0) THEN
            IF (mat%nq_type(1)>0) THEN
              CALL matvec_kernel(output%arrv,mat%bsc(1,3)%arr,operand%arr,      &
                                 mat,1,3)
            ENDIF
            IF (mat%nq_type(2)>0) THEN
              CALL matvec_kernel(output%arrv,mat%bsc(2,3)%arr,operand%arrh,     &
                                 mat,2,3)
            ENDIF
            IF (mat%nq_type(3)>0) THEN
              CALL matvec_kernel(output%arrv,mat%bsc(3,3)%arr,operand%arrv,     &
                                 mat,3,3)
            ENDIF
            IF (mat%nq_type(4)>0.AND..NOT.mat%eliminated) THEN
              CALL matvec_kernel(output%arrv,mat%bsc(4,3)%arr,operand%arri,     &
                                 mat,4,3)
            ENDIF
          ENDIF
        ENDIF
!-------------------------------------------------------------------------------
!       loop over itype => interior vertex
!-------------------------------------------------------------------------------
        IF (mat%nbtype>1.AND..NOT.mat%eliminated) THEN
          IF (mat%nq_type(4)>0) THEN
            IF (mat%nq_type(1)>0) THEN
              CALL matvec_kernel(output%arri,mat%bsc(1,4)%arr,operand%arr,      &
                                 mat,1,4)
            ENDIF
            IF (mat%nq_type(2)>0) THEN
              CALL matvec_kernel(output%arri,mat%bsc(2,4)%arr,operand%arrh,     &
                                 mat,2,4)
            ENDIF
            IF (mat%nq_type(3)>0) THEN
              CALL matvec_kernel(output%arri,mat%bsc(3,4)%arr,operand%arrv,     &
                                 mat,3,4)
            ENDIF
            IF (mat%nq_type(4)>0) THEN
              CALL matvec_kernel(output%arri,mat%bsc(4,4)%arr,operand%arri,     &
                                 mat,4,4)
            ENDIF
          ENDIF
        ENDIF
      CLASS DEFAULT
        CALL par%nim_stop("mat_rect_2D_real_acc::matvec_real unrecognized"//    &
                          " output type")
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop("mat_rect_2D_real_acc::matvec_real unrecognized"//      &
                        " operand type")
    END SELECT
    IF (mat%on_gpu) CALL par%wait_streams(mat%id,4)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE matvec_real

!-------------------------------------------------------------------------------
!> transfer contributions from integrand into internal storage as a plus
!  equals operation (the user must call zero as needed).
!-------------------------------------------------------------------------------
  SUBROUTINE assemble_real(mat,integrand)
    USE local
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real_acc), INTENT(INOUT) :: mat
    !> integrand array
    REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

    INTEGER(i4) :: hstart,vstart,istart,dstart,nqci
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assemble_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   assemble and accumulate the contributions from each element.
!   use direct addressing to avoid a race condition on GPUs.
!   internal subroutines used to improve code readibility.
!-------------------------------------------------------------------------------
    hstart=5
    vstart=3+2*mat%pdcon
    istart=1+4*mat%pdcon
    dstart=MIN(mat%nqcon,1_i4)*(mat%pdcon+1)**2
    nqci=mat%nqcon*((mat%pdcon-1)**2)
!-------------------------------------------------------------------------------
!   loop over itype => grid vertex
!-------------------------------------------------------------------------------
    IF (mat%nq_type(1)>0) THEN
      CALL assemble_gg_real(mat%bsc(1,1)%arr,integrand)
      IF (mat%nbtype>1) THEN
        IF (mat%nq_type(2)>0) THEN
          CALL assemble_hg_real(mat%bsc(2,1)%arr,integrand)
        ENDIF
        IF (mat%nq_type(3)>0) THEN
          CALL assemble_vg_real(mat%bsc(3,1)%arr,integrand)
        ENDIF
        IF (mat%nq_type(4)>0) THEN
          CALL assemble_ig_real(mat%bsc(4,1)%arr,integrand)
        ENDIF
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   loop over itype => horizontal vertex
!-------------------------------------------------------------------------------
    IF (mat%nbtype>1) THEN
      IF (mat%nq_type(2)>0) THEN
        IF (mat%nq_type(1)>0) THEN
          CALL assemble_gh_real(mat%bsc(1,2)%arr,integrand)
        ENDIF
        IF (mat%nq_type(2)>0) THEN
          CALL assemble_hh_real(mat%bsc(2,2)%arr,integrand)
        ENDIF
        IF (mat%nq_type(3)>0) THEN
          CALL assemble_vh_real(mat%bsc(3,2)%arr,integrand)
        ENDIF
        IF (mat%nq_type(4)>0) THEN
          CALL assemble_ih_real(mat%bsc(4,2)%arr,integrand)
        ENDIF
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   loop over itype => vertical vertex
!-------------------------------------------------------------------------------
    IF (mat%nbtype>1) THEN
      IF (mat%nq_type(3)>0) THEN
        IF (mat%nq_type(1)>0) THEN
          CALL assemble_gv_real(mat%bsc(1,3)%arr,integrand)
        ENDIF
        IF (mat%nq_type(2)>0) THEN
          CALL assemble_hv_real(mat%bsc(2,3)%arr,integrand)
        ENDIF
        IF (mat%nq_type(3)>0) THEN
          CALL assemble_vv_real(mat%bsc(3,3)%arr,integrand)
        ENDIF
        IF (mat%nq_type(4)>0) THEN
          CALL assemble_iv_real(mat%bsc(4,3)%arr,integrand)
        ENDIF
      ENDIF
    ENDIF
!-------------------------------------------------------------------------------
!   loop over itype => interior vertex
!-------------------------------------------------------------------------------
    IF (mat%nbtype>1) THEN
      IF (mat%nq_type(4)>0) THEN
        IF (mat%nq_type(1)>0) THEN
          CALL assemble_gi_real(mat%bsc(1,4)%arr,integrand)
        ENDIF
        IF (mat%nq_type(2)>0) THEN
          CALL assemble_hi_real(mat%bsc(2,4)%arr,integrand)
        ENDIF
        IF (mat%nq_type(3)>0) THEN
          CALL assemble_vi_real(mat%bsc(3,4)%arr,integrand)
        ENDIF
        IF (mat%nq_type(4)>0) THEN
          CALL assemble_ii_real(mat%bsc(4,4)%arr,integrand)
        ENDIF
      ENDIF
    ENDIF
    IF (mat%on_gpu) CALL par%wait_streams(mat%id,16)
    CALL timer%end_timer_l2(iftn,idepth)
  CONTAINS

!-------------------------------------------------------------------------------
!>  assemble matrix for vertex-node vertex-node contributions
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_gg_real(arr,integrand)
      USE local
      IMPLICIT NONE

      !> matrix work array
      REAL(r8), DIMENSION(:,-1:,-1:,:,0:,0:), INTENT(INOUT) :: arr
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

      INTEGER(i4) :: iel,ix,iy,iq,jq,id

      id=par%get_stream(mat%id,1,16)
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=0,mat%my-1
        DO ix=0,mat%mx-1
          iel=1+ix+iy*mat%mx
          !$acc loop vector collapse(2)
          DO iq=1,mat%nq_type(1)
            DO jq=1,mat%nq_type(1)
              arr(jq,0,0,iq,ix,iy)=arr(jq,0,0,iq,ix,iy)+                        &
                    integrand(jq,iq,iel,1,1)
              arr(jq,-1,0,iq,ix+1,iy)=arr(jq,-1,0,iq,ix+1,iy)+                  &
                    integrand(jq,iq,iel,1,2)
              arr(jq,0,-1,iq,ix,iy+1)=arr(jq,0,-1,iq,ix,iy+1)+                  &
                    integrand(jq,iq,iel,1,3)
              arr(jq,-1,-1,iq,ix+1,iy+1)=arr(jq,-1,-1,iq,ix+1,iy+1)+            &
                    integrand(jq,iq,iel,1,4)
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=0,mat%my-1
        DO ix=0,mat%mx-1
          iel=1+ix+iy*mat%mx
          !$acc loop vector collapse(2)
          DO iq=1,mat%nq_type(1)
            DO jq=1,mat%nq_type(1)
              arr(jq,1,0,iq,ix,iy)=arr(jq,1,0,iq,ix,iy)+                        &
                    integrand(jq,iq,iel,2,1)
              arr(jq,0,0,iq,ix+1,iy)=arr(jq,0,0,iq,ix+1,iy)+                    &
                    integrand(jq,iq,iel,2,2)
              arr(jq,1,-1,iq,ix,iy+1)=arr(jq,1,-1,iq,ix,iy+1)+                  &
                    integrand(jq,iq,iel,2,3)
              arr(jq,0,-1,iq,ix+1,iy+1)=arr(jq,0,-1,iq,ix+1,iy+1)+              &
                    integrand(jq,iq,iel,2,4)
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=0,mat%my-1
        DO ix=0,mat%mx-1
          iel=1+ix+iy*mat%mx
          !$acc loop vector collapse(2)
          DO iq=1,mat%nq_type(1)
            DO jq=1,mat%nq_type(1)
              arr(jq,0,1,iq,ix,iy)=arr(jq,0,1,iq,ix,iy)+                        &
                    integrand(jq,iq,iel,3,1)
              arr(jq,-1,1,iq,ix+1,iy)=arr(jq,-1,1,iq,ix+1,iy)+                  &
                    integrand(jq,iq,iel,3,2)
              arr(jq,0,0,iq,ix,iy+1)=arr(jq,0,0,iq,ix,iy+1)+                    &
                    integrand(jq,iq,iel,3,3)
              arr(jq,-1,0,iq,ix+1,iy+1)=arr(jq,-1,0,iq,ix+1,iy+1)+              &
                    integrand(jq,iq,iel,3,4)
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=0,mat%my-1
        DO ix=0,mat%mx-1
          iel=1+ix+iy*mat%mx
          !$acc loop vector collapse(2)
          DO iq=1,mat%nq_type(1)
            DO jq=1,mat%nq_type(1)
              arr(jq,1,1,iq,ix,iy)=arr(jq,1,1,iq,ix,iy)+                        &
                    integrand(jq,iq,iel,4,1)
              arr(jq,0,1,iq,ix+1,iy)=arr(jq,0,1,iq,ix+1,iy)+                    &
                    integrand(jq,iq,iel,4,2)
              arr(jq,1,0,iq,ix,iy+1)=arr(jq,1,0,iq,ix,iy+1)+                    &
                    integrand(jq,iq,iel,4,3)
              arr(jq,0,0,iq,ix+1,iy+1)=arr(jq,0,0,iq,ix+1,iy+1)+                &
                    integrand(jq,iq,iel,4,4)
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
    END SUBROUTINE assemble_gg_real

!-------------------------------------------------------------------------------
!>  assemble matrix for horizontal-node vertex-node contributions
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_hg_real(arr,integrand)
      USE local
      IMPLICIT NONE

      !> matrix work array
      REAL(r8), DIMENSION(:,0:,-1:,:,0:,0:), INTENT(INOUT) :: arr
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

      INTEGER(i4) :: iel,ix,iy,jq,iq,jqs,jd,jb,id

      id=par%get_stream(mat%id,2,16)
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=0,mat%my-1
        DO ix=0,mat%mx-1
          iel=1+ix+iy*mat%mx
          !$acc loop worker
          DO iq=1,mat%nq_type(1)
            !$acc loop vector collapse(2) private(jb,jqs)
            DO jd=0,mat%pdcon-2
              DO jq=1,mat%nqcon
                jb=hstart+2*jd
                jqs=jq+mat%nqcon*jd
                arr(jqs,1,0,iq,ix,iy)=arr(jqs,1,0,iq,ix,iy)+                    &
                  integrand(jq,iq,iel,jb,1)
                arr(jqs,0,0,iq,ix+1,iy)=arr(jqs,0,0,iq,ix+1,iy)+                &
                  integrand(jq,iq,iel,jb,2)
                arr(jqs,1,-1,iq,ix,iy+1)=arr(jqs,1,-1,iq,ix,iy+1)+              &
                  integrand(jq,iq,iel,jb,3)
                arr(jqs,0,-1,iq,ix+1,iy+1)=arr(jqs,0,-1,iq,ix+1,iy+1)+          &
                  integrand(jq,iq,iel,jb,4)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=0,mat%my-1
        DO ix=0,mat%mx-1
          iel=1+ix+iy*mat%mx
          !$acc loop worker
          DO iq=1,mat%nq_type(1)
            !$acc loop vector collapse(2) private(jb,jqs)
            DO jd=0,mat%pdcon-2
              DO jq=1,mat%nqcon
                jb=hstart+2*jd
                jqs=jq+mat%nqcon*jd
                arr(jqs,1,1,iq,ix,iy)=arr(jqs,1,1,iq,ix,iy)+                    &
                  integrand(jq,iq,iel,jb+1,1)
                arr(jqs,0,1,iq,ix+1,iy)=arr(jqs,0,1,iq,ix+1,iy)+                &
                  integrand(jq,iq,iel,jb+1,2)
                arr(jqs,1,0,iq,ix,iy+1)=arr(jqs,1,0,iq,ix,iy+1)+                &
                  integrand(jq,iq,iel,jb+1,3)
                arr(jqs,0,0,iq,ix+1,iy+1)=arr(jqs,0,0,iq,ix+1,iy+1)+            &
                  integrand(jq,iq,iel,jb+1,4)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
    END SUBROUTINE assemble_hg_real

!-------------------------------------------------------------------------------
!>  assemble matrix for vertical-node vertex-node contributions
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_vg_real(arr,integrand)
      USE local
      IMPLICIT NONE

      !> matrix work array
      REAL(r8), DIMENSION(:,-1:,0:,:,0:,0:), INTENT(INOUT) :: arr
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

      INTEGER(i4) :: iel,ix,iy,jq,iq,jqs,jd,jb,id

      id=par%get_stream(mat%id,3,16)
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=0,mat%my-1
        DO ix=0,mat%mx-1
          iel=1+ix+iy*mat%mx
          !$acc loop worker
          DO iq=1,mat%nq_type(1)
            !$acc loop vector collapse(2) private(jb,jqs)
            DO jd=0,mat%pdcon-2
              DO jq=1,mat%nqcon
                jb=2*jd+vstart
                jqs=jq+mat%nqcon*jd
                arr(jqs,0,1,iq,ix,iy)=arr(jqs,0,1,iq,ix,iy)+                    &
                      integrand(jq,iq,iel,jb,1)
                arr(jqs,-1,1,iq,ix+1,iy)=arr(jqs,-1,1,iq,ix+1,iy)+              &
                      integrand(jq,iq,iel,jb,2)
                arr(jqs,0,0,iq,ix,iy+1)=arr(jqs,0,0,iq,ix,iy+1)+                &
                      integrand(jq,iq,iel,jb,3)
                arr(jqs,-1,0,iq,ix+1,iy+1)=arr(jqs,-1,0,iq,ix+1,iy+1)+          &
                      integrand(jq,iq,iel,jb,4)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=0,mat%my-1
        DO ix=0,mat%mx-1
          iel=1+ix+iy*mat%mx
          !$acc loop worker
          DO iq=1,mat%nq_type(1)
            !$acc loop vector collapse(2) private(jb,jqs)
            DO jd=0,mat%pdcon-2
              DO jq=1,mat%nqcon
                jb=2*jd+vstart
                jqs=jq+mat%nqcon*jd
                arr(jqs,1,1,iq,ix,iy)=arr(jqs,1,1,iq,ix,iy)+                    &
                  integrand(jq,iq,iel,jb+1,1)
                arr(jqs,0,1,iq,ix+1,iy)=arr(jqs,0,1,iq,ix+1,iy)+                &
                  integrand(jq,iq,iel,jb+1,2)
                arr(jqs,1,0,iq,ix,iy+1)=arr(jqs,1,0,iq,ix,iy+1)+                &
                  integrand(jq,iq,iel,jb+1,3)
                arr(jqs,0,0,iq,ix+1,iy+1)=arr(jqs,0,0,iq,ix+1,iy+1)+            &
                  integrand(jq,iq,iel,jb+1,4)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
    END SUBROUTINE assemble_vg_real

!-------------------------------------------------------------------------------
!>  assemble matrix for interior-node vertex-node contributions
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_ig_real(arr,integrand)
      USE local
      IMPLICIT NONE

      !> matrix work array
      REAL(r8), DIMENSION(:,0:,0:,:,0:,0:), INTENT(INOUT) :: arr
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

      INTEGER(i4) :: iel,ix,iy,jq,iq,jqi,jd,jb,id

      id=par%get_stream(mat%id,4,16)
!-------------------------------------------------------------------------------
!     continuous nodes
!-------------------------------------------------------------------------------
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=0,mat%my-1
        DO ix=0,mat%mx-1
          iel=1+ix+iy*mat%mx
          !$acc loop worker
          DO iq=1,mat%nq_type(1)
            !$acc loop vector collapse(2) private(jb,jqi)
            DO jd=0,(mat%pdcon-1)**2-1
              DO jq=1,mat%nqcon
                jb=jd+istart
                jqi=jq+mat%nqcon*jd
                arr(jqi,1,1,iq,ix,iy)=arr(jqi,1,1,iq,ix,iy)+                    &
                  integrand(jq,iq,iel,jb,1)
                arr(jqi,0,1,iq,ix+1,iy)=arr(jqi,0,1,iq,ix+1,iy)+                &
                  integrand(jq,iq,iel,jb,2)
                arr(jqi,1,0,iq,ix,iy+1)=arr(jqi,1,0,iq,ix,iy+1)+                &
                  integrand(jq,iq,iel,jb,3)
                arr(jqi,0,0,iq,ix+1,iy+1)=arr(jqi,0,0,iq,ix+1,iy+1)+            &
                  integrand(jq,iq,iel,jb,4)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
!-------------------------------------------------------------------------------
!     discontinuous nodes
!-------------------------------------------------------------------------------
      IF (mat%nqdis>0) THEN
        !$acc parallel present(arr,mat,integrand) wait(mat%id)                  &
        !$acc async(id) if(mat%on_gpu)
        !$acc loop gang collapse(2) private(iel)
        DO iy=0,mat%my-1
          DO ix=0,mat%mx-1
            iel=1+ix+iy*mat%mx
            !$acc loop worker
            DO iq=1,mat%nq_type(1)
              !$acc loop vector collapse(2) private(jb,jqi)
              DO jd=0,(mat%nbdisc-1)
                DO jq=1,mat%nqdis
                  jb=jd+dstart
                  jqi=jq+mat%nqdis*jd+nqci
                  arr(jqi,1,1,iq,ix,iy)=arr(jqi,1,1,iq,ix,iy)+                  &
                    integrand(jq,iq,iel,jb,1)
                  arr(jqi,0,1,iq,ix+1,iy)=arr(jqi,0,1,iq,ix+1,iy)+              &
                    integrand(jq,iq,iel,jb,2)
                  arr(jqi,1,0,iq,ix,iy+1)=arr(jqi,1,0,iq,ix,iy+1)+              &
                    integrand(jq,iq,iel,jb,3)
                  arr(jqi,0,0,iq,ix+1,iy+1)=arr(jqi,0,0,iq,ix+1,iy+1)+          &
                    integrand(jq,iq,iel,jb,4)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      ENDIF
    END SUBROUTINE assemble_ig_real

!-------------------------------------------------------------------------------
!>  assemble matrix for vertex-node horizontal-node contributions
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_gh_real(arr,integrand)
      USE local
      IMPLICIT NONE

      !> matrix work array
      REAL(r8), DIMENSION(:,-1:,-1:,:,1:,0:), INTENT(INOUT) :: arr
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

      INTEGER(i4) :: iel,ix,iy,iq,iqs,jq,id,ib

      id=par%get_stream(mat%id,5,16)
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=0,mat%my-1
        DO ix=1,mat%mx
          iel=1+(ix-1)+iy*mat%mx
          !$acc loop worker private(ib)
          DO id=0,mat%pdcon-2
            ib=2*id+hstart
            !$acc loop vector collapse(2) private(iqs)
            DO iq=1,mat%nqcon
              DO jq=1,mat%nqcon
                iqs=iq+mat%nqcon*id
                arr(jq,-1,0,iqs,ix,iy)=arr(jq,-1,0,iqs,ix,iy)+                  &
                      integrand(jq,iq,iel,1,ib)
                arr(jq,-1,-1,iqs,ix,iy+1)=arr(jq,-1,-1,iqs,ix,iy+1)+            &
                      integrand(jq,iq,iel,1,ib+1)
                arr(jq,0,0,iqs,ix,iy)=arr(jq,0,0,iqs,ix,iy)+                    &
                      integrand(jq,iq,iel,2,ib)
                arr(jq,0,-1,iqs,ix,iy+1)=arr(jq,0,-1,iqs,ix,iy+1)+              &
                      integrand(jq,iq,iel,2,ib+1)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=0,mat%my-1
        DO ix=1,mat%mx
          iel=1+(ix-1)+iy*mat%mx
          !$acc loop worker private(ib)
          DO id=0,mat%pdcon-2
            ib=2*id+hstart
            !$acc loop vector collapse(2) private(iqs)
            DO iq=1,mat%nqcon
              DO jq=1,mat%nqcon
                iqs=iq+mat%nqcon*id
                arr(jq,-1,1,iqs,ix,iy)=arr(jq,-1,1,iqs,ix,iy)+                  &
                      integrand(jq,iq,iel,3,ib)
                arr(jq,-1,0,iqs,ix,iy+1)=arr(jq,-1,0,iqs,ix,iy+1)+              &
                      integrand(jq,iq,iel,3,ib+1)
                arr(jq,0,1,iqs,ix,iy)=arr(jq,0,1,iqs,ix,iy)+                    &
                      integrand(jq,iq,iel,4,ib)
                arr(jq,0,0,iqs,ix,iy+1)=arr(jq,0,0,iqs,ix,iy+1)+                &
                      integrand(jq,iq,iel,4,ib+1)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
    END SUBROUTINE assemble_gh_real

!-------------------------------------------------------------------------------
!>  assemble matrix for horizontal-node horizontal-node contributions
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_hh_real(arr,integrand)
      USE local
      IMPLICIT NONE

      !> matrix work array
      REAL(r8), DIMENSION(:,0:,-1:,:,1:,0:), INTENT(INOUT) :: arr
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

      INTEGER(i4) :: iel,ix,iy,iq,jq,iqs,jqs,id,jd,ib,jb

      id=par%get_stream(mat%id,6,16)
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=0,mat%my-1
        DO ix=1,mat%mx
          iel=1+(ix-1)+iy*mat%mx
          !$acc loop worker collapse(2) private(ib,iqs)
          DO id=0,mat%pdcon-2
            DO iq=1,mat%nqcon
              ib=2*id+hstart
              iqs=iq+mat%nqcon*id
              !$acc loop vector collapse(2) private(jb,jqs)
              DO jd=0,mat%pdcon-2
                DO jq=1,mat%nqcon
                  jb=hstart+2*jd
                  jqs=jq+mat%nqcon*jd
                  arr(jqs,0,0,iqs,ix,iy)=arr(jqs,0,0,iqs,ix,iy)+                &
                        integrand(jq,iq,iel,jb,ib)
                  arr(jqs,0,-1,iqs,ix,iy+1)=arr(jqs,0,-1,iqs,ix,iy+1)+          &
                        integrand(jq,iq,iel,jb,ib+1)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=0,mat%my-1
        DO ix=1,mat%mx
          iel=1+(ix-1)+iy*mat%mx
          !$acc loop worker collapse(2) private(ib,iqs)
          DO id=0,mat%pdcon-2
            DO iq=1,mat%nqcon
              ib=2*id+hstart
              iqs=iq+mat%nqcon*id
              !$acc loop vector collapse(2) private(jb,jqs)
              DO jd=0,mat%pdcon-2
                DO jq=1,mat%nqcon
                  jb=hstart+2*jd
                  jqs=jq+mat%nqcon*jd
                  arr(jqs,0,1,iqs,ix,iy)=arr(jqs,0,1,iqs,ix,iy)+                &
                        integrand(jq,iq,iel,jb+1,ib)
                  arr(jqs,0,0,iqs,ix,iy+1)=arr(jqs,0,0,iqs,ix,iy+1)+            &
                        integrand(jq,iq,iel,jb+1,ib+1)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
    END SUBROUTINE assemble_hh_real

!-------------------------------------------------------------------------------
!>  assemble matrix for vertical-node horizontal-node contributions
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_vh_real(arr,integrand)
      USE local
      IMPLICIT NONE

      !> matrix work array
      REAL(r8), DIMENSION(:,-1:,0:,:,1:,0:), INTENT(INOUT) :: arr
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

      INTEGER(i4) :: iel,ix,iy,iq,jq,iqs,jqs,id,jd,ib,jb

      id=par%get_stream(mat%id,7,16)
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=0,mat%my-1
        DO ix=1,mat%mx
          iel=1+(ix-1)+iy*mat%mx
          !$acc loop worker collapse(2) private(ib,iqs)
          DO id=0,mat%pdcon-2
            DO iq=1,mat%nqcon
              ib=2*id+hstart
              iqs=iq+mat%nqcon*id
              !$acc loop vector collapse(2) private(jb,jqs)
              DO jd=0,mat%pdcon-2
                DO jq=1,mat%nqcon
                  jb=2*jd+vstart
                  jqs=jq+mat%nqcon*jd
                  arr(jqs,-1,1,iqs,ix,iy)=arr(jqs,-1,1,iqs,ix,iy)+              &
                        integrand(jq,iq,iel,jb,ib)
                  arr(jqs,-1,0,iqs,ix,iy+1)=arr(jqs,-1,0,iqs,ix,iy+1)+          &
                        integrand(jq,iq,iel,jb,ib+1)
                  arr(jqs,0,1,iqs,ix,iy)=arr(jqs,0,1,iqs,ix,iy)+                &
                        integrand(jq,iq,iel,jb+1,ib)
                  arr(jqs,0,0,iqs,ix,iy+1)=arr(jqs,0,0,iqs,ix,iy+1)+            &
                        integrand(jq,iq,iel,jb+1,ib+1)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
    END SUBROUTINE assemble_vh_real

!-------------------------------------------------------------------------------
!>  assemble matrix for interior-node horizontal-node contributions
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_ih_real(arr,integrand)
      USE local
      IMPLICIT NONE

      !> matrix work array
      REAL(r8), DIMENSION(:,0:,0:,:,1:,0:), INTENT(INOUT) :: arr
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

      INTEGER(i4) :: iel,ix,iy,iq,jq,iqs,jqi,id,jd,ib,jb

      id=par%get_stream(mat%id,8,16)
!-------------------------------------------------------------------------------
!     continuous nodes
!-------------------------------------------------------------------------------
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=0,mat%my-1
        DO ix=1,mat%mx
          iel=1+(ix-1)+iy*mat%mx
          !$acc loop worker collapse(2) private(ib,iqs)
          DO id=0,mat%pdcon-2
            DO iq=1,mat%nqcon
              ib=2*id+hstart
              iqs=iq+mat%nqcon*id
              !$acc loop vector collapse(2) private(jb,jqi)
              DO jd=0,(mat%pdcon-1)**2-1
                DO jq=1,mat%nqcon
                  jb=jd+istart
                  jqi=jq+mat%nqcon*jd
                  arr(jqi,0,1,iqs,ix,iy)=arr(jqi,0,1,iqs,ix,iy)+                &
                        integrand(jq,iq,iel,jb,ib)
                  arr(jqi,0,0,iqs,ix,iy+1)=arr(jqi,0,0,iqs,ix,iy+1)+            &
                        integrand(jq,iq,iel,jb,ib+1)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
!-------------------------------------------------------------------------------
!     discontinuous dof
!-------------------------------------------------------------------------------
      IF (mat%nqdis>0) THEN
        !$acc parallel present(arr,mat,integrand) wait(mat%id)                  &
        !$acc async(id) if(mat%on_gpu)
        !$acc loop gang collapse(2) private(iel)
        DO iy=0,mat%my-1
          DO ix=1,mat%mx
            iel=1+(ix-1)+iy*mat%mx
            !$acc loop worker collapse(2) private(ib,iqs)
            DO id=0,mat%pdcon-2
              DO iq=1,mat%nqcon
                ib=2*id+hstart
                iqs=iq+mat%nqcon*id
                !$acc loop vector collapse(2) private(jb,jqi)
                DO jd=0,mat%nbdisc-1
                  DO jq=1,mat%nqdis
                    jb=jd+dstart
                    jqi=jq+mat%nqdis*jd+nqci
                    arr(jqi,0,1,iqs,ix,iy)=arr(jqi,0,1,iqs,ix,iy)+              &
                          integrand(jq,iq,iel,jb,ib)
                    arr(jqi,0,0,iqs,ix,iy+1)=arr(jqi,0,0,iqs,ix,iy+1)+          &
                          integrand(jq,iq,iel,jb,ib+1)
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      ENDIF
    END SUBROUTINE assemble_ih_real

!-------------------------------------------------------------------------------
!>  assemble matrix for vertex-node vertical-node contributions
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_gv_real(arr,integrand)
      USE local
      IMPLICIT NONE

      !> matrix work array
      REAL(r8), DIMENSION(:,-1:,-1:,:,0:,1:), INTENT(INOUT) :: arr
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

      INTEGER(i4) :: iel,ix,iy,iq,jq,iqs,id,ib

      id=par%get_stream(mat%id,9,16)
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=1,mat%my
        DO ix=0,mat%mx-1
          iel=1+ix+(iy-1)*mat%mx
          !$acc loop worker private(ib)
          DO id=0,mat%pdcon-2
            ib=2*id+vstart
            !$acc loop vector collapse(2) private(iqs)
            DO iq=1,mat%nqcon
              DO jq=1,mat%nqcon
                iqs=iq+mat%nqcon*id
                arr(jq,0,-1,iqs,ix,iy)=arr(jq,0,-1,iqs,ix,iy)+                  &
                      integrand(jq,iq,iel,1,ib)
                arr(jq,-1,-1,iqs,ix+1,iy)=arr(jq,-1,-1,iqs,ix+1,iy)+            &
                      integrand(jq,iq,iel,1,ib+1)
                arr(jq,0,0,iqs,ix,iy)=arr(jq,0,0,iqs,ix,iy)+                    &
                      integrand(jq,iq,iel,3,ib)
                arr(jq,-1,0,iqs,ix+1,iy)=arr(jq,-1,0,iqs,ix+1,iy)+              &
                      integrand(jq,iq,iel,3,ib+1)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=1,mat%my
        DO ix=0,mat%mx-1
          iel=1+ix+(iy-1)*mat%mx
          !$acc loop worker private(ib)
          DO id=0,mat%pdcon-2
            ib=2*id+vstart
            !$acc loop vector collapse(2) private(iqs)
            DO iq=1,mat%nqcon
              DO jq=1,mat%nqcon
                iqs=iq+mat%nqcon*id
                arr(jq,1,-1,iqs,ix,iy)=arr(jq,1,-1,iqs,ix,iy)+                  &
                      integrand(jq,iq,iel,2,ib)
                arr(jq,0,-1,iqs,ix+1,iy)=arr(jq,0,-1,iqs,ix+1,iy)+              &
                      integrand(jq,iq,iel,2,ib+1)
                arr(jq,1,0,iqs,ix,iy)=arr(jq,1,0,iqs,ix,iy)+                    &
                      integrand(jq,iq,iel,4,ib)
                arr(jq,0,0,iqs,ix+1,iy)=arr(jq,0,0,iqs,ix+1,iy)+                &
                      integrand(jq,iq,iel,4,ib+1)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
    END SUBROUTINE assemble_gv_real

!-------------------------------------------------------------------------------
!>  assemble matrix for horizontal-node vertical-node contributions
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_hv_real(arr,integrand)
      USE local
      IMPLICIT NONE

      !> matrix work array
      REAL(r8), DIMENSION(:,0:,-1:,:,0:,1:), INTENT(INOUT) :: arr
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

      INTEGER(i4) :: iel,ix,iy,iq,jq,iqs,jqs,id,jd,ib,jb

      id=par%get_stream(mat%id,10,16)
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=1,mat%my
        DO ix=0,mat%mx-1
          iel=1+ix+(iy-1)*mat%mx
          !$acc loop worker collapse(2) private(ib,iqs)
          DO id=0,mat%pdcon-2
            DO iq=1,mat%nqcon
              ib=2*id+vstart
              iqs=iq+mat%nqcon*id
              !$acc loop vector collapse(2) private(jb,jqs)
              DO jd=0,mat%pdcon-2
                DO jq=1,mat%nqcon
                  jb=hstart+2*jd
                  jqs=jq+mat%nqcon*jd
                  arr(jqs,1,-1,iqs,ix,iy)=arr(jqs,1,-1,iqs,ix,iy)+              &
                        integrand(jq,iq,iel,jb,ib)
                  arr(jqs,0,-1,iqs,ix+1,iy)=arr(jqs,0,-1,iqs,ix+1,iy)+          &
                        integrand(jq,iq,iel,jb,ib+1)
                  arr(jqs,1,0,iqs,ix,iy)=arr(jqs,1,0,iqs,ix,iy)+                &
                        integrand(jq,iq,iel,jb+1,ib)
                  arr(jqs,0,0,iqs,ix+1,iy)=arr(jqs,0,0,iqs,ix+1,iy)+            &
                        integrand(jq,iq,iel,jb+1,ib+1)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
    END SUBROUTINE assemble_hv_real

!-------------------------------------------------------------------------------
!>  assemble matrix for vertical-node vertical-node contributions
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_vv_real(arr,integrand)
      USE local
      IMPLICIT NONE

      !> matrix work array
      REAL(r8), DIMENSION(:,-1:,0:,:,0:,1:), INTENT(INOUT) :: arr
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

      INTEGER(i4) :: iel,ix,iy,iq,jq,iqs,jqs,id,jd,ib,jb

      id=par%get_stream(mat%id,11,16)
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=1,mat%my
        DO ix=0,mat%mx-1
          iel=1+ix+(iy-1)*mat%mx
          !$acc loop worker collapse(2) private(ib,iqs)
          DO id=0,mat%pdcon-2
            DO iq=1,mat%nqcon
              ib=2*id+vstart
              iqs=iq+mat%nqcon*id
              !$acc loop vector collapse(2) private(jb,jqs)
              DO jd=0,mat%pdcon-2
                DO jq=1,mat%nqcon
                  jb=2*jd+vstart
                  jqs=jq+mat%nqcon*jd
                  arr(jqs,0,0,iqs,ix,iy)=arr(jqs,0,0,iqs,ix,iy)+                &
                        integrand(jq,iq,iel,jb,ib)
                  arr(jqs,-1,0,iqs,ix+1,iy)=arr(jqs,-1,0,iqs,ix+1,iy)+          &
                        integrand(jq,iq,iel,jb,ib+1)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=1,mat%my
        DO ix=0,mat%mx-1
          iel=1+ix+(iy-1)*mat%mx
          !$acc loop worker collapse(2) private(ib,iqs)
          DO id=0,mat%pdcon-2
            DO iq=1,mat%nqcon
              ib=2*id+vstart
              iqs=iq+mat%nqcon*id
              !$acc loop vector collapse(2) private(jb,jqs)
              DO jd=0,mat%pdcon-2
                DO jq=1,mat%nqcon
                  jb=2*jd+vstart
                  jqs=jq+mat%nqcon*jd
                  arr(jqs,1,0,iqs,ix,iy)=arr(jqs,1,0,iqs,ix,iy)+                &
                        integrand(jq,iq,iel,jb+1,ib)
                  arr(jqs,0,0,iqs,ix+1,iy)=arr(jqs,0,0,iqs,ix+1,iy)+            &
                        integrand(jq,iq,iel,jb+1,ib+1)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
    END SUBROUTINE assemble_vv_real

!-------------------------------------------------------------------------------
!>  assemble matrix for interior-node vertical-node contributions
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_iv_real(arr,integrand)
      USE local
      IMPLICIT NONE

      !> matrix work array
      REAL(r8), DIMENSION(:,0:,0:,:,0:,1:), INTENT(INOUT) :: arr
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

      INTEGER(i4) :: iel,ix,iy,iq,jq,iqs,jqi,id,jd,ib,jb

      id=par%get_stream(mat%id,12,16)
!-------------------------------------------------------------------------------
!     continuous dof
!-------------------------------------------------------------------------------
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=1,mat%my
        DO ix=0,mat%mx-1
          iel=1+ix+(iy-1)*mat%mx
          !$acc loop worker collapse(2) private(ib,iqs)
          DO id=0,mat%pdcon-2
            DO iq=1,mat%nqcon
              ib=2*id+vstart
              iqs=iq+mat%nqcon*id
              !$acc loop vector collapse(2) private(jb,jqi)
              DO jd=0,(mat%pdcon-1)**2-1
                DO jq=1,mat%nqcon
                  jb=jd+istart
                  jqi=jq+mat%nqcon*jd
                  arr(jqi,1,0,iqs,ix,iy)=arr(jqi,1,0,iqs,ix,iy)+                &
                        integrand(jq,iq,iel,jb,ib)
                  arr(jqi,0,0,iqs,ix+1,iy)=arr(jqi,0,0,iqs,ix+1,iy)+            &
                        integrand(jq,iq,iel,jb,ib+1)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
!-------------------------------------------------------------------------------
!     discontinuous dof
!-------------------------------------------------------------------------------
      IF (mat%nqdis>0) THEN
        !$acc parallel present(arr,mat,integrand) wait(mat%id)                  &
        !$acc async(id) if(mat%on_gpu)
        !$acc loop gang collapse(2) private(iel)
        DO iy=1,mat%my
          DO ix=0,mat%mx-1
            iel=1+ix+(iy-1)*mat%mx
            !$acc loop worker collapse(2) private(ib,iqs)
            DO id=0,mat%pdcon-2
              DO iq=1,mat%nqcon
                ib=2*id+vstart
                iqs=iq+mat%nqcon*id
                !$acc loop vector collapse(2) private(jb,jqi)
                DO jd=0,mat%nbdisc-1
                  DO jq=1,mat%nqdis
                    jb=jd+dstart
                    jqi=jq+mat%nqdis*jd+nqci
                    arr(jqi,1,0,iqs,ix,iy)=arr(jqi,1,0,iqs,ix,iy)+              &
                          integrand(jq,iq,iel,jb,ib)
                    arr(jqi,0,0,iqs,ix+1,iy)=arr(jqi,0,0,iqs,ix+1,iy)+          &
                          integrand(jq,iq,iel,jb,ib+1)
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      ENDIF
    END SUBROUTINE assemble_iv_real

!-------------------------------------------------------------------------------
!>  assemble matrix for vertex-node interior-node contributions
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_gi_real(arr,integrand)
      USE local
      IMPLICIT NONE

      !> matrix work array
      REAL(r8), DIMENSION(:,-1:,-1:,:,1:,1:), INTENT(INOUT) :: arr
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

      INTEGER(i4) :: iel,ix,iy,iq,jq,iqi,id,ib

      id=par%get_stream(mat%id,13,16)
!-------------------------------------------------------------------------------
!     continuous dof
!-------------------------------------------------------------------------------
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=1,mat%my
        DO ix=1,mat%mx
          iel=1+(ix-1)+(iy-1)*mat%mx
          !$acc loop worker collapse(2) private(ib,iqi)
          DO id=0,(mat%pdcon-1)**2-1
            DO iq=1,mat%nqcon
              ib=id+istart
              iqi=iq+mat%nqcon*id
              !$acc loop vector
              DO jq=1,mat%nqcon
                arr(jq,-1,-1,iqi,ix,iy)=arr(jq,-1,-1,iqi,ix,iy)+                &
                  integrand(jq,iq,iel,1,ib)
                arr(jq,0,-1,iqi,ix,iy)=arr(jq,0,-1,iqi,ix,iy)+                  &
                  integrand(jq,iq,iel,2,ib)
                arr(jq,-1,0,iqi,ix,iy)=arr(jq,-1,0,iqi,ix,iy)+                  &
                  integrand(jq,iq,iel,3,ib)
                arr(jq,0,0,iqi,ix,iy)=arr(jq,0,0,iqi,ix,iy)+                    &
                  integrand(jq,iq,iel,4,ib)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
!-------------------------------------------------------------------------------
!     discontinuous dof
!-------------------------------------------------------------------------------
      IF (mat%nqdis>0) THEN
        !$acc parallel present(arr,mat,integrand) wait(mat%id)                  &
        !$acc async(id) if(mat%on_gpu)
        !$acc loop gang collapse(2) private(iel)
        DO iy=1,mat%my
          DO ix=1,mat%mx
            iel=1+(ix-1)+(iy-1)*mat%mx
            !$acc loop worker collapse(2) private(ib,iqi)
            DO id=0,mat%nbdisc-1
              DO iq=1,mat%nqdis
                ib=id+dstart
                iqi=iq+mat%nqdis*id+nqci
                !$acc loop vector
                DO jq=1,mat%nqcon
                  arr(jq,-1,-1,iqi,ix,iy)=arr(jq,-1,-1,iqi,ix,iy)+              &
                    integrand(jq,iq,iel,1,ib)
                  arr(jq,0,-1,iqi,ix,iy)=arr(jq,0,-1,iqi,ix,iy)+                &
                    integrand(jq,iq,iel,2,ib)
                  arr(jq,-1,0,iqi,ix,iy)=arr(jq,-1,0,iqi,ix,iy)+                &
                    integrand(jq,iq,iel,3,ib)
                  arr(jq,0,0,iqi,ix,iy)=arr(jq,0,0,iqi,ix,iy)+                  &
                    integrand(jq,iq,iel,4,ib)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      ENDIF
    END SUBROUTINE assemble_gi_real

!-------------------------------------------------------------------------------
!>  assemble matrix for horizontal-node interior-node hcontributions
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_hi_real(arr,integrand)
      USE local
      IMPLICIT NONE

      !> matrix work array
      REAL(r8), DIMENSION(:,0:,-1:,:,1:,1:), INTENT(INOUT) :: arr
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

      INTEGER(i4) :: iel,ix,iy,iq,jq,iqi,jqs,id,jd,ib,jb

      id=par%get_stream(mat%id,14,16)
!-------------------------------------------------------------------------------
!     continuous dof
!-------------------------------------------------------------------------------
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=1,mat%my
        DO ix=1,mat%mx
          iel=1+(ix-1)+(iy-1)*mat%mx
          !$acc loop worker collapse(2) private(ib,iqi)
          DO id=0,(mat%pdcon-1)**2-1
            DO iq=1,mat%nqcon
              ib=id+istart
              iqi=iq+mat%nqcon*id
              !$acc loop vector collapse(2) private(jd,jqs)
              DO jd=0,mat%pdcon-2
                DO jq=1,mat%nqcon
                  jb=hstart+2*jd
                  jqs=jq+mat%nqcon*jd
                  arr(jqs,0,-1,iqi,ix,iy)=arr(jqs,0,-1,iqi,ix,iy)+              &
                    integrand(jq,iq,iel,jb,ib)
                  arr(jqs,0,0,iqi,ix,iy)=arr(jqs,0,0,iqi,ix,iy)+                &
                    integrand(jq,iq,iel,jb+1,ib)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
!-------------------------------------------------------------------------------
!     discontinuous dof
!-------------------------------------------------------------------------------
      IF (mat%nqdis>0) THEN
        !$acc parallel present(arr,mat,integrand) wait(mat%id)                  &
        !$acc async(id) if(mat%on_gpu)
        !$acc loop gang collapse(2) private(iel)
        DO iy=1,mat%my
          DO ix=1,mat%mx
            iel=1+(ix-1)+(iy-1)*mat%mx
            !$acc loop worker collapse(2) private(ib,iqi)
            DO id=0, mat%nbdisc-1
              DO iq=1,mat%nqdis
                ib=id+dstart
                iqi=iq+mat%nqdis*id+nqci
                !$acc loop vector collapse(2) private(jb,jqs)
                DO jd=0,mat%pdcon-2
                  DO jq=1,mat%nqcon
                    jb=hstart+2*jd
                    jqs=jq+mat%nqcon*jd
                    arr(jqs,0,-1,iqi,ix,iy)=arr(jqs,0,-1,iqi,ix,iy)+            &
                      integrand(jq,iq,iel,jb,ib)
                    arr(jqs,0,0,iqi,ix,iy)=arr(jqs,0,0,iqi,ix,iy)+              &
                      integrand(jq,iq,iel,jb+1,ib)
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      ENDIF
    END SUBROUTINE assemble_hi_real

!-------------------------------------------------------------------------------
!>  assemble matrix for vertical-node interior-node contributions
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_vi_real(arr,integrand)
      USE local
      IMPLICIT NONE

      !> matrix work array
      REAL(r8), DIMENSION(:,-1:,0:,:,1:,1:), INTENT(INOUT) :: arr
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

      INTEGER(i4) :: iel,ix,iy,iq,jq,iqi,jqs,id,jd,ib,jb

      id=par%get_stream(mat%id,15,16)
!-------------------------------------------------------------------------------
!     continuous dof
!-------------------------------------------------------------------------------
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=1,mat%my
        DO ix=1,mat%mx
          iel=1+(ix-1)+(iy-1)*mat%mx
          !$acc loop worker collapse(2) private(ib,iqi)
          DO id=0,(mat%pdcon-1)**2-1
            DO iq=1,mat%nqcon
              ib=id+istart
              iqi=iq+mat%nqcon*id
              !$acc loop vector collapse(2) private(jd,jqs)
              DO jd=0,mat%pdcon-2
                DO jq=1,mat%nqcon
                  jb=vstart+2*jd
                  jqs=jq+mat%nqcon*jd
                  arr(jqs,-1,0,iqi,ix,iy)=arr(jqs,-1,0,iqi,ix,iy)+              &
                    integrand(jq,iq,iel,jb,ib)
                  arr(jqs,0,0,iqi,ix,iy)=arr(jqs,0,0,iqi,ix,iy)+                &
                    integrand(jq,iq,iel,jb+1,ib)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
!-------------------------------------------------------------------------------
!     discontinuous dof
!-------------------------------------------------------------------------------
      IF (mat%nqdis>0) THEN
        !$acc parallel present(arr,mat,integrand) wait(mat%id)                  &
        !$acc async(id) if(mat%on_gpu)
        !$acc loop gang collapse(2) private(iel)
        DO iy=1,mat%my
          DO ix=1,mat%mx
            iel=1+(ix-1)+(iy-1)*mat%mx
            !$acc loop worker collapse(2) private(ib,iqi)
            DO id=0, mat%nbdisc-1
              DO iq=1,mat%nqdis
                ib=id+dstart
                iqi=iq+mat%nqdis*id+nqci
                !$acc loop vector collapse(2) private(jd,jqs)
                DO jd=0,mat%pdcon-2
                  DO jq=1,mat%nqcon
                    jb=vstart+2*jd
                    jqs=jq+mat%nqcon*jd
                    arr(jqs,-1,0,iqi,ix,iy)=arr(jqs,-1,0,iqi,ix,iy)+            &
                      integrand(jq,iq,iel,jb,ib)
                    arr(jqs,0,0,iqi,ix,iy)=arr(jqs,0,0,iqi,ix,iy)+              &
                      integrand(jq,iq,iel,jb+1,ib)
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      ENDIF
    END SUBROUTINE assemble_vi_real

!-------------------------------------------------------------------------------
!>  assemble matrix for interior-node interior-node contributions
!-------------------------------------------------------------------------------
    SUBROUTINE assemble_ii_real(arr,integrand)
      USE local
      IMPLICIT NONE

      !> matrix work array
      REAL(r8), DIMENSION(:,0:,0:,:,1:,1:), INTENT(INOUT) :: arr
      !> integrand array
      REAL(r8), INTENT(IN) :: integrand(:,:,:,:,:)

      INTEGER(i4) :: iel,ix,iy,iq,jq,iqi,jqi,id,jd,ib,jb

      id=par%get_stream(mat%id,16,16)
!-------------------------------------------------------------------------------
!     continuous idof
!     continuous jdof
!-------------------------------------------------------------------------------
      !$acc parallel present(arr,mat,integrand) wait(mat%id)                    &
      !$acc async(id) if(mat%on_gpu)
      !$acc loop gang collapse(2) private(iel)
      DO iy=1,mat%my
        DO ix=1,mat%mx
          iel=1+(ix-1)+(iy-1)*mat%mx
          !$acc loop worker collapse(2) private(ib,iqi)
          DO id=0,(mat%pdcon-1)**2-1
            DO iq=1,mat%nqcon
              ib=id+istart
              iqi=iq+mat%nqcon*id
              !$acc loop vector collapse(2) private(jb,jqi)
              DO jd=0,(mat%pdcon-1)**2-1
                DO jq=1,mat%nqcon
                  jb=istart+jd
                  jqi=jq+mat%nqcon*jd
                  arr(jqi,0,0,iqi,ix,iy)=arr(jqi,0,0,iqi,ix,iy)+                &
                    integrand(jq,iq,iel,jb,ib)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
      IF (mat%nqdis>0) THEN
!-------------------------------------------------------------------------------
!       discontinuous idof
!       continuous jdof
!-------------------------------------------------------------------------------
        !$acc parallel present(arr,mat,integrand) wait(mat%id)                  &
        !$acc async(id) if(mat%on_gpu)
        !$acc loop gang collapse(2) private(iel)
        DO iy=1,mat%my
          DO ix=1,mat%mx
            iel=1+(ix-1)+(iy-1)*mat%mx
            !$acc loop worker collapse(2) private(ib,iqi)
            DO id=0, mat%nbdisc-1
              DO iq=1,mat%nqdis
                ib=id+dstart
                iqi=iq+mat%nqdis*id+nqci
                !$acc loop vector collapse(2) private(jb,jqi)
                DO jd=0,(mat%pdcon-1)**2-1
                  DO jq=1,mat%nqcon
                    jb=istart+jd
                    jqi=jq+mat%nqcon*jd
                    arr(jqi,0,0,iqi,ix,iy)=arr(jqi,0,0,iqi,ix,iy)+              &
                      integrand(jq,iq,iel,jb,ib)
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
!-------------------------------------------------------------------------------
!       continuous idof
!       discontinuous jdof
!-------------------------------------------------------------------------------
        !$acc parallel present(arr,mat,integrand) wait(mat%id)                  &
        !$acc async(id) if(mat%on_gpu)
        !$acc loop gang collapse(2) private(iel)
        DO iy=1,mat%my
          DO ix=1,mat%mx
            iel=1+(ix-1)+(iy-1)*mat%mx
            !$acc loop worker collapse(2) private(ib,iqi)
            DO id=0,(mat%pdcon-1)**2-1
              DO iq=1,mat%nqcon
                ib=id+istart
                iqi=iq+mat%nqcon*id
                !$acc loop vector collapse(2) private(jb,jqi)
                DO jd=0, mat%nbdisc-1
                  DO jq=1,mat%nqdis
                    jb=jd+dstart
                    jqi=jq+mat%nqdis*jd+nqci
                    arr(jqi,0,0,iqi,ix,iy)=arr(jqi,0,0,iqi,ix,iy)+              &
                      integrand(jq,iq,iel,jb,ib)
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
!-------------------------------------------------------------------------------
!       discontinuous idof
!       discontinuous jdof
!-------------------------------------------------------------------------------
        !$acc parallel present(arr,mat,integrand) wait(mat%id)                  &
        !$acc async(id) if(mat%on_gpu)
        !$acc loop gang collapse(2) private(iel)
        DO iy=1,mat%my
          DO ix=1,mat%mx
            iel=1+(ix-1)+(iy-1)*mat%mx
            !$acc loop worker collapse(2) private(ib,iqi)
            DO id=0, mat%nbdisc-1
              DO iq=1,mat%nqdis
                ib=id+dstart
                iqi=iq+mat%nqdis*id+nqci
                !$acc loop vector collapse(2) private(jd,jqi)
                DO jd=0, mat%nbdisc-1
                  DO jq=1,mat%nqdis
                    jb=jd+dstart
                    jqi=jq+mat%nqdis*jd+nqci
                    arr(jqi,0,0,iqi,ix,iy)=arr(jqi,0,0,iqi,ix,iy)+              &
                      integrand(jq,iq,iel,jb,ib)
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      ENDIF
    END SUBROUTINE assemble_ii_real

  END SUBROUTINE assemble_real

!-------------------------------------------------------------------------------
!* set matrix to zero
!-------------------------------------------------------------------------------
  SUBROUTINE zero_real(mat)
    USE local
    IMPLICIT NONE

    !> matrix to set to zero
    CLASS(mat_rect_2D_real_acc), INTENT(INOUT) :: mat

    INTEGER(i4) :: jd,id
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'zero_real',iftn,idepth)
    DO jd=1,SIZE(mat%bsc,2)
      DO id=1,SIZE(mat%bsc,1)
        ASSOCIATE(arr=>mat%bsc(id,jd)%arr)
          !$acc kernels async(mat%id) present(arr) if(mat%on_gpu)
          arr=0._r8
          !$acc end kernels
        END ASSOCIATE
      ENDDO
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE zero_real

!-------------------------------------------------------------------------------
!> invert the connections within cell interiors for basis functions
! of degree 2 or more.
!-------------------------------------------------------------------------------
  SUBROUTINE elim_inv_int_real(mat)
#ifdef HAVE_ACC_BLAS
    USE acc_blas_mod
#endif
    USE math_tran
    IMPLICIT NONE

    !> matrix to eliminate
    CLASS(mat_rect_2D_real_acc), INTENT(INOUT) :: mat

    REAL(r8), ALLOCATABLE, DIMENSION(:,:,:,:) :: aiiaio14,aiiaio24,aiiaio34
    INTEGER(i4) :: ix,iy,iq,jq,kq,ixo,iyo,jxo,jyo,id
    LOGICAL :: sing=.FALSE.
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

!-------------------------------------------------------------------------------
!   check for poly_degree=1
!-------------------------------------------------------------------------------
    IF (mat%nbtype==1) RETURN
    CALL timer%start_timer_l1(mod_name,'elim_inv_int_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   invert the interior to interior matrix, A_ii.
!   TODO: clean up and abstract the ugly nature of this code
!-------------------------------------------------------------------------------
#ifdef HAVE_OPENACC
    IF (mat%on_gpu) THEN
#ifdef HAVE_ACC_BLAS
      CALL acc_blas%dmatinvBatched(mat%nq_type(4),mat%mx*mat%my,mat%dptr_mat44, &
                                   mat%dptr_imat44,mat%id)
#else
      CALL par%nim_stop('Called GPU math_invert_q1 without acc_blas')
#endif
#ifdef OPENACC_AUTOCOMPARE
      CALL math_invert_q1(mat%nq_type(4),mat%mx*mat%my,mat%bsc(4,4)%arr,mat%id, &
                          mat%symmetric,sing)
#else
      ! TODO: remove this when it does not help optimization
      !ASSOCIATE (mat44=>mat%bsc(4,4)%arr,imat44=>mat%imat44)
      !  !$acc kernels present(mat44,imat44) async(mat%id) if(mat%on_gpu)
      !  mat44=imat44
      !  !$acc end kernels
      !END ASSOCIATE
      CALL acc_array_copy(mat%nq_type(4)**2*mat%mx*mat%my,mat%bsc(4,4)%arr,     &
                          mat%imat44,mat%id)
#endif
    ELSE
      CALL math_invert_q1(mat%nq_type(4),mat%mx*mat%my,mat%bsc(4,4)%arr,mat%id, &
                          mat%symmetric,sing)
    ENDIF
#else
    CALL math_invert_q1(mat%nq_type(4),mat%mx*mat%my,mat%bsc(4,4)%arr,mat%id,   &
                        mat%symmetric,sing)
#endif
    IF (sing) CALL par%nim_stop('elim_inv_int_real:'//                          &
                                ' dense interior does not factor.')
!-------------------------------------------------------------------------------
!   if nq is zero, there are no bases that are continuous across
!   element borders.
!-------------------------------------------------------------------------------
    IF (mat%nqcon==0) THEN
      CALL timer%end_timer_l2(iftn,idepth)
      RETURN
    ENDIF
!-------------------------------------------------------------------------------
!   now create the Schur complement, A_oo-A_oi.A_ii**-1.A_io,
!   where i refers to interior data, and o refers to other data.
!-------------------------------------------------------------------------------
    ASSOCIATE(mat11=>mat%bsc(1,1)%arr,mat41=>mat%bsc(4,1)%arr,                  &
              mat12=>mat%bsc(1,2)%arr,mat42=>mat%bsc(4,2)%arr,                  &
              mat13=>mat%bsc(1,3)%arr,mat43=>mat%bsc(4,3)%arr,                  &
              mat14=>mat%bsc(1,4)%arr,mat44=>mat%bsc(4,4)%arr)
      id=par%get_stream(mat%id,1,3)
      ALLOCATE(aiiaio14(mat%nq_type(4),mat%nq_type(1),mat%mx,mat%my))
      !$acc enter data create(aiiaio14) async(id) if(mat%on_gpu)
      DO jyo=-1,0
        DO jxo=-1,0
!-------------------------------------------------------------------------------
!         start with a computation of A_ii**-1.A_io taking one grid offset
!-------------------------------------------------------------------------------
          !$acc parallel present(mat,mat14,mat44,aiiaio14) copyin(jyo,jxo)      &
          !$acc wait(mat%id) async(id) if(mat%on_gpu)
          !$acc loop gang collapse(2)
          DO iy=1,mat%my
            DO ix=1,mat%mx
              !$acc loop vector collapse(2)
              DO jq=1,mat%nq_type(1)
                DO kq=1,mat%nq_type(4)
                  aiiaio14(kq,jq,ix,iy)=0._r8
                  !$acc loop seq
                  DO iq=1,mat%nq_type(4)
                    aiiaio14(kq,jq,ix,iy)=aiiaio14(kq,jq,ix,iy)+                &
                      mat44(iq,0,0,kq,ix,iy)*mat14(jq,jxo,jyo,iq,ix,iy)
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
          !$acc end parallel
!-------------------------------------------------------------------------------
!         complete the computation of A_oo-A_oi.A_ii**-1.A_io
!-------------------------------------------------------------------------------
          !$acc parallel present(mat,mat11,mat12,mat13,mat41,mat42,mat43)       &
          !$acc present(aiiaio14) copyin(jyo,jxo)                               &
          !$acc wait(mat%id) async(id) if(mat%on_gpu)
          !$acc loop gang collapse(2)
          DO iy=1,mat%my
            DO ix=1,mat%mx
!-------------------------------------------------------------------------------
!             modify the rows connecting to grid nodes (i.e. A_oo=A_gg):
!-------------------------------------------------------------------------------
              !$acc loop vector collapse(4)
              DO iq=1,mat%nq_type(1)
                DO iyo=0,1
                  DO ixo=0,1
                    DO jq=1,mat%nq_type(1)
                      !$acc loop seq
                      DO kq=1,mat%nq_type(4)
                        mat11(jq,jxo+ixo,jyo+iyo,iq,ix-ixo,iy-iyo)=             &
                          mat11(jq,jxo+ixo,jyo+iyo,iq,ix-ixo,iy-iyo)-           &
                          mat41(kq,ixo,iyo,iq,ix-ixo,iy-iyo)                    &
                          *aiiaio14(kq,jq,ix,iy)
                      ENDDO
                    ENDDO
                  ENDDO
                ENDDO
              ENDDO
!-------------------------------------------------------------------------------
!             modify the rows connecting to horizontal side nodes
!             (i.e. A_oo=A_hg):
!-------------------------------------------------------------------------------
              !$acc loop vector collapse(3)
              DO iq=1,mat%nq_type(2)
                DO iyo=0,1
                  DO jq=1,mat%nq_type(1)
                    !$acc loop seq
                    DO kq=1,mat%nq_type(4)
                      mat12(jq,jxo,jyo+iyo,iq,ix,iy-iyo)=                       &
                        mat12(jq,jxo,jyo+iyo,iq,ix,iy-iyo)-                     &
                          mat42(kq,0,iyo,iq,ix,iy-iyo)*aiiaio14(kq,jq,ix,iy)
                    ENDDO
                  ENDDO
                ENDDO
              ENDDO
!-------------------------------------------------------------------------------
!             modify the rows connecting to vertical side nodes
!             (i.e. A_oo=A_vg):
!-------------------------------------------------------------------------------
              !$acc loop vector collapse(3)
              DO iq=1,mat%nq_type(3)
                DO ixo=0,1
                  DO jq=1,mat%nq_type(1)
                    !$acc loop seq
                    DO kq=1,mat%nq_type(4)
                      mat13(jq,jxo+ixo,jyo,iq,ix-ixo,iy)=                       &
                        mat13(jq,jxo+ixo,jyo,iq,ix-ixo,iy)-                     &
                          mat43(kq,ixo,0,iq,ix-ixo,iy)*aiiaio14(kq,jq,ix,iy)
                    ENDDO
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
          !$acc end parallel
        ENDDO
      ENDDO
      !$acc exit data delete(aiiaio14) finalize async(id) if(mat%on_gpu)
      DEALLOCATE(aiiaio14)
    END ASSOCIATE
!-------------------------------------------------------------------------------
!   now take one horizontal side node and vector-component-index
!   as a column element.
!-------------------------------------------------------------------------------
    ASSOCIATE(mat21=>mat%bsc(2,1)%arr,mat41=>mat%bsc(4,1)%arr,                  &
              mat22=>mat%bsc(2,2)%arr,mat42=>mat%bsc(4,2)%arr,                  &
              mat23=>mat%bsc(2,3)%arr,mat43=>mat%bsc(4,3)%arr,                  &
              mat24=>mat%bsc(2,4)%arr,mat44=>mat%bsc(4,4)%arr)
      id=par%get_stream(mat%id,2,3)
      ALLOCATE(aiiaio24(mat%nq_type(4),mat%nq_type(2),mat%mx,mat%my))
      !$acc enter data create(aiiaio24) async(id) if(mat%on_gpu)
      DO jyo=-1,0
!-------------------------------------------------------------------------------
!       start with a computation of A_ii**-1.A_io taking one grid offset
!-------------------------------------------------------------------------------
        !$acc parallel present(mat,mat44,mat24,aiiaio24) copyin(jyo)            &
        !$acc wait(mat%id) async(id) if(mat%on_gpu)
        !$acc loop gang collapse(2)
        DO iy=1,mat%my
          DO ix=1,mat%mx
            !$acc loop vector collapse(2)
            DO jq=1,mat%nq_type(2)
              DO kq=1,mat%nq_type(4)
                aiiaio24(kq,jq,ix,iy)=0._r8
                !$acc loop seq
                DO iq=1,mat%nq_type(4)
                  aiiaio24(kq,jq,ix,iy)=aiiaio24(kq,jq,ix,iy)+                  &
                    mat44(iq,0,0,kq,ix,iy)*mat24(jq,0,jyo,iq,ix,iy)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
!-------------------------------------------------------------------------------
!       complete the computation of A_oo-A_oi.A_ii**-1.A_io
!-------------------------------------------------------------------------------
        !$acc parallel present(mat,mat21,mat22,mat23,mat41,mat42,mat43,aiiaio24)&
        !$acc copyin(jyo) wait(mat%id) async(id) if(mat%on_gpu)
        !$acc loop gang collapse(2)
        DO iy=1,mat%my
          DO ix=1,mat%mx
!-------------------------------------------------------------------------------
!           modify the rows connecting to grid nodes (i.e. A_oo=A_gh):
!-------------------------------------------------------------------------------
            !$acc loop vector collapse(4)
            DO iq=1,mat%nq_type(1)
              DO iyo=0,1
                DO ixo=0,1
                  DO jq=1,mat%nq_type(2)
                    !$acc loop seq
                    DO kq=1,mat%nq_type(4)
                      mat21(jq,ixo,jyo+iyo,iq,ix-ixo,iy-iyo)=                   &
                        mat21(jq,ixo,jyo+iyo,iq,ix-ixo,iy-iyo)-                 &
                        mat41(kq,ixo,iyo,iq,ix-ixo,iy-iyo)*aiiaio24(kq,jq,ix,iy)
                    ENDDO
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
!-------------------------------------------------------------------------------
!           modify the rows connecting to horizontal side nodes
!           (i.e. A_oo=A_hh):
!-------------------------------------------------------------------------------
            !$acc loop vector collapse(3)
            DO iq=1,mat%nq_type(2)
              DO iyo=0,1
                DO jq=1,mat%nq_type(2)
                  !$acc loop seq
                  DO kq=1,mat%nq_type(4)
                    mat22(jq,0,jyo+iyo,iq,ix,iy-iyo)=                           &
                      mat22(jq,0,jyo+iyo,iq,ix,iy-iyo)-                         &
                        mat42(kq,0,iyo,iq,ix,iy-iyo)*aiiaio24(kq,jq,ix,iy)
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
!-------------------------------------------------------------------------------
!           modify the rows connecting to vertical side nodes (i.e. A_oo=A_vh):
!-------------------------------------------------------------------------------
            !$acc loop vector collapse(3)
            DO iq=1,mat%nq_type(3)
              DO ixo=0,1
                DO jq=1,mat%nq_type(2)
                  !$acc loop seq
                  DO kq=1,mat%nq_type(4)
                    mat23(jq,ixo,jyo,iq,ix-ixo,iy)=                             &
                      mat23(jq,ixo,jyo,iq,ix-ixo,iy)-                           &
                        mat43(kq,ixo,0,iq,ix-ixo,iy)*aiiaio24(kq,jq,ix,iy)
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      ENDDO
      !$acc exit data delete(aiiaio24) finalize async(id) if(mat%on_gpu)
      DEALLOCATE(aiiaio24)
    END ASSOCIATE
!-------------------------------------------------------------------------------
!   finally take one vertical side node and vector-component-index
!   as a column element.
!-------------------------------------------------------------------------------
    ASSOCIATE(mat31=>mat%bsc(3,1)%arr,mat41=>mat%bsc(4,1)%arr,                  &
              mat32=>mat%bsc(3,2)%arr,mat42=>mat%bsc(4,2)%arr,                  &
              mat33=>mat%bsc(3,3)%arr,mat43=>mat%bsc(4,3)%arr,                  &
              mat34=>mat%bsc(3,4)%arr,mat44=>mat%bsc(4,4)%arr)
      id=par%get_stream(mat%id,3,3)
      ALLOCATE(aiiaio34(mat%nq_type(4),mat%nq_type(3),mat%mx,mat%my))
      !$acc enter data create(aiiaio34) async(id) if(mat%on_gpu)
      DO jxo=-1,0
!-------------------------------------------------------------------------------
!       start with a computation of A_ii**-1.A_io taking one grid offset
!-------------------------------------------------------------------------------
        !$acc parallel present(mat,mat34,mat44,aiiaio34) copyin(jxo)            &
        !$acc wait(mat%id) async(id) if(mat%on_gpu)
        !$acc loop gang collapse(2)
        DO iy=1,mat%my
          DO ix=1,mat%mx
            !$acc loop vector collapse(2)
            DO jq=1,mat%nq_type(3)
              DO kq=1,mat%nq_type(4)
                aiiaio34(kq,jq,ix,iy)=0._r8
                !$acc loop seq
                DO iq=1,mat%nq_type(4)
                  aiiaio34(kq,jq,ix,iy)=aiiaio34(kq,jq,ix,iy)+                  &
                    mat44(iq,0,0,kq,ix,iy)*mat34(jq,jxo,0,iq,ix,iy)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
!-------------------------------------------------------------------------------
!       complete the computation of A_oo-A_oi.A_ii**-1.A_io
!-------------------------------------------------------------------------------
        !$acc parallel present(mat,mat31,mat32,mat33,mat41,mat42,mat43,aiiaio34)&
        !$acc copyin(jxo) wait(mat%id) async(id) if(mat%on_gpu)
        !$acc loop gang collapse(2)
        DO iy=1,mat%my
          DO ix=1,mat%mx
!-------------------------------------------------------------------------------
!           modify the rows connecting to grid nodes (i.e. A_oo=A_gv):
!-------------------------------------------------------------------------------
            !$acc loop vector collapse(4)
            DO iq=1,mat%nq_type(1)
              DO iyo=0,1
                DO ixo=0,1
                  DO jq=1,mat%nq_type(3)
                    !$acc loop seq
                    DO kq=1,mat%nq_type(4)
                      mat31(jq,jxo+ixo,iyo,iq,ix-ixo,iy-iyo)=                   &
                        mat31(jq,jxo+ixo,iyo,iq,ix-ixo,iy-iyo)-                 &
                        mat41(kq,ixo,iyo,iq,ix-ixo,iy-iyo)*aiiaio34(kq,jq,ix,iy)
                    ENDDO
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
!-------------------------------------------------------------------------------
!           modify the rows connecting to horizontal side nodes
!           (i.e. A_oo=A_hv):
!-------------------------------------------------------------------------------
            !$acc loop vector collapse(3)
            DO iq=1,mat%nq_type(2)
              DO iyo=0,1
                DO jq=1,mat%nq_type(3)
                  !$acc loop seq
                  DO kq=1,mat%nq_type(4)
                    mat32(jq,jxo,iyo,iq,ix,iy-iyo)=                             &
                      mat32(jq,jxo,iyo,iq,ix,iy-iyo)-                           &
                      mat42(kq,0,iyo,iq,ix,iy-iyo)*aiiaio34(kq,jq,ix,iy)
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
!-------------------------------------------------------------------------------
!           modify the rows connecting to vertical side nodes (i.e. A_oo=A_vv):
!-------------------------------------------------------------------------------
            !$acc loop vector collapse(3)
            DO iq=1,mat%nq_type(3)
              DO ixo=0,1
                DO jq=1,mat%nq_type(3)
                  !$acc loop seq
                  DO kq=1,mat%nq_type(4)
                    mat33(jq,jxo+ixo,0,iq,ix-ixo,iy)=                           &
                      mat33(jq,jxo+ixo,0,iq,ix-ixo,iy)-                         &
                      mat43(kq,ixo,0,iq,ix-ixo,iy)*aiiaio34(kq,jq,ix,iy)
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      ENDDO
      !$acc exit data delete(aiiaio34) finalize async(id) if(mat%on_gpu)
      DEALLOCATE(aiiaio34)
    END ASSOCIATE
    IF (mat%on_gpu) CALL par%wait_streams(mat%id,3)
!-------------------------------------------------------------------------------
!   set flag.
!-------------------------------------------------------------------------------
    mat%eliminated=.true.
    CALL timer%end_timer_l1(iftn,idepth)
  END SUBROUTINE elim_inv_int_real

!-------------------------------------------------------------------------------
!> for matrices partitioned into cell-interior / other, find
! A_ii**-1.b_i and b_o - A_oi.A_ii**-1.b_i (where i means interior
! and o is other).  assume that elim_inv_int_real has been
! called, so A_ii**-1 is available in the A_ii storage.
!-------------------------------------------------------------------------------
  SUBROUTINE elim_presolve_real(mat,input,output)
    USE vector_mod
    USE vec_rect_2D_mod_acc
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real_acc), INTENT(IN) :: mat
    !> vector on which to eliminate interior data
    CLASS(rvector), INTENT(IN) :: input
    !> vector on which to store eliminated form
    CLASS(rvector), INTENT(INOUT) :: output

    INTEGER(i4) :: id
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

!-------------------------------------------------------------------------------
!   check for poly_degree=1
!-------------------------------------------------------------------------------
    IF (mat%nbtype==1) RETURN
    CALL timer%start_timer_l2(mod_name,'elim_presolve_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   check that A_ii has been inverted.
!-------------------------------------------------------------------------------
    IF (.NOT.mat%eliminated) CALL par%nim_stop('elim_presolve_real:'//          &
                                             ' dense interior is not factored.')
!-------------------------------------------------------------------------------
!   compute and save A_ii**-1.b_i first.
!-------------------------------------------------------------------------------
    SELECT TYPE(input)
    TYPE IS(vec_rect_2D_real_acc)
      SELECT TYPE(output)
      TYPE IS(vec_rect_2D_real_acc)
        ASSOCIATE (arri=>output%arri)
          !$acc kernels present(arri) async(mat%id) if(mat%on_gpu)
          arri=0._r8
          !$acc end kernels
        END ASSOCIATE
        CALL matvec_kernel(output%arri,mat%bsc(4,4)%arr,input%arri,mat,4,4)
        id=par%get_stream(mat%id,4,4)
        !$acc wait(id) async(mat%id) if(mat%on_gpu)
!-------------------------------------------------------------------------------
!       if there are no bases that are continuous across element borders
!-------------------------------------------------------------------------------
        IF (mat%nqcon==0) THEN
          CALL timer%end_timer_l2(iftn,idepth)
          RETURN
        ENDIF
!-------------------------------------------------------------------------------
!       now find b_o-A_oi.A_ii**-1.b_i for each of the "other" types
!       of bases.
!-------------------------------------------------------------------------------
        ASSOCIATE (out_arr=>output%arr,in_arr=>input%arr,                       &
                   out_arrh=>output%arrh,in_arrh=>input%arrh,                   &
                   out_arrv=>output%arrv,in_arrv=>input%arrv)
          !$acc kernels present(out_arr,in_arr,out_arrh,in_arrh)                &
          !$acc present(out_arrv,in_arrv) async(mat%id) if(mat%on_gpu)
          out_arr=-in_arr
          out_arrh=-in_arrh
          out_arrv=-in_arrv
          !$acc end kernels
        END ASSOCIATE
        CALL matvec_kernel(output%arr,mat%bsc(4,1)%arr,output%arri,mat,4,1)
        CALL matvec_kernel(output%arrh,mat%bsc(4,2)%arr,output%arri,mat,4,2)
        CALL matvec_kernel(output%arrv,mat%bsc(4,3)%arr,output%arri,mat,4,3)
        IF (mat%on_gpu) CALL par%wait_streams(mat%id,4)
        ASSOCIATE (out_arr=>output%arr,out_arrh=>output%arrh,                   &
                   out_arrv=>output%arrv)
          !$acc kernels present(out_arr,out_arrh,out_arrv)                      &
          !$acc async(mat%id) if(mat%on_gpu)
          out_arr=-out_arr
          out_arrh=-out_arrh
          out_arrv=-out_arrv
          !$acc end kernels
        END ASSOCIATE
      CLASS DEFAULT
        CALL par%nim_stop("mat_rect_2D_real_acc::elim_presolve_real"//          &
                          " unrecognized output type")
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop("mat_rect_2D_real_acc::elim_presolve_real"//            &
                        " unrecognized input type")
    END SELECT
!-------------------------------------------------------------------------------
!   set output vector to skip operations on arri
!-------------------------------------------------------------------------------
    output%skip_elim_interior=.TRUE.
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE elim_presolve_real

!-------------------------------------------------------------------------------
!> for matrices partitioned into cell-interior / other,
! subtract A_ii**-1.A_io.x_o from A_ii**-1.b_i to get x_i.
! the output should contain A_ii**-1.b_i in the interior storage on call.
!-------------------------------------------------------------------------------
  SUBROUTINE elim_postsolve_real(mat,input,output)
    USE vector_mod
    USE vec_rect_2D_mod_acc
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real_acc), INTENT(IN) :: mat
    !* vector from which to restore interior data
    !  should contain A_ii**-1.b_i in the interior before this call
    CLASS(rvector), INTENT(INOUT) :: input
    !* vector to which to restore full data (i.e. find x_i)
    !  should contain x_o in the exterior before this call
    CLASS(rvector), INTENT(INOUT) :: output

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

!-------------------------------------------------------------------------------
!   check for poly_degree=1
!-------------------------------------------------------------------------------
    IF (mat%nbtype==1) RETURN
    CALL timer%start_timer_l2(mod_name,'elim_postsolve_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   compute -A_io.x_o first in the output interior
!-------------------------------------------------------------------------------
    SELECT TYPE(input)
    TYPE IS(vec_rect_2D_real_acc)
      SELECT TYPE(output)
      TYPE IS(vec_rect_2D_real_acc)
        ASSOCIATE (arri=>output%arri)
          !$acc kernels present(arri) async(mat%id) if(mat%on_gpu)
          arri=0._r8
          !$acc end kernels
        END ASSOCIATE
        CALL matvec_kernel(output%arri,mat%bsc(1,4)%arr,output%arr,mat,1,4)
        CALL matvec_kernel(output%arri,mat%bsc(2,4)%arr,output%arrh,mat,2,4)
        CALL matvec_kernel(output%arri,mat%bsc(3,4)%arr,output%arrv,mat,3,4)
        IF (mat%on_gpu) CALL par%wait_streams(mat%id,4)
        ASSOCIATE (arri=>output%arri)
          !$acc kernels present(arri) async(mat%id) if(mat%on_gpu)
          arri=-arri
          !$acc end kernels
        END ASSOCIATE
!-------------------------------------------------------------------------------
!       now find x_ii=A_ii**-1.b_i-A_ii**-1.A_io.x_o.
!       A_ii**-1.b_i is in input, use this as work space
!-------------------------------------------------------------------------------
        CALL matvec_kernel(input%arri,mat%bsc(4,4)%arr,output%arri,mat,4,4)
        IF (mat%on_gpu) CALL par%wait_streams(mat%id,4)
        ASSOCIATE (out_arri=>output%arri,in_arri=>input%arri)
          !$acc kernels present(out_arri,in_arri) async(mat%id) if(mat%on_gpu)
          out_arri=in_arri
          !$acc end kernels
        END ASSOCIATE
      CLASS DEFAULT
        CALL par%nim_stop("mat_rect_2D_real_acc::elim_postsolve_real"//         &
                          "unrecognized output type")
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop("mat_rect_2D_real_acc::elim_postsolve_real"//           &
                        "unrecognized input type")
    END SELECT
!-------------------------------------------------------------------------------
!   set output vector to do operations on arri
!-------------------------------------------------------------------------------
    output%skip_elim_interior=.FALSE.
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE elim_postsolve_real

!-------------------------------------------------------------------------------
!> determine a scaling factor for diagonal
! matrix elements that is based on grid-vertex entries.
!-------------------------------------------------------------------------------
  SUBROUTINE find_diag_scale_real(mat)
    USE local
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real_acc), INTENT(INOUT) :: mat

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

!-------------------------------------------------------------------------------
!   the scaling factor, regularity, and boundary operations are
!   not called if there are no continuous fields.
!-------------------------------------------------------------------------------
    IF (mat%nqcon==0) RETURN
    CALL timer%start_timer_l2(mod_name,'find_diag_scale_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   find the scaling factor
!-------------------------------------------------------------------------------
    ASSOCIATE (arr=>mat%bsc(1,1)%arr)
      !$acc kernels present(arr,mat) async(mat%id) if(mat%on_gpu)
      mat%diag_scale=MAXVAL(ABS(arr(:,0,0,:,:,:)))
      !$acc end kernels
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE find_diag_scale_real

!-------------------------------------------------------------------------------
!> apply Dirichlet boundary conditions to a Cartesian operator.
! If the specified component is tangent, the resultant matrix is
! (I-tt-zz).M.(I-tt-zz)+tt+zz, where t is the surface tangent in
! the computational plane, z is the unit vector normal to the plane,
! and I is the identity matrix.  If the specified component is
! normal, the resultant matrix is (I-nn).M.(I-nn)+nn, where n is
! the surface normal.  If the input is all, then the result is
! (I-tt-zz-nn).M.(I-tt-zz-nn)+tt+zz+nn.
!
! when the end of the component parameter is 'offdiag,' the passed
! matrix structure contains only off-diagonal matrix entries, and
! there is no diagonal entry to enter after couplings are
! eliminated.
!-------------------------------------------------------------------------------
  SUBROUTINE dirichlet_bc_real(mat,component,edge,symm)
    USE boundary_ftns_mod
    USE edge_mod
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real_acc), INTENT(INOUT) :: mat
    !> flag to determine normal/tangential/scalar behavior
    CHARACTER(*), INTENT(IN) :: component
    !> associated edge
    TYPE(edge_type), INTENT(IN) :: edge
    !> flag for symmetric boundary
    CHARACTER(*), INTENT(IN), OPTIONAL :: symm

    INTEGER(i4) :: iv,is,ix,iy,itype,jtype,isymm,nside
    LOGICAL :: diag
    REAL(r8) :: bcpmat(mat%nqcon,mat%nqcon,edge%nvert)
    REAL(r8) :: bcpmats(mat%nqcon,mat%nqcon,(mat%pdcon-1)*edge%nvert)
    INTEGER(i4), DIMENSION(mat%nqcon,2) :: bciarr
    LOGICAL :: exhsegment(edge%nvert),exvsegment(edge%nvert)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dirichlet_bc_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   set the symmetry flag.  if symm starts with "t", the top boundary
!   is a symmetry condition.  if symm starts with "b", the bottom
!   boundary is a symmetry condition.
!-------------------------------------------------------------------------------
    isymm=0
    IF (PRESENT(symm)) THEN
      SELECT CASE(symm(1:1))
      CASE('t','T')
        isymm=1
      CASE('b','B')
        isymm=-1
      END SELECT
    ENDIF
!-------------------------------------------------------------------------------
!   parse the component flag to create an integer array, which
!   indicates which scalar and 3-vector components have essential
!   conditions.
!-------------------------------------------------------------------------------
    CALL bcflag_parse(component,mat%nqcon,bciarr,diag)
!-------------------------------------------------------------------------------
!   save the essential-condition flag.
!-------------------------------------------------------------------------------
    mat%essential_cond=component
!-------------------------------------------------------------------------------
!   loop over all external boundary points and zero the couplings
!   to the specified components along the boundary.
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   rblock basis types indices are
!   1 == grid vertex centered
!   2 == horizontal side centered
!   3 == vertical side centered
!   4 == interior centered
!   1 : 3 are affected by boundary conditions.
!   note that itype is the to basis type and jtype is the from
!   basis type.
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   setup bcpmat -- transformation array: uu
!-------------------------------------------------------------------------------
    nside=mat%pdcon-1
    exhsegment=.FALSE.
    exvsegment=.FALSE.
    DO iv=1,edge%nvert
      IF (edge%expoint(iv)) THEN
        ix=edge%vertex(iv)%intxy(1)
        iy=edge%vertex(iv)%intxy(2)
        CALL bcdir_set(mat%nqcon,bciarr,edge%excorner(iv),isymm,                &
                       edge%vertex(iv)%norm,edge%vertex(iv)%tang,bcpmat(:,:,iv))
      ENDIF
      IF (edge%exsegment(iv)) THEN
        ix=edge%segment(iv)%intxys(1)
        iy=edge%segment(iv)%intxys(2)
        DO is=1,nside
          CALL bcdir_set(mat%nqcon,bciarr,.FALSE.,isymm,                        &
                         edge%segment(iv)%norm(:,is),                           &
                         edge%segment(iv)%tang(:,is),                           &
                         bcpmats(:,:,is+(iv-1)*nside))
        ENDDO
        IF (edge%segment(iv)%h_side) THEN
          exhsegment(iv)=.TRUE.
        ELSE
          exvsegment(iv)=.TRUE.
        ENDIF
      ENDIF
    ENDDO
    !$acc enter data copyin(bcpmat,bcpmats,exhsegment,exvsegment)               &
    !$acc async(mat%id) if(mat%on_gpu)
!-------------------------------------------------------------------------------
!   eliminate couplings from the specified components only.
!-------------------------------------------------------------------------------
    DO itype=1,mat%nbtype
      DO jtype=1,MIN(mat%nbtype,3_i4)
        IF (jtype==1) THEN
          CALL elim_from(bcpmat,edge%expoint)
        ELSE IF (jtype==2) THEN
          CALL elim_from(bcpmats,exhsegment)
        ELSE IF (jtype==3) THEN
          CALL elim_from(bcpmats,exvsegment)
        ENDIF
      ENDDO
    ENDDO
!-------------------------------------------------------------------------------
!   eliminate couplings to the specified component only.
!-------------------------------------------------------------------------------
    DO itype=1,MIN(mat%nbtype,3_i4)
      DO jtype=1,mat%nbtype
        IF (itype==1) THEN
          CALL elim_to(bcpmat,edge%expoint)
        ELSE IF (itype==2) THEN
          CALL elim_to(bcpmats,exhsegment)
        ELSE IF (itype==3) THEN
          CALL elim_to(bcpmats,exvsegment)
        ENDIF
      ENDDO
    ENDDO
    !$acc exit data delete(bcpmat,bcpmats,exhsegment,exvsegment)                &
    !$acc finalize async(mat%id) if(mat%on_gpu)
    CALL timer%end_timer_l2(iftn,idepth)
  CONTAINS

    SUBROUTINE elim_from(bcparr,exarr)
      IMPLICIT NONE

      REAL(r8), CONTIGUOUS, INTENT(IN) :: bcparr(:,:,:)
      LOGICAL, CONTIGUOUS, INTENT(IN) :: exarr(:)

      INTEGER(i4) :: ivs,jxmin,jxmax,jymin,jymax,jx,jy,ijx,ijy,iq,jmat,         &
                     jq0,jq1,jq2,jqe
      REAL(r8) :: proj

      ASSOCIATE(rmat=>mat%bsc(jtype,itype)%arr,                                 &
                vertex=>edge%vertex,segment=>edge%segment,                      &
                iy0=>mat%iy0(itype),ix0=>mat%ix0(itype),                        &
                jy0=>mat%iy0(jtype),jx0=>mat%ix0(jtype),                        &
                inbasis=>mat%nq_type(itype),jntype=>mat%nb_type(jtype))
        !$acc parallel present(mat,rmat,bcparr,edge,vertex,segment)             &
        !$acc present(exarr,inbasis,jntype,ix0,iy0,jx0,jy0)                     &
        !$acc copyin(jtype,nside) async(mat%id) if(mat%on_gpu)
        !$acc loop gang private(ix,iy,jq0,jqe,ivs,jxmin,jxmax,jymin,jymax)
        DO iv=1,edge%nvert
          IF (exarr(iv)) THEN
            IF (jtype==1) THEN
              ix=vertex(iv)%intxy(1)
              iy=vertex(iv)%intxy(2)
            ELSE
              ix=segment(iv)%intxys(1)
              iy=segment(iv)%intxys(2)
            ENDIF
            jxmin=MAX(ix0-1,ix0-ix)
            jxmax=MIN(1-jx0,mat%mx-ix)
            jymin=MAX(iy0-1,iy0-iy)
            jymax=MIN(1-jy0,mat%my-iy)
            DO jmat=1,jntype
!-------------------------------------------------------------------------------
!             find the vector elements of M.uu then subtract M.uu from M
!-------------------------------------------------------------------------------
              jq0=(jmat-1)*mat%nqcon+1
              jqe=jmat*mat%nqcon
              IF (jtype==1) THEN
                ivs=iv
              ELSE
                ivs=jmat+(iv-1)*nside
              ENDIF
              !$acc loop worker collapse(2)
              DO jy=jymin,jymax
                DO jx=jxmin,jxmax
                  !$acc loop vector collapse(2) private(ijy,ijx,proj)
                  DO iq=1,inbasis
                    DO jq1=1,mat%nqcon
                      ijy=iy+jy
                      ijx=ix+jx
                      proj=0._r8
                      !$acc loop seq
                      DO jq2=1,mat%nqcon
                        proj=proj+bcparr(jq1,jq2,ivs)                           &
                                  *rmat(jq0+jq2-1,-jx,-jy,iq,ijx,ijy)
                      ENDDO
                      rmat(jq1+jq0-1,-jx,-jy,iq,ijx,ijy)=                       &
                        rmat(jq1+jq0-1,-jx,-jy,iq,ijx,ijy)-proj
                    ENDDO
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDIF
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    END SUBROUTINE elim_from

    SUBROUTINE elim_to(bcparr,exarr)
      IMPLICIT NONE

      REAL(r8), CONTIGUOUS, INTENT(IN) :: bcparr(:,:,:)
      LOGICAL, CONTIGUOUS, INTENT(IN) :: exarr(:)

      INTEGER(i4) :: ivs,jxmin,jxmax,jymin,jymax,jx,jy,imat,jq,                 &
                     iq0,iq1,iq2,iqe
      REAL(r8) :: proj

      ASSOCIATE(rmat=>mat%bsc(jtype,itype)%arr,                                 &
                vertex=>edge%vertex,segment=>edge%segment,                      &
                iy0=>mat%iy0(itype),ix0=>mat%ix0(itype),                        &
                jy0=>mat%iy0(jtype),jx0=>mat%ix0(jtype),                        &
                intype=>mat%nb_type(itype),jnbasis=>mat%nq_type(jtype))
        !$acc parallel present(mat,rmat,bcparr,edge,vertex,segment)             &
        !$acc present(exarr,intype,jnbasis,ix0,iy0,jx0,jy0)                     &
        !$acc copyin(itype,jtype,nside,diag) async(mat%id) if(mat%on_gpu)
        !$acc loop gang private(ix,iy,iq0,iqe,ivs,jxmin,jxmax,jymin,jymax)
        DO iv=1,edge%nvert
          IF (exarr(iv)) THEN
            IF (itype==1) THEN
              ix=vertex(iv)%intxy(1)
              iy=vertex(iv)%intxy(2)
            ELSE
              ix=segment(iv)%intxys(1)
              iy=segment(iv)%intxys(2)
            ENDIF
            jxmin=MAX(jx0-1,jx0-ix)
            jxmax=MIN(1-ix0,mat%mx-ix)
            jymin=MAX(jy0-1,jy0-iy)
            jymax=MIN(1-iy0,mat%my-iy)
            DO imat=1,intype
!-------------------------------------------------------------------------------
!             find the vector elements of uu.M then subtract uu.M from M
!-------------------------------------------------------------------------------
              iq0=(imat-1)*mat%nqcon+1
              iqe=imat*mat%nqcon
              IF (itype==1) THEN
                ivs=iv
              ELSE
                ivs=imat+(iv-1)*nside
              ENDIF
              !$acc loop worker collapse(2)
              DO jy=jymin,jymax
                DO jx=jxmin,jxmax
                  !$acc loop vector collapse(2) private(proj)
                  DO jq=1,jnbasis
                    DO iq1=1,mat%nqcon
                      proj=0._r8
                      !$acc loop seq
                      DO iq2=1,mat%nqcon
                        proj=proj+bcparr(iq1,iq2,ivs)                           &
                                  *rmat(jq,jx,jy,iq0+iq2-1,ix,iy)
                      ENDDO
                      rmat(jq,jx,jy,iq1+iq0-1,ix,iy)=                           &
                        rmat(jq,jx,jy,iq1+iq0-1,ix,iy)-proj
                    ENDDO
                  ENDDO
                ENDDO
              ENDDO
!-------------------------------------------------------------------------------
!             take care of uu and zz.
!-------------------------------------------------------------------------------
              IF (jtype==itype.AND.diag) THEN
                !$acc loop vector collapse(2)
                DO iq1=iq0,iqe
                  DO iq2=iq0,iqe
                    rmat(iq2,0,0,iq1,ix,iy)=rmat(iq2,0,0,iq1,ix,iy)             &
                      +bcparr(iq2-iq0+1,iq1-iq0+1,ivs)*mat%diag_scale
                  ENDDO
                ENDDO
              ENDIF
            ENDDO
          ENDIF
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    END SUBROUTINE elim_to

  END SUBROUTINE dirichlet_bc_real

!-------------------------------------------------------------------------------
!> apply regularity conditions to R=0 points of an operator.
!  the action on the matrix is (I-uu).M.(I-uu)+uu, where u is the
!  unit vector for the components that need elimination.  the
!  vcomp list of component descriptions in the matrix structure
!  and the fcomp Fourier component index are used to decide
!  where to apply this action.
!
!  if dscale is provided, it is used to scale the added uu entry,
!  instead of determining the scaling on the fly.
!-------------------------------------------------------------------------------
  SUBROUTINE regularity_real(mat,edge)
    USE edge_mod
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real_acc), INTENT(INOUT) :: mat
    !> associated edge
    TYPE(edge_type), INTENT(IN) :: edge

    REAL(r8), DIMENSION(mat%nqcon,mat%nqcon) :: mult,multj
    INTEGER(i4) :: iv,jcomp,itype,jtype,jq,nvec
    REAL(r8), DIMENSION(mat%nqcon) :: mult_v,mult_j
    LOGICAL :: r0hsegment(edge%nvert),r0vsegment(edge%nvert)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'regularity_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   apply regularity conditions to the different vector components
!   of a matrix.  the elements at R=0 are uncoupled for n>0, if the
!   matrix acts on a scalar field.  the elements at R=0 of a matrix
!   for a vector field are uncoupled for:
!   -  n=0:  r and phi
!   -  n=1:  z
!   -  n>1:  r, z, and phi
!   a covariant phi component (a_phi*r) is uncoupled for all n.  these
!   components are indicated with 'c'.
!-------------------------------------------------------------------------------
    nvec=mat%nqcon/3
!-------------------------------------------------------------------------------
!   create an array of 1s and 0s to zero out the appropriate
!   components.  the geometry is always toroidal at this point.
!
!   the second array, mult_j, is used for the columns.
!   [preconditioning may require matrix elements with different
!   Fourier indices for rows and columns.]
!-------------------------------------------------------------------------------
    mult_v=0
    SELECT CASE(mat%fcomp)
    CASE(0)
      WHERE(mat%vcomp=='s') mult_v=1
      WHERE(mat%vcomp=='z') mult_v=1
    CASE(1)
      WHERE(mat%vcomp=='r') mult_v=1
    END SELECT
    mult_j=0
    jcomp=mat%fcomp
    SELECT CASE(ABS(jcomp))
    CASE(0)
      WHERE(mat%vcomp=='s') mult_j=1
      WHERE(mat%vcomp=='z') mult_j=1
    CASE(1)
      WHERE(mat%vcomp=='r') mult_j=1
    END SELECT
    DO jq=1,mat%nqcon
      mult(jq,:)=mult_v
      multj(:,jq)=mult_j
    ENDDO
!-------------------------------------------------------------------------------
!   loop over the borders of blocks touching R=0, and decouple the
!   appropriate matrix elements.
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   rblock basis types indices are
!   1 == grid vertex centered
!   2 == horizontal side centered
!   3 == vertical side centered
!   4 == interior centered
!   1 : 3 are affected by boundary conditions.
!   note that itype is the to basis type and jtype is the from
!   basis type.
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!   setup r0hsegment and r0vsegment
!-------------------------------------------------------------------------------
    r0hsegment=.FALSE.
    r0vsegment=.FALSE.
    DO iv=1,edge%nvert
      IF (edge%r0segment(iv)) THEN
        IF (edge%segment(iv)%h_side) THEN
          r0hsegment(iv)=.TRUE.
        ELSE
          r0vsegment(iv)=.TRUE.
        ENDIF
      ENDIF
    ENDDO
    !$acc enter data copyin(r0hsegment,r0vsegment,mult_v)                       &
    !$acc async(mat%id) if(mat%on_gpu)
!-------------------------------------------------------------------------------
!   average the specified components only.
!-------------------------------------------------------------------------------
    IF (ABS(jcomp)==1.AND.MODULO(mat%nqcon,3_i4)==0) THEN
      DO itype=1,mat%nbtype
        DO jtype=1,MIN(mat%nbtype,3_i4)
          IF (jtype==1) THEN
            CALL regularity_avg(edge%r0point)
          ELSE IF (jtype==2) THEN
            CALL regularity_avg(r0hsegment)
          ELSE IF (jtype==3) THEN
            CALL regularity_avg(r0vsegment)
          ENDIF
        ENDDO
      ENDDO
    ENDIF
!-------------------------------------------------------------------------------
!   eliminate couplings to the specified component only.
!-------------------------------------------------------------------------------
    DO itype=1,MIN(mat%nbtype,3_i4)
      DO jtype=1,mat%nbtype
        IF (itype==1) THEN
          CALL regularity_to(edge%r0point)
        ELSE IF (itype==2) THEN
          CALL regularity_to(r0hsegment)
        ELSE IF (itype==3) THEN
          CALL regularity_to(r0vsegment)
        ENDIF
      ENDDO
    ENDDO
!-------------------------------------------------------------------------------
!   eliminate couplings from the specified components only.
!-------------------------------------------------------------------------------
    DO itype=1,mat%nbtype
      DO jtype=1,MIN(mat%nbtype,3_i4)
        IF (jtype==1) THEN
          CALL regularity_from(edge%r0point)
        ELSE IF (jtype==2) THEN
          CALL regularity_from(r0hsegment)
        ELSE IF (jtype==3) THEN
          CALL regularity_from(r0vsegment)
        ENDIF
      ENDDO
    ENDDO
    !$acc exit data delete(r0hsegment,r0vsegment,mult_v)                        &
    !$acc finalize async(mat%id) if(mat%on_gpu)
    CALL timer%end_timer_l2(iftn,idepth)
  CONTAINS

    SUBROUTINE regularity_avg(r0arr)
      IMPLICIT NONE

      LOGICAL, CONTIGUOUS, INTENT(IN) :: r0arr(:)

      INTEGER(i4) :: ix,iy,jxmin,jxmax,jymin,jymax,                             &
                     jmat,jy,jx,ijy,ijx,ivec,jq0,jq1
      ASSOCIATE(rmat=>mat%bsc(jtype,itype)%arr,                                 &
                vertex=>edge%vertex,segment=>edge%segment,                      &
                iy0=>mat%iy0(itype),ix0=>mat%ix0(itype),                        &
                jy0=>mat%iy0(jtype),jx0=>mat%ix0(jtype),                        &
                jntype=>mat%nb_type(jtype))
        !$acc parallel present(mat,rmat,edge,vertex,segment)                    &
        !$acc present(r0arr,jntype,ix0,iy0,jx0,jy0)                             &
        !$acc copyin(jtype,jcomp) async(mat%id) if(mat%on_gpu)
        !$acc loop gang private(ix,iy,jxmin,jxmax,jymin,jymax)
        DO iv=1,edge%nvert
          IF (r0arr(iv)) THEN
            IF (jtype==1) THEN
              ix=vertex(iv)%intxy(1)
              iy=vertex(iv)%intxy(2)
            ELSE
              ix=segment(iv)%intxys(1)
              iy=segment(iv)%intxys(2)
            ENDIF
            jxmin=MAX(ix0-1,ix0-ix)
            jxmax=MIN(1-jx0,mat%mx-ix)
            jymin=MAX(iy0-1,iy0-iy)
            jymax=MIN(1-jy0,mat%my-iy)
!-------------------------------------------------------------------------------
!           combine r and phi equations to find averages in the
!           matrix that enforce Vec_phi=i*Vec_r for n=1.  for
!           preconditioning, we may also need Vec_phi=-i*Vec_r
!           for n=-1.
!
!           mathematically, the steps are 1) change variables
!           to X1=(Vec_r+i*Vec_phi)/2 and X2=(Vec_r-i*Vec_phi)/2
!           for each vertex at R=0, 2) set X1=0 (remove
!           couplings to and from X1) giving an overdetermined
!           system of equations, 3) add -i*(the X2-equation) to
!           the X1 equation.
!
!           for the n=-1 preconditioning matrix case, we will
!           set X2 to zero and keep X1 in the r-comp location.
!-------------------------------------------------------------------------------
            !$acc loop worker
            DO jmat=1,jntype
              !$acc loop vector collapse(2) private(ijy,ijx)
              DO jy=jymin,jymax
                DO jx=jxmin,jxmax
                  ijy=iy+jy
                  ijx=ix+jx
                  !$acc loop seq private(jq0,jq1)
                  DO ivec=0,nvec-1
                    jq0=(jmat-1)*mat%nqcon+3*ivec+1
                    jq1=jq0+2
                    rmat(jq0,-jx,-jy,:,ijx,ijy)=                                &
                         rmat(jq0,-jx,-jy,:,ijx,ijy)                            &
                        -rmat(jq1,-jx,-jy,:,ijx,ijy)*jcomp
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDIF
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    END SUBROUTINE regularity_avg

    SUBROUTINE regularity_to(r0arr)
      IMPLICIT NONE

      LOGICAL, CONTIGUOUS, INTENT(IN) :: r0arr(:)

      INTEGER(i4) :: ix,iy,jxmin,jxmax,jymin,jymax,                             &
                     imat,jy,jx,iq0,iq1,ivec,iv0,iv1
      ASSOCIATE(rmat=>mat%bsc(jtype,itype)%arr,                                 &
                vertex=>edge%vertex,segment=>edge%segment,                      &
                iy0=>mat%iy0(itype),ix0=>mat%ix0(itype),                        &
                jy0=>mat%iy0(jtype),jx0=>mat%ix0(jtype),                        &
                intype=>mat%nb_type(itype),jnbasis=>mat%nq_type(jtype))
        !$acc parallel present(mat,rmat,edge,vertex,segment)                    &
        !$acc present(mult_v,r0arr,intype,jnbasis,ix0,iy0,jx0,jy0)              &
        !$acc copyin(itype) async(mat%id) if(mat%on_gpu)
        !$acc loop gang private(ix,iy,jxmin,jxmax,jymin,jymax)
        DO iv=1,edge%nvert
          IF (r0arr(iv)) THEN
            IF (itype==1) THEN
              ix=vertex(iv)%intxy(1)
              iy=vertex(iv)%intxy(2)
            ELSE
              ix=segment(iv)%intxys(1)
              iy=segment(iv)%intxys(2)
            ENDIF
            jxmin=MAX(jx0-1,jx0-ix)
            jxmax=MIN(1-ix0,mat%mx-ix)
            jymin=MAX(jy0-1,jy0-iy)
            jymax=MIN(1-iy0,mat%my-iy)
!-------------------------------------------------------------------------------
!           combine r and phi equations to find averages in the
!           matrix that enforce Vec_phi=i*Vec_r for n=1.  [note
!           rows always represent n>=0.]
!  
!           then, eliminate couplings to this node.
!-------------------------------------------------------------------------------
            !$acc loop worker private(iq0,iq1)
            DO imat=1,intype
              iq0=(imat-1)*mat%nqcon+1
              iq1=imat*mat%nqcon
              !$acc loop vector collapse(2)
              DO jy=jymin,jymax
                DO jx=jxmin,jxmax
                  IF (mat%fcomp==1.AND.MODULO(mat%nqcon,3_i4)==0) THEN
                    !$acc loop seq private(iv0,iv1)
                    DO ivec=0,nvec-1
                      iv0=(imat-1)*mat%nqcon+3*ivec+1
                      iv1=iv0+2
                      rmat(:,jx,jy,iv0,ix,iy)=                                  &
                          rmat(:,jx,jy,iv0,ix,iy)                               &
                         -rmat(:,jx,jy,iv1,ix,iy)
                    ENDDO
                  ENDIF
                  !$acc loop seq
                  DO jq=1,jnbasis
                    rmat(jq,jx,jy,iq0:iq1,ix,iy)=                               &
                        rmat(jq,jx,jy,iq0:iq1,ix,iy)*mult_v
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDIF
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    END SUBROUTINE regularity_to

    SUBROUTINE regularity_from(r0arr)
      IMPLICIT NONE

      LOGICAL, CONTIGUOUS, INTENT(IN) :: r0arr(:)

      INTEGER(i4) :: ix,iy,jxmin,jxmax,jymin,jymax,                             &
                     jmat,jy,jx,ijy,ijx,iq,jq0,jq1,jq
      ASSOCIATE(rmat=>mat%bsc(jtype,itype)%arr,                                 &
                vertex=>edge%vertex,segment=>edge%segment,                      &
                iy0=>mat%iy0(itype),ix0=>mat%ix0(itype),                        &
                jy0=>mat%iy0(jtype),jx0=>mat%ix0(jtype),                        &
                jntype=>mat%nb_type(jtype),inbasis=>mat%nq_type(itype))
        !$acc parallel present(mat,rmat,edge,vertex,segment)                    &
        !$acc present(mult_v,r0arr,jntype,inbasis,ix0,iy0,jx0,jy0)              &
        !$acc copyin(mult_j,jtype,itype) async(mat%id) if(mat%on_gpu)
        !$acc loop gang private(ix,iy,jxmin,jxmax,jymin,jymax)
        DO iv=1,edge%nvert
          IF (r0arr(iv)) THEN
            IF (jtype==1) THEN
              ix=vertex(iv)%intxy(1)
              iy=vertex(iv)%intxy(2)
            ELSE
              ix=segment(iv)%intxys(1)
              iy=segment(iv)%intxys(2)
            ENDIF
            jxmin=MAX(ix0-1,ix0-ix)
            jxmax=MIN(1-jx0,mat%mx-ix)
            jymin=MAX(iy0-1,iy0-iy)
            jymax=MIN(1-jy0,mat%my-iy)
!-------------------------------------------------------------------------------
!           eliminate couplings from the node on R=0.
!-------------------------------------------------------------------------------
            !$acc loop worker private(jq0,jq1)
            DO jmat=1,jntype
              jq0=(jmat-1)*mat%nqcon+1
              jq1=jmat*mat%nqcon
              !$acc loop vector collapse(2) private(ijy,ijx)
              DO jy=jymin,jymax
                DO jx=jxmin,jxmax
                  ijy=iy+jy
                  ijx=ix+jx
                  !$acc loop seq
                  DO iq=1,inbasis
                    rmat(jq0:jq1,-jx,-jy,iq,ijx,ijy)=                           &
                        rmat(jq0:jq1,-jx,-jy,iq,ijx,ijy)*mult_j
                  ENDDO
                ENDDO
              ENDDO
!-------------------------------------------------------------------------------
!             take care of uu and zz.
!-------------------------------------------------------------------------------
              IF (jtype==itype) THEN
                !$acc loop seq
                DO jq=0,mat%nqcon-1
                  rmat(jq+jq0,0,0,jq+jq0,ix,iy)=rmat(jq+jq0,0,0,jq+jq0,ix,iy)   &
                                                +(1-mult_v(jq+1))*mat%diag_scale
                ENDDO
              ENDIF
            ENDDO
          ENDIF
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    END SUBROUTINE regularity_from

  END SUBROUTINE regularity_real

!-------------------------------------------------------------------------------
!* Extract the diagonal as a vector
!-------------------------------------------------------------------------------
  SUBROUTINE get_diag_as_vec_real(mat,output_vec)
    USE vec_rect_2D_mod_acc
    USE vector_mod
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real_acc), INTENT(IN) :: mat
    !> output vector
    CLASS(rvector), INTENT(INOUT) :: output_vec

    INTEGER(i4) :: iq,is,ii,iqs,iqi
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'get_diag_as_vec_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   extract the diagonal
!-------------------------------------------------------------------------------
    SELECT TYPE(output_vec)
    TYPE IS(vec_rect_2D_real_acc)
      IF (ASSOCIATED(output_vec%arr)) THEN
        ASSOCIATE(vec_arr=>output_vec%arr,mat_arr=>mat%bsc(1,1)%arr)
          !$acc parallel present(vec_arr,mat_arr,output_vec,mat)                &
          !$acc async(mat%id) if(mat%on_gpu)
          !$acc loop gang
          DO iq=1,output_vec%nqty
            vec_arr(iq,0:output_vec%mx,0:output_vec%my)=mat_arr(iq,0,0,iq,:,:)
          ENDDO
          !$acc end parallel
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(output_vec%arrh)) THEN
        ASSOCIATE(vec_arr=>output_vec%arrh,mat_arr=>mat%bsc(2,2)%arr)
          !$acc parallel present(vec_arr,mat_arr,output_vec,mat)                &
          !$acc async(mat%id) if(mat%on_gpu)
          !$acc loop gang collapse(2) private(iqs)
          DO iq=1,output_vec%nqty
            DO is=1,output_vec%n_side
              iqs=iq+mat%nqcon*(is-1)
              vec_arr(iq,is,1:output_vec%mx,0:output_vec%my)=                   &
                mat_arr(iqs,0,0,iqs,:,:)
            ENDDO
          ENDDO
          !$acc end parallel
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(output_vec%arrv)) THEN
        ASSOCIATE(vec_arr=>output_vec%arrv,mat_arr=>mat%bsc(3,3)%arr)
          !$acc parallel present(vec_arr,mat_arr,output_vec,mat)                &
          !$acc async(mat%id) if(mat%on_gpu)
          !$acc loop gang collapse(2) private(iqs)
          DO iq=1,output_vec%nqty
            DO is=1,output_vec%n_side
              iqs=iq+mat%nqcon*(is-1)
              vec_arr(iq,is,0:output_vec%mx,1:output_vec%my)=                   &
                mat_arr(iqs,0,0,iqs,:,:)
            ENDDO
          ENDDO
          !$acc end parallel
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(output_vec%arri).AND.                                      &
          .NOT.output_vec%skip_elim_interior) THEN
        ASSOCIATE(vec_arr=>output_vec%arri,mat_arr=>mat%bsc(4,4)%arr)
          !$acc parallel present(vec_arr,mat_arr,output_vec,mat)                &
          !$acc async(mat%id) if(mat%on_gpu)
          !$acc loop gang collapse(2) private(iqi)
          DO iq=1,output_vec%nqty
            DO ii=1,output_vec%n_int
              iqi=iq+mat%nqcon*(ii-1)
              vec_arr(iq,ii,1:output_vec%mx,1:output_vec%my)=                   &
                mat_arr(iqi,0,0,iqi,:,:)
            ENDDO
          ENDDO
          !$acc end parallel
        END ASSOCIATE
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real_acc for output_vec'          &
                        //' in get_diag_as_vec_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE get_diag_as_vec_real

!-------------------------------------------------------------------------------
!*  Set matrix to the identity tensor for testing
!-------------------------------------------------------------------------------
  SUBROUTINE set_identity_real(mat)
    IMPLICIT NONE

    !> matrix
    CLASS(mat_rect_2D_real_acc), INTENT(INOUT) :: mat

    INTEGER(i4) :: id,jd,iq
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

!-------------------------------------------------------------------------------
!   not called if there are no continuous fields.
!-------------------------------------------------------------------------------
    IF (mat%nqcon==0) RETURN
    CALL timer%start_timer_l2(mod_name,'set_identity_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   set to the identity
!-------------------------------------------------------------------------------
    DO id=1,mat%nbtype
      DO jd=1,mat%nbtype
        ASSOCIATE(arr=>mat%bsc(id,jd)%arr)
          !$acc kernels async(mat%id) present(arr) if(mat%on_gpu)
          arr=0._r8
          !$acc end kernels
        END ASSOCIATE
      ENDDO
      DO iq=1,mat%nq_type(id)
        ASSOCIATE(arr=>mat%bsc(id,id)%arr)
          !$acc kernels async(mat%id) present(arr) if(mat%on_gpu)
          arr(iq,0,0,iq,:,:)=1._r8
          !$acc end kernels
        END ASSOCIATE
      ENDDO
    ENDDO
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE set_identity_real

!-------------------------------------------------------------------------------
!> perform a real matrix/vector multiplication and return the output
!-------------------------------------------------------------------------------
  SUBROUTINE matvec_kernel(output,matrix,vector,mat,jb,ib)
    IMPLICIT NONE

    CLASS(mat_rect_2D_real_acc), INTENT(IN) :: mat
    INTEGER(i4), INTENT(IN) :: jb,ib
    REAL(r8), DIMENSION(mat%nq_type(ib),-1:mat%mx+1,-1:mat%my+1),               &
                        INTENT(INOUT) :: output
    REAL(r8), DIMENSION(mat%nq_type(jb),                                        &
                        mat%x0off(jb,ib):mat%x1off(jb,ib),                      &
                        mat%y0off(jb,ib):mat%y1off(jb,ib),                      &
                        mat%nq_type(ib),mat%ix0(ib):mat%mx,mat%iy0(ib):mat%my), &
                        INTENT(IN) :: matrix
    REAL(r8), DIMENSION(mat%nq_type(jb),-1:mat%mx+1,-1:mat%my+1),               &
                        INTENT(IN) :: vector

    INTEGER(i4) :: ix,iy,iq,jyl,jxl,jq,jy,jx,id,myt,mxt
    INTEGER(i4) :: inode_gang,inode_vec,nloop
    INTEGER(i4), PARAMETER :: vstride=128
    REAL(r8) :: val

!-------------------------------------------------------------------------------
!   create output vector.
!-------------------------------------------------------------------------------
    ASSOCIATE (iy0=>mat%iy0(ib),ix0=>mat%ix0(ib),                               &
               jy0=>mat%iy0(jb),jx0=>mat%ix0(jb),                               &
               y0off=>mat%y0off(jb,ib),y1off=>mat%y1off(jb,ib),                 &
               x0off=>mat%x0off(jb,ib),x1off=>mat%x1off(jb,ib))
      id=par%get_stream(mat%id,ib,4)
      myt=mat%my+1-iy0
      mxt=mat%mx+1-ix0
      !$acc parallel present(mat,output,matrix,vector,iy0,ix0,jy0,jx0)          &
      !$acc present(y0off,y1off,x0off,x1off) copyin(myt,mxt)                    &
      !$acc async(id) wait(mat%id) if(mat%on_gpu)
      !$acc loop gang private(nloop)
      DO inode_gang=1,myt*mxt,vstride
        nloop=MIN(vstride,myt*mxt-inode_gang+1)
        !$acc loop vector private(iy,ix,jy,jx,val)
        DO inode_vec=0,nloop-1
          iy=(inode_gang+inode_vec-1)/mxt+iy0
          ix=MOD(inode_gang+inode_vec-1,mxt)+ix0
          !$acc loop seq
          DO iq=1,mat%nq_type(ib)
            val=0._r8
            DO jyl=y0off,y1off
              jy=iy+jyl
              DO jxl=x0off,x1off
                jx=ix+jxl
                DO jq=1,mat%nq_type(jb)
                  val=val+matrix(jq,jxl,jyl,iq,ix,iy)*vector(jq,jx,jy)
                ENDDO
              ENDDO
            ENDDO
            output(iq,ix,iy)=output(iq,ix,iy)+val
          ENDDO
        ENDDO
      ENDDO
      !$acc end parallel
    END ASSOCIATE
  END SUBROUTINE matvec_kernel

END MODULE mat_rect_2D_real_mod_acc
