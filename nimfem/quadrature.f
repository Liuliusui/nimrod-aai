!-------------------------------------------------------------------------------
!< Contains definitions for the data structures for storage of field
!  data and derivatives at quadrature points
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!> Defines the data structures for storage of field data and derivatives
!  at quadrature points
!-------------------------------------------------------------------------------
MODULE quadrature_mod
  USE local
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: qp_real,qp_comp,qalpha_real,alpha_real,qalpha_block_real,           &
            batch_interp_real,interp_real,batch_interp_comp,interp_comp,        &
            qjac_real

!-------------------------------------------------------------------------------
!> Quadrature point storage for real quatities
!-------------------------------------------------------------------------------
  TYPE :: qp_real
    !> field values at each quadrature point
    !  size should be (ng,nel,fqty,nqty)
    REAL(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: qpf
    !> This field will be the fully evaluated differential operator
    !  Size should be (ng,nel,dfqty,nqty)
    REAL(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: qpdf
  END TYPE qp_real

!-------------------------------------------------------------------------------
!> Quadrature point storage for complex quatities
!-------------------------------------------------------------------------------
  TYPE :: qp_comp
    !> field values at each quadrature point
    !  size should be (ng,nel,nmode,fqty,nqty)
    COMPLEX(r8), DIMENSION(:,:,:,:,:), ALLOCATABLE :: qpf
    !> This field will be the fully evaluated differential operator
    !  size should be (ng,nel,nmode,dfqty,nqty)
    COMPLEX(r8), DIMENSION(:,:,:,:,:), ALLOCATABLE :: qpdf
  END TYPE qp_comp

!-------------------------------------------------------------------------------
!> Basis function storage at quadrature point locations. May also be used for
!  basis function evaluation at a batch of points in the reference element.
!-------------------------------------------------------------------------------
  TYPE :: qalpha_real
    !> alpha values at each quadrature point for each basis function
    !  size should be (ng,nbasis)
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: alf
    !> derivatives of alpha at each quadrature point for each basis function
    !  size should be (ng,nbasis,dfqty)
    REAL(r8), DIMENSION(:,:,:), ALLOCATABLE :: aldf
  END TYPE qalpha_real

!-------------------------------------------------------------------------------
!> Basis function storage at a single point in the reference element.
!-------------------------------------------------------------------------------
  TYPE :: alpha_real
    !> alpha values at a single point for each basis function
    !  size should be (nbasis)
    REAL(r8), DIMENSION(:), ALLOCATABLE :: alf
    !> derivatives of alpha at a single point for each basis function
    !  size should be (nbasis,dfqty)
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: aldf
  END TYPE alpha_real

!-------------------------------------------------------------------------------
!> Basis function storage at quadrature point locations over the full block.
!-------------------------------------------------------------------------------
  TYPE :: qalpha_block_real
    !> alpha values at each quadrature point for each basis function
    !  size should be (ng,nbasis,nel,fqty)
    REAL(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: alf
    !> derivatives of alpha at each quadrature point for each basis function
    !  size should be (ng,nbasis,nel,dfqty)
    REAL(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: aldf
  END TYPE qalpha_block_real

!-------------------------------------------------------------------------------
!> Jacobian storage at quadrature point locations.
!-------------------------------------------------------------------------------
  TYPE :: qjac_real
    !> forward Jacobian matrix at quadrature points
    !  jac(i,k,l,iel) == dr_k/dx_l at i-th quad point in an element
    REAL(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: jac
    !> inverse Jacobian matrix at quadrature points
    !  ijac(i,k,l,iel) == dx_k/dr_l at i-th quad point in an element
    REAL(r8), DIMENSION(:,:,:,:), ALLOCATABLE :: ijac
    !> radial position in toroidal geometry at quadrature points
    !  size should be (ng,nel)
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: bigr
    !> determinant of Jacobian matrix at quadrature points
    !  size should be (ng,nel)
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: detj
    !> determinant of Jacobian matrix at quadrature points
    !  multiplied by the integration weight
    !  size should be (ng,nel)
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: wdetj
  END TYPE qjac_real

!-------------------------------------------------------------------------------
!> Interpolation results for `nodal_real` from a batch of points
!-------------------------------------------------------------------------------
  TYPE :: batch_interp_real
    !> field evaluated at an arbitrary collection of points
    !  index ordering is (npoints,nqty*fqty)
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: f
    !> field derivative evaluation at an arbitrary collection of points
    !  index ordering is (npoints,nqty*dfqty)
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: df
    !> field second derivative evaluation at an arbitrary collection of points
    REAL(r8), DIMENSION(:,:), ALLOCATABLE :: ddf
    !> flag indicating the current set evaluation derivative order
    INTEGER(i4) :: dorder=-1
  END TYPE

!-------------------------------------------------------------------------------
!> Interpolation result for `nodal_real` for a single point
!-------------------------------------------------------------------------------
  TYPE :: interp_real
    !> field evaluated at an arbitrary point
    !  size should be fqty*nqty
    REAL(r8), DIMENSION(:), ALLOCATABLE :: f
    !> field derivative evaluation at an arbitrary point
    !  size should be dfqty*nqty
    REAL(r8), DIMENSION(:), ALLOCATABLE :: df
    !> field second derivative evaluation at an arbitrary point
    REAL(r8), DIMENSION(:), ALLOCATABLE :: ddf
    !> flag indicating the current set evaluation derivative order
    INTEGER(i4) :: dorder=-1
  END TYPE

!-------------------------------------------------------------------------------
!> Interpolation results for `nodal_comp` from a batch of points
!-------------------------------------------------------------------------------
  TYPE :: batch_interp_comp
    !> field evaluated at an arbitrary collection of points
    !  index ordering is (npoints,nqty*fqty,nmodes)
    COMPLEX(r8), DIMENSION(:,:,:), ALLOCATABLE :: f
    !> field derivative evaluation at an arbitrary collection of points
    !  index ordering is (npoints,nqty*dfqty,nmodes)
    COMPLEX(r8), DIMENSION(:,:,:), ALLOCATABLE :: df
    !> field second derivative evaluation at an arbitrary collection of points
    COMPLEX(r8), DIMENSION(:,:,:), ALLOCATABLE :: ddf
    !> flag indicating the current set evaluation derivative order
    INTEGER(i4) :: dorder=-1
  END TYPE

!-------------------------------------------------------------------------------
!> Interpolation result for `nodal_comp` for a single point
!-------------------------------------------------------------------------------
  TYPE :: interp_comp
    !> field evaluated at an arbitrary point
    !  shape should be (nqty*fqty,nmodes)
    COMPLEX(r8), DIMENSION(:,:), ALLOCATABLE :: f
    !> field derivative evaluation at an arbitrary point
    !  shape should be (nqty*dfqty,nmodes)
    COMPLEX(r8), DIMENSION(:,:), ALLOCATABLE :: df
    !> field second derivative evaluation at an arbitrary point
    COMPLEX(r8), DIMENSION(:,:), ALLOCATABLE :: ddf
    !> flag indicating the current set evaluation derivative order
    INTEGER(i4) :: dorder=-1
  END TYPE

END MODULE quadrature_mod
