!-------------------------------------------------------------------------------
! Implementation of vec_rect_2D_real_acc included in vec_rect_2D_acc.f
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* allocate a real vector
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_real(rvec,poly_degree,mx,my,nqty,id,on_gpu)
    IMPLICIT NONE

    !> vector to allocate
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> polynomial degree
    INTEGER(i4), INTENT(IN) :: poly_degree
    !> number of elements in the horizontal direction
    INTEGER(i4), INTENT(IN) :: mx
    !> number of elements in the vertical direction
    INTEGER(i4), INTENT(IN) :: my
    !> number of quantities
    INTEGER(i4), INTENT(IN) :: nqty
    !> ID for parallel streams
    INTEGER(i4), INTENT(IN) :: id
    !> true if data on GPU
    LOGICAL, INTENT(IN) :: on_gpu

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   store grid and vector dimensions
!-------------------------------------------------------------------------------
    rvec%nqty=nqty
    rvec%mx=mx
    rvec%my=my
    rvec%n_side=poly_degree-1
    rvec%n_int=(poly_degree-1)**2
    rvec%u_ndof=(poly_degree+1)**2
    rvec%pd=poly_degree
    rvec%nel=mx*my
    rvec%ndim=2
    rvec%id=id
    rvec%on_gpu=on_gpu
    rvec%inf_norm_size=4
    rvec%l2_dot_size=5
    !$acc enter data copyin(rvec) async(rvec%id) if(rvec%on_gpu)
!-------------------------------------------------------------------------------
!   allocate space according to the basis functions needed.
!   pad all basis type arrays, meaningful element bounds are
!   arr(0:mx,0:my) arrh(1:mx,0:my) arrv(0:mx,1:my) and arri(1:mx,1:my)
!-------------------------------------------------------------------------------
    SELECT CASE(poly_degree)
    CASE(1)  !  linear elements
      ALLOCATE(rvec%arr(nqty,-1:mx+1,-1:my+1))
      !$acc enter data create(rvec%arr) async(rvec%id) if(rvec%on_gpu)
      NULLIFY(rvec%arri,rvec%arrh,rvec%arrv)
    CASE(2:) !  higher-order elements
      ALLOCATE(rvec%arr(nqty,-1:mx+1,-1:my+1))
      !$acc enter data create(rvec%arr) async(rvec%id) if(rvec%on_gpu)
      ALLOCATE(rvec%arrh(nqty,poly_degree-1,-1:mx+1,-1:my+1))
      !$acc enter data create(rvec%arrh) async(rvec%id) if(rvec%on_gpu)
      ALLOCATE(rvec%arrv(nqty,poly_degree-1,-1:mx+1,-1:my+1))
      !$acc enter data create(rvec%arrv) async(rvec%id) if(rvec%on_gpu)
      ALLOCATE(rvec%arri(nqty,(poly_degree-1)**2,-1:mx+1,-1:my+1))
      !$acc enter data create(rvec%arri) async(rvec%id) if(rvec%on_gpu)
    END SELECT
!-------------------------------------------------------------------------------
!   register this object.
!-------------------------------------------------------------------------------
    NULLIFY(rvec%mem_id)
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      INTEGER(i4) :: sz
      sz= INT(SIZEOF(rvec%arr)+SIZEOF(rvec%arrh)+SIZEOF(rvec%arrv)              &
             +SIZEOF(rvec%arri)+SIZEOF(rvec),i4)
      CALL memlogger%update(rvec%mem_id,'rvec'//mod_name,'unknown',sz)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_real

!-------------------------------------------------------------------------------
!* Deallocate the vector
!-------------------------------------------------------------------------------
  SUBROUTINE dealloc_real(rvec)
    IMPLICIT NONE

    !> vector to deallocate
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dealloc_real',iftn,idepth)
    IF (ASSOCIATED(rvec%arr)) THEN
      !$acc exit data delete(rvec%arr) async(rvec%id) finalize if(rvec%on_gpu)
      DEALLOCATE(rvec%arr)
      NULLIFY(rvec%arr)
    ENDIF
    IF (ASSOCIATED(rvec%arrh)) THEN
      !$acc exit data delete(rvec%arrh) async(rvec%id) finalize if(rvec%on_gpu)
      DEALLOCATE(rvec%arrh)
      NULLIFY(rvec%arrh)
    ENDIF
    IF (ASSOCIATED(rvec%arrv)) THEN
      !$acc exit data delete(rvec%arrv) async(rvec%id) finalize if(rvec%on_gpu)
      DEALLOCATE(rvec%arrv)
      NULLIFY(rvec%arrv)
    ENDIF
    IF (ASSOCIATED(rvec%arri)) THEN
      !$acc exit data delete(rvec%arri) async(rvec%id) finalize if(rvec%on_gpu)
      DEALLOCATE(rvec%arri)
      NULLIFY(rvec%arri)
    ENDIF
    !$acc exit data delete(rvec) finalize async(rvec%id) if(rvec%on_gpu)
!-------------------------------------------------------------------------------
!   unregister this object.
!-------------------------------------------------------------------------------
#ifdef OBJ_MEM_PROF
    memprof: BLOCK
      USE memlog, ONLY: memlogger
      CALL memlogger%update(rvec%mem_id,'rvec'//mod_name,' ',0,resize=.TRUE.)
    END BLOCK memprof
#endif

    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dealloc_real

!-------------------------------------------------------------------------------
!* Create a new vector of the same type
!-------------------------------------------------------------------------------
  SUBROUTINE alloc_with_mold_real(rvec,new_rvec,nqty,pd)
    IMPLICIT NONE

    !> reference vector to mold
    CLASS(vec_rect_2D_real_acc), INTENT(IN) :: rvec
    !> new vector to be created
    CLASS(rvector), ALLOCATABLE, INTENT(OUT) :: new_rvec
    !> number of quantities, rvec%nqty is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> polynomial degree, rvec%pd is used if unspecified
    INTEGER(i4), OPTIONAL, INTENT(IN) :: pd

    INTEGER(i4) :: new_nqty,new_pd
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'alloc_with_mold_real',iftn,idepth)
    new_nqty=rvec%nqty
    IF (PRESENT(nqty)) THEN
      new_nqty=nqty
    ENDIF
    new_pd=rvec%pd
    IF (PRESENT(pd)) THEN
      new_pd=pd
    ENDIF
!-------------------------------------------------------------------------------
!   do the base allocation
!-------------------------------------------------------------------------------
    ALLOCATE(vec_rect_2D_real_acc::new_rvec)
!-------------------------------------------------------------------------------
!   copy the eliminated flag
!-------------------------------------------------------------------------------
    new_rvec%skip_elim_interior=rvec%skip_elim_interior
!-------------------------------------------------------------------------------
!   call the allocation routine
!-------------------------------------------------------------------------------
    SELECT TYPE (new_rvec)
    TYPE IS (vec_rect_2D_real_acc)
      CALL new_rvec%alloc(new_pd,rvec%mx,rvec%my,new_nqty,rvec%id,rvec%on_gpu)
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE alloc_with_mold_real

!-------------------------------------------------------------------------------
!* Assign zero to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE zero_real(rvec)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'zero_real',iftn,idepth)
    IF (rvec%pd>1) THEN
      ASSOCIATE (arr=>rvec%arr,arrh=>rvec%arrh,arrv=>rvec%arrv,arri=>rvec%arri)
        IF (rvec%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv) async(rvec%id) if(rvec%on_gpu)
          arr=0.0_r8
          arrh=0.0_r8
          arrv=0.0_r8
          !$acc end kernels
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri) async(rvec%id) if(rvec%on_gpu)
          arr=0.0_r8
          arrh=0.0_r8
          arrv=0.0_r8
          arri=0.0_r8
          !$acc end kernels
        ENDIF
      END ASSOCIATE
    ELSE
      ASSOCIATE (arr=>rvec%arr)
        !$acc kernels present(arr) async(rvec%id) if(rvec%on_gpu)
        arr=0.0_r8
        !$acc end kernels
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE zero_real

!-------------------------------------------------------------------------------
!* Assign a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_rvec_real(rvec,rvec2)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> vector to assign
    CLASS(rvector), INTENT(IN) :: rvec2

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_rvec_real',iftn,idepth)
    rvec%skip_elim_interior=rvec2%skip_elim_interior
    SELECT TYPE (rvec2)
    TYPE IS (vec_rect_2D_real_acc)
      ASSOCIATE(arr=>rvec%arr,arr2=>rvec2%arr,                                  &
                arrh=>rvec%arrh,arrh2=>rvec2%arrh,                              &
                arrv=>rvec%arrv,arrv2=>rvec2%arrv,                              &
                arri=>rvec%arri,arri2=>rvec2%arri)
        IF (rvec%pd>1) THEN
          IF (rvec%skip_elim_interior) THEN
            !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)               &
            !$acc async(rvec%id) if(rvec%on_gpu)
            arr=arr2
            arrh=arrh2
            arrv=arrv2
            !$acc end kernels
          ELSE
            !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)    &
            !$acc async(rvec%id) if(rvec%on_gpu)
            arr=arr2
            arrh=arrh2
            arrv=arrv2
            arri=arri2
            !$acc end kernels
          ENDIF
        ELSE
          !$acc kernels present(arr,arr2) async(rvec%id) if(rvec%on_gpu)
          arr=arr2
          !$acc end kernels
        ENDIF
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for vec2 '//                 &
                        'in assign_rvec_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_rvec_real

!-------------------------------------------------------------------------------
!* Partially assign a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_rvec_real(rvec,rvec2,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> vector to assign
    CLASS(rvector), INTENT(IN) :: rvec2
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assignp_rvec_real',iftn,idepth)
    rvec%skip_elim_interior=rvec2%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (rvec2)
    TYPE IS (vec_rect_2D_real_acc)
      ASSOCIATE(arr=>rvec%arr,arr2=>rvec2%arr,                                  &
                arrh=>rvec%arrh,arrh2=>rvec2%arrh,                              &
                arrv=>rvec%arrv,arrv2=>rvec2%arrv,                              &
                arri=>rvec%arri,arri2=>rvec2%arri)
        IF (rvec%pd>1) THEN
          IF (rvec%skip_elim_interior) THEN
            !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)               &
            !$acc copyin(v1s,v1e,v2s,v2e) async(rvec%id) if(rvec%on_gpu)
            arr(v1s:v1e,:,:)=arr2(v2s:v2e,:,:)
            arrh(v1s:v1e,:,:,:)=arrh2(v2s:v2e,:,:,:)
            arrv(v1s:v1e,:,:,:)=arrv2(v2s:v2e,:,:,:)
            !$acc end kernels
          ELSE
            !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)    &
            !$acc copyin(v1s,v1e,v2s,v2e) async(rvec%id) if(rvec%on_gpu)
            arr(v1s:v1e,:,:)=arr2(v2s:v2e,:,:)
            arrh(v1s:v1e,:,:,:)=arrh2(v2s:v2e,:,:,:)
            arrv(v1s:v1e,:,:,:)=arrv2(v2s:v2e,:,:,:)
            arri(v1s:v1e,:,:,:)=arri2(v2s:v2e,:,:,:)
            !$acc end kernels
          ENDIF
        ELSE
          !$acc kernels present(arr,arr2) copyin(v1s,v1e,v2s,v2e)               &
          !$acc async(rvec%id) if(rvec%on_gpu)
          arr(v1s:v1e,:,:)=arr2(v2s:v2e,:,:)
          !$acc end kernels
        ENDIF
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for vec2'//                  &
                        ' in assignp_rvec_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_rvec_real

!-------------------------------------------------------------------------------
!* Assign a cvector1m to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_cv1m_real(rvec,cv1m,r_i)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> vector to assign
    CLASS(cvector1m), INTENT(IN) :: cv1m
    !> 'real' for the real part of cvec, 'imag' for the imaginary part
    CHARACTER(*), INTENT(IN) :: r_i

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_cv1m_real',iftn,idepth)
    rvec%skip_elim_interior=cv1m%skip_elim_interior
    SELECT TYPE (cv1m)
    TYPE IS (vec_rect_2D_cv1m_acc)
      ASSOCIATE(arr=>rvec%arr,arr2=>cv1m%arr,                                   &
                arrh=>rvec%arrh,arrh2=>cv1m%arrh,                               &
                arrv=>rvec%arrv,arrv2=>cv1m%arrv,                               &
                arri=>rvec%arri,arri2=>cv1m%arri)
        SELECT CASE(r_i)
        CASE ('real','REAL')
          IF (rvec%pd>1) THEN
            IF (rvec%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc async(rvec%id) if(rvec%on_gpu)
              arr(:,:,:)=REAL(arr2(:,:,:))
              arrh(:,:,:,:)=REAL(arrh2(:,:,:,:))
              arrv(:,:,:,:)=REAL(arrv2(:,:,:,:))
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc async(rvec%id) if(rvec%on_gpu)
              arr(:,:,:)=REAL(arr2(:,:,:))
              arrh(:,:,:,:)=REAL(arrh2(:,:,:,:))
              arrv(:,:,:,:)=REAL(arrv2(:,:,:,:))
              arri(:,:,:,:)=REAL(arri2(:,:,:,:))
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) async(rvec%id) if(rvec%on_gpu)
            arr(:,:,:)=REAL(arr2(:,:,:))
            !$acc end kernels
          ENDIF
        CASE ('imag','IMAG')
          IF (rvec%pd>1) THEN
            IF (rvec%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc async(rvec%id) if(rvec%on_gpu)
              arr(:,:,:)=AIMAG(arr2(:,:,:))
              arrh(:,:,:,:)=AIMAG(arrh2(:,:,:,:))
              arrv(:,:,:,:)=AIMAG(arrv2(:,:,:,:))
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc async(rvec%id) if(rvec%on_gpu)
              arr(:,:,:)=AIMAG(arr2(:,:,:))
              arrh(:,:,:,:)=AIMAG(arrh2(:,:,:,:))
              arrv(:,:,:,:)=AIMAG(arrv2(:,:,:,:))
              arri(:,:,:,:)=AIMAG(arri2(:,:,:,:))
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) async(rvec%id) if(rvec%on_gpu)
            arr(:,:,:)=AIMAG(arr2(:,:,:))
            !$acc end kernels
          ENDIF
        CASE DEFAULT
          CALL par%nim_stop('assign_cv1m_real '//r_i//' flag not recognized.')
        END SELECT
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for cv1m in assign_cv1m_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_cv1m_real

!-------------------------------------------------------------------------------
!* Partially assign a cvector1m to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_cv1m_real(rvec,cv1m,r_i,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> vector to assign
    CLASS(cvector1m), INTENT(IN) :: cv1m
    !> 'real' for the real part of cvec, 'imag' for the imaginary part
    CHARACTER(*), INTENT(IN) :: r_i
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assignp_cv1m_real',iftn,idepth)
    rvec%skip_elim_interior=cv1m%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (cv1m)
    TYPE IS (vec_rect_2D_cv1m_acc)
      ASSOCIATE(arr=>rvec%arr,arr2=>cv1m%arr,                                   &
                arrh=>rvec%arrh,arrh2=>cv1m%arrh,                               &
                arrv=>rvec%arrv,arrv2=>cv1m%arrv,                               &
                arri=>rvec%arri,arri2=>cv1m%arri)
        SELECT CASE(r_i)
        CASE ('real','REAL')
          IF (rvec%pd>1) THEN
            IF (rvec%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc copyin(v1s,v1e,v2s,v2e) async(rvec%id) if(rvec%on_gpu)
              arr(v1s:v1e,:,:)=REAL(arr2(v2s:v2e,:,:))
              arrh(v1s:v1e,:,:,:)=REAL(arrh2(v2s:v2e,:,:,:))
              arrv(v1s:v1e,:,:,:)=REAL(arrv2(v2s:v2e,:,:,:))
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc copyin(v1s,v1e,v2s,v2e) async(rvec%id) if(rvec%on_gpu)
              arr(v1s:v1e,:,:)=REAL(arr2(v2s:v2e,:,:))
              arrh(v1s:v1e,:,:,:)=REAL(arrh2(v2s:v2e,:,:,:))
              arrv(v1s:v1e,:,:,:)=REAL(arrv2(v2s:v2e,:,:,:))
              arri(v1s:v1e,:,:,:)=REAL(arri2(v2s:v2e,:,:,:))
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) copyin(v1s,v1e,v2s,v2e)             &
            !$acc async(rvec%id) if(rvec%on_gpu)
            arr(v1s:v1e,:,:)=REAL(arr2(v2s:v2e,:,:))
            !$acc end kernels
          ENDIF
        CASE ('imag','IMAG')
          IF (rvec%pd>1) THEN
            IF (rvec%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc copyin(v1s,v1e,v2s,v2e) async(rvec%id) if(rvec%on_gpu)
              arr(v1s:v1e,:,:)=AIMAG(arr2(v2s:v2e,:,:))
              arrh(v1s:v1e,:,:,:)=AIMAG(arrh2(v2s:v2e,:,:,:))
              arrv(v1s:v1e,:,:,:)=AIMAG(arrv2(v2s:v2e,:,:,:))
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc copyin(v1s,v1e,v2s,v2e) async(rvec%id) if(rvec%on_gpu)
              arr(v1s:v1e,:,:)=AIMAG(arr2(v2s:v2e,:,:))
              arrh(v1s:v1e,:,:,:)=AIMAG(arrh2(v2s:v2e,:,:,:))
              arrv(v1s:v1e,:,:,:)=AIMAG(arrv2(v2s:v2e,:,:,:))
              arri(v1s:v1e,:,:,:)=AIMAG(arri2(v2s:v2e,:,:,:))
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) copyin(v1s,v1e,v2s,v2e)             &
            !$acc async(rvec%id) if(rvec%on_gpu)
            arr(v1s:v1e,:,:)=AIMAG(arr2(v2s:v2e,:,:))
            !$acc end kernels
          ENDIF
        CASE DEFAULT
          CALL par%nim_stop('assignp_cv1m_real '//r_i//' flag not recognized.')
        END SELECT
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_cv1m for cv1m in assignp_cv1m_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_cv1m_real

!-------------------------------------------------------------------------------
!* Assign a cvector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assign_cvec_real(rvec,cvec,imode,r_i)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> vector to assign
    CLASS(cvector), INTENT(IN) :: cvec
    !> mode to assign
    INTEGER(i4), INTENT(IN) :: imode
    !> 'real' for the real part of cvec, 'imag' for the imaginary part
    CHARACTER(*), INTENT(IN) :: r_i

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assign_cvec_real',iftn,idepth)
    rvec%skip_elim_interior=cvec%skip_elim_interior
    SELECT TYPE (cvec)
    TYPE IS (vec_rect_2D_comp_acc)
      ASSOCIATE(arr=>rvec%arr,arr2=>cvec%arr,                                   &
                arrh=>rvec%arrh,arrh2=>cvec%arrh,                               &
                arrv=>rvec%arrv,arrv2=>cvec%arrv,                               &
                arri=>rvec%arri,arri2=>cvec%arri)
        SELECT CASE(r_i)
        CASE ('real','REAL')
          IF (rvec%pd>1) THEN
            IF (rvec%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc copyin(imode) async(rvec%id) if(rvec%on_gpu)
              arr(:,:,:)=REAL(arr2(:,:,:,imode))
              arrh(:,:,:,:)=REAL(arrh2(:,:,:,:,imode))
              arrv(:,:,:,:)=REAL(arrv2(:,:,:,:,imode))
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc copyin(imode) async(rvec%id) if(rvec%on_gpu)
              arr(:,:,:)=REAL(arr2(:,:,:,imode))
              arrh(:,:,:,:)=REAL(arrh2(:,:,:,:,imode))
              arrv(:,:,:,:)=REAL(arrv2(:,:,:,:,imode))
              arri(:,:,:,:)=REAL(arri2(:,:,:,:,imode))
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) copyin(imode)                       &
            !$acc async(rvec%id) if(rvec%on_gpu)
            arr(:,:,:)=REAL(arr2(:,:,:,imode))
            !$acc end kernels
          ENDIF
        CASE ('imag','IMAG')
          IF (rvec%pd>1) THEN
            IF (rvec%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc copyin(imode) async(rvec%id) if(rvec%on_gpu)
              arr(:,:,:)=AIMAG(arr2(:,:,:,imode))
              arrh(:,:,:,:)=AIMAG(arrh2(:,:,:,:,imode))
              arrv(:,:,:,:)=AIMAG(arrv2(:,:,:,:,imode))
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc copyin(imode) async(rvec%id) if(rvec%on_gpu)
              arr(:,:,:)=AIMAG(arr2(:,:,:,imode))
              arrh(:,:,:,:)=AIMAG(arrh2(:,:,:,:,imode))
              arrv(:,:,:,:)=AIMAG(arrv2(:,:,:,:,imode))
              arri(:,:,:,:)=AIMAG(arri2(:,:,:,:,imode))
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) copyin(imode)                       &
            !$acc async(rvec%id) if(rvec%on_gpu)
            arr(:,:,:)=AIMAG(arr2(:,:,:,imode))
            !$acc end kernels
          ENDIF
        CASE DEFAULT
          CALL par%nim_stop('assign_cvec_real '//r_i//' flag not recognized.')
        END SELECT
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for cvec in assign_cvec_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_cvec_real

!-------------------------------------------------------------------------------
!* Partially assign a cvector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE assignp_cvec_real(rvec,cvec,imode,r_i,v1st,v2st,nq)
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> vector to assign
    CLASS(cvector), INTENT(IN) :: cvec
    !> mode to assign
    INTEGER(i4), INTENT(IN) :: imode
    !> 'real' for the real part of cvec, 'imag' for the imaginary part
    CHARACTER(*), INTENT(IN) :: r_i
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assignp_cvec_real',iftn,idepth)
    rvec%skip_elim_interior=cvec%skip_elim_interior
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    SELECT TYPE (cvec)
    TYPE IS (vec_rect_2D_comp_acc)
      ASSOCIATE(arr=>rvec%arr,arr2=>cvec%arr,                                   &
                arrh=>rvec%arrh,arrh2=>cvec%arrh,                               &
                arrv=>rvec%arrv,arrv2=>cvec%arrv,                               &
                arri=>rvec%arri,arri2=>cvec%arri)
        SELECT CASE(r_i)
        CASE ('real','REAL')
          IF (rvec%pd>1) THEN
            IF (rvec%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc copyin(v1s,v1e,v2s,v2e,imode) async(rvec%id) if(rvec%on_gpu)
              arr(v1s:v1e,:,:)=REAL(arr2(v2s:v2e,:,:,imode))
              arrh(v1s:v1e,:,:,:)=REAL(arrh2(v2s:v2e,:,:,:,imode))
              arrv(v1s:v1e,:,:,:)=REAL(arrv2(v2s:v2e,:,:,:,imode))
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc copyin(v1s,v1e,v2s,v2e,imode) async(rvec%id) if(rvec%on_gpu)
              arr(v1s:v1e,:,:)=REAL(arr2(v2s:v2e,:,:,imode))
              arrh(v1s:v1e,:,:,:)=REAL(arrh2(v2s:v2e,:,:,:,imode))
              arrv(v1s:v1e,:,:,:)=REAL(arrv2(v2s:v2e,:,:,:,imode))
              arri(v1s:v1e,:,:,:)=REAL(arri2(v2s:v2e,:,:,:,imode))
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) copyin(v1s,v1e,v2s,v2e,imode)       &
            !$acc async(rvec%id) if(rvec%on_gpu)
            arr(v1s:v1e,:,:)=REAL(arr2(v2s:v2e,:,:,imode))
            !$acc end kernels
          ENDIF
        CASE ('imag','IMAG')
          IF (rvec%pd>1) THEN
            IF (rvec%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc copyin(v1s,v1e,v2s,v2e,imode) async(rvec%id) if(rvec%on_gpu)
              arr(v1s:v1e,:,:)=AIMAG(arr2(v2s:v2e,:,:,imode))
              arrh(v1s:v1e,:,:,:)=AIMAG(arrh2(v2s:v2e,:,:,:,imode))
              arrv(v1s:v1e,:,:,:)=AIMAG(arrv2(v2s:v2e,:,:,:,imode))
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc copyin(v1s,v1e,v2s,v2e,imode) async(rvec%id) if(rvec%on_gpu)
              arr(v1s:v1e,:,:)=AIMAG(arr2(v2s:v2e,:,:,imode))
              arrh(v1s:v1e,:,:,:)=AIMAG(arrh2(v2s:v2e,:,:,:,imode))
              arrv(v1s:v1e,:,:,:)=AIMAG(arrv2(v2s:v2e,:,:,:,imode))
              arri(v1s:v1e,:,:,:)=AIMAG(arri2(v2s:v2e,:,:,:,imode))
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr,arr2) copyin(v1s,v1e,v2s,v2e,imode)       &
            !$acc async(rvec%id) if(rvec%on_gpu)
            arr(v1s:v1e,:,:)=AIMAG(arr2(v2s:v2e,:,:,imode))
            !$acc end kernels
          ENDIF
        CASE DEFAULT
          CALL par%nim_stop('assignp_cvec_real '//r_i//' flag not recognized.')
        END SELECT
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_comp for cvec'//                  &
                        ' in assignp_cvec_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assignp_cvec_real

!-------------------------------------------------------------------------------
!* add a vector to a vector
!-------------------------------------------------------------------------------
  SUBROUTINE add_vec_real(rvec,rvec2,v1st,v2st,nq,v1fac,v2fac)
    USE convert_type
    USE local
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> vector to assign
    CLASS(rvector), INTENT(IN) :: rvec2
    !> quantity start for the 1st vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v1st
    !> quantity start for the 2nd vector (default 1)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: v2st
    !> number of quantities to assign (default 1 when v1st or v2st is set
    !> default is all when neither v1st or v2st is set)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nq
    !> factor to multiply vec by (default 1)
    CLASS(*), OPTIONAL, INTENT(IN) :: v1fac
    !> factor to multiply vec2 by (default 1)
    CLASS(*), OPTIONAL, INTENT(IN) :: v2fac

    INTEGER(i4) :: v1s,v1e,v2s,v2e,nqty
    REAL(r8) :: v1f,v2f
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'add_vec_real',iftn,idepth)
    IF (.NOT.PRESENT(v1st)) THEN
      v1s=1_i4
    ELSE
      v1s=v1st
    ENDIF
    IF (.NOT.PRESENT(v2st)) THEN
      v2s=1_i4
    ELSE
      v2s=v2st
    ENDIF
    IF (.NOT.PRESENT(nq)) THEN
      nqty=1_i4
    ELSE
      nqty=nq
    ENDIF
    v1e=v1s+nqty-1_i4
    v2e=v2s+nqty-1_i4
    IF (PRESENT(v1fac)) THEN
      v1f=convert_to_real_r8(v1fac)
    ELSE
      v1f=CMPLX(1._r8,0._r8,r8)
    ENDIF
    IF (PRESENT(v2fac)) THEN
      v2f=convert_to_real_r8(v2fac)
    ELSE
      v2f=CMPLX(1._r8,0._r8,r8)
    ENDIF
    SELECT TYPE (rvec2)
    TYPE IS (vec_rect_2D_real_acc)
      IF (.NOT.PRESENT(v1st).AND..NOT.PRESENT(v2st).AND..NOT.PRESENT(nq)) THEN
        IF (rvec%pd>1) THEN
          ASSOCIATE (arr=>rvec%arr,arr2=>rvec2%arr,                             &
                     arrh=>rvec%arrh,arrh2=>rvec2%arrh,                         &
                     arrv=>rvec%arrv,arrv2=>rvec2%arrv,                         &
                     arri=>rvec%arri,arri2=>rvec2%arri)
            IF (rvec%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc copyin(v1f,v2f) async(rvec%id) if(rvec%on_gpu)
              arr=v1f*arr+v2f*arr2
              arrh=v1f*arrh+v2f*arrh2
              arrv=v1f*arrv+v2f*arrv2
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc copyin(v1f,v2f) async(rvec%id) if(rvec%on_gpu)
              arr=v1f*arr+v2f*arr2
              arrh=v1f*arrh+v2f*arrh2
              arrv=v1f*arrv+v2f*arrv2
              arri=v1f*arri+v2f*arri2
              !$acc end kernels
            ENDIF
          END ASSOCIATE
        ELSE
          ASSOCIATE (arr=>rvec%arr,arr2=>rvec2%arr)
            !$acc kernels present(arr,arr2) copyin(v1f,v2f)                     &
            !$acc async(rvec%id) if(rvec%on_gpu)
            arr=v1f*arr+v2f*arr2
            !$acc end kernels
          END ASSOCIATE
        ENDIF
      ELSE
        IF (rvec%pd>1) THEN
          ASSOCIATE (arr=>rvec%arr,arr2=>rvec2%arr,                             &
                     arrh=>rvec%arrh,arrh2=>rvec2%arrh,                         &
                     arrv=>rvec%arrv,arrv2=>rvec2%arrv,                         &
                     arri=>rvec%arri,arri2=>rvec2%arri)
            IF (rvec%skip_elim_interior) THEN
              !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)             &
              !$acc copyin(v1f,v2f,v1s,v1e) async(rvec%id) if(rvec%on_gpu)
              arr(v1s:v1e,:,:)=v1f*arr(v1s:v1e,:,:)+v2f*arr2(v2s:v2e,:,:)
              arrh(v1s:v1e,:,:,:)=v1f*arrh(v1s:v1e,:,:,:)+v2f*arrh2(v2s:v2e,:,:,:)
              arrv(v1s:v1e,:,:,:)=v1f*arrv(v1s:v1e,:,:,:)+v2f*arrv2(v2s:v2e,:,:,:)
              !$acc end kernels
            ELSE
              !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)  &
              !$acc copyin(v1f,v2f,v1s,v1e) async(rvec%id) if(rvec%on_gpu)
              arr(v1s:v1e,:,:)=v1f*arr(v1s:v1e,:,:)+v2f*arr2(v2s:v2e,:,:)
              arrh(v1s:v1e,:,:,:)=v1f*arrh(v1s:v1e,:,:,:)+v2f*arrh2(v2s:v2e,:,:,:)
              arrv(v1s:v1e,:,:,:)=v1f*arrv(v1s:v1e,:,:,:)+v2f*arrv2(v2s:v2e,:,:,:)
              arri(v1s:v1e,:,:,:)=v1f*arri(v1s:v1e,:,:,:)+v2f*arri2(v2s:v2e,:,:,:)
              !$acc end kernels
            ENDIF
          END ASSOCIATE
        ELSE
          ASSOCIATE (arr=>rvec%arr,arr2=>rvec2%arr)
            !$acc kernels present(arr,arr2) copyin(v1f,v2f,v1s,v1e)             &
            !$acc async(rvec%id) if(rvec%on_gpu)
            arr(v1s:v1e,:,:)=v1f*arr(v1s:v1e,:,:)+v2f*arr2(v2s:v2e,:,:)
            !$acc end kernels
          END ASSOCIATE
        ENDIF
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for cvec in add_vec_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE add_vec_real

!-------------------------------------------------------------------------------
!* multiply a vector by a real scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_rsc_real(rvec,rscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> scalar to multiply
    REAL(r8), INTENT(IN) :: rscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_rsc_real',iftn,idepth)
    ASSOCIATE (arr=>rvec%arr,arrh=>rvec%arrh,                                   &
               arrv=>rvec%arrv,arri=>rvec%arri)
      IF (rvec%pd>1) THEN
        IF (rvec%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv) copyin(rscalar)                  &
          !$acc async(rvec%id) if(rvec%on_gpu)
          arr=arr*rscalar
          arrh=arrh*rscalar
          arrv=arrv*rscalar
          !$acc end kernels
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri) copyin(rscalar)             &
          !$acc async(rvec%id) if(rvec%on_gpu)
          arr=arr*rscalar
          arrh=arrh*rscalar
          arrv=arrv*rscalar
          arri=arri*rscalar
          !$acc end kernels
        ENDIF
      ELSE
        !$acc kernels present(arr) copyin(rscalar) async(rvec%id) if(rvec%on_gpu)
        arr=arr*rscalar
        !$acc end kernels
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_rsc_real

!-------------------------------------------------------------------------------
!* multiply a vector by a complex scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_csc_real(rvec,cscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> scalar to multiply
    COMPLEX(r8), INTENT(IN) :: cscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_csc_real',iftn,idepth)
    ASSOCIATE (arr=>rvec%arr,arrh=>rvec%arrh,                                   &
               arrv=>rvec%arrv,arri=>rvec%arri)
      IF (rvec%pd>1) THEN
        IF (rvec%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv) copyin(cscalar)                  &
          !$acc async(rvec%id) if(rvec%on_gpu)
          arr=arr*REAL(cscalar)
          arrh=arrh*REAL(cscalar)
          arrv=arrv*REAL(cscalar)
          !$acc end kernels
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri) copyin(cscalar)             &
          !$acc async(rvec%id) if(rvec%on_gpu)
          arr=arr*REAL(cscalar)
          arrh=arrh*REAL(cscalar)
          arrv=arrv*REAL(cscalar)
          arri=arri*REAL(cscalar)
          !$acc end kernels
        ENDIF
      ELSE
        !$acc kernels present(arr) copyin(cscalar) async(rvec%id) if(rvec%on_gpu)
        arr=arr*REAL(cscalar)
        !$acc end kernels
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_csc_real

!-------------------------------------------------------------------------------
!* multiply a vector by a integer scalar
!-------------------------------------------------------------------------------
  SUBROUTINE mult_int_real(rvec,iscalar)
    USE local
    IMPLICIT NONE

    !> vector to be multiplied
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> scalar to multiply
    INTEGER(i4), INTENT(IN) :: iscalar

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'mult_int_real',iftn,idepth)
    ASSOCIATE (arr=>rvec%arr,arrh=>rvec%arrh,                                   &
               arrv=>rvec%arrv,arri=>rvec%arri)
      IF (rvec%pd>1) THEN
        IF (rvec%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv) copyin(iscalar)                  &
          !$acc async(rvec%id) if(rvec%on_gpu)
          arr=arr*iscalar
          arrh=arrh*iscalar
          arrv=arrv*iscalar
          !$acc end kernels
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri) copyin(iscalar)             &
          !$acc async(rvec%id) if(rvec%on_gpu)
          arr=arr*iscalar
          arrh=arrh*iscalar
          arrv=arrv*iscalar
          arri=arri*iscalar
          !$acc end kernels
        ENDIF
      ELSE
        !$acc kernels present(arr) copyin(iscalar) async(rvec%id) if(rvec%on_gpu)
        arr=arr*iscalar
        !$acc end kernels
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE mult_int_real

!-------------------------------------------------------------------------------
!* compute the inf norm
!-------------------------------------------------------------------------------
  SUBROUTINE inf_norm_real(rvec,infnorm)
    USE local
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_real_acc), INTENT(IN) :: rvec
    !> inf norm contributions from the vector (size vec%inf_norm_size)
    REAL(r8), INTENT(INOUT) :: infnorm(:)

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'inf_norm_real',iftn,idepth)
    ASSOCIATE (arr=>rvec%arr,arrh=>rvec%arrh,                                   &
               arrv=>rvec%arrv,arri=>rvec%arri)
      IF (rvec%pd>1) THEN
        IF (rvec%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv) copyout(infnorm(1:3))            &
          !$acc async(rvec%id) if(rvec%on_gpu)
          infnorm(1)=MAXVAL(ABS(arr))
          infnorm(2)=MAXVAL(ABS(arrh))
          infnorm(3)=MAXVAL(ABS(arrv))
          !$acc end kernels
          infnorm(4)=0._r8
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri) copyout(infnorm(1:4))       &
          !$acc async(rvec%id) if(rvec%on_gpu)
          infnorm(1)=MAXVAL(ABS(arr))
          infnorm(2)=MAXVAL(ABS(arrh))
          infnorm(3)=MAXVAL(ABS(arrv))
          infnorm(4)=MAXVAL(ABS(arri))
          !$acc end kernels
        ENDIF
      ELSE
        !$acc kernels present(arr) copyout(infnorm(1))                          &
        !$acc async(rvec%id) if(rvec%on_gpu)
        infnorm(1)=MAXVAL(ABS(arr))
        !$acc end kernels
        infnorm(2:4)=0._r8
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE inf_norm_real

!-------------------------------------------------------------------------------
!* compute the L2 norm squared
!-------------------------------------------------------------------------------
  SUBROUTINE l2_norm2_real(rvec,l2norm,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_real_acc), INTENT(IN) :: rvec
    !> L2 norm contributions from the vector (size vec%l2_dot_size)
    REAL(r8), INTENT(INOUT) :: l2norm(:)
    !> edge data to weight edge value contribution
    TYPE(edge_type), INTENT(IN) :: edge

    INTEGER(i4) :: ix,iy,iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'l2_norm2_real',iftn,idepth)
    ASSOCIATE (arr=>rvec%arr,arrh=>rvec%arrh,                                   &
               arrv=>rvec%arrv,arri=>rvec%arri,                                 &
               vertex=>edge%vertex,segment=>edge%segment)
      IF (rvec%pd>1) THEN
        IF (rvec%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv) copyout(l2norm(1:3))             &
          !$acc async(rvec%id) if(rvec%on_gpu)
          l2norm(1)=SUM(arr*arr)
          l2norm(2)=SUM(arrh*arrh)
          l2norm(3)=SUM(arrv*arrv)
          !$acc end kernels
          l2norm(4)=0._r8
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri) copyout(l2norm(1:4))        &
          !$acc async(rvec%id) if(rvec%on_gpu)
          l2norm(1)=SUM(arr*arr)
          l2norm(2)=SUM(arrh*arrh)
          l2norm(3)=SUM(arrv*arrv)
          l2norm(4)=SUM(arri*arri)
          !$acc end kernels
        ENDIF
!-------------------------------------------------------------------------------
!       boundary points must be divided by the number of internal
!       representations to get the correct sum over all blocks.
!-------------------------------------------------------------------------------
        !$acc parallel present(arr,arrh,arrv,edge,vertex,segment)               &
        !$acc copy(l2norm(5)) reduction(+:l2norm(5)) async(rvec%id) if(rvec%on_gpu)
        !$acc loop gang vector private(ix,iy)
        DO iv=1,edge%nvert
          ix=vertex(iv)%intxy(1)
          iy=vertex(iv)%intxy(2)
          l2norm(5)=l2norm(5)+(vertex(iv)%ave_factor-1._r8)*                    &
                              SUM(arr(:,ix,iy)*arr(:,ix,iy))
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          IF (segment(iv)%h_side) THEN
            l2norm(5)=l2norm(5)+(segment(iv)%ave_factor-1._r8)*                 &
                           SUM(arrh(:,:,ix,iy)*arrh(:,:,ix,iy))
          ELSE
            l2norm(5)=l2norm(5)+(segment(iv)%ave_factor-1._r8)*                 &
                           SUM(arrv(:,:,ix,iy)*arrv(:,:,ix,iy))
          ENDIF
        ENDDO
        !$acc end parallel
      ELSE
        !$acc kernels present(arr) copyout(l2norm(1))                           &
        !$acc async(rvec%id) if(rvec%on_gpu)
        l2norm(1)=SUM(arr*arr)
        !$acc end kernels
        !$acc parallel present(arr,edge,vertex) copy(l2norm(2))                 &
        !$acc reduction(+:l2norm(2)) async(rvec%id) if(rvec%on_gpu)
        !$acc loop gang vector private(ix,iy)
        DO iv=1,edge%nvert
          ix=vertex(iv)%intxy(1)
          iy=vertex(iv)%intxy(2)
          l2norm(2)=l2norm(2)+(vertex(iv)%ave_factor-1._r8)*                    &
                              SUM(arr(:,ix,iy)*arr(:,ix,iy))
        ENDDO
        !$acc end parallel
        l2norm(3:5)=0._r8
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE l2_norm2_real

!-------------------------------------------------------------------------------
!*  compute the dot product of vec and vec2
!-------------------------------------------------------------------------------
  SUBROUTINE dot_real(rvec,rvec2,dot,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_real_acc), INTENT(IN) :: rvec
    !> vector to dot
    CLASS(rvector), INTENT(IN) :: rvec2
    !> dot contributions from the vector (size vec%l2_dot_size)
    REAL(r8), INTENT(INOUT) :: dot(:)
    !> edge data to weight edge value contribution
    TYPE(edge_type), INTENT(IN) :: edge

    INTEGER(i4) :: ix,iy,iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dot_real',iftn,idepth)
    SELECT TYPE (rvec2)
    TYPE IS (vec_rect_2D_real_acc)
      ASSOCIATE (arr=>rvec%arr,arrh=>rvec%arrh,                                 &
                 arrv=>rvec%arrv,arri=>rvec%arri,                               &
                 arr2=>rvec2%arr,arrh2=>rvec2%arrh,                             &
                 arrv2=>rvec2%arrv,arri2=>rvec2%arri,                           &
                 vertex=>edge%vertex,segment=>edge%segment)
        IF (rvec%pd>1) THEN
          IF (rvec%skip_elim_interior) THEN
            !$acc kernels present(arr,arrh,arrv,arr2,arrh2,arrv2)               &
            !$acc copyout(dot(1:3)) async(rvec%id) if(rvec%on_gpu)
            dot(1)=SUM(arr*arr2)
            dot(2)=SUM(arrh*arrh2)
            dot(3)=SUM(arrv*arrv2)
            !$acc end kernels
            dot(4)=0._r8
          ELSE
            !$acc kernels present(arr,arrh,arrv,arri,arr2,arrh2,arrv2,arri2)    &
            !$acc copyout(dot(1:4)) async(rvec%id) if(rvec%on_gpu)
            dot(1)=SUM(arr*arr2)
            dot(2)=SUM(arrh*arrh2)
            dot(3)=SUM(arrv*arrv2)
            dot(4)=SUM(arri*arri2)
            !$acc end kernels
          ENDIF
!-------------------------------------------------------------------------------
!         boundary points must be divided by the number of internal
!         representations to get the correct sum over all blocks.
!-------------------------------------------------------------------------------
          !$acc parallel present(arr,arrh,arrv,arr2,arrh2,arrv2)                &
          !$acc present(edge,vertex,segment) copy(dot(5))                       &
          !$acc reduction(+:dot(5)) async(rvec%id) if(rvec%on_gpu)
          !$acc loop gang vector private(ix,iy)
          DO iv=1,edge%nvert
            ix=vertex(iv)%intxy(1)
            iy=vertex(iv)%intxy(2)
            dot(5)=dot(5)+(vertex(iv)%ave_factor-1._r8)*                        &
                                SUM(arr(:,ix,iy)*arr2(:,ix,iy))
            ix=segment(iv)%intxys(1)
            iy=segment(iv)%intxys(2)
            IF (segment(iv)%h_side) THEN
              dot(5)=dot(5)+(segment(iv)%ave_factor-1._r8)*                     &
                             SUM(arrh(:,:,ix,iy)*arrh2(:,:,ix,iy))
            ELSE
              dot(5)=dot(5)+(segment(iv)%ave_factor-1._r8)*                     &
                             SUM(arrv(:,:,ix,iy)*arrv2(:,:,ix,iy))
            ENDIF
          ENDDO
          !$acc end parallel
        ELSE
          !$acc kernels present(arr) copyout(dot(1))                            &
          !$acc async(rvec%id) if(rvec%on_gpu)
          dot(1)=SUM(arr*arr2)
          !$acc end kernels
          !$acc parallel present(arr,edge,vertex) copy(dot(2))                  &
          !$acc reduction(+:dot(2)) async(rvec%id) if(rvec%on_gpu)
          !$acc loop gang vector private(ix,iy)
          DO iv=1,edge%nvert
            ix=vertex(iv)%intxy(1)
            iy=vertex(iv)%intxy(2)
            dot(2)=dot(2)+(vertex(iv)%ave_factor-1._r8)*                        &
                                SUM(arr(:,ix,iy)*arr2(:,ix,iy))
          ENDDO
          !$acc end parallel
          dot(3:5)=0._r8
        ENDIF
      END ASSOCIATE
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for vec2 in dot_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dot_real

!-------------------------------------------------------------------------------
!> transfer contributions from integrand into internal storage as a plus
!  equals operation (the user must call %zero as needed).
!-------------------------------------------------------------------------------
  SUBROUTINE assemble_real(rvec,integrand)
    USE local
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> integrand array
    REAL(r8), INTENT(IN) :: integrand(:,:,:)

    INTEGER(i4) :: iq,iel,iv,iy,ix,is,ii,start_horz,start_vert,start_int,id
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'assemble_real',iftn,idepth)
    start_horz=5
    start_vert=5+2*rvec%n_side
    start_int=5+4*rvec%n_side
!-------------------------------------------------------------------------------
!   assemble and accumulate the contributions from each element into the
!   correct arrays. factors of Jacobian and quadrature weight are already in
!   the test function arrays included in the integrand.
!-------------------------------------------------------------------------------
    IF (ASSOCIATED(rvec%arr)) THEN
      ASSOCIATE (arr=>rvec%arr)
        id=par%get_stream(rvec%id,1,4)
        !$acc parallel present(arr,rvec,integrand)                              &
        !$acc wait(rvec%id) async(id) if(rvec%on_gpu)
        !$acc loop gang
        DO iy=0,rvec%my-1
          !$acc loop vector collapse(2) private(iel)
          DO ix=0,rvec%mx-1
            DO iq=1,rvec%nqty
              iel=1+ix+iy*rvec%mx
              arr(iq,ix,iy)=arr(iq,ix,iy)+integrand(iq,iel,1)
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
        !$acc parallel present(arr,rvec,integrand)                              &
        !$acc wait(rvec%id) async(id) if(rvec%on_gpu)
        !$acc loop gang
        DO iy=0,rvec%my-1
          !$acc loop vector collapse(2) private(iel)
          DO ix=0,rvec%mx-1
            DO iq=1,rvec%nqty
              iel=1+ix+iy*rvec%mx
              arr(iq,ix+1,iy)=arr(iq,ix+1,iy)+integrand(iq,iel,2)
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
        !$acc parallel present(arr,rvec,integrand)                              &
        !$acc wait(rvec%id) async(id) if(rvec%on_gpu)
        !$acc loop gang
        DO iy=0,rvec%my-1
          !$acc loop vector collapse(2) private(iel)
          DO ix=0,rvec%mx-1
            DO iq=1,rvec%nqty
              iel=1+ix+iy*rvec%mx
              arr(iq,ix,iy+1)=arr(iq,ix,iy+1)+integrand(iq,iel,3)
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
        !$acc parallel present(arr,rvec,integrand)                              &
        !$acc wait(rvec%id) async(id) if(rvec%on_gpu)
        !$acc loop gang
        DO iy=0,rvec%my-1
          !$acc loop vector collapse(2) private(iel)
          DO ix=0,rvec%mx-1
            DO iq=1,rvec%nqty
              iel=1+ix+iy*rvec%mx
              arr(iq,ix+1,iy+1)=arr(iq,ix+1,iy+1)+integrand(iq,iel,4)
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(rvec%arrh)) THEN
      ASSOCIATE (arrh=>rvec%arrh)
        id=par%get_stream(rvec%id,2,4)
        !$acc parallel present(arrh,rvec,integrand)                             &
        !$acc copyin(start_horz) wait(rvec%id) async(id) if(rvec%on_gpu)
        !$acc loop gang
        DO iy=0,rvec%my-1
          !$acc loop vector collapse(3) private(iel,iv)
          DO ix=1,rvec%mx
            DO is=1,rvec%n_side
              DO iq=1,rvec%nqty
                iel=ix+iy*rvec%mx
                iv=start_horz+2*(is-1)
                arrh(iq,is,ix,iy)=arrh(iq,is,ix,iy)+integrand(iq,iel,iv)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
        !$acc parallel present(arrh,rvec,integrand)                             &
        !$acc copyin(start_horz) wait(rvec%id) async(id) if(rvec%on_gpu)
        !$acc loop gang
        DO iy=1,rvec%my
          !$acc loop vector collapse(3) private(iel,iv)
          DO ix=1,rvec%mx
            DO is=1,rvec%n_side
              DO iq=1,rvec%nqty
                iel=ix+(iy-1)*rvec%mx
                iv=start_horz+2*(is-1)+1
                arrh(iq,is,ix,iy)=arrh(iq,is,ix,iy)+integrand(iq,iel,iv)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(rvec%arrv)) THEN
      ASSOCIATE (arrv=>rvec%arrv)
        id=par%get_stream(rvec%id,3,4)
        !$acc parallel present(arrv,rvec,integrand)                             &
        !$acc copyin(start_vert) wait(rvec%id) async(id) if(rvec%on_gpu)
        !$acc loop gang
        DO iy=1,rvec%my
          !$acc loop vector collapse(3) private(iel,iv)
          DO ix=0,rvec%mx-1
            DO is=1,rvec%n_side
              DO iq=1,rvec%nqty
                iel=1+ix+(iy-1)*rvec%mx
                iv=start_vert+2*(is-1)
                arrv(iq,is,ix,iy)=arrv(iq,is,ix,iy)+integrand(iq,iel,iv)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
        !$acc parallel present(arrv,rvec,integrand)                             &
        !$acc copyin(start_vert) wait(rvec%id) async(id) if(rvec%on_gpu)
        !$acc loop gang
        DO iy=1,rvec%my
          !$acc loop vector collapse(3) private(iel,iv)
          DO ix=1,rvec%mx
            DO is=1,rvec%n_side
              DO iq=1,rvec%nqty
                iel=ix+(iy-1)*rvec%mx
                iv=start_vert+2*(is-1)+1
                arrv(iq,is,ix,iy)=arrv(iq,is,ix,iy)+integrand(iq,iel,iv)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    ENDIF
    IF (ASSOCIATED(rvec%arri)) THEN
      ASSOCIATE (arri=>rvec%arri)
        id=par%get_stream(rvec%id,4,4)
        !$acc parallel present(arri,rvec,integrand)                             &
        !$acc copyin(start_int) wait(rvec%id) async(id) if(rvec%on_gpu)
        !$acc loop gang
        DO iy=1,rvec%my
          !$acc loop vector collapse(3) private(iel,iv)
          DO ix=1,rvec%mx
            DO ii=1,rvec%n_int
              DO iq=1,rvec%nqty
                iel=ix+(iy-1)*rvec%mx
                iv=start_int+ii-1
                arri(iq,ii,ix,iy)=arri(iq,ii,ix,iy)+integrand(iq,iel,iv)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
        !$acc end parallel
      END ASSOCIATE
    ENDIF
    IF (rvec%on_gpu) CALL par%wait_streams(rvec%id,4)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assemble_real

!-------------------------------------------------------------------------------
!*  Applies dirichlet boundary conditions
!-------------------------------------------------------------------------------
  SUBROUTINE dirichlet_bc_real(rvec,component,edge,symm,seam_save)
    USE boundary_ftns_mod
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> flag to determine normal/tangential/scalar behavior
    CHARACTER(*), INTENT(IN) :: component
    !> associated edge
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> flag for symmetric boundary
    CHARACTER(*), OPTIONAL, INTENT(IN) :: symm
    !> save the existing boundary data in seam_save
    LOGICAL, OPTIONAL, INTENT(IN) :: seam_save

    REAL(r8) :: proj
    REAL(r8), DIMENSION(rvec%nqty,rvec%nqty,edge%nvert) :: bcpmat
    REAL(r8), DIMENSION(rvec%nqty,rvec%nqty,rvec%n_side,edge%nvert) :: bcpmats
    INTEGER(i4), DIMENSION(rvec%nqty,2) :: bciarr
    INTEGER(i4) :: iv,ix,iy,is,isymm,iq1,iq2
    LOGICAL :: seam_save_loc,do_segment
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'dirichlet_bc_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   set the symmetry flag.  if symm starts with "t", the top boundary
!   is a symmetry condition.  if symm starts with "b", the bottom
!   boundary is a symmetry condition.
!-------------------------------------------------------------------------------
    isymm=0
    IF (PRESENT(symm)) THEN
      SELECT CASE(symm(1:1))
      CASE('t','T')
        isymm=1
      CASE('b','B')
        isymm=-1
      END SELECT
    ENDIF
    IF (PRESENT(seam_save)) THEN
      seam_save_loc=seam_save
    ELSE
      seam_save_loc=.FALSE.
    ENDIF
!-------------------------------------------------------------------------------
!   parse the component flag to create an integer array, which
!   indicates which scalar and 3-vector components have essential
!   conditions.
!-------------------------------------------------------------------------------
    CALL bcflag_parse(component,rvec%nqty,bciarr)
!-------------------------------------------------------------------------------
!   the bcdir_set routine combines the bciarr information with local
!   surface-normal and tangential unit directions.
!-------------------------------------------------------------------------------
    do_segment=.FALSE.
    IF (ASSOCIATED(rvec%arrh).OR.ASSOCIATED(rvec%arrv)) do_segment=.TRUE.
    DO iv=1,edge%nvert
      IF (edge%expoint(iv)) THEN
        ix=edge%vertex(iv)%intxy(1)
        iy=edge%vertex(iv)%intxy(2)
        CALL bcdir_set(rvec%nqty,bciarr,edge%excorner(iv),isymm,                &
                       edge%vertex(iv)%norm,edge%vertex(iv)%tang,bcpmat(:,:,iv))
      ENDIF
      IF (do_segment) THEN
        IF (edge%exsegment(iv)) THEN
          DO is=1,rvec%n_side
            CALL bcdir_set(rvec%nqty,bciarr,.false.,isymm,                      &
                           edge%segment(iv)%norm(:,is),                         &
                           edge%segment(iv)%tang(:,is),bcpmats(:,:,is,iv))
          ENDDO
        ENDIF
      ENDIF
    ENDDO
    ASSOCIATE (arr=>rvec%arr,arrh=>rvec%arrh,arrv=>rvec%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               expoint=>edge%expoint,exsegment=>edge%exsegment,                 &
               vert_save=>edge%vert_save,seg_save=>edge%seg_save)
      !$acc parallel present(arr,arrh,arrv,rvec,edge,vertex,segment)            &
      !$acc present(expoint,exsegment,vert_save,seg_save)                       &
      !$acc copyin(bcpmat,bcpmats,seam_save_loc,do_segment)                     &
      !$acc async(rvec%id) if(rvec%on_gpu)
      !$acc loop gang private(ix,iy)
      DO iv=1,edge%nvert
        IF (expoint(iv)) THEN
          ix=vertex(iv)%intxy(1)
          iy=vertex(iv)%intxy(2)
          !$acc loop vector private(proj)
          DO iq1=1,rvec%nqty
            proj=0.0
            !$acc loop seq
            DO iq2=1,rvec%nqty
              proj=proj+bcpmat(iq1,iq2,iv)*arr(iq2,ix,iy)
            ENDDO
            arr(iq1,ix,iy)=arr(iq1,ix,iy)-proj
            IF (seam_save_loc) vert_save(iq1,iv)=vert_save(iq1,iv)+proj
          ENDDO
        ENDIF
        IF (do_segment) THEN
          IF (exsegment(iv)) THEN
            ix=segment(iv)%intxys(1)
            iy=segment(iv)%intxys(2)
            IF (segment(iv)%h_side) THEN
              !$acc loop vector collapse(2) private(proj)
              DO is=1,rvec%n_side
                DO iq1=1,rvec%nqty
                  proj=0.0
                  !$acc loop seq
                  DO iq2=1,rvec%nqty
                    proj=proj+bcpmats(iq1,iq2,is,iv)*arrh(iq2,is,ix,iy)
                  ENDDO
                  arrh(iq1,is,ix,iy)=arrh(iq1,is,ix,iy)-proj
                  IF (seam_save_loc)                                            &
                    seg_save(iq1,is,iv)=seg_save(iq1,is,iv)+proj
                ENDDO
              ENDDO
            ELSE
              !$acc loop vector collapse(2) private(proj)
              DO is=1,rvec%n_side
                DO iq1=1,rvec%nqty
                  proj=0.0
                  !$acc loop seq
                  DO iq2=1,rvec%nqty
                    proj=proj+bcpmats(iq1,iq2,is,iv)*arrv(iq2,is,ix,iy)
                  ENDDO
                  arrv(iq1,is,ix,iy)=arrv(iq1,is,ix,iy)-proj
                  IF (seam_save_loc)                                            &
                    seg_save(iq1,is,iv)=seg_save(iq1,is,iv)+proj
                ENDDO
              ENDDO
            ENDIF
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
      !$acc update self(arr,arrh,arrv) async(rvec%id) if(rvec%on_gpu)
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE dirichlet_bc_real

!-------------------------------------------------------------------------------
!*  Applies regularity conditions at R=0 in toroidal geometry
!-------------------------------------------------------------------------------
  SUBROUTINE regularity_real(rvec,edge,flag)
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> associated edge
    TYPE(edge_type), INTENT(IN) :: edge
    !> flag
    CHARACTER(*), INTENT(IN) :: flag

    INTEGER(i4) :: iv,ix,iy,iside,ivec,nvec
    REAL(r8), DIMENSION(rvec%nqty,1) :: mult
    LOGICAL :: do_segment
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'regularity_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   apply regularity conditions to the different Fourier components
!   of a vector.  for a scalar quantity, n>0 are set to 0.  for a
!   vector, thd following components are set to 0
!   n=0:  r and phi
!   n=1:  z
!   n>1:  r, z, and phi
!
!   create an array of 1s and 0s to zero out the appropriate
!   components.  the geometry is always toroidal at this point.
!-------------------------------------------------------------------------------
    mult=0
    SELECT CASE(rvec%nqty)
    CASE(1,2,4,5)          !   scalars
      mult(:,1)=1
    CASE(3,6,9,12,15,18)   !   3-vectors
      nvec=rvec%nqty/3
      DO ivec=0,nvec-1
        mult(3*ivec+2,1)=1
      ENDDO
    CASE DEFAULT
      CALL par%nim_stop("vec_rect_2D::regularity_real"//                        &
                        " inconsistent # of components.")
    END SELECT
!-------------------------------------------------------------------------------
!   loop over block border elements and apply mult to the vertices
!   at R=0.  also combine rhs for n=1 r and phi comps.
!-------------------------------------------------------------------------------
    do_segment=.FALSE.
    IF (ASSOCIATED(rvec%arrh).OR.ASSOCIATED(rvec%arrv)) do_segment=.TRUE.
    ASSOCIATE (arr=>rvec%arr,arrh=>rvec%arrh,arrv=>rvec%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               r0point=>edge%r0point,r0segment=>edge%r0segment)
      !$acc parallel present(arr,arrh,arrv,rvec,edge,vertex,segment)            &
      !$acc present(r0point,r0segment) copyin(mult,do_segment)                  &
      !$acc async(rvec%id) if(rvec%on_gpu)
      !$acc loop gang private(ix,iy)
      DO iv=1,edge%nvert
        IF (r0point(iv)) THEN
          ix=vertex(iv)%intxy(1)
          iy=vertex(iv)%intxy(2)
          arr(:,ix,iy)=arr(:,ix,iy)*mult(:,1)
        ENDIF
        IF (do_segment) THEN
          IF (r0segment(iv)) THEN
            ix=segment(iv)%intxys(1)
            iy=segment(iv)%intxys(2)
            IF (segment(iv)%h_side) THEN
              !$acc loop worker
              DO iside=1,rvec%n_side
                arrh(:,iside,ix,iy)=arrh(:,iside,ix,iy)*mult(:,1)
              ENDDO
            ELSE
              !$acc loop worker
              DO iside=1,rvec%n_side
                arrv(:,iside,ix,iy)=arrv(:,iside,ix,iy)*mult(:,1)
              ENDDO
            ENDIF
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE regularity_real

!-------------------------------------------------------------------------------
!* set variables needed by edge routines
!-------------------------------------------------------------------------------
  SUBROUTINE set_edge_vars_real(rvec,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_real_acc), INTENT(IN) :: rvec
    !> edge to set vars in
    TYPE(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: iv
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'set_edge_vars_real',iftn,idepth)
    DO iv=1,rvec%mx
      edge%segment(iv)%h_side=.true.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyn
      edge%segment(iv)%load_dir=1_i4
    ENDDO
    DO iv=rvec%mx+1,rvec%mx+rvec%my
      edge%segment(iv)%h_side=.false.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyn
      edge%segment(iv)%load_dir=1_i4
    ENDDO
    DO iv=rvec%mx+rvec%my+1,2*rvec%mx+rvec%my
      edge%segment(iv)%h_side=.true.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyp
      edge%segment(iv)%load_dir=-1_i4
    ENDDO
    DO iv=2*rvec%mx+rvec%my+1,edge%nvert
      edge%segment(iv)%h_side=.false.
      edge%segment(iv)%intxys=edge%segment(iv)%intxyp
      edge%segment(iv)%load_dir=-1_i4
    ENDDO
    !$acc update device(edge%segment) async(edge%id) if(rvec%on_gpu)
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE set_edge_vars_real

!-------------------------------------------------------------------------------
!* load a edge communitcation array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_load_arr_real(rvec,edge,nqty,n_side,do_avg)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be loaded
    CLASS(vec_rect_2D_real_acc), INTENT(IN) :: rvec
    !> edge to load
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side
    !> apply edge average if true
    LOGICAL, OPTIONAL, INTENT(IN) :: do_avg

    INTEGER(i4) :: ix,iy,iv,is,nq,ns,ivs,ld
    LOGICAL :: do_avg_loc
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_load_arr_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=rvec%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=rvec%n_side
    ENDIF
    IF (PRESENT(do_avg)) THEN
      do_avg_loc=do_avg
    ELSE
      do_avg_loc=.FALSE.
    ENDIF
!-------------------------------------------------------------------------------
!   set internal flags for edge_network call
!-------------------------------------------------------------------------------
    edge%nqty_loaded=nq
    edge%nside_loaded=ns
    edge%nmodes_loaded=0_i4
    edge%on_gpu=.TRUE.
    ASSOCIATE (arr=>rvec%arr,arrh=>rvec%arrh,arrv=>rvec%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               vert_in=>edge%vert_in,seg_in=>edge%seg_in)
      !$acc parallel present(arr,arrh,arrv,edge,vertex,segment)                 &
      !$acc present(vert_in,seg_in) copyin(nq,ns,do_avg_loc)                    &
      !$acc async(rvec%id) if(rvec%on_gpu)
      !$acc loop gang private(ix,iy,ld)
      DO iv=1,edge%nvert
!-------------------------------------------------------------------------------
!       copy block-internal data to edge storage.  vertex-centered data first.
!-------------------------------------------------------------------------------
        ix=vertex(iv)%intxy(1)
        iy=vertex(iv)%intxy(2)
        vert_in(1:nq,iv)=arr(1:nq,ix,iy)
!-------------------------------------------------------------------------------
!       element side-centered data.  load side-centered nodes in the
!       direction of the edge (ccw around the block), as indicated by load_dir.
!-------------------------------------------------------------------------------
        IF (ns/=0) THEN
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          ld=segment(iv)%load_dir
          IF (segment(iv)%h_side) THEN
            !$acc loop worker private(ivs)
            DO is=1,ns
              ivs=(((1-ld)/2)*(ns+1)+ld*is-1)*nq+1
              seg_in(ivs:ivs+nq-1,iv)=arrh(1:nq,is,ix,iy)
            ENDDO
          ELSE
            !$acc loop worker private(ivs)
            DO is=1,ns
              ivs=(((1-ld)/2)*(ns+1)+ld*is-1)*nq+1
              seg_in(ivs:ivs+nq-1,iv)=arrv(1:nq,is,ix,iy)
            ENDDO
          ENDIF
        ENDIF
!-------------------------------------------------------------------------------
!       apply average factor
!-------------------------------------------------------------------------------
        IF (do_avg_loc) THEN
          vert_in(1:nq,iv)=vert_in(1:nq,iv)*vertex(iv)%ave_factor
          IF (ns/=0) THEN
            seg_in(1:nq*ns,iv)=seg_in(1:nq*ns,iv)*segment(iv)%ave_factor
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
!-------------------------------------------------------------------------------
!   copy data to CPU, wait/transfer in seam%edge_network
!   TODO: unify seam array structures to avoid extra copy/waits
!   TODO: use GPU RDMA in seam%edge_network
!-------------------------------------------------------------------------------
    ASSOCIATE (vert_in=>edge%vert_in,seg_in=>edge%seg_in)
      !$acc update self(vert_in) async(rvec%id) if(rvec%on_gpu)
      IF (ns/=0) THEN
        !$acc update self(seg_in) async(rvec%id) if(rvec%on_gpu)
      ENDIF
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_load_arr_real

!-------------------------------------------------------------------------------
!* unload a edge communication array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_unload_arr_real(rvec,edge)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be unloaded
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> edge to unload
    TYPE(edge_type), INTENT(INOUT) :: edge

    INTEGER(i4) :: ix,iy,iv,is,nq,ns,ivs,ld
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_unload_arr_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   read edge internal flags
!-------------------------------------------------------------------------------
    nq=edge%nqty_loaded
    ns=edge%nside_loaded
    ASSOCIATE (arr=>rvec%arr,arrh=>rvec%arrh,arrv=>rvec%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               vert_out=>edge%vert_out,seg_out=>edge%seg_out)
      !$acc parallel present(arr,arrh,arrv,edge,vertex,segment)                 &
      !$acc present(vert_out,seg_out) copyin(nq,ns)                             &
      !$acc async(rvec%id) if(rvec%on_gpu)
      !$acc loop gang private(ix,iy,ld)
      DO iv=1,edge%nvert
!-------------------------------------------------------------------------------
!       copy from edge storage to block-internal data.
!       vertex-centered data first.
!-------------------------------------------------------------------------------
        ix=vertex(iv)%intxy(1)
        iy=vertex(iv)%intxy(2)
        arr(1:nq,ix,iy)=vert_out(1:nq,iv)
!-------------------------------------------------------------------------------
!       element side-centered data.  unload side-centered nodes in the
!       direction of the edge (ccw around the block), as indicated by load_dir.
!-------------------------------------------------------------------------------
        IF (ns/=0) THEN
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          ld=segment(iv)%load_dir
          IF (segment(iv)%h_side) THEN
            !$acc loop worker private(ivs)
            DO is=1,ns
              ivs=(((1-ld)/2)*(ns+1)+ld*is-1)*nq+1
              arrh(1:nq,is,ix,iy)=seg_out(ivs:ivs+nq-1,iv)
            ENDDO
          ELSE
            !$acc loop worker private(ivs)
            DO is=1,ns
              ivs=(((1-ld)/2)*(ns+1)+ld*is-1)*nq+1
              arrv(1:nq,is,ix,iy)=seg_out(ivs:ivs+nq-1,iv)
            ENDDO
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_unload_arr_real

!-------------------------------------------------------------------------------
!* load a edge save array with vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_load_save_real(rvec,edge,nqty,n_side)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be loaded
    CLASS(vec_rect_2D_real_acc), INTENT(IN) :: rvec
    !> edge to load
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side

    INTEGER(i4) :: ix,iy,iv,is,nq,ns
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_load_save_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=rvec%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=rvec%n_side
    ENDIF
    ASSOCIATE (arr=>rvec%arr,arrh=>rvec%arrh,arrv=>rvec%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               vert_save=>edge%vert_save,seg_save=>edge%seg_save)
      !$acc parallel present(arr,arrh,arrv,edge,vertex,segment)                 &
      !$acc present(vert_save,seg_save) copyin(nq,ns)                           &
      !$acc async(rvec%id) if(rvec%on_gpu)
      !$acc loop gang private(ix,iy)
      DO iv=1,edge%nvert
!-------------------------------------------------------------------------------
!       copy block-internal data to edge storage.  vertex-centered data first.
!-------------------------------------------------------------------------------
        ix=vertex(iv)%intxy(1)
        iy=vertex(iv)%intxy(2)
        vert_save(1:nq,iv)=arr(1:nq,ix,iy)
!-------------------------------------------------------------------------------
!       element side-centered data.
!-------------------------------------------------------------------------------
        IF (ns/=0) THEN
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          IF (segment(iv)%h_side) THEN
            !$acc loop worker
            DO is=1,ns
              seg_save(1:nq,is,iv)=arrh(1:nq,is,ix,iy)
            ENDDO
          ELSE
            !$acc loop worker
            DO is=1,ns
              seg_save(1:nq,is,iv)=arrv(1:nq,is,ix,iy)
            ENDDO
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_load_save_real

!-------------------------------------------------------------------------------
!* unload a edge save array and add to vector data
!-------------------------------------------------------------------------------
  SUBROUTINE edge_add_save_real(rvec,edge,nqty,n_side)
    USE local
    USE edge_mod
    IMPLICIT NONE

    !> vector to be add to
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> edge to add from
    TYPE(edge_type), INTENT(INOUT) :: edge
    !> number of quantities to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: nqty
    !> number of side points to load (default all)
    INTEGER(i4), OPTIONAL, INTENT(IN) :: n_side

    INTEGER(i4) :: ix,iy,iv,is,nq,ns
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'edge_add_save_real',iftn,idepth)
!-------------------------------------------------------------------------------
!   parse optional input
!-------------------------------------------------------------------------------
    IF (PRESENT(nqty)) THEN
      nq=nqty
    ELSE
      nq=rvec%nqty
    ENDIF
    IF (PRESENT(n_side)) THEN
      ns=n_side
    ELSE
      ns=rvec%n_side
    ENDIF
    ASSOCIATE (arr=>rvec%arr,arrh=>rvec%arrh,arrv=>rvec%arrv,                   &
               vertex=>edge%vertex,segment=>edge%segment,                       &
               vert_save=>edge%vert_save,seg_save=>edge%seg_save)
      !$acc parallel present(arr,arrh,arrv,edge,vertex,segment)                 &
      !$acc present(vert_save,seg_save) copyin(nq,ns)                           &
      !$acc async(rvec%id) if(rvec%on_gpu)
      !$acc loop gang private(ix,iy)
      DO iv=1,edge%nvert
!-------------------------------------------------------------------------------
!       copy from edge storage to block-internal data.
!       vertex-centered data first.
!-------------------------------------------------------------------------------
        ix=vertex(iv)%intxy(1)
        iy=vertex(iv)%intxy(2)
        arr(1:nq,ix,iy)=arr(1:nq,ix,iy)+vert_save(1:nq,iv)
!-------------------------------------------------------------------------------
!       element side-centered data.
!-------------------------------------------------------------------------------
        IF (ns/=0) THEN
          ix=segment(iv)%intxys(1)
          iy=segment(iv)%intxys(2)
          IF (segment(iv)%h_side) THEN
            !$acc loop worker
            DO is=1,ns
              arrh(1:nq,is,ix,iy)=arrh(1:nq,is,ix,iy)+seg_save(1:nq,is,iv)
            ENDDO
          ELSE
            !$acc loop worker
            DO is=1,ns
              arrv(1:nq,is,ix,iy)=arrv(1:nq,is,ix,iy)+seg_save(1:nq,is,iv)
            ENDDO
          ENDIF
        ENDIF
      ENDDO
      !$acc end parallel
    END ASSOCIATE
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE edge_add_save_real

!-------------------------------------------------------------------------------
!> Assume vector data represents the diagonal of a matrix and apply a matrix
!  multiply.
!-------------------------------------------------------------------------------
  SUBROUTINE apply_diag_matvec_real(rvec,input_vec,output_vec)
    IMPLICIT NONE

    !> vector that represents a diagonal matrix
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> vector to multiply
    CLASS(rvector), INTENT(IN) :: input_vec
    !> output vector = rvec (matrix diagonal) dot input_vec
    CLASS(rvector), INTENT(INOUT) :: output_vec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'apply_diag_matvec_real',iftn,idepth)
    SELECT TYPE (input_vec)
    TYPE IS (vec_rect_2D_real_acc)
      SELECT TYPE (output_vec)
      TYPE IS (vec_rect_2D_real_acc)
        ASSOCIATE (arr_mat=>rvec%arr,arrh_mat=>rvec%arrh,                       &
                   arrv_mat=>rvec%arrv,arri_mat=>rvec%arri,                     &
                   arr_in=>input_vec%arr,arrh_in=>input_vec%arrh,               &
                   arrv_in=>input_vec%arrv,arri_in=>input_vec%arri,             &
                   arr_out=>output_vec%arr,arrh_out=>output_vec%arrh,           &
                   arrv_out=>output_vec%arrv,arri_out=>output_vec%arri)
          IF (rvec%pd>1) THEN
            IF (rvec%skip_elim_interior) THEN
              !$acc kernels present(arr_mat,arrh_mat,arrv_mat)                  &
              !$acc present(arr_in,arrh_in,arrv_in)                             &
              !$acc present(arr_out,arrh_out,arrv_out)                          &
              !$acc async(rvec%id) if(rvec%on_gpu)
              arr_out=arr_mat*arr_in
              arrh_out=arrh_mat*arrh_in
              arrv_out=arrv_mat*arrv_in
              !$acc end kernels
            ELSE
              !$acc kernels present(arr_mat,arrh_mat,arrv_mat,arri_mat)         &
              !$acc present(arr_in,arrh_in,arrv_in,arri_in)                     &
              !$acc present(arr_out,arrh_out,arrv_out,arri_out)                 &
              !$acc async(rvec%id) if(rvec%on_gpu)
              arr_out=arr_mat*arr_in
              arrh_out=arrh_mat*arrh_in
              arrv_out=arrv_mat*arrv_in
              arri_out=arri_mat*arri_in
              !$acc end kernels
            ENDIF
          ELSE
            !$acc kernels present(arr_mat,arr_in,arr_out)                       &
            !$acc async(rvec%id) if(rvec%on_gpu)
            arr_out=arr_mat*arr_in
            !$acc end kernels
          ENDIF
        END ASSOCIATE
      CLASS DEFAULT
        CALL par%nim_stop('Expected vec_rect_2D_real_acc for output_vec'        &
                          //' in apply_diag_matvec_real')
      END SELECT
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real_acc for input_vec'           &
                        //' in apply_diag_matvec_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE apply_diag_matvec_real

!-------------------------------------------------------------------------------
!> Invert the values of the vector component-wise
!-------------------------------------------------------------------------------
  SUBROUTINE invert_real(rvec)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'invert_real',iftn,idepth)
    IF (rvec%pd>1) THEN
      ASSOCIATE (arr=>rvec%arr,arrh=>rvec%arrh,arrv=>rvec%arrv,arri=>rvec%arri)
        IF (rvec%skip_elim_interior) THEN
          !$acc kernels present(arr,arrh,arrv,rvec) async(rvec%id) if(rvec%on_gpu)
          arr(:,0:rvec%mx,0:rvec%my)=1._r8/arr(:,0:rvec%mx,0:rvec%my)
          arrh(:,:,1:rvec%mx,0:rvec%my)=1._r8/arrh(:,:,1:rvec%mx,0:rvec%my)
          arrv(:,:,0:rvec%mx,1:rvec%my)=1._r8/arrv(:,:,0:rvec%mx,1:rvec%my)
          !$acc end kernels
        ELSE
          !$acc kernels present(arr,arrh,arrv,arri,rvec)                        &
          !$acc async(rvec%id) if(rvec%on_gpu)
          arr(:,0:rvec%mx,0:rvec%my)=1._r8/arr(:,0:rvec%mx,0:rvec%my)
          arrh(:,:,1:rvec%mx,0:rvec%my)=1._r8/arrh(:,:,1:rvec%mx,0:rvec%my)
          arrv(:,:,0:rvec%mx,1:rvec%my)=1._r8/arrv(:,:,0:rvec%mx,1:rvec%my)
          arri(:,:,1:rvec%mx,1:rvec%my)=1._r8/arri(:,:,1:rvec%mx,1:rvec%my)
          !$acc end kernels
        ENDIF
      END ASSOCIATE
    ELSE
      ASSOCIATE (arr=>rvec%arr)
        !$acc kernels present(arr,rvec) async(rvec%id) if(rvec%on_gpu)
        arr(:,0:rvec%mx,0:rvec%my)=1._r8/arr(:,0:rvec%mx,0:rvec%my)
        !$acc end kernels
      END ASSOCIATE
    ENDIF
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE invert_real

!-------------------------------------------------------------------------------
!* Assign to the vector for testing
!-------------------------------------------------------------------------------
  SUBROUTINE assign_for_testing_real(rvec,operation,const)
    IMPLICIT NONE

    !> vector to assign to
    CLASS(vec_rect_2D_real_acc), INTENT(INOUT) :: rvec
    !> operation: 'constant' or 'index'
    CHARACTER(*), INTENT(IN) :: operation
    !> optional constant value for operation='constant'
    REAL(r8), INTENT(IN), OPTIONAL :: const

    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1
    REAL(r8) :: fac
    INTEGER(i4) :: ii1,ii2,ii3,ii4,ind

    CALL timer%start_timer_l2(mod_name,'assign_for_testing_real',iftn,idepth)
    fac=1._r8
    IF (PRESENT(const)) fac=const
    CALL rvec%zero
    SELECT CASE(operation)
    CASE("constant")
      IF (ASSOCIATED(rvec%arr)) THEN
        ASSOCIATE (arr=>rvec%arr)
          !$acc kernels copyin(fac) present(arr,rvec) async(rvec%id) if(rvec%on_gpu)
          arr(:,0:rvec%mx,0:rvec%my)=fac
          !$acc end kernels
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(rvec%arrh)) THEN
        ASSOCIATE (arrh=>rvec%arrh)
          !$acc kernels copyin(fac) present(arrh,rvec) async(rvec%id) if(rvec%on_gpu)
          arrh(:,:,1:rvec%mx,0:rvec%my)=fac
          !$acc end kernels
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(rvec%arrv)) THEN
        ASSOCIATE (arrv=>rvec%arrv)
          !$acc kernels copyin(fac) present(arrv,rvec) async(rvec%id) if(rvec%on_gpu)
          arrv(:,:,0:rvec%mx,1:rvec%my)=fac
          !$acc end kernels
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior) THEN
        ASSOCIATE (arri=>rvec%arri)
          !$acc kernels copyin(fac) present(arri,rvec) async(rvec%id) if(rvec%on_gpu)
          arri(:,:,1:rvec%mx,1:rvec%my)=fac
          !$acc end kernels
        END ASSOCIATE
      ENDIF
    CASE("index")
      IF (ASSOCIATED(rvec%arr)) THEN
        ASSOCIATE (arr=>rvec%arr)
          !$acc parallel present(rvec,arr) async(rvec%id) if(rvec%on_gpu)
          !$acc loop gang vector collapse(3) private(ind)
          DO ii1=1,rvec%nqty
            DO ii2=0,rvec%mx
              DO ii3=0,rvec%my
                ind=ii1*SIZE(arr,2)*SIZE(arr,3)+ii2*SIZE(arr,3)+ii3
                arr(ii1,ii2,ii3)=ind
              ENDDO
            ENDDO
          ENDDO
          !$acc end parallel
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(rvec%arrh)) THEN
        ASSOCIATE (arrh=>rvec%arrh)
          !$acc parallel present(rvec,arrh) async(rvec%id) if(rvec%on_gpu)
          !$acc loop gang vector collapse(4) private(ind)
          DO ii1=1,rvec%nqty
            DO ii2=1,rvec%n_side
              DO ii3=1,rvec%mx
                DO ii4=0,rvec%my
                  ind=ii1*SIZE(arrh,2)*SIZE(arrh,3)*SIZE(arrh,4)+               &
                      ii2*SIZE(arrh,3)*SIZE(arrh,4)+ii3*SIZE(arrh,4)+ii4
                  arrh(ii1,ii2,ii3,ii4)=ind
                ENDDO
              ENDDO
            ENDDO
          ENDDO
          !$acc end parallel
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(rvec%arrv)) THEN
        ASSOCIATE (arrv=>rvec%arrv)
          !$acc parallel present(rvec,arrv) async(rvec%id) if(rvec%on_gpu)
          !$acc loop gang vector collapse(4) private(ind)
          DO ii1=1,rvec%nqty
            DO ii2=1,rvec%n_side
              DO ii3=0,rvec%mx
                DO ii4=1,rvec%my
                  ind=ii1*SIZE(arrv,2)*SIZE(arrv,3)*SIZE(arrv,4)+               &
                      ii2*SIZE(arrv,3)*SIZE(arrv,4)+ii3*SIZE(arrv,4)+ii4
                  arrv(ii1,ii2,ii3,ii4)=ind
                ENDDO
              ENDDO
            ENDDO
          ENDDO
          !$acc end parallel
        END ASSOCIATE
      ENDIF
      IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior) THEN
        ASSOCIATE (arri=>rvec%arri)
          !$acc parallel present(rvec,arri) async(rvec%id) if(rvec%on_gpu)
          !$acc loop gang vector collapse(4) private(ind)
          DO ii1=1,rvec%nqty
            DO ii2=1,rvec%n_int
              DO ii3=1,rvec%mx
                DO ii4=1,rvec%my
                  ind=ii1*SIZE(arri,2)*SIZE(arri,3)*SIZE(arri,4)+               &
                      ii2*SIZE(arri,3)*SIZE(arri,4)+ii3*SIZE(arri,4)+ii4
                  arri(ii1,ii2,ii3,ii4)=ind
                ENDDO
              ENDDO
            ENDDO
          ENDDO
          !$acc end parallel
        END ASSOCIATE
      ENDIF
    CASE DEFAULT
      CALL par%nim_stop('Expected operation=constant or index'                  &
                        //' in assign_for_testing_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END SUBROUTINE assign_for_testing_real

!-------------------------------------------------------------------------------
!*  Test if one vector is equal to another
!-------------------------------------------------------------------------------
  LOGICAL FUNCTION test_if_equal_real(rvec,rvec2) RESULT(output)
    IMPLICIT NONE

    !> vector
    CLASS(vec_rect_2D_real_acc), INTENT(IN) :: rvec
    !> vector to test against
    CLASS(rvector), INTENT(IN) :: rvec2

    INTEGER(i4) :: shape3a(3),shape3b(3),shape4a(4),shape4b(4)
    INTEGER(i4) :: idepth
    INTEGER(i4), SAVE :: iftn=-1

    CALL timer%start_timer_l2(mod_name,'test_if_equal_real',iftn,idepth)
    output=.TRUE.
    SELECT TYPE (rvec2)
    TYPE IS (vec_rect_2D_real_acc)
      !$acc update self(rvec%arr,rvec%arrh,rvec%arrv,rvec%arri)                 &
      !$acc async(rvec%id) if(rvec%on_gpu)
      !$acc update self(rvec2%arr,rvec2%arrh,rvec2%arrv,rvec2%arri)             &
      !$acc async(rvec%id) if(rvec%on_gpu)
      !$acc wait(rvec%id) if(rvec%on_gpu)
      IF (ASSOCIATED(rvec%arr)) THEN
        shape3a=SHAPE(rvec%arr)
        shape3b=SHAPE(rvec2%arr)
        IF (.NOT.ALL(shape3a == shape3b)) output=.FALSE.
        IF (output.AND..NOT.ALL(rvec%arr == rvec2%arr)) output=.FALSE.
      ELSE IF (ASSOCIATED(rvec2%arr)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(rvec%arrh)) THEN
        shape4a=SHAPE(rvec%arrh)
        shape4b=SHAPE(rvec2%arrh)
        IF (.NOT.ALL(shape4a == shape4b)) output=.FALSE.
        IF (output.AND..NOT.ALL(rvec%arrh == rvec2%arrh)) output=.FALSE.
      ELSE IF (ASSOCIATED(rvec2%arrh)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(rvec%arrv)) THEN
        shape4a=SHAPE(rvec%arrv)
        shape4b=SHAPE(rvec2%arrv)
        IF (.NOT.ALL(shape4a == shape4b)) output=.FALSE.
        IF (output.AND..NOT.ALL(rvec%arrv == rvec2%arrv)) output=.FALSE.
      ELSE IF (ASSOCIATED(rvec2%arrv)) THEN
        output=.FALSE.
      ENDIF
      IF (ASSOCIATED(rvec%arri).AND..NOT.rvec%skip_elim_interior) THEN
        shape4a=SHAPE(rvec%arri)
        shape4b=SHAPE(rvec2%arri)
        IF (.NOT.ALL(shape4a == shape4b)) output=.FALSE.
        IF (output.AND..NOT.ALL(rvec%arri == rvec2%arri)) output=.FALSE.
      ELSE IF (ASSOCIATED(rvec2%arri)) THEN
        output=.FALSE.
      ENDIF
    CLASS DEFAULT
      CALL par%nim_stop('Expected vec_rect_2D_real for vec2'//                  &
                        ' in test_if_equal_real')
    END SELECT
    CALL timer%end_timer_l2(iftn,idepth)
  END FUNCTION test_if_equal_real
