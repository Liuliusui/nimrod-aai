######################################################################
#
# CMakeLists.txt for NIMROD abstract accelerated infrastructure
#
######################################################################

# Required version
cmake_minimum_required(VERSION 3.20.0)

# Use Fortran
enable_language(Fortran)

# Project information
project(nimrod-abstract-accelerated-infrastructure)

#---------------------------------------------------------------------
# Specify cmake project options.
# - Use ccmake to easily view all project options.
# - See cmake/math.cmake for math options
#---------------------------------------------------------------------
option(DEBUG "Whether to enable debug code" $<CONFIG:DEBUG>)
option(ENABLE_CODE_COVERAGE "Whether unit test code coveragea is generated")
option(ENABLE_MPI "Whether to enable MPI: Default is false")
option(ENABLE_OPENACC "Whether to enable OpenACC: Default is false")
option(ENABLE_OPENACC_AUTOCOMPARE "Whether to enable OpenACC autocompare: Default is false")
option(ENABLE_SHARED "Whether to build and use shared libraries")
option(TIME_LEVEL1 "Whether to add timings at level 1")
option(TIME_LEVEL2 "Whether to add timings at level 2")
set(MPI_TEST_NPROCS 4 CACHE STRING "Number of procs to use for mpi tests")
option(NVTX_PROFILE "Whether to enable NVTX profiling")
option(OBJ_MEM_PROF "Wheter to enable type-bound memory profiling")
set(OPENACC_CC native CACHE STRING "OpenACC compute capability: Default is native")
option(TRAP_FP_EXCEPTIONS "Whether to enable trapping of floating point exceptions")

#--------------------------------------------------------------------
# Initialization
#--------------------------------------------------------------------
set(CMAKE_COLOR_MAKEFILE TRUE)
if (ENABLE_SHARED)
  option(BUILD_SHARED_LIBS "Build using shared libraries" ON)
  set(CMAKE_POSITION_INDEPENDENT_CODE 1)
else ()
  set(CMAKE_POSITION_INDEPENDENT_CODE 0)
endif ()
if (NOT DEFINED CMAKE_INSTALL_RPATH_USE_LINK_PATH)
  set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
endif ()
if ("$ENV{NERSC_HOST}" STREQUAL "perlmutter")
  set(NERSC_PERLMUTTER true)
endif () 
include(${CMAKE_CURRENT_LIST_DIR}/cmake/get_tgt_prop.cmake)

#---------------------------------------------------------------------
# Determine project git repository information
#---------------------------------------------------------------------
include(${CMAKE_CURRENT_LIST_DIR}/cmake/git.cmake)

#---------------------------------------------------------------------
# Setup config.f for Fortran preprocessor includes
#---------------------------------------------------------------------
include(${CMAKE_CURRENT_LIST_DIR}/cmake/config_f.cmake)

#---------------------------------------------------------------------
# Add compiler specific definitions and flags interface library
#---------------------------------------------------------------------
add_library(nimrod_compile_options INTERFACE)
target_include_directories(nimrod_compile_options INTERFACE
  $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}/>
  $<INSTALL_INTERFACE:share/>
)
include(${CMAKE_CURRENT_LIST_DIR}/cmake/compiler_flags.cmake)

#---------------------------------------------------------------------
# Find MPI
#---------------------------------------------------------------------
include(${CMAKE_CURRENT_LIST_DIR}/cmake/mpi.cmake)

#---------------------------------------------------------------------
# Find math libs
#---------------------------------------------------------------------
include(${CMAKE_CURRENT_LIST_DIR}/cmake/math.cmake)

#---------------------------------------------------------------------
# Find I/O libraries
#---------------------------------------------------------------------
include(${CMAKE_CURRENT_LIST_DIR}/cmake/io.cmake)

#---------------------------------------------------------------------
#  Add ctest test harness
#---------------------------------------------------------------------
include(${CMAKE_CURRENT_LIST_DIR}/nimtests/nimTests.cmake)

#---------------------------------------------------------------------
# Add subdirectories and assemble object library targets into
# OBJ_TARGETS list for later use in single installed library creation.
#---------------------------------------------------------------------
set(OBJ_TARGETS)

add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/extlibs)
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/nimloc)
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/nimtests)
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/nimlib)
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/nimseam)
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/nimfft)
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/nimlinalg)
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/nimiter)
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/nimfem)
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/nimblk)
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/nimgrid)
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/example)

#---------------------------------------------------------------------
# Create combined library
#---------------------------------------------------------------------
add_library(nimrod-aai-static STATIC ${OBJ_TARGETS})
install(TARGETS nimrod-aai-static EXPORT nimrod-aai-static-target
    DESTINATION lib/
    PERMISSIONS OWNER_READ OWNER_WRITE
                GROUP_READ GROUP_WRITE
                WORLD_READ
)
install(EXPORT nimrod-aai-static-target
    FILE NIMROD-AAI-static.cmake
    NAMESPACE NIMROD-AAI
    DESTINATION share/cmake
)
if (ENABLE_SHARED)
  add_library(nimrod-aai-shared SHARED ${OBJ_TARGETS})
  target_link_libraries(nimrod-aai-shared PUBLIC HDF5::HDF5)
  install(TARGETS nimrod-aai-shared EXPORT nimrod-aai-shared-target
      DESTINATION lib/
      PERMISSIONS OWNER_READ OWNER_WRITE
                  GROUP_READ GROUP_WRITE
                  WORLD_READ
  )
  install(EXPORT nimrod-aai-shared-target
      FILE NIMROD-AAI-shared.cmake
      NAMESPACE NIMROD-AAI
      DESTINATION share/cmake
  )
endif ()

# TODO: improve with cmake support for Fortran modules
# https://gitlab.kitware.com/cmake/cmake/-/issues/19608
install(DIRECTORY
    ${PROJECT_BINARY_DIR}/extlibs/
    ${PROJECT_BINARY_DIR}/nimloc/
    ${PROJECT_BINARY_DIR}/nimtests/
    ${PROJECT_BINARY_DIR}/nimlib/
    ${PROJECT_BINARY_DIR}/nimseam/
    ${PROJECT_BINARY_DIR}/nimfft/
    ${PROJECT_BINARY_DIR}/nimlinalg/
    ${PROJECT_BINARY_DIR}/nimiter/
    ${PROJECT_BINARY_DIR}/nimblk/
    ${PROJECT_BINARY_DIR}/nimbgrid/
    DESTINATION include/
    FILES_MATCHING PATTERN "*.mod"
    PATTERN "CMakeFiles" EXCLUDE
    PATTERN "tests" EXCLUDE
    PERMISSIONS OWNER_READ OWNER_WRITE
                GROUP_READ GROUP_WRITE
                WORLD_READ
)

#---------------------------------------------------------------------
# create config.h
#---------------------------------------------------------------------
CONFIGURE_FILE(${CMAKE_CURRENT_LIST_DIR}/config.h.cmake ${PROJECT_BINARY_DIR}/config.h)

#---------------------------------------------------------------------
# Default install location
#---------------------------------------------------------------------

if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
  set(CMAKE_INSTALL_PREFIX "./install" CACHE PATH "..." FORCE)
endif()
