!-------------------------------------------------------------------------------
!! Module containing a reference function for collocation and interpolation
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!* Module containing a reference function for collocation and interpolation
!-------------------------------------------------------------------------------
MODULE fake_function_mod
  USE function_mod
  USE local
  USE pardata_mod
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: fake_function_real,fake_function_comp

!-------------------------------------------------------------------------------
!>  Module containing a reference function for collocation and interpolation
!   for real types
!-------------------------------------------------------------------------------
  TYPE, EXTENDS(abstract_function_real) :: fake_function_real
    PRIVATE

    !> reference polynomial degree
    INTEGER(i4) :: pd=0
    !> reference derivative type
    CHARACTER(8) :: deriv_type="none"
  CONTAINS

    PROCEDURE, PASS(func) :: init => init_real
    PROCEDURE, PASS(func) :: eval => fake_eval_real
    PROCEDURE, PASS(func) :: eval_d => fake_eval_d_real
    PROCEDURE, PASS(func) :: grad => fake_grad_real
    PROCEDURE, PASS(func) :: rot2 => fake_rot2_real
  END TYPE fake_function_real

!-------------------------------------------------------------------------------
!>  Module containing a reference function for collocation and interpolation
!   for complex types
!-------------------------------------------------------------------------------
  TYPE, EXTENDS(abstract_function_comp) :: fake_function_comp
    PRIVATE

    !> reference polynomial degree
    INTEGER(i4) :: pd=0
    !> reference derivative type
    CHARACTER(8) :: deriv_type="none"
  CONTAINS

    PROCEDURE, PASS(func) :: init => init_comp
    PROCEDURE, PASS(func) :: eval => fake_eval_comp
    PROCEDURE, PASS(func) :: eval_d => fake_eval_d_comp
    PROCEDURE, PASS(func) :: grad => fake_grad_comp
    PROCEDURE, PASS(func) :: rot2 => fake_rot2_comp
  END TYPE fake_function_comp

  REAL(r8), PARAMETER, DIMENSION(0:9) :: rcoef=                                 &
    [1._r8, 0.5_r8, 0.2_r8, 0.1_r8, 0.05_r8, 0.02_r8, 0.01_r8, 0.005_r8,        &
     0.002_r8, 0.001_r8]
  COMPLEX(r8), PARAMETER, DIMENSION(0:9) :: ccoef=                              &
    [CMPLX(1._r8,0.5_r8,r8),      CMPLX(0.5_r8,1._r8,r8),                       &
     CMPLX(0.2_r8,0.1_r8,r8),     CMPLX(0.1_r8,0.2_r8,r8),                      &
     CMPLX(0.05_r8,0.02_r8,r8),   CMPLX(0.02_r8,0.05_r8,r8),                    &
     CMPLX(0.01_r8,0.005_r8,r8),  CMPLX(0.005_r8,0.0025_r8,r8),                 &
     CMPLX(0.002_r8,0.001_r8,r8), CMPLX(0.001_r8,0.0005_r8,r8)]
CONTAINS

!-------------------------------------------------------------------------------
!> Set the polynomial degree and derivative type
!-------------------------------------------------------------------------------
  SUBROUTINE init_real(func,poly_degree,deriv_type)
    IMPLICIT NONE

    !> function to set
    CLASS(fake_function_real), INTENT(INOUT) :: func
    !> polynomial degree
    INTEGER(i4), INTENT(IN) :: poly_degree
    !> polynomial derivative type, see fake_eval_d_real for acceptable values
    CHARACTER(*), INTENT(IN) :: deriv_type

    func%pd=poly_degree
    func%deriv_type=deriv_type
  END SUBROUTINE init_real

!-------------------------------------------------------------------------------
!> Evaluate the polynomial at a given independent variable
!-------------------------------------------------------------------------------
  SUBROUTINE fake_eval_real(func,in_var,out_var)
    IMPLICIT NONE

    !> function to evaluate
    CLASS(fake_function_real), INTENT(IN) :: func
    !> independent variable
    REAL(r8), DIMENSION(:), INTENT(IN) :: in_var
    !> dependent variable
    REAL(r8), DIMENSION(:), INTENT(OUT) :: out_var

    REAL(r8) :: poly_eval
    INTEGER(i4) :: ic,ip

    out_var=1._r8
    DO ic=1,SIZE(in_var)
      poly_eval=0._r8
      DO ip=0,MIN(func%pd,9)
        poly_eval=poly_eval + rcoef(ip)*in_var(ic)**ip
      ENDDO
      out_var=out_var*poly_eval
    ENDDO
  END SUBROUTINE fake_eval_real

!-------------------------------------------------------------------------------
!> Evaluate the polynomial derivative at a given independent variable
!-------------------------------------------------------------------------------
  SUBROUTINE fake_eval_d_real(func,in_var,doutdin_var)
    IMPLICIT NONE

    !> function to evaluate
    CLASS(fake_function_real), INTENT(IN) :: func
    !> independent variable
    REAL(r8), DIMENSION(:), INTENT(IN) :: in_var
    !> dependent variable derivative, size varies by deriv_type
    REAL(r8), DIMENSION(:,:), INTENT(OUT) :: doutdin_var

    SELECT CASE (TRIM(func%deriv_type))
    CASE ("grad")
      CALL func%grad(in_var,doutdin_var)
    CASE ("rot2")
      CALL func%rot2(in_var,doutdin_var)
    CASE DEFAULT
      CALL par%nim_stop("fake_eval_d deriv_type not recognized")
    END SELECT
  END SUBROUTINE fake_eval_d_real

!-------------------------------------------------------------------------------
!> Evaluate the polynomial gradient at a given independent variable
!-------------------------------------------------------------------------------
  SUBROUTINE fake_grad_real(func,in_var,doutdin_var)
    IMPLICIT NONE

    !> function to evaluate
    CLASS(fake_function_real), INTENT(IN) :: func
    !> independent variable
    REAL(r8), DIMENSION(:), INTENT(IN) :: in_var
    !> dependent variable derivative, size (any, SIZE(in_var)), output is
    !> uniform in the first dimension
    REAL(r8), DIMENSION(:,:), INTENT(OUT) :: doutdin_var

    REAL(r8) :: poly_eval,poly_deriv
    INTEGER(i4) :: ic,idc,ip,iq,nin,nqty

    nin=SIZE(in_var)
    nqty=SIZE(doutdin_var,1)
    doutdin_var=1._r8
    DO ic=1,nin
      poly_eval=0._r8
      poly_deriv=0._r8
      DO ip=0,MIN(func%pd,9)
        poly_eval=poly_eval + rcoef(ip)*in_var(ic)**ip
        poly_deriv=poly_deriv + ip*rcoef(ip)*in_var(ic)**(ip-1)
      ENDDO
      DO iq=1,nqty
        DO idc=1,nin
          IF (ic==idc) THEN
            doutdin_var(iq,idc)=doutdin_var(iq,idc)*poly_deriv
          ELSE
            doutdin_var(iq,idc)=doutdin_var(iq,idc)*poly_eval
          ENDIF
        ENDDO
      ENDDO
    ENDDO
  END SUBROUTINE fake_grad_real

!-------------------------------------------------------------------------------
!> Evaluate the polynomial 2D curl at a given independent variable
!-------------------------------------------------------------------------------
  SUBROUTINE fake_rot2_real(func,in_var,doutdin_var)
    IMPLICIT NONE

    !> function to evaluate
    CLASS(fake_function_real), INTENT(IN) :: func
    !> independent variable
    REAL(r8), DIMENSION(:), INTENT(IN) :: in_var
    !> dependent variable derivative, size (any, any), output is
    !> uniform in both dimensions
    REAL(r8), DIMENSION(:,:), INTENT(OUT) :: doutdin_var

    REAL(r8) :: poly_eval,poly_deriv
    REAL(r8), DIMENSION(SIZE(in_var)) :: dfdr
    INTEGER(i4) :: ic,ip

    IF (SIZE(in_var)/=2)                                                        &
      CALL par%nim_stop('fake_rot2_real requires SIZE(in_var)=2')
    dfdr=1._r8
    doutdin_var=1._r8
    DO ic=1,2
      poly_eval=0._r8
      poly_deriv=0._r8
      DO ip=0,MIN(func%pd,9)
        poly_eval=poly_eval + rcoef(ip)*in_var(ic)**ip
        poly_deriv=poly_deriv + ip*rcoef(ip)*in_var(ic)**(ip-1)
      ENDDO
      IF (ic==1) THEN
        dfdr=dfdr*[poly_deriv,poly_eval]
      ELSE
        dfdr=dfdr*[poly_eval,poly_deriv]
      ENDIF
    ENDDO
    doutdin_var(:,:)=dfdr(1)-dfdr(2)
  END SUBROUTINE fake_rot2_real

!-------------------------------------------------------------------------------
!> Set the polynomial degree and derivative type
!-------------------------------------------------------------------------------
  SUBROUTINE init_comp(func,poly_degree,deriv_type)
    IMPLICIT NONE

    !> function to set
    CLASS(fake_function_comp), INTENT(INOUT) :: func
    !> polynomial degree
    INTEGER(i4), INTENT(IN) :: poly_degree
    !> polynomial derivative type, see fake_eval_d_comp for acceptable values
    CHARACTER(*), INTENT(IN) :: deriv_type

    func%pd=poly_degree
    func%deriv_type=deriv_type
  END SUBROUTINE init_comp

!-------------------------------------------------------------------------------
!> Evaluate the polynomial at a given independent variable
!-------------------------------------------------------------------------------
  SUBROUTINE fake_eval_comp(func,in_var,out_var)
    IMPLICIT NONE

    !> function to evaluate
    CLASS(fake_function_comp), INTENT(IN) :: func
    !> independent variable
    REAL(r8), DIMENSION(:), INTENT(IN) :: in_var
    !> dependent variable
    COMPLEX(r8), DIMENSION(:,:), INTENT(OUT) :: out_var

    COMPLEX(r8) :: poly_eval
    INTEGER(i4) :: ic,ip,im

    out_var=1._r8
    DO ic=1,SIZE(in_var)
      poly_eval=0._r8
      DO ip=0,MIN(func%pd,9)
        poly_eval=poly_eval + ccoef(ip)*in_var(ic)**ip
      ENDDO
      DO im=1,SIZE(out_var,2)
        out_var(:,im)=out_var(:,im)*poly_eval/REAL(im,r8)
      ENDDO
    ENDDO
  END SUBROUTINE fake_eval_comp

  SUBROUTINE fake_eval_d_comp(func,in_var,doutdin_var)
    IMPLICIT NONE

    !> function to evaluate
    CLASS(fake_function_comp), INTENT(IN) :: func
    !> independent variable
    REAL(r8), DIMENSION(:), INTENT(IN) :: in_var
    !> dependent variable derivative, size varies by deriv_type
    COMPLEX(r8), DIMENSION(:,:,:), INTENT(OUT) :: doutdin_var

    SELECT CASE (TRIM(func%deriv_type))
    CASE ("grad")
      CALL func%grad(in_var,doutdin_var)
    CASE ("rot2")
      CALL func%rot2(in_var,doutdin_var)
    CASE DEFAULT
      CALL par%nim_stop("fake_eval_d deriv_type not recognized")
    END SELECT
  END SUBROUTINE fake_eval_d_comp

!-------------------------------------------------------------------------------
!> Evaluate the polynomial gradient at a given independent variable
!-------------------------------------------------------------------------------
  SUBROUTINE fake_grad_comp(func,in_var,doutdin_var)
    IMPLICIT NONE

    !> function to evaluate
    CLASS(fake_function_comp), INTENT(IN) :: func
    !> independent variable
    REAL(r8), DIMENSION(:), INTENT(IN) :: in_var
    !> dependent variable derivative, size (any, SIZE(in_var), nmodes),
    !> output is uniform in the second dimension
    COMPLEX(r8), DIMENSION(:,:,:), INTENT(OUT) :: doutdin_var

    COMPLEX(r8) :: poly_eval,poly_deriv
    INTEGER(i4) :: ic,idc,ip,iq,im,nin,nqty,nmodes

    nin=SIZE(in_var)
    nqty=SIZE(doutdin_var,1)
    nmodes=SIZE(doutdin_var,3)
    doutdin_var=1._r8
    DO ic=1,nin
      poly_eval=0._r8
      poly_deriv=0._r8
      DO ip=0,MIN(func%pd,9)
        poly_eval=poly_eval + ccoef(ip)*in_var(ic)**ip
        poly_deriv=poly_deriv + ip*ccoef(ip)*in_var(ic)**(ip-1)
      ENDDO
      DO im=1,nmodes
        DO idc=1,nin
          IF (ic==idc) THEN
            DO iq=1,nqty
              doutdin_var(iq,idc,im)=                                           &
                doutdin_var(iq,idc,im)*poly_deriv/REAL(im,r8)
            ENDDO
          ELSE
            DO iq=1,nqty
              doutdin_var(iq,idc,im)=                                           &
                doutdin_var(iq,idc,im)*poly_eval/REAL(im,r8)
            ENDDO
          ENDIF
        ENDDO
      ENDDO
    ENDDO
  END SUBROUTINE fake_grad_comp

!-------------------------------------------------------------------------------
!> Evaluate the polynomial 2D curl at a given independent variable
!-------------------------------------------------------------------------------
  SUBROUTINE fake_rot2_comp(func,in_var,doutdin_var)
    IMPLICIT NONE

    !> function to evaluate
    CLASS(fake_function_comp), INTENT(IN) :: func
    !> independent variable
    REAL(r8), DIMENSION(:), INTENT(IN) :: in_var
    !> dependent variable derivative, size (any, any, nmodes), output is
    !> uniform in 2nd and 3rd dimensions
    COMPLEX(r8), DIMENSION(:,:,:), INTENT(OUT) :: doutdin_var

    COMPLEX(r8) :: poly_eval,poly_deriv
    COMPLEX(r8), DIMENSION(SIZE(in_var,1),SIZE(doutdin_var,3)) :: dfdr
    INTEGER(i4) :: ic,iq,ip,im,nin,nqty,nmodes

    nin=SIZE(in_var)
    nqty=SIZE(doutdin_var,1)
    IF (nqty/=1) CALL par%nim_stop('fake_rot2_comp requires nqty=1')
    nmodes=SIZE(doutdin_var,3)
    dfdr=1._r8
    DO ic=1,nin
      poly_eval=0._r8
      poly_deriv=0._r8
      DO ip=0,MIN(func%pd,9)
        poly_eval=poly_eval + ccoef(ip)*in_var(ic)**ip
        poly_deriv=poly_deriv + ip*ccoef(ip)*in_var(ic)**(ip-1)
      ENDDO
      IF (ic==1) THEN
        DO im=1,nmodes
          dfdr(:,im)=dfdr(:,im)*[poly_deriv,poly_eval]/REAL(im,r8)
        ENDDO
      ELSEIF(ic==2) THEN
        DO im=1,nmodes
          dfdr(:,im)=dfdr(:,im)*[poly_eval,poly_deriv]/REAL(im,r8)
        ENDDO
      ENDIF
    ENDDO
    DO ic=1,SIZE(doutdin_var,2)
      DO iq=1,nqty
        doutdin_var(iq,ic,:)=dfdr(1,:)-dfdr(2,:)
      ENDDO
    ENDDO
  END SUBROUTINE fake_rot2_comp

END MODULE fake_function_mod
