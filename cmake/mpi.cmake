######################################################################
#
# Find MPI
#
######################################################################

if (ENABLE_MPI)
  if (NOT MPI_FOUND) # may be found by compiler cmake configuration
    message(STATUS "")
    message(STATUS "--------- Finding MPI ---------")
    find_package(MPI REQUIRED)
  endif ()
  if (MPI_FOUND)
    set(HAVE_MPI TRUE)
    if (MPI_RESULT_Fortran_test_mpi_F08_MODULE)
      set(HAVE_MPI_F08 TRUE)
      message(STATUS "Found mpi_f08 capable version")
    else ()
      message(STATUS "Using legacy MPI")
    endif ()
  endif ()
  if (${CMAKE_Fortran_COMPILER_ID} MATCHES "GNU"
      AND ${CMAKE_Fortran_COMPILER_VERSION} VERSION_LESS "12"
      AND ENABLE_MPI)
    # Bug in gcc prevents mpi_f08 with versions less than 12
    # https://gcc.gnu.org/bugzilla/show_bug.cgi?id=97046
    message(FATAL_ERROR "MPI requires GCC version 12+")
  endif ()
endif ()
