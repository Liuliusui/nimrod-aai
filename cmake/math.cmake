######################################################################
#
# CMake to find solver dependencies
#
######################################################################

option(ENABLE_FFTW "Whether to link with FFTW")
option(ENABLE_SuperLU "Whether to link with the SuperLU Direct Solver")
option(ENABLE_Lapack "Whether to link with Lapack")

message(STATUS "")
message(STATUS "--------- Finding math libraries ---------")

add_library(mathlibs INTERFACE)

#---------------------------------------------------------------------
#
# Find BLAS / LAPACK
# https://cmake.org/cmake/help/latest/module/FindBLAS.html
# https://cmake.org/cmake/help/latest/module/FindLAPACK.html
#
#---------------------------------------------------------------------

if (NOT ${CMAKE_Fortran_COMPILER_ID} MATCHES "NVHPC")
  if (ENABLE_SHARED)
    set(BLA_STATIC FALSE)
  else ()
    set(BLA_STATIC TRUE)
  endif ()
  find_package(BLAS)
  target_link_libraries(mathlibs INTERFACE BLAS::BLAS)
  if (ENABLE_Lapack)
    find_package(LAPACK)
    target_link_libraries(mathlibs INTERFACE LAPACK::LAPACK)
  endif()
endif ()

#---------------------------------------------------------------------
#
# Find SuperLU/SuperLU_dist
#
#---------------------------------------------------------------------

if (ENABLE_SuperLU)
  if (ENABLE_PARALLEL) # SuperLU_DIST
    find_library(Superlu_DIST_LIBRARIES
      NAMES libsuperlu_dist.a
      PATHS "${Superlu_DIST_ROOT_DIR}/lib"
      )
    target_link_libraries(mathlibs INTERFACE ${Superlu_DIST_LIBRARIES})
    find_path(Superlu_DIST_INCLUDE_DIRS
      NAMES superlu_defs.h
      PATHS "${Superlu_DIST_ROOT_DIR}/include"
      )
    if (NOT Superlu_DIST_LIBRARIES OR NOT Superlu_DIST_INCLUDE_DIRS)
      message(FATAL_ERROR "Superlu_DIST not found, set Superlu_DIST_ROOT_DIR")
    endif ()
  else () # SuperLU
    find_library(Superlu_LIBRARIES
      NAMES libsuperlu.a
      PATHS "${Superlu_ROOT_DIR}/lib"
      )
    target_link_libraries(mathlibs INTERFACE ${Superlu_LIBRARIES})
    find_path(Superlu_INCLUDE_DIRS
      NAMES slu_ddefs.h
      PATHS "${Superlu_ROOT_DIR}/include"
      )
    if (NOT Superlu_LIBRARIES OR NOT Superlu_INCLUDE_DIRS)
      message(FATAL_ERROR "Superlu not found, set Superlu_ROOT_DIR")
    endif ()
  endif ()
endif ()
  
#---------------------------------------------------------------------
#
# Find fftw - always use the serial version
# TODO
#
#---------------------------------------------------------------------

if (ENABLE_FFTW)
  #if (ENABLE_PARALLEL)
  #  set(ENABLE_PARALLEL FALSE)
  #  if (TARGET_MIC)
  #    set(Fftw3_FIND_VERSION "-ben")
  #  endif ()
  #  find_package(SciFftw3)
  #  set(ENABLE_PARALLEL TRUE)
  #else ()
  #  find_package(SciFftw3)
  #endif ()
endif ()


