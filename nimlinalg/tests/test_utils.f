#include "config.f"
!-------------------------------------------------------------------------------
!* test utils routines driver
!-------------------------------------------------------------------------------
PROGRAM test_utils
  USE local
  USE vector_mod
  USE matrix_mod
  USE seam_mod
  USE pardata_mod
  USE timer_mod
  USE linalg_utils_mod
  USE nimtest_utils
  USE linalg_registry_mod
  IMPLICIT NONE

  TYPE(rvec_storage), ALLOCATABLE :: rvec(:)
  TYPE(cv1m_storage), ALLOCATABLE :: cv1m(:)
  TYPE(cvec_storage), ALLOCATABLE :: cvec(:)
  TYPE(rmat_storage), ALLOCATABLE :: rmat(:)
  TYPE(cmat_storage), ALLOCATABLE :: cmat(:,:)
  TYPE(seam_type) :: seam
  INTEGER(i4) :: nargs,itest
  CHARACTER(64) :: test_name
  INTEGER(i4) :: idepth
  INTEGER(i4), SAVE :: iftn=-1
!-------------------------------------------------------------------------------
! determine which test to run from the command line
!-------------------------------------------------------------------------------
  nargs=command_argument_count()
  IF (nargs /= 2) THEN
    CALL print_usage
    CALL par%nim_stop('Argument error')
  ENDIF
  CALL get_command_argument(1,test_type)
  CALL get_command_argument(2,test_name)
  CALL par%init
  CALL timer%init
  CALL timer%start_timer_l0('test','test_utils',iftn,idepth)
!-------------------------------------------------------------------------------
! setup registry based on test type
!-------------------------------------------------------------------------------
  CALL set_test_type
!-------------------------------------------------------------------------------
! for the purposes of testing set apply_ave_factor=.TRUE. in linalg_utils_mod
!-------------------------------------------------------------------------------
  apply_ave_factor=.TRUE.
!-------------------------------------------------------------------------------
! loop over test parameters
!-------------------------------------------------------------------------------
  DO itest=1,ntests
    CALL setup_linalg(rvec,cv1m,cvec,rmat,cmat,seam,itest)
!-------------------------------------------------------------------------------
!   run the test
!-------------------------------------------------------------------------------
    SELECT CASE(TRIM(test_name))
    CASE ("matvec")
      CALL test_matvec(rvec,cv1m,cvec,rmat,cmat,seam)
    CASE ("resid_norm")
      CALL test_resid_norm(rvec,cv1m,cvec,rmat,cmat,seam)
    CASE ("norm")
      CALL test_norm(rvec,cv1m,cvec,seam)
    CASE ("dot")
      CALL test_dot(rvec,cv1m,cvec,seam)
    CASE DEFAULT
      CALL par%nim_write('No test named '//TRIM(test_name))
      CALL print_usage
    END SELECT
    CALL teardown_linalg(rvec,cv1m,cvec,rmat,cmat,seam)
  ENDDO
  CALL timer%end_timer_l0(iftn,idepth)
  CALL timer%report
  CALL timer%finalize
  CALL par%nim_stop('Normal termination.',clean_shutdown=.TRUE.)
CONTAINS

!-------------------------------------------------------------------------------
! helper routine to print usage
!-------------------------------------------------------------------------------
  SUBROUTINE print_usage
    CALL par%nim_write('Usage: ./test_utils <test type> <test name>')
    CALL print_types
    CALL par%nim_write('  where <test name> is one of')
    CALL par%nim_write('    matvec')
    CALL par%nim_write('    resid_norm')
    CALL par%nim_write('    norm')
    CALL par%nim_write('    dot')
    CALL par%nim_stop('check input')
  END SUBROUTINE print_usage

#ifdef __nvhpc
!-------------------------------------------------------------------------------
!* this routine is required as a workaround for an nvhpc bug
!-------------------------------------------------------------------------------
  SUBROUTINE test_matvec_cv1m(cv1m,cmat1m,seam)
    IMPLICIT NONE

    !* test cv1m
    TYPE(cv1m_storage), INTENT(INOUT) :: cv1m(par%nbl)
    !* test cmat
    TYPE(cmat_storage), INTENT(INOUT) :: cmat1m(par%nbl)
    !* test seam
    TYPE(seam_type), INTENT(INOUT) :: seam

    TYPE(cv1m_storage) :: c1chk(par%nbl)
    INTEGER(i4) :: ibl

    DO ibl=1,par%nbl
      CALL cv1m(ibl)%v%alloc_with_mold(c1chk(ibl)%v)
      CALL cv1m(ibl)%v%assign_for_testing('constant',CMPLX(2._r8,3._r8,r8))
      CALL cmat1m(ibl)%m%set_identity
    ENDDO
    CALL matvec(cmat1m,cv1m,c1chk,seam)
    DO ibl=1,par%nbl
      CALL wrtrue(cv1m(ibl)%v%test_if_equal(c1chk(ibl)%v),'matvec cv1m')
      CALL c1chk(ibl)%v%dealloc
    ENDDO
  END SUBROUTINE test_matvec_cv1m
#endif

!-------------------------------------------------------------------------------
!* test the matvec routine
!-------------------------------------------------------------------------------
  SUBROUTINE test_matvec(rvec,cv1m,cvec,rmat,cmat,seam)
    IMPLICIT NONE

    !* test rvec
    TYPE(rvec_storage), INTENT(INOUT) :: rvec(par%nbl)
    !* test cv1m
    TYPE(cv1m_storage), INTENT(INOUT) :: cv1m(par%nbl)
    !* test cvec
    TYPE(cvec_storage), INTENT(INOUT) :: cvec(par%nbl)
    !* test rmat
    TYPE(rmat_storage), INTENT(INOUT) :: rmat(par%nbl)
    !* test cmat
    TYPE(cmat_storage), INTENT(INOUT) :: cmat(par%nbl,par%nmodes)
    !* test seam
    TYPE(seam_type), INTENT(INOUT) :: seam

    TYPE(rvec_storage) :: rchk(par%nbl)
    TYPE(cv1m_storage) :: c1chk(par%nbl)
    TYPE(cvec_storage) :: cchk(par%nbl)
    INTEGER(i4) :: ibl,imode
!-------------------------------------------------------------------------------
!   create space, initialize the matrix to the identity,
!   and set rvec to the DOF index
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL rvec(ibl)%v%alloc_with_mold(rchk(ibl)%v)
      CALL rvec(ibl)%v%assign_for_testing('constant',1._r8)
      CALL rmat(ibl)%m%set_identity
    ENDDO
    CALL matvec(rmat,rvec,rchk,seam)
    DO ibl=1,par%nbl
      CALL wrtrue(rvec(ibl)%v%test_if_equal(rchk(ibl)%v),'matvec real')
      CALL rchk(ibl)%v%dealloc
    ENDDO
!-------------------------------------------------------------------------------
!   test the complex version -- single-mode interface
!-------------------------------------------------------------------------------
#ifdef __nvhpc
    CALL test_matvec_cv1m(cv1m,cmat(:,1),seam)
#else
    DO ibl=1,par%nbl
      CALL cv1m(ibl)%v%alloc_with_mold(c1chk(ibl)%v)
      CALL cv1m(ibl)%v%assign_for_testing('constant',CMPLX(2._r8,3._r8,r8))
      CALL cmat(ibl,1)%m%set_identity
    ENDDO
    CALL matvec(cmat(:,1),cv1m,c1chk,seam)
    DO ibl=1,par%nbl
      CALL wrtrue(cv1m(ibl)%v%test_if_equal(c1chk(ibl)%v),'matvec cv1m')
      CALL c1chk(ibl)%v%dealloc
    ENDDO
#endif
!-------------------------------------------------------------------------------
!   test the complex version -- multi-mode interface
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL cvec(ibl)%v%alloc_with_mold(cchk(ibl)%v)
      CALL cvec(ibl)%v%assign_for_testing('constant',CMPLX(4._r8,5._r8,r8))
      DO imode=1,par%nmodes
        CALL cmat(ibl,imode)%m%set_identity
      ENDDO
    ENDDO
    CALL matvec(cmat,cvec,cchk,seam)
    DO ibl=1,par%nbl
      CALL wrtrue(cvec(ibl)%v%test_if_equal(cchk(ibl)%v),'matvec comp')
      CALL cchk(ibl)%v%dealloc
    ENDDO
  END SUBROUTINE test_matvec

#ifdef __nvhpc
!-------------------------------------------------------------------------------
!* this routine is required as a workaround for an nvhpc bug
!-------------------------------------------------------------------------------
  SUBROUTINE test_resid_norm_cv1m(cv1m,cmat1m,seam)
    IMPLICIT NONE

    !* test cv1m
    TYPE(cv1m_storage), INTENT(INOUT) :: cv1m(par%nbl)
    !* test cmat
    TYPE(cmat_storage), INTENT(INOUT) :: cmat1m(par%nbl)
    !* test seam
    TYPE(seam_type), INTENT(INOUT) :: seam

    TYPE(cv1m_storage) :: c1sln(par%nbl),c1res(par%nbl)
    INTEGER(i4) :: ibl
    REAL(r8) :: error

    DO ibl=1,par%nbl
      CALL cv1m(ibl)%v%alloc_with_mold(c1sln(ibl)%v)
      CALL cv1m(ibl)%v%alloc_with_mold(c1res(ibl)%v)
      CALL cv1m(ibl)%v%assign_for_testing('constant',CMPLX(4._r8,5._r8,r8))
      CALL c1sln(ibl)%v%assign_for_testing('constant',CMPLX(4._r8,5._r8,r8))
      CALL cmat1m(ibl)%m%set_identity
    ENDDO
    CALL resid_norm(cmat1m,c1sln,cv1m,c1res,error,seam,'inf',1._r8)
    CALL wrequal(error,0._r8,'resid_norm inf_norm cv1m')
    CALL resid_norm(cmat1m,c1sln,cv1m,c1res,error,seam,'l2',1._r8)
    CALL wrequal(error,0._r8,'resid_norm l2_norm cv1m')
    DO ibl=1,par%nbl
      CALL c1sln(ibl)%v%dealloc
      CALL c1res(ibl)%v%dealloc
    ENDDO
  END SUBROUTINE test_resid_norm_cv1m
#endif

!-------------------------------------------------------------------------------
!* test the resid_norm routine
!-------------------------------------------------------------------------------
  SUBROUTINE test_resid_norm(rvec,cv1m,cvec,rmat,cmat,seam)
    IMPLICIT NONE

    !* test rvec
    TYPE(rvec_storage), INTENT(INOUT) :: rvec(par%nbl)
    !* test cv1m
    TYPE(cv1m_storage), INTENT(INOUT) :: cv1m(par%nbl)
    !* test cvec
    TYPE(cvec_storage), INTENT(INOUT) :: cvec(par%nbl)
    !* test rmat
    TYPE(rmat_storage), INTENT(INOUT) :: rmat(par%nbl)
    !* test cmat
    TYPE(cmat_storage), INTENT(INOUT) :: cmat(par%nbl,par%nmodes)
    !* test seam
    TYPE(seam_type), INTENT(INOUT) :: seam

    TYPE(rvec_storage) :: rsln(par%nbl),rres(par%nbl)
    TYPE(cv1m_storage) :: c1sln(par%nbl),c1res(par%nbl)
    TYPE(cvec_storage) :: csln(par%nbl),cres(par%nbl)
    INTEGER(i4) :: ibl,imode
    REAL(r8) :: error
!-------------------------------------------------------------------------------
!   set the RHS (rvec) and sln to a constant and the matrix to the identity
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL rvec(ibl)%v%alloc_with_mold(rsln(ibl)%v)
      CALL rvec(ibl)%v%alloc_with_mold(rres(ibl)%v)
      CALL rvec(ibl)%v%assign_for_testing('constant',1._r8)
      CALL rsln(ibl)%v%assign_for_testing('constant',1._r8)
      CALL rmat(ibl)%m%set_identity
    ENDDO
    CALL resid_norm(rmat,rsln,rvec,rres,error,seam,'inf',1._r8)
    CALL wrequal(error,0._r8,'resid_norm inf_norm real')
    CALL resid_norm(rmat,rsln,rvec,rres,error,seam,'l2',1._r8)
    CALL wrequal(error,0._r8,'resid_norm l2_norm real')
    DO ibl=1,par%nbl
      CALL rsln(ibl)%v%dealloc
      CALL rres(ibl)%v%dealloc
    ENDDO
!-------------------------------------------------------------------------------
!   test the complex version -- single-mode interface
!-------------------------------------------------------------------------------
#ifdef __nvhpc
    CALL test_resid_norm_cv1m(cv1m,cmat(:,1),seam)
#else
    DO ibl=1,par%nbl
      CALL cv1m(ibl)%v%alloc_with_mold(c1sln(ibl)%v)
      CALL cv1m(ibl)%v%alloc_with_mold(c1res(ibl)%v)
      CALL cv1m(ibl)%v%assign_for_testing('constant',CMPLX(4._r8,5._r8,r8))
      CALL c1sln(ibl)%v%assign_for_testing('constant',CMPLX(4._r8,5._r8,r8))
      CALL cmat(ibl,1)%m%set_identity
    ENDDO
    CALL resid_norm(cmat(:,1),c1sln,cv1m,c1res,error,seam,'inf',1._r8)
    CALL wrequal(error,0._r8,'resid_norm inf_norm cv1m')
    CALL resid_norm(cmat(:,1),c1sln,cv1m,c1res,error,seam,'l2',1._r8)
    CALL wrequal(error,0._r8,'resid_norm l2_norm cv1m')
    DO ibl=1,par%nbl
      CALL c1sln(ibl)%v%dealloc
      CALL c1res(ibl)%v%dealloc
    ENDDO
#endif
!-------------------------------------------------------------------------------
!   test the complex version -- multi-mode interface
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL cvec(ibl)%v%alloc_with_mold(csln(ibl)%v)
      CALL cvec(ibl)%v%alloc_with_mold(cres(ibl)%v)
      CALL cvec(ibl)%v%assign_for_testing('constant',CMPLX(6._r8,7._r8,r8))
      CALL csln(ibl)%v%assign_for_testing('constant',CMPLX(6._r8,7._r8,r8))
      DO imode=1,par%nmodes
        CALL cmat(ibl,imode)%m%set_identity
      ENDDO
    ENDDO
    CALL resid_norm(cmat,csln,cvec,cres,error,seam,'inf',1._r8)
    CALL wrequal(error,0._r8,'resid_norm inf_norm comp')
    CALL resid_norm(cmat,csln,cvec,cres,error,seam,'l2',1._r8)
    CALL wrequal(error,0._r8,'resid_norm l2_norm comp')
    DO ibl=1,par%nbl
      CALL csln(ibl)%v%dealloc
      CALL cres(ibl)%v%dealloc
    ENDDO
  END SUBROUTINE test_resid_norm

!-------------------------------------------------------------------------------
!* test the norm routine
!-------------------------------------------------------------------------------
  SUBROUTINE test_norm(rvec,cv1m,cvec,seam)
    IMPLICIT NONE

    !* test rvec
    TYPE(rvec_storage), INTENT(INOUT) :: rvec(par%nbl)
    !* test cv1m
    TYPE(cv1m_storage), INTENT(INOUT) :: cv1m(par%nbl)
    !* test cvec
    TYPE(cvec_storage), INTENT(INOUT) :: cvec(par%nbl)
    !* test seam
    TYPE(seam_type), INTENT(INOUT) :: seam

    INTEGER(i4) :: ibl
    REAL(r8) :: norm_out
!-------------------------------------------------------------------------------
!   set the rvec and sln to the dof index and check norm
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL rvec(ibl)%v%assign_for_testing('constant',1._r8)
    ENDDO
    norm_out=norm(rvec,seam,'inf')
    CALL wrequal(norm_out,1._r8,'norm inf real')
    norm_out=norm(rvec,seam,'l2')
    ! no good check
!-------------------------------------------------------------------------------
!   test the complex version -- single-mode interface
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL cv1m(ibl)%v%assign_for_testing('constant',CMPLX(4._r8,5._r8,r8))
    ENDDO
    norm_out=norm(cv1m,seam,'inf')
    CALL wrequal(norm_out,ABS(CMPLX(4._r8,5._r8,r8)),'norm inf cv1m')
    norm_out=norm(cv1m,seam,'l2')
    ! no good check
!-------------------------------------------------------------------------------
!   test the complex version -- multi-mode interface
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL cvec(ibl)%v%assign_for_testing('constant',CMPLX(6._r8,7._r8,r8))
    ENDDO
    norm_out=norm(cvec,seam,'inf')
    CALL wrequal(norm_out,ABS(CMPLX(6._r8,7._r8,r8)),'norm inf comp')
    norm_out=norm(cvec,seam,'l2')
    ! no good check
  END SUBROUTINE test_norm

!-------------------------------------------------------------------------------
!* test the dot routine
!-------------------------------------------------------------------------------
  SUBROUTINE test_dot(rvec,cv1m,cvec,seam)
    IMPLICIT NONE

    !* test rvec
    TYPE(rvec_storage), INTENT(INOUT) :: rvec(par%nbl)
    !* test cv1m
    TYPE(cv1m_storage), INTENT(INOUT) :: cv1m(par%nbl)
    !* test cvec
    TYPE(cvec_storage), INTENT(INOUT) :: cvec(par%nbl)
    !* test seam
    TYPE(seam_type), INTENT(INOUT) :: seam

    TYPE(rvec_storage) :: rvec2(par%nbl)
    TYPE(cv1m_storage) :: cv1m2(par%nbl)
    TYPE(cvec_storage) :: cvec2(par%nbl)
    INTEGER(i4) :: ibl
    REAL(r8) :: dot_out
    COMPLEX(r8) :: cdot_out
!-------------------------------------------------------------------------------
!   set the RHS (rvec) and sln to the dof index and the matrix to the identity
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL rvec(ibl)%v%alloc_with_mold(rvec2(ibl)%v)
      CALL rvec(ibl)%v%assign_for_testing('index')
      CALL rvec2(ibl)%v%assign_for_testing('constant',0._r8)
    ENDDO
    dot_out=dot(rvec,rvec2,seam)
    CALL wrequal(dot_out,0._r8,'dot real')
    DO ibl=1,par%nbl
      CALL rvec2(ibl)%v%dealloc
    ENDDO
!-------------------------------------------------------------------------------
!   test the complex version -- single-mode interface
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL cv1m(ibl)%v%alloc_with_mold(cv1m2(ibl)%v)
      CALL cv1m(ibl)%v%assign_for_testing('index')
      CALL cv1m2(ibl)%v%assign_for_testing('constant',CMPLX(0._r8,0._r8,r8))
    ENDDO
    cdot_out=dot(cv1m,cv1m2,seam)
    CALL wrequal(ABS(dot_out),0._r8,'dot cv1m')
    DO ibl=1,par%nbl
      CALL cv1m2(ibl)%v%dealloc
    ENDDO
!-------------------------------------------------------------------------------
!   test the complex version -- multi-mode interface
!-------------------------------------------------------------------------------
    DO ibl=1,par%nbl
      CALL cvec(ibl)%v%alloc_with_mold(cvec2(ibl)%v)
      CALL cvec(ibl)%v%assign_for_testing('index')
      CALL cvec2(ibl)%v%assign_for_testing('constant',CMPLX(0._r8,0._r8,r8))
    ENDDO
    cdot_out=dot(cvec,cvec2,seam)
    CALL wrequal(ABS(dot_out),0._r8,'dot comp')
    DO ibl=1,par%nbl
      CALL cvec2(ibl)%v%dealloc
    ENDDO
  END SUBROUTINE test_dot

END PROGRAM test_utils
